#!/bin/bash

if [ ! -f $1 ]; then
    echo "Please provide a PDDL HTN problem file."
    exit 1
fi

f=$1
dir=`dirname $f`

mkdir -p $dir/non_htn/

if grep -qE "\(define( *)\(domain" $f ; then
    # Domain file
    cat $f | perl -pe 's|(.*);.*$|\1|' \
    | tr '\n' '€' \
    | perl -pe 's|\(:requirements.*?\)||' \
    | perl -pe 's|\(:method.*$|\)|' \
    | tr '€' '\n' \
    > $dir/non_htn/`basename $f`
else
    # Problem file
    cat $f | perl -pe 's|(.*);.*$|\1|' \
    | tr '\n' '€' \
    | perl -pe 's|\(:requirements.*?\)||' \
    | perl -pe 's|:tasks.*?:constraints.*?and.*?\(and|\(and|' \
    | perl -pe 's|(.*)t[0-9]+\)[ \t€]*\)|\1|g' \
    | tr '€' '\n' \
    > $dir/non_htn/`basename $f`
fi

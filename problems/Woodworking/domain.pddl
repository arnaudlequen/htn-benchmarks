;; Woodworking
;;

(define (domain woodworking)
  (:requirements :typing :strips :htn :equality :negative-preconditions)
  (:types
      acolour awood woodobj machine
      surface treatmentstatus
      aboardsize apartsize - object
      highspeed-saw glazer grinder immersion-varnisher
      planer saw spray-varnisher - machine
      board part - woodobj)

  (:constants
              verysmooth smooth rough - surface
              varnished glazed untreated colourfragments - treatmentstatus
              natural - acolour
              small medium large - apartsize)

  (:predicates
            (unused ?obj - part)
            (available ?obj - woodobj)

            (surface-condition ?obj - woodobj ?surface - surface)
            (treatment ?obj - part ?treatment - treatmentstatus)
            (colour ?obj - part ?colour - acolour)
            (wood ?obj - woodobj ?wood - awood)
            (boardsize ?board - board ?size - aboardsize)
            (goalsize ?part - part ?size - apartsize)
            (boardsize-successor ?size1 ?size2 - aboardsize)

            (in-highspeed-saw ?b - board ?m - highspeed-saw)
            (empty ?m - highspeed-saw)
            (has-colour ?machine - machine ?colour - acolour)
            (contains-part ?b - board ?p - part)
            (grind-treatment-change ?old ?new - treatmentstatus)
            (is-smooth ?surface - surface))

  ;(:functions (total-cost) - number
  ;          (spray-varnish-cost ?obj - part) - number
  ;          (glaze-cost ?obj - part) - number
  ;          (grind-cost ?obj - part) - number
  ;          (plane-cost ?obj - part) - number)

  (:action do-immersion-varnish
    :parameters (?x - part ?m - immersion-varnisher
                 ?newcolour - acolour ?surface - surface)
    :precondition (and
            (available ?x)
            (has-colour ?m ?newcolour)
            (surface-condition ?x ?surface)
            (is-smooth ?surface)
            (treatment ?x untreated);REMOVE THIS LINE TO MAKE THINGS WORK
            )
    :effect (and
            ;(increase (total-cost) 10)
            (not (treatment ?x untreated))
            (treatment ?x varnished)
            (not (colour ?x natural))
            (colour ?x ?newcolour)))

  (:action do-spray-varnish
    :parameters (?x - part ?m - spray-varnisher
                 ?newcolour - acolour ?surface - surface)
    :precondition (and
            (available ?x)
            (has-colour ?m ?newcolour)
            (surface-condition ?x ?surface)
            (is-smooth ?surface)
            (treatment ?x untreated))
    :effect (and
            ;(increase (total-cost) (spray-varnish-cost ?x))
            (not (treatment ?x untreated))
            (treatment ?x varnished)
            (not (colour ?x natural))
            (colour ?x ?newcolour)))

  (:action do-glaze
    :parameters (?x - part ?m - glazer
                 ?newcolour - acolour)
    :precondition (and
            (available ?x)
            (has-colour ?m ?newcolour)
            (treatment ?x untreated))
    :effect (and
            ;(increase (total-cost) (glaze-cost ?x))
            (not (treatment ?x untreated))
            (treatment ?x glazed)
            (not (colour ?x natural))
            (colour ?x ?newcolour)))

  (:action do-grind
    :parameters (?x - part ?m - grinder ?oldsurface - surface
                 ?oldcolour - acolour
                 ?oldtreatment ?newtreatment - treatmentstatus)
    :precondition (and
            (available ?x)
            (surface-condition ?x ?oldsurface)
            (is-smooth ?oldsurface)
            (colour ?x ?oldcolour)
            (treatment ?x ?oldtreatment)
            (grind-treatment-change ?oldtreatment ?newtreatment))
    :effect (and
            ;(increase (total-cost) (grind-cost ?x))
            (not (surface-condition ?x ?oldsurface))
            (surface-condition ?x verysmooth)
            (not (treatment ?x ?oldtreatment))
            (treatment ?x ?newtreatment)
            (not (colour ?x ?oldcolour))
            (colour ?x natural)))

  (:action do-plane
    :parameters (?x - part ?m - planer ?oldsurface - surface
                 ?oldcolour - acolour ?oldtreatment - treatmentstatus)
    :precondition (and
            (available ?x)
            (surface-condition ?x ?oldsurface)
            (treatment ?x ?oldtreatment)
            (colour ?x ?oldcolour))
    :effect (and
            ;(increase (total-cost) (plane-cost ?x))
            (not (surface-condition ?x ?oldsurface))
            (surface-condition ?x smooth)
            (not (treatment ?x ?oldtreatment));THIS LINE ALSO MAKES THINGS WORK WHEN REMOVED
            (treatment ?x untreated)
            (not (colour ?x ?oldcolour))
            (colour ?x natural)))

  (:action load-highspeed-saw
    :parameters (?b - board ?m - highspeed-saw)
    :precondition (and
            (empty ?m)
            (available ?b))
    :effect (and
            ;(increase (total-cost) 30)
            (not (available ?b))
            (not (empty ?m))
            (in-highspeed-saw ?b ?m)))

  (:action unload-highspeed-saw
    :parameters (?b - board ?m - highspeed-saw)
    :precondition (in-highspeed-saw ?b ?m)
    :effect (and
            ;(increase (total-cost) 10)
            (available ?b)
            (not (in-highspeed-saw ?b ?m))
            (empty ?m)))

  (:action cut-board-small
    :parameters (?b - board ?p - part ?m - highspeed-saw ?w - awood
                 ?surface - surface ?size_before ?size_after - aboardsize)
    :precondition (and
            (unused ?p)
            (goalsize ?p small)
            (in-highspeed-saw ?b ?m)
            (wood ?b ?w)
            (surface-condition ?b ?surface)
            (boardsize ?b ?size_before)
            (boardsize-successor ?size_after ?size_before))
    :effect (and
            ;(increase (total-cost) 10)
            (not (unused ?p))
            (available ?p)
            (wood ?p ?w)
            (surface-condition ?p ?surface)
            (colour ?p natural)
            (treatment ?p untreated)
            (boardsize ?b ?size_after)))

  (:action cut-board-medium
    :parameters (?b - board ?p - part ?m - highspeed-saw ?w - awood
                 ?surface - surface
                 ?size_before ?s1 ?size_after - aboardsize)
    :precondition (and
            (unused ?p)
            (goalsize ?p medium)
            (in-highspeed-saw ?b ?m)
            (wood ?b ?w)
            (surface-condition ?b ?surface)
            (boardsize ?b ?size_before)
            (boardsize-successor ?size_after ?s1)
            (boardsize-successor ?s1 ?size_before))
    :effect (and
            ;(increase (total-cost) 10)
            (not (unused ?p))
            (available ?p)
            (wood ?p ?w)
            (surface-condition ?p ?surface)
            (colour ?p natural)
            (treatment ?p untreated)
            (boardsize ?b ?size_after)))

  (:action cut-board-large
    :parameters (?b - board ?p - part ?m - highspeed-saw ?w - awood
                 ?surface - surface
                 ?size_before ?s1 ?s2 ?size_after - aboardsize)
    :precondition (and
            (unused ?p)
            (goalsize ?p large)
            (in-highspeed-saw ?b ?m)
            (wood ?b ?w)
            (surface-condition ?b ?surface)
            (boardsize ?b ?size_before)
            (boardsize-successor ?size_after ?s1)
            (boardsize-successor ?s1 ?s2)
            (boardsize-successor ?s2 ?size_before))
    :effect (and
            ;(increase (total-cost) 10)
            (not (unused ?p))
            (available ?p)
            (wood ?p ?w)
            (surface-condition ?p ?surface)
            (colour ?p natural)
            (treatment ?p untreated)
            (boardsize ?b ?size_after)))

  (:action do-saw-small
    :parameters (?b - board ?p - part ?m - saw ?w - awood
                 ?surface - surface ?size_before ?size_after - aboardsize)
    :precondition (and
            (unused ?p)
            (goalsize ?p small)
            (available ?b)
            (wood ?b ?w)
            (surface-condition ?b ?surface)
            (boardsize ?b ?size_before)
            (boardsize-successor ?size_after ?size_before))
    :effect (and
            ;(increase (total-cost) 30)
            (not (unused ?p))
            (available ?p)
            (wood ?p ?w)
            (surface-condition ?p ?surface)
            (colour ?p natural)
            (treatment ?p untreated)
            (boardsize ?b ?size_after)))

  (:action do-saw-medium
    :parameters (?b - board ?p - part ?m - saw ?w - awood
                 ?surface - surface
                 ?size_before ?s1 ?size_after - aboardsize)
    :precondition (and
            (unused ?p)
            (goalsize ?p medium)
            (available ?b)
            (wood ?b ?w)
            (surface-condition ?b ?surface)
            (boardsize ?b ?size_before)
            (boardsize-successor ?size_after ?s1)
            (boardsize-successor ?s1 ?size_before))
    :effect (and
            ;(increase (total-cost) 30)
            (not (unused ?p))
            (available ?p)
            (wood ?p ?w)
            (surface-condition ?p ?surface)
            (colour ?p natural)
            (treatment ?p untreated)
            (boardsize ?b ?size_after)))

  (:action do-saw-large
    :parameters (?b - board ?p - part ?m - saw ?w - awood
                 ?surface - surface
                 ?size_before ?s1 ?s2 ?size_after - aboardsize)
    :precondition (and
            (unused ?p)
            (goalsize ?p large)
            (available ?b)
            (wood ?b ?w)
            (surface-condition ?b ?surface)
            (boardsize ?b ?size_before)
            (boardsize-successor ?size_after ?s1)
            (boardsize-successor ?s1 ?s2)
            (boardsize-successor ?s2 ?size_before))
    :effect (and
            ;(increase (total-cost) 30)
            (not (unused ?p))
            (available ?p)
            (wood ?p ?w)
            (surface-condition ?p ?surface)
            (colour ?p natural)
            (treatment ?p untreated)
            (boardsize ?b ?size_after)))

    (:action nop
         :parameters    ()
         :precondition  ()
         :effect        (and))

;-------------------------------------------------------------------
;			               				Methods
;-------------------------------------------------------------------
;                   Varnish a part: colour + treatment
;-------------------------------------------------------------------

(:method colour-varnish-part
  :parameters (?p - part ?newcolour - acolour)
  :expansion 	(
                  (tag t1 (do-immersion-varnish ?p ?m ?newcolour ?surface))
              )
  :constraints
        (and (before (and
                (available ?p)
                (has-colour ?m ?newcolour)
                (surface-condition ?p ?surface)
                (is-smooth ?surface)
                (treatment ?p untreated);REMOVE THIS LINE TO MAKE IT WORK
            )
          t1)
        )
)

(:method colour-varnish-part
  :parameters (?p - part ?newcolour - acolour)
  :expansion 	(
                  (tag t1 (do-spray-varnish ?p ?m ?newcolour ?surface))
              )
  :constraints
        (and (before (and
                (available ?p)
                (has-colour ?m ?newcolour)
                (surface-condition ?p ?surface)
                (is-smooth ?surface)
                (treatment ?p untreated)
            )
          t1)
        )
)

;-------------------------------------------------------------------
;                   Glaze a part: colour + treatment
;-------------------------------------------------------------------

(:method colour-glaze-part
  :parameters (?p - part ?newcolour - acolour)
  :expansion 	(
                  (tag t1 (do-glaze ?p ?m ?newcolour))
              )
  :constraints
        (and (before (and
                (available ?p)
                (has-colour ?m ?newcolour)
                (treatment ?p untreated)
            )
          t1)
        )
)


;-------------------------------------------------------------------
;                       Cut a part out of a board
;-------------------------------------------------------------------
;The size of the part could also be infered from the predicates
; Small
(:method setup-part
  :parameters (?p - part ?w - awood ?s - apartsize)
  :expansion 	(
                  (tag t1 (do-saw-small ?board ?p ?m ?w ?surface ?size_before ?size_after))
              )
  :constraints
        (and (before (and
                (unused ?p)
                (= ?s small)
                (available ?board)
                (wood ?board ?w)
                (surface-condition ?board ?surface)
                (boardsize ?board ?size_before)
                (boardsize-successor ?size_after ?size_before)
            )
          t1)
        )
)

; Medium
(:method setup-part
  :parameters (?p - part ?w - awood ?s - apartsize)
  :expansion 	(
                  (tag t1 (do-saw-medium ?board ?p ?m ?w ?surface ?size_before ?s1 ?size_after))
              )
  :constraints
        (and (before (and
                (unused ?p)
                (= ?s medium)
                (available ?board)
                (wood ?board ?w)
                (surface-condition ?board ?surface)
                (boardsize ?board ?size_before)
                (boardsize-successor ?size_after ?s1)
                (boardsize-successor ?s1 ?size_before)
            )
          t1)
        )
)

; Large
(:method setup-part
  :parameters (?p - part ?w - awood ?s - apartsize)
  :expansion 	(
                  (tag t1 (do-saw-large ?board ?p ?m ?w ?surface ?size_before ?s1 ?s2 ?size_after))
              )
  :constraints
        (and (before (and
                (unused ?p)
                (= ?s large)
                (available ?board)
                (wood ?board ?w)
                (surface-condition ?board ?surface)
                (boardsize ?board ?size_before)
                (boardsize-successor ?size_after ?s1)
                (boardsize-successor ?s1 ?s2)
                (boardsize-successor ?s2 ?size_before)
            )
          t1)
        )
)

;-------------------------------------------------------------------
;                     Smooth the surface of a part
;-------------------------------------------------------------------

(:method change-surface-part
  :parameters (?p - part ?s - surface)
  :expansion 	(
                  (tag t1 (nop))
              )
  :constraints
        (and (before (and
                (surface-condition ?p ?s)
            )
          t1)
        )
)

(:method change-surface-part
  :parameters (?p - part ?s - surface)
  :expansion 	(
                  (tag t1 (do-plane ?p ?m ?oldsurface ?oldcolour ?oldtreatment))
              )
  :constraints
        (and (before (and
                (not (surface-condition ?p ?s))
                (= ?s smooth)
                (available ?p)
                (surface-condition ?p ?oldsurface)
                (treatment ?p ?oldtreatment)
                (colour ?p ?oldcolour)
            )
          t1)
        )
)

(:method change-surface-part
  :parameters (?p - part ?s - surface)
  :expansion 	(
                  (tag t1 (do-grind ?p ?m ?oldsurface ?oldcolour ?oldtreatment ?newtreatment))
              )
  :constraints
        (and (before (and
                (not (surface-condition ?p ?s))
                (= ?s verysmooth)
                (available ?p)
                (surface-condition ?p ?oldsurface)
                (is-smooth ?oldsurface)
                (colour ?p ?oldcolour)
                (treatment ?p ?oldtreatment)
                (grind-treatment-change ?oldtreatment ?newtreatment)
            )
          t1)
        )
)

; Can be optimized
(:method change-surface-part
  :parameters (?p - part ?s - surface)
  :expansion 	(
                  (tag t1 (do-plane ?p ?m ?oldsurface ?oldcolour ?oldtreatment))
                  (tag t2 (change-surface-part ?p ?s))
              )
  :constraints
        (and (before (and
                (not (surface-condition ?p ?s))
                (= ?s verysmooth)
                (available ?p)
                (surface-condition ?p ?oldsurface)
                (not (is-smooth ?oldsurface))
                (colour ?p ?oldcolour)
                (treatment ?p ?oldtreatment)
                (grind-treatment-change ?oldtreatment ?newtreatment)
            )
          t1)
        )
)

;-------------------------------------------------------------------
;                 Set the treatment to untreated
;-------------------------------------------------------------------

(:method untreat-part
  :parameters (?p - part)
  :expansion 	(
                  (tag t1 (nop))
              )
  :constraints
        (and (before (and
                (treatment ?p untreated)
            )
          t1)
        )
)

(:method untreat-part
  :parameters (?p - part)
  :expansion 	(
                  (tag t1 (do-plane ?p ?m ?oldsurface ?oldcolour ?oldtreatment))
              )
  :constraints
        (and (before (and
                (not (treatment ?p untreated))
                (available ?p)
                (surface-condition ?p ?oldsurface)
                (treatment ?p ?oldtreatment)
                (colour ?p ?oldcolour)
            )
          t1)
        )
)

;-------------------------------------------------------------------
;             Prepare a part so that it can be coloured and treated
;-------------------------------------------------------------------

(:method prepare-part
  :parameters (?p - part ?surface - surface ?w - awood ?size - apartsize)
  :expansion 	(
                  (tag t1 (setup-part ?p ?w ?size))
                  (tag t2 (untreat-part ?p))
                  (tag t3 (change-surface-part ?p ?surface))
              )
  :constraints
        (and (before (and
                (goalsize ?p ?size)
            )
          t1)
        )
)

;-------------------------------------------------------------------
;                            Make a full part
;-------------------------------------------------------------------

;Make a part: make sure the part is ready
(:method make-part
  :parameters (?p - part ?surface - surface ?w - awood ?size - apartsize ?c - acolour ?t - treatmentstatus)
  :expansion 	(
                  (tag t1 (prepare-part ?p ?surface ?w ?size))
                  (tag t2 (make-part-2 ?p ?c ?t))
              )
  :constraints
        (and (before (and
                (unused ?p)
            )
          t1)
        )
)

(:method make-part
  :parameters (?p - part ?surface - surface ?w - awood ?size - apartsize ?c - acolour ?t - treatmentstatus)
  :expansion 	(
                  (tag t1 (untreat-part ?p))
                  (tag t2 (make-part-2 ?p ?c ?t))
              )
  :constraints
        (and (before (and
                (not (unused ?p))
            )
          t1)
        )
)

; Might be somewhat redundant
(:method make-part
  :parameters (?p - part ?c - acolour ?t - treatmentstatus)
  :expansion 	(
                  (tag t1 (untreat-part ?p))
                  (tag t2 (make-part-2 ?p ?c ?t))
              )
  :constraints
        (and (before (and
                (not (unused ?p))
            )
          t1)
        )
)

(:method make-part
  :parameters (?p - part ?c - acolour)
  :expansion 	(
                  (tag t1 (untreat-part ?p))
                  (tag t2 (make-part-2 ?p ?c))
              )
  :constraints
        (and (before (and
                (not (unused ?p))
            )
          t1)
        )
)

;Make a part: color and treatment
(:method make-part-2
  :parameters (?p - part ?c - acolour)
  :expansion 	(
                  (tag t1 (nop))
              )
  :constraints
        (and (before (and
                (colour ?p ?c)
            )
          t1)
        )
)

(:method make-part-2
  :parameters (?p - part ?c - acolour ?t - treatmentstatus)
  :expansion 	(
                  (tag t1 (colour-glaze-part ?p ?c))
              )
  :constraints
        (and (before (and
                (= ?t glazed)
            )
          t1)
        )
)

(:method make-part-2
  :parameters (?p - part ?c - acolour ?t - treatmentstatus)
  :expansion 	(
                  (tag t1 (colour-varnish-part ?p ?c))
              )
  :constraints
        (and (before (and
                (= ?t varnished)
            )
          t1)
        )
)

)

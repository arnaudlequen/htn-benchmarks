(define (domain parking)
 (:requirements :strips :typing :htn :equality :negative-preconditions)
 (:types car curb)
 (:predicates
    (at-curb ?car - car)
    (at-curb-num ?car - car ?curb - curb)
    (behind-car ?car ?front-car - car)
    (car-clear ?car - car)
    (curb-clear ?curb - curb)
 )



	(:action move-curb-to-curb
		:parameters (?car - car ?curbsrc ?curbdest - curb)
		:precondition (and
			(car-clear ?car)
			(curb-clear ?curbdest)
			(at-curb-num ?car ?curbsrc)
		)
		:effect (and
			(not (curb-clear ?curbdest))
			(curb-clear ?curbsrc)
			(at-curb-num ?car ?curbdest)
			(not (at-curb-num ?car ?curbsrc))

		)
	)

	(:action move-curb-to-car
		:parameters (?car - car ?curbsrc - curb ?cardest - car)
		:precondition (and
			(car-clear ?car)
			(car-clear ?cardest)
			(at-curb-num ?car ?curbsrc)
			(at-curb ?cardest)
		)
		:effect (and
			(not (car-clear ?cardest))
			(curb-clear ?curbsrc)
			(behind-car ?car ?cardest)
			(not (at-curb-num ?car ?curbsrc))
			(not (at-curb ?car))

		)
	)

	(:action move-car-to-curb
		:parameters (?car - car ?carsrc - car ?curbdest - curb)
		:precondition (and
			(car-clear ?car)
			(curb-clear ?curbdest)
			(behind-car ?car ?carsrc)
		)
		:effect (and
			(not (curb-clear ?curbdest))
			(car-clear ?carsrc)
			(at-curb-num ?car ?curbdest)
			(not (behind-car ?car ?carsrc))
			(at-curb ?car)

		)
	)

	(:action move-car-to-car
		:parameters (?car - car ?carsrc - car ?cardest - car)
		:precondition (and
			(car-clear ?car)
			(car-clear ?cardest)
			(behind-car ?car ?carsrc)
			(at-curb ?cardest)
		)
		:effect (and
			(not (car-clear ?cardest))
			(car-clear ?carsrc)
			(behind-car ?car ?cardest)
			(not (behind-car ?car ?carsrc))

		)
	)

  (:action nop
         :parameters    ()
         :precondition  ()
         :effect        (and)
)

  ;------------------------------------------------------------------
  ;                               Methods
  ;------------------------------------------------------------------
  ;                 Move a car to another one
  ;------------------------------------------------------------------
  (:method do-move-car
          :parameters   (?car ?cardest - car)
          :expansion    (     (tag t1 (move-car-to-car ?car ?carsrc ?cardest))
                        )
          :constraints  (and  (before (and
                              (not (at-curb ?car))
                              (behind-car ?car ?carsrc)
                              )
                          t1)
                        )
  )

  (:method do-move-car
          :parameters   (?car ?cardest - car)
          :expansion    (     (tag t1 (move-curb-to-car ?car ?curbsrc ?cardest))
                        )
          :constraints  (and  (before (and
                              (at-curb ?car)
                              (at-curb-num ?car ?curbsrc)
                              )
                          t1)
                        )
  )

  ;------------------------------------------------------------------
  ;                 Move a car to a free curb
  ;------------------------------------------------------------------
  (:method do-move-curb
          :parameters   (?car - car ?curbdest - curb)
          :expansion    (     (tag t1 (move-car-to-curb ?car ?carsrc ?curbdest))
                        )
          :constraints  (and  (before (and
                              (not (at-curb ?car))
                              (behind-car ?car ?carsrc)
                              )
                          t1)
                        )
  )

  (:method do-move-curb
          :parameters   (?car - car ?curbdest - curb)
          :expansion    (     (tag t1 (move-curb-to-curb ?car ?curbsrc ?curbdest))
                        )
          :constraints  (and  (before (and
                              (at-curb ?car)
                              (at-curb-num ?car ?curbsrc)
                              )
                          t1)
                        )
  )

  ;------------------------------------------------------------------
  ;             Remove the car parked next to another
  ;------------------------------------------------------------------
  (:method free-car
          :parameters   (?car - car)
          :expansion    (     (tag t1 (move-car-to-car ?car2 ?car ?cardest))
                        )
          :constraints  (and  (before (and
                              (car-clear ?cardest)
                              (at-curb ?cardest)
                              (behind-car ?car2 ?car)
                              )
                          t1)
                        )
  )

  (:method free-car
          :parameters   (?car - car)
          :expansion    (     (tag t1 (move-car-to-curb ?car2 ?car ?curbdest))
                        )
          :constraints  (and  (before (and
                              (curb-clear ?curbdest)
                              (behind-car ?car2 ?car)
                              )
                          t1)
                        )
  )

  ;------------------------------------------------------------------
  ;             Remove the car parked at a curb
  ;------------------------------------------------------------------

  ; Two cars and free cars
  (:method free-curb
          :parameters   (?curb - curb)
          :expansion    (     (tag t1 (free-car ?carbot))
                              (tag t2 (do-move-car ?carbot ?car2))
                        )
          :constraints  (and  (before (and
                              (behind-car ?cartop ?carbot)
                              (at-curb-num ?carbot ?curb)
                              (car-clear ?car2)
                              (at-curb ?car2)
                              )
                          t1)
                        )
  )

  ; Two cars and free curb
  (:method free-curb
        :parameters   (?curb - curb)
          :expansion    (     (tag t1 (free-car ?carbot))
                              (tag t2 (do-move-car ?carbot ?cartop))
                        )
          :constraints  (and  (before (and
                              (behind-car ?cartop ?carbot)
                              (at-curb-num ?carbot ?curb)
                              )
                          t1)
                        )
  )


  ; One car and one free car
  (:method free-curb
          :parameters   (?curb - curb)
          :expansion    (     (tag t1 (move-curb-to-car ?car ?curb ?cardest))
                        )
          :constraints  (and  (before (and
                              (car-clear ?cardest)
                              (at-curb ?cardest)
                              (at-curb-num ?car ?curb)
                              (car-clear ?car)
                              )
                          t1)
                        )
  )

  ; One car and free curb
  (:method free-curb
          :parameters   (?curb - curb)
          :expansion    (     (tag t1 (move-curb-to-curb ?car ?curb ?curbdest))
                        )
          :constraints  (and  (before (and
                              (curb-clear ?curbdest)
                              (at-curb ?car)
                              (at-curb-num ?car ?curb)
                              (car-clear ?car)
                              )
                          t1)
                        )
  )

  ;------------------------------------------------------------------
  ;       Take a random car (except the one given) and fill a gap
  ;------------------------------------------------------------------
  ;Remove this if it doesn't work
  (:method flatten
          :parameters   (?forbiddencar - car)
          :expansion    (     (tag t1 (nop))
                        )
          :constraints  (and  (before (and
                              (not (curb-clear ?curbdest))
                              )
                          t1)
                        )
  )


  (:method flatten
          :parameters   (?forbiddencar - car)
          :expansion    (     (tag t1 (do-move-curb ?car ?curbdest))
                        )
          :constraints  (and  (before (and
                              (not (at-curb ?car))
                              (not (= ?car ?forbiddencar))
                              (curb-clear ?curbdest)
                              )
                          t1)
                        )
  )

  (:method flatten
          :parameters   ()
          :expansion    (     (tag t1 (nop))
                        )
          :constraints  (and  (before (and
                              (not (curb-clear ?curbdest))
                              )
                          t1)
                        )
  )

  (:method flatten
          :parameters   ()
          :expansion    (     (tag t1 (do-move-curb ?car ?curbdest))
                        )
          :constraints  (and  (before (and
                              (not (at-curb ?car))
                              (curb-clear ?curbdest)
                              )
                          t1)
                        )
  )


  ;------------------------------------------------------------------
  ;                 Flip two cars in the same spot
  ;                     Side effect : flatten
  ;------------------------------------------------------------------
  ; No free curb
  ; ?free-car and free-curb can't occupy the same spot, as by hypothesis, there is
  ; no free curb available
  (:method flip
          :parameters   (?curb - curb)
          :expansion    (     (tag t1 (free-car ?carbot))
                              (tag t2 (free-curb ?curb))
                              (tag t3 (do-move-curb ?cartop ?curb))
                              (tag t4 (do-move-car ?carbot ?cartop))
                        )
          :constraints  (and  (before (and
                              (behind-car ?cartop ?carbot)
                              (at-curb-num ?carbot ?curb)
                              )
                          t1)
                        )
  )

  ; Free curb available
  (:method flip
          :parameters   (?curb - curb)
          :expansion    (     (tag t1 (flatten))
                              (tag t2 (flip ?curb))
                        )
          :constraints  (and  (before (and
                              (curb-clear ?freecurb)
                              )
                          t1)
                        )
  )

  ;------------------------------------------------------------------
  ;                       Park a car on a curb
  ;------------------------------------------------------------------
  ;Make sure the car is free for the next step, on top of another
  (:method park-curb
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (park-curb-2 ?car ?curb))
                        )
          :constraints  (and  (before (and
                                (car-clear ?car)
                                (not (at-curb ?car))
                              )
                          t1)
                        )
  )

  ;Put a car below
  (:method park-curb
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (do-move-car ?padcar ?car))
                              (tag t2 (flip ?currcurb))
                              (tag t3 (park-curb-2 ?car ?curb))
                        )
          :constraints  (and  (before (and
                                (car-clear ?car)
                                (at-curb-num ?car ?currcurb)
                                (car-clear ?padcar)
                                (not (at-curb ?padcar))
                              )
                          t1)
                        )
  )

  (:method park-curb
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (flip ?currcurb))
                              (tag t2 (park-curb-2 ?car ?curb))
                        )
          :constraints  (and  (before (and
                                (not (car-clear ?car))
                                (at-curb-num ?car ?currcurb)
                                (not (= ?currcurb ?curb))
                              )
                          t1)
                        )
  )


  ; Sometimes, we only have to flip cars because it's already on spot
  (:method park-curb
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (flip ?curb))
                        )
          :constraints  (and  (before (and
                                (car-clear ?car)
                                (behind-car ?car ?car2)
                                (at-curb-num ?car2 ?curb)
                              )
                          t1)
                        )
  )

  ; If everything is fine already
  (:method park-curb
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (nop))
                        )
          :constraints  (and  (before (and
                                (at-curb ?car)
                                (at-curb-num ?car ?curb)
                              )
                          t1)
                        )
  )

  ; The spot is not free
  (:method park-curb-2
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (free-curb ?curb))
                              (tag t2 (park-curb-3 ?car ?curb))
                        )
          :constraints  (and  (before (and
                                (not (curb-clear ?curb))
                              )
                          t1)
                        )
  )

  ; The spot is already free
  (:method park-curb-2
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (park-curb-3 ?car ?curb))
                        )
          :constraints  (and  (before (and
                                (curb-clear ?curb)
                              )
                          t1)
                        )
  )


  ;Move the car to the newly freed curb
  (:method park-curb-3
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (do-move-curb ?car ?curb))
                        )
          :constraints  (and  (before (and
                                (not (at-curb ?car))
                                (car-clear ?car)
                                (curb-clear ?curb)
                              )
                          t1)
                        )
  )

  (:method park-curb-3
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (nop))
                        )
          :constraints  (and  (before (and
                                (at-curb ?car)
                                (at-curb-num ?car ?curb)
                              )
                          t1)
                        )
  )

  ;------------------------------------------------------------------
  ;                       Park a car on another car
  ;------------------------------------------------------------------
  ; Should only be used if every car that is supposed to be parked at a curb
  ; is already parked

  ; If there is nothing to do
  (:method park-car
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (nop))
                        )
          :constraints  (and  (before (and
                                (behind-car ?car ?destcar)
                              )
                          t1)
                        )
  )

  ; Make sure the car is free for the next step
  (:method park-car
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (park-car-2 ?car ?destcar))
                        )
          :constraints  (and  (before (and
                                (car-clear ?car)
                              )
                          t1)
                        )
  )

  (:method park-car
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (flip ?currcurb))
                              (tag t2 (park-car-2 ?car ?destcar))
                        )
          :constraints  (and  (before (and
                                (not (car-clear ?car))
                                (at-curb-num ?car ?currcurb)
                                ;(behind-car ?car2 ?car)
                              )
                          t1)
                        )
  )


  ; This method makes sure the spot is free
  ; The spot is not free
  (:method park-car-2
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (free-car ?destcar))
                              (tag t2 (park-car-3 ?car ?destcar))
                        )
          :constraints  (and  (before (and
                                (not (car-clear ?destcar))
                                ;(car-clear ?transcar)
                                ;(at-curb ?transcar)
                              )
                          t1)
                        )
  )

  ; A whole curb is free
  (:method park-car-2
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (flatten ?car))
                              (tag t2 (park-car-2 ?car ?destcar))
                        )
          :constraints  (and  (before (and
                                (not (car-clear ?destcar))
                                (curb-clear ?transcurb)
                              )
                          t1)
                        )
  )

  ; The spot is already free
  (:method park-car-2
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (park-car-3 ?car ?destcar))
                        )
          :constraints  (and  (before (and
                                (car-clear ?destcar)
                              )
                          t1)
                        )
  )

  ;Move the car to the newly freed car
  (:method park-car-3
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (do-move-car ?car ?destcar))
                        )
          :constraints  (and  (before (and
                                (not (at-curb ?car))
                                (car-clear ?car)
                                (car-clear ?destcar)
                              )
                          t1)
                        )
  )

  (:method park-car-3
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (nop))
                        )
          :constraints  (and  (before (and
                                (behind-car ?car ?destcar)
                              )
                          t1)
                        )
  )

)

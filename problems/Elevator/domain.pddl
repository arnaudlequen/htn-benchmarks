(define (domain miconic)
(:requirements :strips :typing :htn :negative-preconditions :equality)
  (:types passenger - object
          floor - object
         )

(:predicates
(origin ?person - passenger ?floor - floor)
;; entry of ?person is ?floor
;; inertia

(destin ?person - passenger ?floor - floor)
;; exit of ?person is ?floor
;; inertia

(above ?floor1 - floor  ?floor2 - floor)
;; ?floor2 is located above of ?floor1

(boarded ?person - passenger)
;; true if ?person has boarded the lift

(not-boarded ?person - passenger)
;; true if ?person has not boarded the lift

(served ?person - passenger)
;; true if ?person has alighted as her destination

(not-served ?person - passenger)
;; true if ?person is not at their destination

(lift-at ?floor - floor)
;; current position of the lift is at ?floor
)


;;stop and allow boarding

(:action board
  :parameters (?f - floor ?p - passenger)
  :precondition (and (lift-at ?f) (origin ?p ?f))
  :effect (boarded ?p)
)

(:action depart
  :parameters (?f - floor ?p - passenger)
  :precondition (and (lift-at ?f) (destin ?p ?f)
		     (boarded ?p))
  :effect (and (not (boarded ?p))
	       (served ?p))
)
;;drive up

(:action up
  :parameters (?f1 - floor ?f2 - floor)
  :precondition (and (lift-at ?f1) (above ?f1 ?f2))
  :effect (and (lift-at ?f2) (not (lift-at ?f1)))
)


;;drive down

(:action down
  :parameters (?f1 - floor ?f2 - floor)
  :precondition (and (lift-at ?f1) (above ?f2 ?f1))
  :effect (and (lift-at ?f2) (not (lift-at ?f1)))
)


;-------------------------------------------------------------------
;			               				Methods
;-------------------------------------------------------------------
;                       Go and fetch someone
;-------------------------------------------------------------------

;Note : Doesn't work if someone's origin and destination are the same
(:method pick_up
  :parameters (?person - passenger ?pass_floor - floor)
  :expansion 	(
                  (tag t1 (board ?pass_floor ?person))
              )
  :constraints
        (and (before (and
                (lift-at ?pass_floor)
            )
          t1)
        )
)

(:method pick_up
  :parameters (?person - passenger ?pass_floor - floor)
  :expansion 	(
                  (tag t1 (down ?current_floor ?pass_floor))
                  (tag t2 (board ?pass_floor ?person))
              )
  :constraints
        (and (before (and
                (lift-at ?current_floor)
                (above ?pass_floor ?current_floor)
                (origin ?person ?pass_floor)
                (not (= ?current_floor ?pass_floor))
            )
          t1)
        )
)


(:method pick_up
  :parameters (?person - passenger ?pass_floor - floor)
  :expansion 	(
                  (tag t1 (up ?current_floor ?pass_floor))
                  (tag t2 (board ?pass_floor ?person))
              )
  :constraints
        (and (before (and
                (lift-at ?current_floor)
                (above ?current_floor ?pass_floor)
                (origin ?person ?pass_floor)
                (not (= ?current_floor ?pass_floor))
            )
          t1)
          )
)

;-------------------------------------------------------------------
;                        Lead someone to their destination
;-------------------------------------------------------------------
(:method drop_off
  :parameters (?person - passenger ?pass_floor - floor)
  :expansion 	(
                  (tag t1 (depart ?pass_floor ?person))
              )
  :constraints
        (and (before (and
                (lift-at ?pass_floor)
                (boarded ?person)
                (destin ?person ?pass_floor)
            )
          t1)
        )
)

(:method drop_off
  :parameters (?person - passenger ?pass_floor - floor)
  :expansion 	(
                  (tag t1 (up ?current_floor ?pass_floor))
                  (tag t2 (depart ?pass_floor ?person))
              )
  :constraints
        (and (before (and
                (lift-at ?current_floor)
                (above ?current_floor ?pass_floor)
                (boarded ?person)
                (destin ?person ?pass_floor)
            )
          t1)
        )
)

(:method drop_off
  :parameters (?person - passenger ?pass_floor - floor)
  :expansion 	(
                  (tag t1 (down ?current_floor ?pass_floor))
                  (tag t2 (depart ?pass_floor ?person))
              )
  :constraints
        (and (before (and
                (lift-at ?current_floor)
                (above ?pass_floor ?current_floor)
                (boarded ?person)
                (destin ?person ?pass_floor)
            )
          t1)
        )
)

;-------------------------------------------------------------------
;                       Serve one person
;-------------------------------------------------------------------
(:method give_a_ride
  :parameters (?person - passenger)
  :expansion 	(
                  (tag t1 (pick_up ?person ?from))
                  (tag t2 (drop_off ?person ?to))
              )
  :constraints
        (and (before (and
                (origin ?person ?from)
                (destin ?person ?to)
                (not (served ?person))
            )
          t1)
        )
)


)

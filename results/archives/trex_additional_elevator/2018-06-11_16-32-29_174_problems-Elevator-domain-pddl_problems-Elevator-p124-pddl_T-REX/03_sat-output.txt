Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.018] Processed problem encoding.
[0.019] Calculated possible fact changes of composite elements.
[0.020] Initialized instantiation procedure.
[0.020] 
[0.020] *************************************
[0.020] * * *   M a k e s p a n     0   * * *
[0.020] *************************************
[0.022] Instantiated 2,873 initial clauses.
[0.022] The encoding contains a total of 1,503 distinct variables.
[0.022] Attempting solve with solver <glucose4> ...
c 26 assumptions
[0.022] Executed solver; result: UNSAT.
[0.022] 
[0.022] *************************************
[0.022] * * *   M a k e s p a n     1   * * *
[0.022] *************************************
[0.026] Computed next depth properties: array size of 51.
[0.029] Instantiated 2,627 transitional clauses.
[0.037] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.037] Instantiated 11,572 universal clauses.
[0.037] Instantiated and added clauses for a total of 17,072 clauses.
[0.037] The encoding contains a total of 5,332 distinct variables.
[0.037] Attempting solve with solver <glucose4> ...
c 51 assumptions
[0.038] Executed solver; result: UNSAT.
[0.038] 
[0.038] *************************************
[0.038] * * *   M a k e s p a n     2   * * *
[0.038] *************************************
[0.044] Computed next depth properties: array size of 101.
[0.052] Instantiated 7,602 transitional clauses.
[0.068] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.068] Instantiated 81,947 universal clauses.
[0.068] Instantiated and added clauses for a total of 106,621 clauses.
[0.068] The encoding contains a total of 10,611 distinct variables.
[0.068] Attempting solve with solver <glucose4> ...
c 101 assumptions
c last restart ## conflicts  :  0 107 
[0.068] Executed solver; result: SAT.
[0.068] Solver returned SAT; a solution has been found at makespan 2.
100
solution 1752 1
536 2 1007 1056 574 3 1117 1057 105 4 1139 1058 128 5 800 1059 146 6 1445 1060 168 7 1181 1061 665 8 563 1062 244 9 1196 1063 266 10 41 1064 718 11 259 1065 327 12 1219 1066 766 13 1266 1067 358 14 1166 1068 797 15 1552 1069 381 16 1362 1070 133 17 989 1071 400 18 1574 1072 756 19 441 1073 660 20 1382 1074 416 21 1325 1075 472 22 1664 1076 919 23 1726 1077 969 24 248 1078 1016 25 1752 1079 1050 26 1696 1080 
[0.069] Exiting.

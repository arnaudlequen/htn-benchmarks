Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.024] Processed problem encoding.
[0.026] Calculated possible fact changes of composite elements.
[0.027] Initialized instantiation procedure.
[0.027] 
[0.027] *************************************
[0.027] * * *   M a k e s p a n     0   * * *
[0.027] *************************************
[0.030] Instantiated 3,554 initial clauses.
[0.030] The encoding contains a total of 1,851 distinct variables.
[0.030] Attempting solve with solver <glucose4> ...
c 29 assumptions
[0.030] Executed solver; result: UNSAT.
[0.030] 
[0.030] *************************************
[0.030] * * *   M a k e s p a n     1   * * *
[0.030] *************************************
[0.035] Computed next depth properties: array size of 57.
[0.040] Instantiated 3,278 transitional clauses.
[0.051] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.051] Instantiated 14,473 universal clauses.
[0.051] Instantiated and added clauses for a total of 21,305 clauses.
[0.051] The encoding contains a total of 6,643 distinct variables.
[0.051] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.051] Executed solver; result: UNSAT.
[0.051] 
[0.051] *************************************
[0.051] * * *   M a k e s p a n     2   * * *
[0.051] *************************************
[0.060] Computed next depth properties: array size of 113.
[0.070] Instantiated 9,522 transitional clauses.
[0.092] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.092] Instantiated 112,109 universal clauses.
[0.092] Instantiated and added clauses for a total of 142,936 clauses.
[0.092] The encoding contains a total of 13,227 distinct variables.
[0.092] Attempting solve with solver <glucose4> ...
c 113 assumptions
c last restart ## conflicts  :  0 119 
[0.092] Executed solver; result: SAT.
[0.092] Solver returned SAT; a solution has been found at makespan 2.
111
solution 2195 1
653 2 1393 1350 106 3 1433 1351 133 4 1444 1352 190 5 813 1353 158 6 1096 1354 257 7 1426 1355 709 8 1861 1356 735 9 1895 1357 765 10 1925 1358 325 11 1956 1359 343 12 1970 1360 850 13 2032 1361 397 14 2044 1362 953 15 315 1363 1008 16 893 1364 442 17 1661 1365 454 18 2052 1366 1121 19 2104 1367 137 20 1968 1368 1140 21 263 1369 1193 22 1722 1370 555 23 1751 1371 1232 24 2195 1372 575 25 809 1373 1184 26 1924 1374 472 27 972 1375 632 28 1349 1376 29 96 1377 
[0.093] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 173 initial clauses.
[0.001] The encoding contains a total of 103 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 6 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 11.
[0.001] Instantiated 127 transitional clauses.
[0.001] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.001] Instantiated 512 universal clauses.
[0.001] Instantiated and added clauses for a total of 812 clauses.
[0.001] The encoding contains a total of 272 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 11 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     2   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 21.
[0.001] Instantiated 322 transitional clauses.
[0.001] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.001] Instantiated 1,387 universal clauses.
[0.001] Instantiated and added clauses for a total of 2,521 clauses.
[0.001] The encoding contains a total of 531 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 21 assumptions
c last restart ## conflicts  :  0 27 
[0.001] Executed solver; result: SAT.
[0.001] Solver returned SAT; a solution has been found at makespan 2.
20
solution 83 1
20 2 64 43 26 3 70 44 25 4 51 45 18 5 8 46 39 6 83 47 
[0.002] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.004] Processed problem encoding.
[0.004] Calculated possible fact changes of composite elements.
[0.004] Initialized instantiation procedure.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     0   * * *
[0.004] *************************************
[0.004] Instantiated 548 initial clauses.
[0.004] The encoding contains a total of 303 distinct variables.
[0.004] Attempting solve with solver <glucose4> ...
c 11 assumptions
[0.004] Executed solver; result: UNSAT.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     1   * * *
[0.004] *************************************
[0.005] Computed next depth properties: array size of 21.
[0.005] Instantiated 452 transitional clauses.
[0.007] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.007] Instantiated 1,927 universal clauses.
[0.007] Instantiated and added clauses for a total of 2,927 clauses.
[0.007] The encoding contains a total of 937 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 21 assumptions
[0.007] Executed solver; result: UNSAT.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     2   * * *
[0.007] *************************************
[0.008] Computed next depth properties: array size of 41.
[0.009] Instantiated 1,242 transitional clauses.
[0.011] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.011] Instantiated 7,277 universal clauses.
[0.011] Instantiated and added clauses for a total of 11,446 clauses.
[0.011] The encoding contains a total of 1,851 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 41 assumptions
c last restart ## conflicts  :  0 47 
[0.011] Executed solver; result: SAT.
[0.011] Solver returned SAT; a solution has been found at makespan 2.
39
solution 276 1
105 2 206 183 26 3 130 184 4 95 185 33 5 170 186 156 6 219 187 45 7 231 188 62 8 122 189 71 9 236 190 172 10 276 191 176 11 86 192 
[0.012] Exiting.

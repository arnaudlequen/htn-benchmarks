Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.011] Processed problem encoding.
[0.012] Calculated possible fact changes of composite elements.
[0.012] Initialized instantiation procedure.
[0.012] 
[0.012] *************************************
[0.012] * * *   M a k e s p a n     0   * * *
[0.012] *************************************
[0.013] Instantiated 3,554 initial clauses.
[0.013] The encoding contains a total of 1,851 distinct variables.
[0.013] Attempting solve with solver <glucose4> ...
c 29 assumptions
[0.013] Executed solver; result: UNSAT.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     1   * * *
[0.013] *************************************
[0.016] Computed next depth properties: array size of 57.
[0.018] Instantiated 3,278 transitional clauses.
[0.024] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.024] Instantiated 14,473 universal clauses.
[0.024] Instantiated and added clauses for a total of 21,305 clauses.
[0.024] The encoding contains a total of 6,643 distinct variables.
[0.024] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.024] Executed solver; result: UNSAT.
[0.024] 
[0.024] *************************************
[0.024] * * *   M a k e s p a n     2   * * *
[0.024] *************************************
[0.029] Computed next depth properties: array size of 113.
[0.035] Instantiated 9,522 transitional clauses.
[0.048] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.048] Instantiated 112,109 universal clauses.
[0.048] Instantiated and added clauses for a total of 142,936 clauses.
[0.048] The encoding contains a total of 13,227 distinct variables.
[0.048] Attempting solve with solver <glucose4> ...
c 113 assumptions
c last restart ## conflicts  :  0 119 
[0.048] Executed solver; result: SAT.
[0.048] Solver returned SAT; a solution has been found at makespan 2.
110
solution 2145 1
571 2 1690 1240 630 3 165 1241 652 4 85 1242 5 1306 1243 130 6 1711 1244 144 7 573 1245 789 8 444 1246 216 9 1339 1247 258 10 1352 1248 294 11 1820 1249 864 12 317 1250 13 1839 1251 880 14 1860 1252 980 15 569 1253 1005 16 1886 1254 1033 17 1893 1255 1071 18 201 1256 1089 19 1918 1257 64 20 1934 1258 1159 21 932 1259 429 22 1961 1260 445 23 1984 1261 756 24 99 1262 927 25 2059 1263 1221 26 1616 1264 248 27 1023 1265 1117 28 2145 1266 540 29 1290 1267 
[0.049] Exiting.

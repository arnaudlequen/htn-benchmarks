Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.020] Processed problem encoding.
[0.022] Calculated possible fact changes of composite elements.
[0.023] Initialized instantiation procedure.
[0.023] 
[0.023] *************************************
[0.023] * * *   M a k e s p a n     0   * * *
[0.023] *************************************
[0.025] Instantiated 3,092 initial clauses.
[0.025] The encoding contains a total of 1,615 distinct variables.
[0.025] Attempting solve with solver <glucose4> ...
c 27 assumptions
[0.025] Executed solver; result: UNSAT.
[0.025] 
[0.025] *************************************
[0.025] * * *   M a k e s p a n     1   * * *
[0.025] *************************************
[0.029] Computed next depth properties: array size of 53.
[0.033] Instantiated 2,836 transitional clauses.
[0.042] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.042] Instantiated 12,503 universal clauses.
[0.042] Instantiated and added clauses for a total of 18,431 clauses.
[0.042] The encoding contains a total of 5,753 distinct variables.
[0.042] Attempting solve with solver <glucose4> ...
c 53 assumptions
[0.042] Executed solver; result: UNSAT.
[0.042] 
[0.042] *************************************
[0.042] * * *   M a k e s p a n     2   * * *
[0.042] *************************************
[0.049] Computed next depth properties: array size of 105.
[0.058] Instantiated 8,218 transitional clauses.
[0.076] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.076] Instantiated 91,309 universal clauses.
[0.076] Instantiated and added clauses for a total of 117,958 clauses.
[0.076] The encoding contains a total of 11,451 distinct variables.
[0.076] Attempting solve with solver <glucose4> ...
c 105 assumptions
c last restart ## conflicts  :  0 111 
[0.076] Executed solver; result: SAT.
[0.076] Solver returned SAT; a solution has been found at makespan 2.
104
solution 1652 1
591 2 1426 1150 652 3 1144 1151 95 4 1199 1152 695 5 1484 1153 68 6 1242 1154 147 7 878 1155 711 8 1133 1156 777 9 416 1157 799 10 1553 1158 843 11 1568 1159 225 12 1315 1160 257 13 1059 1161 908 14 573 1162 132 15 878 1163 924 16 1130 1164 371 17 677 1165 403 18 1190 1166 433 19 1112 1167 1015 20 1631 1168 473 21 1395 1169 102 22 631 1170 488 23 1440 1171 525 24 971 1172 1094 25 1652 1173 1107 26 157 1174 54 27 1361 1175 
[0.077] Exiting.

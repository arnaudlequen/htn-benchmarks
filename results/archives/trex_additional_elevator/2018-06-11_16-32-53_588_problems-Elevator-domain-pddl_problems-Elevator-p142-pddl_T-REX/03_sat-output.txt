Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.013] Processed problem encoding.
[0.014] Calculated possible fact changes of composite elements.
[0.015] Initialized instantiation procedure.
[0.015] 
[0.015] *************************************
[0.015] * * *   M a k e s p a n     0   * * *
[0.015] *************************************
[0.017] Instantiated 3,797 initial clauses.
[0.017] The encoding contains a total of 1,975 distinct variables.
[0.017] Attempting solve with solver <glucose4> ...
c 30 assumptions
[0.017] Executed solver; result: UNSAT.
[0.017] 
[0.017] *************************************
[0.017] * * *   M a k e s p a n     1   * * *
[0.017] *************************************
[0.020] Computed next depth properties: array size of 59.
[0.023] Instantiated 3,511 transitional clauses.
[0.028] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.028] Instantiated 15,512 universal clauses.
[0.028] Instantiated and added clauses for a total of 22,820 clauses.
[0.028] The encoding contains a total of 7,112 distinct variables.
[0.028] Attempting solve with solver <glucose4> ...
c 59 assumptions
[0.028] Executed solver; result: UNSAT.
[0.028] 
[0.028] *************************************
[0.028] * * *   M a k e s p a n     2   * * *
[0.028] *************************************
[0.033] Computed next depth properties: array size of 117.
[0.039] Instantiated 10,210 transitional clauses.
[0.052] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.052] Instantiated 123,595 universal clauses.
[0.052] Instantiated and added clauses for a total of 156,625 clauses.
[0.052] The encoding contains a total of 14,163 distinct variables.
[0.052] Attempting solve with solver <glucose4> ...
c 117 assumptions
c last restart ## conflicts  :  0 123 
[0.052] Executed solver; result: SAT.
[0.052] Solver returned SAT; a solution has been found at makespan 2.
116
solution 1946 1
722 2 937 1399 43 3 1261 1400 105 4 770 1401 124 5 313 1402 157 6 1429 1403 854 7 530 1404 927 8 43 1405 232 9 639 1406 58 10 1437 1407 984 11 181 1408 252 12 891 1409 333 13 1846 1410 1043 14 1900 1411 164 15 1490 1412 350 16 1563 1413 392 17 314 1414 1179 18 602 1415 420 19 1503 1416 489 20 1321 1417 1250 21 189 1418 1271 22 1927 1419 400 23 222 1420 310 24 1648 1421 541 25 221 1422 553 26 956 1423 591 27 469 1424 1392 28 1279 1425 707 29 1946 1426 1209 30 1690 1427 
[0.053] Exiting.

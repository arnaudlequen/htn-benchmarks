Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.018] Processed problem encoding.
[0.019] Calculated possible fact changes of composite elements.
[0.020] Initialized instantiation procedure.
[0.020] 
[0.020] *************************************
[0.020] * * *   M a k e s p a n     0   * * *
[0.020] *************************************
[0.022] Instantiated 2,662 initial clauses.
[0.022] The encoding contains a total of 1,395 distinct variables.
[0.022] Attempting solve with solver <glucose4> ...
c 25 assumptions
[0.022] Executed solver; result: UNSAT.
[0.022] 
[0.022] *************************************
[0.022] * * *   M a k e s p a n     1   * * *
[0.022] *************************************
[0.026] Computed next depth properties: array size of 49.
[0.029] Instantiated 2,426 transitional clauses.
[0.036] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.036] Instantiated 10,677 universal clauses.
[0.036] Instantiated and added clauses for a total of 15,765 clauses.
[0.036] The encoding contains a total of 4,927 distinct variables.
[0.036] Attempting solve with solver <glucose4> ...
c 49 assumptions
[0.037] Executed solver; result: UNSAT.
[0.037] 
[0.037] *************************************
[0.037] * * *   M a k e s p a n     2   * * *
[0.037] *************************************
[0.043] Computed next depth properties: array size of 97.
[0.051] Instantiated 7,010 transitional clauses.
[0.067] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.067] Instantiated 73,245 universal clauses.
[0.067] Instantiated and added clauses for a total of 96,020 clauses.
[0.067] The encoding contains a total of 9,803 distinct variables.
[0.067] Attempting solve with solver <glucose4> ...
c 97 assumptions
c last restart ## conflicts  :  0 103 
[0.067] Executed solver; result: SAT.
[0.067] Solver returned SAT; a solution has been found at makespan 2.
96
solution 1450 1
509 2 1099 1060 78 3 467 1061 544 4 343 1062 105 5 1305 1063 611 6 449 1064 125 7 1010 1065 164 8 991 1066 208 9 811 1067 630 10 506 1068 236 11 315 1069 245 12 720 1070 280 13 1135 1071 747 14 1364 1072 788 15 1287 1073 329 16 1196 1074 834 17 1114 1075 376 18 407 1076 855 19 1410 1077 917 20 139 1078 422 21 1016 1079 448 22 125 1080 481 23 1238 1081 504 24 1450 1082 1021 25 214 1083 
[0.068] Exiting.

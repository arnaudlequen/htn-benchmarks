Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.019] Processed problem encoding.
[0.020] Calculated possible fact changes of composite elements.
[0.021] Initialized instantiation procedure.
[0.021] 
[0.021] *************************************
[0.021] * * *   M a k e s p a n     0   * * *
[0.021] *************************************
[0.023] Instantiated 2,873 initial clauses.
[0.023] The encoding contains a total of 1,503 distinct variables.
[0.023] Attempting solve with solver <glucose4> ...
c 26 assumptions
[0.023] Executed solver; result: UNSAT.
[0.023] 
[0.023] *************************************
[0.023] * * *   M a k e s p a n     1   * * *
[0.023] *************************************
[0.027] Computed next depth properties: array size of 51.
[0.031] Instantiated 2,627 transitional clauses.
[0.039] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.039] Instantiated 11,572 universal clauses.
[0.039] Instantiated and added clauses for a total of 17,072 clauses.
[0.039] The encoding contains a total of 5,332 distinct variables.
[0.039] Attempting solve with solver <glucose4> ...
c 51 assumptions
[0.039] Executed solver; result: UNSAT.
[0.039] 
[0.039] *************************************
[0.039] * * *   M a k e s p a n     2   * * *
[0.039] *************************************
[0.046] Computed next depth properties: array size of 101.
[0.055] Instantiated 7,602 transitional clauses.
[0.073] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.073] Instantiated 81,947 universal clauses.
[0.073] Instantiated and added clauses for a total of 106,621 clauses.
[0.073] The encoding contains a total of 10,611 distinct variables.
[0.073] Attempting solve with solver <glucose4> ...
c 101 assumptions
c last restart ## conflicts  :  0 107 
[0.073] Executed solver; result: SAT.
[0.073] Solver returned SAT; a solution has been found at makespan 2.
100
solution 1540 1
408 2 544 909 51 3 385 910 483 4 1221 911 546 5 1005 912 589 6 357 913 92 7 453 914 635 8 1265 915 657 9 1033 916 634 10 1288 917 169 11 533 918 217 12 1057 919 231 13 760 920 740 14 1330 921 101 15 1106 922 236 16 951 923 285 17 1400 924 663 18 1434 925 780 19 232 926 485 20 1498 927 689 21 980 928 292 22 1353 929 331 23 301 930 831 24 1167 931 883 25 1540 932 382 26 1209 933 
[0.074] Exiting.

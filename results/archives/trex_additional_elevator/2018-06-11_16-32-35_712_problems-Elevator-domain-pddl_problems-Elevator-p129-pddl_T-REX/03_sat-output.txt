Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.009] Processed problem encoding.
[0.009] Calculated possible fact changes of composite elements.
[0.010] Initialized instantiation procedure.
[0.010] 
[0.010] *************************************
[0.010] * * *   M a k e s p a n     0   * * *
[0.010] *************************************
[0.011] Instantiated 3,092 initial clauses.
[0.011] The encoding contains a total of 1,615 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 27 assumptions
[0.011] Executed solver; result: UNSAT.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     1   * * *
[0.011] *************************************
[0.013] Computed next depth properties: array size of 53.
[0.015] Instantiated 2,836 transitional clauses.
[0.019] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.019] Instantiated 12,503 universal clauses.
[0.019] Instantiated and added clauses for a total of 18,431 clauses.
[0.019] The encoding contains a total of 5,753 distinct variables.
[0.019] Attempting solve with solver <glucose4> ...
c 53 assumptions
[0.020] Executed solver; result: UNSAT.
[0.020] 
[0.020] *************************************
[0.020] * * *   M a k e s p a n     2   * * *
[0.020] *************************************
[0.024] Computed next depth properties: array size of 105.
[0.029] Instantiated 8,218 transitional clauses.
[0.040] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.040] Instantiated 91,309 universal clauses.
[0.040] Instantiated and added clauses for a total of 117,958 clauses.
[0.040] The encoding contains a total of 11,451 distinct variables.
[0.040] Attempting solve with solver <glucose4> ...
c 105 assumptions
c last restart ## conflicts  :  0 111 
[0.040] Executed solver; result: SAT.
[0.040] Solver returned SAT; a solution has been found at makespan 2.
104
solution 1877 1
453 2 411 1099 517 3 1512 1100 539 4 1175 1101 97 5 608 1102 568 6 1575 1103 588 7 1588 1104 621 8 1642 1105 645 9 119 1106 704 10 1224 1107 184 11 1667 1108 188 12 1695 1109 808 13 270 1110 822 14 354 1111 833 15 995 1112 264 16 1341 1113 287 17 1380 1114 315 18 1031 1115 363 19 617 1116 873 20 1755 1117 766 21 1784 1118 391 22 1798 1119 946 23 120 1120 1000 24 372 1121 594 25 559 1122 436 26 1850 1123 1081 27 1877 1124 
[0.040] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.007] Processed problem encoding.
[0.008] Calculated possible fact changes of composite elements.
[0.008] Initialized instantiation procedure.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     0   * * *
[0.008] *************************************
[0.009] Instantiated 1,123 initial clauses.
[0.009] The encoding contains a total of 603 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 16 assumptions
[0.009] Executed solver; result: UNSAT.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     1   * * *
[0.009] *************************************
[0.010] Computed next depth properties: array size of 31.
[0.011] Instantiated 977 transitional clauses.
[0.014] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.014] Instantiated 4,242 universal clauses.
[0.014] Instantiated and added clauses for a total of 6,342 clauses.
[0.014] The encoding contains a total of 2,002 distinct variables.
[0.014] Attempting solve with solver <glucose4> ...
c 31 assumptions
[0.014] Executed solver; result: UNSAT.
[0.014] 
[0.014] *************************************
[0.014] * * *   M a k e s p a n     2   * * *
[0.014] *************************************
[0.016] Computed next depth properties: array size of 61.
[0.019] Instantiated 2,762 transitional clauses.
[0.025] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.025] Instantiated 20,667 universal clauses.
[0.025] Instantiated and added clauses for a total of 29,771 clauses.
[0.025] The encoding contains a total of 3,971 distinct variables.
[0.025] Attempting solve with solver <glucose4> ...
c 61 assumptions
c last restart ## conflicts  :  0 67 
[0.025] Executed solver; result: SAT.
[0.025] Solver returned SAT; a solution has been found at makespan 2.
60
solution 605 1
193 2 494 394 206 3 413 395 66 4 287 396 87 5 533 397 247 6 552 398 222 7 356 399 124 8 445 400 255 9 290 401 283 10 599 402 299 11 505 403 259 12 186 404 316 13 161 405 164 14 605 406 182 15 292 407 391 16 30 408 
[0.025] Exiting.

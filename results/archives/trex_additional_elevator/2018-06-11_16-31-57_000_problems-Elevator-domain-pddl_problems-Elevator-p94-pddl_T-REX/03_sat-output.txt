Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.012] Processed problem encoding.
[0.013] Calculated possible fact changes of composite elements.
[0.013] Initialized instantiation procedure.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     0   * * *
[0.013] *************************************
[0.014] Instantiated 1,727 initial clauses.
[0.014] The encoding contains a total of 915 distinct variables.
[0.014] Attempting solve with solver <glucose4> ...
c 20 assumptions
[0.014] Executed solver; result: UNSAT.
[0.014] 
[0.014] *************************************
[0.014] * * *   M a k e s p a n     1   * * *
[0.014] *************************************
[0.017] Computed next depth properties: array size of 39.
[0.019] Instantiated 1,541 transitional clauses.
[0.024] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.024] Instantiated 6,742 universal clauses.
[0.024] Instantiated and added clauses for a total of 10,010 clauses.
[0.024] The encoding contains a total of 3,142 distinct variables.
[0.024] Attempting solve with solver <glucose4> ...
c 39 assumptions
[0.024] Executed solver; result: UNSAT.
[0.024] 
[0.024] *************************************
[0.024] * * *   M a k e s p a n     2   * * *
[0.024] *************************************
[0.028] Computed next depth properties: array size of 77.
[0.033] Instantiated 4,410 transitional clauses.
[0.043] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.043] Instantiated 38,795 universal clauses.
[0.043] Instantiated and added clauses for a total of 53,215 clauses.
[0.043] The encoding contains a total of 6,243 distinct variables.
[0.043] Attempting solve with solver <glucose4> ...
c 77 assumptions
c last restart ## conflicts  :  0 83 
[0.043] Executed solver; result: SAT.
[0.043] Solver returned SAT; a solution has been found at makespan 2.
76
solution 988 1
342 2 115 576 372 3 814 577 49 4 560 578 410 5 838 579 104 6 623 580 430 7 912 581 457 8 223 582 465 9 921 583 508 10 116 584 516 11 606 585 184 12 52 586 532 13 659 587 253 14 608 588 267 15 692 589 366 16 971 590 355 17 988 591 292 18 356 592 224 19 741 593 338 20 772 594 
[0.043] Exiting.

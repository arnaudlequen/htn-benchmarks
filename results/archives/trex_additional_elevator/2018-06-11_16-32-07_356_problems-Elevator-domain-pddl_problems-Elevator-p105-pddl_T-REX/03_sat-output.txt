Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.014] Processed problem encoding.
[0.015] Calculated possible fact changes of composite elements.
[0.016] Initialized instantiation procedure.
[0.016] 
[0.016] *************************************
[0.016] * * *   M a k e s p a n     0   * * *
[0.016] *************************************
[0.017] Instantiated 2,077 initial clauses.
[0.017] The encoding contains a total of 1,095 distinct variables.
[0.017] Attempting solve with solver <glucose4> ...
c 22 assumptions
[0.017] Executed solver; result: UNSAT.
[0.017] 
[0.017] *************************************
[0.017] * * *   M a k e s p a n     1   * * *
[0.017] *************************************
[0.020] Computed next depth properties: array size of 43.
[0.023] Instantiated 1,871 transitional clauses.
[0.029] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.029] Instantiated 8,208 universal clauses.
[0.029] Instantiated and added clauses for a total of 12,156 clauses.
[0.029] The encoding contains a total of 3,808 distinct variables.
[0.029] Attempting solve with solver <glucose4> ...
c 43 assumptions
[0.029] Executed solver; result: UNSAT.
[0.029] 
[0.029] *************************************
[0.029] * * *   M a k e s p a n     2   * * *
[0.029] *************************************
[0.034] Computed next depth properties: array size of 85.
[0.040] Instantiated 5,378 transitional clauses.
[0.052] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.052] Instantiated 50,859 universal clauses.
[0.052] Instantiated and added clauses for a total of 68,393 clauses.
[0.052] The encoding contains a total of 7,571 distinct variables.
[0.052] Attempting solve with solver <glucose4> ...
c 85 assumptions
c last restart ## conflicts  :  0 91 
[0.052] Executed solver; result: SAT.
[0.052] Solver returned SAT; a solution has been found at makespan 2.
84
solution 1163 1
422 2 318 720 45 3 531 721 97 4 741 722 501 5 306 723 118 6 663 724 168 7 783 725 161 8 576 726 186 9 806 727 528 10 1111 728 233 11 829 729 96 12 865 730 276 13 906 731 307 14 756 732 319 15 1147 733 623 16 1163 734 121 17 966 735 635 18 998 736 144 19 1012 737 689 20 928 738 401 21 424 739 413 22 162 740 
[0.053] Exiting.

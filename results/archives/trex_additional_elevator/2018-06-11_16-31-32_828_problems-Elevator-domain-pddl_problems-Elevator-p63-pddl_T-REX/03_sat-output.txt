Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.006] Processed problem encoding.
[0.006] Calculated possible fact changes of composite elements.
[0.007] Initialized instantiation procedure.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     0   * * *
[0.007] *************************************
[0.007] Instantiated 869 initial clauses.
[0.007] The encoding contains a total of 471 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.007] Executed solver; result: UNSAT.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     1   * * *
[0.007] *************************************
[0.008] Computed next depth properties: array size of 27.
[0.009] Instantiated 743 transitional clauses.
[0.011] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.011] Instantiated 3,208 universal clauses.
[0.011] Instantiated and added clauses for a total of 4,820 clauses.
[0.011] The encoding contains a total of 1,528 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 27 assumptions
[0.011] Executed solver; result: UNSAT.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     2   * * *
[0.011] *************************************
[0.013] Computed next depth properties: array size of 53.
[0.015] Instantiated 2,082 transitional clauses.
[0.020] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.020] Instantiated 14,219 universal clauses.
[0.020] Instantiated and added clauses for a total of 21,121 clauses.
[0.020] The encoding contains a total of 3,027 distinct variables.
[0.020] Attempting solve with solver <glucose4> ...
c 53 assumptions
c last restart ## conflicts  :  0 59 
[0.020] Executed solver; result: SAT.
[0.020] Solver returned SAT; a solution has been found at makespan 2.
51
solution 516 1
113 2 391 265 134 3 390 266 152 4 293 267 43 5 304 268 34 6 421 269 137 7 444 270 120 8 65 271 9 455 272 185 10 496 273 207 11 343 274 233 12 516 275 240 13 374 276 110 14 509 277 
[0.020] Exiting.

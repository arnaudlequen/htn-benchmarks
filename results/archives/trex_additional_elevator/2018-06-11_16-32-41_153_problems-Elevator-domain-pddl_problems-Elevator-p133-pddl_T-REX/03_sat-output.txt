Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.021] Processed problem encoding.
[0.023] Calculated possible fact changes of composite elements.
[0.024] Initialized instantiation procedure.
[0.024] 
[0.024] *************************************
[0.024] * * *   M a k e s p a n     0   * * *
[0.024] *************************************
[0.026] Instantiated 3,319 initial clauses.
[0.026] The encoding contains a total of 1,731 distinct variables.
[0.026] Attempting solve with solver <glucose4> ...
c 28 assumptions
[0.026] Executed solver; result: UNSAT.
[0.026] 
[0.026] *************************************
[0.026] * * *   M a k e s p a n     1   * * *
[0.026] *************************************
[0.031] Computed next depth properties: array size of 55.
[0.035] Instantiated 3,053 transitional clauses.
[0.045] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.045] Instantiated 13,470 universal clauses.
[0.045] Instantiated and added clauses for a total of 19,842 clauses.
[0.045] The encoding contains a total of 6,190 distinct variables.
[0.045] Attempting solve with solver <glucose4> ...
c 55 assumptions
[0.045] Executed solver; result: UNSAT.
[0.045] 
[0.045] *************************************
[0.045] * * *   M a k e s p a n     2   * * *
[0.045] *************************************
[0.053] Computed next depth properties: array size of 109.
[0.063] Instantiated 8,858 transitional clauses.
[0.083] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.083] Instantiated 101,355 universal clauses.
[0.083] Instantiated and added clauses for a total of 130,055 clauses.
[0.083] The encoding contains a total of 12,323 distinct variables.
[0.083] Attempting solve with solver <glucose4> ...
c 109 assumptions
c last restart ## conflicts  :  0 115 
[0.083] Executed solver; result: SAT.
[0.083] Solver returned SAT; a solution has been found at makespan 2.
108
solution 1738 1
741 2 1579 1301 46 3 209 1302 842 4 363 1303 75 5 1240 1304 123 6 469 1305 875 7 561 1306 939 8 1588 1307 201 9 1420 1308 239 10 1643 1309 993 11 1465 1310 320 12 1260 1311 340 13 1490 1312 379 14 169 1313 417 15 1438 1314 118 16 1023 1315 1097 17 1713 1316 1107 18 1738 1317 1153 19 1535 1318 467 20 1540 1319 1172 21 401 1320 1189 22 653 1321 584 23 741 1322 640 24 1612 1323 660 25 476 1324 1255 26 1708 1325 864 27 1556 1326 1293 28 663 1327 
[0.084] Exiting.

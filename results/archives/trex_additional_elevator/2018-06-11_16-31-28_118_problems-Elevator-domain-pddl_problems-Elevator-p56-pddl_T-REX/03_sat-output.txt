Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.005] Processed problem encoding.
[0.005] Calculated possible fact changes of composite elements.
[0.006] Initialized instantiation procedure.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     0   * * *
[0.006] *************************************
[0.006] Instantiated 754 initial clauses.
[0.006] The encoding contains a total of 411 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.007] Computed next depth properties: array size of 25.
[0.008] Instantiated 638 transitional clauses.
[0.009] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.009] Instantiated 2,745 universal clauses.
[0.009] Instantiated and added clauses for a total of 4,137 clauses.
[0.009] The encoding contains a total of 1,315 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 25 assumptions
[0.009] Executed solver; result: UNSAT.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     2   * * *
[0.009] *************************************
[0.011] Computed next depth properties: array size of 49.
[0.013] Instantiated 1,778 transitional clauses.
[0.016] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.016] Instantiated 11,565 universal clauses.
[0.016] Instantiated and added clauses for a total of 17,480 clauses.
[0.016] The encoding contains a total of 2,603 distinct variables.
[0.016] Attempting solve with solver <glucose4> ...
c 49 assumptions
c last restart ## conflicts  :  0 55 
[0.016] Executed solver; result: SAT.
[0.016] Solver returned SAT; a solution has been found at makespan 2.
48
solution 379 1
134 2 240 221 42 3 264 222 45 4 23 223 174 5 200 224 32 6 283 225 73 7 257 226 79 8 257 227 91 9 244 228 105 10 299 229 120 11 309 230 88 12 379 231 214 13 344 232 
[0.017] Exiting.

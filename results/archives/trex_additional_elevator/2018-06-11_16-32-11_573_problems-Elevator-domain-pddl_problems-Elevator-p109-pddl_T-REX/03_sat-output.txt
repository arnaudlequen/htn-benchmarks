Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.014] Processed problem encoding.
[0.015] Calculated possible fact changes of composite elements.
[0.016] Initialized instantiation procedure.
[0.016] 
[0.016] *************************************
[0.016] * * *   M a k e s p a n     0   * * *
[0.016] *************************************
[0.018] Instantiated 2,264 initial clauses.
[0.018] The encoding contains a total of 1,191 distinct variables.
[0.018] Attempting solve with solver <glucose4> ...
c 23 assumptions
[0.018] Executed solver; result: UNSAT.
[0.018] 
[0.018] *************************************
[0.018] * * *   M a k e s p a n     1   * * *
[0.018] *************************************
[0.021] Computed next depth properties: array size of 45.
[0.023] Instantiated 2,048 transitional clauses.
[0.029] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.029] Instantiated 8,995 universal clauses.
[0.029] Instantiated and added clauses for a total of 13,307 clauses.
[0.029] The encoding contains a total of 4,165 distinct variables.
[0.029] Attempting solve with solver <glucose4> ...
c 45 assumptions
[0.029] Executed solver; result: UNSAT.
[0.029] 
[0.029] *************************************
[0.029] * * *   M a k e s p a n     2   * * *
[0.029] *************************************
[0.034] Computed next depth properties: array size of 89.
[0.040] Instantiated 5,898 transitional clauses.
[0.053] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.053] Instantiated 57,725 universal clauses.
[0.053] Instantiated and added clauses for a total of 76,930 clauses.
[0.053] The encoding contains a total of 8,283 distinct variables.
[0.053] Attempting solve with solver <glucose4> ...
c 89 assumptions
c last restart ## conflicts  :  0 95 
[0.053] Executed solver; result: SAT.
[0.053] Solver returned SAT; a solution has been found at makespan 2.
88
solution 1409 1
431 2 790 755 66 3 802 756 110 4 231 757 481 5 1175 758 502 6 866 759 173 7 1205 760 178 8 1235 761 574 9 1257 762 219 10 513 763 268 11 1292 764 608 12 1324 765 637 13 1275 766 651 14 1337 767 698 15 1347 768 740 16 1352 769 146 17 687 770 355 18 1030 771 214 19 331 772 743 20 1076 773 290 21 1387 774 526 22 1409 775 407 23 1078 776 
[0.054] Exiting.

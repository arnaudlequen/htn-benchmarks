Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.012] Processed problem encoding.
[0.013] Calculated possible fact changes of composite elements.
[0.013] Initialized instantiation procedure.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     0   * * *
[0.013] *************************************
[0.015] Instantiated 1,898 initial clauses.
[0.015] The encoding contains a total of 1,003 distinct variables.
[0.015] Attempting solve with solver <glucose4> ...
c 21 assumptions
[0.015] Executed solver; result: UNSAT.
[0.015] 
[0.015] *************************************
[0.015] * * *   M a k e s p a n     1   * * *
[0.015] *************************************
[0.017] Computed next depth properties: array size of 41.
[0.019] Instantiated 1,702 transitional clauses.
[0.024] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.024] Instantiated 7,457 universal clauses.
[0.024] Instantiated and added clauses for a total of 11,057 clauses.
[0.024] The encoding contains a total of 3,467 distinct variables.
[0.024] Attempting solve with solver <glucose4> ...
c 41 assumptions
[0.024] Executed solver; result: UNSAT.
[0.024] 
[0.024] *************************************
[0.024] * * *   M a k e s p a n     2   * * *
[0.024] *************************************
[0.028] Computed next depth properties: array size of 81.
[0.033] Instantiated 4,882 transitional clauses.
[0.044] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.044] Instantiated 44,557 universal clauses.
[0.044] Instantiated and added clauses for a total of 60,496 clauses.
[0.044] The encoding contains a total of 6,891 distinct variables.
[0.044] Attempting solve with solver <glucose4> ...
c 81 assumptions
c last restart ## conflicts  :  0 87 
[0.044] Executed solver; result: SAT.
[0.044] Solver returned SAT; a solution has been found at makespan 2.
80
solution 982 1
353 2 681 646 74 3 498 647 390 4 480 648 110 5 900 649 424 6 711 650 452 7 928 651 89 8 940 652 477 9 982 653 509 10 807 654 180 11 125 655 548 12 805 656 212 13 71 657 394 14 302 658 585 15 827 659 207 16 446 660 241 17 426 661 601 18 134 662 633 19 232 663 282 20 859 664 325 21 703 665 
[0.044] Exiting.

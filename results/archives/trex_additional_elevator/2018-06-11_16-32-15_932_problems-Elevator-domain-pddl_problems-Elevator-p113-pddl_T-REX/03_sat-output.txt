Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.015] Processed problem encoding.
[0.016] Calculated possible fact changes of composite elements.
[0.017] Initialized instantiation procedure.
[0.017] 
[0.017] *************************************
[0.017] * * *   M a k e s p a n     0   * * *
[0.017] *************************************
[0.019] Instantiated 2,459 initial clauses.
[0.019] The encoding contains a total of 1,291 distinct variables.
[0.019] Attempting solve with solver <glucose4> ...
c 24 assumptions
[0.019] Executed solver; result: UNSAT.
[0.019] 
[0.019] *************************************
[0.019] * * *   M a k e s p a n     1   * * *
[0.019] *************************************
[0.022] Computed next depth properties: array size of 47.
[0.025] Instantiated 2,233 transitional clauses.
[0.031] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.031] Instantiated 9,818 universal clauses.
[0.031] Instantiated and added clauses for a total of 14,510 clauses.
[0.031] The encoding contains a total of 4,538 distinct variables.
[0.031] Attempting solve with solver <glucose4> ...
c 47 assumptions
[0.031] Executed solver; result: UNSAT.
[0.031] 
[0.031] *************************************
[0.031] * * *   M a k e s p a n     2   * * *
[0.031] *************************************
[0.036] Computed next depth properties: array size of 93.
[0.043] Instantiated 6,442 transitional clauses.
[0.056] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.056] Instantiated 65,179 universal clauses.
[0.056] Instantiated and added clauses for a total of 86,131 clauses.
[0.056] The encoding contains a total of 9,027 distinct variables.
[0.056] Attempting solve with solver <glucose4> ...
c 93 assumptions
c last restart ## conflicts  :  0 99 
[0.056] Executed solver; result: SAT.
[0.056] Solver returned SAT; a solution has been found at makespan 2.
92
solution 1228 1
469 2 617 790 62 3 1080 791 49 4 856 792 534 5 1124 793 558 6 1165 794 578 7 900 795 143 8 838 796 642 9 362 797 165 10 584 798 205 11 502 799 711 12 1228 800 728 13 687 801 286 14 531 802 634 15 713 803 204 16 719 804 297 17 934 805 363 18 978 806 704 19 155 807 381 20 743 808 414 21 849 809 787 22 1024 810 508 23 53 811 658 24 1057 812 
[0.057] Exiting.

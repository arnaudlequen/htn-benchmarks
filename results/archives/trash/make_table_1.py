import numpy as np
import matplotlib.pyplot as plt

# Adjust layout to make room for the table:
plt.subplots_adjust(left=0.2, bottom=0.2)

data = [[  66386,  174296,   75131,  577908,   32015,   69638,  577908,   32015,   69638],
        [  58230,  381139,   78045,   99308,  160454,   69638,  577908,   32015,   69638],
        [  89135,   80552,  152558,  497981,  603535,   69638,  577908,   32015,   69638],
        [  78415,   81858,  150656,  193263,   69638,   69638,  577908,   32015,   69638],
        [  78415,   81858,  150656,  193263,   69638,   69638,  577908,   32015,   69638],
        [  78415,   81858,  150656,  193263,   69638,   69638,  577908,   32015,   69638],
        [  78415,   81858,  150656,  193263,   69638,   69638,  577908,   32015,   69638],
        [  78415,   81858,  150656,  193263,   69638,   69638,  577908,   32015,   69638],
        [  78415,   81858,  150656,  193263,   69638,   69638,  577908,   32015,   69638],
        [  78415,   81858,  150656,  193263,   69638,   69638,  577908,   32015,   69638],
        [  78415,   81858,  150656,  193263,   69638,   69638,  577908,   32015,   69638],
        [  78415,   81858,  150656,  193263,   69638,   69638,  577908,   32015,   69638],
        [  78415,   81858,  150656,  193263,   69638,   69638,  577908,   32015,   69638],
        [  78415,   81858,  150656,  193263,   69638,   69638,  577908,   32015,   69638],
        [  78415,   81858,  150656,  193263,   69638,   69638,  577908,   32015,   69638],
        [  78415,   81858,  150656,  193263,   69638,   69638,  577908,   32015,   69638],
        [  78415,   81858,  150656,  193263,   69638,   69638,  577908,   32015,   69638],
        [  78415,   81858,  150656,  193263,   69638,   69638,  577908,   32015,   69638],
        [  78415,   81858,  150656,  193263,   69638,   69638,  577908,   32015,   69638],
        [ 139361,  331509,  343164,  781380,   52269,   69638,  577908,   32015,   69638]]

columns = ('Freeze', 'Wind', 'Flood', 'Quake', 'Hail', 'Potato', '', '', '')
rows = [' p' + str(i) for i in range(1, 21)]
plt.axis('off')

# Get some pastel shades for the colors
n_rows = len(data)

index = np.arange(len(columns)) + 0.3

# Initialize the vertical-offset for the stacked bar chart.
y_offset = np.array([0.0] * len(columns))

# Plot bars and create text labels for the table
cell_text = []
for row in range(n_rows):
    y_offset = y_offset + data[row]
    cell_text.append(['%1.1f' % (x/1000.0) for x in y_offset])

# Reverse colors and text labels to display the last value at the top.
cell_text.reverse()

# Add headers and a table at the bottom of the axes
header_0 = plt.table(cellText=[['']*3],
                     colLabels=['Fabulous problem', 'Potato problem', 'NPHARD problem'],
                     loc='top',
                     bbox=[0, 0.8, 1.0, 0.2]
                     )

header_1 = plt.table(cellText=[['']*9],
                     colLabels=[['Madagascar', 'T-REX', 'iSHOP'][x % 3] for x in range(9)],
                     loc='top',
                     bbox=[0, 0.7, 1.0, 0.2]
                     )

the_table = plt.table(cellText=cell_text,
                      rowLabels=rows,
                      loc='top',
                      bbox=[0, -0.2, 1.0, 1.0]
                      )

plt.title('myTitle')

plt.show()

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.010] Processed problem encoding.
[0.012] Calculated possible fact changes of composite elements.
[0.012] Initialized instantiation procedure.
[0.012] 
[0.012] *************************************
[0.012] * * *   M a k e s p a n     0   * * *
[0.012] *************************************
[0.012] Instantiated 258 initial clauses.
[0.012] The encoding contains a total of 176 distinct variables.
[0.012] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.012] Executed solver; result: UNSAT.
[0.012] 
[0.012] *************************************
[0.012] * * *   M a k e s p a n     1   * * *
[0.012] *************************************
[0.012] Computed next depth properties: array size of 4.
[0.013] Instantiated 803 transitional clauses.
[0.013] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.013] Instantiated 2,764 universal clauses.
[0.013] Instantiated and added clauses for a total of 3,825 clauses.
[0.013] The encoding contains a total of 911 distinct variables.
[0.013] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.013] Executed solver; result: UNSAT.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     2   * * *
[0.013] *************************************
[0.014] Computed next depth properties: array size of 7.
[0.016] Instantiated 4,482 transitional clauses.
[0.020] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.020] Instantiated 164,147 universal clauses.
[0.020] Instantiated and added clauses for a total of 172,454 clauses.
[0.020] The encoding contains a total of 2,518 distinct variables.
[0.020] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.020] Executed solver; result: UNSAT.
[0.020] 
[0.020] *************************************
[0.020] * * *   M a k e s p a n     3   * * *
[0.020] *************************************
[0.020] Computed next depth properties: array size of 13.
[0.024] Instantiated 6,995 transitional clauses.
[0.037] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.037] Instantiated 312,323 universal clauses.
[0.037] Instantiated and added clauses for a total of 491,772 clauses.
[0.037] The encoding contains a total of 4,736 distinct variables.
[0.037] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.037] Executed solver; result: UNSAT.
[0.037] 
[0.037] *************************************
[0.037] * * *   M a k e s p a n     4   * * *
[0.037] *************************************
[0.038] Computed next depth properties: array size of 21.
[0.041] Instantiated 6,923 transitional clauses.
[0.058] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.058] Instantiated 371,468 universal clauses.
[0.058] Instantiated and added clauses for a total of 870,163 clauses.
[0.058] The encoding contains a total of 7,650 distinct variables.
[0.058] Attempting solve with solver <glucose4> ...
c 21 assumptions
[0.060] Executed solver; result: UNSAT.
[0.060] 
[0.060] *************************************
[0.060] * * *   M a k e s p a n     5   * * *
[0.060] *************************************
[0.061] Computed next depth properties: array size of 31.
[0.064] Instantiated 8,389 transitional clauses.
[0.085] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.085] Instantiated 431,840 universal clauses.
[0.085] Instantiated and added clauses for a total of 1,310,392 clauses.
[0.085] The encoding contains a total of 11,342 distinct variables.
[0.085] Attempting solve with solver <glucose4> ...
c 31 assumptions
[0.086] Executed solver; result: UNSAT.
[0.086] 
[0.086] *************************************
[0.086] * * *   M a k e s p a n     6   * * *
[0.086] *************************************
[0.087] Computed next depth properties: array size of 43.
[0.091] Instantiated 9,975 transitional clauses.
[0.117] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.117] Instantiated 493,439 universal clauses.
[0.117] Instantiated and added clauses for a total of 1,813,806 clauses.
[0.117] The encoding contains a total of 15,874 distinct variables.
[0.117] Attempting solve with solver <glucose4> ...
c 43 assumptions
[0.118] Executed solver; result: UNSAT.
[0.118] 
[0.118] *************************************
[0.118] * * *   M a k e s p a n     7   * * *
[0.118] *************************************
[0.120] Computed next depth properties: array size of 57.
[0.125] Instantiated 11,681 transitional clauses.
[0.154] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.154] Instantiated 556,265 universal clauses.
[0.154] Instantiated and added clauses for a total of 2,381,752 clauses.
[0.154] The encoding contains a total of 21,308 distinct variables.
[0.154] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.171] Executed solver; result: UNSAT.
[0.171] 
[0.171] *************************************
[0.171] * * *   M a k e s p a n     8   * * *
[0.171] *************************************
[0.174] Computed next depth properties: array size of 73.
[0.180] Instantiated 13,507 transitional clauses.
[0.213] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.213] Instantiated 620,318 universal clauses.
[0.213] Instantiated and added clauses for a total of 3,015,577 clauses.
[0.213] The encoding contains a total of 27,706 distinct variables.
[0.213] Attempting solve with solver <glucose4> ...
c 73 assumptions
[1.749] Executed solver; result: UNSAT.
[1.749] 
[1.749] *************************************
[1.749] * * *   M a k e s p a n     9   * * *
[1.749] *************************************
[1.752] Computed next depth properties: array size of 91.
[1.758] Instantiated 15,453 transitional clauses.
[1.796] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.796] Instantiated 685,598 universal clauses.
[1.796] Instantiated and added clauses for a total of 3,716,628 clauses.
[1.796] The encoding contains a total of 35,130 distinct variables.
[1.796] Attempting solve with solver <glucose4> ...
c 91 assumptions
c |      147         0       68 |   28445  2360849  4932220 |     2     6202      212     3792 | 19.027 % |
c |      177        13      112 |   28430  2360849  4932220 |     3    11207      356     8786 | 19.070 % |
c |      236        13      127 |   28398  2359553  4929605 |     4    13781      412    16182 | 19.161 % |
c |      319        13      125 |   28398  2359553  4929605 |     5    13905      444    26058 | 19.161 % |
c |      438        13      114 |   28398  2359553  4929605 |     5    23905      460    26058 | 19.161 % |
c |      588        18      102 |   28398  2359553  4929605 |     6    21465      474    38498 | 19.161 % |
[12.096] Executed solver; result: UNSAT.
[12.096] 
[12.096] *************************************
[12.096] * * *   M a k e s p a n    10   * * *
[12.096] *************************************
[12.099] Computed next depth properties: array size of 111.
[12.108] Instantiated 17,519 transitional clauses.
[12.150] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[12.150] Instantiated 752,105 universal clauses.
[12.150] Instantiated and added clauses for a total of 4,486,252 clauses.
[12.150] The encoding contains a total of 43,642 distinct variables.
[12.150] Attempting solve with solver <glucose4> ...
c 111 assumptions
c |      631        75      110 |   35980  2955206  6158883 |     7    16439      514    53524 | 17.555 % |
c |      648        75      123 |   35952  2954436  6157331 |     7    26438      602    53524 | 17.619 % |
c |      668        75      134 |   35952  2954436  6157331 |     8    18846      628    71116 | 17.619 % |
c |      690        75      144 |   35942  2954204  6156861 |     8    28845      660    71116 | 17.642 % |
c |      734        75      149 |   35942  2954204  6156861 |     8    38845      684    71116 | 17.642 % |
c |      771        75      155 |   35942  2954204  6156861 |     9    28645      702    91316 | 17.642 % |
c |      828        75      157 |   35942  2954204  6156861 |     9    38645      720    91316 | 17.642 % |
c |      930        75      150 |   35942  2954204  6156861 |    10    25872      762   114089 | 17.642 % |
c |     1051        75      142 |   35942  2954204  6156861 |    10    35872      778   114089 | 17.642 % |
c |     1180        75      135 |   35942  2954204  6156861 |    10    45872      788   114089 | 17.642 % |
c |     1286        76      132 |   35942  2954204  6156861 |    11    30471      798   139490 | 17.642 % |
c |     1398        76      128 |   35942  2954204  6156861 |    11    40471      816   139490 | 17.642 % |
c |     1561        76      121 |   35942  2954204  6156861 |    11    50471      830   139490 | 17.642 % |
c |     1674        76      119 |   35942  2954204  6156861 |    12    32477      842   167484 | 17.642 % |
c |     1782        76      117 |   35942  2954204  6156861 |    12    42477      858   167484 | 17.642 % |
c |     1942        76      113 |   35942  2954204  6156861 |    12    52477      870   167484 | 17.642 % |
c |     2084        76      110 |   35942  2954204  6156861 |    13    31897      872   198064 | 17.642 % |
c |     2120        76      113 |   35942  2954204  6156861 |    13    41897      894   198064 | 17.642 % |
c |     2211        76      113 |   35936  2954080  6156609 |    13    51896      898   198064 | 17.655 % |
c |     2350        76      110 |   35936  2954080  6156609 |    13    61896      910   198064 | 17.655 % |
c |     2455        76      109 |   35921  2953702  6155845 |    14    38686      918   231273 | 17.690 % |
c |     2569        76      108 |   35921  2953702  6155845 |    14    48686      922   231273 | 17.690 % |
c |     2716        77      106 |   35921  2953702  6155845 |    14    58686      924   231273 | 17.690 % |
c |     2885        77      103 |   35921  2953702  6155845 |    14    68686      924   231273 | 17.690 % |
c |     2977        77      104 |   35921  2953702  6155845 |    15    42871      940   267088 | 17.690 % |
c |     3123        77      102 |   35900  2953143  6154717 |    15    52870      944   267088 | 17.738 % |
c |     3303        77       99 |   35895  2953068  6154563 |    15    62868      946   267088 | 17.749 % |
c |     3437        77       98 |   35895  2953068  6154563 |    15    72868      954   267088 | 17.749 % |
c |     3535        77       99 |   35895  2953068  6154563 |    16    44465      960   305491 | 17.749 % |
c |     3597        78      100 |   35895  2953068  6154563 |    16    54465      972   305491 | 17.749 % |
c |     3729        78       99 |   35895  2953068  6154563 |    16    64465      974   305491 | 17.749 % |
c |     3914        78       97 |   35895  2953068  6154563 |    16    74465      974   305491 | 17.749 % |
c |     4054        79       96 |   35895  2953068  6154563 |    17    43470      976   346486 | 17.749 % |
c |     4088        85       97 |   35883  2952908  6154235 |    17    53468      994   346486 | 17.777 % |
c |     4173        85       98 |   35883  2952908  6154235 |    17    63468     1000   346486 | 17.777 % |
c |     4326        85       97 |   35883  2952908  6154235 |    17    73468     1000   346486 | 17.777 % |
c |     4507        85       95 |   35883  2952908  6154235 |    17    83468     1000   346486 | 17.777 % |
c |     4594        85       95 |   35862  2952243  6152891 |    18    49868     1004   390085 | 17.825 % |
c |     4687        89       96 |   35862  2952243  6152891 |    18    59868     1006   390085 | 17.825 % |
c |     4846        89       94 |   35862  2952243  6152891 |    18    69868     1006   390085 | 17.825 % |
c |     5017        91       93 |   35862  2952243  6152891 |    18    79868     1006   390085 | 17.825 % |
c |     5205        94       92 |   35862  2952243  6152891 |    18    89868     1006   390085 | 17.825 % |
c |     5270       104       92 |   35862  2952243  6152891 |    19    53658     1008   436295 | 17.825 % |
c |     5294       108       94 |   35862  2952243  6152891 |    19    63658     1030   436295 | 17.825 % |
c |     5357       109       95 |   35862  2952243  6152891 |    19    73658     1032   436295 | 17.825 % |
c |     5490       114       94 |   35862  2952243  6152891 |    19    83658     1036   436295 | 17.825 % |
c |     5680       114       93 |   35862  2952243  6152891 |    19    93658     1036   436295 | 17.825 % |
c |     5767       128       93 |   35862  2952243  6152891 |    20    54857     1038   485096 | 17.825 % |
c |     5814       133       94 |   35862  2952243  6152891 |    20    64857     1038   485096 | 17.825 % |
c |     5940       142       94 |   35862  2952243  6152891 |    20    74857     1038   485096 | 17.825 % |
c |     6111       144       93 |   35862  2952243  6152891 |    20    84857     1040   485096 | 17.825 % |
c |     6306       144       91 |   35862  2952243  6152891 |    20    94857     1040   485096 | 17.825 % |
[174.541] Executed solver; result: UNSAT.
[174.541] 
[174.541] *************************************
[174.541] * * *   M a k e s p a n    11   * * *
[174.541] *************************************
[174.546] Computed next depth properties: array size of 133.
[174.555] Instantiated 19,705 transitional clauses.
[174.603] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[174.603] Instantiated 819,839 universal clauses.
[174.603] Instantiated and added clauses for a total of 5,325,796 clauses.
[174.603] The encoding contains a total of 53,304 distinct variables.
[174.603] Attempting solve with solver <glucose4> ...
c 133 assumptions
c |     6461       164       91 |   44508  3615383  7520283 |    21    53459     1040   536494 | 16.500 % |
c |     6465       187       92 |   44487  3615383  7520283 |    21    63458     1114   536494 | 16.539 % |
c |     6472       187       94 |   44487  3615383  7520283 |    21    73458     1154   536494 | 16.539 % |
c |     6482       187       95 |   44451  3613805  7517103 |    21    83457     1184   536494 | 16.607 % |
c |     6497       187       96 |   44445  3613805  7517103 |    21    93456     1210   536494 | 16.618 % |
c |     6507       187       98 |   44445  3613805  7517103 |    21   103456     1222   536494 | 16.618 % |
c |     6533       187       99 |   44445  3613805  7517103 |    22    59461     1250   590489 | 16.618 % |
c |     6548       187      100 |   44445  3613805  7517103 |    22    69461     1268   590489 | 16.618 % |
c |     6572       187      101 |   44445  3613805  7517103 |    22    79461     1288   590489 | 16.618 % |
c |     6588       187      103 |   44445  3613805  7517103 |    22    89461     1300   590489 | 16.618 % |
c |     6634       187      104 |   44439  3613566  7516617 |    22    99460     1320   590489 | 16.629 % |
c |     6688       187      104 |   44439  3613566  7516617 |    22   109460     1328   590489 | 16.629 % |
c |     6717       187      105 |   44439  3613566  7516617 |    23    62875     1344   647074 | 16.629 % |
c |     6739       197      106 |   44439  3613566  7516617 |    23    72875     1354   647074 | 16.629 % |
c |     6800       197      107 |   44439  3613566  7516617 |    23    82875     1372   647074 | 16.629 % |
c |     6883       197      107 |   44439  3613566  7516617 |    23    92875     1382   647074 | 16.629 % |
c |     6994       197      107 |   44439  3613566  7516617 |    23   102875     1400   647074 | 16.629 % |
c |     7095       198      107 |   44439  3613566  7516617 |    23   112875     1406   647074 | 16.629 % |
c |     7191       198      107 |   44439  3613566  7516617 |    24    63701     1410   706248 | 16.629 % |
c |     7221       198      108 |   44439  3613566  7516617 |    24    73701     1422   706248 | 16.629 % |
c |     7261       198      108 |   44439  3613566  7516617 |    24    83701     1428   706248 | 16.629 % |
c |     7339       198      109 |   44439  3613566  7516617 |    24    93701     1444   706248 | 16.629 % |
c |     7450       198      108 |   44439  3613566  7516617 |    24   103701     1450   706248 | 16.629 % |
c |     7546       198      108 |   44439  3613566  7516617 |    24   113701     1452   706248 | 16.629 % |
c |     7679       198      108 |   44439  3613566  7516617 |    24   123701     1454   706248 | 16.629 % |
c |     7699       198      109 |   44439  3613566  7516617 |    25    71920     1458   768029 | 16.629 % |
c |     7738       198      109 |   44439  3613566  7516617 |    25    81920     1462   768029 | 16.629 % |
c |     7793       198      110 |   44424  3613208  7515893 |    25    91919     1472   768029 | 16.658 % |
c |     7873       198      110 |   44424  3613208  7515893 |    25   101919     1472   768029 | 16.658 % |
c |     7984       198      110 |   44424  3613208  7515893 |    25   111919     1484   768029 | 16.658 % |
c |     8124       198      109 |   44424  3613208  7515893 |    25   121919     1488   768029 | 16.658 % |
c |     8221       198      109 |   44424  3613208  7515893 |    26    67526     1494   832422 | 16.658 % |
c |     8239       198      110 |   44424  3613208  7515893 |    26    77526     1498   832422 | 16.658 % |
c |     8247       198      111 |   44424  3613208  7515893 |    26    87526     1500   832422 | 16.658 % |
c |     8277       198      112 |   44421  3613162  7515799 |    26    97525     1502   832422 | 16.663 % |
c |     8327       198      112 |   44421  3613162  7515799 |    26   107525     1502   832422 | 16.663 % |
c |     8382       198      113 |   44421  3613162  7515799 |    26   117525     1504   832422 | 16.663 % |
c |     8486       198      113 |   44421  3613162  7515799 |    26   127525     1510   832422 | 16.663 % |
c |     8573       198      113 |   44421  3613162  7515799 |    27    70525     1514   899422 | 16.663 % |
c |     8588       198      114 |   44412  3612910  7515287 |    27    80521     1518   899422 | 16.680 % |
Interrupt signal (15) received.

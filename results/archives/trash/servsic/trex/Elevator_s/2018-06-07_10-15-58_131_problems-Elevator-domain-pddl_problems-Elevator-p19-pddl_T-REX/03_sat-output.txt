Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.027] Processed problem encoding.
[0.032] Calculated possible fact changes of composite elements.
[0.033] Initialized instantiation procedure.
[0.033] 
[0.033] *************************************
[0.033] * * *   M a k e s p a n     0   * * *
[0.033] *************************************
[0.033] Instantiated 288 initial clauses.
[0.033] The encoding contains a total of 196 distinct variables.
[0.033] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.033] Executed solver; result: UNSAT.
[0.033] 
[0.033] *************************************
[0.033] * * *   M a k e s p a n     1   * * *
[0.033] *************************************
[0.033] Computed next depth properties: array size of 4.
[0.034] Instantiated 973 transitional clauses.
[0.035] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.035] Instantiated 3,382 universal clauses.
[0.035] Instantiated and added clauses for a total of 4,643 clauses.
[0.035] The encoding contains a total of 1,087 distinct variables.
[0.035] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.036] Executed solver; result: UNSAT.
[0.036] 
[0.036] *************************************
[0.036] * * *   M a k e s p a n     2   * * *
[0.036] *************************************
[0.037] Computed next depth properties: array size of 7.
[0.041] Instantiated 5,348 transitional clauses.
[0.050] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.050] Instantiated 161,386 universal clauses.
[0.050] Instantiated and added clauses for a total of 171,377 clauses.
[0.050] The encoding contains a total of 2,836 distinct variables.
[0.050] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.050] Executed solver; result: UNSAT.
[0.050] 
[0.050] *************************************
[0.050] * * *   M a k e s p a n     3   * * *
[0.050] *************************************
[0.051] Computed next depth properties: array size of 13.
[0.058] Instantiated 7,690 transitional clauses.
[0.083] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.083] Instantiated 321,710 universal clauses.
[0.083] Instantiated and added clauses for a total of 500,777 clauses.
[0.083] The encoding contains a total of 5,071 distinct variables.
[0.083] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.083] Executed solver; result: UNSAT.
[0.083] 
[0.083] *************************************
[0.083] * * *   M a k e s p a n     4   * * *
[0.083] *************************************
[0.084] Computed next depth properties: array size of 21.
[0.089] Instantiated 6,949 transitional clauses.
[0.117] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.117] Instantiated 365,496 universal clauses.
[0.117] Instantiated and added clauses for a total of 873,222 clauses.
[0.117] The encoding contains a total of 7,930 distinct variables.
[0.117] Attempting solve with solver <glucose4> ...
c 21 assumptions
[0.117] Executed solver; result: UNSAT.
[0.117] 
[0.117] *************************************
[0.117] * * *   M a k e s p a n     5   * * *
[0.117] *************************************
[0.119] Computed next depth properties: array size of 31.
[0.123] Instantiated 8,260 transitional clauses.
[0.150] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.150] Instantiated 410,354 universal clauses.
[0.150] Instantiated and added clauses for a total of 1,291,836 clauses.
[0.150] The encoding contains a total of 11,494 distinct variables.
[0.150] Attempting solve with solver <glucose4> ...
c 31 assumptions
[0.152] Executed solver; result: UNSAT.
[0.152] 
[0.152] *************************************
[0.152] * * *   M a k e s p a n     6   * * *
[0.152] *************************************
[0.154] Computed next depth properties: array size of 43.
[0.158] Instantiated 9,681 transitional clauses.
[0.183] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.183] Instantiated 456,284 universal clauses.
[0.183] Instantiated and added clauses for a total of 1,757,801 clauses.
[0.183] The encoding contains a total of 15,820 distinct variables.
[0.183] Attempting solve with solver <glucose4> ...
c 43 assumptions
[0.185] Executed solver; result: UNSAT.
[0.185] 
[0.185] *************************************
[0.185] * * *   M a k e s p a n     7   * * *
[0.185] *************************************
[0.187] Computed next depth properties: array size of 57.
[0.191] Instantiated 11,212 transitional clauses.
[0.218] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.218] Instantiated 503,286 universal clauses.
[0.218] Instantiated and added clauses for a total of 2,272,299 clauses.
[0.218] The encoding contains a total of 20,965 distinct variables.
[0.218] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.228] Executed solver; result: UNSAT.
[0.228] 
[0.228] *************************************
[0.228] * * *   M a k e s p a n     8   * * *
[0.228] *************************************
[0.231] Computed next depth properties: array size of 73.
[0.236] Instantiated 12,853 transitional clauses.
[0.267] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.267] Instantiated 551,360 universal clauses.
[0.267] Instantiated and added clauses for a total of 2,836,512 clauses.
[0.267] The encoding contains a total of 26,986 distinct variables.
[0.267] Attempting solve with solver <glucose4> ...
c 73 assumptions
[0.312] Executed solver; result: UNSAT.
[0.312] 
[0.312] *************************************
[0.312] * * *   M a k e s p a n     9   * * *
[0.312] *************************************
[0.315] Computed next depth properties: array size of 91.
[0.321] Instantiated 14,604 transitional clauses.
[0.355] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.355] Instantiated 600,506 universal clauses.
[0.355] Instantiated and added clauses for a total of 3,451,622 clauses.
[0.355] The encoding contains a total of 33,940 distinct variables.
[0.355] Attempting solve with solver <glucose4> ...
c 91 assumptions
[0.608] Executed solver; result: UNSAT.
[0.608] 
[0.608] *************************************
[0.608] * * *   M a k e s p a n    10   * * *
[0.608] *************************************
[0.612] Computed next depth properties: array size of 111.
[0.620] Instantiated 16,465 transitional clauses.
[0.657] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.657] Instantiated 650,724 universal clauses.
[0.657] Instantiated and added clauses for a total of 4,118,811 clauses.
[0.657] The encoding contains a total of 41,884 distinct variables.
[0.657] Attempting solve with solver <glucose4> ...
c 111 assumptions
[1.251] Executed solver; result: UNSAT.
[1.251] 
[1.251] *************************************
[1.251] * * *   M a k e s p a n    11   * * *
[1.251] *************************************
[1.256] Computed next depth properties: array size of 133.
[1.263] Instantiated 18,436 transitional clauses.
[1.305] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.305] Instantiated 702,014 universal clauses.
[1.305] Instantiated and added clauses for a total of 4,839,261 clauses.
[1.305] The encoding contains a total of 50,875 distinct variables.
[1.305] Attempting solve with solver <glucose4> ...
c 133 assumptions
c |       96         0      104 |   42071  3198518  6713022 |     2     6200      152     3798 | 17.304 % |
[2.301] Executed solver; result: UNSAT.
[2.301] 
[2.301] *************************************
[2.301] * * *   M a k e s p a n    12   * * *
[2.301] *************************************
[2.306] Computed next depth properties: array size of 157.
[2.315] Instantiated 20,517 transitional clauses.
[2.361] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.361] Instantiated 754,376 universal clauses.
[2.361] Instantiated and added clauses for a total of 5,614,154 clauses.
[2.361] The encoding contains a total of 60,970 distinct variables.
[2.361] Attempting solve with solver <glucose4> ...
c 157 assumptions
c |      183        49      109 |   51038  3803621  7969090 |     3    11186      214     8796 | 16.289 % |
c |      250        52      120 |   51028  3801507  7964814 |     4    13795      256    16186 | 16.305 % |
c |      322        59      124 |   51028  3801507  7964814 |     5    13907      270    26074 | 16.305 % |
c |      477        59      104 |   51028  3801507  7964814 |     5    23907      276    26074 | 16.305 % |
c |      642        63       93 |   51028  3801507  7964814 |     6    21456      292    38525 | 16.305 % |
c |      814        80       85 |   51028  3801507  7964814 |     7    16431      302    53550 | 16.305 % |
c |      961        83       83 |   51028  3801507  7964814 |     7    26431      304    53550 | 16.305 % |
[11.047] Executed solver; result: UNSAT.
[11.047] 
[11.047] *************************************
[11.047] * * *   M a k e s p a n    13   * * *
[11.047] *************************************
[11.053] Computed next depth properties: array size of 183.
[11.064] Instantiated 22,708 transitional clauses.
[11.113] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[11.113] Instantiated 807,810 universal clauses.
[11.113] Instantiated and added clauses for a total of 6,444,672 clauses.
[11.113] The encoding contains a total of 72,226 distinct variables.
[11.113] Attempting solve with solver <glucose4> ...
c 183 assumptions
c |      974       145       92 |   61126  4459557  9330164 |     8    18826      392    71146 | 15.367 % |
c |      986       146      101 |   61036  4457959  9326948 |     8    28821      490    71146 | 15.492 % |
c |     1021       149      107 |   61026  4457959  9326948 |     8    38820      548    71146 | 15.506 % |
c |     1031       150      116 |   61026  4457959  9326948 |     9    28674      578    91292 | 15.506 % |
c |     1057       183      122 |   60971  4455425  9321837 |     9    38671      620    91292 | 15.582 % |
c |     1098       200      127 |   60971  4455425  9321837 |    10    25876      676   114087 | 15.582 % |
c |     1128       201      132 |   60965  4455313  9321609 |    10    35875      712   114087 | 15.590 % |
c |     1183       201      135 |   60910  4453950  9318865 |    10    45874      762   114087 | 15.666 % |
c |     1213       234      140 |   60910  4453950  9318865 |    11    30476      782   139485 | 15.666 % |
c |     1243       234      144 |   60910  4453950  9318865 |    11    40476      794   139485 | 15.666 % |
c |     1302       243      145 |   60910  4453950  9318865 |    11    50476      818   139485 | 15.666 % |
c |     1353       270      147 |   60890  4453748  9318455 |    12    32574      834   167385 | 15.694 % |
c |     1382       273      151 |   60878  4453446  9317841 |    12    42572      860   167385 | 15.711 % |
c |     1431       283      153 |   60868  4453154  9317248 |    12    52571      908   167385 | 15.724 % |
c |     1488       311      154 |   60864  4453053  9317044 |    13    31923      928   198031 | 15.730 % |
c |     1518       340      158 |   60854  4452867  9316663 |    13    41922      968   198031 | 15.744 % |
c |     1564       346      159 |   60854  4452867  9316663 |    13    51922      994   198031 | 15.744 % |
c |     1640       346      158 |   60844  4452689  9316301 |    13    61921     1006   198031 | 15.758 % |
c |     1663       360      162 |   60844  4452689  9316301 |    14    38698     1018   231254 | 15.758 % |
c |     1685       370      166 |   60844  4452689  9316301 |    14    48698     1032   231254 | 15.758 % |
c |     1704       404      170 |   60844  4452689  9316301 |    14    58698     1046   231254 | 15.758 % |
c |     1731       425      173 |   60844  4452689  9316301 |    14    68698     1068   231254 | 15.758 % |
c |     1759       465      176 |   60844  4452689  9316301 |    15    42914     1092   267038 | 15.758 % |
c |     1795       500      178 |   60844  4452689  9316301 |    15    52914     1102   267038 | 15.758 % |
c |     1834       517      179 |   60844  4452689  9316301 |    15    62914     1118   267038 | 15.758 % |
c |     1887       542      180 |   60838  4452580  9316079 |    15    72913     1134   267038 | 15.766 % |
c |     1917       563      182 |   60838  4452580  9316079 |    16    44504     1152   305447 | 15.766 % |
c |     1943       599      185 |   60838  4452580  9316079 |    16    54504     1170   305447 | 15.766 % |
c |     2006       612      184 |   60838  4452580  9316079 |    16    64504     1192   305447 | 15.766 % |
c |     2084       632      182 |   60838  4452580  9316079 |    16    74504     1222   305447 | 15.766 % |
c |     2167       662      179 |   60838  4452580  9316079 |    17    43557     1236   346394 | 15.766 % |
c |     2211       697      180 |   60838  4452580  9316079 |    17    53557     1244   346394 | 15.766 % |
c |     2271       725      180 |   60826  4452480  9315875 |    17    63555     1250   346394 | 15.783 % |
c |     2323       744      180 |   60826  4452480  9315875 |    17    73555     1276   346394 | 15.783 % |
c |     2396       777      179 |   60826  4452480  9315875 |    17    83555     1286   346394 | 15.783 % |
c |     2455       790      179 |   60826  4452480  9315875 |    18    49967     1312   389982 | 15.783 % |
c |     2490       791      180 |   60826  4452480  9315875 |    18    59967     1326   389982 | 15.783 % |
c |     2538       838      181 |   60811  4452045  9314993 |    18    69966     1336   389982 | 15.803 % |
c |     2630       866      178 |   60811  4452045  9314993 |    18    79966     1344   389982 | 15.803 % |
c |     2731       889      175 |   60811  4452045  9314993 |    18    89966     1354   389982 | 15.803 % |
c |     2790       897      175 |   60811  4452045  9314993 |    19    53765     1358   436183 | 15.803 % |
c |     2835       925      176 |   60811  4452045  9314993 |    19    63765     1362   436183 | 15.803 % |
c |     2902       946      175 |   60811  4452045  9314993 |    19    73765     1382   436183 | 15.803 % |
c |     3003       964      173 |   60796  4451717  9314329 |    19    83764     1386   436183 | 15.824 % |
c |     3100       994      170 |   60796  4451717  9314329 |    19    93764     1388   436183 | 15.824 % |
c |     3170      1005      170 |   60796  4451717  9314329 |    20    54979     1394   484968 | 15.824 % |
c |     3205      1005      171 |   60796  4451717  9314329 |    20    64979     1400   484968 | 15.824 % |
c |     3282      1033      170 |   60796  4451717  9314329 |    20    74979     1402   484968 | 15.824 % |
c |     3336      1065      170 |   60796  4451717  9314329 |    20    84979     1404   484968 | 15.824 % |
c |     3429      1099      169 |   60796  4451717  9314329 |    20    94979     1412   484968 | 15.824 % |
c |     3545      1115      166 |   60796  4451717  9314329 |    21    53572     1414   536375 | 15.824 % |
c |     3580      1166      167 |   60796  4451717  9314329 |    21    63572     1422   536375 | 15.824 % |
c |     3648      1188      167 |   60796  4451717  9314329 |    21    73572     1424   536375 | 15.824 % |
c |     3733      1221      166 |   60796  4451717  9314329 |    21    83572     1430   536375 | 15.824 % |
c |     3839      1247      164 |   60796  4451717  9314329 |    21    93572     1434   536375 | 15.824 % |
c |     3955      1264      161 |   60796  4451717  9314329 |    21   103572     1438   536375 | 15.824 % |
c |     4048      1276      160 |   60792  4451608  9314110 |    22    59562     1444   590383 | 15.830 % |
c |     4093      1294      161 |   60792  4451608  9314110 |    22    69562     1444   590383 | 15.830 % |
c |     4159      1310      161 |   60792  4451608  9314110 |    22    79562     1446   590383 | 15.830 % |
c |     4271      1338      159 |   60792  4451608  9314110 |    22    89562     1454   590383 | 15.830 % |
c |     4396      1356      156 |   60792  4451608  9314110 |    22    99562     1460   590383 | 15.830 % |
c |     4543      1374      154 |   60792  4451608  9314110 |    22   109562     1466   590383 | 15.830 % |
c |     4623      1415      153 |   60792  4451608  9314110 |    23    62978     1470   646967 | 15.830 % |
c |     4689      1424      153 |   60792  4451608  9314110 |    23    72978     1474   646967 | 15.830 % |
c |     4787      1434      152 |   60792  4451608  9314110 |    23    82978     1478   646967 | 15.830 % |
c |     4918      1463      150 |   60764  4450897  9312673 |    23    92977     1480   646967 | 15.868 % |
c |     5046      1487      148 |   60764  4450897  9312673 |    23   102977     1486   646967 | 15.868 % |
c |     5173      1509      146 |   60764  4450897  9312673 |    23   112977     1500   646967 | 15.868 % |
c |     5281      1519      145 |   60764  4450897  9312673 |    24    63819     1510   706125 | 15.868 % |
c |     5315      1536      146 |   60764  4450897  9312673 |    24    73819     1514   706125 | 15.868 % |
c |     5393      1575      146 |   60764  4450897  9312673 |    24    83819     1522   706125 | 15.868 % |
c |     5506      1598      145 |   60764  4450897  9312673 |    24    93819     1522   706125 | 15.868 % |
c |     5628      1613      143 |   60764  4450897  9312673 |    24   103819     1524   706125 | 15.868 % |
c |     5761      1635      142 |   60764  4450897  9312673 |    24   113819     1528   706125 | 15.868 % |
c |     5911      1653      140 |   60764  4450897  9312673 |    24   123819     1534   706125 | 15.868 % |
c |     5945      1665      141 |   60764  4450897  9312673 |    25    72019     1536   767925 | 15.868 % |
c |     6034      1695      140 |   60764  4450897  9312673 |    25    82019     1540   767925 | 15.868 % |
c |     6154      1716      139 |   60709  4449579  9310019 |    25    92018     1546   767925 | 15.945 % |
c |     6270      1728      138 |   60709  4449579  9310019 |    25   102018     1548   767925 | 15.945 % |
c |     6429      1738      136 |   60709  4449579  9310019 |    25   112018     1548   767925 | 15.945 % |
c |     6594      1751      134 |   60709  4449579  9310019 |    25   122018     1550   767925 | 15.945 % |
c |     6726      1775      133 |   60709  4449579  9310019 |    26    67617     1554   832326 | 15.945 % |
c |     6767      1808      134 |   60709  4449579  9310019 |    26    77617     1556   832326 | 15.945 % |
c |     6831      1835      134 |   60709  4449579  9310019 |    26    87617     1558   832326 | 15.945 % |
c |     6942      1839      133 |   60709  4449579  9310019 |    26    97617     1558   832326 | 15.945 % |
c |     7054      1848      133 |   60709  4449579  9310019 |    26   107617     1558   832326 | 15.945 % |
c |     7188      1864      132 |   60709  4449579  9310019 |    26   117617     1566   832326 | 15.945 % |
c |     7326      1885      131 |   60709  4449579  9310019 |    26   127617     1570   832326 | 15.945 % |
c |     7420      1894      130 |   60709  4449579  9310019 |    27    70605     1570   899338 | 15.945 % |
c |     7420      1894      132 |   60709  4449579  9310019 |    27    80605     1570   899338 | 15.945 % |
c |     7420      1894      133 |   60709  4449579  9310019 |    27    90605     1570   899338 | 15.945 % |
c |     7430      1894      134 |   60709  4449579  9310019 |    27   100605     1570   899338 | 15.945 % |
c |     7453      1961      135 |   60709  4449579  9310019 |    27   110605     1570   899338 | 15.945 % |
c |     7540      1981      135 |   60709  4449579  9310019 |    27   120605     1570   899338 | 15.945 % |
c |     7605      2005      135 |   60709  4449579  9310019 |    27   130605     1584   899338 | 15.945 % |
c |     7701      2045      135 |   60709  4449579  9310019 |    28    70998     1586   968945 | 15.945 % |
c |     7705      2068      136 |   60709  4449579  9310019 |    28    80998     1586   968945 | 15.945 % |
c |     7714      2068      137 |   60709  4449579  9310019 |    28    90998     1586   968945 | 15.945 % |
c |     7714      2068      138 |   60709  4449579  9310019 |    28   100998     1586   968945 | 15.945 % |
c |     7735      2074      139 |   60709  4449579  9310019 |    28   110998     1586   968945 | 15.945 % |
c |     7778      2121      140 |   60709  4449579  9310019 |    28   120998     1588   968945 | 15.945 % |
c |     7871      2160      139 |   60704  4449449  9309755 |    28   130996     1590   968945 | 15.952 % |
c |     7995      2169      138 |   60704  4449449  9309755 |    28   140996     1590   968945 | 15.952 % |
c |     8049      2215      139 |   60704  4449449  9309755 |    29    78782     1590  1041159 | 15.952 % |
c |     8115      2227      139 |   60704  4449449  9309755 |    29    88782     1596  1041159 | 15.952 % |
c |     8221      2238      138 |   60704  4449449  9309755 |    29    98782     1596  1041159 | 15.952 % |
c |     8343      2244      137 |   60704  4449449  9309755 |    29   108782     1596  1041159 | 15.952 % |
c |     8480      2251      136 |   60704  4449449  9309755 |    29   118782     1596  1041159 | 15.952 % |
c |     8626      2276      135 |   60704  4449449  9309755 |    29   128782     1596  1041159 | 15.952 % |
c |     8776      2292      134 |   60704  4449449  9309755 |    29   138782     1598  1041159 | 15.952 % |
c |     8921      2302      133 |   60704  4449449  9309755 |    29   148782     1598  1041159 | 15.952 % |
c |     8966      2343      133 |   60704  4449449  9309755 |    30    83989     1598  1115952 | 15.952 % |
c |     9004      2358      134 |   60704  4449449  9309755 |    30    93989     1600  1115952 | 15.952 % |
c |     9086      2364      134 |   60704  4449449  9309755 |    30   103989     1604  1115952 | 15.952 % |
c |     9198      2390      133 |   60704  4449449  9309755 |    30   113989     1606  1115952 | 15.952 % |
c |     9322      2403      133 |   60704  4449449  9309755 |    30   123989     1606  1115952 | 15.952 % |
c |     9446      2413      132 |   60704  4449449  9309755 |    30   133989     1606  1115952 | 15.952 % |
c |     9575      2425      131 |   60704  4449449  9309755 |    30   143989     1608  1115952 | 15.952 % |
c |     9720      2434      130 |   60704  4449449  9309755 |    30   153989     1612  1115952 | 15.952 % |
c |     9759      2489      131 |   60704  4449449  9309755 |    31    86608     1616  1193333 | 15.952 % |
c |     9797      2489      131 |   60704  4449449  9309755 |    31    96608     1618  1193333 | 15.952 % |
c |     9874      2509      131 |   60704  4449449  9309755 |    31   106608     1622  1193333 | 15.952 % |
c |     9963      2534      131 |   60698  4449269  9309393 |    31   116607     1622  1193333 | 15.960 % |
Interrupt signal (15) received.

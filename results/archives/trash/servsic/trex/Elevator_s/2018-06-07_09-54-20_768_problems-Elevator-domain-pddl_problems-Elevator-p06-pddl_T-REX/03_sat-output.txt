Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.003] Processed problem encoding.
[0.004] Calculated possible fact changes of composite elements.
[0.004] Initialized instantiation procedure.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     0   * * *
[0.004] *************************************
[0.004] Instantiated 93 initial clauses.
[0.004] The encoding contains a total of 66 distinct variables.
[0.004] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.004] Executed solver; result: UNSAT.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     1   * * *
[0.004] *************************************
[0.004] Computed next depth properties: array size of 4.
[0.004] Instantiated 154 transitional clauses.
[0.004] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.004] Instantiated 445 universal clauses.
[0.004] Instantiated and added clauses for a total of 692 clauses.
[0.004] The encoding contains a total of 197 distinct variables.
[0.004] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.004] Executed solver; result: UNSAT.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     2   * * *
[0.004] *************************************
[0.004] Computed next depth properties: array size of 7.
[0.005] Instantiated 583 transitional clauses.
[0.005] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.005] Instantiated 2,441 universal clauses.
[0.005] Instantiated and added clauses for a total of 3,716 clauses.
[0.005] The encoding contains a total of 425 distinct variables.
[0.005] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.005] Executed solver; result: UNSAT.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     3   * * *
[0.005] *************************************
[0.006] Computed next depth properties: array size of 13.
[0.006] Instantiated 882 transitional clauses.
[0.008] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.008] Instantiated 5,516 universal clauses.
[0.008] Instantiated and added clauses for a total of 10,114 clauses.
[0.008] The encoding contains a total of 782 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.008] Executed solver; result: UNSAT.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     4   * * *
[0.008] *************************************
[0.008] Computed next depth properties: array size of 21.
[0.009] Instantiated 992 transitional clauses.
[0.011] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.011] Instantiated 7,025 universal clauses.
[0.011] Instantiated and added clauses for a total of 18,131 clauses.
[0.011] The encoding contains a total of 1,279 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 21 assumptions
[0.011] Executed solver; result: UNSAT.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     5   * * *
[0.011] *************************************
[0.011] Computed next depth properties: array size of 31.
[0.012] Instantiated 1,284 transitional clauses.
[0.014] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.014] Instantiated 8,732 universal clauses.
[0.014] Instantiated and added clauses for a total of 28,147 clauses.
[0.014] The encoding contains a total of 1,944 distinct variables.
[0.014] Attempting solve with solver <glucose4> ...
c 31 assumptions
[0.015] Executed solver; result: UNSAT.
[0.015] 
[0.015] *************************************
[0.015] * * *   M a k e s p a n     6   * * *
[0.015] *************************************
[0.015] Computed next depth properties: array size of 43.
[0.016] Instantiated 1,616 transitional clauses.
[0.019] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.019] Instantiated 10,637 universal clauses.
[0.019] Instantiated and added clauses for a total of 40,400 clauses.
[0.019] The encoding contains a total of 2,799 distinct variables.
[0.019] Attempting solve with solver <glucose4> ...
c 43 assumptions
[0.020] Executed solver; result: UNSAT.
[0.020] 
[0.020] *************************************
[0.020] * * *   M a k e s p a n     7   * * *
[0.020] *************************************
[0.021] Computed next depth properties: array size of 57.
[0.022] Instantiated 1,988 transitional clauses.
[0.026] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.026] Instantiated 12,740 universal clauses.
[0.026] Instantiated and added clauses for a total of 55,128 clauses.
[0.026] The encoding contains a total of 3,866 distinct variables.
[0.026] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.052] Executed solver; result: UNSAT.
[0.052] 
[0.052] *************************************
[0.052] * * *   M a k e s p a n     8   * * *
[0.052] *************************************
[0.052] Computed next depth properties: array size of 73.
[0.054] Instantiated 2,400 transitional clauses.
[0.057] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.057] Instantiated 15,041 universal clauses.
[0.057] Instantiated and added clauses for a total of 72,569 clauses.
[0.057] The encoding contains a total of 5,167 distinct variables.
[0.057] Attempting solve with solver <glucose4> ...
c 73 assumptions
c last restart ## conflicts  :  1 152 
[0.059] Executed solver; result: SAT.
[0.059] Solver returned SAT; a solution has been found at makespan 8.
23
solution 109 1
18 14 67 10 25 11 29 5 12 52 9 28 104 3 19 7 13 88 8 109 4 74 6 
[0.059] Exiting.

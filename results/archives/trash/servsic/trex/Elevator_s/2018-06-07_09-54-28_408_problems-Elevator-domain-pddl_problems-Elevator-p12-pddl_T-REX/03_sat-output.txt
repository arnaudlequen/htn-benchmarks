Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.011] Processed problem encoding.
[0.012] Calculated possible fact changes of composite elements.
[0.012] Initialized instantiation procedure.
[0.012] 
[0.012] *************************************
[0.012] * * *   M a k e s p a n     0   * * *
[0.012] *************************************
[0.012] Instantiated 183 initial clauses.
[0.012] The encoding contains a total of 126 distinct variables.
[0.012] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.012] Executed solver; result: UNSAT.
[0.012] 
[0.012] *************************************
[0.012] * * *   M a k e s p a n     1   * * *
[0.012] *************************************
[0.012] Computed next depth properties: array size of 4.
[0.013] Instantiated 448 transitional clauses.
[0.013] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.013] Instantiated 1,469 universal clauses.
[0.013] Instantiated and added clauses for a total of 2,100 clauses.
[0.013] The encoding contains a total of 526 distinct variables.
[0.013] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.013] Executed solver; result: UNSAT.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     2   * * *
[0.013] *************************************
[0.014] Computed next depth properties: array size of 7.
[0.016] Instantiated 2,232 transitional clauses.
[0.018] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.018] Instantiated 35,377 universal clauses.
[0.018] Instantiated and added clauses for a total of 39,709 clauses.
[0.018] The encoding contains a total of 1,328 distinct variables.
[0.018] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.018] Executed solver; result: UNSAT.
[0.018] 
[0.018] *************************************
[0.018] * * *   M a k e s p a n     3   * * *
[0.018] *************************************
[0.019] Computed next depth properties: array size of 13.
[0.022] Instantiated 3,432 transitional clauses.
[0.029] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.029] Instantiated 75,800 universal clauses.
[0.029] Instantiated and added clauses for a total of 118,941 clauses.
[0.029] The encoding contains a total of 2,490 distinct variables.
[0.029] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.029] Executed solver; result: UNSAT.
[0.029] 
[0.029] *************************************
[0.029] * * *   M a k e s p a n     4   * * *
[0.029] *************************************
[0.030] Computed next depth properties: array size of 21.
[0.033] Instantiated 3,545 transitional clauses.
[0.042] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.042] Instantiated 90,839 universal clauses.
[0.042] Instantiated and added clauses for a total of 213,325 clauses.
[0.042] The encoding contains a total of 4,045 distinct variables.
[0.042] Attempting solve with solver <glucose4> ...
c 21 assumptions
[0.042] Executed solver; result: UNSAT.
[0.042] 
[0.042] *************************************
[0.042] * * *   M a k e s p a n     5   * * *
[0.042] *************************************
[0.044] Computed next depth properties: array size of 31.
[0.046] Instantiated 4,377 transitional clauses.
[0.058] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.058] Instantiated 106,540 universal clauses.
[0.058] Instantiated and added clauses for a total of 324,242 clauses.
[0.058] The encoding contains a total of 6,050 distinct variables.
[0.058] Attempting solve with solver <glucose4> ...
c 31 assumptions
[0.060] Executed solver; result: UNSAT.
[0.060] 
[0.060] *************************************
[0.060] * * *   M a k e s p a n     6   * * *
[0.060] *************************************
[0.061] Computed next depth properties: array size of 43.
[0.064] Instantiated 5,293 transitional clauses.
[0.077] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.077] Instantiated 122,903 universal clauses.
[0.077] Instantiated and added clauses for a total of 452,438 clauses.
[0.077] The encoding contains a total of 8,549 distinct variables.
[0.077] Attempting solve with solver <glucose4> ...
c 43 assumptions
[0.079] Executed solver; result: UNSAT.
[0.079] 
[0.079] *************************************
[0.079] * * *   M a k e s p a n     7   * * *
[0.079] *************************************
[0.080] Computed next depth properties: array size of 57.
[0.083] Instantiated 6,293 transitional clauses.
[0.096] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.096] Instantiated 139,928 universal clauses.
[0.096] Instantiated and added clauses for a total of 598,659 clauses.
[0.096] The encoding contains a total of 11,586 distinct variables.
[0.096] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.101] Executed solver; result: UNSAT.
[0.101] 
[0.101] *************************************
[0.101] * * *   M a k e s p a n     8   * * *
[0.101] *************************************
[0.103] Computed next depth properties: array size of 73.
[0.106] Instantiated 7,377 transitional clauses.
[0.121] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.121] Instantiated 157,615 universal clauses.
[0.121] Instantiated and added clauses for a total of 763,651 clauses.
[0.121] The encoding contains a total of 15,205 distinct variables.
[0.121] Attempting solve with solver <glucose4> ...
c 73 assumptions
[0.249] Executed solver; result: UNSAT.
[0.249] 
[0.249] *************************************
[0.249] * * *   M a k e s p a n     9   * * *
[0.249] *************************************
[0.250] Computed next depth properties: array size of 91.
[0.254] Instantiated 8,545 transitional clauses.
[0.266] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.266] Instantiated 175,964 universal clauses.
[0.266] Instantiated and added clauses for a total of 948,160 clauses.
[0.266] The encoding contains a total of 19,450 distinct variables.
[0.266] Attempting solve with solver <glucose4> ...
c 91 assumptions
c |      107         0       93 |   15996   657638  1405592 |     2     6206      258     3788 | 17.754 % |
c |      241         0       82 |   15996   657638  1405592 |     3    11212      302     8782 | 17.754 % |
c |      396         0       75 |   15996   657638  1405592 |     4    13824      326    16170 | 17.754 % |
c |      568         3       70 |   15996   657638  1405592 |     5    13925      336    26069 | 17.754 % |
[6.516] Executed solver; result: UNSAT.
[6.516] 
[6.516] *************************************
[6.516] * * *   M a k e s p a n    10   * * *
[6.516] *************************************
[6.517] Computed next depth properties: array size of 111.
[6.521] Instantiated 9,797 transitional clauses.
[6.543] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[6.543] Instantiated 194,975 universal clauses.
[6.543] Instantiated and added clauses for a total of 1,152,932 clauses.
[6.543] The encoding contains a total of 24,365 distinct variables.
[6.543] Attempting solve with solver <glucose4> ...
c 111 assumptions
c |      706        55       70 |   20391   823873  1756302 |     5    23922      372    26069 | 16.307 % |
c |      713        55       84 |   20370   823310  1755160 |     6    21468      428    38522 | 16.393 % |
c |      719        55       97 |   20367   823273  1755084 |     7    16456      450    53533 | 16.405 % |
c |      738        55      108 |   20360   823155  1754845 |     7    26454      484    53533 | 16.434 % |
c |      784        55      114 |   20360   823155  1754845 |     8    18857      516    71130 | 16.434 % |
c |      814        55      122 |   20360   823155  1754845 |     8    28857      532    71130 | 16.434 % |
c |      926        55      118 |   20360   823155  1754845 |     8    38857      546    71130 | 16.434 % |
c |      974        55      123 |   20332   822554  1753628 |     9    28646      552    91338 | 16.549 % |
c |     1103        55      117 |   20329   822495  1753508 |     9    38645      578    91338 | 16.561 % |
c |     1227        55      114 |   20324   822385  1753284 |    10    25856      590   114124 | 16.582 % |
c |     1289        55      116 |   20324   822385  1753284 |    10    35856      592   114124 | 16.582 % |
c |     1438        55      111 |   20324   822385  1753284 |    10    45856      604   114124 | 16.582 % |
c |     1544        55      110 |   20324   822385  1753284 |    11    30468      612   139512 | 16.582 % |
c |     1676        55      107 |   20324   822385  1753284 |    11    40468      616   139512 | 16.582 % |
c |     1863        55      101 |   20324   822385  1753284 |    11    50468      620   139512 | 16.582 % |
c |     1986        59      100 |   20324   822385  1753284 |    12    32473      630   167507 | 16.582 % |
c |     2132        59       98 |   20290   822287  1753082 |    12    42471      632   167507 | 16.721 % |
c |     2297        59       95 |   20290   822287  1753082 |    12    52471      636   167507 | 16.721 % |
c |     2467        69       93 |   20290   822287  1753082 |    13    31867      646   198111 | 16.721 % |
c |     2510        69       95 |   20290   822287  1753082 |    13    41867      646   198111 | 16.721 % |
c |     2663        69       93 |   20290   822287  1753082 |    13    51867      650   198111 | 16.721 % |
c |     2847        69       91 |   20290   822287  1753082 |    13    61867      656   198111 | 16.721 % |
c |     2942        69       91 |   20290   822287  1753082 |    14    38668      662   231310 | 16.721 % |
c |     3052        69       91 |   20290   822287  1753082 |    14    48668      666   231310 | 16.721 % |
c |     3241        69       89 |   20290   822287  1753082 |    14    58668      670   231310 | 16.721 % |
c |     3431        69       87 |   20290   822287  1753082 |    14    68668      674   231310 | 16.721 % |
c |     3537        79       87 |   20290   822287  1753082 |    15    42867      676   267111 | 16.721 % |
c |     3660        79       87 |   20290   822287  1753082 |    15    52867      678   267111 | 16.721 % |
c |     3839        79       85 |   20290   822287  1753082 |    15    62867      678   267111 | 16.721 % |
c |     4035        79       84 |   20290   822287  1753082 |    15    72867      678   267111 | 16.721 % |
c |     4124        92       84 |   20290   822287  1753082 |    16    44450      682   305528 | 16.721 % |
c |     4216        92       85 |   20290   822287  1753082 |    16    54450      686   305528 | 16.721 % |
c |     4396        92       84 |   20290   822287  1753082 |    16    64450      686   305528 | 16.721 % |
c |     4592        92       82 |   20290   822287  1753082 |    16    74450      686   305528 | 16.721 % |
c |     4744        96       82 |   20290   822287  1753082 |    17    43452      686   346526 | 16.721 % |
c |     4818        97       83 |   20290   822287  1753082 |    17    53452      688   346526 | 16.721 % |
c |     4997        97       82 |   20290   822287  1753082 |    17    63452      688   346526 | 16.721 % |
c |     5194        97       80 |   20290   822287  1753082 |    17    73452      688   346526 | 16.721 % |
c |     5390        97       79 |   20290   822287  1753082 |    17    83452      688   346526 | 16.721 % |
c |     5481       115       80 |   20290   822287  1753082 |    18    49837      688   390141 | 16.721 % |
c |     5563       117       80 |   20290   822287  1753082 |    18    59837      690   390141 | 16.721 % |
c |     5745       117       80 |   20290   822287  1753082 |    18    69837      690   390141 | 16.721 % |
c |     5942       117       79 |   20290   822287  1753082 |    18    79837      690   390141 | 16.721 % |
c |     6138       117       78 |   20290   822287  1753082 |    18    89837      690   390141 | 16.721 % |
[97.446] Executed solver; result: UNSAT.
[97.446] 
[97.446] *************************************
[97.446] * * *   M a k e s p a n    11   * * *
[97.446] *************************************
[97.448] Computed next depth properties: array size of 133.
[97.453] Instantiated 11,133 transitional clauses.
[97.467] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[97.468] Instantiated 214,648 universal clauses.
[97.468] Instantiated and added clauses for a total of 1,378,713 clauses.
[97.468] The encoding contains a total of 29,994 distinct variables.
[97.468] Attempting solve with solver <glucose4> ...
c 133 assumptions
c |     6156       173       79 |   25318  1007610  2143897 |    19    53630      724   436346 | 15.587 % |
c last restart ## conflicts  :  7725 232 
[99.734] Executed solver; result: SAT.
[99.734] Solver returned SAT; a solution has been found at makespan 11.
42
solution 372 1
30 15 42 21 189 3 17 16 74 24 146 14 26 279 25 13 269 19 87 23 211 20 101 358 6 95 22 8 194 5 4 18 82 10 372 9 363 7 332 11 351 12 
[99.734] Exiting.

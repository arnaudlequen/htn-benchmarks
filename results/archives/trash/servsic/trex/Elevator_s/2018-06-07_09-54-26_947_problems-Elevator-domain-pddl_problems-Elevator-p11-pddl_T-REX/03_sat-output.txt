Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.010] Processed problem encoding.
[0.011] Calculated possible fact changes of composite elements.
[0.011] Initialized instantiation procedure.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     0   * * *
[0.011] *************************************
[0.011] Instantiated 168 initial clauses.
[0.011] The encoding contains a total of 116 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.011] Executed solver; result: UNSAT.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     1   * * *
[0.011] *************************************
[0.012] Computed next depth properties: array size of 4.
[0.012] Instantiated 389 transitional clauses.
[0.013] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.013] Instantiated 1,258 universal clauses.
[0.013] Instantiated and added clauses for a total of 1,815 clauses.
[0.013] The encoding contains a total of 461 distinct variables.
[0.013] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.013] Executed solver; result: UNSAT.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     2   * * *
[0.013] *************************************
[0.013] Computed next depth properties: array size of 7.
[0.015] Instantiated 1,850 transitional clauses.
[0.017] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.017] Instantiated 20,581 universal clauses.
[0.017] Instantiated and added clauses for a total of 24,246 clauses.
[0.017] The encoding contains a total of 1,110 distinct variables.
[0.017] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.017] Executed solver; result: UNSAT.
[0.017] 
[0.017] *************************************
[0.017] * * *   M a k e s p a n     3   * * *
[0.017] *************************************
[0.017] Computed next depth properties: array size of 13.
[0.020] Instantiated 2,691 transitional clauses.
[0.025] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.025] Instantiated 38,918 universal clauses.
[0.025] Instantiated and added clauses for a total of 65,855 clauses.
[0.025] The encoding contains a total of 1,982 distinct variables.
[0.025] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.025] Executed solver; result: UNSAT.
[0.025] 
[0.025] *************************************
[0.025] * * *   M a k e s p a n     4   * * *
[0.025] *************************************
[0.025] Computed next depth properties: array size of 21.
[0.028] Instantiated 2,561 transitional clauses.
[0.034] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.034] Instantiated 45,216 universal clauses.
[0.034] Instantiated and added clauses for a total of 113,632 clauses.
[0.034] The encoding contains a total of 3,134 distinct variables.
[0.034] Attempting solve with solver <glucose4> ...
c 21 assumptions
[0.034] Executed solver; result: UNSAT.
[0.034] 
[0.034] *************************************
[0.034] * * *   M a k e s p a n     5   * * *
[0.034] *************************************
[0.035] Computed next depth properties: array size of 31.
[0.037] Instantiated 3,159 transitional clauses.
[0.044] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.044] Instantiated 52,033 universal clauses.
[0.044] Instantiated and added clauses for a total of 168,824 clauses.
[0.044] The encoding contains a total of 4,618 distinct variables.
[0.044] Attempting solve with solver <glucose4> ...
c 31 assumptions
[0.045] Executed solver; result: UNSAT.
[0.045] 
[0.045] *************************************
[0.045] * * *   M a k e s p a n     6   * * *
[0.045] *************************************
[0.046] Computed next depth properties: array size of 43.
[0.049] Instantiated 3,829 transitional clauses.
[0.058] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.058] Instantiated 59,369 universal clauses.
[0.058] Instantiated and added clauses for a total of 232,022 clauses.
[0.058] The encoding contains a total of 6,472 distinct variables.
[0.058] Attempting solve with solver <glucose4> ...
c 43 assumptions
[0.059] Executed solver; result: UNSAT.
[0.059] 
[0.059] *************************************
[0.059] * * *   M a k e s p a n     7   * * *
[0.059] *************************************
[0.060] Computed next depth properties: array size of 57.
[0.063] Instantiated 4,571 transitional clauses.
[0.073] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.073] Instantiated 67,224 universal clauses.
[0.073] Instantiated and added clauses for a total of 303,817 clauses.
[0.073] The encoding contains a total of 8,734 distinct variables.
[0.073] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.167] Executed solver; result: UNSAT.
[0.167] 
[0.167] *************************************
[0.167] * * *   M a k e s p a n     8   * * *
[0.167] *************************************
[0.167] Computed next depth properties: array size of 73.
[0.169] Instantiated 5,385 transitional clauses.
[0.175] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.175] Instantiated 75,598 universal clauses.
[0.175] Instantiated and added clauses for a total of 384,800 clauses.
[0.175] The encoding contains a total of 11,442 distinct variables.
[0.175] Attempting solve with solver <glucose4> ...
c 73 assumptions
c |      118         0       84 |    9011   232299   516809 |     2     6207      182     3787 | 21.239 % |
[0.786] Executed solver; result: UNSAT.
[0.786] 
[0.786] *************************************
[0.786] * * *   M a k e s p a n     9   * * *
[0.786] *************************************
[0.787] Computed next depth properties: array size of 91.
[0.790] Instantiated 6,271 transitional clauses.
[0.796] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.796] Instantiated 84,491 universal clauses.
[0.796] Instantiated and added clauses for a total of 475,562 clauses.
[0.796] The encoding contains a total of 14,634 distinct variables.
[0.796] Attempting solve with solver <glucose4> ...
c 91 assumptions
c last restart ## conflicts  :  21 194 
[0.800] Executed solver; result: SAT.
[0.800] Solver returned SAT; a solution has been found at makespan 9.
36
solution 282 1
14 28 23 175 17 111 22 9 171 7 18 121 20 19 161 15 16 60 24 202 275 6 82 21 8 127 10 12 11 282 5 4 242 3 218 13 
[0.801] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.016] Processed problem encoding.
[0.017] Calculated possible fact changes of composite elements.
[0.017] Initialized instantiation procedure.
[0.017] 
[0.017] *************************************
[0.017] * * *   M a k e s p a n     0   * * *
[0.017] *************************************
[0.017] Instantiated 198 initial clauses.
[0.017] The encoding contains a total of 136 distinct variables.
[0.017] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.017] Executed solver; result: UNSAT.
[0.017] 
[0.017] *************************************
[0.017] * * *   M a k e s p a n     1   * * *
[0.017] *************************************
[0.018] Computed next depth properties: array size of 4.
[0.018] Instantiated 511 transitional clauses.
[0.019] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.019] Instantiated 1,688 universal clauses.
[0.019] Instantiated and added clauses for a total of 2,397 clauses.
[0.019] The encoding contains a total of 591 distinct variables.
[0.019] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.019] Executed solver; result: UNSAT.
[0.019] 
[0.019] *************************************
[0.019] * * *   M a k e s p a n     2   * * *
[0.019] *************************************
[0.020] Computed next depth properties: array size of 7.
[0.022] Instantiated 2,532 transitional clauses.
[0.025] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.025] Instantiated 34,821 universal clauses.
[0.025] Instantiated and added clauses for a total of 39,750 clauses.
[0.025] The encoding contains a total of 1,440 distinct variables.
[0.025] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.025] Executed solver; result: UNSAT.
[0.025] 
[0.025] *************************************
[0.025] * * *   M a k e s p a n     3   * * *
[0.025] *************************************
[0.026] Computed next depth properties: array size of 13.
[0.029] Instantiated 3,628 transitional clauses.
[0.037] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.037] Instantiated 72,212 universal clauses.
[0.037] Instantiated and added clauses for a total of 115,590 clauses.
[0.037] The encoding contains a total of 2,570 distinct variables.
[0.037] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.037] Executed solver; result: UNSAT.
[0.037] 
[0.037] *************************************
[0.037] * * *   M a k e s p a n     4   * * *
[0.037] *************************************
[0.038] Computed next depth properties: array size of 21.
[0.041] Instantiated 3,403 transitional clauses.
[0.051] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.051] Instantiated 82,955 universal clauses.
[0.051] Instantiated and added clauses for a total of 201,948 clauses.
[0.051] The encoding contains a total of 4,044 distinct variables.
[0.051] Attempting solve with solver <glucose4> ...
c 21 assumptions
[0.051] Executed solver; result: UNSAT.
[0.051] 
[0.051] *************************************
[0.051] * * *   M a k e s p a n     5   * * *
[0.051] *************************************
[0.052] Computed next depth properties: array size of 31.
[0.056] Instantiated 4,134 transitional clauses.
[0.066] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.066] Instantiated 94,288 universal clauses.
[0.066] Instantiated and added clauses for a total of 300,370 clauses.
[0.066] The encoding contains a total of 5,919 distinct variables.
[0.066] Attempting solve with solver <glucose4> ...
c 31 assumptions
[0.067] Executed solver; result: UNSAT.
[0.067] 
[0.067] *************************************
[0.067] * * *   M a k e s p a n     6   * * *
[0.067] *************************************
[0.069] Computed next depth properties: array size of 43.
[0.072] Instantiated 4,943 transitional clauses.
[0.083] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.083] Instantiated 106,211 universal clauses.
[0.083] Instantiated and added clauses for a total of 411,524 clauses.
[0.083] The encoding contains a total of 8,236 distinct variables.
[0.083] Attempting solve with solver <glucose4> ...
c 43 assumptions
[0.085] Executed solver; result: UNSAT.
[0.085] 
[0.085] *************************************
[0.085] * * *   M a k e s p a n     7   * * *
[0.085] *************************************
[0.086] Computed next depth properties: array size of 57.
[0.090] Instantiated 5,830 transitional clauses.
[0.104] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.104] Instantiated 118,724 universal clauses.
[0.104] Instantiated and added clauses for a total of 536,078 clauses.
[0.104] The encoding contains a total of 11,036 distinct variables.
[0.104] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.119] Executed solver; result: UNSAT.
[0.119] 
[0.119] *************************************
[0.119] * * *   M a k e s p a n     8   * * *
[0.119] *************************************
[0.121] Computed next depth properties: array size of 73.
[0.124] Instantiated 6,795 transitional clauses.
[0.137] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.137] Instantiated 131,827 universal clauses.
[0.137] Instantiated and added clauses for a total of 674,700 clauses.
[0.137] The encoding contains a total of 14,360 distinct variables.
[0.137] Attempting solve with solver <glucose4> ...
c 73 assumptions
[0.193] Executed solver; result: UNSAT.
[0.193] 
[0.193] *************************************
[0.193] * * *   M a k e s p a n     9   * * *
[0.193] *************************************
[0.195] Computed next depth properties: array size of 91.
[0.198] Instantiated 7,838 transitional clauses.
[0.207] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.207] Instantiated 145,520 universal clauses.
[0.207] Instantiated and added clauses for a total of 828,058 clauses.
[0.207] The encoding contains a total of 18,249 distinct variables.
[0.207] Attempting solve with solver <glucose4> ...
c 91 assumptions
c |      116         0       86 |   14595   533575  1157654 |     2     6216      212     3774 | 20.019 % |
[2.005] Executed solver; result: UNSAT.
[2.005] 
[2.005] *************************************
[2.005] * * *   M a k e s p a n    10   * * *
[2.005] *************************************
[2.006] Computed next depth properties: array size of 111.
[2.009] Instantiated 8,959 transitional clauses.
[2.021] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.021] Instantiated 159,803 universal clauses.
[2.021] Instantiated and added clauses for a total of 996,820 clauses.
[2.021] The encoding contains a total of 22,744 distinct variables.
[2.021] Attempting solve with solver <glucose4> ...
c 111 assumptions
c |      229        12       87 |   18541   662841  1433645 |     3    11228      310     8761 | 18.476 % |
c last restart ## conflicts  :  131 221 
[2.398] Executed solver; result: SAT.
[2.398] Solver returned SAT; a solution has been found at makespan 10.
41
solution 421 1
16 31 24 26 25 145 20 79 9 23 22 134 19 75 28 231 5 18 68 21 6 127 17 300 15 421 14 412 4 3 50 11 27 392 8 7 321 13 12 399 10 
[2.398] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.008] Processed problem encoding.
[0.009] Calculated possible fact changes of composite elements.
[0.009] Initialized instantiation procedure.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     0   * * *
[0.009] *************************************
[0.009] Instantiated 228 initial clauses.
[0.009] The encoding contains a total of 156 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.009] Executed solver; result: UNSAT.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     1   * * *
[0.009] *************************************
[0.009] Computed next depth properties: array size of 4.
[0.010] Instantiated 649 transitional clauses.
[0.010] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.010] Instantiated 2,192 universal clauses.
[0.010] Instantiated and added clauses for a total of 3,069 clauses.
[0.010] The encoding contains a total of 742 distinct variables.
[0.010] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.010] Executed solver; result: UNSAT.
[0.010] 
[0.010] *************************************
[0.010] * * *   M a k e s p a n     2   * * *
[0.010] *************************************
[0.011] Computed next depth properties: array size of 7.
[0.012] Instantiated 3,327 transitional clauses.
[0.014] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.014] Instantiated 55,453 universal clauses.
[0.014] Instantiated and added clauses for a total of 61,849 clauses.
[0.014] The encoding contains a total of 1,829 distinct variables.
[0.014] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.014] Executed solver; result: UNSAT.
[0.014] 
[0.014] *************************************
[0.014] * * *   M a k e s p a n     3   * * *
[0.014] *************************************
[0.014] Computed next depth properties: array size of 13.
[0.017] Instantiated 4,696 transitional clauses.
[0.023] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.023] Instantiated 114,993 universal clauses.
[0.023] Instantiated and added clauses for a total of 181,538 clauses.
[0.023] The encoding contains a total of 3,235 distinct variables.
[0.023] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.023] Executed solver; result: UNSAT.
[0.023] 
[0.023] *************************************
[0.023] * * *   M a k e s p a n     4   * * *
[0.023] *************************************
[0.024] Computed next depth properties: array size of 21.
[0.025] Instantiated 4,268 transitional clauses.
[0.033] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.033] Instantiated 130,474 universal clauses.
[0.033] Instantiated and added clauses for a total of 316,280 clauses.
[0.033] The encoding contains a total of 5,054 distinct variables.
[0.033] Attempting solve with solver <glucose4> ...
c 21 assumptions
[0.033] Executed solver; result: UNSAT.
[0.033] 
[0.033] *************************************
[0.033] * * *   M a k e s p a n     5   * * *
[0.033] *************************************
[0.034] Computed next depth properties: array size of 31.
[0.036] Instantiated 5,146 transitional clauses.
[0.044] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.044] Instantiated 146,740 universal clauses.
[0.044] Instantiated and added clauses for a total of 468,166 clauses.
[0.044] The encoding contains a total of 7,353 distinct variables.
[0.044] Attempting solve with solver <glucose4> ...
c 31 assumptions
[0.045] Executed solver; result: UNSAT.
[0.045] 
[0.045] *************************************
[0.045] * * *   M a k e s p a n     6   * * *
[0.045] *************************************
[0.046] Computed next depth properties: array size of 43.
[0.049] Instantiated 6,116 transitional clauses.
[0.059] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.059] Instantiated 163,791 universal clauses.
[0.059] Instantiated and added clauses for a total of 638,073 clauses.
[0.059] The encoding contains a total of 10,180 distinct variables.
[0.059] Attempting solve with solver <glucose4> ...
c 43 assumptions
[0.060] Executed solver; result: UNSAT.
[0.060] 
[0.060] *************************************
[0.060] * * *   M a k e s p a n     7   * * *
[0.060] *************************************
[0.062] Computed next depth properties: array size of 57.
[0.064] Instantiated 7,178 transitional clauses.
[0.076] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.076] Instantiated 181,627 universal clauses.
[0.076] Instantiated and added clauses for a total of 826,878 clauses.
[0.076] The encoding contains a total of 13,583 distinct variables.
[0.076] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.081] Executed solver; result: UNSAT.
[0.081] 
[0.081] *************************************
[0.081] * * *   M a k e s p a n     8   * * *
[0.081] *************************************
[0.083] Computed next depth properties: array size of 73.
[0.086] Instantiated 8,332 transitional clauses.
[0.099] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.100] Instantiated 200,248 universal clauses.
[0.100] Instantiated and added clauses for a total of 1,035,458 clauses.
[0.100] The encoding contains a total of 17,610 distinct variables.
[0.100] Attempting solve with solver <glucose4> ...
c 73 assumptions
[0.138] Executed solver; result: UNSAT.
[0.138] 
[0.138] *************************************
[0.138] * * *   M a k e s p a n     9   * * *
[0.138] *************************************
[0.140] Computed next depth properties: array size of 91.
[0.143] Instantiated 9,578 transitional clauses.
[0.157] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.157] Instantiated 219,654 universal clauses.
[0.157] Instantiated and added clauses for a total of 1,264,690 clauses.
[0.157] The encoding contains a total of 22,309 distinct variables.
[0.157] Attempting solve with solver <glucose4> ...
c 91 assumptions
c |      115         0       86 |   17840   801713  1728768 |     2     6232      190     3751 | 20.029 % |
c |      240         0       83 |   17840   801713  1728768 |     3    11253      224     8730 | 20.029 % |
[2.001] Executed solver; result: UNSAT.
[2.001] 
[2.001] *************************************
[2.001] * * *   M a k e s p a n    10   * * *
[2.001] *************************************
[2.003] Computed next depth properties: array size of 111.
[2.008] Instantiated 10,916 transitional clauses.
[2.024] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.024] Instantiated 239,845 universal clauses.
[2.024] Instantiated and added clauses for a total of 1,515,451 clauses.
[2.024] The encoding contains a total of 27,728 distinct variables.
[2.024] Attempting solve with solver <glucose4> ...
c 111 assumptions
c |      254        36      118 |   22586   991288  2131094 |     4    13838      336    16141 | 18.541 % |
c |      311        36      128 |   22579   990778  2130042 |     5    13931      398    26043 | 18.567 % |
c |      435        36      114 |   22563   990778  2130042 |     5    23929      476    26043 | 18.624 % |
c |      536        36      111 |   22563   990778  2130042 |     6    21530      504    38442 | 18.624 % |
c |      645        38      108 |   22563   990778  2130042 |     7    16533      530    53439 | 18.624 % |
c |      799        38      100 |   22563   990778  2130042 |     7    26533      538    53439 | 18.624 % |
c |      958        41       93 |   22559   990392  2129246 |     8    18910      552    71060 | 18.639 % |
c |     1050        42       95 |   22531   989772  2127992 |     8    28909      570    71060 | 18.740 % |
c |     1232        42       89 |   22531   989772  2127992 |     8    38909      586    71060 | 18.740 % |
c |     1382        45       86 |   22500   989084  2126603 |     9    28723      602    91244 | 18.852 % |
c |     1573        46       82 |   22500   989084  2126603 |     9    38723      606    91244 | 18.852 % |
c |     1716        63       81 |   22498   989047  2126522 |    10    25910      616   114056 | 18.859 % |
c |     1830        63       81 |   22470   988440  2125296 |    10    35909      622   114056 | 18.960 % |
c |     2012        63       79 |   22463   988289  2124986 |    10    45904      636   114056 | 18.985 % |
c |     2134        65       79 |   22463   988289  2124986 |    11    30525      646   139435 | 18.985 % |
c |     2310        65       77 |   22463   988289  2124986 |    11    40525      648   139435 | 18.985 % |
c |     2505        65       75 |   22463   988289  2124986 |    11    50525      650   139435 | 18.985 % |
c |     2628        77       76 |   22463   988289  2124986 |    12    32544      660   167416 | 18.985 % |
c |     2782        77       75 |   22463   988289  2124986 |    12    42544      668   167416 | 18.985 % |
c |     2975        77       73 |   22463   988289  2124986 |    12    52544      668   167416 | 18.985 % |
c |     3148        79       73 |   22463   988289  2124986 |    13    31945      668   198015 | 18.985 % |
c |     3225        79       74 |   22463   988289  2124986 |    13    41945      674   198015 | 18.985 % |
c |     3412        79       73 |   22463   988289  2124986 |    13    51945      674   198015 | 18.985 % |
c |     3605        79       72 |   22463   988289  2124986 |    13    61945      676   198015 | 18.985 % |
c |     3710        82       72 |   22463   988289  2124986 |    14    38718      680   231242 | 18.985 % |
c |     3818        82       73 |   22463   988289  2124986 |    14    48718      684   231242 | 18.985 % |
c |     4007        82       72 |   22463   988289  2124986 |    14    58718      684   231242 | 18.985 % |
c |     4201        82       71 |   22463   988289  2124986 |    14    68718      686   231242 | 18.985 % |
c |     4271        86       72 |   22463   988289  2124986 |    15    42914      688   267046 | 18.985 % |
c |     4348        86       73 |   22463   988289  2124986 |    15    52914      692   267046 | 18.985 % |
c |     4535        86       72 |   22463   988289  2124986 |    15    62914      692   267046 | 18.985 % |
c |     4726        86       71 |   22463   988289  2124986 |    15    72914      696   267046 | 18.985 % |
c |     4809        92       72 |   22463   988289  2124986 |    16    44505      698   305455 | 18.985 % |
c |     4876        92       73 |   22463   988289  2124986 |    16    54505      698   305455 | 18.985 % |
c |     5039        92       73 |   22463   988289  2124986 |    16    64505      702   305455 | 18.985 % |
c |     5218        95       72 |   22463   988289  2124986 |    16    74505      702   305455 | 18.985 % |
c |     5363       104       72 |   22463   988289  2124986 |    17    43467      704   346493 | 18.985 % |
c |     5388       104       74 |   22463   988289  2124986 |    17    53467      704   346493 | 18.985 % |
c |     5492       104       74 |   22463   988289  2124986 |    17    63467      706   346493 | 18.985 % |
c |     5667       104       74 |   22463   988289  2124986 |    17    73467      708   346493 | 18.985 % |
c |     5846       104       73 |   22463   988289  2124986 |    17    83467      708   346493 | 18.985 % |
c |     5927       107       74 |   22463   988289  2124986 |    18    49836      712   390124 | 18.985 % |
c |     5971       107       75 |   22463   988289  2124986 |    18    59836      712   390124 | 18.985 % |
c |     6065       107       75 |   22463   988289  2124986 |    18    69836      712   390124 | 18.985 % |
c |     6202       107       75 |   22463   988289  2124986 |    18    79836      714   390124 | 18.985 % |
c |     6347       107       75 |   22463   988289  2124986 |    18    89836      714   390124 | 18.985 % |
c |     6401       118       76 |   22463   988289  2124986 |    19    53628      716   436332 | 18.985 % |
c |     6422       118       77 |   22463   988289  2124986 |    19    63628      718   436332 | 18.985 % |
c |     6467       118       78 |   22463   988289  2124986 |    19    73628      718   436332 | 18.985 % |
c |     6540       118       79 |   22463   988289  2124986 |    19    83628      718   436332 | 18.985 % |
[118.997] Executed solver; result: UNSAT.
[118.997] 
[118.997] *************************************
[118.997] * * *   M a k e s p a n    11   * * *
[118.997] *************************************
[119.001] Computed next depth properties: array size of 133.
[119.005] Instantiated 12,346 transitional clauses.
[119.022] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[119.022] Instantiated 260,821 universal clauses.
[119.022] Instantiated and added clauses for a total of 1,788,618 clauses.
[119.022] The encoding contains a total of 33,915 distinct variables.
[119.022] Attempting solve with solver <glucose4> ...
c 133 assumptions
c |     6572       164       80 |   27902  1198008  2569778 |    19    93626      762   436332 | 17.727 % |
c |     6572       164       82 |   27902  1198008  2569778 |    20    54822      772   485136 | 17.727 % |
c |     6575       164       83 |   27867  1197254  2567295 |    20    64814      816   485136 | 17.830 % |
c last restart ## conflicts  :  2037 242 
[123.392] Executed solver; result: SAT.
[123.392] Solver returned SAT; a solution has been found at makespan 11.
48
solution 498 1
18 34 32 31 324 21 19 20 57 7 23 116 29 30 287 28 273 9 25 142 10 12 157 27 14 263 24 208 429 4 92 26 11 412 13 468 8 213 22 5 387 16 15 17 498 3 362 6 
[123.392] Exiting.

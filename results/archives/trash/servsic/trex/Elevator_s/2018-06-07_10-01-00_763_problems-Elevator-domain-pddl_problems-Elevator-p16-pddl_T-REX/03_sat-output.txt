Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.023] Processed problem encoding.
[0.026] Calculated possible fact changes of composite elements.
[0.026] Initialized instantiation procedure.
[0.026] 
[0.026] *************************************
[0.026] * * *   M a k e s p a n     0   * * *
[0.026] *************************************
[0.026] Instantiated 243 initial clauses.
[0.026] The encoding contains a total of 166 distinct variables.
[0.026] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.026] Executed solver; result: UNSAT.
[0.026] 
[0.026] *************************************
[0.026] * * *   M a k e s p a n     1   * * *
[0.026] *************************************
[0.027] Computed next depth properties: array size of 4.
[0.028] Instantiated 724 transitional clauses.
[0.028] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.028] Instantiated 2,459 universal clauses.
[0.028] Instantiated and added clauses for a total of 3,426 clauses.
[0.028] The encoding contains a total of 819 distinct variables.
[0.028] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.029] Executed solver; result: UNSAT.
[0.029] 
[0.029] *************************************
[0.029] * * *   M a k e s p a n     2   * * *
[0.029] *************************************
[0.029] Computed next depth properties: array size of 7.
[0.033] Instantiated 3,878 transitional clauses.
[0.039] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.039] Instantiated 99,772 universal clauses.
[0.039] Instantiated and added clauses for a total of 107,076 clauses.
[0.039] The encoding contains a total of 2,144 distinct variables.
[0.039] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.039] Executed solver; result: UNSAT.
[0.039] 
[0.039] *************************************
[0.039] * * *   M a k e s p a n     3   * * *
[0.039] *************************************
[0.040] Computed next depth properties: array size of 13.
[0.045] Instantiated 5,731 transitional clauses.
[0.062] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.062] Instantiated 188,681 universal clauses.
[0.062] Instantiated and added clauses for a total of 301,488 clauses.
[0.062] The encoding contains a total of 3,878 distinct variables.
[0.062] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.062] Executed solver; result: UNSAT.
[0.062] 
[0.062] *************************************
[0.062] * * *   M a k e s p a n     4   * * *
[0.062] *************************************
[0.064] Computed next depth properties: array size of 21.
[0.067] Instantiated 5,332 transitional clauses.
[0.087] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.087] Instantiated 217,788 universal clauses.
[0.087] Instantiated and added clauses for a total of 524,608 clauses.
[0.087] The encoding contains a total of 6,112 distinct variables.
[0.087] Attempting solve with solver <glucose4> ...
c 21 assumptions
[0.087] Executed solver; result: UNSAT.
[0.087] 
[0.087] *************************************
[0.087] * * *   M a k e s p a n     5   * * *
[0.087] *************************************
[0.088] Computed next depth properties: array size of 31.
[0.092] Instantiated 6,377 transitional clauses.
[0.114] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.114] Instantiated 247,601 universal clauses.
[0.114] Instantiated and added clauses for a total of 778,586 clauses.
[0.114] The encoding contains a total of 8,911 distinct variables.
[0.114] Attempting solve with solver <glucose4> ...
c 31 assumptions
[0.116] Executed solver; result: UNSAT.
[0.116] 
[0.116] *************************************
[0.116] * * *   M a k e s p a n     6   * * *
[0.116] *************************************
[0.117] Computed next depth properties: array size of 43.
[0.122] Instantiated 7,508 transitional clauses.
[0.143] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.143] Instantiated 278,120 universal clauses.
[0.143] Instantiated and added clauses for a total of 1,064,214 clauses.
[0.143] The encoding contains a total of 12,320 distinct variables.
[0.143] Attempting solve with solver <glucose4> ...
c 43 assumptions
[0.144] Executed solver; result: UNSAT.
[0.144] 
[0.144] *************************************
[0.144] * * *   M a k e s p a n     7   * * *
[0.144] *************************************
[0.146] Computed next depth properties: array size of 57.
[0.150] Instantiated 8,725 transitional clauses.
[0.170] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.170] Instantiated 309,345 universal clauses.
[0.170] Instantiated and added clauses for a total of 1,382,284 clauses.
[0.170] The encoding contains a total of 16,384 distinct variables.
[0.170] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.174] Executed solver; result: UNSAT.
[0.174] 
[0.174] *************************************
[0.174] * * *   M a k e s p a n     8   * * *
[0.174] *************************************
[0.176] Computed next depth properties: array size of 73.
[0.180] Instantiated 10,028 transitional clauses.
[0.201] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.201] Instantiated 341,276 universal clauses.
[0.201] Instantiated and added clauses for a total of 1,733,588 clauses.
[0.201] The encoding contains a total of 21,148 distinct variables.
[0.201] Attempting solve with solver <glucose4> ...
c 73 assumptions
[0.239] Executed solver; result: UNSAT.
[0.239] 
[0.239] *************************************
[0.239] * * *   M a k e s p a n     9   * * *
[0.239] *************************************
[0.241] Computed next depth properties: array size of 91.
[0.245] Instantiated 11,417 transitional clauses.
[0.268] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.268] Instantiated 373,913 universal clauses.
[0.268] Instantiated and added clauses for a total of 2,118,918 clauses.
[0.268] The encoding contains a total of 26,657 distinct variables.
[0.268] Attempting solve with solver <glucose4> ...
c 91 assumptions
[0.509] Executed solver; result: UNSAT.
[0.509] 
[0.509] *************************************
[0.509] * * *   M a k e s p a n    10   * * *
[0.509] *************************************
[0.512] Computed next depth properties: array size of 111.
[0.517] Instantiated 12,892 transitional clauses.
[0.541] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.541] Instantiated 407,256 universal clauses.
[0.541] Instantiated and added clauses for a total of 2,539,066 clauses.
[0.541] The encoding contains a total of 32,956 distinct variables.
[0.541] Attempting solve with solver <glucose4> ...
c 111 assumptions
[1.119] Executed solver; result: UNSAT.
[1.119] 
[1.119] *************************************
[1.119] * * *   M a k e s p a n    11   * * *
[1.119] *************************************
[1.122] Computed next depth properties: array size of 133.
[1.128] Instantiated 14,453 transitional clauses.
[1.156] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.156] Instantiated 441,305 universal clauses.
[1.156] Instantiated and added clauses for a total of 2,994,824 clauses.
[1.156] The encoding contains a total of 40,090 distinct variables.
[1.156] Attempting solve with solver <glucose4> ...
c 133 assumptions
c |      126         0       79 |   33116  1977109  4168330 |     2     6208      126     3789 | 17.394 % |
c |      193        12      103 |   33085  1977109  4168330 |     3    11210      152     8785 | 17.471 % |
c |      377        12       79 |   33085  1977109  4168330 |     4    13811      160    16184 | 17.471 % |
[4.521] Executed solver; result: UNSAT.
[4.521] 
[4.521] *************************************
[4.521] * * *   M a k e s p a n    12   * * *
[4.521] *************************************
[4.525] Computed next depth properties: array size of 157.
[4.532] Instantiated 16,100 transitional clauses.
[4.562] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.562] Instantiated 476,060 universal clauses.
[4.562] Instantiated and added clauses for a total of 3,486,984 clauses.
[4.562] The encoding contains a total of 48,104 distinct variables.
[4.562] Attempting solve with solver <glucose4> ...
c 157 assumptions
c |      484        49       82 |   40177  2362823  4972363 |     5    13909      388    26079 | 16.477 % |
c |      659        49       75 |   40177  2362823  4972363 |     5    23909      442    26079 | 16.477 % |
c |      817        49       73 |   40177  2362823  4972363 |     6    21471      468    38517 | 16.477 % |
c |      971        49       72 |   40177  2362823  4972363 |     7    16459      490    53529 | 16.477 % |
c |     1092        49       73 |   40177  2362823  4972363 |     7    26459      508    53529 | 16.477 % |
c |     1249        49       72 |   40177  2362823  4972363 |     8    18857      510    71131 | 16.477 % |
c |     1350        49       74 |   40177  2362823  4972363 |     8    28857      510    71131 | 16.477 % |
c |     1482        49       74 |   40177  2362823  4972363 |     8    38857      510    71131 | 16.477 % |
c |     1556        49       77 |   40177  2362823  4972363 |     9    28652      514    91336 | 16.477 % |
c |     1599        49       81 |   40177  2362823  4972363 |     9    38652      516    91336 | 16.477 % |
c |     1709        49       81 |   40177  2362823  4972363 |    10    25842      518   114146 | 16.477 % |
c |     1819        49       82 |   40177  2362823  4972363 |    10    35842      518   114146 | 16.477 % |
c |     1971        49       81 |   40177  2362823  4972363 |    10    45842      520   114146 | 16.477 % |
c |     2098        49       81 |   40167  2360914  4968497 |    11    30431      522   139555 | 16.498 % |
c |     2190        49       82 |   40167  2360914  4968497 |    11    40431      522   139555 | 16.498 % |
c |     2363        49       80 |   40157  2360738  4968139 |    11    50428      522   139555 | 16.519 % |
c |     2499        50       80 |   40157  2360738  4968139 |    12    32428      522   167555 | 16.519 % |
c |     2613        50       80 |   40157  2360738  4968139 |    12    42428      522   167555 | 16.519 % |
c |     2780        50       79 |   40157  2360738  4968139 |    12    52428      522   167555 | 16.519 % |
c |     2953        50       77 |   40157  2360738  4968139 |    13    31828      522   198155 | 16.519 % |
c |     3062        54       78 |   40157  2360738  4968139 |    13    41828      522   198155 | 16.519 % |
c |     3225        54       77 |   40157  2360738  4968139 |    13    51828      522   198155 | 16.519 % |
c |     3417        54       76 |   40157  2360738  4968139 |    13    61828      522   198155 | 16.519 % |
c |     3551        59       76 |   40157  2360738  4968139 |    14    38631      522   231352 | 16.519 % |
c |     3683        59       76 |   40157  2360738  4968139 |    14    48631      522   231352 | 16.519 % |
c |     3865        59       75 |   40157  2360738  4968139 |    14    58631      522   231352 | 16.519 % |
c |     4058        59       73 |   40157  2360738  4968139 |    14    68631      522   231352 | 16.519 % |
c |     4153        62       74 |   40157  2360738  4968139 |    15    42832      522   267151 | 16.519 % |
c |     4291        62       74 |   40157  2360738  4968139 |    15    52832      522   267151 | 16.519 % |
c |     4476        63       73 |   40157  2360738  4968139 |    15    62832      522   267151 | 16.519 % |
c |     4670        63       72 |   40157  2360738  4968139 |    15    72832      522   267151 | 16.519 % |
c |     4772        63       73 |   40157  2360738  4968139 |    16    44430      522   305553 | 16.519 % |
c |     4892        64       73 |   40157  2360738  4968139 |    16    54430      522   305553 | 16.519 % |
c |     5073        65       72 |   40157  2360738  4968139 |    16    64430      522   305553 | 16.519 % |
c |     5269        65       72 |   40157  2360738  4968139 |    16    74430      522   305553 | 16.519 % |
c |     5420        65       71 |   40157  2360738  4968139 |    17    43422      522   346561 | 16.519 % |
c |     5455        66       73 |   40157  2360738  4968139 |    17    53422      522   346561 | 16.519 % |
c |     5559        70       73 |   40157  2360738  4968139 |    17    63422      522   346561 | 16.519 % |
c |     5741        70       73 |   40157  2360738  4968139 |    17    73422      522   346561 | 16.519 % |
c |     5937        70       72 |   40157  2360738  4968139 |    17    83422      522   346561 | 16.519 % |
[80.233] Executed solver; result: UNSAT.
[80.233] 
[80.233] *************************************
[80.233] * * *   M a k e s p a n    13   * * *
[80.233] *************************************
[80.237] Computed next depth properties: array size of 183.
[80.245] Instantiated 17,833 transitional clauses.
[80.279] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[80.279] Instantiated 511,521 universal clauses.
[80.279] Instantiated and added clauses for a total of 4,016,338 clauses.
[80.279] The encoding contains a total of 57,043 distinct variables.
[80.279] Attempting solve with solver <glucose4> ...
c 183 assumptions
c |     5987       147       73 |   48159  2782513  5846961 |    18    49820      596   390161 | 15.573 % |
c |     5998       175       75 |   48114  2781112  5844131 |    18    59819      686   390161 | 15.652 % |
c |     6005       175       76 |   47975  2780310  5842513 |    18    69814      702   390161 | 15.895 % |
c |     6005       175       78 |   47975  2780310  5842513 |    18    79814      712   390161 | 15.895 % |
c |     6006       189       79 |   47932  2777465  5836771 |    18    89812      740   390161 | 15.971 % |
c |     6008       227       81 |   47856  2776743  5835309 |    19    53629      754   436342 | 16.104 % |
c |     6008       227       83 |   47856  2776743  5835309 |    19    63629      768   436342 | 16.104 % |
c |     6008       227       84 |   47856  2776743  5835309 |    19    73629      772   436342 | 16.104 % |
c |     6008       227       86 |   47825  2775141  5832077 |    19    83627      780   436342 | 16.158 % |
c |     6008       227       88 |   47825  2775141  5832077 |    19    93627      788   436342 | 16.158 % |
c |     6008       227       89 |   47825  2775141  5832077 |    20    54844      794   485125 | 16.158 % |
c |     6008       227       91 |   47780  2773946  5829665 |    20    64843      804   485125 | 16.237 % |
c |     6008       227       93 |   47780  2773946  5829665 |    20    74843      808   485125 | 16.237 % |
c |     6008       227       94 |   47780  2773946  5829665 |    20    84843      808   485125 | 16.237 % |
c |     6008       227       96 |   47780  2773946  5829665 |    20    94843      808   485125 | 16.237 % |
c |     6008       227       98 |   47780  2773946  5829665 |    21    53447      808   536521 | 16.237 % |
c |     6008       227       99 |   47780  2773946  5829665 |    21    63447      808   536521 | 16.237 % |
c |     6008       227      101 |   47780  2773946  5829665 |    21    73447      808   536521 | 16.237 % |
c |     6008       227      103 |   47780  2773946  5829665 |    21    83447      812   536521 | 16.237 % |
c |     6008       227      104 |   47780  2773946  5829665 |    21    93447      814   536521 | 16.237 % |
c |     6008       227      106 |   47780  2773946  5829665 |    21   103447      824   536521 | 16.237 % |
c |     6012       227      108 |   47728  2773552  5828867 |    22    59431      834   590534 | 16.328 % |
c |     6012       227      109 |   47718  2773552  5828867 |    22    69430      838   590534 | 16.346 % |
c |     6012       227      111 |   47718  2773552  5828867 |    22    79430      838   590534 | 16.346 % |
c |     6014       227      113 |   47703  2772543  5826819 |    22    89429      842   590534 | 16.372 % |
c |     6023       235      114 |   47703  2772543  5826819 |    22    99429      854   590534 | 16.372 % |
c |     6029       281      116 |   47703  2772543  5826819 |    22   109429      872   590534 | 16.372 % |
c |     6031       299      117 |   47693  2772377  5826481 |    23    62840      878   647122 | 16.390 % |
c |     6031       299      119 |   47693  2772377  5826481 |    23    72840      884   647122 | 16.390 % |
c |     6031       299      121 |   47693  2772377  5826481 |    23    82840      884   647122 | 16.390 % |
c |     6036       299      122 |   47693  2772377  5826481 |    23    92840      892   647122 | 16.390 % |
c |     6042       299      124 |   47683  2772217  5826155 |    23   102839      898   647122 | 16.407 % |
c |     6042       320      125 |   47668  2772217  5826155 |    23   112838      908   647122 | 16.434 % |
c |     6042       320      127 |   47668  2772217  5826155 |    24    63648      910   706312 | 16.434 % |
c |     6055       320      128 |   47668  2772217  5826155 |    24    73648      924   706312 | 16.434 % |
c |     6058       320      130 |   47658  2771785  5825277 |    24    83647      934   706312 | 16.451 % |
c |     6071       320      131 |   47658  2771785  5825277 |    24    93647      946   706312 | 16.451 % |
c |     6079       320      133 |   47658  2771785  5825277 |    24   103647      952   706312 | 16.451 % |
c |     6090       354      134 |   47658  2771785  5825277 |    24   113647      958   706312 | 16.451 % |
c |     6111       357      135 |   47658  2771785  5825277 |    24   123647      962   706312 | 16.451 % |
c |     6112       367      137 |   47658  2771785  5825277 |    25    71867      968   768092 | 16.451 % |
c |     6113       367      139 |   47658  2771785  5825277 |    25    81867      978   768092 | 16.451 % |
c |     6115       402      140 |   47658  2771785  5825277 |    25    91867      982   768092 | 16.451 % |
c |     6131       425      141 |   47658  2771785  5825277 |    25   101867      992   768092 | 16.451 % |
c |     6131       425      143 |   47658  2771785  5825277 |    25   111867      992   768092 | 16.451 % |
c |     6149       425      144 |   47658  2771785  5825277 |    25   121867      996   768092 | 16.451 % |
c |     6164       425      146 |   47658  2771785  5825277 |    26    67456     1002   832503 | 16.451 % |
c |     6164       425      147 |   47658  2771785  5825277 |    26    77456     1006   832503 | 16.451 % |
c |     6172       476      149 |   47648  2771621  5824943 |    26    87455     1012   832503 | 16.469 % |
c |     6189       476      150 |   47648  2771621  5824943 |    26    97455     1022   832503 | 16.469 % |
c |     6202       476      151 |   47648  2771621  5824943 |    26   107455     1032   832503 | 16.469 % |
c |     6232       482      152 |   47648  2771621  5824943 |    26   117455     1034   832503 | 16.469 % |
c |     6260       523      153 |   47648  2771621  5824943 |    26   127455     1036   832503 | 16.469 % |
c |     6288       549      154 |   47648  2771621  5824943 |    27    70489     1052   899469 | 16.469 % |
c |     6293       574      155 |   47648  2771621  5824943 |    27    80489     1056   899469 | 16.469 % |
c |     6293       574      157 |   47648  2771621  5824943 |    27    90489     1056   899469 | 16.469 % |
c |     6301       578      158 |   47648  2771621  5824943 |    27   100489     1094   899469 | 16.469 % |
c |     6301       608      160 |   47648  2771621  5824943 |    27   110489     1096   899469 | 16.469 % |
c |     6303       608      161 |   47633  2771353  5824399 |    27   120488     1098   899469 | 16.495 % |
c |     6316       608      163 |   47612  2771353  5824399 |    27   130487     1102   899469 | 16.532 % |
c |     6332       608      164 |   47612  2771353  5824399 |    28    70866     1104   969090 | 16.532 % |
c |     6336       608      165 |   47612  2771353  5824399 |    28    80866     1106   969090 | 16.532 % |
c |     6339       608      167 |   47612  2771353  5824399 |    28    90866     1112   969090 | 16.532 % |
c |     6341       608      168 |   47612  2771353  5824399 |    28   100866     1140   969090 | 16.532 % |
c |     6347       608      170 |   47576  2770278  5822225 |    28   110865     1156   969090 | 16.595 % |
c |     6353       631      171 |   47576  2770278  5822225 |    28   120865     1156   969090 | 16.595 % |
c |     6359       631      172 |   47576  2770278  5822225 |    28   130865     1156   969090 | 16.595 % |
c |     6372       631      174 |   47576  2770278  5822225 |    28   140865     1160   969090 | 16.595 % |
c |     6372       631      175 |   47576  2770278  5822225 |    29    78673     1160  1041282 | 16.595 % |
c |     6372       631      177 |   47576  2770278  5822225 |    29    88673     1166  1041282 | 16.595 % |
c |     6377       644      178 |   47576  2770278  5822225 |    29    98673     1168  1041282 | 16.595 % |
c |     6382       644      180 |   47576  2770278  5822225 |    29   108673     1170  1041282 | 16.595 % |
c |     6382       644      181 |   47531  2769297  5820247 |    29   118672     1186  1041282 | 16.674 % |
c |     6386       644      183 |   47531  2769297  5820247 |    29   128672     1186  1041282 | 16.674 % |
c |     6388       701      184 |   47531  2769297  5820247 |    29   138672     1190  1041282 | 16.674 % |
c |     6390       701      186 |   47531  2769297  5820247 |    29   148672     1196  1041282 | 16.674 % |
c |     6390       701      187 |   47531  2769297  5820247 |    30    83898     1200  1116056 | 16.674 % |
c |     6390       701      189 |   47531  2769297  5820247 |    30    93898     1200  1116056 | 16.674 % |
c |     6390       701      190 |   47531  2769297  5820247 |    30   103898     1200  1116056 | 16.674 % |
c |     6395       701      192 |   47531  2769297  5820247 |    30   113898     1202  1116056 | 16.674 % |
c |     6400       701      193 |   47531  2769297  5820247 |    30   123898     1204  1116056 | 16.674 % |
c |     6418       715      194 |   47531  2769297  5820247 |    30   133898     1232  1116056 | 16.674 % |
c |     6441       736      195 |   47531  2769297  5820247 |    30   143898     1242  1116056 | 16.674 % |
c |     6469       744      196 |   47531  2769297  5820247 |    30   153898     1246  1116056 | 16.674 % |
c |     6473       744      197 |   47531  2769297  5820247 |    31    86502     1246  1193452 | 16.674 % |
c |     6476       744      199 |   47503  2768704  5819049 |    31    96501     1246  1193452 | 16.723 % |
c |     6491       772      200 |   47503  2768704  5819049 |    31   106501     1246  1193452 | 16.723 % |
c |     6510       812      201 |   47503  2768704  5819049 |    31   116501     1256  1193452 | 16.723 % |
c |     6535       836      201 |   47503  2768704  5819049 |    31   126501     1256  1193452 | 16.723 % |
c |     6546       855      203 |   47503  2768704  5819049 |    31   136501     1262  1193452 | 16.723 % |
c |     6579       905      203 |   47503  2768704  5819049 |    31   146501     1262  1193452 | 16.723 % |
c |     6623       930      203 |   47503  2768704  5819049 |    31   156501     1262  1193452 | 16.723 % |
Interrupt signal (15) received.

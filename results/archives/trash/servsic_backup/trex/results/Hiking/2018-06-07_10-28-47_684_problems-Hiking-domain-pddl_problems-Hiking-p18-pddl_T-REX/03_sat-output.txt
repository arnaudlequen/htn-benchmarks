Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.003] Parsed head comment information.
[0.403] Processed problem encoding.
[0.514] Calculated possible fact changes of composite elements.
[0.514] Initialized instantiation procedure.
[0.514] 
[0.514] *************************************
[0.514] * * *   M a k e s p a n     0   * * *
[0.514] *************************************
[0.516] Instantiated 531 initial clauses.
[0.516] The encoding contains a total of 363 distinct variables.
[0.516] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.516] Executed solver; result: UNSAT.
[0.516] 
[0.516] *************************************
[0.516] * * *   M a k e s p a n     1   * * *
[0.516] *************************************
[0.516] Computed next depth properties: array size of 3.
[0.517] Instantiated 82 transitional clauses.
[0.519] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.519] Instantiated 685 universal clauses.
[0.519] Instantiated and added clauses for a total of 1,298 clauses.
[0.519] The encoding contains a total of 535 distinct variables.
[0.519] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.519] Executed solver; result: UNSAT.
[0.519] 
[0.519] *************************************
[0.519] * * *   M a k e s p a n     2   * * *
[0.519] *************************************
[0.524] Computed next depth properties: array size of 4.
[0.538] Instantiated 48,659 transitional clauses.
[1.185] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.185] Instantiated 30,166,524 universal clauses.
[1.185] Instantiated and added clauses for a total of 30,216,481 clauses.
[1.185] The encoding contains a total of 18,200 distinct variables.
[1.185] Attempting solve with solver <glucose4> ...
c 4 assumptions
[1.187] Executed solver; result: UNSAT.
[1.187] 
[1.187] *************************************
[1.187] * * *   M a k e s p a n     3   * * *
[1.187] *************************************
[1.200] Computed next depth properties: array size of 14.
[1.287] Instantiated 152,889 transitional clauses.
[2.617] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.617] Instantiated 59,902,310 universal clauses.
[2.617] Instantiated and added clauses for a total of 90,271,680 clauses.
[2.617] The encoding contains a total of 41,334 distinct variables.
[2.617] Attempting solve with solver <glucose4> ...
c 14 assumptions
[2.617] Executed solver; result: UNSAT.
[2.617] 
[2.617] *************************************
[2.617] * * *   M a k e s p a n     4   * * *
[2.617] *************************************
[2.636] Computed next depth properties: array size of 32.
[2.733] Instantiated 170,664 transitional clauses.
[4.158] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.158] Instantiated 61,389,200 universal clauses.
[4.158] Instantiated and added clauses for a total of 151,831,544 clauses.
[4.158] The encoding contains a total of 67,461 distinct variables.
[4.158] Attempting solve with solver <glucose4> ...
c 32 assumptions
[4.160] Executed solver; result: UNSAT.
[4.160] 
[4.160] *************************************
[4.160] * * *   M a k e s p a n     5   * * *
[4.160] *************************************
[4.185] Computed next depth properties: array size of 52.
[4.291] Instantiated 170,089 transitional clauses.
[5.806] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[5.806] Instantiated 62,772,047 universal clauses.
[5.806] Instantiated and added clauses for a total of 214,773,680 clauses.
[5.806] The encoding contains a total of 96,007 distinct variables.
[5.806] Attempting solve with solver <glucose4> ...
c 52 assumptions
[5.809] Executed solver; result: UNSAT.
[5.809] 
[5.809] *************************************
[5.809] * * *   M a k e s p a n     6   * * *
[5.809] *************************************
[5.841] Computed next depth properties: array size of 74.
[5.951] Instantiated 168,732 transitional clauses.
[7.552] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[7.552] Instantiated 64,058,676 universal clauses.
[7.552] Instantiated and added clauses for a total of 279,001,088 clauses.
[7.552] The encoding contains a total of 126,678 distinct variables.
[7.552] Attempting solve with solver <glucose4> ...
c 74 assumptions
[7.556] Executed solver; result: UNSAT.
[7.556] 
[7.556] *************************************
[7.556] * * *   M a k e s p a n     7   * * *
[7.556] *************************************
[7.594] Computed next depth properties: array size of 98.
[7.715] Instantiated 166,030 transitional clauses.
[9.395] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[9.395] Instantiated 65,270,404 universal clauses.
[9.395] Instantiated and added clauses for a total of 344,437,522 clauses.
[9.395] The encoding contains a total of 158,391 distinct variables.
[9.395] Attempting solve with solver <glucose4> ...
c 98 assumptions
[9.400] Executed solver; result: UNSAT.
[9.400] 
[9.400] *************************************
[9.400] * * *   M a k e s p a n     8   * * *
[9.400] *************************************
[9.442] Computed next depth properties: array size of 108.
[9.564] Instantiated 155,392 transitional clauses.
[11.334] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[11.334] Instantiated 66,425,096 universal clauses.
[11.334] Instantiated and added clauses for a total of 411,018,010 clauses.
[11.334] The encoding contains a total of 191,653 distinct variables.
[11.334] Attempting solve with solver <glucose4> ...
c 108 assumptions
[11.343] Executed solver; result: UNSAT.
[11.343] 
[11.343] *************************************
[11.343] * * *   M a k e s p a n     9   * * *
[11.343] *************************************
[11.388] Computed next depth properties: array size of 118.
[11.517] Instantiated 159,182 transitional clauses.
[13.344] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[13.344] Instantiated 67,579,788 universal clauses.
[13.344] Instantiated and added clauses for a total of 478,756,980 clauses.
[13.344] The encoding contains a total of 226,820 distinct variables.
[13.344] Attempting solve with solver <glucose4> ...
c 118 assumptions
[13.462] Executed solver; result: UNSAT.
[13.462] 
[13.462] *************************************
[13.462] * * *   M a k e s p a n    10   * * *
[13.462] *************************************
[13.510] Computed next depth properties: array size of 128.
[13.639] Instantiated 162,972 transitional clauses.
[15.536] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[15.536] Instantiated 68,734,480 universal clauses.
[15.536] Instantiated and added clauses for a total of 547,654,432 clauses.
[15.536] The encoding contains a total of 263,892 distinct variables.
[15.536] Attempting solve with solver <glucose4> ...
c 128 assumptions
[15.554] Executed solver; result: UNSAT.
[15.554] 
[15.554] *************************************
[15.554] * * *   M a k e s p a n    11   * * *
[15.554] *************************************
[15.607] Computed next depth properties: array size of 138.
[15.743] Instantiated 166,762 transitional clauses.
[17.715] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[17.715] Instantiated 69,889,172 universal clauses.
[17.715] Instantiated and added clauses for a total of 617,710,366 clauses.
[17.715] The encoding contains a total of 302,869 distinct variables.
[17.715] Attempting solve with solver <glucose4> ...
c 138 assumptions
[17.743] Executed solver; result: UNSAT.
[17.743] 
[17.743] *************************************
[17.743] * * *   M a k e s p a n    12   * * *
[17.743] *************************************
[17.799] Computed next depth properties: array size of 148.
[17.938] Instantiated 170,552 transitional clauses.
[19.983] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[19.983] Instantiated 71,043,864 universal clauses.
[19.983] Instantiated and added clauses for a total of 688,924,782 clauses.
[19.983] The encoding contains a total of 343,751 distinct variables.
[19.983] Attempting solve with solver <glucose4> ...
c 148 assumptions
c last restart ## conflicts  :  32 432 
[20.399] Executed solver; result: SAT.
[20.399] Solver returned SAT; a solution has been found at makespan 12.
66
solution 8778 1
8778 45 28 43 29 1657 1656 1655 1653 1654 44 10 11 28 109 92 107 93 1662 1659 1658 1660 1661 108 74 75 92 173 156 171 157 1665 1663 1666 1664 1667 172 138 139 156 238 218 235 219 1671 1670 1668 1672 1669 236 205 206 218 302 282 299 283 1676 1674 1675 1673 1677 300 269 270 282 
[20.403] Exiting.

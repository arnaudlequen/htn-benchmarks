Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.002] Parsed head comment information.
[0.221] Processed problem encoding.
[0.269] Calculated possible fact changes of composite elements.
[0.269] Initialized instantiation procedure.
[0.269] 
[0.269] *************************************
[0.269] * * *   M a k e s p a n     0   * * *
[0.269] *************************************
[0.270] Instantiated 429 initial clauses.
[0.270] The encoding contains a total of 294 distinct variables.
[0.270] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.271] Executed solver; result: UNSAT.
[0.271] 
[0.271] *************************************
[0.271] * * *   M a k e s p a n     1   * * *
[0.271] *************************************
[0.271] Computed next depth properties: array size of 3.
[0.271] Instantiated 67 transitional clauses.
[0.273] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.273] Instantiated 554 universal clauses.
[0.273] Instantiated and added clauses for a total of 1,050 clauses.
[0.273] The encoding contains a total of 435 distinct variables.
[0.273] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.273] Executed solver; result: UNSAT.
[0.273] 
[0.273] *************************************
[0.273] * * *   M a k e s p a n     2   * * *
[0.273] *************************************
[0.275] Computed next depth properties: array size of 4.
[0.283] Instantiated 26,250 transitional clauses.
[0.489] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.489] Instantiated 8,725,487 universal clauses.
[0.489] Instantiated and added clauses for a total of 8,752,787 clauses.
[0.489] The encoding contains a total of 10,050 distinct variables.
[0.489] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.492] Executed solver; result: UNSAT.
[0.492] 
[0.492] *************************************
[0.492] * * *   M a k e s p a n     3   * * *
[0.492] *************************************
[0.499] Computed next depth properties: array size of 14.
[0.538] Instantiated 73,395 transitional clauses.
[0.955] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.955] Instantiated 17,407,992 universal clauses.
[0.955] Instantiated and added clauses for a total of 26,234,174 clauses.
[0.955] The encoding contains a total of 22,649 distinct variables.
[0.955] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.955] Executed solver; result: UNSAT.
[0.955] 
[0.955] *************************************
[0.955] * * *   M a k e s p a n     4   * * *
[0.955] *************************************
[0.965] Computed next depth properties: array size of 32.
[1.006] Instantiated 80,662 transitional clauses.
[1.526] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.526] Instantiated 18,029,294 universal clauses.
[1.526] Instantiated and added clauses for a total of 44,344,130 clauses.
[1.526] The encoding contains a total of 37,370 distinct variables.
[1.526] Attempting solve with solver <glucose4> ...
c 32 assumptions
[1.527] Executed solver; result: UNSAT.
[1.527] 
[1.527] *************************************
[1.527] * * *   M a k e s p a n     5   * * *
[1.527] *************************************
[1.541] Computed next depth properties: array size of 52.
[1.587] Instantiated 81,886 transitional clauses.
[2.078] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.078] Instantiated 18,619,142 universal clauses.
[2.078] Instantiated and added clauses for a total of 63,045,158 clauses.
[2.078] The encoding contains a total of 53,802 distinct variables.
[2.078] Attempting solve with solver <glucose4> ...
c 52 assumptions
[2.080] Executed solver; result: UNSAT.
[2.080] 
[2.080] *************************************
[2.080] * * *   M a k e s p a n     6   * * *
[2.080] *************************************
[2.098] Computed next depth properties: array size of 74.
[2.149] Instantiated 82,630 transitional clauses.
[2.685] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.685] Instantiated 19,178,976 universal clauses.
[2.685] Instantiated and added clauses for a total of 82,306,764 clauses.
[2.685] The encoding contains a total of 71,767 distinct variables.
[2.685] Attempting solve with solver <glucose4> ...
c 74 assumptions
[2.687] Executed solver; result: UNSAT.
[2.687] 
[2.687] *************************************
[2.687] * * *   M a k e s p a n     7   * * *
[2.687] *************************************
[2.709] Computed next depth properties: array size of 98.
[2.764] Instantiated 82,698 transitional clauses.
[3.322] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.322] Instantiated 19,714,626 universal clauses.
[3.322] Instantiated and added clauses for a total of 102,104,088 clauses.
[3.322] The encoding contains a total of 90,740 distinct variables.
[3.322] Attempting solve with solver <glucose4> ...
c 98 assumptions
[3.325] Executed solver; result: UNSAT.
[3.325] 
[3.325] *************************************
[3.325] * * *   M a k e s p a n     8   * * *
[3.325] *************************************
[3.348] Computed next depth properties: array size of 108.
[3.406] Instantiated 78,956 transitional clauses.
[4.000] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.000] Instantiated 20,231,608 universal clauses.
[4.000] Instantiated and added clauses for a total of 122,414,652 clauses.
[4.000] The encoding contains a total of 110,694 distinct variables.
[4.000] Attempting solve with solver <glucose4> ...
c 108 assumptions
[4.005] Executed solver; result: UNSAT.
[4.005] 
[4.005] *************************************
[4.005] * * *   M a k e s p a n     9   * * *
[4.005] *************************************
[4.031] Computed next depth properties: array size of 118.
[4.089] Instantiated 81,476 transitional clauses.
[4.707] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.707] Instantiated 20,748,590 universal clauses.
[4.707] Instantiated and added clauses for a total of 143,244,718 clauses.
[4.707] The encoding contains a total of 131,918 distinct variables.
[4.707] Attempting solve with solver <glucose4> ...
c 118 assumptions
[4.763] Executed solver; result: UNSAT.
[4.763] 
[4.763] *************************************
[4.763] * * *   M a k e s p a n    10   * * *
[4.763] *************************************
[4.790] Computed next depth properties: array size of 128.
[4.855] Instantiated 83,996 transitional clauses.
[5.506] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[5.506] Instantiated 21,265,572 universal clauses.
[5.506] Instantiated and added clauses for a total of 164,594,286 clauses.
[5.506] The encoding contains a total of 154,412 distinct variables.
[5.506] Attempting solve with solver <glucose4> ...
c 128 assumptions
[5.522] Executed solver; result: UNSAT.
[5.522] 
[5.522] *************************************
[5.522] * * *   M a k e s p a n    11   * * *
[5.522] *************************************
[5.552] Computed next depth properties: array size of 138.
[5.614] Instantiated 86,516 transitional clauses.
[6.296] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[6.296] Instantiated 21,782,554 universal clauses.
[6.296] Instantiated and added clauses for a total of 186,463,356 clauses.
[6.296] The encoding contains a total of 178,176 distinct variables.
[6.296] Attempting solve with solver <glucose4> ...
c 138 assumptions
c last restart ## conflicts  :  81 689 
[6.519] Executed solver; result: SAT.
[6.519] Solver returned SAT; a solution has been found at makespan 11.
61
solution 4814 1
4814 482 505 484 506 963 964 965 966 486 487 488 505 540 547 532 548 967 969 968 970 534 541 542 547 578 601 580 602 972 974 973 971 582 583 584 601 636 643 628 644 975 977 978 976 630 637 638 643 684 691 676 692 982 981 979 980 678 685 686 691 
[6.521] Exiting.

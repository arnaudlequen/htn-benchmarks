Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.063] Processed problem encoding.
[0.069] Calculated possible fact changes of composite elements.
[0.070] Initialized instantiation procedure.
[0.070] 
[0.070] *************************************
[0.070] * * *   M a k e s p a n     0   * * *
[0.070] *************************************
[0.070] Instantiated 276 initial clauses.
[0.070] The encoding contains a total of 190 distinct variables.
[0.070] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.070] Executed solver; result: UNSAT.
[0.070] 
[0.070] *************************************
[0.070] * * *   M a k e s p a n     1   * * *
[0.070] *************************************
[0.070] Computed next depth properties: array size of 3.
[0.070] Instantiated 42 transitional clauses.
[0.071] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.071] Instantiated 355 universal clauses.
[0.071] Instantiated and added clauses for a total of 673 clauses.
[0.071] The encoding contains a total of 283 distinct variables.
[0.071] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.071] Executed solver; result: UNSAT.
[0.071] 
[0.071] *************************************
[0.071] * * *   M a k e s p a n     2   * * *
[0.071] *************************************
[0.072] Computed next depth properties: array size of 4.
[0.075] Instantiated 7,845 transitional clauses.
[0.096] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.096] Instantiated 770,832 universal clauses.
[0.096] Instantiated and added clauses for a total of 779,350 clauses.
[0.096] The encoding contains a total of 3,240 distinct variables.
[0.096] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.097] Executed solver; result: UNSAT.
[0.097] 
[0.097] *************************************
[0.097] * * *   M a k e s p a n     3   * * *
[0.097] *************************************
[0.099] Computed next depth properties: array size of 14.
[0.106] Instantiated 19,531 transitional clauses.
[0.148] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.148] Instantiated 1,545,576 universal clauses.
[0.148] Instantiated and added clauses for a total of 2,344,457 clauses.
[0.148] The encoding contains a total of 7,348 distinct variables.
[0.148] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.148] Executed solver; result: UNSAT.
[0.148] 
[0.148] *************************************
[0.148] * * *   M a k e s p a n     4   * * *
[0.148] *************************************
[0.150] Computed next depth properties: array size of 32.
[0.159] Instantiated 21,771 transitional clauses.
[0.214] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.214] Instantiated 1,642,505 universal clauses.
[0.214] Instantiated and added clauses for a total of 4,008,733 clauses.
[0.214] The encoding contains a total of 12,422 distinct variables.
[0.214] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.214] Executed solver; result: UNSAT.
[0.214] 
[0.214] *************************************
[0.214] * * *   M a k e s p a n     5   * * *
[0.214] *************************************
[0.218] Computed next depth properties: array size of 52.
[0.229] Instantiated 22,536 transitional clauses.
[0.286] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.286] Instantiated 1,733,396 universal clauses.
[0.286] Instantiated and added clauses for a total of 5,764,665 clauses.
[0.286] The encoding contains a total of 18,204 distinct variables.
[0.286] Attempting solve with solver <glucose4> ...
c 52 assumptions
[0.287] Executed solver; result: UNSAT.
[0.287] 
[0.287] *************************************
[0.287] * * *   M a k e s p a n     6   * * *
[0.287] *************************************
[0.292] Computed next depth properties: array size of 74.
[0.303] Instantiated 22,916 transitional clauses.
[0.366] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.366] Instantiated 1,818,098 universal clauses.
[0.366] Instantiated and added clauses for a total of 7,605,679 clauses.
[0.366] The encoding contains a total of 24,467 distinct variables.
[0.366] Attempting solve with solver <glucose4> ...
c 74 assumptions
[0.367] Executed solver; result: UNSAT.
[0.367] 
[0.367] *************************************
[0.367] * * *   M a k e s p a n     7   * * *
[0.367] *************************************
[0.374] Computed next depth properties: array size of 82.
[0.386] Instantiated 21,720 transitional clauses.
[0.457] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.457] Instantiated 1,897,464 universal clauses.
[0.457] Instantiated and added clauses for a total of 9,524,863 clauses.
[0.457] The encoding contains a total of 31,006 distinct variables.
[0.457] Attempting solve with solver <glucose4> ...
c 82 assumptions
[0.463] Executed solver; result: UNSAT.
[0.463] 
[0.463] *************************************
[0.463] * * *   M a k e s p a n     8   * * *
[0.463] *************************************
[0.470] Computed next depth properties: array size of 90.
[0.482] Instantiated 22,700 transitional clauses.
[0.555] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.555] Instantiated 1,976,830 universal clauses.
[0.555] Instantiated and added clauses for a total of 11,524,393 clauses.
[0.555] The encoding contains a total of 38,043 distinct variables.
[0.555] Attempting solve with solver <glucose4> ...
c 90 assumptions
[0.557] Executed solver; result: UNSAT.
[0.557] 
[0.557] *************************************
[0.557] * * *   M a k e s p a n     9   * * *
[0.557] *************************************
[0.564] Computed next depth properties: array size of 98.
[0.579] Instantiated 23,680 transitional clauses.
[0.660] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.660] Instantiated 2,056,196 universal clauses.
[0.660] Instantiated and added clauses for a total of 13,604,269 clauses.
[0.660] The encoding contains a total of 45,578 distinct variables.
[0.660] Attempting solve with solver <glucose4> ...
c 98 assumptions
c last restart ## conflicts  :  36 218 
[0.750] Executed solver; result: SAT.
[0.750] Solver returned SAT; a solution has been found at makespan 9.
45
solution 1537 1
1537 151 156 140 157 412 413 411 142 154 155 156 179 173 174 175 415 416 414 176 180 181 173 213 207 208 209 418 419 417 210 214 215 207 253 258 242 259 421 422 420 244 256 257 258 
[0.750] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.174] Processed problem encoding.
[0.192] Calculated possible fact changes of composite elements.
[0.192] Initialized instantiation procedure.
[0.192] 
[0.192] *************************************
[0.192] * * *   M a k e s p a n     0   * * *
[0.192] *************************************
[0.193] Instantiated 365 initial clauses.
[0.193] The encoding contains a total of 249 distinct variables.
[0.193] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.193] Executed solver; result: UNSAT.
[0.193] 
[0.193] *************************************
[0.193] * * *   M a k e s p a n     1   * * *
[0.193] *************************************
[0.193] Computed next depth properties: array size of 3.
[0.193] Instantiated 50 transitional clauses.
[0.194] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.194] Instantiated 465 universal clauses.
[0.194] Instantiated and added clauses for a total of 880 clauses.
[0.194] The encoding contains a total of 367 distinct variables.
[0.194] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.194] Executed solver; result: UNSAT.
[0.194] 
[0.194] *************************************
[0.194] * * *   M a k e s p a n     2   * * *
[0.194] *************************************
[0.196] Computed next depth properties: array size of 4.
[0.202] Instantiated 18,335 transitional clauses.
[0.308] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.308] Instantiated 4,113,512 universal clauses.
[0.308] Instantiated and added clauses for a total of 4,132,727 clauses.
[0.308] The encoding contains a total of 7,320 distinct variables.
[0.308] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.308] Executed solver; result: UNSAT.
[0.308] 
[0.308] *************************************
[0.308] * * *   M a k e s p a n     3   * * *
[0.308] *************************************
[0.314] Computed next depth properties: array size of 14.
[0.339] Instantiated 60,749 transitional clauses.
[0.545] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.545] Instantiated 8,097,342 universal clauses.
[0.545] Instantiated and added clauses for a total of 12,290,818 clauses.
[0.545] The encoding contains a total of 17,146 distinct variables.
[0.545] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.545] Executed solver; result: UNSAT.
[0.545] 
[0.545] *************************************
[0.545] * * *   M a k e s p a n     4   * * *
[0.545] *************************************
[0.553] Computed next depth properties: array size of 32.
[0.583] Instantiated 72,054 transitional clauses.
[0.806] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.806] Instantiated 8,375,200 universal clauses.
[0.806] Instantiated and added clauses for a total of 20,738,072 clauses.
[0.806] The encoding contains a total of 28,127 distinct variables.
[0.806] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.807] Executed solver; result: UNSAT.
[0.807] 
[0.807] *************************************
[0.807] * * *   M a k e s p a n     5   * * *
[0.807] *************************************
[0.817] Computed next depth properties: array size of 52.
[0.846] Instantiated 67,214 transitional clauses.
[1.084] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.084] Instantiated 8,587,477 universal clauses.
[1.084] Instantiated and added clauses for a total of 29,392,763 clauses.
[1.084] The encoding contains a total of 38,963 distinct variables.
[1.084] Attempting solve with solver <glucose4> ...
c 52 assumptions
[1.086] Executed solver; result: UNSAT.
[1.086] 
[1.086] *************************************
[1.086] * * *   M a k e s p a n     6   * * *
[1.086] *************************************
[1.097] Computed next depth properties: array size of 58.
[1.127] Instantiated 54,198 transitional clauses.
[1.375] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.375] Instantiated 8,742,868 universal clauses.
[1.375] Instantiated and added clauses for a total of 38,189,829 clauses.
[1.375] The encoding contains a total of 50,151 distinct variables.
[1.375] Attempting solve with solver <glucose4> ...
c 58 assumptions
[1.378] Executed solver; result: UNSAT.
[1.378] 
[1.378] *************************************
[1.378] * * *   M a k e s p a n     7   * * *
[1.378] *************************************
[1.390] Computed next depth properties: array size of 64.
[1.422] Instantiated 55,602 transitional clauses.
[1.678] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.678] Instantiated 8,898,259 universal clauses.
[1.678] Instantiated and added clauses for a total of 47,143,690 clauses.
[1.678] The encoding contains a total of 62,047 distinct variables.
[1.678] Attempting solve with solver <glucose4> ...
c 64 assumptions
[1.681] Executed solver; result: UNSAT.
[1.681] 
[1.681] *************************************
[1.681] * * *   M a k e s p a n     8   * * *
[1.681] *************************************
[1.695] Computed next depth properties: array size of 70.
[1.726] Instantiated 57,006 transitional clauses.
[1.993] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.993] Instantiated 9,053,650 universal clauses.
[1.993] Instantiated and added clauses for a total of 56,254,346 clauses.
[1.993] The encoding contains a total of 74,651 distinct variables.
[1.993] Attempting solve with solver <glucose4> ...
c 70 assumptions
[2.023] Executed solver; result: UNSAT.
[2.023] 
[2.023] *************************************
[2.023] * * *   M a k e s p a n     9   * * *
[2.023] *************************************
[2.037] Computed next depth properties: array size of 76.
[2.068] Instantiated 58,410 transitional clauses.
[2.336] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.336] Instantiated 9,209,041 universal clauses.
[2.336] Instantiated and added clauses for a total of 65,521,797 clauses.
[2.336] The encoding contains a total of 87,963 distinct variables.
[2.336] Attempting solve with solver <glucose4> ...
c 76 assumptions
[2.375] Executed solver; result: UNSAT.
[2.375] 
[2.375] *************************************
[2.375] * * *   M a k e s p a n    10   * * *
[2.375] *************************************
[2.389] Computed next depth properties: array size of 82.
[2.423] Instantiated 59,814 transitional clauses.
[2.696] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.696] Instantiated 9,364,432 universal clauses.
[2.696] Instantiated and added clauses for a total of 74,946,043 clauses.
[2.696] The encoding contains a total of 101,983 distinct variables.
[2.696] Attempting solve with solver <glucose4> ...
c 82 assumptions
c last restart ## conflicts  :  203 250 
[3.254] Executed solver; result: SAT.
[3.254] Solver returned SAT; a solution has been found at makespan 10.
40
solution 3547 1
3547 814 798 811 799 997 996 993 995 994 812 781 782 798 879 860 875 861 999 1001 998 1000 1002 876 848 849 860 942 926 939 927 1005 1004 1003 1007 1006 940 909 910 926 
[3.255] Exiting.

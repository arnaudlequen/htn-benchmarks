Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.101] Processed problem encoding.
[0.111] Calculated possible fact changes of composite elements.
[0.112] Initialized instantiation procedure.
[0.112] 
[0.112] *************************************
[0.112] * * *   M a k e s p a n     0   * * *
[0.112] *************************************
[0.112] Instantiated 295 initial clauses.
[0.112] The encoding contains a total of 202 distinct variables.
[0.112] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.112] Executed solver; result: UNSAT.
[0.112] 
[0.112] *************************************
[0.112] * * *   M a k e s p a n     1   * * *
[0.112] *************************************
[0.112] Computed next depth properties: array size of 3.
[0.113] Instantiated 41 transitional clauses.
[0.113] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.113] Instantiated 376 universal clauses.
[0.113] Instantiated and added clauses for a total of 712 clauses.
[0.113] The encoding contains a total of 299 distinct variables.
[0.113] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.113] Executed solver; result: UNSAT.
[0.113] 
[0.113] *************************************
[0.113] * * *   M a k e s p a n     2   * * *
[0.113] *************************************
[0.115] Computed next depth properties: array size of 4.
[0.118] Instantiated 9,878 transitional clauses.
[0.161] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.161] Instantiated 1,207,531 universal clauses.
[0.161] Instantiated and added clauses for a total of 1,218,121 clauses.
[0.161] The encoding contains a total of 4,048 distinct variables.
[0.161] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.161] Executed solver; result: UNSAT.
[0.161] 
[0.161] *************************************
[0.161] * * *   M a k e s p a n     3   * * *
[0.161] *************************************
[0.163] Computed next depth properties: array size of 14.
[0.174] Instantiated 28,963 transitional clauses.
[0.234] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.234] Instantiated 2,382,100 universal clauses.
[0.234] Instantiated and added clauses for a total of 3,629,184 clauses.
[0.234] The encoding contains a total of 9,391 distinct variables.
[0.234] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.234] Executed solver; result: UNSAT.
[0.234] 
[0.234] *************************************
[0.234] * * *   M a k e s p a n     4   * * *
[0.234] *************************************
[0.237] Computed next depth properties: array size of 32.
[0.250] Instantiated 33,918 transitional clauses.
[0.319] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.319] Instantiated 2,492,062 universal clauses.
[0.319] Instantiated and added clauses for a total of 6,155,164 clauses.
[0.319] The encoding contains a total of 15,656 distinct variables.
[0.319] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.319] Executed solver; result: UNSAT.
[0.319] 
[0.319] *************************************
[0.319] * * *   M a k e s p a n     5   * * *
[0.319] *************************************
[0.324] Computed next depth properties: array size of 52.
[0.338] Instantiated 32,570 transitional clauses.
[0.415] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.415] Instantiated 2,581,216 universal clauses.
[0.415] Instantiated and added clauses for a total of 8,768,950 clauses.
[0.415] The encoding contains a total of 22,141 distinct variables.
[0.415] Attempting solve with solver <glucose4> ...
c 52 assumptions
[0.416] Executed solver; result: UNSAT.
[0.416] 
[0.416] *************************************
[0.416] * * *   M a k e s p a n     6   * * *
[0.416] *************************************
[0.422] Computed next depth properties: array size of 58.
[0.437] Instantiated 27,252 transitional clauses.
[0.519] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.519] Instantiated 2,651,782 universal clauses.
[0.519] Instantiated and added clauses for a total of 11,447,984 clauses.
[0.519] The encoding contains a total of 28,811 distinct variables.
[0.519] Attempting solve with solver <glucose4> ...
c 58 assumptions
[0.520] Executed solver; result: UNSAT.
[0.520] 
[0.520] *************************************
[0.520] * * *   M a k e s p a n     7   * * *
[0.520] *************************************
[0.527] Computed next depth properties: array size of 64.
[0.541] Instantiated 28,188 transitional clauses.
[0.627] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.627] Instantiated 2,722,348 universal clauses.
[0.627] Instantiated and added clauses for a total of 14,198,520 clauses.
[0.627] The encoding contains a total of 35,955 distinct variables.
[0.627] Attempting solve with solver <glucose4> ...
c 64 assumptions
[0.629] Executed solver; result: UNSAT.
[0.629] 
[0.629] *************************************
[0.629] * * *   M a k e s p a n     8   * * *
[0.629] *************************************
[0.636] Computed next depth properties: array size of 70.
[0.653] Instantiated 29,124 transitional clauses.
[0.743] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.743] Instantiated 2,792,914 universal clauses.
[0.743] Instantiated and added clauses for a total of 17,020,558 clauses.
[0.743] The encoding contains a total of 43,573 distinct variables.
[0.743] Attempting solve with solver <glucose4> ...
c 70 assumptions
[0.757] Executed solver; result: UNSAT.
[0.757] 
[0.757] *************************************
[0.757] * * *   M a k e s p a n     9   * * *
[0.757] *************************************
[0.765] Computed next depth properties: array size of 76.
[0.780] Instantiated 30,060 transitional clauses.
[0.873] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.873] Instantiated 2,863,480 universal clauses.
[0.873] Instantiated and added clauses for a total of 19,914,098 clauses.
[0.873] The encoding contains a total of 51,665 distinct variables.
[0.873] Attempting solve with solver <glucose4> ...
c 76 assumptions
c last restart ## conflicts  :  16 323 
[0.883] Executed solver; result: SAT.
[0.883] Solver returned SAT; a solution has been found at makespan 9.
37
solution 1965 1
1965 36 23 37 24 603 606 605 604 38 7 8 23 87 67 85 68 608 609 610 607 86 58 59 67 132 119 133 120 612 614 613 611 134 103 104 119 
[0.884] Exiting.

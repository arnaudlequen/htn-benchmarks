Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.003] Parsed head comment information.
[0.168] Processed problem encoding.
[0.261] Calculated possible fact changes of composite elements.
[0.262] Initialized instantiation procedure.
[0.262] 
[0.262] *************************************
[0.262] * * *   M a k e s p a n     0   * * *
[0.262] *************************************
[0.264] Instantiated 7,601 initial clauses.
[0.264] The encoding contains a total of 4,393 distinct variables.
[0.264] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.264] Executed solver; result: UNSAT.
[0.264] 
[0.264] *************************************
[0.264] * * *   M a k e s p a n     1   * * *
[0.264] *************************************
[0.277] Computed next depth properties: array size of 57.
[0.295] Instantiated 25,131 transitional clauses.
[0.317] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.317] Instantiated 155,612 universal clauses.
[0.317] Instantiated and added clauses for a total of 188,344 clauses.
[0.317] The encoding contains a total of 29,233 distinct variables.
[0.317] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.317] Executed solver; result: UNSAT.
[0.317] 
[0.317] *************************************
[0.317] * * *   M a k e s p a n     2   * * *
[0.317] *************************************
[0.354] Computed next depth properties: array size of 102.
[0.424] Instantiated 150,028 transitional clauses.
[0.489] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.489] Instantiated 517,416 universal clauses.
[0.489] Instantiated and added clauses for a total of 855,788 clauses.
[0.489] The encoding contains a total of 105,635 distinct variables.
[0.489] Attempting solve with solver <glucose4> ...
c 102 assumptions
[0.489] Executed solver; result: UNSAT.
[0.489] 
[0.489] *************************************
[0.489] * * *   M a k e s p a n     3   * * *
[0.489] *************************************
[0.556] Computed next depth properties: array size of 178.
[0.721] Instantiated 402,303 transitional clauses.
[0.858] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.858] Instantiated 1,355,150 universal clauses.
[0.858] Instantiated and added clauses for a total of 2,613,241 clauses.
[0.858] The encoding contains a total of 214,591 distinct variables.
[0.858] Attempting solve with solver <glucose4> ...
c 178 assumptions
[0.921] Executed solver; result: UNSAT.
[0.921] 
[0.921] *************************************
[0.921] * * *   M a k e s p a n     4   * * *
[0.921] *************************************
[1.015] Computed next depth properties: array size of 271.
[1.318] Instantiated 564,337 transitional clauses.
[1.614] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.614] Instantiated 2,846,904 universal clauses.
[1.614] Instantiated and added clauses for a total of 6,024,482 clauses.
[1.614] The encoding contains a total of 334,786 distinct variables.
[1.614] Attempting solve with solver <glucose4> ...
c 271 assumptions
c last restart ## conflicts  :  40 825 
[2.087] Executed solver; result: SAT.
[2.087] Solver returned SAT; a solution has been found at makespan 4.
196
solution 1899 1
19 88 7 81 61 132 62 8 20 508 47 111 61 127 62 48 571 63 267 64 481 71 216 63 271 64 72 663 21 433 47 458 48 22 497 861 5 77 61 133 62 6 473 513 69 140 61 129 62 70 582 17 87 61 131 62 18 473 503 37 106 61 127 62 38 562 51 327 52 501 33 312 51 322 52 34 751 29 243 63 268 11 229 12 64 30 475 491 25 240 26 677 11 230 63 267 71 211 72 64 12 475 486 13 232 14 666 17 358 15 356 16 18 868 49 386 15 354 16 50 934 71 213 33 246 34 72 475 870 67 275 33 245 34 68 891 71 216 63 270 64 72 475 862 975 5 77 6 1899 61 128 7 79 35 104 36 8 62 1328 1738 11 84 12 1886 35 103 7 78 19 90 20 8 36 1204 51 115 52 1731 19 88 7 80 8 20 1896 55 121 7 81 61 129 62 8 56 1599 1748 
[2.088] Exiting.


Compact plan:
0: (visit waypoint9)
1: (navigate rover0 waypoint9 waypoint3)
2: (visit waypoint3)
3: (navigate rover0 waypoint3 waypoint30)
4: (visit waypoint30)
5: (navigate rover0 waypoint30 waypoint23)
6: (unvisit waypoint30)
7: (unvisit waypoint3)
8: (unvisit waypoint9)
9: (sample_soil rover0 rover0store waypoint23)
10: (visit waypoint23)
11: (navigate rover0 waypoint23 waypoint30)
12: (visit waypoint30)
13: (navigate rover0 waypoint30 waypoint2)
14: (unvisit waypoint30)
15: (unvisit waypoint23)
16: (communicate_soil_data rover0 general waypoint23 waypoint2 waypoint31)
17: (visit waypoint31)
18: (navigate rover2 waypoint31 waypoint0)
19: (unvisit waypoint31)
20: (sample_soil rover2 rover2store waypoint0)
21: (visit waypoint0)
22: (navigate rover2 waypoint0 waypoint31)
23: (visit waypoint31)
24: (navigate rover2 waypoint31 waypoint14)
25: (unvisit waypoint31)
26: (unvisit waypoint0)
27: (communicate_soil_data rover2 general waypoint0 waypoint14 waypoint31)
28: (visit waypoint10)
29: (navigate rover5 waypoint10 waypoint23)
30: (visit waypoint23)
31: (navigate rover5 waypoint23 waypoint14)
32: (unvisit waypoint23)
33: (unvisit waypoint10)
34: (sample_soil rover5 rover5store waypoint14)
35: (communicate_soil_data rover5 general waypoint14 waypoint31)
36: (visit waypoint2)
37: (navigate rover0 waypoint2 waypoint30)
38: (visit waypoint30)
39: (navigate rover0 waypoint30 waypoint34)
40: (unvisit waypoint30)
41: (unvisit waypoint2)
42: (drop rover0 rover0store)
43: (sample_soil rover0 rover0store waypoint34)
44: (visit waypoint34)
45: (navigate rover0 waypoint34 waypoint30)
46: (visit waypoint30)
47: (navigate rover0 waypoint30 waypoint8)
48: (unvisit waypoint30)
49: (unvisit waypoint34)
50: (communicate_soil_data rover0 general waypoint34 waypoint8 waypoint31)
51: (visit waypoint8)
52: (navigate rover0 waypoint8 waypoint30)
53: (visit waypoint30)
54: (navigate rover0 waypoint30 waypoint18)
55: (unvisit waypoint30)
56: (unvisit waypoint8)
57: (drop rover0 rover0store)
58: (sample_soil rover0 rover0store waypoint18)
59: (visit waypoint18)
60: (navigate rover0 waypoint18 waypoint30)
61: (visit waypoint30)
62: (navigate rover0 waypoint30 waypoint2)
63: (unvisit waypoint30)
64: (unvisit waypoint18)
65: (communicate_soil_data rover0 general waypoint18 waypoint2 waypoint31)
66: (visit waypoint25)
67: (navigate rover3 waypoint25 waypoint16)
68: (unvisit waypoint25)
69: (sample_soil rover3 rover3store waypoint16)
70: (visit waypoint16)
71: (navigate rover3 waypoint16 waypoint25)
72: (visit waypoint25)
73: (navigate rover3 waypoint25 waypoint0)
74: (unvisit waypoint25)
75: (unvisit waypoint16)
76: (communicate_soil_data rover3 general waypoint16 waypoint0 waypoint31)
77: (visit waypoint14)
78: (navigate rover2 waypoint14 waypoint31)
79: (visit waypoint31)
80: (navigate rover2 waypoint31 waypoint5)
81: (visit waypoint5)
82: (navigate rover2 waypoint5 waypoint12)
83: (unvisit waypoint5)
84: (unvisit waypoint31)
85: (unvisit waypoint14)
86: (drop rover2 rover2store)
87: (sample_soil rover2 rover2store waypoint12)
88: (visit waypoint12)
89: (navigate rover2 waypoint12 waypoint5)
90: (unvisit waypoint12)
91: (communicate_soil_data rover2 general waypoint12 waypoint5 waypoint31)
92: (visit waypoint5)
93: (navigate rover2 waypoint5 waypoint31)
94: (visit waypoint31)
95: (navigate rover2 waypoint31 waypoint0)
96: (visit waypoint0)
97: (navigate rover2 waypoint0 waypoint6)
98: (unvisit waypoint0)
99: (unvisit waypoint31)
100: (unvisit waypoint5)
101: (drop rover2 rover2store)
102: (sample_soil rover2 rover2store waypoint6)
103: (visit waypoint6)
104: (navigate rover2 waypoint6 waypoint0)
105: (unvisit waypoint6)
106: (communicate_soil_data rover2 general waypoint6 waypoint0 waypoint31)
107: (visit waypoint8)
108: (navigate rover4 waypoint8 waypoint7)
109: (visit waypoint7)
110: (navigate rover4 waypoint7 waypoint24)
111: (unvisit waypoint7)
112: (unvisit waypoint8)
113: (sample_rock rover4 rover4store waypoint24)
114: (visit waypoint24)
115: (navigate rover4 waypoint24 waypoint7)
116: (visit waypoint7)
117: (navigate rover4 waypoint7 waypoint8)
118: (unvisit waypoint7)
119: (unvisit waypoint24)
120: (communicate_rock_data rover4 general waypoint24 waypoint8 waypoint31)
121: (visit waypoint0)
122: (navigate rover2 waypoint0 waypoint16)
123: (visit waypoint16)
124: (navigate rover2 waypoint16 waypoint33)
125: (unvisit waypoint16)
126: (unvisit waypoint0)
127: (drop rover2 rover2store)
128: (sample_rock rover2 rover2store waypoint33)
129: (visit waypoint33)
130: (navigate rover2 waypoint33 waypoint16)
131: (visit waypoint16)
132: (navigate rover2 waypoint16 waypoint0)
133: (unvisit waypoint16)
134: (unvisit waypoint33)
135: (communicate_rock_data rover2 general waypoint33 waypoint0 waypoint31)
136: (visit waypoint0)
137: (navigate rover2 waypoint0 waypoint31)
138: (visit waypoint31)
139: (navigate rover2 waypoint31 waypoint9)
140: (unvisit waypoint31)
141: (unvisit waypoint0)
142: (drop rover2 rover2store)
143: (sample_rock rover2 rover2store waypoint9)
144: (communicate_rock_data rover2 general waypoint9 waypoint31)
145: (visit waypoint2)
146: (navigate rover0 waypoint2 waypoint30)
147: (unvisit waypoint2)
148: (calibrate rover0 camera3 objective7 waypoint30)
149: (visit waypoint30)
150: (navigate rover0 waypoint30 waypoint3)
151: (visit waypoint3)
152: (navigate rover0 waypoint3 waypoint17)
153: (visit waypoint17)
154: (navigate rover0 waypoint17 waypoint5)
155: (unvisit waypoint17)
156: (unvisit waypoint3)
157: (unvisit waypoint30)
158: (take_image rover0 waypoint5 objective4 camera3 high_res)
159: (communicate_image_data rover0 general objective4 high_res waypoint5 waypoint31)
160: (visit waypoint5)
161: (navigate rover0 waypoint5 waypoint17)
162: (unvisit waypoint5)
163: (calibrate rover0 camera3 objective7 waypoint17)
164: (visit waypoint17)
165: (navigate rover0 waypoint17 waypoint3)
166: (visit waypoint3)
167: (navigate rover0 waypoint3 waypoint9)
168: (visit waypoint9)
169: (navigate rover0 waypoint9 waypoint25)
170: (unvisit waypoint9)
171: (unvisit waypoint3)
172: (unvisit waypoint17)
173: (take_image rover0 waypoint25 objective3 camera3 high_res)
174: (visit waypoint25)
175: (navigate rover0 waypoint25 waypoint9)
176: (unvisit waypoint25)
177: (communicate_image_data rover0 general objective3 high_res waypoint9 waypoint31)
178: (visit waypoint9)
179: (navigate rover0 waypoint9 waypoint3)
180: (visit waypoint3)
181: (navigate rover0 waypoint3 waypoint27)
182: (unvisit waypoint3)
183: (unvisit waypoint9)
184: (calibrate rover0 camera3 objective7 waypoint27)
185: (visit waypoint27)
186: (navigate rover0 waypoint27 waypoint3)
187: (visit waypoint3)
188: (navigate rover0 waypoint3 waypoint30)
189: (visit waypoint30)
190: (navigate rover0 waypoint30 waypoint8)
191: (unvisit waypoint30)
192: (unvisit waypoint3)
193: (unvisit waypoint27)
194: (take_image rover0 waypoint8 objective7 camera3 colour)
195: (communicate_image_data rover0 general objective7 colour waypoint8 waypoint31)



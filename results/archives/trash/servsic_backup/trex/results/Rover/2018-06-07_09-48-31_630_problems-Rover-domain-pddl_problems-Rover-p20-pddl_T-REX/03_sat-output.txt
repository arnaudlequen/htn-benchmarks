Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.012] Parsed head comment information.
[0.664] Processed problem encoding.
[2.194] Calculated possible fact changes of composite elements.
[2.211] Initialized instantiation procedure.
[2.211] 
[2.211] *************************************
[2.211] * * *   M a k e s p a n     0   * * *
[2.211] *************************************
[2.224] Instantiated 51,364 initial clauses.
[2.224] The encoding contains a total of 27,656 distinct variables.
[2.224] Attempting solve with solver <glucose4> ...
c 45 assumptions
[2.224] Executed solver; result: UNSAT.
[2.224] 
[2.224] *************************************
[2.224] * * *   M a k e s p a n     1   * * *
[2.224] *************************************
[2.385] Computed next depth properties: array size of 177.
[2.526] Instantiated 142,868 transitional clauses.
[2.717] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.717] Instantiated 828,100 universal clauses.
[2.717] Instantiated and added clauses for a total of 1,022,332 clauses.
[2.717] The encoding contains a total of 183,119 distinct variables.
[2.717] Attempting solve with solver <glucose4> ...
c 177 assumptions
[2.717] Executed solver; result: UNSAT.
[2.717] 
[2.717] *************************************
[2.717] * * *   M a k e s p a n     2   * * *
[2.717] *************************************
[3.175] Computed next depth properties: array size of 316.
[3.837] Instantiated 1,004,389 transitional clauses.
[4.384] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.384] Instantiated 3,141,982 universal clauses.
[4.384] Instantiated and added clauses for a total of 5,168,703 clauses.
[4.384] The encoding contains a total of 734,770 distinct variables.
[4.384] Attempting solve with solver <glucose4> ...
c 316 assumptions
[4.384] Executed solver; result: UNSAT.
[4.384] 
[4.384] *************************************
[4.384] * * *   M a k e s p a n     3   * * *
[4.384] *************************************
[5.295] Computed next depth properties: array size of 550.
[6.900] Instantiated 2,963,678 transitional clauses.
[8.731] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[8.731] Instantiated 20,896,852 universal clauses.
[8.731] Instantiated and added clauses for a total of 29,029,233 clauses.
[8.731] The encoding contains a total of 1,576,940 distinct variables.
[8.731] Attempting solve with solver <glucose4> ...
c 550 assumptions
[8.732] Executed solver; result: UNSAT.
[8.732] 
[8.732] *************************************
[8.732] * * *   M a k e s p a n     4   * * *
[8.732] *************************************
[10.034] Computed next depth properties: array size of 835.
[12.438] Instantiated 4,439,203 transitional clauses.
[16.718] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[16.718] Instantiated 55,422,172 universal clauses.
[16.718] Instantiated and added clauses for a total of 88,890,608 clauses.
[16.718] The encoding contains a total of 2,514,759 distinct variables.
[16.718] Attempting solve with solver <glucose4> ...
c 835 assumptions
c last restart ## conflicts  :  587 2170 
[53.228] Executed solver; result: SAT.
[53.228] Solver returned SAT; a solution has been found at makespan 4.
632
solution 7340 1
105 1497 5 1401 89 1485 90 6 106 1660 3 1397 89 1486 90 4 2208 111 1627 97 1611 98 112 1744 125 1643 126 2526 97 1609 111 1628 112 98 1656 1724 101 1614 111 1624 112 102 2483 43 1179 44 1688 33 1164 34 2104 5 1529 113 1632 114 6 1656 1685 21 1542 113 1630 114 22 2411 43 1177 44 1654 1664 2531 5 1523 49 1563 50 6 1656 1671 9 1533 49 1562 50 10 2385 5 1144 55 1204 56 6 1654 1698 53 1201 54 2123 5 1403 105 1500 61 1465 62 106 6 1655 1675 2534 55 1205 56 1654 1708 79 1225 55 1202 56 80 2136 107 877 108 1692 39 808 40 1964 13 1407 61 1468 62 14 1655 1718 91 1488 61 1465 62 92 2316 63 317 13 270 61 312 62 14 64 1711 85 346 61 311 62 86 1836 5 1142 43 1181 65 1214 66 44 6 1654 1737 123 1258 65 1211 66 124 2194 59 830 39 810 40 60 1652 1702 77 844 39 808 40 78 1982 43 1181 65 1213 66 44 1654 1732 109 1248 65 1211 66 110 2185 5 1528 111 1625 112 6 1656 1681 17 1540 111 1624 112 18 2402 5 1528 111 1627 97 1607 98 112 6 1656 1729 103 1617 97 1606 98 104 2498 13 1407 61 1470 62 14 1655 2678 109 1504 61 1466 62 110 4017 127 637 59 580 60 128 2664 93 609 94 3175 2647 81 719 29 671 30 82 3325 71 1077 131 1016 83 1097 84 132 72 2685 113 1125 83 1093 84 114 3684 59 1575 97 1605 57 1572 58 98 60 1656 2616 45 1560 57 1574 58 46 4118 39 171 40 2635 77 221 39 169 40 78 2810 59 584 127 633 131 511 132 128 60 1650 2574 23 538 24 3085 43 1183 89 1235 90 44 1654 2596 41 1176 89 1236 90 42 3769 43 1180 47 1187 48 44 1654 2569 21 1154 47 1188 48 22 3743 97 1604 51 1567 52 98 1656 2563 19 1541 51 1569 52 20 4066 53 453 37 427 38 54 2547 11 389 12 2887 131 1015 71 1081 72 132 1653 2658 87 1104 71 1077 72 88 3657 43 1177 5 1143 6 44 1654 2623 49 1191 50 3791 5 1143 49 1192 50 6 1654 2543 9 1148 49 1191 50 10 3712 131 1015 71 1081 87 1105 88 72 132 1653 2702 121 1129 87 1100 88 122 3703 5 1142 43 1178 23 1156 24 44 6 1654 2587 27 1160 23 1158 24 28 3761 43 1179 33 1170 34 44 1654 2694 115 1254 33 1164 34 116 3867 59 201 39 168 53 193 54 40 60 1647 2599 43 175 53 194 54 44 2773 59 830 39 809 40 60 1652 2630 67 834 39 808 40 68 3477 61 1317 105 1362 106 62 7340 47 1305 105 1363 61 1316 62 106 48 6083 87 1348 61 1313 62 88 6905 7275 5 1020 87 1104 71 1082 72 88 6 6577 97 1112 71 1077 72 98 6830 13 269 11 268 12 14 7095 51 302 11 266 12 52 4504 6702 43 553 23 537 24 44 7132 131 514 51 566 52 132 4373 33 547 51 562 52 34 6744 131 1015 71 1078 72 132 7294 5178 45 1045 71 1082 72 46 6829 7077 5294 13 275 127 381 128 14 6722 55 197 56 6998 31 163 32 5004 6660 
[53.241] Exiting.

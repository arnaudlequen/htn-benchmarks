Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.002] Parsed head comment information.
[0.145] Processed problem encoding.
[0.232] Calculated possible fact changes of composite elements.
[0.234] Initialized instantiation procedure.
[0.234] 
[0.234] *************************************
[0.234] * * *   M a k e s p a n     0   * * *
[0.234] *************************************
[0.236] Instantiated 7,900 initial clauses.
[0.236] The encoding contains a total of 4,444 distinct variables.
[0.236] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.236] Executed solver; result: UNSAT.
[0.236] 
[0.236] *************************************
[0.236] * * *   M a k e s p a n     1   * * *
[0.236] *************************************
[0.247] Computed next depth properties: array size of 57.
[0.260] Instantiated 18,705 transitional clauses.
[0.278] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.278] Instantiated 83,189 universal clauses.
[0.278] Instantiated and added clauses for a total of 109,794 clauses.
[0.278] The encoding contains a total of 24,511 distinct variables.
[0.278] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.278] Executed solver; result: UNSAT.
[0.278] 
[0.278] *************************************
[0.278] * * *   M a k e s p a n     2   * * *
[0.278] *************************************
[0.313] Computed next depth properties: array size of 102.
[0.371] Instantiated 112,474 transitional clauses.
[0.420] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.420] Instantiated 345,369 universal clauses.
[0.420] Instantiated and added clauses for a total of 567,637 clauses.
[0.420] The encoding contains a total of 82,552 distinct variables.
[0.420] Attempting solve with solver <glucose4> ...
c 102 assumptions
[0.420] Executed solver; result: UNSAT.
[0.420] 
[0.420] *************************************
[0.420] * * *   M a k e s p a n     3   * * *
[0.420] *************************************
[0.482] Computed next depth properties: array size of 178.
[0.615] Instantiated 294,781 transitional clauses.
[0.736] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.736] Instantiated 1,178,758 universal clauses.
[0.736] Instantiated and added clauses for a total of 2,041,176 clauses.
[0.736] The encoding contains a total of 165,418 distinct variables.
[0.736] Attempting solve with solver <glucose4> ...
c 178 assumptions
[0.791] Executed solver; result: UNSAT.
[0.791] 
[0.791] *************************************
[0.791] * * *   M a k e s p a n     4   * * *
[0.791] *************************************
[0.883] Computed next depth properties: array size of 271.
[1.144] Instantiated 409,597 transitional clauses.
[1.449] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.449] Instantiated 2,981,438 universal clauses.
[1.449] Instantiated and added clauses for a total of 5,432,211 clauses.
[1.449] The encoding contains a total of 260,604 distinct variables.
[1.449] Attempting solve with solver <glucose4> ...
c 271 assumptions
c last restart ## conflicts  :  135 1018 
[2.312] Executed solver; result: SAT.
[2.312] Solver returned SAT; a solution has been found at makespan 4.
194
solution 1855 1
7 406 21 430 22 8 681 49 455 21 423 22 50 856 33 207 34 649 5 183 43 217 44 6 698 17 417 21 425 31 437 32 22 18 639 645 3 404 4 810 19 596 20 672 25 607 19 597 20 26 1030 7 522 21 545 37 559 38 22 8 665 23 550 24 958 37 558 21 538 7 523 8 22 38 641 677 43 567 44 975 59 515 21 482 22 60 658 1061 31 438 21 423 17 418 18 22 32 639 687 51 456 52 863 29 95 30 1067 13 75 29 99 30 14 1091 37 105 29 95 13 77 14 30 38 635 1080 57 118 58 1108 7 232 21 253 22 8 1075 41 270 21 246 22 42 1131 37 328 38 1855 33 323 37 326 23 312 24 38 34 1365 61 287 23 315 24 62 1750 37 325 38 1849 21 304 37 326 23 313 24 38 22 1509 11 295 23 315 24 12 1758 1838 1267 7 234 55 280 56 8 1723 
[2.313] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.012] Processed problem encoding.
[0.014] Calculated possible fact changes of composite elements.
[0.014] Initialized instantiation procedure.
[0.014] 
[0.014] *************************************
[0.014] * * *   M a k e s p a n     0   * * *
[0.014] *************************************
[0.015] Instantiated 907 initial clauses.
[0.015] The encoding contains a total of 553 distinct variables.
[0.015] Attempting solve with solver <glucose4> ...
c 9 assumptions
[0.015] Executed solver; result: UNSAT.
[0.015] 
[0.015] *************************************
[0.015] * * *   M a k e s p a n     1   * * *
[0.015] *************************************
[0.016] Computed next depth properties: array size of 33.
[0.018] Instantiated 1,532 transitional clauses.
[0.021] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.021] Instantiated 5,198 universal clauses.
[0.021] Instantiated and added clauses for a total of 7,637 clauses.
[0.021] The encoding contains a total of 2,053 distinct variables.
[0.021] Attempting solve with solver <glucose4> ...
c 33 assumptions
[0.021] Executed solver; result: UNSAT.
[0.021] 
[0.021] *************************************
[0.021] * * *   M a k e s p a n     2   * * *
[0.021] *************************************
[0.024] Computed next depth properties: array size of 60.
[0.029] Instantiated 6,350 transitional clauses.
[0.035] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.035] Instantiated 17,494 universal clauses.
[0.035] Instantiated and added clauses for a total of 31,481 clauses.
[0.035] The encoding contains a total of 5,592 distinct variables.
[0.035] Attempting solve with solver <glucose4> ...
c 60 assumptions
[0.035] Executed solver; result: UNSAT.
[0.035] 
[0.035] *************************************
[0.035] * * *   M a k e s p a n     3   * * *
[0.035] *************************************
[0.040] Computed next depth properties: array size of 106.
[0.050] Instantiated 14,616 transitional clauses.
[0.059] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.059] Instantiated 34,127 universal clauses.
[0.059] Instantiated and added clauses for a total of 80,224 clauses.
[0.059] The encoding contains a total of 10,549 distinct variables.
[0.059] Attempting solve with solver <glucose4> ...
c 106 assumptions
[0.060] Executed solver; result: UNSAT.
[0.060] 
[0.060] *************************************
[0.060] * * *   M a k e s p a n     4   * * *
[0.060] *************************************
[0.066] Computed next depth properties: array size of 160.
[0.078] Instantiated 19,323 transitional clauses.
[0.090] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.090] Instantiated 57,647 universal clauses.
[0.090] Instantiated and added clauses for a total of 157,194 clauses.
[0.090] The encoding contains a total of 16,645 distinct variables.
[0.090] Attempting solve with solver <glucose4> ...
c 160 assumptions
c last restart ## conflicts  :  27 247 
[0.096] Executed solver; result: SAT.
[0.096] Solver returned SAT; a solution has been found at makespan 4.
79
solution 292 1
3 24 5 29 6 4 95 113 9 50 10 98 13 55 14 112 17 57 13 54 9 49 10 14 18 94 116 128 11 52 12 94 115 9 50 13 55 14 10 121 94 117 129 292 5 81 6 154 13 89 14 253 17 91 13 86 14 18 290 21 75 13 87 5 80 6 14 22 190 258 7 30 5 28 6 8 263 165 226 
[0.096] Exiting.

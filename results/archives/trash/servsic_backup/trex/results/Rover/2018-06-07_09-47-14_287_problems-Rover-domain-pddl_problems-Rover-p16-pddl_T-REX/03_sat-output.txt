Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.004] Parsed head comment information.
[0.237] Processed problem encoding.
[0.447] Calculated possible fact changes of composite elements.
[0.451] Initialized instantiation procedure.
[0.451] 
[0.451] *************************************
[0.451] * * *   M a k e s p a n     0   * * *
[0.451] *************************************
[0.454] Instantiated 13,729 initial clauses.
[0.454] The encoding contains a total of 7,698 distinct variables.
[0.454] Attempting solve with solver <glucose4> ...
c 20 assumptions
[0.454] Executed solver; result: UNSAT.
[0.454] 
[0.454] *************************************
[0.454] * * *   M a k e s p a n     1   * * *
[0.454] *************************************
[0.482] Computed next depth properties: array size of 77.
[0.511] Instantiated 42,064 transitional clauses.
[0.548] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.548] Instantiated 192,765 universal clauses.
[0.548] Instantiated and added clauses for a total of 248,558 clauses.
[0.548] The encoding contains a total of 52,158 distinct variables.
[0.548] Attempting solve with solver <glucose4> ...
c 77 assumptions
[0.548] Executed solver; result: UNSAT.
[0.548] 
[0.548] *************************************
[0.548] * * *   M a k e s p a n     2   * * *
[0.548] *************************************
[0.633] Computed next depth properties: array size of 139.
[0.780] Instantiated 282,389 transitional clauses.
[0.911] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.911] Instantiated 894,541 universal clauses.
[0.911] Instantiated and added clauses for a total of 1,425,488 clauses.
[0.911] The encoding contains a total of 206,391 distinct variables.
[0.911] Attempting solve with solver <glucose4> ...
c 139 assumptions
[0.911] Executed solver; result: UNSAT.
[0.911] 
[0.911] *************************************
[0.911] * * *   M a k e s p a n     3   * * *
[0.911] *************************************
[1.059] Computed next depth properties: array size of 244.
[1.415] Instantiated 818,340 transitional clauses.
[1.743] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.743] Instantiated 3,473,676 universal clauses.
[1.743] Instantiated and added clauses for a total of 5,717,504 clauses.
[1.743] The encoding contains a total of 435,919 distinct variables.
[1.743] Attempting solve with solver <glucose4> ...
c 244 assumptions
c last restart ## conflicts  :  61 2346 
[1.859] Executed solver; result: SAT.
[1.859] Solver returned SAT; a solution has been found at makespan 3.
166
solution 3320 1
23 410 24 880 21 403 22 1204 23 415 24 851 908 57 445 58 1248 11 170 59 220 60 12 864 1517 41 350 42 900 49 358 50 1150 23 412 24 851 887 31 425 32 1215 5 627 6 895 35 663 36 1310 57 753 23 719 24 58 875 7 704 8 1365 5 629 59 682 60 6 853 860 1522 17 567 18 1530 9 558 10 1681 29 812 5 775 6 30 1535 35 818 36 1750 5 771 15 790 16 6 855 1551 1809 33 259 11 245 12 34 1540 49 283 50 1583 11 247 12 849 1544 55 291 56 1593 11 243 33 260 34 12 849 1536 37 266 38 1572 23 411 24 3250 2647 2945 41 125 42 3174 5 89 51 133 52 6 2332 2787 3172 2163 2776 3172 1995 2765 53 601 54 3320 9 555 10 1884 17 566 18 2974 
[1.862] Exiting.

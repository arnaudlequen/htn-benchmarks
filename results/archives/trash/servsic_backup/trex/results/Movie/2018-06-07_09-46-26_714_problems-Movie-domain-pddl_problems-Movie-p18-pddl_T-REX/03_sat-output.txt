Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 32 initial clauses.
[0.001] The encoding contains a total of 21 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 8.
[0.002] Instantiated 231 transitional clauses.
[0.002] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.002] Instantiated 1,406 universal clauses.
[0.002] Instantiated and added clauses for a total of 1,669 clauses.
[0.002] The encoding contains a total of 150 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 8 assumptions
c last restart ## conflicts  :  0 117 
[0.002] Executed solver; result: SAT.
[0.002] Solver returned SAT; a solution has been found at makespan 1.
7
solution 92 1
2 4 26 48 70 92 3 
[0.002] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.004] Parsed head comment information.
[0.243] Processed problem encoding.
[0.252] Calculated possible fact changes of composite elements.
[0.268] Initialized instantiation procedure.
[0.268] 
[0.268] *************************************
[0.268] * * *   M a k e s p a n     0   * * *
[0.268] *************************************
[0.286] Instantiated 52,420 initial clauses.
[0.286] The encoding contains a total of 50,637 distinct variables.
[0.286] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.287] Executed solver; result: UNSAT.
[0.287] 
[0.287] *************************************
[0.287] * * *   M a k e s p a n     1   * * *
[0.287] *************************************
[0.320] Computed next depth properties: array size of 71.
[0.398] Instantiated 261,298 transitional clauses.
[1.107] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.107] Instantiated 11,408,993 universal clauses.
[1.107] Instantiated and added clauses for a total of 11,722,711 clauses.
[1.107] The encoding contains a total of 71,416 distinct variables.
[1.107] Attempting solve with solver <glucose4> ...
c 71 assumptions
c last restart ## conflicts  :  6 7119 
[1.381] Executed solver; result: SAT.
[1.381] Solver returned SAT; a solution has been found at makespan 1.
70
solution 2871 1
145 39 8 40 10 2010 111 603 896 605 960 74 600 2461 602 366 121 597 651 599 415 69 12 686 14 2374 130 720 2542 722 1256 16 597 2550 599 559 3 717 718 719 1751 25 720 2611 722 2216 88 600 2695 602 1564 44 717 2733 719 1121 139 723 2831 725 1724 100 717 2871 719 308 114 4 825 6 
[1.382] Exiting.

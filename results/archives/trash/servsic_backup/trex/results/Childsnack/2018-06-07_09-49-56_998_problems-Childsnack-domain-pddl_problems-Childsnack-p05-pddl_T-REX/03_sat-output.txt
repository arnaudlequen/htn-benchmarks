Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.003] Parsed head comment information.
[0.188] Processed problem encoding.
[0.194] Calculated possible fact changes of composite elements.
[0.207] Initialized instantiation procedure.
[0.207] 
[0.207] *************************************
[0.207] * * *   M a k e s p a n     0   * * *
[0.207] *************************************
[0.220] Instantiated 35,654 initial clauses.
[0.220] The encoding contains a total of 34,151 distinct variables.
[0.220] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.220] Executed solver; result: UNSAT.
[0.220] 
[0.220] *************************************
[0.220] * * *   M a k e s p a n     1   * * *
[0.220] *************************************
[0.248] Computed next depth properties: array size of 66.
[0.308] Instantiated 174,670 transitional clauses.
[0.656] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.656] Instantiated 5,317,979 universal clauses.
[0.656] Instantiated and added clauses for a total of 5,528,303 clauses.
[0.656] The encoding contains a total of 48,796 distinct variables.
[0.656] Attempting solve with solver <glucose4> ...
c 66 assumptions
c last restart ## conflicts  :  93 6892 
[0.810] Executed solver; result: SAT.
[0.810] Solver returned SAT; a solution has been found at makespan 1.
65
solution 2212 1
113 118 12 119 14 1195 27 541 762 543 1570 30 535 1899 537 445 93 535 577 537 1285 58 4 1962 6 852 121 592 2040 594 1869 104 541 2084 543 216 39 595 611 597 1364 16 592 2097 594 1629 90 541 2180 543 994 46 595 2212 597 498 107 592 691 594 341 83 598 732 600 
[0.811] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.015] Parsed head comment information.
[1.178] Processed problem encoding.
[1.233] Calculated possible fact changes of composite elements.
[1.324] Initialized instantiation procedure.
[1.324] 
[1.324] *************************************
[1.324] * * *   M a k e s p a n     0   * * *
[1.324] *************************************
[1.408] Instantiated 214,682 initial clauses.
[1.408] The encoding contains a total of 211,019 distinct variables.
[1.408] Attempting solve with solver <glucose4> ...
c 20 assumptions
[1.411] Executed solver; result: UNSAT.
[1.411] 
[1.411] *************************************
[1.411] * * *   M a k e s p a n     1   * * *
[1.411] *************************************
[1.580] Computed next depth properties: array size of 96.
[1.972] Instantiated 1,091,229 transitional clauses.
[8.208] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[8.208] Instantiated 83,636,920 universal clauses.
[8.208] Instantiated and added clauses for a total of 84,942,831 clauses.
[8.208] The encoding contains a total of 272,802 distinct variables.
[8.208] Attempting solve with solver <glucose4> ...
c 96 assumptions
c last restart ## conflicts  :  4 25935 
[8.465] Executed solver; result: SAT.
[8.465] Solver returned SAT; a solution has been found at makespan 1.
95
solution 6790 1
4121 195 1949 2154 1951 510 11 12 13 14 2190 143 1952 5810 1954 4997 200 1943 5935 1945 859 229 1438 1540 1440 263 44 1444 1558 1446 929 186 12 1721 14 645 107 1444 1786 1446 1418 89 16 1878 18 3555 53 16 5970 18 3287 110 4 6095 6 3749 218 4 6243 6 2753 31 1438 6260 1440 1115 62 1952 1974 1954 5418 166 8 6420 10 5557 65 4 6475 6 4527 24 12 6557 14 2517 159 1949 6717 1951 4735 98 1444 6790 1446 
[8.466] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.021] Processed problem encoding.
[0.022] Calculated possible fact changes of composite elements.
[0.023] Initialized instantiation procedure.
[0.023] 
[0.023] *************************************
[0.023] * * *   M a k e s p a n     0   * * *
[0.023] *************************************
[0.025] Instantiated 3,092 initial clauses.
[0.025] The encoding contains a total of 1,615 distinct variables.
[0.025] Attempting solve with solver <glucose4> ...
c 27 assumptions
[0.026] Executed solver; result: UNSAT.
[0.026] 
[0.026] *************************************
[0.026] * * *   M a k e s p a n     1   * * *
[0.026] *************************************
[0.030] Computed next depth properties: array size of 53.
[0.034] Instantiated 2,836 transitional clauses.
[0.043] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.043] Instantiated 12,503 universal clauses.
[0.043] Instantiated and added clauses for a total of 18,431 clauses.
[0.043] The encoding contains a total of 5,753 distinct variables.
[0.043] Attempting solve with solver <glucose4> ...
c 53 assumptions
[0.043] Executed solver; result: UNSAT.
[0.043] 
[0.043] *************************************
[0.043] * * *   M a k e s p a n     2   * * *
[0.043] *************************************
[0.051] Computed next depth properties: array size of 105.
[0.060] Instantiated 8,218 transitional clauses.
[0.080] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.080] Instantiated 91,309 universal clauses.
[0.080] Instantiated and added clauses for a total of 117,958 clauses.
[0.080] The encoding contains a total of 11,451 distinct variables.
[0.080] Attempting solve with solver <glucose4> ...
c 105 assumptions
c last restart ## conflicts  :  0 111 
[0.080] Executed solver; result: SAT.
[0.080] Solver returned SAT; a solution has been found at makespan 2.
102
solution 1915 1
548 2 1186 1201 595 3 1663 1202 613 4 874 1203 108 5 1239 1204 152 6 1683 1205 170 7 1706 1206 555 8 710 1207 9 1743 1208 212 10 900 1209 248 11 1355 1210 297 12 1400 1211 810 13 1464 1212 324 14 531 1213 867 15 1479 1214 387 16 1543 1215 390 17 1836 1216 952 18 224 1217 1016 19 1880 1218 831 20 669 1219 425 21 332 1220 1070 22 1915 1221 628 23 1588 1222 1104 24 1165 1223 25 1383 1224 492 26 1621 1225 525 27 997 1226 
[0.080] Exiting.

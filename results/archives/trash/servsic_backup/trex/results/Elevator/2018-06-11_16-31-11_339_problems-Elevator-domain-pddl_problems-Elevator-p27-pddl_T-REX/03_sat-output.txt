Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.002] Instantiated 232 initial clauses.
[0.002] The encoding contains a total of 135 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     1   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 13.
[0.002] Instantiated 176 transitional clauses.
[0.002] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.002] Instantiated 723 universal clauses.
[0.002] Instantiated and added clauses for a total of 1,131 clauses.
[0.002] The encoding contains a total of 373 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     2   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 25.
[0.003] Instantiated 458 transitional clauses.
[0.004] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.004] Instantiated 2,109 universal clauses.
[0.004] Instantiated and added clauses for a total of 3,698 clauses.
[0.004] The encoding contains a total of 731 distinct variables.
[0.004] Attempting solve with solver <glucose4> ...
c 25 assumptions
c last restart ## conflicts  :  0 31 
[0.004] Executed solver; result: SAT.
[0.004] Solver returned SAT; a solution has been found at makespan 2.
24
solution 93 1
39 2 93 63 52 3 10 64 14 4 78 65 23 5 82 66 26 6 75 67 34 7 58 68 
[0.004] Exiting.

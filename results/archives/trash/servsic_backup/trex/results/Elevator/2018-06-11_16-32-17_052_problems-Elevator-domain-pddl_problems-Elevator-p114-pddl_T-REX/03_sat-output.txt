Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.013] Processed problem encoding.
[0.014] Calculated possible fact changes of composite elements.
[0.015] Initialized instantiation procedure.
[0.015] 
[0.015] *************************************
[0.015] * * *   M a k e s p a n     0   * * *
[0.015] *************************************
[0.017] Instantiated 2,459 initial clauses.
[0.017] The encoding contains a total of 1,291 distinct variables.
[0.017] Attempting solve with solver <glucose4> ...
c 24 assumptions
[0.017] Executed solver; result: UNSAT.
[0.017] 
[0.017] *************************************
[0.017] * * *   M a k e s p a n     1   * * *
[0.017] *************************************
[0.020] Computed next depth properties: array size of 47.
[0.023] Instantiated 2,233 transitional clauses.
[0.028] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.028] Instantiated 9,818 universal clauses.
[0.028] Instantiated and added clauses for a total of 14,510 clauses.
[0.028] The encoding contains a total of 4,538 distinct variables.
[0.028] Attempting solve with solver <glucose4> ...
c 47 assumptions
[0.029] Executed solver; result: UNSAT.
[0.029] 
[0.029] *************************************
[0.029] * * *   M a k e s p a n     2   * * *
[0.029] *************************************
[0.034] Computed next depth properties: array size of 93.
[0.040] Instantiated 6,442 transitional clauses.
[0.054] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.054] Instantiated 65,179 universal clauses.
[0.054] Instantiated and added clauses for a total of 86,131 clauses.
[0.054] The encoding contains a total of 9,027 distinct variables.
[0.054] Attempting solve with solver <glucose4> ...
c 93 assumptions
c last restart ## conflicts  :  0 99 
[0.054] Executed solver; result: SAT.
[0.054] Solver returned SAT; a solution has been found at makespan 2.
89
solution 1145 1
396 2 1041 835 424 3 68 836 4 902 837 82 5 149 838 521 6 157 839 550 7 1076 840 131 8 146 841 9 500 842 628 10 927 843 221 11 148 844 225 12 546 845 257 13 297 846 602 14 621 847 719 15 357 848 287 16 787 849 534 17 1087 850 721 18 88 851 321 19 633 852 279 20 983 853 775 21 564 854 22 190 855 800 23 1145 856 810 24 1034 857 
[0.054] Exiting.

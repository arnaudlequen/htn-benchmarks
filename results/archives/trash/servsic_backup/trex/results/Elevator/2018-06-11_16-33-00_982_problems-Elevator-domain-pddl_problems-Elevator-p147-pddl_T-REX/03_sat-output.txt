Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.027] Processed problem encoding.
[0.029] Calculated possible fact changes of composite elements.
[0.030] Initialized instantiation procedure.
[0.030] 
[0.030] *************************************
[0.030] * * *   M a k e s p a n     0   * * *
[0.030] *************************************
[0.033] Instantiated 4,048 initial clauses.
[0.033] The encoding contains a total of 2,103 distinct variables.
[0.033] Attempting solve with solver <glucose4> ...
c 31 assumptions
[0.033] Executed solver; result: UNSAT.
[0.033] 
[0.033] *************************************
[0.033] * * *   M a k e s p a n     1   * * *
[0.033] *************************************
[0.040] Computed next depth properties: array size of 61.
[0.045] Instantiated 3,752 transitional clauses.
[0.057] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.057] Instantiated 16,587 universal clauses.
[0.057] Instantiated and added clauses for a total of 24,387 clauses.
[0.057] The encoding contains a total of 7,597 distinct variables.
[0.057] Attempting solve with solver <glucose4> ...
c 61 assumptions
[0.058] Executed solver; result: UNSAT.
[0.058] 
[0.058] *************************************
[0.058] * * *   M a k e s p a n     2   * * *
[0.058] *************************************
[0.068] Computed next depth properties: array size of 121.
[0.080] Instantiated 10,922 transitional clauses.
[0.106] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.106] Instantiated 135,837 universal clauses.
[0.106] Instantiated and added clauses for a total of 171,146 clauses.
[0.106] The encoding contains a total of 15,131 distinct variables.
[0.106] Attempting solve with solver <glucose4> ...
c 121 assumptions
c last restart ## conflicts  :  0 127 
[0.107] Executed solver; result: SAT.
[0.107] Solver returned SAT; a solution has been found at makespan 2.
120
solution 2351 1
790 2 1557 1507 851 3 527 1508 98 4 1594 1509 131 5 672 1510 156 6 1091 1511 236 7 1621 1512 275 8 1138 1513 323 9 176 1514 959 10 2055 1515 946 11 2101 1516 1001 12 1571 1517 1073 13 2147 1518 415 14 1711 1519 1103 15 1749 1520 477 16 515 1521 497 17 545 1522 919 18 902 1523 1219 19 612 1524 552 20 2208 1525 1248 21 182 1526 1283 22 1279 1527 634 23 1806 1528 96 24 1370 1529 519 25 1863 1530 703 26 1885 1531 1368 27 593 1532 1157 28 1990 1533 753 29 1893 1534 774 30 2351 1535 1484 31 592 1536 
[0.107] Exiting.

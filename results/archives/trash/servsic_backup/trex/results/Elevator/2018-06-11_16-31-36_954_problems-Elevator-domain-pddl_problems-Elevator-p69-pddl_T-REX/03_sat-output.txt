Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.006] Processed problem encoding.
[0.007] Calculated possible fact changes of composite elements.
[0.007] Initialized instantiation procedure.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     0   * * *
[0.007] *************************************
[0.008] Instantiated 992 initial clauses.
[0.008] The encoding contains a total of 535 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.008] Executed solver; result: UNSAT.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     1   * * *
[0.008] *************************************
[0.009] Computed next depth properties: array size of 29.
[0.010] Instantiated 856 transitional clauses.
[0.012] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.012] Instantiated 3,707 universal clauses.
[0.012] Instantiated and added clauses for a total of 5,555 clauses.
[0.012] The encoding contains a total of 1,757 distinct variables.
[0.012] Attempting solve with solver <glucose4> ...
c 29 assumptions
[0.012] Executed solver; result: UNSAT.
[0.012] 
[0.012] *************************************
[0.012] * * *   M a k e s p a n     2   * * *
[0.012] *************************************
[0.014] Computed next depth properties: array size of 57.
[0.016] Instantiated 2,410 transitional clauses.
[0.021] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.021] Instantiated 17,245 universal clauses.
[0.021] Instantiated and added clauses for a total of 25,210 clauses.
[0.021] The encoding contains a total of 3,483 distinct variables.
[0.021] Attempting solve with solver <glucose4> ...
c 57 assumptions
c last restart ## conflicts  :  0 63 
[0.021] Executed solver; result: SAT.
[0.021] Solver returned SAT; a solution has been found at makespan 2.
55
solution 419 1
129 2 40 313 26 3 201 314 38 4 81 315 149 5 398 316 203 6 128 317 226 7 362 318 72 8 233 319 9 412 320 270 10 410 321 290 11 395 322 102 12 137 323 107 13 195 324 128 14 419 325 285 15 25 326 
[0.022] Exiting.

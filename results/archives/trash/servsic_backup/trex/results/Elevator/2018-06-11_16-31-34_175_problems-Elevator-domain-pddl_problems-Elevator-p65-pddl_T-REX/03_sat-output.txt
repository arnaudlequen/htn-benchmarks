Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.006] Processed problem encoding.
[0.006] Calculated possible fact changes of composite elements.
[0.006] Initialized instantiation procedure.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     0   * * *
[0.006] *************************************
[0.007] Instantiated 869 initial clauses.
[0.007] The encoding contains a total of 471 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.007] Executed solver; result: UNSAT.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     1   * * *
[0.007] *************************************
[0.008] Computed next depth properties: array size of 27.
[0.009] Instantiated 743 transitional clauses.
[0.011] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.011] Instantiated 3,208 universal clauses.
[0.011] Instantiated and added clauses for a total of 4,820 clauses.
[0.011] The encoding contains a total of 1,528 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 27 assumptions
[0.011] Executed solver; result: UNSAT.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     2   * * *
[0.011] *************************************
[0.012] Computed next depth properties: array size of 53.
[0.014] Instantiated 2,082 transitional clauses.
[0.019] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.019] Instantiated 14,219 universal clauses.
[0.019] Instantiated and added clauses for a total of 21,121 clauses.
[0.019] The encoding contains a total of 3,027 distinct variables.
[0.019] Attempting solve with solver <glucose4> ...
c 53 assumptions
c last restart ## conflicts  :  0 59 
[0.019] Executed solver; result: SAT.
[0.019] Solver returned SAT; a solution has been found at makespan 2.
52
solution 408 1
183 2 233 265 37 3 282 266 200 4 366 267 200 5 296 268 72 6 303 269 97 7 305 270 106 8 326 271 116 9 39 272 232 10 408 273 237 11 74 274 152 12 225 275 201 13 351 276 181 14 404 277 
[0.019] Exiting.

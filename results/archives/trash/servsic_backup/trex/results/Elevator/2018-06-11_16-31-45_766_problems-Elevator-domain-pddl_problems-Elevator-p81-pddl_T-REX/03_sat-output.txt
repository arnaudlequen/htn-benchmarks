Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.010] Processed problem encoding.
[0.010] Calculated possible fact changes of composite elements.
[0.010] Initialized instantiation procedure.
[0.010] 
[0.010] *************************************
[0.010] * * *   M a k e s p a n     0   * * *
[0.010] *************************************
[0.011] Instantiated 1,409 initial clauses.
[0.011] The encoding contains a total of 751 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 18 assumptions
[0.011] Executed solver; result: UNSAT.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     1   * * *
[0.011] *************************************
[0.013] Computed next depth properties: array size of 35.
[0.015] Instantiated 1,243 transitional clauses.
[0.018] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.018] Instantiated 5,420 universal clauses.
[0.018] Instantiated and added clauses for a total of 8,072 clauses.
[0.018] The encoding contains a total of 2,540 distinct variables.
[0.018] Attempting solve with solver <glucose4> ...
c 35 assumptions
[0.018] Executed solver; result: UNSAT.
[0.018] 
[0.018] *************************************
[0.018] * * *   M a k e s p a n     2   * * *
[0.018] *************************************
[0.021] Computed next depth properties: array size of 69.
[0.025] Instantiated 3,538 transitional clauses.
[0.032] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.032] Instantiated 28,795 universal clauses.
[0.032] Instantiated and added clauses for a total of 40,405 clauses.
[0.032] The encoding contains a total of 5,043 distinct variables.
[0.032] Attempting solve with solver <glucose4> ...
c 69 assumptions
c last restart ## conflicts  :  0 75 
[0.032] Executed solver; result: SAT.
[0.032] Solver returned SAT; a solution has been found at makespan 2.
68
solution 738 1
280 2 591 547 47 3 529 548 50 4 667 549 334 5 200 550 82 6 459 551 381 7 511 552 77 8 181 553 403 9 108 554 435 10 627 555 145 11 473 556 161 12 482 557 456 13 702 558 472 14 376 559 227 15 642 560 510 16 99 561 531 17 581 562 278 18 738 563 
[0.033] Exiting.

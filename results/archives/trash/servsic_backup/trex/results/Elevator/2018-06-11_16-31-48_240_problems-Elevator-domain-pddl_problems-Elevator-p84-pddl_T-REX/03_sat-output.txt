Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.010] Processed problem encoding.
[0.011] Calculated possible fact changes of composite elements.
[0.011] Initialized instantiation procedure.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     0   * * *
[0.011] *************************************
[0.012] Instantiated 1,409 initial clauses.
[0.012] The encoding contains a total of 751 distinct variables.
[0.012] Attempting solve with solver <glucose4> ...
c 18 assumptions
[0.012] Executed solver; result: UNSAT.
[0.012] 
[0.012] *************************************
[0.012] * * *   M a k e s p a n     1   * * *
[0.012] *************************************
[0.014] Computed next depth properties: array size of 35.
[0.016] Instantiated 1,243 transitional clauses.
[0.019] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.019] Instantiated 5,420 universal clauses.
[0.019] Instantiated and added clauses for a total of 8,072 clauses.
[0.019] The encoding contains a total of 2,540 distinct variables.
[0.019] Attempting solve with solver <glucose4> ...
c 35 assumptions
[0.019] Executed solver; result: UNSAT.
[0.019] 
[0.019] *************************************
[0.019] * * *   M a k e s p a n     2   * * *
[0.019] *************************************
[0.023] Computed next depth properties: array size of 69.
[0.026] Instantiated 3,538 transitional clauses.
[0.035] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.035] Instantiated 28,795 universal clauses.
[0.035] Instantiated and added clauses for a total of 40,405 clauses.
[0.035] The encoding contains a total of 5,043 distinct variables.
[0.035] Attempting solve with solver <glucose4> ...
c 69 assumptions
c last restart ## conflicts  :  0 75 
[0.035] Executed solver; result: SAT.
[0.035] Solver returned SAT; a solution has been found at makespan 2.
68
solution 866 1
204 2 435 415 51 3 661 416 226 4 477 417 98 5 688 418 27 6 501 419 259 7 748 420 304 8 396 421 122 9 534 422 117 10 761 423 339 11 790 424 302 12 76 425 149 13 578 426 172 14 601 427 388 15 794 428 271 16 826 429 197 17 845 430 354 18 866 431 
[0.035] Exiting.

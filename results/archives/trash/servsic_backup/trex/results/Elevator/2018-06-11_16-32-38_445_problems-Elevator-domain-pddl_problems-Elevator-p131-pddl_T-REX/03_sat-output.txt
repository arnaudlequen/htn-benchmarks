Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.030] Processed problem encoding.
[0.032] Calculated possible fact changes of composite elements.
[0.034] Initialized instantiation procedure.
[0.034] 
[0.034] *************************************
[0.034] * * *   M a k e s p a n     0   * * *
[0.034] *************************************
[0.037] Instantiated 3,319 initial clauses.
[0.037] The encoding contains a total of 1,731 distinct variables.
[0.037] Attempting solve with solver <glucose4> ...
c 28 assumptions
[0.037] Executed solver; result: UNSAT.
[0.037] 
[0.037] *************************************
[0.037] * * *   M a k e s p a n     1   * * *
[0.037] *************************************
[0.043] Computed next depth properties: array size of 55.
[0.048] Instantiated 3,053 transitional clauses.
[0.060] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.060] Instantiated 13,470 universal clauses.
[0.060] Instantiated and added clauses for a total of 19,842 clauses.
[0.060] The encoding contains a total of 6,190 distinct variables.
[0.060] Attempting solve with solver <glucose4> ...
c 55 assumptions
[0.060] Executed solver; result: UNSAT.
[0.060] 
[0.060] *************************************
[0.060] * * *   M a k e s p a n     2   * * *
[0.060] *************************************
[0.072] Computed next depth properties: array size of 109.
[0.083] Instantiated 8,858 transitional clauses.
[0.110] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.110] Instantiated 101,355 universal clauses.
[0.110] Instantiated and added clauses for a total of 130,055 clauses.
[0.110] The encoding contains a total of 12,323 distinct variables.
[0.110] Attempting solve with solver <glucose4> ...
c 109 assumptions
c last restart ## conflicts  :  0 115 
[0.110] Executed solver; result: SAT.
[0.110] Solver returned SAT; a solution has been found at makespan 2.
108
solution 1825 1
618 2 430 1354 653 3 1559 1355 707 4 1555 1356 748 5 370 1357 781 6 1136 1358 839 7 533 1359 883 8 1592 1360 135 9 1614 1361 893 10 1668 1362 895 11 1757 1363 957 12 267 1364 1008 13 107 1365 1066 14 1794 1366 1085 15 1461 1367 297 16 165 1368 1145 17 1680 1369 1158 18 140 1370 379 19 1106 1371 932 20 535 1372 442 21 998 1373 488 22 1825 1374 1225 23 831 1375 533 24 833 1376 584 25 1495 1377 1325 26 509 1378 593 27 1131 1379 269 28 1776 1380 
[0.111] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.010] Processed problem encoding.
[0.011] Calculated possible fact changes of composite elements.
[0.011] Initialized instantiation procedure.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     0   * * *
[0.011] *************************************
[0.012] Instantiated 1,564 initial clauses.
[0.012] The encoding contains a total of 831 distinct variables.
[0.012] Attempting solve with solver <glucose4> ...
c 19 assumptions
[0.012] Executed solver; result: UNSAT.
[0.012] 
[0.012] *************************************
[0.012] * * *   M a k e s p a n     1   * * *
[0.012] *************************************
[0.014] Computed next depth properties: array size of 37.
[0.016] Instantiated 1,388 transitional clauses.
[0.020] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.020] Instantiated 6,063 universal clauses.
[0.020] Instantiated and added clauses for a total of 9,015 clauses.
[0.020] The encoding contains a total of 2,833 distinct variables.
[0.020] Attempting solve with solver <glucose4> ...
c 37 assumptions
[0.020] Executed solver; result: UNSAT.
[0.020] 
[0.020] *************************************
[0.020] * * *   M a k e s p a n     2   * * *
[0.020] *************************************
[0.024] Computed next depth properties: array size of 73.
[0.028] Instantiated 3,962 transitional clauses.
[0.036] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.036] Instantiated 33,549 universal clauses.
[0.036] Instantiated and added clauses for a total of 46,526 clauses.
[0.036] The encoding contains a total of 5,627 distinct variables.
[0.036] Attempting solve with solver <glucose4> ...
c 73 assumptions
c last restart ## conflicts  :  0 79 
[0.036] Executed solver; result: SAT.
[0.036] Solver returned SAT; a solution has been found at makespan 2.
72
solution 795 1
308 2 292 510 333 3 34 511 38 4 544 512 71 5 487 513 390 6 222 514 116 7 547 515 397 8 345 516 179 9 569 517 453 10 745 518 462 11 767 519 196 12 607 520 180 13 795 521 421 14 569 522 234 15 644 523 337 16 34 524 262 17 310 525 292 18 57 526 309 19 666 527 
[0.037] Exiting.

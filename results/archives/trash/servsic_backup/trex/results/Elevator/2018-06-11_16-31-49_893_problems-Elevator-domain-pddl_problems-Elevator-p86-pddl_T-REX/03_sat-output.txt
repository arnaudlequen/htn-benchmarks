Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.010] Processed problem encoding.
[0.011] Calculated possible fact changes of composite elements.
[0.011] Initialized instantiation procedure.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     0   * * *
[0.011] *************************************
[0.013] Instantiated 1,564 initial clauses.
[0.013] The encoding contains a total of 831 distinct variables.
[0.013] Attempting solve with solver <glucose4> ...
c 19 assumptions
[0.013] Executed solver; result: UNSAT.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     1   * * *
[0.013] *************************************
[0.015] Computed next depth properties: array size of 37.
[0.016] Instantiated 1,388 transitional clauses.
[0.020] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.020] Instantiated 6,063 universal clauses.
[0.020] Instantiated and added clauses for a total of 9,015 clauses.
[0.020] The encoding contains a total of 2,833 distinct variables.
[0.020] Attempting solve with solver <glucose4> ...
c 37 assumptions
[0.020] Executed solver; result: UNSAT.
[0.020] 
[0.020] *************************************
[0.020] * * *   M a k e s p a n     2   * * *
[0.020] *************************************
[0.024] Computed next depth properties: array size of 73.
[0.028] Instantiated 3,962 transitional clauses.
[0.037] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.037] Instantiated 33,549 universal clauses.
[0.037] Instantiated and added clauses for a total of 46,526 clauses.
[0.037] The encoding contains a total of 5,627 distinct variables.
[0.037] Attempting solve with solver <glucose4> ...
c 73 assumptions
c last restart ## conflicts  :  0 79 
[0.037] Executed solver; result: SAT.
[0.037] Solver returned SAT; a solution has been found at makespan 2.
72
solution 812 1
266 2 582 545 60 3 709 546 63 4 602 547 306 5 614 548 321 6 259 549 343 7 528 550 135 8 714 551 363 9 759 552 401 10 216 553 164 11 654 554 447 12 805 555 462 13 324 556 208 14 540 557 31 15 75 558 500 16 692 559 86 17 812 560 229 18 404 561 530 19 594 562 
[0.037] Exiting.

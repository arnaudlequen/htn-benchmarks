Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.003] Processed problem encoding.
[0.003] Calculated possible fact changes of composite elements.
[0.003] Initialized instantiation procedure.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     0   * * *
[0.003] *************************************
[0.003] Instantiated 374 initial clauses.
[0.003] The encoding contains a total of 211 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 9 assumptions
[0.003] Executed solver; result: UNSAT.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     1   * * *
[0.003] *************************************
[0.003] Computed next depth properties: array size of 17.
[0.004] Instantiated 298 transitional clauses.
[0.005] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.005] Instantiated 1,253 universal clauses.
[0.005] Instantiated and added clauses for a total of 1,925 clauses.
[0.005] The encoding contains a total of 623 distinct variables.
[0.005] Attempting solve with solver <glucose4> ...
c 17 assumptions
[0.005] Executed solver; result: UNSAT.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     2   * * *
[0.005] *************************************
[0.005] Computed next depth properties: array size of 33.
[0.006] Instantiated 802 transitional clauses.
[0.008] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.008] Instantiated 4,189 universal clauses.
[0.008] Instantiated and added clauses for a total of 6,916 clauses.
[0.008] The encoding contains a total of 1,227 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 33 assumptions
c last restart ## conflicts  :  0 39 
[0.008] Executed solver; result: SAT.
[0.008] Solver returned SAT; a solution has been found at makespan 2.
32
solution 170 1
89 2 56 130 102 3 145 131 36 4 97 132 45 5 151 133 54 6 76 134 113 7 121 135 84 8 170 136 119 9 72 137 
[0.008] Exiting.

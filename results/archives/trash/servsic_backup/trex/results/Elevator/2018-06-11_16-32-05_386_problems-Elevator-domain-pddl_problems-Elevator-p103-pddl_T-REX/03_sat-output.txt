Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.006] Processed problem encoding.
[0.007] Calculated possible fact changes of composite elements.
[0.007] Initialized instantiation procedure.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     0   * * *
[0.007] *************************************
[0.008] Instantiated 2,077 initial clauses.
[0.008] The encoding contains a total of 1,095 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 22 assumptions
[0.008] Executed solver; result: UNSAT.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     1   * * *
[0.008] *************************************
[0.009] Computed next depth properties: array size of 43.
[0.010] Instantiated 1,871 transitional clauses.
[0.013] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.013] Instantiated 8,208 universal clauses.
[0.013] Instantiated and added clauses for a total of 12,156 clauses.
[0.013] The encoding contains a total of 3,808 distinct variables.
[0.013] Attempting solve with solver <glucose4> ...
c 43 assumptions
[0.013] Executed solver; result: UNSAT.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     2   * * *
[0.013] *************************************
[0.015] Computed next depth properties: array size of 85.
[0.018] Instantiated 5,378 transitional clauses.
[0.025] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.025] Instantiated 50,859 universal clauses.
[0.025] Instantiated and added clauses for a total of 68,393 clauses.
[0.025] The encoding contains a total of 7,571 distinct variables.
[0.025] Attempting solve with solver <glucose4> ...
c 85 assumptions
c last restart ## conflicts  :  0 91 
[0.025] Executed solver; result: SAT.
[0.025] Solver returned SAT; a solution has been found at makespan 2.
84
solution 1190 1
388 2 1020 761 34 3 548 762 42 4 547 763 81 5 820 764 123 6 851 765 72 7 873 766 142 8 907 767 516 9 806 768 127 10 915 769 180 11 1123 770 557 12 712 771 225 13 744 772 246 14 565 773 278 15 683 774 612 16 161 775 658 17 1154 776 320 18 619 777 364 19 976 778 53 20 753 779 721 21 1190 780 732 22 1086 781 
[0.025] Exiting.

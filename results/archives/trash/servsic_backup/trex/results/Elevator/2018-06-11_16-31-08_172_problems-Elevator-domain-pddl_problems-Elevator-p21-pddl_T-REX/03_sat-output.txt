Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.002] Initialized instantiation procedure.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     0   * * *
[0.002] *************************************
[0.002] Instantiated 173 initial clauses.
[0.002] The encoding contains a total of 103 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 6 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     1   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 11.
[0.002] Instantiated 127 transitional clauses.
[0.002] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.002] Instantiated 512 universal clauses.
[0.002] Instantiated and added clauses for a total of 812 clauses.
[0.002] The encoding contains a total of 272 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 11 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     2   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 21.
[0.003] Instantiated 322 transitional clauses.
[0.003] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.003] Instantiated 1,387 universal clauses.
[0.003] Instantiated and added clauses for a total of 2,521 clauses.
[0.003] The encoding contains a total of 531 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 21 assumptions
c last restart ## conflicts  :  0 27 
[0.003] Executed solver; result: SAT.
[0.003] Solver returned SAT; a solution has been found at makespan 2.
20
solution 74 1
18 2 31 43 27 3 57 44 8 4 51 45 33 5 63 46 36 6 74 47 
[0.004] Exiting.

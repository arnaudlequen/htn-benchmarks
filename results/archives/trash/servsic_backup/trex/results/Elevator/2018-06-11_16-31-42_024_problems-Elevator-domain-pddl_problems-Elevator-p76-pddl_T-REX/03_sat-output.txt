Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.008] Processed problem encoding.
[0.009] Calculated possible fact changes of composite elements.
[0.009] Initialized instantiation procedure.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     0   * * *
[0.009] *************************************
[0.010] Instantiated 1,262 initial clauses.
[0.010] The encoding contains a total of 675 distinct variables.
[0.010] Attempting solve with solver <glucose4> ...
c 17 assumptions
[0.010] Executed solver; result: UNSAT.
[0.010] 
[0.010] *************************************
[0.010] * * *   M a k e s p a n     1   * * *
[0.010] *************************************
[0.012] Computed next depth properties: array size of 33.
[0.013] Instantiated 1,106 transitional clauses.
[0.017] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.017] Instantiated 4,813 universal clauses.
[0.017] Instantiated and added clauses for a total of 7,181 clauses.
[0.017] The encoding contains a total of 2,263 distinct variables.
[0.017] Attempting solve with solver <glucose4> ...
c 33 assumptions
[0.017] Executed solver; result: UNSAT.
[0.017] 
[0.017] *************************************
[0.017] * * *   M a k e s p a n     2   * * *
[0.017] *************************************
[0.019] Computed next depth properties: array size of 65.
[0.023] Instantiated 3,138 transitional clauses.
[0.029] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.029] Instantiated 24,509 universal clauses.
[0.029] Instantiated and added clauses for a total of 34,828 clauses.
[0.029] The encoding contains a total of 4,491 distinct variables.
[0.029] Attempting solve with solver <glucose4> ...
c 65 assumptions
c last restart ## conflicts  :  0 71 
[0.030] Executed solver; result: SAT.
[0.030] Solver returned SAT; a solution has been found at makespan 2.
64
solution 680 1
201 2 544 421 214 3 411 422 65 4 460 423 98 5 484 424 44 6 495 425 257 7 620 426 281 8 357 427 140 9 440 428 297 10 651 429 321 11 532 430 363 12 680 431 18 13 493 432 379 14 266 433 183 15 127 434 405 16 462 435 63 17 253 436 
[0.030] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.007] Processed problem encoding.
[0.007] Calculated possible fact changes of composite elements.
[0.007] Initialized instantiation procedure.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     0   * * *
[0.008] *************************************
[0.008] Instantiated 1,262 initial clauses.
[0.008] The encoding contains a total of 675 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 17 assumptions
[0.008] Executed solver; result: UNSAT.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     1   * * *
[0.008] *************************************
[0.010] Computed next depth properties: array size of 33.
[0.011] Instantiated 1,106 transitional clauses.
[0.013] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.013] Instantiated 4,813 universal clauses.
[0.013] Instantiated and added clauses for a total of 7,181 clauses.
[0.013] The encoding contains a total of 2,263 distinct variables.
[0.013] Attempting solve with solver <glucose4> ...
c 33 assumptions
[0.013] Executed solver; result: UNSAT.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     2   * * *
[0.013] *************************************
[0.016] Computed next depth properties: array size of 65.
[0.018] Instantiated 3,138 transitional clauses.
[0.024] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.024] Instantiated 24,509 universal clauses.
[0.024] Instantiated and added clauses for a total of 34,828 clauses.
[0.024] The encoding contains a total of 4,491 distinct variables.
[0.024] Attempting solve with solver <glucose4> ...
c 65 assumptions
c last restart ## conflicts  :  0 71 
[0.024] Executed solver; result: SAT.
[0.024] Solver returned SAT; a solution has been found at makespan 2.
64
solution 681 1
161 2 530 359 190 3 354 360 60 4 185 361 69 5 397 362 78 6 564 363 249 7 425 364 108 8 577 365 115 9 598 366 130 10 382 367 207 11 625 368 111 12 259 369 320 13 589 370 115 14 635 371 219 15 560 372 334 16 509 373 159 17 681 374 
[0.024] Exiting.

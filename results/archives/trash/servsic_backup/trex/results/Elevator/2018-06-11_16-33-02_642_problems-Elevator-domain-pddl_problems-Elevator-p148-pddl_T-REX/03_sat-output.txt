Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.027] Processed problem encoding.
[0.029] Calculated possible fact changes of composite elements.
[0.030] Initialized instantiation procedure.
[0.030] 
[0.030] *************************************
[0.030] * * *   M a k e s p a n     0   * * *
[0.030] *************************************
[0.034] Instantiated 4,048 initial clauses.
[0.034] The encoding contains a total of 2,103 distinct variables.
[0.034] Attempting solve with solver <glucose4> ...
c 31 assumptions
[0.034] Executed solver; result: UNSAT.
[0.034] 
[0.034] *************************************
[0.034] * * *   M a k e s p a n     1   * * *
[0.034] *************************************
[0.040] Computed next depth properties: array size of 61.
[0.045] Instantiated 3,752 transitional clauses.
[0.057] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.057] Instantiated 16,587 universal clauses.
[0.057] Instantiated and added clauses for a total of 24,387 clauses.
[0.057] The encoding contains a total of 7,597 distinct variables.
[0.057] Attempting solve with solver <glucose4> ...
c 61 assumptions
[0.057] Executed solver; result: UNSAT.
[0.057] 
[0.057] *************************************
[0.057] * * *   M a k e s p a n     2   * * *
[0.057] *************************************
[0.068] Computed next depth properties: array size of 121.
[0.079] Instantiated 10,922 transitional clauses.
[0.104] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.104] Instantiated 135,837 universal clauses.
[0.104] Instantiated and added clauses for a total of 171,146 clauses.
[0.104] The encoding contains a total of 15,131 distinct variables.
[0.104] Attempting solve with solver <glucose4> ...
c 121 assumptions
c last restart ## conflicts  :  0 127 
[0.104] Executed solver; result: SAT.
[0.104] Solver returned SAT; a solution has been found at makespan 2.
120
solution 2230 1
627 2 1744 1212 670 3 1246 1213 111 4 1041 1214 132 5 617 1215 771 6 1303 1216 746 7 471 1217 799 8 1826 1218 809 9 1014 1219 287 10 1864 1220 853 11 1725 1221 813 12 1915 1222 870 13 296 1223 914 14 1951 1224 377 15 1046 1225 412 16 654 1226 1051 17 2008 1227 630 18 1445 1228 443 19 2042 1229 899 20 411 1230 448 21 66 1231 811 22 2065 1232 1127 23 1494 1233 542 24 1511 1234 294 25 1554 1235 706 26 1595 1236 273 27 732 1237 572 28 2230 1238 612 29 1256 1239 341 30 918 1240 352 31 1675 1241 
[0.105] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.015] Processed problem encoding.
[0.016] Calculated possible fact changes of composite elements.
[0.016] Initialized instantiation procedure.
[0.016] 
[0.016] *************************************
[0.016] * * *   M a k e s p a n     0   * * *
[0.016] *************************************
[0.018] Instantiated 2,077 initial clauses.
[0.018] The encoding contains a total of 1,095 distinct variables.
[0.018] Attempting solve with solver <glucose4> ...
c 22 assumptions
[0.018] Executed solver; result: UNSAT.
[0.018] 
[0.018] *************************************
[0.018] * * *   M a k e s p a n     1   * * *
[0.018] *************************************
[0.021] Computed next depth properties: array size of 43.
[0.023] Instantiated 1,871 transitional clauses.
[0.028] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.028] Instantiated 8,208 universal clauses.
[0.028] Instantiated and added clauses for a total of 12,156 clauses.
[0.028] The encoding contains a total of 3,808 distinct variables.
[0.028] Attempting solve with solver <glucose4> ...
c 43 assumptions
[0.029] Executed solver; result: UNSAT.
[0.029] 
[0.029] *************************************
[0.029] * * *   M a k e s p a n     2   * * *
[0.029] *************************************
[0.033] Computed next depth properties: array size of 85.
[0.039] Instantiated 5,378 transitional clauses.
[0.052] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.052] Instantiated 50,859 universal clauses.
[0.052] Instantiated and added clauses for a total of 68,393 clauses.
[0.052] The encoding contains a total of 7,571 distinct variables.
[0.052] Attempting solve with solver <glucose4> ...
c 85 assumptions
c last restart ## conflicts  :  0 91 
[0.052] Executed solver; result: SAT.
[0.052] Solver returned SAT; a solution has been found at makespan 2.
84
solution 1188 1
314 2 437 720 351 3 617 721 83 4 939 722 380 5 370 723 402 6 982 724 131 7 988 725 433 8 1047 726 463 9 959 727 501 10 1087 728 470 11 1091 729 78 12 581 730 62 13 837 731 194 14 386 732 218 15 303 733 574 16 1117 734 596 17 1153 735 646 18 222 736 260 19 1161 737 597 20 1188 738 272 21 564 739 303 22 467 740 
[0.052] Exiting.

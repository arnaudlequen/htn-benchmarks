Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.011] Processed problem encoding.
[0.012] Calculated possible fact changes of composite elements.
[0.012] Initialized instantiation procedure.
[0.012] 
[0.012] *************************************
[0.012] * * *   M a k e s p a n     0   * * *
[0.012] *************************************
[0.014] Instantiated 1,564 initial clauses.
[0.014] The encoding contains a total of 831 distinct variables.
[0.014] Attempting solve with solver <glucose4> ...
c 19 assumptions
[0.014] Executed solver; result: UNSAT.
[0.014] 
[0.014] *************************************
[0.014] * * *   M a k e s p a n     1   * * *
[0.014] *************************************
[0.016] Computed next depth properties: array size of 37.
[0.017] Instantiated 1,388 transitional clauses.
[0.021] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.021] Instantiated 6,063 universal clauses.
[0.021] Instantiated and added clauses for a total of 9,015 clauses.
[0.021] The encoding contains a total of 2,833 distinct variables.
[0.021] Attempting solve with solver <glucose4> ...
c 37 assumptions
[0.022] Executed solver; result: UNSAT.
[0.022] 
[0.022] *************************************
[0.022] * * *   M a k e s p a n     2   * * *
[0.022] *************************************
[0.025] Computed next depth properties: array size of 73.
[0.029] Instantiated 3,962 transitional clauses.
[0.038] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.038] Instantiated 33,549 universal clauses.
[0.038] Instantiated and added clauses for a total of 46,526 clauses.
[0.038] The encoding contains a total of 5,627 distinct variables.
[0.038] Attempting solve with solver <glucose4> ...
c 73 assumptions
c last restart ## conflicts  :  0 79 
[0.038] Executed solver; result: SAT.
[0.038] Solver returned SAT; a solution has been found at makespan 2.
72
solution 876 1
270 2 754 510 310 3 557 511 70 4 573 512 93 5 575 513 113 6 609 514 149 7 779 515 353 8 648 516 179 9 815 517 195 10 670 518 412 11 234 519 359 12 286 520 237 13 693 521 159 14 492 522 267 15 782 523 276 16 98 524 446 17 184 525 158 18 720 526 509 19 876 527 
[0.039] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.004] Processed problem encoding.
[0.005] Calculated possible fact changes of composite elements.
[0.005] Initialized instantiation procedure.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     0   * * *
[0.005] *************************************
[0.005] Instantiated 647 initial clauses.
[0.005] The encoding contains a total of 355 distinct variables.
[0.005] Attempting solve with solver <glucose4> ...
c 12 assumptions
[0.005] Executed solver; result: UNSAT.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     1   * * *
[0.005] *************************************
[0.006] Computed next depth properties: array size of 23.
[0.007] Instantiated 541 transitional clauses.
[0.008] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.008] Instantiated 2,318 universal clauses.
[0.008] Instantiated and added clauses for a total of 3,506 clauses.
[0.008] The encoding contains a total of 1,118 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 23 assumptions
[0.008] Executed solver; result: UNSAT.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     2   * * *
[0.008] *************************************
[0.009] Computed next depth properties: array size of 45.
[0.011] Instantiated 1,498 transitional clauses.
[0.014] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.014] Instantiated 9,259 universal clauses.
[0.014] Instantiated and added clauses for a total of 14,263 clauses.
[0.014] The encoding contains a total of 2,211 distinct variables.
[0.014] Attempting solve with solver <glucose4> ...
c 45 assumptions
c last restart ## conflicts  :  0 51 
[0.014] Executed solver; result: SAT.
[0.014] Solver returned SAT; a solution has been found at makespan 2.
44
solution 331 1
101 2 226 202 33 3 238 203 55 4 181 204 129 5 296 205 150 6 177 206 74 7 81 207 22 8 271 208 172 9 318 209 83 10 161 210 197 11 331 211 192 12 314 212 
[0.014] Exiting.

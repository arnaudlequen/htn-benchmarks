Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.005] Processed problem encoding.
[0.005] Calculated possible fact changes of composite elements.
[0.005] Initialized instantiation procedure.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     0   * * *
[0.005] *************************************
[0.006] Instantiated 647 initial clauses.
[0.006] The encoding contains a total of 355 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 12 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.006] Computed next depth properties: array size of 23.
[0.007] Instantiated 541 transitional clauses.
[0.008] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.008] Instantiated 2,318 universal clauses.
[0.008] Instantiated and added clauses for a total of 3,506 clauses.
[0.008] The encoding contains a total of 1,118 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 23 assumptions
[0.008] Executed solver; result: UNSAT.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     2   * * *
[0.008] *************************************
[0.010] Computed next depth properties: array size of 45.
[0.011] Instantiated 1,498 transitional clauses.
[0.014] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.014] Instantiated 9,259 universal clauses.
[0.014] Instantiated and added clauses for a total of 14,263 clauses.
[0.014] The encoding contains a total of 2,211 distinct variables.
[0.014] Attempting solve with solver <glucose4> ...
c 45 assumptions
c last restart ## conflicts  :  0 51 
[0.014] Executed solver; result: SAT.
[0.014] Solver returned SAT; a solution has been found at makespan 2.
44
solution 345 1
110 2 40 202 22 3 284 203 110 4 316 204 141 5 233 205 57 6 167 206 155 7 332 207 165 8 249 208 181 9 345 209 81 10 325 210 149 11 268 211 105 12 273 212 
[0.014] Exiting.

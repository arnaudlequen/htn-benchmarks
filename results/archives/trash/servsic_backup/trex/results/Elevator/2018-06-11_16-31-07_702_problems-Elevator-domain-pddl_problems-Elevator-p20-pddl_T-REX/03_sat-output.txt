Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.000] Processed problem encoding.
[0.000] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 122 initial clauses.
[0.001] The encoding contains a total of 75 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 5 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 9.
[0.001] Instantiated 86 transitional clauses.
[0.001] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.001] Instantiated 337 universal clauses.
[0.001] Instantiated and added clauses for a total of 545 clauses.
[0.001] The encoding contains a total of 187 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 9 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     2   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 17.
[0.001] Instantiated 210 transitional clauses.
[0.001] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.001] Instantiated 845 universal clauses.
[0.001] Instantiated and added clauses for a total of 1,600 clauses.
[0.001] The encoding contains a total of 363 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 17 assumptions
c last restart ## conflicts  :  0 23 
[0.001] Executed solver; result: SAT.
[0.001] Solver returned SAT; a solution has been found at makespan 2.
15
solution 56 1
14 2 10 34 3 40 35 27 4 51 36 32 5 56 37 
[0.001] Exiting.

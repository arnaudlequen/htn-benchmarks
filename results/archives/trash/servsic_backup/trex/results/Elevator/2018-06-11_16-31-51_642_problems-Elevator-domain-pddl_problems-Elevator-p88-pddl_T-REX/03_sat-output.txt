Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.009] Processed problem encoding.
[0.010] Calculated possible fact changes of composite elements.
[0.010] Initialized instantiation procedure.
[0.010] 
[0.010] *************************************
[0.011] * * *   M a k e s p a n     0   * * *
[0.011] *************************************
[0.012] Instantiated 1,564 initial clauses.
[0.012] The encoding contains a total of 831 distinct variables.
[0.012] Attempting solve with solver <glucose4> ...
c 19 assumptions
[0.012] Executed solver; result: UNSAT.
[0.012] 
[0.012] *************************************
[0.012] * * *   M a k e s p a n     1   * * *
[0.012] *************************************
[0.013] Computed next depth properties: array size of 37.
[0.015] Instantiated 1,388 transitional clauses.
[0.019] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.019] Instantiated 6,063 universal clauses.
[0.019] Instantiated and added clauses for a total of 9,015 clauses.
[0.019] The encoding contains a total of 2,833 distinct variables.
[0.019] Attempting solve with solver <glucose4> ...
c 37 assumptions
[0.019] Executed solver; result: UNSAT.
[0.019] 
[0.019] *************************************
[0.019] * * *   M a k e s p a n     2   * * *
[0.019] *************************************
[0.022] Computed next depth properties: array size of 73.
[0.026] Instantiated 3,962 transitional clauses.
[0.035] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.035] Instantiated 33,549 universal clauses.
[0.035] Instantiated and added clauses for a total of 46,526 clauses.
[0.035] The encoding contains a total of 5,627 distinct variables.
[0.035] Attempting solve with solver <glucose4> ...
c 73 assumptions
c last restart ## conflicts  :  0 79 
[0.035] Executed solver; result: SAT.
[0.035] Solver returned SAT; a solution has been found at makespan 2.
72
solution 793 1
345 2 234 545 388 3 464 546 410 4 584 547 84 5 595 548 96 6 291 549 425 7 726 550 368 8 122 551 134 9 581 552 172 10 631 553 180 11 166 554 492 12 384 555 219 13 669 556 240 14 437 557 278 15 636 558 109 16 570 559 314 17 677 560 328 18 152 561 393 19 793 562 
[0.035] Exiting.

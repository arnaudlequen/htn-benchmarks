Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.026] Processed problem encoding.
[0.027] Calculated possible fact changes of composite elements.
[0.029] Initialized instantiation procedure.
[0.029] 
[0.029] *************************************
[0.029] * * *   M a k e s p a n     0   * * *
[0.029] *************************************
[0.031] Instantiated 3,797 initial clauses.
[0.032] The encoding contains a total of 1,975 distinct variables.
[0.032] Attempting solve with solver <glucose4> ...
c 30 assumptions
[0.032] Executed solver; result: UNSAT.
[0.032] 
[0.032] *************************************
[0.032] * * *   M a k e s p a n     1   * * *
[0.032] *************************************
[0.038] Computed next depth properties: array size of 59.
[0.042] Instantiated 3,511 transitional clauses.
[0.053] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.053] Instantiated 15,512 universal clauses.
[0.053] Instantiated and added clauses for a total of 22,820 clauses.
[0.053] The encoding contains a total of 7,112 distinct variables.
[0.053] Attempting solve with solver <glucose4> ...
c 59 assumptions
[0.053] Executed solver; result: UNSAT.
[0.053] 
[0.053] *************************************
[0.053] * * *   M a k e s p a n     2   * * *
[0.053] *************************************
[0.063] Computed next depth properties: array size of 117.
[0.073] Instantiated 10,210 transitional clauses.
[0.096] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.096] Instantiated 123,595 universal clauses.
[0.096] Instantiated and added clauses for a total of 156,625 clauses.
[0.096] The encoding contains a total of 14,163 distinct variables.
[0.096] Attempting solve with solver <glucose4> ...
c 117 assumptions
c last restart ## conflicts  :  0 123 
[0.096] Executed solver; result: SAT.
[0.096] Solver returned SAT; a solution has been found at makespan 2.
115
solution 2153 1
730 2 1465 1399 85 3 1244 1400 112 4 64 1401 147 5 1466 1402 811 6 969 1403 222 7 833 1404 227 8 375 1405 953 9 1917 1406 259 10 1572 1407 274 11 1957 1408 1060 12 1637 1409 1130 13 2023 1410 844 14 289 1411 341 15 1276 1412 378 16 2035 1413 401 17 2065 1414 1196 18 1717 1415 478 19 1262 1416 1078 20 764 1417 525 21 1255 1418 22 1753 1419 621 23 1790 1420 628 24 1587 1421 671 25 1715 1422 1373 26 962 1423 691 27 608 1424 889 28 2153 1425 704 29 1203 1426 166 30 1320 1427 
[0.097] Exiting.

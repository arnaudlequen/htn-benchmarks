Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.003] Processed problem encoding.
[0.003] Calculated possible fact changes of composite elements.
[0.003] Initialized instantiation procedure.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     0   * * *
[0.003] *************************************
[0.004] Instantiated 457 initial clauses.
[0.004] The encoding contains a total of 255 distinct variables.
[0.004] Attempting solve with solver <glucose4> ...
c 10 assumptions
[0.004] Executed solver; result: UNSAT.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     1   * * *
[0.004] *************************************
[0.004] Computed next depth properties: array size of 19.
[0.005] Instantiated 371 transitional clauses.
[0.006] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.006] Instantiated 1,572 universal clauses.
[0.006] Instantiated and added clauses for a total of 2,400 clauses.
[0.006] The encoding contains a total of 772 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 19 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     2   * * *
[0.006] *************************************
[0.006] Computed next depth properties: array size of 37.
[0.007] Instantiated 1,010 transitional clauses.
[0.010] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.010] Instantiated 5,595 universal clauses.
[0.010] Instantiated and added clauses for a total of 9,005 clauses.
[0.010] The encoding contains a total of 1,523 distinct variables.
[0.010] Attempting solve with solver <glucose4> ...
c 37 assumptions
c last restart ## conflicts  :  0 43 
[0.010] Executed solver; result: SAT.
[0.010] Solver returned SAT; a solution has been found at makespan 2.
36
solution 229 1
70 2 140 130 33 3 193 131 36 4 160 132 92 5 172 133 107 6 24 134 111 7 207 135 123 8 198 136 36 9 211 137 128 10 229 138 
[0.010] Exiting.

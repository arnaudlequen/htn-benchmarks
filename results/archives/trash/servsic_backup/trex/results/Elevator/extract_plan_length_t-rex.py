import re
import numpy as np
import matplotlib.pyplot as plt
import os

MAX = 20

plan_ids = []
plan_length = []

time_ids = []
time = []

def main():
    filenames= os.listdir(".") # get all files' and folders' names in the current directory

    for filename in filenames: # loop through all the files and folders
        if os.path.isdir(os.path.join(os.path.abspath("."), filename)) and filename != "sysinfo": # check whether the current object is a folder or not
            substr = filename.split("-")
            p = re.compile("[p][0-9][0-9]")
            id = -1
            for s in substr:
                if p.match(s):
                    id = int(re.sub("[p]", "", s))
                    break
            if id == -1:
                print("FAILED TO FETCH ID")
                continue


            try:
                in_file = open(filename + "/04_solution.txt", "rt")
                ln = 0
                for line in in_file:
                    l = line.split(":")
                    if len(l) > 1 and len(l[1]) > 2:
                        ln = int(l[0])
                plan_length.append(ln)
                plan_ids.append(id)

            except:
                pass

            try:
                in_file = open(filename + "/general_output", "rt")
                l = 0
                for line in in_file:
                    if "Total execution time:" in line:
                        l = int(re.sub("[^0-9]", "", line))
                        l /= 1000
                        break

                time.append(l)
                time_ids.append(id)

            except:
                pass

    print(plan_ids)
    print(plan_length)

    print(time_ids)
    print(time)

    plt.title("Elevator" + " (T-REX)")
    plt.plot(plan_ids, plan_length, 'ro')
    plt.xlabel("Problem")
    plt.ylabel("Plan length (# of actions)")
    plt.grid(True)
    plt.xticks(list(range(0, 151, 30)))
    plt.show()

main()

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 94 initial clauses.
[0.001] The encoding contains a total of 59 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 7.
[0.001] Instantiated 14 transitional clauses.
[0.001] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.001] Instantiated 149 universal clauses.
[0.001] Instantiated and added clauses for a total of 257 clauses.
[0.001] The encoding contains a total of 105 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     2   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 10.
[0.002] Instantiated 101 transitional clauses.
[0.002] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.002] Instantiated 389 universal clauses.
[0.002] Instantiated and added clauses for a total of 747 clauses.
[0.002] The encoding contains a total of 196 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 10 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     3   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 13.
[0.002] Instantiated 134 transitional clauses.
[0.002] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.002] Instantiated 398 universal clauses.
[0.002] Instantiated and added clauses for a total of 1,279 clauses.
[0.002] The encoding contains a total of 260 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     4   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 16.
[0.002] Instantiated 95 transitional clauses.
[0.003] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.003] Instantiated 473 universal clauses.
[0.003] Instantiated and added clauses for a total of 1,847 clauses.
[0.003] The encoding contains a total of 354 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 16 assumptions
[0.003] Executed solver; result: UNSAT.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     5   * * *
[0.003] *************************************
[0.003] Computed next depth properties: array size of 19.
[0.003] Instantiated 185 transitional clauses.
[0.003] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.003] Instantiated 734 universal clauses.
[0.003] Instantiated and added clauses for a total of 2,766 clauses.
[0.003] The encoding contains a total of 493 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 19 assumptions
c last restart ## conflicts  :  0 31 
[0.003] Executed solver; result: SAT.
[0.003] Solver returned SAT; a solution has been found at makespan 5.
9
solution 31 1
5 13 7 16 2 24 3 31 4 
[0.003] Exiting.

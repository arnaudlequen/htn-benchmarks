Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.005] Parsed head comment information.
[0.333] Processed problem encoding.
[0.433] Calculated possible fact changes of composite elements.
[0.445] Initialized instantiation procedure.
[0.445] 
[0.445] *************************************
[0.445] * * *   M a k e s p a n     0   * * *
[0.445] *************************************
[0.488] Instantiated 71,967 initial clauses.
[0.488] The encoding contains a total of 36,838 distinct variables.
[0.488] Attempting solve with solver <glucose4> ...
c 69 assumptions
[0.488] Executed solver; result: UNSAT.
[0.488] 
[0.488] *************************************
[0.488] * * *   M a k e s p a n     1   * * *
[0.488] *************************************
[0.568] Computed next depth properties: array size of 133.
[0.618] Instantiated 3,398 transitional clauses.
[0.736] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.736] Instantiated 157,994 universal clauses.
[0.736] Instantiated and added clauses for a total of 233,359 clauses.
[0.736] The encoding contains a total of 72,746 distinct variables.
[0.736] Attempting solve with solver <glucose4> ...
c 133 assumptions
[0.736] Executed solver; result: UNSAT.
[0.736] 
[0.736] *************************************
[0.736] * * *   M a k e s p a n     2   * * *
[0.736] *************************************
[0.868] Computed next depth properties: array size of 197.
[1.121] Instantiated 278,524 transitional clauses.
[1.863] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.863] Instantiated 8,458,858 universal clauses.
[1.863] Instantiated and added clauses for a total of 8,970,741 clauses.
[1.863] The encoding contains a total of 279,395 distinct variables.
[1.863] Attempting solve with solver <glucose4> ...
c 197 assumptions
[1.863] Executed solver; result: UNSAT.
[1.863] 
[1.863] *************************************
[1.863] * * *   M a k e s p a n     3   * * *
[1.863] *************************************
[2.032] Computed next depth properties: array size of 325.
[2.371] Instantiated 804,455 transitional clauses.
[3.119] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.119] Instantiated 8,291,120 universal clauses.
[3.119] Instantiated and added clauses for a total of 18,066,316 clauses.
[3.119] The encoding contains a total of 352,449 distinct variables.
[3.119] Attempting solve with solver <glucose4> ...
c 325 assumptions
[3.120] Executed solver; result: UNSAT.
[3.120] 
[3.120] *************************************
[3.120] * * *   M a k e s p a n     4   * * *
[3.120] *************************************
[3.343] Computed next depth properties: array size of 453.
[3.572] Instantiated 76,555 transitional clauses.
[4.454] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.454] Instantiated 8,432,813 universal clauses.
[4.454] Instantiated and added clauses for a total of 26,575,684 clauses.
[4.454] The encoding contains a total of 455,549 distinct variables.
[4.454] Attempting solve with solver <glucose4> ...
c 453 assumptions
[4.454] Executed solver; result: UNSAT.
[4.454] 
[4.454] *************************************
[4.454] * * *   M a k e s p a n     5   * * *
[4.454] *************************************
[4.853] Computed next depth properties: array size of 581.
[5.356] Instantiated 636,731 transitional clauses.
[8.088] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[8.088] Instantiated 36,425,339 universal clauses.
[8.088] Instantiated and added clauses for a total of 63,637,754 clauses.
[8.088] The encoding contains a total of 916,483 distinct variables.
[8.088] Attempting solve with solver <glucose4> ...
c 581 assumptions
c last restart ## conflicts  :  193 3511 
[9.812] Executed solver; result: SAT.
[9.812] Solver returned SAT; a solution has been found at makespan 5.
329
solution 32375 1
715 16495 736 16606 8 705 3403 726 3540 12 721 28519 743 28725 32 756 714 16388 735 16824 38 708 6706 729 7187 45 749 707 6829 728 7262 58 17049 72 17125 82 762 719 28508 741 29163 96 748 709 6757 730 7553 98 750 707 6835 728 7627 108 746 706 3395 727 3905 118 711 11652 732 12592 134 752 712 11583 733 12663 145 12753 155 12828 166 748 708 6690 729 8063 175 749 707 6843 728 8138 188 748 708 6699 729 8209 199 703 795 724 1493 208 749 709 6773 730 8356 222 13267 235 717 24734 739 26169 249 8601 254 753 710 11594 731 13466 264 744 704 788 725 1785 273 30487 294 760 720 28535 742 30550 303 750 708 6705 729 8939 307 751 712 11597 733 13831 323 749 707 6856 728 9087 330 747 705 3406 726 5000 339 748 709 6785 730 9232 355 761 721 28536 743 30988 373 762 719 28542 741 31061 386 758 718 24735 740 26899 395 759 716 24675 738 26971 407 755 715 16468 736 19380 416 756 713 16574 734 19454 425 750 707 6860 728 9744 435 757 718 24749 740 27191 452 759 717 24753 739 27264 462 748 709 6794 730 9962 470 753 711 11675 732 14855 484 760 720 28543 742 31791 500 10230 504 758 716 24681 738 27555 518 757 718 24761 740 27629 530 10453 533 754 715 16502 736 20256 548 750 708 6732 729 10545 555 20458 570 761 719 28555 741 32375 586 759 716 24689 738 27993 597 749 709 6807 730 10838 601 750 707 6885 728 10912 611 745 703 794 724 3026 620 15926 633 748 709 6813 730 11130 641 11270 652 752 710 11633 731 16094 663 744 704 828 725 3245 671 745 703 833 724 3318 683 16382 696 13756 17247 26886 28782 
[9.849] Exiting.

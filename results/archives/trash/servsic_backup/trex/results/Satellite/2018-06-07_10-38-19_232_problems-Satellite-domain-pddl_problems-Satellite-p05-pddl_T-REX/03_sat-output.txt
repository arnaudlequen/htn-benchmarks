Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.024] Processed problem encoding.
[0.027] Calculated possible fact changes of composite elements.
[0.028] Initialized instantiation procedure.
[0.028] 
[0.028] *************************************
[0.028] * * *   M a k e s p a n     0   * * *
[0.028] *************************************
[0.029] Instantiated 3,348 initial clauses.
[0.029] The encoding contains a total of 1,799 distinct variables.
[0.029] Attempting solve with solver <glucose4> ...
c 20 assumptions
[0.030] Executed solver; result: UNSAT.
[0.030] 
[0.030] *************************************
[0.030] * * *   M a k e s p a n     1   * * *
[0.030] *************************************
[0.032] Computed next depth properties: array size of 39.
[0.033] Instantiated 294 transitional clauses.
[0.038] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.038] Instantiated 6,520 universal clauses.
[0.038] Instantiated and added clauses for a total of 10,162 clauses.
[0.038] The encoding contains a total of 3,424 distinct variables.
[0.038] Attempting solve with solver <glucose4> ...
c 39 assumptions
[0.038] Executed solver; result: UNSAT.
[0.038] 
[0.038] *************************************
[0.038] * * *   M a k e s p a n     2   * * *
[0.038] *************************************
[0.043] Computed next depth properties: array size of 58.
[0.051] Instantiated 9,834 transitional clauses.
[0.066] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.066] Instantiated 64,839 universal clauses.
[0.066] Instantiated and added clauses for a total of 84,835 clauses.
[0.066] The encoding contains a total of 10,885 distinct variables.
[0.066] Attempting solve with solver <glucose4> ...
c 58 assumptions
[0.066] Executed solver; result: UNSAT.
[0.066] 
[0.066] *************************************
[0.066] * * *   M a k e s p a n     3   * * *
[0.066] *************************************
[0.071] Computed next depth properties: array size of 96.
[0.085] Instantiated 27,560 transitional clauses.
[0.098] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.098] Instantiated 61,399 universal clauses.
[0.098] Instantiated and added clauses for a total of 173,794 clauses.
[0.098] The encoding contains a total of 14,365 distinct variables.
[0.098] Attempting solve with solver <glucose4> ...
c 96 assumptions
[0.098] Executed solver; result: UNSAT.
[0.098] 
[0.098] *************************************
[0.098] * * *   M a k e s p a n     4   * * *
[0.098] *************************************
[0.105] Computed next depth properties: array size of 134.
[0.111] Instantiated 3,877 transitional clauses.
[0.126] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.126] Instantiated 67,658 universal clauses.
[0.126] Instantiated and added clauses for a total of 245,329 clauses.
[0.126] The encoding contains a total of 19,093 distinct variables.
[0.126] Attempting solve with solver <glucose4> ...
c 134 assumptions
[0.126] Executed solver; result: UNSAT.
[0.126] 
[0.126] *************************************
[0.126] * * *   M a k e s p a n     5   * * *
[0.126] *************************************
[0.137] Computed next depth properties: array size of 172.
[0.149] Instantiated 23,361 transitional clauses.
[0.178] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.178] Instantiated 268,832 universal clauses.
[0.178] Instantiated and added clauses for a total of 537,522 clauses.
[0.178] The encoding contains a total of 36,165 distinct variables.
[0.178] Attempting solve with solver <glucose4> ...
c 172 assumptions
c last restart ## conflicts  :  48 401 
[0.190] Executed solver; result: SAT.
[0.190] Solver returned SAT; a solution has been found at makespan 5.
81
solution 1259 1
76 167 87 179 2 97 75 108 86 200 7 96 77 133 88 225 12 84 973 92 996 20 78 89 564 24 99 79 472 90 588 27 618 28 81 1077 94 1259 34 667 36 694 37 98 76 158 87 323 42 97 75 119 86 344 48 385 52 767 56 795 57 820 58 96 77 145 88 393 61 98 75 125 86 416 66 96 76 174 87 443 70 
[0.191] Exiting.

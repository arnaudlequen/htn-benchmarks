Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.032] Parsed head comment information.
[1.361] Processed problem encoding.
[2.299] Calculated possible fact changes of composite elements.
[2.421] Initialized instantiation procedure.
[2.421] 
[2.421] *************************************
[2.421] * * *   M a k e s p a n     0   * * *
[2.421] *************************************
[2.797] Instantiated 417,041 initial clauses.
[2.797] The encoding contains a total of 210,937 distinct variables.
[2.797] Attempting solve with solver <glucose4> ...
c 134 assumptions
[2.800] Executed solver; result: UNSAT.
[2.800] 
[2.800] *************************************
[2.800] * * *   M a k e s p a n     1   * * *
[2.800] *************************************
[3.425] Computed next depth properties: array size of 264.
[3.915] Instantiated 8,252 transitional clauses.
[5.080] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[5.080] Instantiated 879,316 universal clauses.
[5.080] Instantiated and added clauses for a total of 1,304,609 clauses.
[5.080] The encoding contains a total of 418,333 distinct variables.
[5.080] Attempting solve with solver <glucose4> ...
c 264 assumptions
[5.080] Executed solver; result: UNSAT.
[5.080] 
[5.080] *************************************
[5.080] * * *   M a k e s p a n     2   * * *
[5.080] *************************************
[6.329] Computed next depth properties: array size of 394.
[7.716] Instantiated 1,423,165 transitional clauses.
Interrupt signal (15) received.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.095] Processed problem encoding.
[0.118] Calculated possible fact changes of composite elements.
[0.119] Initialized instantiation procedure.
[0.119] 
[0.119] *************************************
[0.119] * * *   M a k e s p a n     0   * * *
[0.119] *************************************
[0.120] Instantiated 2,751 initial clauses.
[0.120] The encoding contains a total of 1,838 distinct variables.
[0.120] Attempting solve with solver <glucose4> ...
c 6 assumptions
[0.120] Executed solver; result: UNSAT.
[0.120] 
[0.120] *************************************
[0.120] * * *   M a k e s p a n     1   * * *
[0.120] *************************************
[0.122] Computed next depth properties: array size of 21.
[0.126] Instantiated 5,667 transitional clauses.
[0.131] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.131] Instantiated 23,214 universal clauses.
[0.131] Instantiated and added clauses for a total of 31,632 clauses.
[0.131] The encoding contains a total of 5,975 distinct variables.
[0.131] Attempting solve with solver <glucose4> ...
c 21 assumptions
[0.131] Executed solver; result: UNSAT.
[0.131] 
[0.131] *************************************
[0.131] * * *   M a k e s p a n     2   * * *
[0.131] *************************************
[0.136] Computed next depth properties: array size of 61.
[0.157] Instantiated 24,351 transitional clauses.
[0.171] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.171] Instantiated 125,863 universal clauses.
[0.171] Instantiated and added clauses for a total of 181,846 clauses.
[0.171] The encoding contains a total of 15,628 distinct variables.
[0.171] Attempting solve with solver <glucose4> ...
c 61 assumptions
[0.171] Executed solver; result: UNSAT.
[0.171] 
[0.171] *************************************
[0.171] * * *   M a k e s p a n     3   * * *
[0.171] *************************************
[0.180] Computed next depth properties: array size of 121.
[0.220] Instantiated 62,265 transitional clauses.
[0.252] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.252] Instantiated 354,863 universal clauses.
[0.252] Instantiated and added clauses for a total of 598,974 clauses.
[0.252] The encoding contains a total of 34,065 distinct variables.
[0.252] Attempting solve with solver <glucose4> ...
c 121 assumptions
[0.260] Executed solver; result: UNSAT.
[0.260] 
[0.260] *************************************
[0.260] * * *   M a k e s p a n     4   * * *
[0.260] *************************************
[0.274] Computed next depth properties: array size of 181.
[0.347] Instantiated 115,633 transitional clauses.
[0.410] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.410] Instantiated 719,589 universal clauses.
[0.410] Instantiated and added clauses for a total of 1,434,196 clauses.
[0.410] The encoding contains a total of 58,344 distinct variables.
[0.410] Attempting solve with solver <glucose4> ...
c 181 assumptions
c last restart ## conflicts  :  22 235 
[0.432] Executed solver; result: SAT.
[0.432] Solver returned SAT; a solution has been found at makespan 4.
31
solution 505 1
468 177 95 505 98 158 141 128 145 43 9 84 271 272 107 47 10 92 166 89 169 42 8 87 80 18 376 194 103 25 3 
[0.433] Exiting.

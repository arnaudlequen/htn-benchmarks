Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.014] Processed problem encoding.
[0.016] Calculated possible fact changes of composite elements.
[0.017] Initialized instantiation procedure.
[0.017] 
[0.017] *************************************
[0.017] * * *   M a k e s p a n     0   * * *
[0.017] *************************************
[0.018] Instantiated 1,670 initial clauses.
[0.018] The encoding contains a total of 1,043 distinct variables.
[0.018] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.018] Executed solver; result: UNSAT.
[0.018] 
[0.018] *************************************
[0.018] * * *   M a k e s p a n     1   * * *
[0.018] *************************************
[0.019] Computed next depth properties: array size of 25.
[0.021] Instantiated 2,231 transitional clauses.
[0.025] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.025] Instantiated 9,271 universal clauses.
[0.025] Instantiated and added clauses for a total of 13,172 clauses.
[0.025] The encoding contains a total of 3,369 distinct variables.
[0.025] Attempting solve with solver <glucose4> ...
c 25 assumptions
[0.025] Executed solver; result: UNSAT.
[0.025] 
[0.025] *************************************
[0.025] * * *   M a k e s p a n     2   * * *
[0.025] *************************************
[0.028] Computed next depth properties: array size of 73.
[0.034] Instantiated 7,208 transitional clauses.
[0.044] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.044] Instantiated 31,954 universal clauses.
[0.044] Instantiated and added clauses for a total of 52,334 clauses.
[0.044] The encoding contains a total of 8,425 distinct variables.
[0.044] Attempting solve with solver <glucose4> ...
c 73 assumptions
[0.045] Executed solver; result: UNSAT.
[0.045] 
[0.045] *************************************
[0.045] * * *   M a k e s p a n     3   * * *
[0.045] *************************************
[0.050] Computed next depth properties: array size of 145.
[0.064] Instantiated 21,071 transitional clauses.
[0.084] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.084] Instantiated 86,323 universal clauses.
[0.084] Instantiated and added clauses for a total of 159,728 clauses.
[0.084] The encoding contains a total of 17,300 distinct variables.
[0.084] Attempting solve with solver <glucose4> ...
c 145 assumptions
[0.085] Executed solver; result: UNSAT.
[0.085] 
[0.085] *************************************
[0.085] * * *   M a k e s p a n     4   * * *
[0.085] *************************************
[0.091] Computed next depth properties: array size of 217.
[0.111] Instantiated 37,076 transitional clauses.
[0.141] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.141] Instantiated 175,297 universal clauses.
[0.141] Instantiated and added clauses for a total of 372,101 clauses.
[0.141] The encoding contains a total of 29,151 distinct variables.
[0.141] Attempting solve with solver <glucose4> ...
c 217 assumptions
c last restart ## conflicts  :  7 241 
[0.150] Executed solver; result: SAT.
[0.150] Solver returned SAT; a solution has been found at makespan 4.
32
solution 158 1
45 54 56 42 149 104 93 40 94 22 6 47 23 7 158 50 83 112 80 69 71 32 11 46 20 5 49 28 8 42 38 14 
[0.151] Exiting.

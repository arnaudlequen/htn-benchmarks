Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.002] Processed problem encoding.
[0.002] Calculated possible fact changes of composite elements.
[0.002] Initialized instantiation procedure.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     0   * * *
[0.002] *************************************
[0.003] Instantiated 216 initial clauses.
[0.003] The encoding contains a total of 143 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.003] Executed solver; result: UNSAT.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     1   * * *
[0.003] *************************************
[0.003] Computed next depth properties: array size of 9.
[0.003] Instantiated 202 transitional clauses.
[0.003] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.003] Instantiated 809 universal clauses.
[0.003] Instantiated and added clauses for a total of 1,227 clauses.
[0.003] The encoding contains a total of 381 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 9 assumptions
[0.004] Executed solver; result: UNSAT.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     2   * * *
[0.004] *************************************
[0.004] Computed next depth properties: array size of 25.
[0.004] Instantiated 584 transitional clauses.
[0.005] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.005] Instantiated 2,213 universal clauses.
[0.005] Instantiated and added clauses for a total of 4,024 clauses.
[0.005] The encoding contains a total of 829 distinct variables.
[0.005] Attempting solve with solver <glucose4> ...
c 25 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     3   * * *
[0.006] *************************************
[0.006] Computed next depth properties: array size of 49.
[0.007] Instantiated 868 transitional clauses.
[0.008] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.008] Instantiated 3,577 universal clauses.
[0.008] Instantiated and added clauses for a total of 8,469 clauses.
[0.008] The encoding contains a total of 1,451 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 49 assumptions
c last restart ## conflicts  :  0 61 
[0.009] Executed solver; result: SAT.
[0.009] Solver returned SAT; a solution has been found at makespan 3.
10
solution 29 1
24 26 11 27 29 8 4 16 6 3 
[0.009] Exiting.

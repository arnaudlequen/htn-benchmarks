Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.035] Processed problem encoding.
[0.043] Calculated possible fact changes of composite elements.
[0.045] Initialized instantiation procedure.
[0.045] 
[0.045] *************************************
[0.045] * * *   M a k e s p a n     0   * * *
[0.045] *************************************
[0.047] Instantiated 3,772 initial clauses.
[0.047] The encoding contains a total of 2,265 distinct variables.
[0.047] Attempting solve with solver <glucose4> ...
c 8 assumptions
[0.047] Executed solver; result: UNSAT.
[0.047] 
[0.047] *************************************
[0.047] * * *   M a k e s p a n     1   * * *
[0.047] *************************************
[0.048] Computed next depth properties: array size of 29.
[0.052] Instantiated 3,542 transitional clauses.
[0.058] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.058] Instantiated 17,955 universal clauses.
[0.058] Instantiated and added clauses for a total of 25,269 clauses.
[0.058] The encoding contains a total of 7,120 distinct variables.
[0.058] Attempting solve with solver <glucose4> ...
c 29 assumptions
[0.058] Executed solver; result: UNSAT.
[0.058] 
[0.058] *************************************
[0.058] * * *   M a k e s p a n     2   * * *
[0.058] *************************************
[0.063] Computed next depth properties: array size of 85.
[0.075] Instantiated 13,364 transitional clauses.
[0.092] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.092] Instantiated 67,289 universal clauses.
[0.092] Instantiated and added clauses for a total of 105,922 clauses.
[0.092] The encoding contains a total of 18,199 distinct variables.
[0.092] Attempting solve with solver <glucose4> ...
c 85 assumptions
[0.093] Executed solver; result: UNSAT.
[0.093] 
[0.093] *************************************
[0.093] * * *   M a k e s p a n     3   * * *
[0.093] *************************************
[0.103] Computed next depth properties: array size of 169.
[0.136] Instantiated 53,241 transitional clauses.
[0.174] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.174] Instantiated 307,938 universal clauses.
[0.174] Instantiated and added clauses for a total of 467,101 clauses.
[0.174] The encoding contains a total of 38,937 distinct variables.
[0.174] Attempting solve with solver <glucose4> ...
c 169 assumptions
[0.174] Executed solver; result: UNSAT.
[0.174] 
[0.174] *************************************
[0.174] * * *   M a k e s p a n     4   * * *
[0.174] *************************************
[0.184] Computed next depth properties: array size of 253.
[0.228] Instantiated 100,922 transitional clauses.
[0.302] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.302] Instantiated 795,290 universal clauses.
[0.302] Instantiated and added clauses for a total of 1,363,313 clauses.
[0.302] The encoding contains a total of 66,823 distinct variables.
[0.302] Attempting solve with solver <glucose4> ...
c 253 assumptions
[0.319] Executed solver; result: UNSAT.
[0.319] 
[0.319] *************************************
[0.319] * * *   M a k e s p a n     5   * * *
[0.319] *************************************
[0.331] Computed next depth properties: array size of 337.
[0.378] Instantiated 115,684 transitional clauses.
[0.489] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.489] Instantiated 1,326,589 universal clauses.
[0.489] Instantiated and added clauses for a total of 2,805,586 clauses.
[0.489] The encoding contains a total of 100,572 distinct variables.
[0.489] Attempting solve with solver <glucose4> ...
c 337 assumptions
c last restart ## conflicts  :  5 364 
[0.511] Executed solver; result: SAT.
[0.511] Solver returned SAT; a solution has been found at makespan 5.
36
solution 314 1
41 135 136 44 54 55 34 13 37 297 130 45 314 98 205 95 81 83 30 11 43 29 10 148 79 41 142 121 14 3 262 12 160 8 21 6 
[0.512] Exiting.

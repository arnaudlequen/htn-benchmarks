Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.014] Processed problem encoding.
[0.016] Calculated possible fact changes of composite elements.
[0.016] Initialized instantiation procedure.
[0.016] 
[0.017] *************************************
[0.017] * * *   M a k e s p a n     0   * * *
[0.017] *************************************
[0.018] Instantiated 1,537 initial clauses.
[0.018] The encoding contains a total of 967 distinct variables.
[0.018] Attempting solve with solver <glucose4> ...
c 6 assumptions
[0.018] Executed solver; result: UNSAT.
[0.018] 
[0.018] *************************************
[0.018] * * *   M a k e s p a n     1   * * *
[0.018] *************************************
[0.018] Computed next depth properties: array size of 21.
[0.020] Instantiated 1,881 transitional clauses.
[0.024] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.024] Instantiated 7,809 universal clauses.
[0.024] Instantiated and added clauses for a total of 11,227 clauses.
[0.024] The encoding contains a total of 2,913 distinct variables.
[0.024] Attempting solve with solver <glucose4> ...
c 21 assumptions
[0.024] Executed solver; result: UNSAT.
[0.024] 
[0.024] *************************************
[0.024] * * *   M a k e s p a n     2   * * *
[0.024] *************************************
[0.026] Computed next depth properties: array size of 61.
[0.031] Instantiated 5,795 transitional clauses.
[0.039] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.039] Instantiated 25,567 universal clauses.
[0.039] Instantiated and added clauses for a total of 42,589 clauses.
[0.039] The encoding contains a total of 6,970 distinct variables.
[0.039] Attempting solve with solver <glucose4> ...
c 61 assumptions
c last restart ## conflicts  :  4 74 
[0.041] Executed solver; result: SAT.
[0.041] Solver returned SAT; a solution has been found at makespan 2.
23
solution 153 1
33 153 87 41 21 7 119 73 100 3 37 106 82 23 8 19 6 40 56 58 33 29 11 
[0.041] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.030] Processed problem encoding.
[0.035] Calculated possible fact changes of composite elements.
[0.036] Initialized instantiation procedure.
[0.036] 
[0.036] *************************************
[0.036] * * *   M a k e s p a n     0   * * *
[0.036] *************************************
[0.038] Instantiated 2,688 initial clauses.
[0.038] The encoding contains a total of 1,662 distinct variables.
[0.038] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.038] Executed solver; result: UNSAT.
[0.038] 
[0.038] *************************************
[0.038] * * *   M a k e s p a n     1   * * *
[0.038] *************************************
[0.039] Computed next depth properties: array size of 25.
[0.041] Instantiated 2,660 transitional clauses.
[0.045] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.045] Instantiated 9,957 universal clauses.
[0.045] Instantiated and added clauses for a total of 15,305 clauses.
[0.045] The encoding contains a total of 4,348 distinct variables.
[0.045] Attempting solve with solver <glucose4> ...
c 25 assumptions
[0.046] Executed solver; result: UNSAT.
[0.046] 
[0.046] *************************************
[0.046] * * *   M a k e s p a n     2   * * *
[0.046] *************************************
[0.049] Computed next depth properties: array size of 73.
[0.057] Instantiated 9,506 transitional clauses.
[0.068] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.068] Instantiated 43,697 universal clauses.
[0.068] Instantiated and added clauses for a total of 68,508 clauses.
[0.068] The encoding contains a total of 9,452 distinct variables.
[0.068] Attempting solve with solver <glucose4> ...
c 73 assumptions
[0.069] Executed solver; result: UNSAT.
[0.069] 
[0.069] *************************************
[0.069] * * *   M a k e s p a n     3   * * *
[0.069] *************************************
[0.075] Computed next depth properties: array size of 145.
[0.089] Instantiated 20,864 transitional clauses.
[0.109] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.109] Instantiated 92,867 universal clauses.
[0.109] Instantiated and added clauses for a total of 182,239 clauses.
[0.109] The encoding contains a total of 18,576 distinct variables.
[0.109] Attempting solve with solver <glucose4> ...
c 145 assumptions
[0.109] Executed solver; result: UNSAT.
[0.109] 
[0.109] *************************************
[0.109] * * *   M a k e s p a n     4   * * *
[0.109] *************************************
[0.117] Computed next depth properties: array size of 217.
[0.141] Instantiated 43,790 transitional clauses.
[0.167] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.167] Instantiated 198,677 universal clauses.
[0.167] Instantiated and added clauses for a total of 424,706 clauses.
[0.167] The encoding contains a total of 30,862 distinct variables.
[0.167] Attempting solve with solver <glucose4> ...
c 217 assumptions
c last restart ## conflicts  :  4 236 
[0.174] Executed solver; result: SAT.
[0.174] Solver returned SAT; a solution has been found at makespan 4.
27
solution 257 1
27 257 63 133 223 121 117 119 46 87 41 88 10 3 25 11 4 75 13 5 204 6 147 7 62 19 8 
[0.175] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.078] Processed problem encoding.
[0.114] Calculated possible fact changes of composite elements.
[0.120] Initialized instantiation procedure.
[0.120] 
[0.120] *************************************
[0.120] * * *   M a k e s p a n     0   * * *
[0.120] *************************************
[0.123] Instantiated 10,130 initial clauses.
[0.123] The encoding contains a total of 6,078 distinct variables.
[0.123] Attempting solve with solver <glucose4> ...
c 12 assumptions
[0.124] Executed solver; result: UNSAT.
[0.124] 
[0.124] *************************************
[0.124] * * *   M a k e s p a n     1   * * *
[0.124] *************************************
[0.127] Computed next depth properties: array size of 45.
[0.135] Instantiated 11,258 transitional clauses.
[0.148] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.148] Instantiated 59,329 universal clauses.
[0.148] Instantiated and added clauses for a total of 80,717 clauses.
[0.148] The encoding contains a total of 20,875 distinct variables.
[0.148] Attempting solve with solver <glucose4> ...
c 45 assumptions
[0.148] Executed solver; result: UNSAT.
[0.148] 
[0.148] *************************************
[0.148] * * *   M a k e s p a n     2   * * *
[0.148] *************************************
[0.160] Computed next depth properties: array size of 133.
[0.190] Instantiated 51,299 transitional clauses.
[0.232] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.232] Instantiated 301,003 universal clauses.
[0.232] Instantiated and added clauses for a total of 433,019 clauses.
[0.232] The encoding contains a total of 64,829 distinct variables.
[0.232] Attempting solve with solver <glucose4> ...
c 133 assumptions
[0.233] Executed solver; result: UNSAT.
[0.233] 
[0.233] *************************************
[0.233] * * *   M a k e s p a n     3   * * *
[0.233] *************************************
[0.266] Computed next depth properties: array size of 265.
[0.417] Instantiated 278,013 transitional clauses.
[0.702] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.702] Instantiated 3,630,932 universal clauses.
[0.702] Instantiated and added clauses for a total of 4,341,964 clauses.
[0.702] The encoding contains a total of 153,546 distinct variables.
[0.702] Attempting solve with solver <glucose4> ...
c 265 assumptions
[0.703] Executed solver; result: UNSAT.
[0.703] 
[0.703] *************************************
[0.703] * * *   M a k e s p a n     4   * * *
[0.703] *************************************
[0.754] Computed next depth properties: array size of 397.
[1.096] Instantiated 521,584 transitional clauses.
[1.852] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.852] Instantiated 10,489,088 universal clauses.
[1.852] Instantiated and added clauses for a total of 15,352,636 clauses.
[1.852] The encoding contains a total of 274,825 distinct variables.
[1.852] Attempting solve with solver <glucose4> ...
c 397 assumptions
[1.855] Executed solver; result: UNSAT.
[1.855] 
[1.855] *************************************
[1.855] * * *   M a k e s p a n     5   * * *
[1.855] *************************************
[1.925] Computed next depth properties: array size of 529.
[2.247] Instantiated 588,956 transitional clauses.
[3.514] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.514] Instantiated 17,793,774 universal clauses.
[3.514] Instantiated and added clauses for a total of 33,735,366 clauses.
[3.514] The encoding contains a total of 423,762 distinct variables.
[3.514] Attempting solve with solver <glucose4> ...
c 529 assumptions
[3.611] Executed solver; result: UNSAT.
[3.611] 
[3.611] *************************************
[3.611] * * *   M a k e s p a n     6   * * *
[3.611] *************************************
[3.694] Computed next depth properties: array size of 661.
[4.042] Instantiated 643,476 transitional clauses.
[5.754] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[5.754] Instantiated 25,098,460 universal clauses.
[5.754] Instantiated and added clauses for a total of 59,477,302 clauses.
[5.754] The encoding contains a total of 600,091 distinct variables.
[5.754] Attempting solve with solver <glucose4> ...
c 661 assumptions
c last restart ## conflicts  :  30 728 
[6.017] Executed solver; result: SAT.
[6.017] Solver returned SAT; a solution has been found at makespan 6.
62
solution 804 1
93 804 184 761 181 634 178 589 169 546 166 88 493 130 278 115 99 100 66 21 85 478 217 347 202 260 87 194 186 187 92 30 3 60 18 76 26 84 419 95 164 374 154 88 54 15 70 23 84 150 151 68 22 37 7 46 10 89 82 29 50 14 
[6.024] Exiting.

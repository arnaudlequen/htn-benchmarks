Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.047] Processed problem encoding.
[0.058] Calculated possible fact changes of composite elements.
[0.061] Initialized instantiation procedure.
[0.061] 
[0.061] *************************************
[0.061] * * *   M a k e s p a n     0   * * *
[0.061] *************************************
[0.063] Instantiated 5,210 initial clauses.
[0.063] The encoding contains a total of 3,199 distinct variables.
[0.063] Attempting solve with solver <glucose4> ...
c 11 assumptions
[0.063] Executed solver; result: UNSAT.
[0.063] 
[0.063] *************************************
[0.063] * * *   M a k e s p a n     1   * * *
[0.063] *************************************
[0.065] Computed next depth properties: array size of 41.
[0.072] Instantiated 6,875 transitional clauses.
[0.080] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.080] Instantiated 31,039 universal clauses.
[0.080] Instantiated and added clauses for a total of 43,124 clauses.
[0.080] The encoding contains a total of 10,841 distinct variables.
[0.080] Attempting solve with solver <glucose4> ...
c 41 assumptions
[0.080] Executed solver; result: UNSAT.
[0.080] 
[0.080] *************************************
[0.080] * * *   M a k e s p a n     2   * * *
[0.080] *************************************
[0.087] Computed next depth properties: array size of 121.
[0.103] Instantiated 26,072 transitional clauses.
[0.122] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.122] Instantiated 134,532 universal clauses.
[0.122] Instantiated and added clauses for a total of 203,728 clauses.
[0.122] The encoding contains a total of 30,793 distinct variables.
[0.122] Attempting solve with solver <glucose4> ...
c 121 assumptions
[0.122] Executed solver; result: UNSAT.
[0.122] 
[0.122] *************************************
[0.122] * * *   M a k e s p a n     3   * * *
[0.122] *************************************
[0.133] Computed next depth properties: array size of 241.
[0.178] Instantiated 110,551 transitional clauses.
[0.249] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.249] Instantiated 748,333 universal clauses.
[0.249] Instantiated and added clauses for a total of 1,062,612 clauses.
[0.249] The encoding contains a total of 69,156 distinct variables.
[0.249] Attempting solve with solver <glucose4> ...
c 241 assumptions
[0.249] Executed solver; result: UNSAT.
[0.249] 
[0.249] *************************************
[0.249] * * *   M a k e s p a n     4   * * *
[0.249] *************************************
[0.270] Computed next depth properties: array size of 361.
[0.352] Instantiated 203,064 transitional clauses.
[0.515] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.515] Instantiated 1,947,557 universal clauses.
[0.515] Instantiated and added clauses for a total of 3,213,233 clauses.
[0.515] The encoding contains a total of 121,227 distinct variables.
[0.515] Attempting solve with solver <glucose4> ...
c 361 assumptions
[0.541] Executed solver; result: UNSAT.
[0.541] 
[0.541] *************************************
[0.541] * * *   M a k e s p a n     5   * * *
[0.541] *************************************
[0.564] Computed next depth properties: array size of 481.
[0.659] Instantiated 233,300 transitional clauses.
[0.928] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.928] Instantiated 3,254,790 universal clauses.
[0.928] Instantiated and added clauses for a total of 6,701,323 clauses.
[0.928] The encoding contains a total of 185,017 distinct variables.
[0.928] Attempting solve with solver <glucose4> ...
c 481 assumptions
c last restart ## conflicts  :  10 527 
[0.987] Executed solver; result: SAT.
[0.987] Solver returned SAT; a solution has been found at makespan 5.
50
solution 382 1
76 338 175 83 261 100 206 97 90 91 73 26 76 253 163 196 82 155 147 149 41 10 84 382 86 143 354 140 135 136 33 6 79 55 17 43 11 49 14 81 30 3 38 7 77 65 21 82 72 25 
[0.988] Exiting.

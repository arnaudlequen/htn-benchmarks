Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.006] Processed problem encoding.
[0.006] Calculated possible fact changes of composite elements.
[0.006] Initialized instantiation procedure.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     0   * * *
[0.006] *************************************
[0.007] Instantiated 722 initial clauses.
[0.007] The encoding contains a total of 454 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 5 assumptions
[0.007] Executed solver; result: UNSAT.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     1   * * *
[0.007] *************************************
[0.008] Computed next depth properties: array size of 17.
[0.009] Instantiated 845 transitional clauses.
[0.011] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.011] Instantiated 3,499 universal clauses.
[0.011] Instantiated and added clauses for a total of 5,066 clauses.
[0.011] The encoding contains a total of 1,382 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 17 assumptions
[0.011] Executed solver; result: UNSAT.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     2   * * *
[0.011] *************************************
[0.012] Computed next depth properties: array size of 49.
[0.014] Instantiated 2,564 transitional clauses.
[0.017] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.017] Instantiated 10,377 universal clauses.
[0.017] Instantiated and added clauses for a total of 18,007 clauses.
[0.017] The encoding contains a total of 3,190 distinct variables.
[0.017] Attempting solve with solver <glucose4> ...
c 49 assumptions
[0.018] Executed solver; result: UNSAT.
[0.018] 
[0.018] *************************************
[0.018] * * *   M a k e s p a n     3   * * *
[0.018] *************************************
[0.019] Computed next depth properties: array size of 97.
[0.023] Instantiated 5,491 transitional clauses.
[0.029] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.029] Instantiated 20,782 universal clauses.
[0.029] Instantiated and added clauses for a total of 44,280 clauses.
[0.029] The encoding contains a total of 6,081 distinct variables.
[0.029] Attempting solve with solver <glucose4> ...
c 97 assumptions
c last restart ## conflicts  :  5 115 
[0.031] Executed solver; result: SAT.
[0.031] Solver returned SAT; a solution has been found at makespan 3.
17
solution 85 1
22 85 64 29 33 34 17 7 22 60 61 30 12 5 26 9 3 
[0.032] Exiting.

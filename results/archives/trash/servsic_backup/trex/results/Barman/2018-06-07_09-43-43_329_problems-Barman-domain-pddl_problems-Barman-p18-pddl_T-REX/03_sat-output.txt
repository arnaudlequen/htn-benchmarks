Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.004] Processed problem encoding.
[0.005] Calculated possible fact changes of composite elements.
[0.005] Initialized instantiation procedure.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     0   * * *
[0.005] *************************************
[0.006] Instantiated 1,136 initial clauses.
[0.006] The encoding contains a total of 668 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.006] Computed next depth properties: array size of 55.
[0.007] Instantiated 266 transitional clauses.
[0.008] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.008] Instantiated 1,889 universal clauses.
[0.008] Instantiated and added clauses for a total of 3,291 clauses.
[0.008] The encoding contains a total of 1,188 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 55 assumptions
[0.008] Executed solver; result: UNSAT.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     2   * * *
[0.008] *************************************
[0.010] Computed next depth properties: array size of 175.
[0.011] Instantiated 890 transitional clauses.
[0.015] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.015] Instantiated 6,437 universal clauses.
[0.015] Instantiated and added clauses for a total of 10,618 clauses.
[0.015] The encoding contains a total of 2,350 distinct variables.
[0.015] Attempting solve with solver <glucose4> ...
c 175 assumptions
c last restart ## conflicts  :  11 195 
[0.017] Executed solver; result: SAT.
[0.017] Solver returned SAT; a solution has been found at makespan 2.
174
solution 326 1
2 32 112 113 34 207 110 30 254 296 297 298 256 257 4 38 120 121 40 211 118 36 254 292 306 293 256 257 6 44 128 129 46 215 126 42 254 284 308 285 256 257 9 50 137 138 53 221 141 51 259 260 311 263 261 262 10 58 144 145 54 223 142 55 254 288 312 289 256 257 12 64 152 153 60 227 150 61 254 268 314 269 256 257 14 66 158 159 70 231 164 67 254 272 316 273 256 257 16 76 168 169 72 235 166 73 254 302 318 303 256 257 18 78 174 175 82 239 180 79 254 264 320 265 256 257 20 88 182 183 86 243 188 84 254 276 322 277 256 257 22 94 190 191 92 247 196 90 254 280 324 281 256 257 24 96 97 26 102 100 28 106 200 201 108 251 198 104 254 296 326 298 256 257 
[0.017] Exiting.

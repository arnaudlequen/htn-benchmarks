Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.004] Processed problem encoding.
[0.004] Calculated possible fact changes of composite elements.
[0.004] Initialized instantiation procedure.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     0   * * *
[0.004] *************************************
[0.005] Instantiated 1,093 initial clauses.
[0.005] The encoding contains a total of 642 distinct variables.
[0.005] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.005] Executed solver; result: UNSAT.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     1   * * *
[0.005] *************************************
[0.005] Computed next depth properties: array size of 52.
[0.005] Instantiated 254 transitional clauses.
[0.007] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.007] Instantiated 1,796 universal clauses.
[0.007] Instantiated and added clauses for a total of 3,143 clauses.
[0.007] The encoding contains a total of 1,144 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 52 assumptions
[0.007] Executed solver; result: UNSAT.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     2   * * *
[0.007] *************************************
[0.008] Computed next depth properties: array size of 172.
[0.009] Instantiated 878 transitional clauses.
[0.012] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.012] Instantiated 6,344 universal clauses.
[0.012] Instantiated and added clauses for a total of 10,365 clauses.
[0.012] The encoding contains a total of 2,297 distinct variables.
[0.012] Attempting solve with solver <glucose4> ...
c 172 assumptions
c last restart ## conflicts  :  9 192 
[0.013] Executed solver; result: SAT.
[0.013] Solver returned SAT; a solution has been found at makespan 2.
171
solution 320 1
2 28 106 107 32 201 104 29 248 282 283 284 250 251 5 36 117 118 39 207 113 37 253 290 301 291 255 256 6 42 120 121 44 209 126 40 248 266 302 267 250 251 9 51 131 132 48 215 135 49 253 264 305 265 255 256 10 52 138 139 56 217 136 53 248 278 306 279 250 251 12 62 144 145 58 221 150 59 248 270 308 271 250 251 14 64 154 155 68 225 152 65 248 274 310 275 250 251 16 70 162 163 74 229 160 71 248 292 312 293 250 251 18 80 170 171 78 233 168 76 248 296 314 297 250 251 20 86 176 177 82 237 182 83 248 258 316 259 250 251 22 92 184 185 88 241 190 89 248 249 318 252 250 251 24 96 94 26 98 194 195 102 245 192 99 248 278 320 279 250 251 
[0.014] Exiting.

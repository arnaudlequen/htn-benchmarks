Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.005] Processed problem encoding.
[0.005] Calculated possible fact changes of composite elements.
[0.005] Initialized instantiation procedure.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     0   * * *
[0.005] *************************************
[0.006] Instantiated 1,171 initial clauses.
[0.006] The encoding contains a total of 686 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.007] Computed next depth properties: array size of 55.
[0.007] Instantiated 266 transitional clauses.
[0.008] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.008] Instantiated 1,923 universal clauses.
[0.009] Instantiated and added clauses for a total of 3,360 clauses.
[0.009] The encoding contains a total of 1,206 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 55 assumptions
[0.009] Executed solver; result: UNSAT.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     2   * * *
[0.009] *************************************
[0.010] Computed next depth properties: array size of 175.
[0.011] Instantiated 890 transitional clauses.
[0.015] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.015] Instantiated 6,471 universal clauses.
[0.015] Instantiated and added clauses for a total of 10,721 clauses.
[0.015] The encoding contains a total of 2,368 distinct variables.
[0.015] Attempting solve with solver <glucose4> ...
c 175 assumptions
c last restart ## conflicts  :  10 199 
[0.017] Executed solver; result: SAT.
[0.017] Solver returned SAT; a solution has been found at makespan 2.
174
solution 326 1
2 34 112 113 32 207 110 30 254 276 277 278 256 257 5 39 123 124 41 213 119 37 259 270 307 271 261 262 7 47 131 132 45 217 127 43 259 292 309 293 261 262 8 50 134 135 52 219 140 48 254 282 310 283 256 257 10 54 144 145 58 223 142 55 254 272 312 273 256 257 12 64 152 153 62 227 150 60 254 298 314 299 256 257 14 70 158 159 68 231 164 66 254 264 316 265 256 257 16 74 166 167 76 235 172 72 254 294 318 295 256 257 18 80 176 177 82 239 174 78 254 302 320 303 256 257 20 86 182 183 88 243 188 84 254 255 322 258 256 257 22 94 190 191 90 247 196 91 254 286 324 287 256 257 24 98 96 26 102 200 201 104 251 198 100 254 302 326 303 256 257 28 108 106 
[0.017] Exiting.

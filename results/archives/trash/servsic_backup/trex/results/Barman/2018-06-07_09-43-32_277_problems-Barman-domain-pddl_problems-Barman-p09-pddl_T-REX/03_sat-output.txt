Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.004] Processed problem encoding.
[0.004] Calculated possible fact changes of composite elements.
[0.005] Initialized instantiation procedure.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     0   * * *
[0.005] *************************************
[0.005] Instantiated 1,006 initial clauses.
[0.005] The encoding contains a total of 592 distinct variables.
[0.005] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.006] Computed next depth properties: array size of 48.
[0.006] Instantiated 234 transitional clauses.
[0.008] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.008] Instantiated 1,653 universal clauses.
[0.008] Instantiated and added clauses for a total of 2,893 clauses.
[0.008] The encoding contains a total of 1,054 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 48 assumptions
[0.008] Executed solver; result: UNSAT.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     2   * * *
[0.008] *************************************
[0.009] Computed next depth properties: array size of 158.
[0.010] Instantiated 806 transitional clauses.
[0.014] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.014] Instantiated 5,822 universal clauses.
[0.014] Instantiated and added clauses for a total of 9,521 clauses.
[0.014] The encoding contains a total of 2,112 distinct variables.
[0.014] Attempting solve with solver <glucose4> ...
c 158 assumptions
c last restart ## conflicts  :  8 190 
[0.016] Executed solver; result: SAT.
[0.016] Solver returned SAT; a solution has been found at makespan 2.
157
solution 295 1
2 30 96 97 26 185 102 27 228 270 271 272 230 231 4 36 104 105 34 189 110 32 228 250 276 251 230 231 7 43 115 116 40 195 119 41 233 256 279 257 235 236 9 49 123 124 46 199 127 47 233 244 281 245 235 236 10 54 128 129 52 201 134 50 228 229 282 232 230 231 12 60 136 137 56 205 142 57 228 258 284 259 230 231 14 62 144 145 66 209 150 63 228 246 286 247 230 231 16 70 154 155 72 213 152 68 228 266 288 267 230 231 18 78 160 161 76 217 166 74 228 238 290 239 230 231 21 85 171 172 83 223 175 81 233 264 293 265 235 236 22 88 86 25 93 181 182 95 227 177 91 233 268 295 269 235 236 
[0.016] Exiting.

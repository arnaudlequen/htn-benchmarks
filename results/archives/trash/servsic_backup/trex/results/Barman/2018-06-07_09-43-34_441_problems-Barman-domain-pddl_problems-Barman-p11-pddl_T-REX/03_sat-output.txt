Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.004] Processed problem encoding.
[0.004] Calculated possible fact changes of composite elements.
[0.005] Initialized instantiation procedure.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     0   * * *
[0.005] *************************************
[0.005] Instantiated 1,130 initial clauses.
[0.005] The encoding contains a total of 664 distinct variables.
[0.005] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.006] Computed next depth properties: array size of 54.
[0.006] Instantiated 258 transitional clauses.
[0.008] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.008] Instantiated 1,875 universal clauses.
[0.008] Instantiated and added clauses for a total of 3,263 clauses.
[0.008] The encoding contains a total of 1,162 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 54 assumptions
[0.008] Executed solver; result: UNSAT.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     2   * * *
[0.008] *************************************
[0.009] Computed next depth properties: array size of 164.
[0.010] Instantiated 830 transitional clauses.
[0.014] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.014] Instantiated 6,044 universal clauses.
[0.014] Instantiated and added clauses for a total of 10,137 clauses.
[0.014] The encoding contains a total of 2,238 distinct variables.
[0.014] Attempting solve with solver <glucose4> ...
c 164 assumptions
c last restart ## conflicts  :  10 188 
[0.016] Executed solver; result: SAT.
[0.016] Solver returned SAT; a solution has been found at makespan 2.
163
solution 310 1
2 32 108 109 34 193 114 30 240 254 255 256 242 243 4 40 116 117 36 198 197 37 240 272 292 273 242 243 7 47 125 126 44 207 206 45 245 246 295 249 247 248 8 52 128 129 50 209 134 48 240 268 296 269 242 243 10 56 136 137 58 213 142 54 240 260 298 261 242 243 12 64 146 147 62 217 144 60 240 284 300 285 242 243 14 70 154 155 68 221 152 66 240 280 302 281 242 243 16 74 162 163 76 225 160 72 240 276 304 277 242 243 18 82 168 169 80 229 174 78 240 264 306 265 242 243 20 88 178 179 86 233 176 84 240 288 308 289 242 243 22 94 186 187 92 237 184 90 240 250 310 251 242 243 24 96 97 26 102 100 29 107 105 
[0.016] Exiting.

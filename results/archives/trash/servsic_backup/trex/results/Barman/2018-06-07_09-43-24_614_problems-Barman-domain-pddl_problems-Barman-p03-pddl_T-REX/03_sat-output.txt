Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.006] Processed problem encoding.
[0.006] Calculated possible fact changes of composite elements.
[0.006] Initialized instantiation procedure.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     0   * * *
[0.006] *************************************
[0.007] Instantiated 1,305 initial clauses.
[0.007] The encoding contains a total of 762 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 16 assumptions
[0.008] Executed solver; result: UNSAT.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     1   * * *
[0.008] *************************************
[0.008] Computed next depth properties: array size of 61.
[0.009] Instantiated 302 transitional clauses.
[0.011] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.011] Instantiated 2,128 universal clauses.
[0.011] Instantiated and added clauses for a total of 3,735 clauses.
[0.011] The encoding contains a total of 1,366 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 61 assumptions
[0.011] Executed solver; result: UNSAT.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     2   * * *
[0.011] *************************************
[0.013] Computed next depth properties: array size of 211.
[0.014] Instantiated 1,082 transitional clauses.
[0.020] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.020] Instantiated 7,813 universal clauses.
[0.020] Instantiated and added clauses for a total of 12,630 clauses.
[0.020] The encoding contains a total of 2,795 distinct variables.
[0.020] Attempting solve with solver <glucose4> ...
c 211 assumptions
c last restart ## conflicts  :  10 238 
[0.022] Executed solver; result: SAT.
[0.022] Solver returned SAT; a solution has been found at makespan 2.
210
solution 380 1
3 35 127 128 37 245 123 33 307 335 336 337 309 310 5 43 135 136 40 249 131 41 307 326 355 327 309 310 6 48 140 141 44 251 138 45 302 338 356 339 304 305 8 50 146 147 54 255 152 51 302 303 358 306 304 305 11 61 159 160 58 261 155 59 307 344 361 345 309 310 12 64 164 165 66 263 162 62 302 316 362 317 304 305 14 72 172 173 70 267 170 68 302 350 364 351 304 305 16 76 180 181 78 271 178 74 302 346 366 347 304 305 18 84 186 187 82 275 192 80 302 320 368 321 304 305 20 90 196 197 86 279 194 87 302 328 370 329 304 305 22 96 202 203 94 283 208 92 302 312 372 313 304 305 24 102 212 213 98 287 210 99 302 328 374 329 304 305 26 108 218 219 106 291 224 104 302 320 376 321 304 305 28 112 228 229 114 295 226 110 302 332 378 334 304 305 30 116 234 235 120 299 240 117 302 303 380 306 304 305 
[0.022] Exiting.

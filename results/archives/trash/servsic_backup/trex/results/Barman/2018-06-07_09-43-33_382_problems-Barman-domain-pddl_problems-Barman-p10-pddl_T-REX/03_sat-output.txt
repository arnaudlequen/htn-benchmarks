Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.005] Processed problem encoding.
[0.005] Calculated possible fact changes of composite elements.
[0.005] Initialized instantiation procedure.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     0   * * *
[0.005] *************************************
[0.006] Instantiated 1,177 initial clauses.
[0.006] The encoding contains a total of 690 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.006] Computed next depth properties: array size of 56.
[0.007] Instantiated 274 transitional clauses.
[0.008] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.008] Instantiated 1,937 universal clauses.
[0.008] Instantiated and added clauses for a total of 3,388 clauses.
[0.008] The encoding contains a total of 1,232 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 56 assumptions
[0.008] Executed solver; result: UNSAT.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     2   * * *
[0.008] *************************************
[0.010] Computed next depth properties: array size of 186.
[0.011] Instantiated 950 transitional clauses.
[0.016] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.016] Instantiated 6,864 universal clauses.
[0.016] Instantiated and added clauses for a total of 11,202 clauses.
[0.016] The encoding contains a total of 2,480 distinct variables.
[0.016] Attempting solve with solver <glucose4> ...
c 186 assumptions
c last restart ## conflicts  :  8 208 
[0.017] Executed solver; result: SAT.
[0.017] Solver returned SAT; a solution has been found at makespan 2.
185
solution 342 1
2 34 114 115 32 217 112 30 268 290 291 292 270 271 5 38 125 126 41 223 121 39 273 314 321 315 275 276 6 46 128 129 44 225 134 42 268 308 322 309 270 271 8 52 136 137 48 229 142 49 268 269 324 272 270 271 10 54 146 147 58 233 144 55 268 282 326 283 270 271 12 62 154 155 64 237 152 60 268 296 328 297 270 271 14 70 160 161 68 241 166 66 268 304 330 305 270 271 16 76 168 169 72 245 174 73 268 278 332 279 270 271 18 82 176 177 80 249 182 78 268 316 334 317 270 271 20 86 186 187 88 253 184 84 268 300 336 301 270 271 22 90 194 195 94 257 192 91 268 286 338 287 270 271 24 100 200 201 98 261 206 96 268 316 340 317 270 271 27 105 103 28 110 210 211 108 265 208 106 268 290 342 292 270 271 
[0.017] Exiting.

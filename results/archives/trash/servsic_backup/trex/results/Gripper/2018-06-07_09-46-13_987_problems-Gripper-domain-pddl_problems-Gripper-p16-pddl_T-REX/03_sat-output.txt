Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.002] Processed problem encoding.
[0.002] Calculated possible fact changes of composite elements.
[0.002] Initialized instantiation procedure.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     0   * * *
[0.002] *************************************
[0.003] Instantiated 891 initial clauses.
[0.003] The encoding contains a total of 528 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 18 assumptions
[0.003] Executed solver; result: UNSAT.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     1   * * *
[0.003] *************************************
[0.003] Computed next depth properties: array size of 103.
[0.004] Instantiated 461 transitional clauses.
[0.005] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.005] Instantiated 2,433 universal clauses.
[0.005] Instantiated and added clauses for a total of 3,785 clauses.
[0.005] The encoding contains a total of 1,144 distinct variables.
[0.005] Attempting solve with solver <glucose4> ...
c 103 assumptions
c last restart ## conflicts  :  0 170 
[0.005] Executed solver; result: SAT.
[0.005] Solver returned SAT; a solution has been found at makespan 1.
101
solution 139 1
138 132 5 139 133 2 130 124 5 131 125 2 122 116 5 123 117 2 114 108 5 115 109 2 106 100 5 107 101 2 98 92 5 99 93 2 90 84 5 91 85 2 82 76 5 83 77 2 74 68 5 75 69 2 66 60 5 67 61 2 58 52 5 59 53 2 50 44 5 51 45 2 42 36 5 43 37 2 34 28 5 35 29 2 26 20 5 27 21 2 18 12 5 19 13 2 9 8 5 11 10 
[0.006] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.017] Processed problem encoding.
[0.018] Calculated possible fact changes of composite elements.
[0.018] Initialized instantiation procedure.
[0.018] 
[0.018] *************************************
[0.018] * * *   M a k e s p a n     0   * * *
[0.018] *************************************
[0.020] Instantiated 3,092 initial clauses.
[0.020] The encoding contains a total of 1,615 distinct variables.
[0.020] Attempting solve with solver <glucose4> ...
c 27 assumptions
[0.020] Executed solver; result: UNSAT.
[0.020] 
[0.020] *************************************
[0.020] * * *   M a k e s p a n     1   * * *
[0.020] *************************************
[0.024] Computed next depth properties: array size of 53.
[0.028] Instantiated 2,836 transitional clauses.
[0.035] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.035] Instantiated 12,503 universal clauses.
[0.035] Instantiated and added clauses for a total of 18,431 clauses.
[0.035] The encoding contains a total of 5,753 distinct variables.
[0.035] Attempting solve with solver <glucose4> ...
c 53 assumptions
[0.035] Executed solver; result: UNSAT.
[0.035] 
[0.035] *************************************
[0.035] * * *   M a k e s p a n     2   * * *
[0.035] *************************************
[0.041] Computed next depth properties: array size of 105.
[0.049] Instantiated 8,218 transitional clauses.
[0.065] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.065] Instantiated 91,309 universal clauses.
[0.065] Instantiated and added clauses for a total of 117,958 clauses.
[0.065] The encoding contains a total of 11,451 distinct variables.
[0.065] Attempting solve with solver <glucose4> ...
c 105 assumptions
c last restart ## conflicts  :  0 111 
[0.065] Executed solver; result: SAT.
[0.065] Solver returned SAT; a solution has been found at makespan 2.
104
solution 1630 1
465 2 1311 997 503 3 1060 998 560 4 1333 999 107 5 1074 1000 126 6 969 1001 203 7 1083 1002 238 8 774 1003 620 9 52 1004 663 10 321 1005 292 11 926 1006 154 12 1404 1007 595 13 535 1008 723 14 1485 1009 740 15 1357 1010 617 16 1495 1011 343 17 1178 1012 277 18 191 1013 787 19 977 1014 428 20 1210 1015 835 21 328 1016 661 22 856 1017 903 23 1384 1018 381 24 1236 1019 455 25 1606 1020 954 26 448 1021 947 27 1630 1022 
[0.066] Exiting.

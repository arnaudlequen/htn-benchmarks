Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.011] Processed problem encoding.
[0.012] Calculated possible fact changes of composite elements.
[0.012] Initialized instantiation procedure.
[0.012] 
[0.012] *************************************
[0.012] * * *   M a k e s p a n     0   * * *
[0.012] *************************************
[0.013] Instantiated 2,077 initial clauses.
[0.013] The encoding contains a total of 1,095 distinct variables.
[0.013] Attempting solve with solver <glucose4> ...
c 22 assumptions
[0.013] Executed solver; result: UNSAT.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     1   * * *
[0.013] *************************************
[0.016] Computed next depth properties: array size of 43.
[0.017] Instantiated 1,871 transitional clauses.
[0.022] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.022] Instantiated 8,208 universal clauses.
[0.022] Instantiated and added clauses for a total of 12,156 clauses.
[0.022] The encoding contains a total of 3,808 distinct variables.
[0.022] Attempting solve with solver <glucose4> ...
c 43 assumptions
[0.022] Executed solver; result: UNSAT.
[0.022] 
[0.022] *************************************
[0.022] * * *   M a k e s p a n     2   * * *
[0.022] *************************************
[0.025] Computed next depth properties: array size of 85.
[0.030] Instantiated 5,378 transitional clauses.
[0.039] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.039] Instantiated 50,859 universal clauses.
[0.039] Instantiated and added clauses for a total of 68,393 clauses.
[0.039] The encoding contains a total of 7,571 distinct variables.
[0.039] Attempting solve with solver <glucose4> ...
c 85 assumptions
c last restart ## conflicts  :  0 91 
[0.039] Executed solver; result: SAT.
[0.039] Solver returned SAT; a solution has been found at makespan 2.
84
solution 1166 1
380 2 701 679 385 3 713 680 409 4 752 681 127 5 764 682 152 6 738 683 192 7 706 684 465 8 804 685 491 9 1080 686 227 10 861 687 260 11 608 688 268 12 660 689 86 13 170 690 563 14 1098 691 34 15 705 692 321 16 1118 693 507 17 1166 694 604 18 70 695 30 19 810 696 637 20 261 697 138 21 913 698 377 22 970 699 
[0.039] Exiting.

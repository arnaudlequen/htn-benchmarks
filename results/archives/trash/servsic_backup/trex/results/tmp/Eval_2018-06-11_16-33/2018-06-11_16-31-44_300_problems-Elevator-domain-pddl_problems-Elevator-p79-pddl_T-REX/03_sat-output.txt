Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.008] Processed problem encoding.
[0.009] Calculated possible fact changes of composite elements.
[0.009] Initialized instantiation procedure.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     0   * * *
[0.009] *************************************
[0.010] Instantiated 1,262 initial clauses.
[0.010] The encoding contains a total of 675 distinct variables.
[0.010] Attempting solve with solver <glucose4> ...
c 17 assumptions
[0.010] Executed solver; result: UNSAT.
[0.010] 
[0.010] *************************************
[0.010] * * *   M a k e s p a n     1   * * *
[0.010] *************************************
[0.011] Computed next depth properties: array size of 33.
[0.013] Instantiated 1,106 transitional clauses.
[0.016] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.016] Instantiated 4,813 universal clauses.
[0.016] Instantiated and added clauses for a total of 7,181 clauses.
[0.016] The encoding contains a total of 2,263 distinct variables.
[0.016] Attempting solve with solver <glucose4> ...
c 33 assumptions
[0.016] Executed solver; result: UNSAT.
[0.016] 
[0.016] *************************************
[0.016] * * *   M a k e s p a n     2   * * *
[0.016] *************************************
[0.018] Computed next depth properties: array size of 65.
[0.021] Instantiated 3,138 transitional clauses.
[0.028] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.028] Instantiated 24,509 universal clauses.
[0.028] Instantiated and added clauses for a total of 34,828 clauses.
[0.028] The encoding contains a total of 4,491 distinct variables.
[0.028] Attempting solve with solver <glucose4> ...
c 65 assumptions
c last restart ## conflicts  :  0 71 
[0.028] Executed solver; result: SAT.
[0.028] Solver returned SAT; a solution has been found at makespan 2.
63
solution 646 1
201 2 407 390 48 3 230 391 4 559 392 247 5 115 393 216 6 582 394 264 7 166 395 100 8 202 396 129 9 451 397 316 10 91 398 166 11 600 399 327 12 79 400 115 13 216 401 342 14 627 402 369 15 646 403 187 16 525 404 88 17 56 405 
[0.028] Exiting.

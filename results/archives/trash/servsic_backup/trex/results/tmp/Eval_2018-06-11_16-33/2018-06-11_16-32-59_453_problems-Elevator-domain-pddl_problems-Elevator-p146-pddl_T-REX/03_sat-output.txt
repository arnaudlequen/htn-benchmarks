Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.029] Processed problem encoding.
[0.033] Calculated possible fact changes of composite elements.
[0.035] Initialized instantiation procedure.
[0.035] 
[0.035] *************************************
[0.035] * * *   M a k e s p a n     0   * * *
[0.035] *************************************
[0.040] Instantiated 4,048 initial clauses.
[0.040] The encoding contains a total of 2,103 distinct variables.
[0.040] Attempting solve with solver <glucose4> ...
c 31 assumptions
[0.040] Executed solver; result: UNSAT.
[0.040] 
[0.040] *************************************
[0.040] * * *   M a k e s p a n     1   * * *
[0.040] *************************************
[0.051] Computed next depth properties: array size of 61.
[0.057] Instantiated 3,752 transitional clauses.
[0.077] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.077] Instantiated 16,587 universal clauses.
[0.077] Instantiated and added clauses for a total of 24,387 clauses.
[0.077] The encoding contains a total of 7,597 distinct variables.
[0.077] Attempting solve with solver <glucose4> ...
c 61 assumptions
[0.078] Executed solver; result: UNSAT.
[0.078] 
[0.078] *************************************
[0.078] * * *   M a k e s p a n     2   * * *
[0.078] *************************************
[0.092] Computed next depth properties: array size of 121.
[0.103] Instantiated 10,922 transitional clauses.
[0.125] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.125] Instantiated 135,837 universal clauses.
[0.125] Instantiated and added clauses for a total of 171,146 clauses.
[0.125] The encoding contains a total of 15,131 distinct variables.
[0.125] Attempting solve with solver <glucose4> ...
c 121 assumptions
c last restart ## conflicts  :  0 127 
[0.125] Executed solver; result: SAT.
[0.125] Solver returned SAT; a solution has been found at makespan 2.
119
solution 2157 1
684 2 880 1389 773 3 1740 1390 51 4 1472 1391 64 5 1776 1392 71 6 1797 1393 893 7 1059 1394 901 8 1878 1395 963 9 860 1396 266 10 1506 1397 973 11 676 1398 291 12 1943 1399 1038 13 86 1400 125 14 896 1401 166 15 686 1402 402 16 1426 1403 444 17 1122 1404 18 2015 1405 1082 19 336 1406 479 20 1386 1407 1164 21 2041 1408 81 22 1569 1409 551 23 1614 1410 592 24 2102 1411 1281 25 1238 1412 622 26 2122 1413 814 27 62 1414 526 28 2157 1415 1337 29 2092 1416 149 30 896 1417 674 31 1804 1418 
[0.126] Exiting.

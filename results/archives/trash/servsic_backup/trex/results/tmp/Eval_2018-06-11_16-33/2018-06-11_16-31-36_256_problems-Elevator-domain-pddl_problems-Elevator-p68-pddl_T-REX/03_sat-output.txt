Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.007] Processed problem encoding.
[0.007] Calculated possible fact changes of composite elements.
[0.008] Initialized instantiation procedure.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     0   * * *
[0.008] *************************************
[0.008] Instantiated 992 initial clauses.
[0.008] The encoding contains a total of 535 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.008] Executed solver; result: UNSAT.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     1   * * *
[0.008] *************************************
[0.009] Computed next depth properties: array size of 29.
[0.011] Instantiated 856 transitional clauses.
[0.013] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.013] Instantiated 3,707 universal clauses.
[0.013] Instantiated and added clauses for a total of 5,555 clauses.
[0.013] The encoding contains a total of 1,757 distinct variables.
[0.013] Attempting solve with solver <glucose4> ...
c 29 assumptions
[0.013] Executed solver; result: UNSAT.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     2   * * *
[0.013] *************************************
[0.015] Computed next depth properties: array size of 57.
[0.018] Instantiated 2,410 transitional clauses.
[0.023] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.023] Instantiated 17,245 universal clauses.
[0.023] Instantiated and added clauses for a total of 25,210 clauses.
[0.023] The encoding contains a total of 3,483 distinct variables.
[0.023] Attempting solve with solver <glucose4> ...
c 57 assumptions
c last restart ## conflicts  :  0 63 
[0.023] Executed solver; result: SAT.
[0.023] Solver returned SAT; a solution has been found at makespan 2.
55
solution 606 1
198 2 504 367 27 3 234 368 223 4 71 369 5 391 370 88 6 525 371 65 7 200 372 274 8 558 373 284 9 430 374 135 10 443 375 143 11 333 376 160 12 579 377 321 13 91 378 353 14 606 379 184 15 482 380 
[0.023] Exiting.

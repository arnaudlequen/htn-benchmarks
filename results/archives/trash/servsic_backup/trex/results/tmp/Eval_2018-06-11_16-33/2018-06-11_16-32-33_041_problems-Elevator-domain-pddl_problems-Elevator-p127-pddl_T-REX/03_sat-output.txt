Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.020] Processed problem encoding.
[0.021] Calculated possible fact changes of composite elements.
[0.022] Initialized instantiation procedure.
[0.022] 
[0.022] *************************************
[0.022] * * *   M a k e s p a n     0   * * *
[0.022] *************************************
[0.024] Instantiated 3,092 initial clauses.
[0.024] The encoding contains a total of 1,615 distinct variables.
[0.024] Attempting solve with solver <glucose4> ...
c 27 assumptions
[0.024] Executed solver; result: UNSAT.
[0.024] 
[0.024] *************************************
[0.024] * * *   M a k e s p a n     1   * * *
[0.024] *************************************
[0.029] Computed next depth properties: array size of 53.
[0.032] Instantiated 2,836 transitional clauses.
[0.041] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.041] Instantiated 12,503 universal clauses.
[0.041] Instantiated and added clauses for a total of 18,431 clauses.
[0.041] The encoding contains a total of 5,753 distinct variables.
[0.041] Attempting solve with solver <glucose4> ...
c 53 assumptions
[0.041] Executed solver; result: UNSAT.
[0.041] 
[0.041] *************************************
[0.041] * * *   M a k e s p a n     2   * * *
[0.041] *************************************
[0.049] Computed next depth properties: array size of 105.
[0.057] Instantiated 8,218 transitional clauses.
[0.076] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.076] Instantiated 91,309 universal clauses.
[0.076] Instantiated and added clauses for a total of 117,958 clauses.
[0.076] The encoding contains a total of 11,451 distinct variables.
[0.076] Attempting solve with solver <glucose4> ...
c 105 assumptions
c last restart ## conflicts  :  0 111 
[0.076] Executed solver; result: SAT.
[0.076] Solver returned SAT; a solution has been found at makespan 2.
103
solution 1611 1
590 2 440 1099 611 3 1060 1100 106 4 1141 1101 684 5 266 1102 144 6 1365 1103 702 7 842 1104 735 8 890 1105 763 9 1424 1106 79 10 786 1107 11 1382 1108 795 12 1450 1109 302 13 1010 1110 272 14 1476 1111 863 15 1221 1112 386 16 300 1113 394 17 1043 1114 468 18 591 1115 474 19 847 1116 952 20 88 1117 138 21 666 1118 531 22 1200 1119 765 23 1509 1120 1018 24 1561 1121 1029 25 1611 1122 1083 26 891 1123 300 27 1329 1124 
[0.077] Exiting.

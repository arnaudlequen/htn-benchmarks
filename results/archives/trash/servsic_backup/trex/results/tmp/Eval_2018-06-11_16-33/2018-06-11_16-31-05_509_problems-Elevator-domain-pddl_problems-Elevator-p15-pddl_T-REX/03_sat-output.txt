Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 79 initial clauses.
[0.001] The encoding contains a total of 51 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 7.
[0.001] Instantiated 53 transitional clauses.
[0.001] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.001] Instantiated 198 universal clauses.
[0.001] Instantiated and added clauses for a total of 330 clauses.
[0.001] The encoding contains a total of 118 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     2   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 13.
[0.001] Instantiated 122 transitional clauses.
[0.001] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.001] Instantiated 459 universal clauses.
[0.001] Instantiated and added clauses for a total of 911 clauses.
[0.001] The encoding contains a total of 227 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 13 assumptions
c last restart ## conflicts  :  0 19 
[0.001] Executed solver; result: SAT.
[0.001] Solver returned SAT; a solution has been found at makespan 2.
11
solution 23 1
15 2 12 20 18 3 11 21 4 23 22 
[0.002] Exiting.

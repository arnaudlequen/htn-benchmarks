Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 299 initial clauses.
[0.001] The encoding contains a total of 171 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 8 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 15.
[0.001] Instantiated 233 transitional clauses.
[0.001] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.001] Instantiated 970 universal clauses.
[0.001] Instantiated and added clauses for a total of 1,502 clauses.
[0.001] The encoding contains a total of 490 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     2   * * *
[0.001] *************************************
[0.002] Computed next depth properties: array size of 29.
[0.002] Instantiated 618 transitional clauses.
[0.002] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.002] Instantiated 3,035 universal clauses.
[0.002] Instantiated and added clauses for a total of 5,155 clauses.
[0.002] The encoding contains a total of 963 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 29 assumptions
c last restart ## conflicts  :  0 35 
[0.002] Executed solver; result: SAT.
[0.002] Solver returned SAT; a solution has been found at makespan 2.
28
solution 116 1
46 2 82 74 51 3 116 75 12 4 91 76 59 5 35 77 9 6 91 78 28 7 87 79 73 8 99 80 
[0.002] Exiting.

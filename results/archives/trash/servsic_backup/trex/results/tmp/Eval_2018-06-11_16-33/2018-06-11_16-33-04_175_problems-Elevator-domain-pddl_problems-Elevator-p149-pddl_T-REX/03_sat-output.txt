Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.021] Processed problem encoding.
[0.022] Calculated possible fact changes of composite elements.
[0.023] Initialized instantiation procedure.
[0.023] 
[0.023] *************************************
[0.023] * * *   M a k e s p a n     0   * * *
[0.023] *************************************
[0.026] Instantiated 4,048 initial clauses.
[0.026] The encoding contains a total of 2,103 distinct variables.
[0.026] Attempting solve with solver <glucose4> ...
c 31 assumptions
[0.026] Executed solver; result: UNSAT.
[0.026] 
[0.026] *************************************
[0.026] * * *   M a k e s p a n     1   * * *
[0.026] *************************************
[0.031] Computed next depth properties: array size of 61.
[0.035] Instantiated 3,752 transitional clauses.
[0.045] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.045] Instantiated 16,587 universal clauses.
[0.045] Instantiated and added clauses for a total of 24,387 clauses.
[0.045] The encoding contains a total of 7,597 distinct variables.
[0.045] Attempting solve with solver <glucose4> ...
c 61 assumptions
[0.045] Executed solver; result: UNSAT.
[0.045] 
[0.045] *************************************
[0.045] * * *   M a k e s p a n     2   * * *
[0.045] *************************************
[0.054] Computed next depth properties: array size of 121.
[0.064] Instantiated 10,922 transitional clauses.
[0.085] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.085] Instantiated 135,837 universal clauses.
[0.085] Instantiated and added clauses for a total of 171,146 clauses.
[0.085] The encoding contains a total of 15,131 distinct variables.
[0.085] Attempting solve with solver <glucose4> ...
c 121 assumptions
c last restart ## conflicts  :  0 127 
[0.085] Executed solver; result: SAT.
[0.085] Solver returned SAT; a solution has been found at makespan 2.
120
solution 2440 1
760 2 491 1566 66 3 1602 1567 838 4 1654 1568 153 5 1620 1569 911 6 2125 1570 173 7 1726 1571 250 8 817 1572 955 9 569 1573 978 10 1757 1574 1022 11 2097 1575 372 12 788 1576 424 13 1252 1577 383 14 261 1578 1091 15 2170 1579 456 16 777 1580 491 17 631 1581 1039 18 1862 1582 266 19 2219 1583 1179 20 1888 1584 1227 21 2315 1585 1253 22 430 1586 559 23 1323 1587 651 24 959 1588 1338 25 2378 1589 1382 26 2394 1590 1285 27 2429 1591 1454 28 409 1592 1478 29 1265 1593 741 30 2440 1594 1548 31 2053 1595 
[0.086] Exiting.

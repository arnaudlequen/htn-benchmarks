Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.003] Processed problem encoding.
[0.003] Calculated possible fact changes of composite elements.
[0.003] Initialized instantiation procedure.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     0   * * *
[0.003] *************************************
[0.003] Instantiated 992 initial clauses.
[0.003] The encoding contains a total of 535 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.003] Executed solver; result: UNSAT.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     1   * * *
[0.003] *************************************
[0.004] Computed next depth properties: array size of 29.
[0.004] Instantiated 856 transitional clauses.
[0.005] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.005] Instantiated 3,707 universal clauses.
[0.005] Instantiated and added clauses for a total of 5,555 clauses.
[0.005] The encoding contains a total of 1,757 distinct variables.
[0.005] Attempting solve with solver <glucose4> ...
c 29 assumptions
[0.005] Executed solver; result: UNSAT.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     2   * * *
[0.005] *************************************
[0.006] Computed next depth properties: array size of 57.
[0.007] Instantiated 2,410 transitional clauses.
[0.009] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.009] Instantiated 17,245 universal clauses.
[0.009] Instantiated and added clauses for a total of 25,210 clauses.
[0.009] The encoding contains a total of 3,483 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 57 assumptions
c last restart ## conflicts  :  0 63 
[0.009] Executed solver; result: SAT.
[0.009] Solver returned SAT; a solution has been found at makespan 2.
56
solution 512 1
177 2 342 313 36 3 354 314 63 4 365 315 205 5 281 316 60 6 368 317 103 7 347 318 126 8 390 319 231 9 510 320 261 10 512 321 143 11 460 322 151 12 345 323 37 13 95 324 289 14 246 325 27 15 360 326 
[0.009] Exiting.

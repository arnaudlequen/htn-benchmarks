Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.027] Processed problem encoding.
[0.029] Calculated possible fact changes of composite elements.
[0.030] Initialized instantiation procedure.
[0.030] 
[0.030] *************************************
[0.030] * * *   M a k e s p a n     0   * * *
[0.030] *************************************
[0.033] Instantiated 4,048 initial clauses.
[0.033] The encoding contains a total of 2,103 distinct variables.
[0.033] Attempting solve with solver <glucose4> ...
c 31 assumptions
[0.033] Executed solver; result: UNSAT.
[0.033] 
[0.033] *************************************
[0.033] * * *   M a k e s p a n     1   * * *
[0.033] *************************************
[0.039] Computed next depth properties: array size of 61.
[0.043] Instantiated 3,752 transitional clauses.
[0.055] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.055] Instantiated 16,587 universal clauses.
[0.055] Instantiated and added clauses for a total of 24,387 clauses.
[0.055] The encoding contains a total of 7,597 distinct variables.
[0.055] Attempting solve with solver <glucose4> ...
c 61 assumptions
[0.055] Executed solver; result: UNSAT.
[0.055] 
[0.055] *************************************
[0.055] * * *   M a k e s p a n     2   * * *
[0.055] *************************************
[0.065] Computed next depth properties: array size of 121.
[0.076] Instantiated 10,922 transitional clauses.
[0.100] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.100] Instantiated 135,837 universal clauses.
[0.100] Instantiated and added clauses for a total of 171,146 clauses.
[0.100] The encoding contains a total of 15,131 distinct variables.
[0.100] Attempting solve with solver <glucose4> ...
c 121 assumptions
c last restart ## conflicts  :  0 127 
[0.100] Executed solver; result: SAT.
[0.100] Solver returned SAT; a solution has been found at makespan 2.
120
solution 2341 1
692 2 1493 1448 84 3 2031 1449 94 4 707 1450 791 5 412 1451 154 6 2084 1452 861 7 1584 1453 182 8 2038 1454 947 9 2128 1455 1002 10 530 1456 244 11 1621 1457 939 12 2138 1458 242 13 1664 1459 735 14 651 1460 1057 15 649 1461 310 16 1249 1462 350 17 639 1463 1146 18 2212 1464 1131 19 421 1465 1169 20 544 1466 112 21 1691 1467 424 22 873 1468 481 23 2273 1469 1240 24 1771 1470 550 25 2341 1471 1219 26 1803 1472 1313 27 1860 1473 589 28 1904 1474 636 29 2055 1475 1394 30 1955 1476 688 31 1266 1477 
[0.101] Exiting.

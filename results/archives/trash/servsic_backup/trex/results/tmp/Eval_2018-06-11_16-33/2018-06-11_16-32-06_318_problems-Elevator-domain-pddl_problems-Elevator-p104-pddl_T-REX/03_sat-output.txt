Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.013] Processed problem encoding.
[0.014] Calculated possible fact changes of composite elements.
[0.014] Initialized instantiation procedure.
[0.014] 
[0.014] *************************************
[0.014] * * *   M a k e s p a n     0   * * *
[0.014] *************************************
[0.016] Instantiated 2,077 initial clauses.
[0.016] The encoding contains a total of 1,095 distinct variables.
[0.016] Attempting solve with solver <glucose4> ...
c 22 assumptions
[0.016] Executed solver; result: UNSAT.
[0.016] 
[0.016] *************************************
[0.016] * * *   M a k e s p a n     1   * * *
[0.016] *************************************
[0.019] Computed next depth properties: array size of 43.
[0.021] Instantiated 1,871 transitional clauses.
[0.026] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.026] Instantiated 8,208 universal clauses.
[0.026] Instantiated and added clauses for a total of 12,156 clauses.
[0.026] The encoding contains a total of 3,808 distinct variables.
[0.026] Attempting solve with solver <glucose4> ...
c 43 assumptions
[0.027] Executed solver; result: UNSAT.
[0.027] 
[0.027] *************************************
[0.027] * * *   M a k e s p a n     2   * * *
[0.027] *************************************
[0.031] Computed next depth properties: array size of 85.
[0.037] Instantiated 5,378 transitional clauses.
[0.049] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.049] Instantiated 50,859 universal clauses.
[0.049] Instantiated and added clauses for a total of 68,393 clauses.
[0.049] The encoding contains a total of 7,571 distinct variables.
[0.049] Attempting solve with solver <glucose4> ...
c 85 assumptions
c last restart ## conflicts  :  0 91 
[0.049] Executed solver; result: SAT.
[0.049] Solver returned SAT; a solution has been found at makespan 2.
84
solution 1065 1
268 2 851 638 33 3 664 639 73 4 888 640 274 5 925 641 104 6 932 642 122 7 953 643 370 8 488 644 164 9 996 645 409 10 994 646 429 11 922 647 158 12 290 648 495 13 576 649 205 14 1045 650 211 15 32 651 540 16 28 652 254 17 627 653 530 18 1065 654 222 19 681 655 318 20 810 656 265 21 878 657 594 22 957 658 
[0.049] Exiting.

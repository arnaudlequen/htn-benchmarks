Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.004] Processed problem encoding.
[0.004] Calculated possible fact changes of composite elements.
[0.004] Initialized instantiation procedure.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     0   * * *
[0.004] *************************************
[0.004] Instantiated 548 initial clauses.
[0.004] The encoding contains a total of 303 distinct variables.
[0.004] Attempting solve with solver <glucose4> ...
c 11 assumptions
[0.004] Executed solver; result: UNSAT.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     1   * * *
[0.004] *************************************
[0.005] Computed next depth properties: array size of 21.
[0.005] Instantiated 452 transitional clauses.
[0.006] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.007] Instantiated 1,927 universal clauses.
[0.007] Instantiated and added clauses for a total of 2,927 clauses.
[0.007] The encoding contains a total of 937 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 21 assumptions
[0.007] Executed solver; result: UNSAT.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     2   * * *
[0.007] *************************************
[0.007] Computed next depth properties: array size of 41.
[0.009] Instantiated 1,242 transitional clauses.
[0.011] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.011] Instantiated 7,277 universal clauses.
[0.011] Instantiated and added clauses for a total of 11,446 clauses.
[0.011] The encoding contains a total of 1,851 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 41 assumptions
c last restart ## conflicts  :  0 47 
[0.011] Executed solver; result: SAT.
[0.011] Solver returned SAT; a solution has been found at makespan 2.
40
solution 244 1
103 2 86 183 106 3 59 184 112 4 167 185 52 5 164 186 147 6 57 187 18 7 196 188 66 8 206 189 160 9 244 190 173 10 38 191 87 11 210 192 
[0.011] Exiting.

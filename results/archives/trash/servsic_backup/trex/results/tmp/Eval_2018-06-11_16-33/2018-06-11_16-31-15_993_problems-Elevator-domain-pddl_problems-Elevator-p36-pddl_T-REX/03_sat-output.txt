Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.003] Processed problem encoding.
[0.003] Calculated possible fact changes of composite elements.
[0.003] Initialized instantiation procedure.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     0   * * *
[0.003] *************************************
[0.003] Instantiated 374 initial clauses.
[0.003] The encoding contains a total of 211 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 9 assumptions
[0.003] Executed solver; result: UNSAT.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     1   * * *
[0.003] *************************************
[0.003] Computed next depth properties: array size of 17.
[0.004] Instantiated 298 transitional clauses.
[0.004] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.004] Instantiated 1,253 universal clauses.
[0.004] Instantiated and added clauses for a total of 1,925 clauses.
[0.004] The encoding contains a total of 623 distinct variables.
[0.004] Attempting solve with solver <glucose4> ...
c 17 assumptions
[0.004] Executed solver; result: UNSAT.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     2   * * *
[0.004] *************************************
[0.005] Computed next depth properties: array size of 33.
[0.006] Instantiated 802 transitional clauses.
[0.007] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.007] Instantiated 4,189 universal clauses.
[0.007] Instantiated and added clauses for a total of 6,916 clauses.
[0.007] The encoding contains a total of 1,227 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 33 assumptions
c last restart ## conflicts  :  0 39 
[0.007] Executed solver; result: SAT.
[0.007] Solver returned SAT; a solution has been found at makespan 2.
32
solution 165 1
68 2 165 100 81 3 61 101 25 4 115 102 42 5 139 103 20 6 150 104 40 7 164 105 51 8 156 106 63 9 111 107 
[0.007] Exiting.

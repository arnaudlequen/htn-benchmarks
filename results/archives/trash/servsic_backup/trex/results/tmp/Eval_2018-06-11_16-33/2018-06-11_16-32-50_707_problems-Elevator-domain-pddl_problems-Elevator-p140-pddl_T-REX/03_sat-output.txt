Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.024] Processed problem encoding.
[0.026] Calculated possible fact changes of composite elements.
[0.027] Initialized instantiation procedure.
[0.027] 
[0.027] *************************************
[0.027] * * *   M a k e s p a n     0   * * *
[0.027] *************************************
[0.029] Instantiated 3,554 initial clauses.
[0.029] The encoding contains a total of 1,851 distinct variables.
[0.029] Attempting solve with solver <glucose4> ...
c 29 assumptions
[0.029] Executed solver; result: UNSAT.
[0.029] 
[0.029] *************************************
[0.029] * * *   M a k e s p a n     1   * * *
[0.029] *************************************
[0.035] Computed next depth properties: array size of 57.
[0.039] Instantiated 3,278 transitional clauses.
[0.050] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.050] Instantiated 14,473 universal clauses.
[0.050] Instantiated and added clauses for a total of 21,305 clauses.
[0.050] The encoding contains a total of 6,643 distinct variables.
[0.050] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.050] Executed solver; result: UNSAT.
[0.050] 
[0.050] *************************************
[0.050] * * *   M a k e s p a n     2   * * *
[0.050] *************************************
[0.059] Computed next depth properties: array size of 113.
[0.069] Instantiated 9,522 transitional clauses.
[0.090] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.090] Instantiated 112,109 universal clauses.
[0.090] Instantiated and added clauses for a total of 142,936 clauses.
[0.090] The encoding contains a total of 13,227 distinct variables.
[0.090] Attempting solve with solver <glucose4> ...
c 113 assumptions
c last restart ## conflicts  :  0 119 
[0.090] Executed solver; result: SAT.
[0.090] Solver returned SAT; a solution has been found at makespan 2.
111
solution 1974 1
556 2 1061 1240 66 3 1702 1241 639 4 1745 1242 101 5 1298 1243 148 6 303 1244 709 7 1802 1245 746 8 1823 1246 799 9 130 1247 826 10 1386 1248 242 11 382 1249 251 12 1410 1250 316 13 1366 1251 331 14 200 1252 1009 15 1062 1253 16 507 1254 1103 17 1856 1255 888 18 1882 1256 1131 19 1531 1257 394 20 707 1258 428 21 1543 1259 234 22 1612 1260 956 23 1960 1261 1099 24 214 1262 468 25 1974 1263 1210 26 854 1264 48 27 1686 1265 527 28 784 1266 439 29 1123 1267 
[0.091] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.024] Processed problem encoding.
[0.026] Calculated possible fact changes of composite elements.
[0.027] Initialized instantiation procedure.
[0.027] 
[0.027] *************************************
[0.027] * * *   M a k e s p a n     0   * * *
[0.027] *************************************
[0.029] Instantiated 3,319 initial clauses.
[0.029] The encoding contains a total of 1,731 distinct variables.
[0.029] Attempting solve with solver <glucose4> ...
c 28 assumptions
[0.029] Executed solver; result: UNSAT.
[0.029] 
[0.029] *************************************
[0.029] * * *   M a k e s p a n     1   * * *
[0.029] *************************************
[0.035] Computed next depth properties: array size of 55.
[0.039] Instantiated 3,053 transitional clauses.
[0.049] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.049] Instantiated 13,470 universal clauses.
[0.049] Instantiated and added clauses for a total of 19,842 clauses.
[0.049] The encoding contains a total of 6,190 distinct variables.
[0.049] Attempting solve with solver <glucose4> ...
c 55 assumptions
[0.049] Executed solver; result: UNSAT.
[0.049] 
[0.049] *************************************
[0.049] * * *   M a k e s p a n     2   * * *
[0.049] *************************************
[0.058] Computed next depth properties: array size of 109.
[0.068] Instantiated 8,858 transitional clauses.
[0.089] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.089] Instantiated 101,355 universal clauses.
[0.089] Instantiated and added clauses for a total of 130,055 clauses.
[0.089] The encoding contains a total of 12,323 distinct variables.
[0.089] Attempting solve with solver <glucose4> ...
c 109 assumptions
c last restart ## conflicts  :  0 115 
[0.089] Executed solver; result: SAT.
[0.089] Solver returned SAT; a solution has been found at makespan 2.
106
solution 2018 1
645 2 1260 1195 49 3 680 1196 756 4 1708 1197 98 5 915 1198 129 6 622 1199 143 7 1318 1200 705 8 1758 1201 837 9 1375 1202 221 10 1121 1203 890 11 1801 1204 211 12 1425 1205 943 13 1490 1206 290 14 960 1207 15 1498 1208 976 16 1913 1209 665 17 1928 1210 346 18 664 1211 393 19 427 1212 20 1545 1213 1092 21 1988 1214 893 22 1771 1215 1128 23 1624 1216 505 24 2018 1217 659 25 1680 1218 562 26 1390 1219 596 27 979 1220 644 28 1712 1221 
[0.090] Exiting.

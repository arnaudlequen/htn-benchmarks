Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.002] Processed problem encoding.
[0.002] Calculated possible fact changes of composite elements.
[0.002] Initialized instantiation procedure.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     0   * * *
[0.002] *************************************
[0.002] Instantiated 299 initial clauses.
[0.002] The encoding contains a total of 171 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 8 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     1   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 15.
[0.002] Instantiated 233 transitional clauses.
[0.003] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.003] Instantiated 970 universal clauses.
[0.003] Instantiated and added clauses for a total of 1,502 clauses.
[0.003] The encoding contains a total of 490 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.003] Executed solver; result: UNSAT.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     2   * * *
[0.003] *************************************
[0.003] Computed next depth properties: array size of 29.
[0.003] Instantiated 618 transitional clauses.
[0.004] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.004] Instantiated 3,035 universal clauses.
[0.004] Instantiated and added clauses for a total of 5,155 clauses.
[0.004] The encoding contains a total of 963 distinct variables.
[0.004] Attempting solve with solver <glucose4> ...
c 29 assumptions
c last restart ## conflicts  :  0 35 
[0.004] Executed solver; result: SAT.
[0.004] Solver returned SAT; a solution has been found at makespan 2.
28
solution 155 1
52 2 99 87 17 3 140 88 27 4 117 89 75 5 154 90 77 6 127 91 83 7 136 92 49 8 155 93 
[0.004] Exiting.

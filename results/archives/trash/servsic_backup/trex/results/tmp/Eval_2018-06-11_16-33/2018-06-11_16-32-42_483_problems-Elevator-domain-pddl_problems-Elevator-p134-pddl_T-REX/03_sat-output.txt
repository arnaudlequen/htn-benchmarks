Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.023] Processed problem encoding.
[0.024] Calculated possible fact changes of composite elements.
[0.025] Initialized instantiation procedure.
[0.025] 
[0.025] *************************************
[0.025] * * *   M a k e s p a n     0   * * *
[0.025] *************************************
[0.028] Instantiated 3,319 initial clauses.
[0.028] The encoding contains a total of 1,731 distinct variables.
[0.028] Attempting solve with solver <glucose4> ...
c 28 assumptions
[0.028] Executed solver; result: UNSAT.
[0.028] 
[0.028] *************************************
[0.028] * * *   M a k e s p a n     1   * * *
[0.028] *************************************
[0.033] Computed next depth properties: array size of 55.
[0.037] Instantiated 3,053 transitional clauses.
[0.047] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.047] Instantiated 13,470 universal clauses.
[0.047] Instantiated and added clauses for a total of 19,842 clauses.
[0.047] The encoding contains a total of 6,190 distinct variables.
[0.047] Attempting solve with solver <glucose4> ...
c 55 assumptions
[0.047] Executed solver; result: UNSAT.
[0.047] 
[0.047] *************************************
[0.047] * * *   M a k e s p a n     2   * * *
[0.047] *************************************
[0.055] Computed next depth properties: array size of 109.
[0.066] Instantiated 8,858 transitional clauses.
[0.087] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.087] Instantiated 101,355 universal clauses.
[0.087] Instantiated and added clauses for a total of 130,055 clauses.
[0.087] The encoding contains a total of 12,323 distinct variables.
[0.087] Attempting solve with solver <glucose4> ...
c 109 assumptions
c last restart ## conflicts  :  0 115 
[0.087] Executed solver; result: SAT.
[0.087] Solver returned SAT; a solution has been found at makespan 2.
108
solution 1866 1
533 2 913 1142 81 3 402 1143 593 4 1190 1144 625 5 1582 1145 648 6 1613 1146 725 7 1584 1147 742 8 1619 1148 188 9 1265 1149 240 10 1661 1150 783 11 1343 1151 157 12 918 1152 300 13 613 1153 150 14 982 1154 312 15 1715 1155 926 16 523 1156 936 17 1406 1157 970 18 132 1158 999 19 1769 1159 102 20 1468 1160 197 21 1490 1161 321 22 1316 1162 379 23 409 1163 1038 24 878 1164 430 25 1820 1165 439 26 1866 1166 1090 27 787 1167 1136 28 84 1168 
[0.088] Exiting.

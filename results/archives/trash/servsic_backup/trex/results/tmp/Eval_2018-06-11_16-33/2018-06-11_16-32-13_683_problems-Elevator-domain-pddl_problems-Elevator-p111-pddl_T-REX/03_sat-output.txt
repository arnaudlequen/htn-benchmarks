Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.015] Processed problem encoding.
[0.016] Calculated possible fact changes of composite elements.
[0.017] Initialized instantiation procedure.
[0.017] 
[0.017] *************************************
[0.017] * * *   M a k e s p a n     0   * * *
[0.017] *************************************
[0.019] Instantiated 2,459 initial clauses.
[0.019] The encoding contains a total of 1,291 distinct variables.
[0.019] Attempting solve with solver <glucose4> ...
c 24 assumptions
[0.019] Executed solver; result: UNSAT.
[0.019] 
[0.019] *************************************
[0.019] * * *   M a k e s p a n     1   * * *
[0.019] *************************************
[0.022] Computed next depth properties: array size of 47.
[0.025] Instantiated 2,233 transitional clauses.
[0.031] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.031] Instantiated 9,818 universal clauses.
[0.031] Instantiated and added clauses for a total of 14,510 clauses.
[0.031] The encoding contains a total of 4,538 distinct variables.
[0.031] Attempting solve with solver <glucose4> ...
c 47 assumptions
[0.031] Executed solver; result: UNSAT.
[0.031] 
[0.031] *************************************
[0.031] * * *   M a k e s p a n     2   * * *
[0.031] *************************************
[0.037] Computed next depth properties: array size of 93.
[0.043] Instantiated 6,442 transitional clauses.
[0.057] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.057] Instantiated 65,179 universal clauses.
[0.057] Instantiated and added clauses for a total of 86,131 clauses.
[0.057] The encoding contains a total of 9,027 distinct variables.
[0.057] Attempting solve with solver <glucose4> ...
c 93 assumptions
c last restart ## conflicts  :  0 99 
[0.057] Executed solver; result: SAT.
[0.057] Solver returned SAT; a solution has been found at makespan 2.
92
solution 1395 1
379 2 1080 835 390 3 239 836 415 4 342 837 83 5 612 838 37 6 1124 839 434 7 230 840 143 8 731 841 157 9 442 842 187 10 1186 843 194 11 877 844 546 12 1231 845 244 13 1259 846 601 14 1287 847 638 15 361 848 561 16 370 849 679 17 1309 850 307 18 1315 851 313 19 1344 852 657 20 1385 853 345 21 1171 854 775 22 318 855 809 23 1395 856 296 24 1141 857 
[0.058] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 54 initial clauses.
[0.001] The encoding contains a total of 37 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 5.
[0.001] Instantiated 36 transitional clauses.
[0.001] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.001] Instantiated 131 universal clauses.
[0.001] Instantiated and added clauses for a total of 221 clauses.
[0.001] The encoding contains a total of 83 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 5 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     2   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 9.
[0.001] Instantiated 82 transitional clauses.
[0.001] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.001] Instantiated 305 universal clauses.
[0.001] Instantiated and added clauses for a total of 608 clauses.
[0.001] The encoding contains a total of 157 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 9 assumptions
c last restart ## conflicts  :  0 15 
[0.001] Executed solver; result: SAT.
[0.001] Solver returned SAT; a solution has been found at makespan 2.
8
solution 17 1
10 2 17 14 8 3 5 15 
[0.002] Exiting.

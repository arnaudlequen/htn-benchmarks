import os
import sys
import argparse
import random


def column(matrix, i):
    return [row[i] for row in matrix]

def fill(lst, n):
    while len(lst) < n:
        lst.append(float('inf'))
    return lst

def remove_zero(tab):
    for i in range(len(tab)):
        for j in range(len(tab[0])):
            if tab[i][j] == '1.00':
                tab[i][j] = 1
            if tab[i][j] == '0.00':
                tab[i][j] = 0

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def make_pb_list(n):
    text  = '\\begin{tabular}{c|}\n'
    text += 'Pb \\\\ \\hline\n'
    text += '\\\\\n'.join(['' + str(i) for i in range(1, n+1)])
    text += "\\\\\n\\hline\\end{tabular}"
    return text


parser = argparse.ArgumentParser(description='Create LaTeX tables with the provided data.')
parser.add_argument("--time", metavar="time", type=str2bool, nargs='?',
                        const=True, default='false',
                        help="print time tables")
parser.add_argument('--dpp', metavar="dpp", help="domains per page", type=int, default=2)

args = parser.parse_args()

table = []

# Prob_table + name of the problem
def format_prob_table(prob_table, name):
    text  = "\\begin{tabular}{|" + ' '.join('l'*len(prob_table)) + '|}\n\\hline\n'
    text += '\\multicolumn{' + str(len(prob_table)) + '}{|c|}{' + name + '}\\\\\n'
    text += ' & '.join(list(map(str, column(prob_table, 0))))
    text += ' \\\\\n\\hline\n'
    for i in range(1, len(prob_table[0]) - 1):
        line  = '\t'
        line += ' & '.join(list(map(str, column(prob_table, i))))
        line += ' \\\\\n'
        text += line
    text += '\\hline'
    text += '\t'
    text += ' & '.join(list(map(str, column(prob_table, len(prob_table[0]) - 1))))
    text += ' \\\\\n'
    text += '\\hline'
    text += '\\end{tabular}'
    return text

def total_score(line):
    score = 0
    for s in line:
        if s != '-':
            score += float(s)
    return score



def main():
    dirlist = next(os.walk("Data/"))[1]
    dirlist.sort()

    domains = []

    if not os.path.isdir("table/"):
        os.makedirs("table/")
    write_file = open("table/table.tex", "w+")

    header  = "\\documentclass[10pt,a4paper]{article}\n\\usepackage[utf8]{inputenc}\n\\usepackage[french]{babel}"
    header += "\n\\usepackage[T1]{fontenc}\n\\usepackage{amsmath}\n\\usepackage{amsfonts}\n\\usepackage{amssymb}"
    header += "\n\\usepackage{graphicx}\n\\usepackage{multirow}\n\\author{Arnaud Lequen}\n\\begin{document}\n"

    write_file.write(header)

    # Retrieve domains list
    for p in range(len(dirlist)):
        planner = dirlist[p]
        problems = next(os.walk("Data/" + str(planner) + "/"))[1]
        problems.sort()
        for prob in problems:
            if not prob in domains:
                domains.append(prob)

    # There is no way to simultaneously draw two different graphs, so there is
    # some duplicated code here
    prob_id = 1

    summary = []

    for prob in domains:
        if (prob_id-1) % args.dpp == 0:
            write_file.write(make_pb_list(20)) #HARDCODED

        prob_table = []
        print(prob, end="_time...")
        if not os.path.exists("Figures/" + prob):
            os.makedirs("Figures/" + prob)

        summary.append([prob])

        n_problems = 0 # Number of problems
        # For each planner
        for p in range(len(dirlist)):
            planner = dirlist[p]
            prob_table.append([""])
            if os.path.isdir("Data/" + planner + "/" + prob):
                file_name = "/length.txt"
                if args.time:
                    file_name = "/time.txt"

                f = open("Data/" + planner + "/" + prob + file_name, "r")
                i = 0

                for line in f:
                    if not float(line) == -1 and (float(line) <= 301 or not args.time):
                        prob_table[-1].append(float(line))
                    else:
                        prob_table[-1].append(float('inf'))
                    i += 1
                f.close()
                n_problems = max(i, n_problems)

                if os.path.exists("Data/" + planner + "/name"):
                    f = open("Data/" + planner + "/name")
                    for line in f:
                        prob_table[-1][0], = line.split()
                        break
                    f.close()

        for p in range(len(prob_table)):
            prob_table[p] = fill(prob_table[p], n_problems+1)

        for i in range(1, n_problems+1):
            col = column(prob_table, i)
            m = min([i for i in col if i != 0])
            for j in range(len(col)):
                if prob_table[j][i] == float('inf') or prob_table[j][i] == 0: # Check if 0 is correct
                    prob_table[j][i] = '-'
                else:
                    prob_table[j][i] = '%.2f'%(m/prob_table[j][i])

        for planner in prob_table:
            s = total_score(planner[1:])
            planner.append('%.2f'%(s))
            summary[-1].append(s)

        remove_zero(prob_table)

        write_file.write(format_prob_table(prob_table, prob))
        if prob_id % args.dpp == 0:
            write_file.write("\\\\\n")

        prob_id += 1


        print("done")



    text = "\\begin{tabular}{l|" + ' '.join('r'*(len(summary[0]) - 1)) + '|}\n'
    text += 'Problem & '
    lst = []
    for planner in dirlist:
        if os.path.exists("Data/" + planner + "/name"):
            f = open("Data/" + planner + "/name")
            for line in f:
                plannername, = line.split()
                lst.append(plannername)
                break
            f.close()

    summary.sort()

    text += ' & '.join(lst)

    #text += ' & '.join(list(map(str, column(prob_table, i))))
    text += '\\\\\\hline\n'
    for problem in summary:
        text += '\t'
        text += problem[0] + ' & '
        text += ' & '.join(list(map(lambda x: '%.2f'%(x), problem[1:])))
        text += '\\\\\n'

    text += '\\hline\n'
    text += '\t'
    text += 'Total & '
    text += ' & '.join(['%.2f'%(sum(column(summary, planner))) for planner in range(1, len(summary[0]))])
    text += '\\end{tabular}'

    write_file.write(text)

    write_file.write("\\end{document}")
    write_file.close()


main()

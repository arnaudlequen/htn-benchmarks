import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import os
import sys
import argparse
import random

plt.rcParams.update({'figure.max_open_warning': 0})

#colors = ['r', 'g', 'b', 'y', 'c']
#markers = ['^', 'D', 's', 'p', 'x']

# Dominik's colors are way fancier
colors = ['#377eb8', '#ff7f00', '#f781bf', '#a65628', '#4daf4a', '#984ea3', '#999999', '#e41a1c', '#dede00']
markers = ['s', '^', '*', 'o', '+', 'x', 'd', 'D', 'p']

lines = ['--', '-.', ':', '-', '-']
d = 120 #dpi


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


parser = argparse.ArgumentParser(description='Creates graphs with the provided data.')
parser.add_argument('--width',  metavar="width", help="width of the output in pixels", type=int, default="1280")
parser.add_argument('--height', metavar="height", help="height of the output in pixels", type=int, default="860")
parser.add_argument('--ext', metavar="ext", help="extension of the output pictures", type=str, default="png")
parser.add_argument('--timeout', metavar="timeout", help="timeout for the cumulative graph", type=int, default="300")
parser.add_argument("--printready",  metavar="printready", type=str2bool, nargs='?',
                        const=True, default='false',
                        help="outputs a printable pdf")
parser.add_argument("--title",  metavar="title", type=str2bool, nargs='?',
                        const=True, default='true',
                        help="put a title or not")
parser.add_argument("--french",  metavar="french", type=str2bool, nargs='?',
                        const=True, default='false',
                        help="write legends in French")

args = parser.parse_args()

def main():
    global d

    if not args.ext in ['pdf', 'png', 'jpg', 'svg', 'jpeg']:
        print("Extension not supported")
        return

    if not os.path.exists("Figures/"):
        os.makedirs("Figures/")

    mainpdf = PdfPages('Figures/results.pdf')
    if args.printready:
        plt.figure(figsize=(args.width/d, args.height/d), dpi=d)
        if args.french:
            plt.title("Evaluations de T-REX, iSHOP et Madagascar")
        else:
            plt.title("Evaluations of T-REX, iSHOP and Madagascar")
        plt.axis('off')
        mainpdf.savefig()
        plt.clf()

    dirlist = next(os.walk("Data/"))[1]

    domains = []

    # Dictionnary : each key is a planner, and each entry is a list of computation times
    solvedproblems = {}

    # Retrieve domains list
    for p in range(len(dirlist)):
        planner = dirlist[p]
        problems = next(os.walk("Data/" + str(planner) + "/"))[1]
        for prob in problems:
            if not prob in domains:
                domains.append(prob)

    for prob in domains:
        # Time part
        print(prob, end="_time...")
        if not os.path.exists("Figures/" + prob):
            os.makedirs("Figures/" + prob)

        plt.figure(figsize=(args.width/d, args.height/d), dpi=d)
        legend = []

        n_problems = 0 # Number of problems
        for p in range(len(dirlist)):
            planner = dirlist[p]
            if os.path.isdir("Data/" + planner + "/" + prob):
                ids = []
                time = []
                f = open("Data/" + planner + "/" + prob + "/time.txt", "r")
                i = 1

                for line in f:
                    if not float(line) == -1 and float(line) <= 301:
                        ids.append(i)
                        time.append(float(line))
                    else:
                        ids.append(None)
                        time.append(None)
                    i += 1
                f.close()
                n_problems = max(i, n_problems)

                if os.path.exists("Data/" + planner + "/name"):
                    f = open("Data/" + planner + "/name")
                    for line in f:
                        planner, = line.split()
                        break
                    f.close()

                line, = plt.plot(ids, time, markers[p] + lines[p],
                                 color=colors[p], mfc="#ffffff00",
                                 mec=colors[p], label = planner)

                legend.append(line)

                try:
                    solvedproblems[planner].extend(time)
                except:
                    solvedproblems[planner] = time

        if args.title:
            plt.title(prob)
        plt.legend(handles = legend, loc=1)
        if args.french:
            plt.xlabel("Problème")
            plt.ylabel("Durée d'exécution (s)")
        else:
            plt.xlabel("Problem")
            plt.ylabel("Running Time (s)")
        plt.grid(True)
        plt.xticks([0, n_problems//4, n_problems//2, 3*(n_problems//4), n_problems-1])
        plt.savefig("Figures/" + prob + "/time." + args.ext)
        mainpdf.savefig()
        plt.clf()

        print("done")


        # Length part
        print(prob, end="_length...")
        if not os.path.exists("Figures/" + prob):
            os.makedirs("Figures/" + prob)

        plt.figure(figsize=(args.width/d, args.height/d), dpi=d)
        legend = []

        n_problems = 0 # Number of problems
        for p in range(len(dirlist)):
            planner = dirlist[p]
            if os.path.isdir("Data/" + planner + "/" + prob):
                ids = []
                plan = []
                f = open("Data/" + planner + "/" + prob + "/length.txt", "r")
                i = 1

                for line in f:
                    if not float(line) == -1:
                        ids.append(i)
                        plan.append(float(line))
                    else:
                        ids.append(None)
                        plan.append(None)
                    i += 1
                f.close()
                n_problems = max(n_problems, i)

                if os.path.exists("Data/" + planner + "/name"):
                    f = open("Data/" + planner + "/name")
                    for line in f:
                        planner, = line.split()
                        break
                    f.close()

                line, = plt.plot(ids, plan, markers[p] + lines[p],
                                 color=colors[p], mfc="#ffffff00",
                                 mec=colors[p], label = planner)

                legend.append(line)

        if args.title:
            plt.title(prob)
        plt.legend(handles = legend, loc=1)
        if args.french:
            plt.xlabel("Problème")
            plt.ylabel("Longueur du plan (nombre d'actions)")
        else:
            plt.xlabel("Problem")
            plt.ylabel("Plan length (# of actions)")
        plt.grid(True)
        plt.xticks([0, n_problems//4, n_problems//2, 3*(n_problems//4), n_problems-1])
        plt.savefig("Figures/" + prob + "/length." + args.ext)
        mainpdf.savefig()
        plt.clf()

        print("done")


    # Print the super cool graph
    legend = []
    p = 0
    for planner in solvedproblems:

        time = [t for t in solvedproblems[planner] if t is not None and t <= args.timeout]
        time.sort()

        line, = plt.plot(list(range(len(time))), time, markers[p] + lines[p],
                         color=colors[p], mfc="#ffffff00",
                         mec=colors[p], label = planner)

        legend.append(line)
        p += 1

    if args.title:
        plt.title("Schreiber's Graph")
    plt.legend(handles = legend, loc=1)
    if args.french:
            plt.xlabel("Problems résolus")
            plt.ylabel("Durée d'exécution max (s)")
    else:
        plt.xlabel("Problem solved")
        plt.ylabel("Timeout (s)")
    plt.grid(True)
    #plt.xticks([0, n_problems//4, n_problems//2, 3*(n_problems//4), n_problems-1])
    plt.savefig("Figures/score." + args.ext)
    plt.clf()


    mainpdf.close()


main()

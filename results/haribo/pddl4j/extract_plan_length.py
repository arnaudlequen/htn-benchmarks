import re
import numpy as np
import matplotlib.pyplot as plt
import os

MAX = 20

plan_length = [-1]
time = [-1]

def main():

    for i in range(1, MAX + 1):
        pad = str(i).zfill(2)
        in_file = open("result" + pad, "rt")
        tsk = False
        for line in in_file:
            if len(line) <= 1 and tsk:
                tsk = False

            if tsk:
                taskid, _ = line.split(":")
                taskid = re.sub("[^0-9]", "", taskid)
                plan_length[i] = int(taskid)

            if "Tasks:" in line:
                tsk = True
                plan_length.append(0)


            if "seconds total time" in line:
                n = float(re.sub("[^0-9.]", "", line))
                time.append(n)
        in_file.close()

    plan_length.remove(-1)
    time.remove(-1)

    print(plan_length)
    print(time)

    plt.plot(list(range(1, 21)), plan_length, 'ro')
    plt.xlabel("Problem")
    plt.ylabel("Plan length")
    plt.grid(True)
    plt.xticks(list(range(0, 21, 5)))
    plt.show()


main()

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.176] Processed problem encoding.
[0.180] Calculated possible fact changes of composite elements.
[0.181] Initialized instantiation procedure.
[0.181] 
[0.181] *************************************
[0.181] * * *   M a k e s p a n     0   * * *
[0.181] *************************************
[0.182] Instantiated 269 initial clauses.
[0.182] The encoding contains a total of 183 distinct variables.
[0.182] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.182] Executed solver; result: UNSAT.
[0.182] 
[0.182] *************************************
[0.182] * * *   M a k e s p a n     1   * * *
[0.182] *************************************
[0.182] Computed next depth properties: array size of 3.
[0.183] Instantiated 23 transitional clauses.
[0.185] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.185] Instantiated 357 universal clauses.
[0.185] Instantiated and added clauses for a total of 649 clauses.
[0.185] The encoding contains a total of 286 distinct variables.
[0.185] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.186] Executed solver; result: UNSAT.
[0.186] 
[0.186] *************************************
[0.186] * * *   M a k e s p a n     2   * * *
[0.186] *************************************
[0.186] Computed next depth properties: array size of 6.
[0.188] Instantiated 585 transitional clauses.
[0.192] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.192] Instantiated 4,542 universal clauses.
[0.192] Instantiated and added clauses for a total of 5,776 clauses.
[0.192] The encoding contains a total of 774 distinct variables.
[0.192] Attempting solve with solver <glucose4> ...
c 6 assumptions
[0.192] Executed solver; result: UNSAT.
[0.192] 
[0.192] *************************************
[0.192] * * *   M a k e s p a n     3   * * *
[0.192] *************************************
[0.197] Computed next depth properties: array size of 14.
[0.219] Instantiated 117,393 transitional clauses.
[0.614] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.614] Instantiated 11,509,706 universal clauses.
[0.614] Instantiated and added clauses for a total of 11,632,875 clauses.
[0.614] The encoding contains a total of 8,672 distinct variables.
[0.614] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.615] Executed solver; result: UNSAT.
[0.615] 
[0.615] *************************************
[0.615] * * *   M a k e s p a n     4   * * *
[0.615] *************************************
[0.624] Computed next depth properties: array size of 23.
[0.664] Instantiated 131,699 transitional clauses.
[2.244] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.244] Instantiated 23,016,097 universal clauses.
[2.244] Instantiated and added clauses for a total of 34,780,671 clauses.
[2.244] The encoding contains a total of 23,777 distinct variables.
[2.244] Attempting solve with solver <glucose4> ...
c 23 assumptions
[2.247] Executed solver; result: UNSAT.
[2.247] 
[2.247] *************************************
[2.247] * * *   M a k e s p a n     5   * * *
[2.247] *************************************
[2.261] Computed next depth properties: array size of 33.
[2.311] Instantiated 146,081 transitional clauses.
[5.032] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[5.032] Instantiated 34,523,715 universal clauses.
[5.032] Instantiated and added clauses for a total of 69,450,467 clauses.
[5.032] The encoding contains a total of 46,128 distinct variables.
[5.032] Attempting solve with solver <glucose4> ...
c 33 assumptions
[5.040] Executed solver; result: UNSAT.
[5.040] 
[5.040] *************************************
[5.040] * * *   M a k e s p a n     6   * * *
[5.040] *************************************
[5.063] Computed next depth properties: array size of 44.
[5.151] Instantiated 160,539 transitional clauses.
[9.168] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[9.168] Instantiated 46,032,560 universal clauses.
[9.168] Instantiated and added clauses for a total of 115,643,566 clauses.
[9.168] The encoding contains a total of 75,764 distinct variables.
[9.168] Attempting solve with solver <glucose4> ...
c 44 assumptions
[9.176] Executed solver; result: UNSAT.
[9.176] 
[9.176] *************************************
[9.176] * * *   M a k e s p a n     7   * * *
[9.176] *************************************
[9.195] Computed next depth properties: array size of 56.
[9.283] Instantiated 175,073 transitional clauses.
[14.372] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[14.372] Instantiated 57,542,632 universal clauses.
[14.372] Instantiated and added clauses for a total of 173,361,271 clauses.
[14.372] The encoding contains a total of 112,724 distinct variables.
[14.372] Attempting solve with solver <glucose4> ...
c 56 assumptions
c |       90         0      111 |   77792 115717122 232588102 |     2     6229      428     3768 | 30.988 % |
c |      185         0      108 |   77792 115717122 232588102 |     3    11217      432     8780 | 30.988 % |
c |      333         0       90 |   77792 115717122 232588102 |     4    13822      478    16175 | 30.988 % |
c |      359         0      111 |   77792 115717122 232588102 |     5    13913      756    26084 | 30.988 % |
c |      376         0      132 |   77792 115717122 232588102 |     5    23913      862    26084 | 30.988 % |
[108.633] Executed solver; result: UNSAT.
[108.633] 
[108.633] *************************************
[108.633] * * *   M a k e s p a n     8   * * *
[108.633] *************************************
[108.663] Computed next depth properties: array size of 69.
[108.775] Instantiated 189,683 transitional clauses.
[114.992] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[114.992] Instantiated 69,053,931 universal clauses.
[114.992] Instantiated and added clauses for a total of 242,604,885 clauses.
[114.992] The encoding contains a total of 157,047 distinct variables.
[114.992] Attempting solve with solver <glucose4> ...
c 69 assumptions
c |      414        13      144 |  115124 173449957 348470317 |     6    21473     1046    38523 | 26.694 % |
c last restart ## conflicts  :  3221 246 
[115.899] Executed solver; result: SAT.
[115.899] Solver returned SAT; a solution has been found at makespan 8.
35
solution 5763 1
7 567 206 90 1723 153 147 150 24 3576 701 218 100 1943 157 163 160 55 4856 953 324 121 2001 169 166 172 58 5763 842 259 144 2193 180 174 177 
[115.902] Exiting.

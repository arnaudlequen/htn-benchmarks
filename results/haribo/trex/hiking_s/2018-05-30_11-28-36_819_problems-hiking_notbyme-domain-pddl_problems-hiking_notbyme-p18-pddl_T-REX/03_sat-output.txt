Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.008] Parsed head comment information.
[2.404] Processed problem encoding.
[2.485] Calculated possible fact changes of composite elements.
[2.486] Initialized instantiation procedure.
[2.486] 
[2.486] *************************************
[2.486] * * *   M a k e s p a n     0   * * *
[2.486] *************************************
[2.533] Instantiated 512 initial clauses.
[2.533] The encoding contains a total of 344 distinct variables.
[2.533] Attempting solve with solver <glucose4> ...
c 2 assumptions
[2.533] Executed solver; result: UNSAT.
[2.533] 
[2.533] *************************************
[2.533] * * *   M a k e s p a n     1   * * *
[2.533] *************************************
[2.537] Computed next depth properties: array size of 3.
[2.553] Instantiated 27 transitional clauses.
[2.614] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.614] Instantiated 679 universal clauses.
[2.614] Instantiated and added clauses for a total of 1,218 clauses.
[2.614] The encoding contains a total of 529 distinct variables.
[2.614] Attempting solve with solver <glucose4> ...
c 3 assumptions
[2.614] Executed solver; result: UNSAT.
[2.614] 
[2.614] *************************************
[2.614] * * *   M a k e s p a n     2   * * *
[2.614] *************************************
[2.621] Computed next depth properties: array size of 6.
[2.647] Instantiated 1,977 transitional clauses.
[2.740] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.740] Instantiated 37,185 universal clauses.
[2.740] Instantiated and added clauses for a total of 40,380 clauses.
[2.740] The encoding contains a total of 1,993 distinct variables.
[2.740] Attempting solve with solver <glucose4> ...
c 6 assumptions
[2.741] Executed solver; result: UNSAT.
[2.741] 
[2.741] *************************************
[2.741] * * *   M a k e s p a n     3   * * *
[2.741] *************************************
[2.790] Computed next depth properties: array size of 14.
[3.125] Instantiated 1,905,651 transitional clauses.
[4.373] 92.9% quadratic At-Most-One encodings, the rest being binary encoded.
[4.373] Instantiated 30,775,522 universal clauses.
[4.373] Instantiated and added clauses for a total of 32,721,553 clauses.
[4.373] The encoding contains a total of 73,446 distinct variables.
[4.374] Attempting solve with solver <glucose4> ...
c 14 assumptions
[4.375] Executed solver; result: UNSAT.
[4.375] 
[4.375] *************************************
[4.375] * * *   M a k e s p a n     4   * * *
[4.375] *************************************
[4.477] Computed next depth properties: array size of 23.
[5.044] Instantiated 2,044,727 transitional clauses.
[9.577] 91.3% quadratic At-Most-One encodings, the rest being binary encoded.
[9.577] Instantiated 61,523,542 universal clauses.
[9.577] Instantiated and added clauses for a total of 96,289,822 clauses.
[9.577] The encoding contains a total of 214,552 distinct variables.
[9.577] Attempting solve with solver <glucose4> ...
c 23 assumptions
[9.631] Executed solver; result: UNSAT.
[9.631] 
[9.631] *************************************
[9.631] * * *   M a k e s p a n     5   * * *
[9.631] *************************************
[9.782] Computed next depth properties: array size of 33.
[10.454] Instantiated 2,184,057 transitional clauses.
[18.270] 90.9% quadratic At-Most-One encodings, the rest being binary encoded.
[18.270] Instantiated 92,281,245 universal clauses.
[18.270] Instantiated and added clauses for a total of 190,755,124 clauses.
[18.270] The encoding contains a total of 425,439 distinct variables.
[18.270] Attempting solve with solver <glucose4> ...
c 33 assumptions
[18.311] Executed solver; result: UNSAT.
[18.311] 
[18.311] *************************************
[18.311] * * *   M a k e s p a n     6   * * *
[18.311] *************************************
[18.508] Computed next depth properties: array size of 44.
[19.312] Instantiated 2,323,641 transitional clauses.
[30.465] 90.9% quadratic At-Most-One encodings, the rest being binary encoded.
[30.466] Instantiated 123,048,631 universal clauses.
[30.466] Instantiated and added clauses for a total of 316,127,396 clauses.
[30.466] The encoding contains a total of 706,235 distinct variables.
[30.466] Attempting solve with solver <glucose4> ...
c 44 assumptions
[30.504] Executed solver; result: UNSAT.
[30.504] 
[30.504] *************************************
[30.504] * * *   M a k e s p a n     7   * * *
[30.504] *************************************
[30.715] Computed next depth properties: array size of 56.
[31.638] Instantiated 2,463,479 transitional clauses.
Interrupt signal (15) received.

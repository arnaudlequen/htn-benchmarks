Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.106] Processed problem encoding.
[0.109] Calculated possible fact changes of composite elements.
[0.109] Initialized instantiation procedure.
[0.109] 
[0.109] *************************************
[0.109] * * *   M a k e s p a n     0   * * *
[0.109] *************************************
[0.110] Instantiated 220 initial clauses.
[0.110] The encoding contains a total of 150 distinct variables.
[0.110] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.110] Executed solver; result: UNSAT.
[0.110] 
[0.110] *************************************
[0.110] * * *   M a k e s p a n     1   * * *
[0.110] *************************************
[0.110] Computed next depth properties: array size of 3.
[0.111] Instantiated 19 transitional clauses.
[0.112] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.112] Instantiated 291 universal clauses.
[0.112] Instantiated and added clauses for a total of 530 clauses.
[0.112] The encoding contains a total of 235 distinct variables.
[0.112] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.112] Executed solver; result: UNSAT.
[0.112] 
[0.112] *************************************
[0.112] * * *   M a k e s p a n     2   * * *
[0.112] *************************************
[0.112] Computed next depth properties: array size of 6.
[0.114] Instantiated 407 transitional clauses.
[0.116] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.116] Instantiated 2,861 universal clauses.
[0.116] Instantiated and added clauses for a total of 3,798 clauses.
[0.116] The encoding contains a total of 581 distinct variables.
[0.116] Attempting solve with solver <glucose4> ...
c 6 assumptions
[0.116] Executed solver; result: UNSAT.
[0.116] 
[0.116] *************************************
[0.116] * * *   M a k e s p a n     3   * * *
[0.116] *************************************
[0.119] Computed next depth properties: array size of 14.
[0.132] Instantiated 62,063 transitional clauses.
[0.266] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.266] Instantiated 3,174,811 universal clauses.
[0.266] Instantiated and added clauses for a total of 3,240,672 clauses.
[0.266] The encoding contains a total of 5,138 distinct variables.
[0.266] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.266] Executed solver; result: UNSAT.
[0.266] 
[0.266] *************************************
[0.266] * * *   M a k e s p a n     4   * * *
[0.266] *************************************
[0.272] Computed next depth properties: array size of 23.
[0.296] Instantiated 70,063 transitional clauses.
[0.805] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.805] Instantiated 6,347,565 universal clauses.
[0.805] Instantiated and added clauses for a total of 9,658,300 clauses.
[0.805] The encoding contains a total of 13,740 distinct variables.
[0.805] Attempting solve with solver <glucose4> ...
c 23 assumptions
[0.806] Executed solver; result: UNSAT.
[0.806] 
[0.806] *************************************
[0.806] * * *   M a k e s p a n     5   * * *
[0.806] *************************************
[0.814] Computed next depth properties: array size of 33.
[0.843] Instantiated 78,121 transitional clauses.
[1.730] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.730] Instantiated 9,521,123 universal clauses.
[1.730] Instantiated and added clauses for a total of 19,257,544 clauses.
[1.730] The encoding contains a total of 26,417 distinct variables.
[1.730] Attempting solve with solver <glucose4> ...
c 33 assumptions
[1.732] Executed solver; result: UNSAT.
[1.732] 
[1.732] *************************************
[1.732] * * *   M a k e s p a n     6   * * *
[1.732] *************************************
[1.743] Computed next depth properties: array size of 44.
[1.781] Instantiated 86,237 transitional clauses.
[2.924] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.924] Instantiated 12,695,485 universal clauses.
[2.924] Instantiated and added clauses for a total of 32,039,266 clauses.
[2.924] The encoding contains a total of 43,199 distinct variables.
[2.924] Attempting solve with solver <glucose4> ...
c 44 assumptions
c |       62         0      161 |   27989 19315518 39061456 |     2     6222      302     3775 | 35.208 % |
[7.156] Executed solver; result: UNSAT.
[7.156] 
[7.156] *************************************
[7.156] * * *   M a k e s p a n     7   * * *
[7.156] *************************************
[7.168] Computed next depth properties: array size of 56.
[7.215] Instantiated 94,411 transitional clauses.
[8.558] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[8.558] Instantiated 15,870,651 universal clauses.
[8.558] Instantiated and added clauses for a total of 48,004,328 clauses.
[8.558] The encoding contains a total of 64,116 distinct variables.
[8.558] Attempting solve with solver <glucose4> ...
c 56 assumptions
c |       64        31      312 |   45123 32111867 64852645 |     3    11220      524     8777 | 29.622 % |
c last restart ## conflicts  :  405 1615 
[9.117] Executed solver; result: SAT.
[9.117] Solver returned SAT; a solution has been found at makespan 7.
26
solution 3271 1
20 372 158 57 1033 111 117 114 33 2361 485 169 90 1221 123 126 120 51 3271 663 257 102 1291 132 135 129 
[9.119] Exiting.

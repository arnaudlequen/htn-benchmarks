Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.002] Parsed head comment information.
[0.414] Processed problem encoding.
[0.426] Calculated possible fact changes of composite elements.
[0.426] Initialized instantiation procedure.
[0.426] 
[0.426] *************************************
[0.426] * * *   M a k e s p a n     0   * * *
[0.426] *************************************
[0.436] Instantiated 367 initial clauses.
[0.436] The encoding contains a total of 249 distinct variables.
[0.436] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.436] Executed solver; result: UNSAT.
[0.436] 
[0.436] *************************************
[0.436] * * *   M a k e s p a n     1   * * *
[0.436] *************************************
[0.437] Computed next depth properties: array size of 3.
[0.439] Instantiated 31 transitional clauses.
[0.451] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.451] Instantiated 489 universal clauses.
[0.451] Instantiated and added clauses for a total of 887 clauses.
[0.451] The encoding contains a total of 388 distinct variables.
[0.451] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.451] Executed solver; result: UNSAT.
[0.451] 
[0.451] *************************************
[0.451] * * *   M a k e s p a n     2   * * *
[0.451] *************************************
[0.453] Computed next depth properties: array size of 6.
[0.458] Instantiated 1,013 transitional clauses.
[0.473] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.473] Instantiated 9,092 universal clauses.
[0.473] Instantiated and added clauses for a total of 10,992 clauses.
[0.473] The encoding contains a total of 1,232 distinct variables.
[0.473] Attempting solve with solver <glucose4> ...
c 6 assumptions
[0.473] Executed solver; result: UNSAT.
[0.473] 
[0.473] *************************************
[0.473] * * *   M a k e s p a n     3   * * *
[0.473] *************************************
[0.484] Computed next depth properties: array size of 14.
[0.536] Instantiated 280,037 transitional clauses.
[0.707] 92.9% quadratic At-Most-One encodings, the rest being binary encoded.
[0.707] Instantiated 3,979,545 universal clauses.
[0.707] Instantiated and added clauses for a total of 4,270,574 clauses.
[0.707] The encoding contains a total of 18,634 distinct variables.
[0.707] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.707] Executed solver; result: UNSAT.
[0.707] 
[0.707] *************************************
[0.707] * * *   M a k e s p a n     4   * * *
[0.707] *************************************
[0.727] Computed next depth properties: array size of 23.
[0.817] Instantiated 312,427 transitional clauses.
[1.388] 91.3% quadratic At-Most-One encodings, the rest being binary encoded.
[1.388] Instantiated 7,952,314 universal clauses.
[1.388] Instantiated and added clauses for a total of 12,535,315 clauses.
[1.388] The encoding contains a total of 52,317 distinct variables.
[1.388] Attempting solve with solver <glucose4> ...
c 23 assumptions
[1.395] Executed solver; result: UNSAT.
[1.395] 
[1.395] *************************************
[1.395] * * *   M a k e s p a n     5   * * *
[1.395] *************************************
[1.426] Computed next depth properties: array size of 33.
[1.535] Instantiated 344,929 transitional clauses.
[2.521] 90.9% quadratic At-Most-One encodings, the rest being binary encoded.
[2.521] Instantiated 11,927,399 universal clauses.
[2.521] Instantiated and added clauses for a total of 24,807,643 clauses.
[2.521] The encoding contains a total of 102,338 distinct variables.
[2.521] Attempting solve with solver <glucose4> ...
c 33 assumptions
[2.532] Executed solver; result: UNSAT.
[2.532] 
[2.532] *************************************
[2.532] * * *   M a k e s p a n     6   * * *
[2.532] *************************************
[2.585] Computed next depth properties: array size of 44.
[2.720] Instantiated 377,543 transitional clauses.
[4.101] 90.9% quadratic At-Most-One encodings, the rest being binary encoded.
[4.101] Instantiated 15,904,800 universal clauses.
[4.101] Instantiated and added clauses for a total of 41,089,986 clauses.
[4.101] The encoding contains a total of 168,754 distinct variables.
[4.101] Attempting solve with solver <glucose4> ...
c 44 assumptions
[4.231] Executed solver; result: UNSAT.
[4.231] 
[4.231] *************************************
[4.231] * * *   M a k e s p a n     7   * * *
[4.231] *************************************
[4.281] Computed next depth properties: array size of 56.
[4.441] Instantiated 410,269 transitional clauses.
[6.221] 91.1% quadratic At-Most-One encodings, the rest being binary encoded.
[6.221] Instantiated 19,884,517 universal clauses.
[6.221] Instantiated and added clauses for a total of 61,384,772 clauses.
[6.221] The encoding contains a total of 251,622 distinct variables.
[6.221] Attempting solve with solver <glucose4> ...
c 56 assumptions
[6.233] Executed solver; result: UNSAT.
[6.233] 
[6.233] *************************************
[6.233] * * *   M a k e s p a n     8   * * *
[6.233] *************************************
[6.296] Computed next depth properties: array size of 69.
[6.475] Instantiated 443,107 transitional clauses.
[8.645] 91.3% quadratic At-Most-One encodings, the rest being binary encoded.
[8.645] Instantiated 23,866,550 universal clauses.
[8.645] Instantiated and added clauses for a total of 85,694,429 clauses.
[8.645] The encoding contains a total of 350,999 distinct variables.
[8.646] Attempting solve with solver <glucose4> ...
c 69 assumptions
[8.679] Executed solver; result: UNSAT.
[8.679] 
[8.679] *************************************
[8.679] * * *   M a k e s p a n     9   * * *
[8.679] *************************************
[8.748] Computed next depth properties: array size of 83.
[8.960] Instantiated 476,057 transitional clauses.
[11.521] 91.6% quadratic At-Most-One encodings, the rest being binary encoded.
[11.521] Instantiated 27,850,899 universal clauses.
[11.521] Instantiated and added clauses for a total of 114,021,385 clauses.
[11.521] The encoding contains a total of 466,942 distinct variables.
[11.521] Attempting solve with solver <glucose4> ...
c 83 assumptions
c |      102         0       98 |  353994 85790596 176317272 |     2     6213      354     3781 | 24.189 % |
c |      141         0      141 |  353994 85790596 176317272 |     3    11218      662     8776 | 24.189 % |
c |      185         0      162 |  353994 85790596 176317272 |     4    13813      882    16181 | 24.189 % |
c |      230         0      173 |  353994 85790596 176317272 |     5    13924     1142    26070 | 24.189 % |
c |      282         0      177 |  353994 85790596 176317272 |     5    23924     1664    26070 | 24.189 % |
c |      288         0      208 |  353994 85790596 176317272 |     6    21486     2074    38508 | 24.189 % |
[134.549] Executed solver; result: UNSAT.
[134.549] 
[134.549] *************************************
[134.549] * * *   M a k e s p a n    10   * * *
[134.549] *************************************
[134.630] Computed next depth properties: array size of 98.
[134.888] Instantiated 509,119 transitional clauses.
[137.857] 91.8% quadratic At-Most-One encodings, the rest being binary encoded.
[137.857] Instantiated 31,837,564 universal clauses.
[137.857] Instantiated and added clauses for a total of 146,368,068 clauses.
[137.857] The encoding contains a total of 599,508 distinct variables.
[137.857] Attempting solve with solver <glucose4> ...
c 98 assumptions
c |      295        17      237 |  470366 114130003 234199999 |     7    16462     2374    53527 | 21.541 % |
c last restart ## conflicts  :  726 585 
[145.827] Executed solver; result: SAT.
[145.827] Solver returned SAT; a solution has been found at makespan 10.
53
solution 13683 1
20 1154 280 128 3755 227 221 224 35 6413 1229 320 144 3893 228 231 234 51 8341 1343 337 154 4072 244 241 238 58 10438 1760 474 175 4177 247 253 250 91 12868 2532 746 193 4267 259 262 256 94 13683 1549 398 215 4413 272 269 266 
[145.835] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.044] Processed problem encoding.
[0.045] Calculated possible fact changes of composite elements.
[0.045] Initialized instantiation procedure.
[0.045] 
[0.045] *************************************
[0.045] * * *   M a k e s p a n     0   * * *
[0.045] *************************************
[0.045] Instantiated 171 initial clauses.
[0.045] The encoding contains a total of 117 distinct variables.
[0.045] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.045] Executed solver; result: UNSAT.
[0.045] 
[0.045] *************************************
[0.045] * * *   M a k e s p a n     1   * * *
[0.045] *************************************
[0.045] Computed next depth properties: array size of 3.
[0.046] Instantiated 15 transitional clauses.
[0.046] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.046] Instantiated 225 universal clauses.
[0.046] Instantiated and added clauses for a total of 411 clauses.
[0.046] The encoding contains a total of 184 distinct variables.
[0.046] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.046] Executed solver; result: UNSAT.
[0.046] 
[0.046] *************************************
[0.046] * * *   M a k e s p a n     2   * * *
[0.046] *************************************
[0.046] Computed next depth properties: array size of 6.
[0.047] Instantiated 253 transitional clauses.
[0.048] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.048] Instantiated 1,576 universal clauses.
[0.048] Instantiated and added clauses for a total of 2,240 clauses.
[0.048] The encoding contains a total of 412 distinct variables.
[0.048] Attempting solve with solver <glucose4> ...
c 6 assumptions
[0.048] Executed solver; result: UNSAT.
[0.048] 
[0.048] *************************************
[0.048] * * *   M a k e s p a n     3   * * *
[0.048] *************************************
[0.049] Computed next depth properties: array size of 14.
[0.055] Instantiated 24,061 transitional clauses.
[0.079] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.079] Instantiated 485,661 universal clauses.
[0.079] Instantiated and added clauses for a total of 511,962 clauses.
[0.079] The encoding contains a total of 2,564 distinct variables.
[0.079] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.079] Executed solver; result: UNSAT.
[0.079] 
[0.079] *************************************
[0.079] * * *   M a k e s p a n     4   * * *
[0.079] *************************************
[0.081] Computed next depth properties: array size of 23.
[0.091] Instantiated 27,579 transitional clauses.
[0.171] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.171] Instantiated 970,208 universal clauses.
[0.171] Instantiated and added clauses for a total of 1,509,749 clauses.
[0.171] The encoding contains a total of 6,511 distinct variables.
[0.171] Attempting solve with solver <glucose4> ...
c 23 assumptions
[0.171] Executed solver; result: UNSAT.
[0.171] 
[0.171] *************************************
[0.171] * * *   M a k e s p a n     5   * * *
[0.171] *************************************
[0.175] Computed next depth properties: array size of 33.
[0.187] Instantiated 31,137 transitional clauses.
[0.315] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.315] Instantiated 1,455,217 universal clauses.
[0.315] Instantiated and added clauses for a total of 2,996,103 clauses.
[0.315] The encoding contains a total of 12,274 distinct variables.
[0.315] Attempting solve with solver <glucose4> ...
c 33 assumptions
[0.370] Executed solver; result: UNSAT.
[0.370] 
[0.370] *************************************
[0.370] * * *   M a k e s p a n     6   * * *
[0.370] *************************************
[0.375] Computed next depth properties: array size of 44.
[0.390] Instantiated 34,735 transitional clauses.
[0.569] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.569] Instantiated 1,940,688 universal clauses.
[0.569] Instantiated and added clauses for a total of 4,971,526 clauses.
[0.569] The encoding contains a total of 19,874 distinct variables.
[0.569] Attempting solve with solver <glucose4> ...
c 44 assumptions
c last restart ## conflicts  :  92 244 
[0.577] Executed solver; result: SAT.
[0.577] Solver returned SAT; a solution has been found at makespan 6.
17
solution 1369 1
15 207 115 51 510 78 75 81 27 1369 317 121 63 709 90 87 84 
[0.578] Exiting.

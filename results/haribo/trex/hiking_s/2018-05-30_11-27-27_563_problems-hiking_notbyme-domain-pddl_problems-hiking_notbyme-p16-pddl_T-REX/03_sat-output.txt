Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.004] Parsed head comment information.
[0.802] Processed problem encoding.
[0.819] Calculated possible fact changes of composite elements.
[0.820] Initialized instantiation procedure.
[0.820] 
[0.820] *************************************
[0.820] * * *   M a k e s p a n     0   * * *
[0.820] *************************************
[0.830] Instantiated 354 initial clauses.
[0.830] The encoding contains a total of 238 distinct variables.
[0.830] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.830] Executed solver; result: UNSAT.
[0.830] 
[0.830] *************************************
[0.830] * * *   M a k e s p a n     1   * * *
[0.830] *************************************
[0.831] Computed next depth properties: array size of 3.
[0.836] Instantiated 19 transitional clauses.
[0.853] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.853] Instantiated 467 universal clauses.
[0.853] Instantiated and added clauses for a total of 840 clauses.
[0.853] The encoding contains a total of 367 distinct variables.
[0.853] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.853] Executed solver; result: UNSAT.
[0.853] 
[0.853] *************************************
[0.853] * * *   M a k e s p a n     2   * * *
[0.853] *************************************
[0.855] Computed next depth properties: array size of 6.
[0.863] Instantiated 1,013 transitional clauses.
[0.886] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.886] Instantiated 14,395 universal clauses.
[0.886] Instantiated and added clauses for a total of 16,248 clauses.
[0.886] The encoding contains a total of 1,103 distinct variables.
[0.886] Attempting solve with solver <glucose4> ...
c 6 assumptions
[0.887] Executed solver; result: UNSAT.
[0.887] 
[0.887] *************************************
[0.887] * * *   M a k e s p a n     3   * * *
[0.887] *************************************
[0.903] Computed next depth properties: array size of 14.
[1.008] Instantiated 603,767 transitional clauses.
[1.267] 92.9% quadratic At-Most-One encodings, the rest being binary encoded.
[1.267] Instantiated 5,555,082 universal clauses.
[1.267] Instantiated and added clauses for a total of 6,175,097 clauses.
[1.267] The encoding contains a total of 25,529 distinct variables.
[1.267] Attempting solve with solver <glucose4> ...
c 14 assumptions
[1.268] Executed solver; result: UNSAT.
[1.268] 
[1.268] *************************************
[1.268] * * *   M a k e s p a n     4   * * *
[1.268] *************************************
[1.297] Computed next depth properties: array size of 23.
[1.472] Instantiated 650,503 transitional clauses.
[2.276] 91.3% quadratic At-Most-One encodings, the rest being binary encoded.
[2.276] Instantiated 11,099,717 universal clauses.
[2.276] Instantiated and added clauses for a total of 17,925,317 clauses.
[2.276] The encoding contains a total of 73,407 distinct variables.
[2.276] Attempting solve with solver <glucose4> ...
c 23 assumptions
[2.285] Executed solver; result: UNSAT.
[2.285] 
[2.285] *************************************
[2.285] * * *   M a k e s p a n     5   * * *
[2.285] *************************************
[2.336] Computed next depth properties: array size of 33.
[2.550] Instantiated 697,393 transitional clauses.
[3.897] 90.9% quadratic At-Most-One encodings, the rest being binary encoded.
[3.897] Instantiated 16,648,300 universal clauses.
[3.897] Instantiated and added clauses for a total of 35,271,010 clauses.
[3.897] The encoding contains a total of 144,815 distinct variables.
[3.897] Attempting solve with solver <glucose4> ...
c 33 assumptions
[3.905] Executed solver; result: UNSAT.
[3.905] 
[3.905] *************************************
[3.905] * * *   M a k e s p a n     6   * * *
[3.905] *************************************
[3.964] Computed next depth properties: array size of 44.
[4.214] Instantiated 744,437 transitional clauses.
[6.115] 90.9% quadratic At-Most-One encodings, the rest being binary encoded.
[6.115] Instantiated 22,200,831 universal clauses.
[6.115] Instantiated and added clauses for a total of 58,216,278 clauses.
[6.115] The encoding contains a total of 239,831 distinct variables.
[6.115] Attempting solve with solver <glucose4> ...
c 44 assumptions
[6.204] Executed solver; result: UNSAT.
[6.204] 
[6.204] *************************************
[6.204] * * *   M a k e s p a n     7   * * *
[6.204] *************************************
[6.273] Computed next depth properties: array size of 56.
[6.569] Instantiated 791,635 transitional clauses.
[9.021] 91.1% quadratic At-Most-One encodings, the rest being binary encoded.
[9.021] Instantiated 27,757,310 universal clauses.
[9.021] Instantiated and added clauses for a total of 86,765,223 clauses.
[9.021] The encoding contains a total of 358,533 distinct variables.
[9.021] Attempting solve with solver <glucose4> ...
c 56 assumptions
[9.714] Executed solver; result: UNSAT.
[9.714] 
[9.714] *************************************
[9.714] * * *   M a k e s p a n     8   * * *
[9.714] *************************************
[9.810] Computed next depth properties: array size of 69.
[10.142] Instantiated 838,987 transitional clauses.
[13.124] 91.3% quadratic At-Most-One encodings, the rest being binary encoded.
[13.124] Instantiated 33,317,737 universal clauses.
[13.124] Instantiated and added clauses for a total of 120,921,947 clauses.
[13.124] The encoding contains a total of 500,999 distinct variables.
[13.124] Attempting solve with solver <glucose4> ...
c 69 assumptions
c |       90         0      111 |  366073 87785981 182397476 |     2     6241       86     3756 | 26.931 % |
c last restart ## conflicts  :  160 869 
[19.278] Executed solver; result: SAT.
[19.278] Solver returned SAT; a solution has been found at makespan 8.
50
solution 20128 1
16 1000 387 166 3791 311 326 321 306 91 10923 1265 469 242 4173 337 332 352 347 112 17811 1708 545 257 5031 372 377 362 357 28 8608 3093 842 175 3748 315 75 12658 1422 493 228 4567 338 123 20128 2272 663 288 5008 367 
[19.284] Exiting.

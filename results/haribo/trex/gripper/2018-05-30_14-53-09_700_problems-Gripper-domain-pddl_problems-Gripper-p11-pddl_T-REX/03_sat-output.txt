Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.001] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.002] Processed problem encoding.
[0.002] Calculated possible fact changes of composite elements.
[0.003] Initialized instantiation procedure.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     0   * * *
[0.003] *************************************
[0.003] Instantiated 626 initial clauses.
[0.003] The encoding contains a total of 373 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.003] Executed solver; result: UNSAT.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     1   * * *
[0.003] *************************************
[0.004] Computed next depth properties: array size of 73.
[0.004] Instantiated 326 transitional clauses.
[0.006] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.006] Instantiated 1,713 universal clauses.
[0.006] Instantiated and added clauses for a total of 2,665 clauses.
[0.006] The encoding contains a total of 809 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 73 assumptions
c last restart ## conflicts  :  0 120 
[0.006] Executed solver; result: SAT.
[0.006] Solver returned SAT; a solution has been found at makespan 1.
71
solution 99 1
98 92 5 99 93 2 90 84 5 91 85 2 82 76 5 83 77 2 74 68 5 75 69 2 66 60 5 67 61 2 58 52 5 59 53 2 50 44 5 51 45 2 42 36 5 43 37 2 34 28 5 35 29 2 26 20 5 27 21 2 18 12 5 19 13 2 9 8 5 11 10 
[0.006] Exiting.

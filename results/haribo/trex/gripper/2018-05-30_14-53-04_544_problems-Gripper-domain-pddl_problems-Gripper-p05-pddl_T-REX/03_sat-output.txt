Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 308 initial clauses.
[0.001] The encoding contains a total of 187 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.002] Computed next depth properties: array size of 37.
[0.002] Instantiated 164 transitional clauses.
[0.002] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.002] Instantiated 849 universal clauses.
[0.002] Instantiated and added clauses for a total of 1,321 clauses.
[0.002] The encoding contains a total of 407 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 37 assumptions
c last restart ## conflicts  :  0 60 
[0.003] Executed solver; result: SAT.
[0.003] Solver returned SAT; a solution has been found at makespan 1.
35
solution 49 1
48 46 5 49 47 2 42 36 5 43 37 2 34 28 5 35 29 2 26 20 5 27 21 2 18 12 5 19 13 2 9 8 5 11 10 
[0.003] Exiting.

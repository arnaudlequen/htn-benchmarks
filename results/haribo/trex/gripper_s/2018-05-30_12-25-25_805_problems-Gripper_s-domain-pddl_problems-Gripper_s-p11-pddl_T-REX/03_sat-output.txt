Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.600] Processed problem encoding.
[0.681] Calculated possible fact changes of composite elements.
[0.682] Initialized instantiation procedure.
[0.682] 
[0.682] *************************************
[0.682] * * *   M a k e s p a n     0   * * *
[0.682] *************************************
[0.683] Instantiated 1,455 initial clauses.
[0.683] The encoding contains a total of 1,334 distinct variables.
[0.683] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.683] Executed solver; result: UNSAT.
[0.683] 
[0.683] *************************************
[0.683] * * *   M a k e s p a n     1   * * *
[0.683] *************************************
[0.695] Computed next depth properties: array size of 8.
[0.869] Instantiated 39,012 transitional clauses.
[0.871] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.871] Instantiated 12,292 universal clauses.
[0.871] Instantiated and added clauses for a total of 52,759 clauses.
[0.871] The encoding contains a total of 2,951 distinct variables.
[0.871] Attempting solve with solver <glucose4> ...
c 8 assumptions
[0.874] Executed solver; result: UNSAT.
[0.874] 
[0.874] *************************************
[0.874] * * *   M a k e s p a n     2   * * *
[0.874] *************************************
[0.887] Computed next depth properties: array size of 14.
[1.057] Instantiated 39,259 transitional clauses.
[1.059] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.059] Instantiated 18,863 universal clauses.
[1.059] Instantiated and added clauses for a total of 110,881 clauses.
[1.059] The encoding contains a total of 4,775 distinct variables.
[1.059] Attempting solve with solver <glucose4> ...
c 14 assumptions
[1.425] Executed solver; result: UNSAT.
[1.425] 
[1.425] *************************************
[1.425] * * *   M a k e s p a n     3   * * *
[1.425] *************************************
[1.438] Computed next depth properties: array size of 20.
[1.611] Instantiated 39,661 transitional clauses.
[1.615] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.615] Instantiated 25,434 universal clauses.
[1.615] Instantiated and added clauses for a total of 175,976 clauses.
[1.615] The encoding contains a total of 6,806 distinct variables.
[1.615] Attempting solve with solver <glucose4> ...
c 20 assumptions
[2.416] Executed solver; result: UNSAT.
[2.416] 
[2.416] *************************************
[2.416] * * *   M a k e s p a n     4   * * *
[2.416] *************************************
[2.431] Computed next depth properties: array size of 26.
[2.605] Instantiated 40,063 transitional clauses.
[2.610] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.610] Instantiated 32,005 universal clauses.
[2.610] Instantiated and added clauses for a total of 248,044 clauses.
[2.610] The encoding contains a total of 9,044 distinct variables.
[2.610] Attempting solve with solver <glucose4> ...
c 26 assumptions
c |       33         0      303 |    8901   242359 10397516 |     2     6204      232     3792 |  1.570 % |
[5.057] Executed solver; result: UNSAT.
[5.057] 
[5.057] *************************************
[5.057] * * *   M a k e s p a n     5   * * *
[5.057] *************************************
[5.072] Computed next depth properties: array size of 32.
[5.247] Instantiated 40,465 transitional clauses.
[5.253] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[5.253] Instantiated 38,576 universal clauses.
[5.253] Instantiated and added clauses for a total of 327,085 clauses.
[5.253] The encoding contains a total of 11,489 distinct variables.
[5.253] Attempting solve with solver <glucose4> ...
c 32 assumptions
c |      104        55      192 |   11341   320974 13036679 |     3    11188      496     8769 |  1.280 % |
c |      124        93      241 |   11341   320974 13036679 |     4    13802      520    16155 |  1.280 % |
c |      192       113      208 |   11340   320974 13036679 |     5    13908      534    26048 |  1.288 % |
c |      299       123      167 |   11340   320974 13036679 |     5    23908      550    26048 |  1.288 % |
c |      476       127      126 |   11340   320974 13036679 |     6    21459      560    38497 |  1.288 % |
c |      673       127      104 |   11340   320974 13036679 |     7    16430      562    53526 |  1.288 % |
c |      866       128       92 |   11340   320974 13036679 |     7    26430      564    53526 |  1.288 % |
c |     1061       128       84 |   11340   320974 13036679 |     8    18817      564    71139 |  1.288 % |
[28.933] Executed solver; result: UNSAT.
[28.933] 
[28.933] *************************************
[28.933] * * *   M a k e s p a n     6   * * *
[28.933] *************************************
[28.948] Computed next depth properties: array size of 38.
[29.126] Instantiated 40,867 transitional clauses.
[29.133] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[29.133] Instantiated 45,147 universal clauses.
[29.133] Instantiated and added clauses for a total of 413,099 clauses.
[29.133] The encoding contains a total of 14,141 distinct variables.
[29.133] Attempting solve with solver <glucose4> ...
c 38 assumptions
c |     1186       179       84 |   13989   406660 15691164 |     8    28665      638    71139 |  1.068 % |
c |     1195       218       92 |   13989   406660 15691164 |     8    38665      856    71139 |  1.068 % |
c |     1206       261       99 |   13989   406660 15691164 |     9    28538      892    91266 |  1.068 % |
c |     1218       287      106 |   13989   406660 15691164 |     9    38538      924    91266 |  1.068 % |
c |     1220       307      114 |   13989   406660 15691164 |    10    25784      948   114020 |  1.068 % |
c |     1232       345      121 |   13989   406660 15691164 |    10    35784      962   114020 |  1.068 % |
c |     1252       361      127 |   13989   406660 15691164 |    10    45784      980   114020 |  1.068 % |
c |     1261       390      134 |   13989   406660 15691164 |    11    30403      988   139401 |  1.068 % |
c |     1273       423      141 |   13989   406660 15691164 |    11    40403     1008   139401 |  1.068 % |
c |     1292       436      147 |   13989   406660 15691164 |    11    50403     1028   139401 |  1.068 % |
c |     1314       455      152 |   13989   406660 15691164 |    12    32408     1034   167396 |  1.068 % |
c |     1332       470      157 |   13989   406660 15691164 |    12    42408     1040   167396 |  1.068 % |
c |     1389       489      158 |   13989   406660 15691164 |    12    52408     1050   167396 |  1.068 % |
c |     1472       502      156 |   13988   406611 15691066 |    13    31797     1054   197994 |  1.075 % |
c |     1503       521      159 |   13988   406611 15691066 |    13    41797     1060   197994 |  1.075 % |
c |     1572       533      159 |   13988   406611 15691066 |    13    51797     1068   197994 |  1.075 % |
c |     1728       536      150 |   13988   406611 15691066 |    13    61797     1068   197994 |  1.075 % |
c |     1843       552      146 |   13988   406611 15691066 |    14    38599     1072   231192 |  1.075 % |
c |     1983       558      141 |   13988   406611 15691066 |    14    48599     1124   231192 |  1.075 % |
c |     2176       558      133 |   13988   406611 15691066 |    14    58599     1128   231192 |  1.075 % |
c |     2372       558      126 |   13988   406611 15691066 |    14    68599     1128   231192 |  1.075 % |
c |     2529       567      122 |   13988   406611 15691066 |    15    42804     1132   266987 |  1.075 % |
c |     2713       567      117 |   13988   406611 15691066 |    15    52804     1138   266987 |  1.075 % |
c |     2909       567      113 |   13988   406611 15691066 |    15    62804     1138   266987 |  1.075 % |
c |     3106       567      109 |   13988   406611 15691066 |    15    72804     1138   266987 |  1.075 % |
c |     3279       571      106 |   13988   406611 15691066 |    16    44403     1138   305388 |  1.075 % |
c |     3471       571      103 |   13988   406611 15691066 |    16    54403     1144   305388 |  1.075 % |
c |     3665       571      100 |   13988   406611 15691066 |    16    64403     1144   305388 |  1.075 % |
c |     3862       571       98 |   13988   406611 15691066 |    16    74403     1144   305388 |  1.075 % |
c |     4038       574       96 |   13988   406611 15691066 |    17    43403     1144   346388 |  1.075 % |
c |     4215       575       94 |   13987   406562 15690968 |    17    53327     1152   346388 |  1.082 % |
c |     4410       575       92 |   13986   406562 15690968 |    17    63326     1152   346388 |  1.089 % |
c |     4606       575       91 |   13986   406562 15690968 |    17    73326     1152   346388 |  1.089 % |
c |     4801       575       89 |   13986   406562 15690968 |    17    83326     1152   346388 |  1.089 % |
c |     4963       579       88 |   13986   406562 15690968 |    18    49767     1160   389947 |  1.089 % |
c |     5145       581       87 |   13986   406562 15690968 |    18    59767     1160   389947 |  1.089 % |
c |     5340       581       86 |   13986   406562 15690968 |    18    69767     1160   389947 |  1.089 % |
c |     5535       581       84 |   13986   406562 15690968 |    18    79767     1160   389947 |  1.089 % |
c |     5731       581       83 |   13986   406562 15690968 |    18    89767     1160   389947 |  1.089 % |
c |     5908       581       82 |   13986   406562 15690968 |    19    53586     1160   436128 |  1.089 % |
c |     6095       581       82 |   13986   406562 15690968 |    19    63586     1160   436128 |  1.089 % |
c |     6280       581       81 |   13986   406562 15690968 |    19    73586     1160   436128 |  1.089 % |
c |     6468       581       80 |   13986   406562 15690968 |    19    83586     1160   436128 |  1.089 % |
c |     6661       581       79 |   13986   406562 15690968 |    19    93586     1160   436128 |  1.089 % |
c |     6824       582       79 |   13986   406562 15690968 |    20    54797     1160   484917 |  1.089 % |
c |     6988       582       78 |   13986   406562 15690968 |    20    64797     1160   484917 |  1.089 % |
c |     7126       582       78 |   13986   406562 15690968 |    20    74797     1160   484917 |  1.089 % |
c |     7308       582       77 |   13986   406562 15690968 |    20    84797     1160   484917 |  1.089 % |
Interrupt signal (15) received.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.001] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.013] Processed problem encoding.
[0.013] Calculated possible fact changes of composite elements.
[0.013] Initialized instantiation procedure.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     0   * * *
[0.013] *************************************
[0.014] Instantiated 239 initial clauses.
[0.014] The encoding contains a total of 198 distinct variables.
[0.014] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.014] Executed solver; result: UNSAT.
[0.014] 
[0.014] *************************************
[0.014] * * *   M a k e s p a n     1   * * *
[0.014] *************************************
[0.014] Computed next depth properties: array size of 8.
[0.016] Instantiated 2,260 transitional clauses.
[0.017] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.017] Instantiated 1,828 universal clauses.
[0.017] Instantiated and added clauses for a total of 4,327 clauses.
[0.017] The encoding contains a total of 503 distinct variables.
[0.017] Attempting solve with solver <glucose4> ...
c 8 assumptions
[0.017] Executed solver; result: UNSAT.
[0.017] 
[0.017] *************************************
[0.017] * * *   M a k e s p a n     2   * * *
[0.017] *************************************
[0.017] Computed next depth properties: array size of 14.
[0.020] Instantiated 2,363 transitional clauses.
[0.021] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.021] Instantiated 3,023 universal clauses.
[0.021] Instantiated and added clauses for a total of 9,713 clauses.
[0.021] The encoding contains a total of 887 distinct variables.
[0.021] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.033] Executed solver; result: UNSAT.
[0.033] 
[0.033] *************************************
[0.033] * * *   M a k e s p a n     3   * * *
[0.033] *************************************
[0.033] Computed next depth properties: array size of 20.
[0.036] Instantiated 2,509 transitional clauses.
[0.037] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.037] Instantiated 4,218 universal clauses.
[0.037] Instantiated and added clauses for a total of 16,440 clauses.
[0.037] The encoding contains a total of 1,350 distinct variables.
[0.037] Attempting solve with solver <glucose4> ...
c 20 assumptions
[0.163] Executed solver; result: UNSAT.
[0.163] 
[0.163] *************************************
[0.163] * * *   M a k e s p a n     4   * * *
[0.163] *************************************
[0.164] Computed next depth properties: array size of 26.
[0.166] Instantiated 2,655 transitional clauses.
[0.167] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.167] Instantiated 5,413 universal clauses.
[0.167] Instantiated and added clauses for a total of 24,508 clauses.
[0.167] The encoding contains a total of 1,892 distinct variables.
[0.167] Attempting solve with solver <glucose4> ...
c 26 assumptions
c last restart ## conflicts  :  10 55 
[0.171] Executed solver; result: SAT.
[0.171] Solver returned SAT; a solution has been found at makespan 4.
23
solution 35 1
3 2 4 6 5 35 31 17 4 32 18 35 29 9 4 30 10 35 23 11 4 24 12 
[0.171] Exiting.

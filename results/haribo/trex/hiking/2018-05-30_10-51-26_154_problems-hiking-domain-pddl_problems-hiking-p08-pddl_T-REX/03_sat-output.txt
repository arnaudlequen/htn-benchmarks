Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.001] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.040] Processed problem encoding.
[0.044] Calculated possible fact changes of composite elements.
[0.044] Initialized instantiation procedure.
[0.044] 
[0.044] *************************************
[0.044] * * *   M a k e s p a n     0   * * *
[0.044] *************************************
[0.045] Instantiated 228 initial clauses.
[0.045] The encoding contains a total of 156 distinct variables.
[0.045] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.045] Executed solver; result: UNSAT.
[0.045] 
[0.045] *************************************
[0.045] * * *   M a k e s p a n     1   * * *
[0.045] *************************************
[0.045] Computed next depth properties: array size of 3.
[0.045] Instantiated 28 transitional clauses.
[0.045] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.045] Instantiated 287 universal clauses.
[0.045] Instantiated and added clauses for a total of 543 clauses.
[0.045] The encoding contains a total of 231 distinct variables.
[0.045] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.045] Executed solver; result: UNSAT.
[0.045] 
[0.045] *************************************
[0.045] * * *   M a k e s p a n     2   * * *
[0.045] *************************************
[0.046] Computed next depth properties: array size of 4.
[0.048] Instantiated 4,692 transitional clauses.
[0.061] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.061] Instantiated 261,569 universal clauses.
[0.061] Instantiated and added clauses for a total of 266,804 clauses.
[0.061] The encoding contains a total of 2,127 distinct variables.
[0.061] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.062] Executed solver; result: UNSAT.
[0.062] 
[0.062] *************************************
[0.062] * * *   M a k e s p a n     3   * * *
[0.062] *************************************
[0.063] Computed next depth properties: array size of 14.
[0.071] Instantiated 14,907 transitional clauses.
[0.098] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.098] Instantiated 516,750 universal clauses.
[0.098] Instantiated and added clauses for a total of 798,461 clauses.
[0.098] The encoding contains a total of 5,042 distinct variables.
[0.098] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.098] Executed solver; result: UNSAT.
[0.098] 
[0.098] *************************************
[0.098] * * *   M a k e s p a n     4   * * *
[0.098] *************************************
[0.101] Computed next depth properties: array size of 32.
[0.110] Instantiated 17,670 transitional clauses.
[0.142] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.142] Instantiated 549,068 universal clauses.
[0.142] Instantiated and added clauses for a total of 1,365,199 clauses.
[0.142] The encoding contains a total of 8,060 distinct variables.
[0.142] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.143] Executed solver; result: UNSAT.
[0.143] 
[0.143] *************************************
[0.143] * * *   M a k e s p a n     5   * * *
[0.143] *************************************
[0.146] Computed next depth properties: array size of 36.
[0.155] Instantiated 12,016 transitional clauses.
[0.189] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.189] Instantiated 564,494 universal clauses.
[0.189] Instantiated and added clauses for a total of 1,941,709 clauses.
[0.189] The encoding contains a total of 11,017 distinct variables.
[0.189] Attempting solve with solver <glucose4> ...
c 36 assumptions
[0.190] Executed solver; result: UNSAT.
[0.190] 
[0.190] *************************************
[0.190] * * *   M a k e s p a n     6   * * *
[0.190] *************************************
[0.194] Computed next depth properties: array size of 40.
[0.205] Instantiated 12,448 transitional clauses.
[0.239] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.239] Instantiated 579,920 universal clauses.
[0.239] Instantiated and added clauses for a total of 2,534,077 clauses.
[0.239] The encoding contains a total of 14,194 distinct variables.
[0.239] Attempting solve with solver <glucose4> ...
c 40 assumptions
[0.244] Executed solver; result: UNSAT.
[0.244] 
[0.244] *************************************
[0.244] * * *   M a k e s p a n     7   * * *
[0.244] *************************************
[0.248] Computed next depth properties: array size of 44.
[0.256] Instantiated 12,880 transitional clauses.
[0.294] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.294] Instantiated 595,346 universal clauses.
[0.294] Instantiated and added clauses for a total of 3,142,303 clauses.
[0.294] The encoding contains a total of 17,591 distinct variables.
[0.294] Attempting solve with solver <glucose4> ...
c 44 assumptions
[0.299] Executed solver; result: UNSAT.
[0.299] 
[0.299] *************************************
[0.299] * * *   M a k e s p a n     8   * * *
[0.299] *************************************
[0.304] Computed next depth properties: array size of 48.
[0.313] Instantiated 13,312 transitional clauses.
[0.351] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.351] Instantiated 610,772 universal clauses.
[0.351] Instantiated and added clauses for a total of 3,766,387 clauses.
[0.351] The encoding contains a total of 21,208 distinct variables.
[0.351] Attempting solve with solver <glucose4> ...
c 48 assumptions
c last restart ## conflicts  :  309 189 
[0.373] Executed solver; result: SAT.
[0.373] Solver returned SAT; a solution has been found at makespan 8.
25
solution 1024 1
1024 142 99 140 101 414 413 412 411 141 106 107 99 193 167 188 168 417 416 415 418 189 165 166 167 
[0.373] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.251] Processed problem encoding.
[0.411] Calculated possible fact changes of composite elements.
[0.411] Initialized instantiation procedure.
[0.411] 
[0.411] *************************************
[0.411] * * *   M a k e s p a n     0   * * *
[0.411] *************************************
[0.414] Instantiated 480 initial clauses.
[0.414] The encoding contains a total of 330 distinct variables.
[0.414] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.414] Executed solver; result: UNSAT.
[0.414] 
[0.414] *************************************
[0.414] * * *   M a k e s p a n     1   * * *
[0.414] *************************************
[0.415] Computed next depth properties: array size of 3.
[0.415] Instantiated 82 transitional clauses.
[0.417] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.417] Instantiated 627 universal clauses.
[0.417] Instantiated and added clauses for a total of 1,189 clauses.
[0.417] The encoding contains a total of 491 distinct variables.
[0.417] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.417] Executed solver; result: UNSAT.
[0.417] 
[0.417] *************************************
[0.417] * * *   M a k e s p a n     2   * * *
[0.417] *************************************
[0.421] Computed next depth properties: array size of 4.
[0.436] Instantiated 30,173 transitional clauses.
[0.820] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.820] Instantiated 11,373,792 universal clauses.
[0.820] Instantiated and added clauses for a total of 11,405,154 clauses.
[0.820] The encoding contains a total of 11,648 distinct variables.
[0.820] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.822] Executed solver; result: UNSAT.
[0.822] 
[0.822] *************************************
[0.822] * * *   M a k e s p a n     3   * * *
[0.822] *************************************
[0.833] Computed next depth properties: array size of 14.
[0.902] Instantiated 73,291 transitional clauses.
[1.734] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.734] Instantiated 23,305,376 universal clauses.
[1.734] Instantiated and added clauses for a total of 34,783,821 clauses.
[1.734] The encoding contains a total of 25,708 distinct variables.
[1.734] Attempting solve with solver <glucose4> ...
c 14 assumptions
[1.735] Executed solver; result: UNSAT.
[1.735] 
[1.735] *************************************
[1.735] * * *   M a k e s p a n     4   * * *
[1.735] *************************************
[1.751] Computed next depth properties: array size of 32.
[1.823] Instantiated 75,231 transitional clauses.
[2.766] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.766] Instantiated 24,571,697 universal clauses.
[2.766] Instantiated and added clauses for a total of 59,430,749 clauses.
[2.766] The encoding contains a total of 42,538 distinct variables.
[2.766] Attempting solve with solver <glucose4> ...
c 32 assumptions
[2.767] Executed solver; result: UNSAT.
[2.767] 
[2.767] *************************************
[2.767] * * *   M a k e s p a n     5   * * *
[2.767] *************************************
[2.792] Computed next depth properties: array size of 52.
[2.869] Instantiated 79,356 transitional clauses.
[3.930] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.930] Instantiated 25,827,174 universal clauses.
[3.930] Instantiated and added clauses for a total of 85,337,279 clauses.
[3.930] The encoding contains a total of 61,792 distinct variables.
[3.930] Attempting solve with solver <glucose4> ...
c 52 assumptions
[3.932] Executed solver; result: UNSAT.
[3.932] 
[3.932] *************************************
[3.932] * * *   M a k e s p a n     6   * * *
[3.932] *************************************
[3.962] Computed next depth properties: array size of 74.
[4.051] Instantiated 83,273 transitional clauses.
[5.230] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[5.230] Instantiated 27,070,427 universal clauses.
[5.230] Instantiated and added clauses for a total of 112,490,979 clauses.
[5.230] The encoding contains a total of 83,401 distinct variables.
[5.230] Attempting solve with solver <glucose4> ...
c 74 assumptions
[5.233] Executed solver; result: UNSAT.
[5.233] 
[5.233] *************************************
[5.233] * * *   M a k e s p a n     7   * * *
[5.233] *************************************
[5.269] Computed next depth properties: array size of 98.
[5.363] Instantiated 87,052 transitional clauses.
[6.650] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[6.650] Instantiated 28,302,833 universal clauses.
[6.650] Instantiated and added clauses for a total of 140,880,864 clauses.
[6.650] The encoding contains a total of 107,293 distinct variables.
[6.650] Attempting solve with solver <glucose4> ...
c 98 assumptions
[6.748] Executed solver; result: UNSAT.
[6.748] 
[6.748] *************************************
[6.748] * * *   M a k e s p a n     8   * * *
[6.748] *************************************
[6.791] Computed next depth properties: array size of 124.
[6.889] Instantiated 90,683 transitional clauses.
[8.301] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[8.301] Instantiated 29,526,066 universal clauses.
[8.301] Instantiated and added clauses for a total of 170,497,613 clauses.
[8.301] The encoding contains a total of 133,387 distinct variables.
[8.301] Attempting solve with solver <glucose4> ...
c 124 assumptions
[8.306] Executed solver; result: UNSAT.
[8.306] 
[8.306] *************************************
[8.306] * * *   M a k e s p a n     9   * * *
[8.306] *************************************
[8.360] Computed next depth properties: array size of 152.
[8.470] Instantiated 94,148 transitional clauses.
[9.996] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[9.996] Instantiated 30,741,881 universal clauses.
[9.996] Instantiated and added clauses for a total of 201,333,642 clauses.
[9.996] The encoding contains a total of 161,593 distinct variables.
[9.996] Attempting solve with solver <glucose4> ...
c 152 assumptions
[10.000] Executed solver; result: UNSAT.
[10.000] 
[10.000] *************************************
[10.000] * * *   M a k e s p a n    10   * * *
[10.000] *************************************
[10.060] Computed next depth properties: array size of 182.
[10.174] Instantiated 97,324 transitional clauses.
[11.809] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[11.809] Instantiated 31,951,435 universal clauses.
[11.809] Instantiated and added clauses for a total of 233,382,401 clauses.
[11.809] The encoding contains a total of 191,690 distinct variables.
[11.809] Attempting solve with solver <glucose4> ...
c 182 assumptions
[11.815] Executed solver; result: UNSAT.
[11.815] 
[11.815] *************************************
[11.815] * * *   M a k e s p a n    11   * * *
[11.815] *************************************
[11.882] Computed next depth properties: array size of 198.
[12.005] Instantiated 98,932 transitional clauses.
[13.751] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[13.751] Instantiated 33,155,653 universal clauses.
[13.751] Instantiated and added clauses for a total of 266,636,986 clauses.
[13.751] The encoding contains a total of 223,473 distinct variables.
[13.751] Attempting solve with solver <glucose4> ...
c 198 assumptions
[13.763] Executed solver; result: UNSAT.
[13.763] 
[13.763] *************************************
[13.763] * * *   M a k e s p a n    12   * * *
[13.763] *************************************
[13.834] Computed next depth properties: array size of 214.
[13.956] Instantiated 102,716 transitional clauses.
[15.810] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[15.810] Instantiated 34,359,871 universal clauses.
[15.810] Instantiated and added clauses for a total of 301,099,573 clauses.
[15.810] The encoding contains a total of 257,164 distinct variables.
[15.810] Attempting solve with solver <glucose4> ...
c 214 assumptions
[15.821] Executed solver; result: UNSAT.
[15.821] 
[15.821] *************************************
[15.821] * * *   M a k e s p a n    13   * * *
[15.821] *************************************
[15.896] Computed next depth properties: array size of 230.
[16.029] Instantiated 106,500 transitional clauses.
[17.987] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[17.987] Instantiated 35,564,089 universal clauses.
[17.987] Instantiated and added clauses for a total of 336,770,162 clauses.
[17.987] The encoding contains a total of 292,763 distinct variables.
[17.987] Attempting solve with solver <glucose4> ...
c 230 assumptions
c last restart ## conflicts  :  253 501 
[18.984] Executed solver; result: SAT.
[18.984] Solver returned SAT; a solution has been found at makespan 13.
89
solution 5335 1
5335 556 547 548 549 820 821 819 550 557 558 547 590 581 582 583 823 824 822 584 591 592 581 624 615 616 617 826 825 827 618 625 626 615 661 668 650 669 829 828 830 652 664 665 668 692 683 684 685 832 833 831 686 693 694 683 726 717 718 719 834 836 835 720 727 728 717 763 770 752 771 838 839 837 754 766 767 770 794 785 786 787 840 842 841 788 795 796 785 
[18.991] Exiting.

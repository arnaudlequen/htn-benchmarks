Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.001] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.002] Parsed head comment information.
[0.209] Processed problem encoding.
[0.293] Calculated possible fact changes of composite elements.
[0.293] Initialized instantiation procedure.
[0.293] 
[0.293] *************************************
[0.293] * * *   M a k e s p a n     0   * * *
[0.293] *************************************
[0.295] Instantiated 429 initial clauses.
[0.295] The encoding contains a total of 295 distinct variables.
[0.295] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.295] Executed solver; result: UNSAT.
[0.295] 
[0.295] *************************************
[0.295] * * *   M a k e s p a n     1   * * *
[0.295] *************************************
[0.296] Computed next depth properties: array size of 3.
[0.296] Instantiated 72 transitional clauses.
[0.297] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.297] Instantiated 559 universal clauses.
[0.297] Instantiated and added clauses for a total of 1,060 clauses.
[0.297] The encoding contains a total of 439 distinct variables.
[0.297] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.297] Executed solver; result: UNSAT.
[0.297] 
[0.297] *************************************
[0.297] * * *   M a k e s p a n     2   * * *
[0.297] *************************************
[0.301] Computed next depth properties: array size of 4.
[0.311] Instantiated 23,223 transitional clauses.
[0.541] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.541] Instantiated 6,739,794 universal clauses.
[0.541] Instantiated and added clauses for a total of 6,764,077 clauses.
[0.541] The encoding contains a total of 9,042 distinct variables.
[0.541] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.545] Executed solver; result: UNSAT.
[0.545] 
[0.545] *************************************
[0.545] * * *   M a k e s p a n     3   * * *
[0.545] *************************************
[0.553] Computed next depth properties: array size of 14.
[0.601] Instantiated 56,539 transitional clauses.
[1.095] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.095] Instantiated 13,759,536 universal clauses.
[1.095] Instantiated and added clauses for a total of 20,580,152 clauses.
[1.095] The encoding contains a total of 20,038 distinct variables.
[1.095] Attempting solve with solver <glucose4> ...
c 14 assumptions
[1.095] Executed solver; result: UNSAT.
[1.095] 
[1.095] *************************************
[1.095] * * *   M a k e s p a n     4   * * *
[1.095] *************************************
[1.107] Computed next depth properties: array size of 32.
[1.156] Instantiated 58,842 transitional clauses.
[1.718] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.718] Instantiated 14,517,950 universal clauses.
[1.718] Instantiated and added clauses for a total of 35,156,944 clauses.
[1.718] The encoding contains a total of 33,281 distinct variables.
[1.718] Attempting solve with solver <glucose4> ...
c 32 assumptions
[1.719] Executed solver; result: UNSAT.
[1.719] 
[1.719] *************************************
[1.719] * * *   M a k e s p a n     5   * * *
[1.719] *************************************
[1.737] Computed next depth properties: array size of 52.
[1.792] Instantiated 61,983 transitional clauses.
[2.423] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.423] Instantiated 15,266,600 universal clauses.
[2.423] Instantiated and added clauses for a total of 50,485,527 clauses.
[2.423] The encoding contains a total of 48,447 distinct variables.
[2.423] Attempting solve with solver <glucose4> ...
c 52 assumptions
[2.426] Executed solver; result: UNSAT.
[2.426] 
[2.426] *************************************
[2.426] * * *   M a k e s p a n     6   * * *
[2.426] *************************************
[2.451] Computed next depth properties: array size of 74.
[2.517] Instantiated 64,898 transitional clauses.
[3.212] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.212] Instantiated 16,004,583 universal clauses.
[3.212] Instantiated and added clauses for a total of 66,555,008 clauses.
[3.212] The encoding contains a total of 65,458 distinct variables.
[3.212] Attempting solve with solver <glucose4> ...
c 74 assumptions
[3.214] Executed solver; result: UNSAT.
[3.214] 
[3.214] *************************************
[3.214] * * *   M a k e s p a n     7   * * *
[3.214] *************************************
[3.242] Computed next depth properties: array size of 98.
[3.307] Instantiated 67,657 transitional clauses.
[4.069] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.069] Instantiated 16,733,393 universal clauses.
[4.069] Instantiated and added clauses for a total of 83,356,058 clauses.
[4.069] The encoding contains a total of 84,233 distinct variables.
[4.069] Attempting solve with solver <glucose4> ...
c 98 assumptions
[4.073] Executed solver; result: UNSAT.
[4.073] 
[4.073] *************************************
[4.073] * * *   M a k e s p a n     8   * * *
[4.073] *************************************
[4.108] Computed next depth properties: array size of 124.
[4.188] Instantiated 70,250 transitional clauses.
[5.016] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[5.016] Instantiated 17,454,785 universal clauses.
[5.016] Instantiated and added clauses for a total of 100,881,093 clauses.
[5.016] The encoding contains a total of 104,682 distinct variables.
[5.016] Attempting solve with solver <glucose4> ...
c 124 assumptions
[5.019] Executed solver; result: UNSAT.
[5.019] 
[5.019] *************************************
[5.019] * * *   M a k e s p a n     9   * * *
[5.019] *************************************
[5.060] Computed next depth properties: array size of 152.
[5.136] Instantiated 72,554 transitional clauses.
[6.032] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[6.032] Instantiated 18,169,916 universal clauses.
[6.032] Instantiated and added clauses for a total of 119,123,563 clauses.
[6.032] The encoding contains a total of 126,584 distinct variables.
[6.032] Attempting solve with solver <glucose4> ...
c 152 assumptions
[6.141] Executed solver; result: UNSAT.
[6.141] 
[6.141] *************************************
[6.141] * * *   M a k e s p a n    10   * * *
[6.141] *************************************
[6.186] Computed next depth properties: array size of 166.
[6.271] Instantiated 73,290 transitional clauses.
[7.220] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[7.220] Instantiated 18,879,711 universal clauses.
[7.220] Instantiated and added clauses for a total of 138,076,564 clauses.
[7.220] The encoding contains a total of 149,734 distinct variables.
[7.220] Attempting solve with solver <glucose4> ...
c 166 assumptions
[7.228] Executed solver; result: UNSAT.
[7.228] 
[7.228] *************************************
[7.228] * * *   M a k e s p a n    11   * * *
[7.228] *************************************
[7.277] Computed next depth properties: array size of 180.
[7.359] Instantiated 76,202 transitional clauses.
[8.377] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[8.377] Instantiated 19,589,506 universal clauses.
[8.377] Instantiated and added clauses for a total of 157,742,272 clauses.
[8.377] The encoding contains a total of 174,354 distinct variables.
[8.377] Attempting solve with solver <glucose4> ...
c 180 assumptions
[8.388] Executed solver; result: UNSAT.
[8.388] 
[8.388] *************************************
[8.388] * * *   M a k e s p a n    12   * * *
[8.388] *************************************
[8.441] Computed next depth properties: array size of 194.
[8.531] Instantiated 79,114 transitional clauses.
[9.619] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[9.619] Instantiated 20,299,301 universal clauses.
[9.619] Instantiated and added clauses for a total of 178,120,687 clauses.
[9.619] The encoding contains a total of 200,444 distinct variables.
[9.619] Attempting solve with solver <glucose4> ...
c 194 assumptions
c last restart ## conflicts  :  67 265 
[11.345] Executed solver; result: SAT.
[11.345] Solver returned SAT; a solution has been found at makespan 12.
78
solution 4155 1
4155 491 492 480 493 717 719 718 482 494 495 492 512 513 514 515 722 720 721 516 517 518 513 559 560 548 561 725 723 724 550 562 563 560 580 581 582 583 727 726 728 584 585 586 581 614 615 616 617 729 730 731 618 619 620 615 648 649 650 651 733 732 734 652 653 654 649 682 683 684 685 736 737 735 686 687 688 683 
[11.350] Exiting.

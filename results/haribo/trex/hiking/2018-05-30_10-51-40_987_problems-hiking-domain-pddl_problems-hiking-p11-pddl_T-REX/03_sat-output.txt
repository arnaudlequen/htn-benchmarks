Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.215] Processed problem encoding.
[0.280] Calculated possible fact changes of composite elements.
[0.280] Initialized instantiation procedure.
[0.280] 
[0.280] *************************************
[0.280] * * *   M a k e s p a n     0   * * *
[0.280] *************************************
[0.282] Instantiated 429 initial clauses.
[0.282] The encoding contains a total of 294 distinct variables.
[0.282] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.282] Executed solver; result: UNSAT.
[0.282] 
[0.282] *************************************
[0.282] * * *   M a k e s p a n     1   * * *
[0.282] *************************************
[0.283] Computed next depth properties: array size of 3.
[0.284] Instantiated 67 transitional clauses.
[0.285] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.285] Instantiated 554 universal clauses.
[0.285] Instantiated and added clauses for a total of 1,050 clauses.
[0.285] The encoding contains a total of 435 distinct variables.
[0.285] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.285] Executed solver; result: UNSAT.
[0.285] 
[0.285] *************************************
[0.285] * * *   M a k e s p a n     2   * * *
[0.285] *************************************
[0.289] Computed next depth properties: array size of 4.
[0.299] Instantiated 26,250 transitional clauses.
[0.591] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.592] Instantiated 8,725,487 universal clauses.
[0.592] Instantiated and added clauses for a total of 8,752,787 clauses.
[0.592] The encoding contains a total of 10,050 distinct variables.
[0.592] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.595] Executed solver; result: UNSAT.
[0.595] 
[0.595] *************************************
[0.595] * * *   M a k e s p a n     3   * * *
[0.595] *************************************
[0.605] Computed next depth properties: array size of 14.
[0.657] Instantiated 73,395 transitional clauses.
[1.274] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.274] Instantiated 17,407,992 universal clauses.
[1.274] Instantiated and added clauses for a total of 26,234,174 clauses.
[1.274] The encoding contains a total of 22,649 distinct variables.
[1.274] Attempting solve with solver <glucose4> ...
c 14 assumptions
[1.275] Executed solver; result: UNSAT.
[1.275] 
[1.275] *************************************
[1.275] * * *   M a k e s p a n     4   * * *
[1.275] *************************************
[1.292] Computed next depth properties: array size of 32.
[1.359] Instantiated 80,662 transitional clauses.
[2.171] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.171] Instantiated 18,029,294 universal clauses.
[2.171] Instantiated and added clauses for a total of 44,344,130 clauses.
[2.171] The encoding contains a total of 37,370 distinct variables.
[2.171] Attempting solve with solver <glucose4> ...
c 32 assumptions
[2.173] Executed solver; result: UNSAT.
[2.173] 
[2.173] *************************************
[2.173] * * *   M a k e s p a n     5   * * *
[2.173] *************************************
[2.199] Computed next depth properties: array size of 52.
[2.277] Instantiated 81,886 transitional clauses.
[3.072] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.072] Instantiated 18,619,142 universal clauses.
[3.072] Instantiated and added clauses for a total of 63,045,158 clauses.
[3.072] The encoding contains a total of 53,802 distinct variables.
[3.072] Attempting solve with solver <glucose4> ...
c 52 assumptions
[3.074] Executed solver; result: UNSAT.
[3.074] 
[3.074] *************************************
[3.074] * * *   M a k e s p a n     6   * * *
[3.074] *************************************
[3.100] Computed next depth properties: array size of 74.
[3.168] Instantiated 82,630 transitional clauses.
[3.937] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.938] Instantiated 19,178,976 universal clauses.
[3.938] Instantiated and added clauses for a total of 82,306,764 clauses.
[3.938] The encoding contains a total of 71,767 distinct variables.
[3.938] Attempting solve with solver <glucose4> ...
c 74 assumptions
[3.940] Executed solver; result: UNSAT.
[3.940] 
[3.940] *************************************
[3.940] * * *   M a k e s p a n     7   * * *
[3.940] *************************************
[3.971] Computed next depth properties: array size of 98.
[4.047] Instantiated 82,698 transitional clauses.
[4.870] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.870] Instantiated 19,714,626 universal clauses.
[4.870] Instantiated and added clauses for a total of 102,104,088 clauses.
[4.870] The encoding contains a total of 90,740 distinct variables.
[4.870] Attempting solve with solver <glucose4> ...
c 98 assumptions
[4.874] Executed solver; result: UNSAT.
[4.874] 
[4.874] *************************************
[4.874] * * *   M a k e s p a n     8   * * *
[4.874] *************************************
[4.910] Computed next depth properties: array size of 108.
[4.989] Instantiated 78,956 transitional clauses.
[5.857] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[5.857] Instantiated 20,231,608 universal clauses.
[5.857] Instantiated and added clauses for a total of 122,414,652 clauses.
[5.857] The encoding contains a total of 110,694 distinct variables.
[5.857] Attempting solve with solver <glucose4> ...
c 108 assumptions
[5.865] Executed solver; result: UNSAT.
[5.865] 
[5.865] *************************************
[5.865] * * *   M a k e s p a n     9   * * *
[5.865] *************************************
[5.903] Computed next depth properties: array size of 118.
[5.980] Instantiated 81,476 transitional clauses.
[6.891] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[6.891] Instantiated 20,748,590 universal clauses.
[6.891] Instantiated and added clauses for a total of 143,244,718 clauses.
[6.891] The encoding contains a total of 131,918 distinct variables.
[6.891] Attempting solve with solver <glucose4> ...
c 118 assumptions
[6.994] Executed solver; result: UNSAT.
[6.994] 
[6.994] *************************************
[6.994] * * *   M a k e s p a n    10   * * *
[6.994] *************************************
[7.035] Computed next depth properties: array size of 128.
[7.120] Instantiated 83,996 transitional clauses.
[8.081] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[8.081] Instantiated 21,265,572 universal clauses.
[8.081] Instantiated and added clauses for a total of 164,594,286 clauses.
[8.081] The encoding contains a total of 154,412 distinct variables.
[8.081] Attempting solve with solver <glucose4> ...
c 128 assumptions
[8.105] Executed solver; result: UNSAT.
[8.105] 
[8.105] *************************************
[8.105] * * *   M a k e s p a n    11   * * *
[8.105] *************************************
[8.152] Computed next depth properties: array size of 138.
[8.233] Instantiated 86,516 transitional clauses.
[9.240] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[9.240] Instantiated 21,782,554 universal clauses.
[9.240] Instantiated and added clauses for a total of 186,463,356 clauses.
[9.240] The encoding contains a total of 178,176 distinct variables.
[9.240] Attempting solve with solver <glucose4> ...
c 138 assumptions
c last restart ## conflicts  :  81 689 
[9.619] Executed solver; result: SAT.
[9.619] Solver returned SAT; a solution has been found at makespan 11.
61
solution 4814 1
4814 482 505 484 506 963 964 965 966 486 487 488 505 540 547 532 548 967 969 968 970 534 541 542 547 578 601 580 602 972 974 973 971 582 583 584 601 636 643 628 644 975 977 978 976 630 637 638 643 684 691 676 692 982 981 979 980 678 685 686 691 
[9.622] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.003] Parsed head comment information.
[0.709] Processed problem encoding.
[1.155] Calculated possible fact changes of composite elements.
[1.156] Initialized instantiation procedure.
[1.156] 
[1.156] *************************************
[1.156] * * *   M a k e s p a n     0   * * *
[1.156] *************************************
[1.161] Instantiated 614 initial clauses.
[1.161] The encoding contains a total of 420 distinct variables.
[1.161] Attempting solve with solver <glucose4> ...
c 2 assumptions
[1.161] Executed solver; result: UNSAT.
[1.161] 
[1.161] *************************************
[1.161] * * *   M a k e s p a n     1   * * *
[1.161] *************************************
[1.163] Computed next depth properties: array size of 3.
[1.164] Instantiated 98 transitional clauses.
[1.169] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.169] Instantiated 795 universal clauses.
[1.169] Instantiated and added clauses for a total of 1,507 clauses.
[1.169] The encoding contains a total of 619 distinct variables.
[1.169] Attempting solve with solver <glucose4> ...
c 3 assumptions
[1.169] Executed solver; result: UNSAT.
[1.169] 
[1.169] *************************************
[1.169] * * *   M a k e s p a n     2   * * *
[1.169] *************************************
[1.179] Computed next depth properties: array size of 4.
[1.211] Instantiated 69,401 transitional clauses.
[1.249] 75.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.249] Instantiated 342,034 universal clauses.
[1.249] Instantiated and added clauses for a total of 412,942 clauses.
[1.249] The encoding contains a total of 25,634 distinct variables.
[1.249] Attempting solve with solver <glucose4> ...
c 4 assumptions
[1.252] Executed solver; result: UNSAT.
[1.252] 
[1.252] *************************************
[1.252] * * *   M a k e s p a n     3   * * *
[1.252] *************************************
[1.280] Computed next depth properties: array size of 14.
[1.470] Instantiated 215,879 transitional clauses.
[1.757] 85.7% quadratic At-Most-One encodings, the rest being binary encoded.
[1.757] Instantiated 2,949,437 universal clauses.
[1.757] Instantiated and added clauses for a total of 3,578,258 clauses.
[1.757] The encoding contains a total of 57,610 distinct variables.
[1.757] Attempting solve with solver <glucose4> ...
c 14 assumptions
[1.758] Executed solver; result: UNSAT.
[1.758] 
[1.758] *************************************
[1.758] * * *   M a k e s p a n     4   * * *
[1.758] *************************************
[1.797] Computed next depth properties: array size of 32.
[2.004] Instantiated 235,449 transitional clauses.
[2.574] 93.8% quadratic At-Most-One encodings, the rest being binary encoded.
[2.574] Instantiated 5,802,948 universal clauses.
[2.574] Instantiated and added clauses for a total of 9,616,655 clauses.
[2.574] The encoding contains a total of 93,678 distinct variables.
[2.574] Attempting solve with solver <glucose4> ...
c 32 assumptions
[2.577] Executed solver; result: UNSAT.
[2.577] 
[2.577] *************************************
[2.577] * * *   M a k e s p a n     5   * * *
[2.577] *************************************
[2.634] Computed next depth properties: array size of 52.
[2.852] Instantiated 236,994 transitional clauses.
[3.687] 96.2% quadratic At-Most-One encodings, the rest being binary encoded.
[3.687] Instantiated 8,531,846 universal clauses.
[3.687] Instantiated and added clauses for a total of 18,385,495 clauses.
[3.687] The encoding contains a total of 133,240 distinct variables.
[3.687] Attempting solve with solver <glucose4> ...
c 52 assumptions
[3.771] Executed solver; result: UNSAT.
[3.771] 
[3.771] *************************************
[3.771] * * *   M a k e s p a n     6   * * *
[3.771] *************************************
[3.840] Computed next depth properties: array size of 74.
[4.079] Instantiated 237,807 transitional clauses.
[5.180] 97.3% quadratic At-Most-One encodings, the rest being binary encoded.
[5.180] Instantiated 11,138,981 universal clauses.
[5.180] Instantiated and added clauses for a total of 29,762,283 clauses.
[5.180] The encoding contains a total of 176,027 distinct variables.
[5.180] Attempting solve with solver <glucose4> ...
c 74 assumptions
[5.186] Executed solver; result: UNSAT.
[5.187] 
[5.187] *************************************
[5.187] * * *   M a k e s p a n     7   * * *
[5.187] *************************************
[5.271] Computed next depth properties: array size of 98.
[5.525] Instantiated 238,070 transitional clauses.
[6.877] 98.0% quadratic At-Most-One encodings, the rest being binary encoded.
[6.877] Instantiated 13,649,448 universal clauses.
[6.877] Instantiated and added clauses for a total of 43,649,801 clauses.
[6.877] The encoding contains a total of 221,755 distinct variables.
[6.877] Attempting solve with solver <glucose4> ...
c 98 assumptions
[6.884] Executed solver; result: UNSAT.
[6.884] 
[6.884] *************************************
[6.884] * * *   M a k e s p a n     8   * * *
[6.884] *************************************
[6.980] Computed next depth properties: array size of 124.
[7.239] Instantiated 236,996 transitional clauses.
[8.831] 98.4% quadratic At-Most-One encodings, the rest being binary encoded.
[8.831] Instantiated 16,085,014 universal clauses.
[8.831] Instantiated and added clauses for a total of 59,971,811 clauses.
[8.831] The encoding contains a total of 269,341 distinct variables.
[8.831] Attempting solve with solver <glucose4> ...
c 124 assumptions
[8.840] Executed solver; result: UNSAT.
[8.840] 
[8.840] *************************************
[8.840] * * *   M a k e s p a n     9   * * *
[8.840] *************************************
[8.951] Computed next depth properties: array size of 136.
[9.226] Instantiated 227,986 transitional clauses.
[11.053] 98.5% quadratic At-Most-One encodings, the rest being binary encoded.
[11.053] Instantiated 18,463,544 universal clauses.
[11.053] Instantiated and added clauses for a total of 78,663,341 clauses.
[11.053] The encoding contains a total of 319,292 distinct variables.
[11.053] Attempting solve with solver <glucose4> ...
c 136 assumptions
[11.075] Executed solver; result: UNSAT.
[11.075] 
[11.075] *************************************
[11.075] * * *   M a k e s p a n    10   * * *
[11.075] *************************************
[11.194] Computed next depth properties: array size of 148.
[11.474] Instantiated 233,404 transitional clauses.
[13.572] 98.6% quadratic At-Most-One encodings, the rest being binary encoded.
[13.572] Instantiated 20,842,074 universal clauses.
[13.572] Instantiated and added clauses for a total of 99,738,819 clauses.
[13.572] The encoding contains a total of 371,964 distinct variables.
[13.572] Attempting solve with solver <glucose4> ...
c 148 assumptions
[13.592] Executed solver; result: UNSAT.
[13.592] 
[13.592] *************************************
[13.592] * * *   M a k e s p a n    11   * * *
[13.592] *************************************
[13.718] Computed next depth properties: array size of 160.
[14.014] Instantiated 238,822 transitional clauses.
[16.311] 98.8% quadratic At-Most-One encodings, the rest being binary encoded.
[16.311] Instantiated 23,220,604 universal clauses.
[16.311] Instantiated and added clauses for a total of 123,198,245 clauses.
[16.311] The encoding contains a total of 427,357 distinct variables.
[16.311] Attempting solve with solver <glucose4> ...
c 160 assumptions
[16.335] Executed solver; result: UNSAT.
[16.335] 
[16.335] *************************************
[16.335] * * *   M a k e s p a n    12   * * *
[16.335] *************************************
[16.470] Computed next depth properties: array size of 172.
[16.771] Instantiated 244,240 transitional clauses.
[19.305] 98.8% quadratic At-Most-One encodings, the rest being binary encoded.
[19.305] Instantiated 25,599,134 universal clauses.
[19.305] Instantiated and added clauses for a total of 149,041,619 clauses.
[19.305] The encoding contains a total of 485,471 distinct variables.
[19.305] Attempting solve with solver <glucose4> ...
c 172 assumptions
[19.416] Executed solver; result: UNSAT.
[19.416] 
[19.416] *************************************
[19.416] * * *   M a k e s p a n    13   * * *
[19.416] *************************************
[19.558] Computed next depth properties: array size of 184.
[19.906] Instantiated 249,658 transitional clauses.
[22.709] 98.9% quadratic At-Most-One encodings, the rest being binary encoded.
[22.709] Instantiated 27,977,664 universal clauses.
[22.709] Instantiated and added clauses for a total of 177,268,941 clauses.
[22.709] The encoding contains a total of 546,306 distinct variables.
[22.709] Attempting solve with solver <glucose4> ...
c 184 assumptions
c last restart ## conflicts  :  443 382 
[27.603] Executed solver; result: SAT.
[27.603] Solver returned SAT; a solution has been found at makespan 13.
79
solution 12319 1
12319 61 32 59 33 2046 2047 2044 2045 2043 60 10 11 32 125 96 123 97 2048 2050 2049 2051 2052 124 74 75 96 189 160 187 161 2057 2056 2053 2054 2055 188 138 139 160 253 224 251 225 2061 2060 2059 2062 2058 252 202 203 224 317 288 315 289 2065 2064 2066 2067 2063 316 266 267 288 384 346 379 347 2069 2068 2071 2072 2070 380 339 340 346 
[27.614] Exiting.

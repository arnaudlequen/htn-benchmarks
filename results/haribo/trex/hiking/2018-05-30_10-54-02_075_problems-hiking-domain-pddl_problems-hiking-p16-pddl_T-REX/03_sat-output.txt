Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.162] Processed problem encoding.
[0.184] Calculated possible fact changes of composite elements.
[0.184] Initialized instantiation procedure.
[0.184] 
[0.184] *************************************
[0.184] * * *   M a k e s p a n     0   * * *
[0.184] *************************************
[0.185] Instantiated 365 initial clauses.
[0.185] The encoding contains a total of 249 distinct variables.
[0.185] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.185] Executed solver; result: UNSAT.
[0.185] 
[0.185] *************************************
[0.185] * * *   M a k e s p a n     1   * * *
[0.185] *************************************
[0.186] Computed next depth properties: array size of 3.
[0.186] Instantiated 50 transitional clauses.
[0.187] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.187] Instantiated 465 universal clauses.
[0.187] Instantiated and added clauses for a total of 880 clauses.
[0.187] The encoding contains a total of 367 distinct variables.
[0.187] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.187] Executed solver; result: UNSAT.
[0.187] 
[0.187] *************************************
[0.187] * * *   M a k e s p a n     2   * * *
[0.187] *************************************
[0.190] Computed next depth properties: array size of 4.
[0.197] Instantiated 18,335 transitional clauses.
[0.345] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.345] Instantiated 4,113,512 universal clauses.
[0.345] Instantiated and added clauses for a total of 4,132,727 clauses.
[0.345] The encoding contains a total of 7,320 distinct variables.
[0.345] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.346] Executed solver; result: UNSAT.
[0.346] 
[0.346] *************************************
[0.346] * * *   M a k e s p a n     3   * * *
[0.346] *************************************
[0.353] Computed next depth properties: array size of 14.
[0.385] Instantiated 60,749 transitional clauses.
[0.676] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.676] Instantiated 8,097,342 universal clauses.
[0.676] Instantiated and added clauses for a total of 12,290,818 clauses.
[0.676] The encoding contains a total of 17,146 distinct variables.
[0.676] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.677] Executed solver; result: UNSAT.
[0.677] 
[0.677] *************************************
[0.677] * * *   M a k e s p a n     4   * * *
[0.677] *************************************
[0.687] Computed next depth properties: array size of 32.
[0.727] Instantiated 72,054 transitional clauses.
[1.046] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.046] Instantiated 8,375,200 universal clauses.
[1.046] Instantiated and added clauses for a total of 20,738,072 clauses.
[1.046] The encoding contains a total of 28,127 distinct variables.
[1.046] Attempting solve with solver <glucose4> ...
c 32 assumptions
[1.047] Executed solver; result: UNSAT.
[1.047] 
[1.047] *************************************
[1.047] * * *   M a k e s p a n     5   * * *
[1.047] *************************************
[1.061] Computed next depth properties: array size of 52.
[1.101] Instantiated 67,214 transitional clauses.
[1.436] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.436] Instantiated 8,587,477 universal clauses.
[1.436] Instantiated and added clauses for a total of 29,392,763 clauses.
[1.436] The encoding contains a total of 38,963 distinct variables.
[1.436] Attempting solve with solver <glucose4> ...
c 52 assumptions
[1.439] Executed solver; result: UNSAT.
[1.439] 
[1.439] *************************************
[1.439] * * *   M a k e s p a n     6   * * *
[1.439] *************************************
[1.454] Computed next depth properties: array size of 58.
[1.491] Instantiated 54,198 transitional clauses.
[1.836] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.836] Instantiated 8,742,868 universal clauses.
[1.836] Instantiated and added clauses for a total of 38,189,829 clauses.
[1.836] The encoding contains a total of 50,151 distinct variables.
[1.836] Attempting solve with solver <glucose4> ...
c 58 assumptions
[1.840] Executed solver; result: UNSAT.
[1.840] 
[1.840] *************************************
[1.840] * * *   M a k e s p a n     7   * * *
[1.840] *************************************
[1.858] Computed next depth properties: array size of 64.
[1.901] Instantiated 55,602 transitional clauses.
[2.259] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.259] Instantiated 8,898,259 universal clauses.
[2.259] Instantiated and added clauses for a total of 47,143,690 clauses.
[2.259] The encoding contains a total of 62,047 distinct variables.
[2.259] Attempting solve with solver <glucose4> ...
c 64 assumptions
[2.263] Executed solver; result: UNSAT.
[2.263] 
[2.263] *************************************
[2.263] * * *   M a k e s p a n     8   * * *
[2.263] *************************************
[2.281] Computed next depth properties: array size of 70.
[2.320] Instantiated 57,006 transitional clauses.
[2.697] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.697] Instantiated 9,053,650 universal clauses.
[2.697] Instantiated and added clauses for a total of 56,254,346 clauses.
[2.697] The encoding contains a total of 74,651 distinct variables.
[2.697] Attempting solve with solver <glucose4> ...
c 70 assumptions
[2.740] Executed solver; result: UNSAT.
[2.740] 
[2.740] *************************************
[2.740] * * *   M a k e s p a n     9   * * *
[2.740] *************************************
[2.761] Computed next depth properties: array size of 76.
[2.801] Instantiated 58,410 transitional clauses.
[3.190] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.190] Instantiated 9,209,041 universal clauses.
[3.190] Instantiated and added clauses for a total of 65,521,797 clauses.
[3.190] The encoding contains a total of 87,963 distinct variables.
[3.190] Attempting solve with solver <glucose4> ...
c 76 assumptions
[3.251] Executed solver; result: UNSAT.
[3.251] 
[3.251] *************************************
[3.251] * * *   M a k e s p a n    10   * * *
[3.251] *************************************
[3.273] Computed next depth properties: array size of 82.
[3.320] Instantiated 59,814 transitional clauses.
[3.720] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.720] Instantiated 9,364,432 universal clauses.
[3.720] Instantiated and added clauses for a total of 74,946,043 clauses.
[3.720] The encoding contains a total of 101,983 distinct variables.
[3.720] Attempting solve with solver <glucose4> ...
c 82 assumptions
c last restart ## conflicts  :  203 250 
[4.533] Executed solver; result: SAT.
[4.533] Solver returned SAT; a solution has been found at makespan 10.
40
solution 3547 1
3547 814 798 811 799 997 996 993 995 994 812 781 782 798 879 860 875 861 999 1001 998 1000 1002 876 848 849 860 942 926 939 927 1005 1004 1003 1007 1006 940 909 910 926 
[4.535] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.001] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.067] Processed problem encoding.
[0.075] Calculated possible fact changes of composite elements.
[0.075] Initialized instantiation procedure.
[0.075] 
[0.075] *************************************
[0.075] * * *   M a k e s p a n     0   * * *
[0.075] *************************************
[0.076] Instantiated 276 initial clauses.
[0.076] The encoding contains a total of 190 distinct variables.
[0.076] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.076] Executed solver; result: UNSAT.
[0.076] 
[0.076] *************************************
[0.076] * * *   M a k e s p a n     1   * * *
[0.076] *************************************
[0.076] Computed next depth properties: array size of 3.
[0.076] Instantiated 42 transitional clauses.
[0.077] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.077] Instantiated 355 universal clauses.
[0.077] Instantiated and added clauses for a total of 673 clauses.
[0.077] The encoding contains a total of 283 distinct variables.
[0.077] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.077] Executed solver; result: UNSAT.
[0.077] 
[0.077] *************************************
[0.077] * * *   M a k e s p a n     2   * * *
[0.077] *************************************
[0.078] Computed next depth properties: array size of 4.
[0.082] Instantiated 7,845 transitional clauses.
[0.116] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.116] Instantiated 770,832 universal clauses.
[0.116] Instantiated and added clauses for a total of 779,350 clauses.
[0.116] The encoding contains a total of 3,240 distinct variables.
[0.116] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.117] Executed solver; result: UNSAT.
[0.117] 
[0.117] *************************************
[0.117] * * *   M a k e s p a n     3   * * *
[0.117] *************************************
[0.120] Computed next depth properties: array size of 14.
[0.133] Instantiated 19,531 transitional clauses.
[0.207] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.207] Instantiated 1,545,576 universal clauses.
[0.207] Instantiated and added clauses for a total of 2,344,457 clauses.
[0.207] The encoding contains a total of 7,348 distinct variables.
[0.207] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.207] Executed solver; result: UNSAT.
[0.207] 
[0.207] *************************************
[0.207] * * *   M a k e s p a n     4   * * *
[0.207] *************************************
[0.212] Computed next depth properties: array size of 32.
[0.227] Instantiated 21,771 transitional clauses.
[0.312] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.312] Instantiated 1,642,505 universal clauses.
[0.312] Instantiated and added clauses for a total of 4,008,733 clauses.
[0.312] The encoding contains a total of 12,422 distinct variables.
[0.312] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.313] Executed solver; result: UNSAT.
[0.313] 
[0.313] *************************************
[0.313] * * *   M a k e s p a n     5   * * *
[0.313] *************************************
[0.320] Computed next depth properties: array size of 52.
[0.336] Instantiated 22,536 transitional clauses.
[0.433] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.434] Instantiated 1,733,396 universal clauses.
[0.434] Instantiated and added clauses for a total of 5,764,665 clauses.
[0.434] The encoding contains a total of 18,204 distinct variables.
[0.434] Attempting solve with solver <glucose4> ...
c 52 assumptions
[0.435] Executed solver; result: UNSAT.
[0.435] 
[0.435] *************************************
[0.435] * * *   M a k e s p a n     6   * * *
[0.435] *************************************
[0.444] Computed next depth properties: array size of 74.
[0.463] Instantiated 22,916 transitional clauses.
[0.570] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.570] Instantiated 1,818,098 universal clauses.
[0.570] Instantiated and added clauses for a total of 7,605,679 clauses.
[0.570] The encoding contains a total of 24,467 distinct variables.
[0.570] Attempting solve with solver <glucose4> ...
c 74 assumptions
[0.571] Executed solver; result: UNSAT.
[0.571] 
[0.571] *************************************
[0.571] * * *   M a k e s p a n     7   * * *
[0.571] *************************************
[0.582] Computed next depth properties: array size of 82.
[0.602] Instantiated 21,720 transitional clauses.
[0.719] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.719] Instantiated 1,897,464 universal clauses.
[0.719] Instantiated and added clauses for a total of 9,524,863 clauses.
[0.719] The encoding contains a total of 31,006 distinct variables.
[0.719] Attempting solve with solver <glucose4> ...
c 82 assumptions
[0.729] Executed solver; result: UNSAT.
[0.729] 
[0.729] *************************************
[0.729] * * *   M a k e s p a n     8   * * *
[0.729] *************************************
[0.741] Computed next depth properties: array size of 90.
[0.761] Instantiated 22,700 transitional clauses.
[0.887] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.887] Instantiated 1,976,830 universal clauses.
[0.887] Instantiated and added clauses for a total of 11,524,393 clauses.
[0.887] The encoding contains a total of 38,043 distinct variables.
[0.887] Attempting solve with solver <glucose4> ...
c 90 assumptions
[0.890] Executed solver; result: UNSAT.
[0.890] 
[0.890] *************************************
[0.890] * * *   M a k e s p a n     9   * * *
[0.890] *************************************
[0.902] Computed next depth properties: array size of 98.
[0.927] Instantiated 23,680 transitional clauses.
[1.059] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.059] Instantiated 2,056,196 universal clauses.
[1.059] Instantiated and added clauses for a total of 13,604,269 clauses.
[1.059] The encoding contains a total of 45,578 distinct variables.
[1.059] Attempting solve with solver <glucose4> ...
c 98 assumptions
c last restart ## conflicts  :  36 218 
[1.214] Executed solver; result: SAT.
[1.214] Solver returned SAT; a solution has been found at makespan 9.
45
solution 1537 1
1537 151 156 140 157 412 413 411 142 154 155 156 179 173 174 175 415 416 414 176 180 181 173 213 207 208 209 418 419 417 210 214 215 207 253 258 242 259 421 422 420 244 256 257 258 
[1.215] Exiting.

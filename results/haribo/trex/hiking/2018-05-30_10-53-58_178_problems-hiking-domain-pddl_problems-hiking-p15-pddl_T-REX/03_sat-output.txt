Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.069] Processed problem encoding.
[0.076] Calculated possible fact changes of composite elements.
[0.076] Initialized instantiation procedure.
[0.076] 
[0.076] *************************************
[0.076] * * *   M a k e s p a n     0   * * *
[0.076] *************************************
[0.076] Instantiated 282 initial clauses.
[0.076] The encoding contains a total of 192 distinct variables.
[0.076] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.076] Executed solver; result: UNSAT.
[0.076] 
[0.076] *************************************
[0.076] * * *   M a k e s p a n     1   * * *
[0.076] *************************************
[0.076] Computed next depth properties: array size of 3.
[0.077] Instantiated 34 transitional clauses.
[0.077] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.077] Instantiated 355 universal clauses.
[0.077] Instantiated and added clauses for a total of 671 clauses.
[0.077] The encoding contains a total of 283 distinct variables.
[0.077] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.077] Executed solver; result: UNSAT.
[0.077] 
[0.077] *************************************
[0.077] * * *   M a k e s p a n     2   * * *
[0.077] *************************************
[0.078] Computed next depth properties: array size of 4.
[0.082] Instantiated 8,753 transitional clauses.
[0.114] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.114] Instantiated 871,146 universal clauses.
[0.114] Instantiated and added clauses for a total of 880,570 clauses.
[0.114] The encoding contains a total of 3,860 distinct variables.
[0.114] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.114] Executed solver; result: UNSAT.
[0.114] 
[0.114] *************************************
[0.114] * * *   M a k e s p a n     3   * * *
[0.114] *************************************
[0.118] Computed next depth properties: array size of 14.
[0.131] Instantiated 31,599 transitional clauses.
[0.199] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.199] Instantiated 1,718,798 universal clauses.
[0.199] Instantiated and added clauses for a total of 2,630,967 clauses.
[0.199] The encoding contains a total of 9,192 distinct variables.
[0.199] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.199] Executed solver; result: UNSAT.
[0.199] 
[0.199] *************************************
[0.199] * * *   M a k e s p a n     4   * * *
[0.199] *************************************
[0.204] Computed next depth properties: array size of 32.
[0.220] Instantiated 37,484 transitional clauses.
[0.296] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.296] Instantiated 1,804,092 universal clauses.
[0.296] Instantiated and added clauses for a total of 4,472,543 clauses.
[0.296] The encoding contains a total of 14,166 distinct variables.
[0.296] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.298] Executed solver; result: UNSAT.
[0.298] 
[0.298] *************************************
[0.298] * * *   M a k e s p a n     5   * * *
[0.298] *************************************
[0.303] Computed next depth properties: array size of 36.
[0.316] Instantiated 23,958 transitional clauses.
[0.395] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.395] Instantiated 1,837,170 universal clauses.
[0.395] Instantiated and added clauses for a total of 6,333,671 clauses.
[0.395] The encoding contains a total of 19,121 distinct variables.
[0.395] Attempting solve with solver <glucose4> ...
c 36 assumptions
[0.396] Executed solver; result: UNSAT.
[0.396] 
[0.396] *************************************
[0.396] * * *   M a k e s p a n     6   * * *
[0.396] *************************************
[0.402] Computed next depth properties: array size of 40.
[0.415] Instantiated 24,604 transitional clauses.
[0.497] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.497] Instantiated 1,870,248 universal clauses.
[0.497] Instantiated and added clauses for a total of 8,228,523 clauses.
[0.497] The encoding contains a total of 24,403 distinct variables.
[0.497] Attempting solve with solver <glucose4> ...
c 40 assumptions
[0.502] Executed solver; result: UNSAT.
[0.502] 
[0.502] *************************************
[0.502] * * *   M a k e s p a n     7   * * *
[0.502] *************************************
[0.509] Computed next depth properties: array size of 44.
[0.524] Instantiated 25,250 transitional clauses.
[0.609] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.609] Instantiated 1,903,326 universal clauses.
[0.609] Instantiated and added clauses for a total of 10,157,099 clauses.
[0.609] The encoding contains a total of 30,012 distinct variables.
[0.609] Attempting solve with solver <glucose4> ...
c 44 assumptions
[0.621] Executed solver; result: UNSAT.
[0.621] 
[0.621] *************************************
[0.621] * * *   M a k e s p a n     8   * * *
[0.621] *************************************
[0.628] Computed next depth properties: array size of 48.
[0.643] Instantiated 25,896 transitional clauses.
[0.731] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.731] Instantiated 1,936,404 universal clauses.
[0.731] Instantiated and added clauses for a total of 12,119,399 clauses.
[0.731] The encoding contains a total of 35,948 distinct variables.
[0.731] Attempting solve with solver <glucose4> ...
c 48 assumptions
[0.757] Executed solver; result: UNSAT.
[0.757] 
[0.757] *************************************
[0.757] * * *   M a k e s p a n     9   * * *
[0.757] *************************************
[0.765] Computed next depth properties: array size of 52.
[0.782] Instantiated 26,542 transitional clauses.
[0.874] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.874] Instantiated 1,969,482 universal clauses.
[0.874] Instantiated and added clauses for a total of 14,115,423 clauses.
[0.874] The encoding contains a total of 42,211 distinct variables.
[0.874] Attempting solve with solver <glucose4> ...
c 52 assumptions
c last restart ## conflicts  :  914 160 
[0.928] Executed solver; result: SAT.
[0.928] Solver returned SAT; a solution has been found at makespan 9.
27
solution 1812 1
1812 294 282 291 283 655 654 657 656 653 292 269 270 282 358 346 355 347 658 660 662 661 659 356 333 334 346 
[0.928] Exiting.

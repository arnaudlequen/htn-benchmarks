Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.003] Parsed head comment information.
[0.600] Processed problem encoding.
[0.924] Calculated possible fact changes of composite elements.
[0.925] Initialized instantiation procedure.
[0.925] 
[0.925] *************************************
[0.925] * * *   M a k e s p a n     0   * * *
[0.925] *************************************
[0.932] Instantiated 614 initial clauses.
[0.932] The encoding contains a total of 420 distinct variables.
[0.932] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.932] Executed solver; result: UNSAT.
[0.932] 
[0.932] *************************************
[0.932] * * *   M a k e s p a n     1   * * *
[0.932] *************************************
[0.933] Computed next depth properties: array size of 3.
[0.934] Instantiated 98 transitional clauses.
[0.938] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.938] Instantiated 795 universal clauses.
[0.938] Instantiated and added clauses for a total of 1,507 clauses.
[0.938] The encoding contains a total of 619 distinct variables.
[0.938] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.938] Executed solver; result: UNSAT.
[0.938] 
[0.938] *************************************
[0.938] * * *   M a k e s p a n     2   * * *
[0.938] *************************************
[0.945] Computed next depth properties: array size of 4.
[0.968] Instantiated 69,401 transitional clauses.
[0.995] 75.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.995] Instantiated 342,034 universal clauses.
[0.995] Instantiated and added clauses for a total of 412,942 clauses.
[0.995] The encoding contains a total of 25,634 distinct variables.
[0.995] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.997] Executed solver; result: UNSAT.
[0.997] 
[0.997] *************************************
[0.997] * * *   M a k e s p a n     3   * * *
[0.997] *************************************
[1.017] Computed next depth properties: array size of 14.
[1.155] Instantiated 215,879 transitional clauses.
[1.350] 85.7% quadratic At-Most-One encodings, the rest being binary encoded.
[1.350] Instantiated 2,949,437 universal clauses.
[1.350] Instantiated and added clauses for a total of 3,578,258 clauses.
[1.350] The encoding contains a total of 57,610 distinct variables.
[1.350] Attempting solve with solver <glucose4> ...
c 14 assumptions
[1.351] Executed solver; result: UNSAT.
[1.351] 
[1.351] *************************************
[1.351] * * *   M a k e s p a n     4   * * *
[1.351] *************************************
[1.379] Computed next depth properties: array size of 32.
[1.532] Instantiated 235,449 transitional clauses.
[1.916] 93.8% quadratic At-Most-One encodings, the rest being binary encoded.
[1.916] Instantiated 5,802,948 universal clauses.
[1.916] Instantiated and added clauses for a total of 9,616,655 clauses.
[1.916] The encoding contains a total of 93,678 distinct variables.
[1.917] Attempting solve with solver <glucose4> ...
c 32 assumptions
[1.919] Executed solver; result: UNSAT.
[1.919] 
[1.919] *************************************
[1.919] * * *   M a k e s p a n     5   * * *
[1.919] *************************************
[1.955] Computed next depth properties: array size of 52.
[2.116] Instantiated 236,994 transitional clauses.
[2.702] 96.2% quadratic At-Most-One encodings, the rest being binary encoded.
[2.702] Instantiated 8,531,846 universal clauses.
[2.702] Instantiated and added clauses for a total of 18,385,495 clauses.
[2.702] The encoding contains a total of 133,240 distinct variables.
[2.702] Attempting solve with solver <glucose4> ...
c 52 assumptions
[2.750] Executed solver; result: UNSAT.
[2.750] 
[2.750] *************************************
[2.750] * * *   M a k e s p a n     6   * * *
[2.750] *************************************
[2.797] Computed next depth properties: array size of 74.
[2.971] Instantiated 237,807 transitional clauses.
[3.714] 97.3% quadratic At-Most-One encodings, the rest being binary encoded.
[3.714] Instantiated 11,138,981 universal clauses.
[3.714] Instantiated and added clauses for a total of 29,762,283 clauses.
[3.714] The encoding contains a total of 176,027 distinct variables.
[3.714] Attempting solve with solver <glucose4> ...
c 74 assumptions
[3.719] Executed solver; result: UNSAT.
[3.719] 
[3.719] *************************************
[3.719] * * *   M a k e s p a n     7   * * *
[3.719] *************************************
[3.774] Computed next depth properties: array size of 98.
[3.962] Instantiated 238,070 transitional clauses.
[4.875] 98.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.875] Instantiated 13,649,448 universal clauses.
[4.875] Instantiated and added clauses for a total of 43,649,801 clauses.
[4.875] The encoding contains a total of 221,755 distinct variables.
[4.875] Attempting solve with solver <glucose4> ...
c 98 assumptions
[4.880] Executed solver; result: UNSAT.
[4.880] 
[4.880] *************************************
[4.880] * * *   M a k e s p a n     8   * * *
[4.880] *************************************
[4.944] Computed next depth properties: array size of 124.
[5.140] Instantiated 236,996 transitional clauses.
[6.250] 98.4% quadratic At-Most-One encodings, the rest being binary encoded.
[6.250] Instantiated 16,085,014 universal clauses.
[6.250] Instantiated and added clauses for a total of 59,971,811 clauses.
[6.250] The encoding contains a total of 269,341 distinct variables.
[6.250] Attempting solve with solver <glucose4> ...
c 124 assumptions
[6.256] Executed solver; result: UNSAT.
[6.256] 
[6.256] *************************************
[6.256] * * *   M a k e s p a n     9   * * *
[6.256] *************************************
[6.329] Computed next depth properties: array size of 136.
[6.536] Instantiated 227,986 transitional clauses.
[7.810] 98.5% quadratic At-Most-One encodings, the rest being binary encoded.
[7.810] Instantiated 18,463,544 universal clauses.
[7.810] Instantiated and added clauses for a total of 78,663,341 clauses.
[7.810] The encoding contains a total of 319,292 distinct variables.
[7.810] Attempting solve with solver <glucose4> ...
c 136 assumptions
[7.826] Executed solver; result: UNSAT.
[7.826] 
[7.826] *************************************
[7.826] * * *   M a k e s p a n    10   * * *
[7.826] *************************************
[7.904] Computed next depth properties: array size of 148.
[8.115] Instantiated 233,404 transitional clauses.
[9.515] 98.6% quadratic At-Most-One encodings, the rest being binary encoded.
[9.515] Instantiated 20,842,074 universal clauses.
[9.515] Instantiated and added clauses for a total of 99,738,819 clauses.
[9.515] The encoding contains a total of 371,964 distinct variables.
[9.515] Attempting solve with solver <glucose4> ...
c 148 assumptions
[9.529] Executed solver; result: UNSAT.
[9.529] 
[9.529] *************************************
[9.529] * * *   M a k e s p a n    11   * * *
[9.529] *************************************
[9.612] Computed next depth properties: array size of 160.
[9.835] Instantiated 238,822 transitional clauses.
[11.440] 98.8% quadratic At-Most-One encodings, the rest being binary encoded.
[11.440] Instantiated 23,220,604 universal clauses.
[11.440] Instantiated and added clauses for a total of 123,198,245 clauses.
[11.440] The encoding contains a total of 427,357 distinct variables.
[11.440] Attempting solve with solver <glucose4> ...
c 160 assumptions
[11.456] Executed solver; result: UNSAT.
[11.456] 
[11.456] *************************************
[11.456] * * *   M a k e s p a n    12   * * *
[11.456] *************************************
[11.543] Computed next depth properties: array size of 172.
[11.771] Instantiated 244,240 transitional clauses.
[13.485] 98.8% quadratic At-Most-One encodings, the rest being binary encoded.
[13.485] Instantiated 25,599,134 universal clauses.
[13.485] Instantiated and added clauses for a total of 149,041,619 clauses.
[13.485] The encoding contains a total of 485,471 distinct variables.
[13.485] Attempting solve with solver <glucose4> ...
c 172 assumptions
[13.551] Executed solver; result: UNSAT.
[13.551] 
[13.551] *************************************
[13.551] * * *   M a k e s p a n    13   * * *
[13.551] *************************************
[13.644] Computed next depth properties: array size of 184.
[13.879] Instantiated 249,658 transitional clauses.
[15.772] 98.9% quadratic At-Most-One encodings, the rest being binary encoded.
[15.772] Instantiated 27,977,664 universal clauses.
[15.772] Instantiated and added clauses for a total of 177,268,941 clauses.
[15.772] The encoding contains a total of 546,306 distinct variables.
[15.772] Attempting solve with solver <glucose4> ...
c 184 assumptions
c last restart ## conflicts  :  443 382 
[19.013] Executed solver; result: SAT.
[19.013] Solver returned SAT; a solution has been found at makespan 13.
79
solution 12319 1
12319 61 32 59 33 2046 2047 2044 2045 2043 60 10 11 32 125 96 123 97 2048 2050 2049 2051 2052 124 74 75 96 189 160 187 161 2057 2056 2053 2054 2055 188 138 139 160 253 224 251 225 2061 2060 2059 2062 2058 252 202 203 224 317 288 315 289 2065 2064 2066 2067 2063 316 266 267 288 384 346 379 347 2069 2068 2071 2072 2070 380 339 340 346 
[19.020] Exiting.

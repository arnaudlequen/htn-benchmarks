Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.022] Processed problem encoding.
[0.023] Calculated possible fact changes of composite elements.
[0.023] Initialized instantiation procedure.
[0.023] 
[0.023] *************************************
[0.023] * * *   M a k e s p a n     0   * * *
[0.023] *************************************
[0.023] Instantiated 174 initial clauses.
[0.023] The encoding contains a total of 120 distinct variables.
[0.023] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.024] Executed solver; result: UNSAT.
[0.024] 
[0.024] *************************************
[0.024] * * *   M a k e s p a n     1   * * *
[0.024] *************************************
[0.024] Computed next depth properties: array size of 3.
[0.024] Instantiated 22 transitional clauses.
[0.024] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.024] Instantiated 219 universal clauses.
[0.024] Instantiated and added clauses for a total of 415 clauses.
[0.024] The encoding contains a total of 179 distinct variables.
[0.024] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.024] Executed solver; result: UNSAT.
[0.024] 
[0.024] *************************************
[0.024] * * *   M a k e s p a n     2   * * *
[0.024] *************************************
[0.025] Computed next depth properties: array size of 4.
[0.026] Instantiated 2,153 transitional clauses.
[0.030] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.030] Instantiated 58,704 universal clauses.
[0.030] Instantiated and added clauses for a total of 61,272 clauses.
[0.030] The encoding contains a total of 1,052 distinct variables.
[0.030] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.030] Executed solver; result: UNSAT.
[0.030] 
[0.030] *************************************
[0.030] * * *   M a k e s p a n     3   * * *
[0.030] *************************************
[0.031] Computed next depth properties: array size of 14.
[0.035] Instantiated 5,899 transitional clauses.
[0.044] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.044] Instantiated 116,756 universal clauses.
[0.044] Instantiated and added clauses for a total of 183,927 clauses.
[0.044] The encoding contains a total of 2,488 distinct variables.
[0.044] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.044] Executed solver; result: UNSAT.
[0.044] 
[0.044] *************************************
[0.044] * * *   M a k e s p a n     4   * * *
[0.044] *************************************
[0.046] Computed next depth properties: array size of 32.
[0.050] Instantiated 7,032 transitional clauses.
[0.060] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.060] Instantiated 127,666 universal clauses.
[0.060] Instantiated and added clauses for a total of 318,625 clauses.
[0.060] The encoding contains a total of 4,154 distinct variables.
[0.060] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.061] Executed solver; result: UNSAT.
[0.061] 
[0.061] *************************************
[0.061] * * *   M a k e s p a n     5   * * *
[0.061] *************************************
[0.062] Computed next depth properties: array size of 36.
[0.066] Instantiated 5,230 transitional clauses.
[0.077] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.077] Instantiated 133,756 universal clauses.
[0.077] Instantiated and added clauses for a total of 457,611 clauses.
[0.077] The encoding contains a total of 5,739 distinct variables.
[0.077] Attempting solve with solver <glucose4> ...
c 36 assumptions
[0.077] Executed solver; result: UNSAT.
[0.077] 
[0.077] *************************************
[0.077] * * *   M a k e s p a n     6   * * *
[0.077] *************************************
[0.079] Computed next depth properties: array size of 40.
[0.082] Instantiated 5,492 transitional clauses.
[0.093] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.093] Instantiated 139,846 universal clauses.
[0.093] Instantiated and added clauses for a total of 602,949 clauses.
[0.093] The encoding contains a total of 7,459 distinct variables.
[0.093] Attempting solve with solver <glucose4> ...
c 40 assumptions
[0.096] Executed solver; result: UNSAT.
[0.096] 
[0.096] *************************************
[0.096] * * *   M a k e s p a n     7   * * *
[0.096] *************************************
[0.097] Computed next depth properties: array size of 44.
[0.101] Instantiated 5,754 transitional clauses.
[0.112] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.112] Instantiated 145,936 universal clauses.
[0.112] Instantiated and added clauses for a total of 754,639 clauses.
[0.112] The encoding contains a total of 9,314 distinct variables.
[0.112] Attempting solve with solver <glucose4> ...
c 44 assumptions
c last restart ## conflicts  :  30 120 
[0.115] Executed solver; result: SAT.
[0.115] Solver returned SAT; a solution has been found at makespan 7.
22
solution 218 1
92 71 93 73 214 215 213 94 75 76 71 126 105 127 107 217 216 218 128 109 110 105 
[0.116] Exiting.

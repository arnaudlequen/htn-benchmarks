Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.003] Parsed head comment information.
[0.394] Processed problem encoding.
[0.575] Calculated possible fact changes of composite elements.
[0.575] Initialized instantiation procedure.
[0.575] 
[0.575] *************************************
[0.575] * * *   M a k e s p a n     0   * * *
[0.575] *************************************
[0.577] Instantiated 563 initial clauses.
[0.577] The encoding contains a total of 386 distinct variables.
[0.577] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.577] Executed solver; result: UNSAT.
[0.577] 
[0.577] *************************************
[0.577] * * *   M a k e s p a n     1   * * *
[0.577] *************************************
[0.578] Computed next depth properties: array size of 3.
[0.579] Instantiated 93 transitional clauses.
[0.581] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.581] Instantiated 732 universal clauses.
[0.581] Instantiated and added clauses for a total of 1,388 clauses.
[0.581] The encoding contains a total of 571 distinct variables.
[0.581] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.581] Executed solver; result: UNSAT.
[0.581] 
[0.581] *************************************
[0.581] * * *   M a k e s p a n     2   * * *
[0.581] *************************************
[0.586] Computed next depth properties: array size of 4.
[0.601] Instantiated 50,622 transitional clauses.
[1.308] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.308] Instantiated 32,653,619 universal clauses.
[1.308] Instantiated and added clauses for a total of 32,705,629 clauses.
[1.308] The encoding contains a total of 18,932 distinct variables.
[1.308] Attempting solve with solver <glucose4> ...
c 4 assumptions
[1.310] Executed solver; result: UNSAT.
[1.310] 
[1.310] *************************************
[1.310] * * *   M a k e s p a n     3   * * *
[1.310] *************************************
[1.323] Computed next depth properties: array size of 14.
[1.413] Instantiated 139,587 transitional clauses.
[2.889] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.889] Instantiated 65,650,940 universal clauses.
[2.889] Instantiated and added clauses for a total of 98,496,156 clauses.
[2.889] The encoding contains a total of 41,987 distinct variables.
[2.889] Attempting solve with solver <glucose4> ...
c 14 assumptions
[2.889] Executed solver; result: UNSAT.
[2.889] 
[2.889] *************************************
[2.889] * * *   M a k e s p a n     4   * * *
[2.889] *************************************
[2.909] Computed next depth properties: array size of 32.
[3.006] Instantiated 147,246 transitional clauses.
[4.614] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.614] Instantiated 67,807,550 universal clauses.
[4.614] Instantiated and added clauses for a total of 166,450,952 clauses.
[4.614] The encoding contains a total of 68,684 distinct variables.
[4.614] Attempting solve with solver <glucose4> ...
c 32 assumptions
[4.615] Executed solver; result: UNSAT.
[4.615] 
[4.615] *************************************
[4.615] * * *   M a k e s p a n     5   * * *
[4.615] *************************************
[4.641] Computed next depth properties: array size of 52.
[4.749] Instantiated 151,366 transitional clauses.
[6.491] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[6.491] Instantiated 69,921,122 universal clauses.
[6.491] Instantiated and added clauses for a total of 236,523,440 clauses.
[6.491] The encoding contains a total of 98,564 distinct variables.
[6.491] Attempting solve with solver <glucose4> ...
c 52 assumptions
[6.493] Executed solver; result: UNSAT.
[6.493] 
[6.493] *************************************
[6.493] * * *   M a k e s p a n     6   * * *
[6.493] *************************************
[6.527] Computed next depth properties: array size of 74.
[6.639] Instantiated 155,070 transitional clauses.
[8.531] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[8.531] Instantiated 71,989,544 universal clauses.
[8.531] Instantiated and added clauses for a total of 308,668,054 clauses.
[8.531] The encoding contains a total of 131,481 distinct variables.
[8.531] Attempting solve with solver <glucose4> ...
c 74 assumptions
[8.534] Executed solver; result: UNSAT.
[8.534] 
[8.534] *************************************
[8.535] * * *   M a k e s p a n     7   * * *
[8.535] *************************************
[8.574] Computed next depth properties: array size of 98.
[8.700] Instantiated 158,478 transitional clauses.
[10.704] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[10.704] Instantiated 74,019,936 universal clauses.
[10.704] Instantiated and added clauses for a total of 382,846,468 clauses.
[10.704] The encoding contains a total of 167,281 distinct variables.
[10.704] Attempting solve with solver <glucose4> ...
c 98 assumptions
[10.708] Executed solver; result: UNSAT.
[10.708] 
[10.708] *************************************
[10.708] * * *   M a k e s p a n     8   * * *
[10.708] *************************************
[10.754] Computed next depth properties: array size of 124.
[10.886] Instantiated 161,566 transitional clauses.
[13.020] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[13.020] Instantiated 76,020,074 universal clauses.
[13.020] Instantiated and added clauses for a total of 459,028,108 clauses.
[13.020] The encoding contains a total of 205,794 distinct variables.
[13.020] Attempting solve with solver <glucose4> ...
c 124 assumptions
[13.024] Executed solver; result: UNSAT.
[13.024] 
[13.024] *************************************
[13.024] * * *   M a k e s p a n     9   * * *
[13.024] *************************************
[13.079] Computed next depth properties: array size of 152.
[13.222] Instantiated 163,986 transitional clauses.
[15.480] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[15.480] Instantiated 77,996,028 universal clauses.
[15.480] Instantiated and added clauses for a total of 537,188,122 clauses.
[15.480] The encoding contains a total of 246,495 distinct variables.
[15.480] Attempting solve with solver <glucose4> ...
c 152 assumptions
[15.486] Executed solver; result: UNSAT.
[15.486] 
[15.486] *************************************
[15.486] * * *   M a k e s p a n    10   * * *
[15.486] *************************************
[15.547] Computed next depth properties: array size of 166.
[15.697] Instantiated 162,596 transitional clauses.
[18.078] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[18.078] Instantiated 79,953,314 universal clauses.
[18.078] Instantiated and added clauses for a total of 617,304,032 clauses.
[18.078] The encoding contains a total of 289,357 distinct variables.
[18.078] Attempting solve with solver <glucose4> ...
c 166 assumptions
[18.091] Executed solver; result: UNSAT.
[18.091] 
[18.091] *************************************
[18.091] * * *   M a k e s p a n    11   * * *
[18.091] *************************************
[18.157] Computed next depth properties: array size of 180.
[18.311] Instantiated 167,468 transitional clauses.
[20.821] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[20.821] Instantiated 81,910,600 universal clauses.
[20.821] Instantiated and added clauses for a total of 699,382,100 clauses.
[20.821] The encoding contains a total of 334,669 distinct variables.
[20.821] Attempting solve with solver <glucose4> ...
c 180 assumptions
[21.093] Executed solver; result: UNSAT.
[21.093] 
[21.093] *************************************
[21.093] * * *   M a k e s p a n    12   * * *
[21.093] *************************************
[21.163] Computed next depth properties: array size of 194.
[21.323] Instantiated 172,340 transitional clauses.
[23.949] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[23.949] Instantiated 83,867,886 universal clauses.
[23.949] Instantiated and added clauses for a total of 783,422,326 clauses.
[23.949] The encoding contains a total of 382,431 distinct variables.
[23.949] Attempting solve with solver <glucose4> ...
c 194 assumptions
[23.969] Executed solver; result: UNSAT.
[23.969] 
[23.969] *************************************
[23.969] * * *   M a k e s p a n    13   * * *
[23.969] *************************************
[24.043] Computed next depth properties: array size of 208.
[24.214] Instantiated 177,212 transitional clauses.
[27.013] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[27.013] Instantiated 85,825,172 universal clauses.
[27.013] Instantiated and added clauses for a total of 869,424,710 clauses.
[27.013] The encoding contains a total of 432,643 distinct variables.
[27.013] Attempting solve with solver <glucose4> ...
c 208 assumptions
c last restart ## conflicts  :  13 953 
[31.774] Executed solver; result: SAT.
[31.774] Solver returned SAT; a solution has been found at makespan 13.
85
solution 8953 1
8953 43 25 44 26 1432 1431 1434 1433 45 7 8 25 95 67 92 68 1438 1436 1437 1435 93 61 62 67 143 115 140 116 1439 1442 1440 1441 141 109 110 115 187 169 188 170 1443 1446 1444 1445 189 151 152 169 235 217 236 218 1447 1450 1449 1448 237 199 200 217 283 265 284 266 1451 1454 1453 1452 285 247 248 265 331 313 332 314 1455 1457 1458 1456 333 295 296 313 
[31.780] Exiting.

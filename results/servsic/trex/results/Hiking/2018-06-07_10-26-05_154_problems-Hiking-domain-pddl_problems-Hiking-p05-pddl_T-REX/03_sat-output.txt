Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.157] Processed problem encoding.
[0.188] Calculated possible fact changes of composite elements.
[0.188] Initialized instantiation procedure.
[0.188] 
[0.188] *************************************
[0.188] * * *   M a k e s p a n     0   * * *
[0.188] *************************************
[0.188] Instantiated 378 initial clauses.
[0.188] The encoding contains a total of 260 distinct variables.
[0.188] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.188] Executed solver; result: UNSAT.
[0.188] 
[0.188] *************************************
[0.188] * * *   M a k e s p a n     1   * * *
[0.188] *************************************
[0.189] Computed next depth properties: array size of 3.
[0.189] Instantiated 62 transitional clauses.
[0.190] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.190] Instantiated 491 universal clauses.
[0.190] Instantiated and added clauses for a total of 931 clauses.
[0.190] The encoding contains a total of 387 distinct variables.
[0.190] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.190] Executed solver; result: UNSAT.
[0.190] 
[0.190] *************************************
[0.190] * * *   M a k e s p a n     2   * * *
[0.190] *************************************
[0.191] Computed next depth properties: array size of 4.
[0.196] Instantiated 17,185 transitional clauses.
[0.281] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.281] Instantiated 3,692,352 universal clauses.
[0.281] Instantiated and added clauses for a total of 3,710,468 clauses.
[0.281] The encoding contains a total of 6,772 distinct variables.
[0.281] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.282] Executed solver; result: UNSAT.
[0.282] 
[0.282] *************************************
[0.282] * * *   M a k e s p a n     3   * * *
[0.282] *************************************
[0.287] Computed next depth properties: array size of 14.
[0.309] Instantiated 41,995 transitional clauses.
[0.501] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.501] Instantiated 7,503,340 universal clauses.
[0.501] Instantiated and added clauses for a total of 11,255,803 clauses.
[0.501] The encoding contains a total of 15,088 distinct variables.
[0.501] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.502] Executed solver; result: UNSAT.
[0.502] 
[0.502] *************************************
[0.502] * * *   M a k e s p a n     4   * * *
[0.502] *************************************
[0.508] Computed next depth properties: array size of 32.
[0.534] Instantiated 44,469 transitional clauses.
[0.747] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.747] Instantiated 7,926,353 universal clauses.
[0.747] Instantiated and added clauses for a total of 19,226,625 clauses.
[0.747] The encoding contains a total of 25,176 distinct variables.
[0.747] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.748] Executed solver; result: UNSAT.
[0.748] 
[0.748] *************************************
[0.748] * * *   M a k e s p a n     5   * * *
[0.748] *************************************
[0.757] Computed next depth properties: array size of 52.
[0.783] Instantiated 46,722 transitional clauses.
[1.022] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.022] Instantiated 8,340,763 universal clauses.
[1.022] Instantiated and added clauses for a total of 27,614,110 clauses.
[1.022] The encoding contains a total of 36,734 distinct variables.
[1.022] Attempting solve with solver <glucose4> ...
c 52 assumptions
[1.023] Executed solver; result: UNSAT.
[1.023] 
[1.023] *************************************
[1.023] * * *   M a k e s p a n     6   * * *
[1.023] *************************************
[1.035] Computed next depth properties: array size of 74.
[1.063] Instantiated 48,731 transitional clauses.
[1.325] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.325] Instantiated 8,746,144 universal clauses.
[1.325] Instantiated and added clauses for a total of 36,408,985 clauses.
[1.325] The encoding contains a total of 49,675 distinct variables.
[1.325] Attempting solve with solver <glucose4> ...
c 74 assumptions
[1.326] Executed solver; result: UNSAT.
[1.326] 
[1.326] *************************************
[1.326] * * *   M a k e s p a n     7   * * *
[1.326] *************************************
[1.340] Computed next depth properties: array size of 98.
[1.373] Instantiated 50,566 transitional clauses.
[1.657] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.657] Instantiated 9,144,107 universal clauses.
[1.657] Instantiated and added clauses for a total of 45,603,658 clauses.
[1.657] The encoding contains a total of 63,909 distinct variables.
[1.657] Attempting solve with solver <glucose4> ...
c 98 assumptions
[1.659] Executed solver; result: UNSAT.
[1.659] 
[1.659] *************************************
[1.659] * * *   M a k e s p a n     8   * * *
[1.659] *************************************
[1.675] Computed next depth properties: array size of 124.
[1.708] Instantiated 52,112 transitional clauses.
[2.018] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.018] Instantiated 9,535,809 universal clauses.
[2.018] Instantiated and added clauses for a total of 55,191,579 clauses.
[2.018] The encoding contains a total of 79,215 distinct variables.
[2.018] Attempting solve with solver <glucose4> ...
c 124 assumptions
[2.046] Executed solver; result: UNSAT.
[2.046] 
[2.046] *************************************
[2.046] * * *   M a k e s p a n     9   * * *
[2.046] *************************************
[2.065] Computed next depth properties: array size of 136.
[2.104] Instantiated 52,090 transitional clauses.
[2.438] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.438] Instantiated 9,922,175 universal clauses.
[2.438] Instantiated and added clauses for a total of 65,165,844 clauses.
[2.438] The encoding contains a total of 95,388 distinct variables.
[2.438] Attempting solve with solver <glucose4> ...
c 136 assumptions
[2.442] Executed solver; result: UNSAT.
[2.442] 
[2.442] *************************************
[2.442] * * *   M a k e s p a n    10   * * *
[2.442] *************************************
[2.463] Computed next depth properties: array size of 148.
[2.500] Instantiated 54,244 transitional clauses.
[2.860] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.860] Instantiated 10,308,541 universal clauses.
[2.860] Instantiated and added clauses for a total of 75,528,629 clauses.
[2.860] The encoding contains a total of 112,650 distinct variables.
[2.860] Attempting solve with solver <glucose4> ...
c 148 assumptions
[2.864] Executed solver; result: UNSAT.
[2.864] 
[2.864] *************************************
[2.864] * * *   M a k e s p a n    11   * * *
[2.864] *************************************
[2.886] Computed next depth properties: array size of 160.
[2.927] Instantiated 56,398 transitional clauses.
[3.318] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.318] Instantiated 10,694,907 universal clauses.
[3.318] Instantiated and added clauses for a total of 86,279,934 clauses.
[3.318] The encoding contains a total of 131,001 distinct variables.
[3.318] Attempting solve with solver <glucose4> ...
c 160 assumptions
c last restart ## conflicts  :  68 258 
[3.343] Executed solver; result: SAT.
[3.343] Solver returned SAT; a solution has been found at makespan 11.
67
solution 3147 1
3147 239 220 235 221 653 651 652 236 222 223 220 268 241 269 243 655 654 656 270 245 246 241 302 275 303 277 657 659 658 304 279 280 275 336 309 337 311 662 661 660 338 313 314 309 370 343 371 345 663 665 664 372 347 348 343 404 377 405 379 667 666 668 406 381 382 377 
[3.346] Exiting.

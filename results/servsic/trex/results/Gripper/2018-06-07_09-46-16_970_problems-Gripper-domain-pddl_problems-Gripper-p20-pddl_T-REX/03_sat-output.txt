Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.003] Processed problem encoding.
[0.003] Calculated possible fact changes of composite elements.
[0.003] Initialized instantiation procedure.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     0   * * *
[0.003] *************************************
[0.004] Instantiated 1,103 initial clauses.
[0.004] The encoding contains a total of 652 distinct variables.
[0.004] Attempting solve with solver <glucose4> ...
c 22 assumptions
[0.004] Executed solver; result: UNSAT.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     1   * * *
[0.004] *************************************
[0.004] Computed next depth properties: array size of 127.
[0.005] Instantiated 569 transitional clauses.
[0.007] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.007] Instantiated 3,009 universal clauses.
[0.007] Instantiated and added clauses for a total of 4,681 clauses.
[0.007] The encoding contains a total of 1,412 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 127 assumptions
c last restart ## conflicts  :  0 210 
[0.007] Executed solver; result: SAT.
[0.007] Solver returned SAT; a solution has been found at makespan 1.
125
solution 171 1
170 164 5 171 165 2 162 156 5 163 157 2 154 148 5 155 149 2 146 140 5 147 141 2 138 132 5 139 133 2 130 124 5 131 125 2 122 116 5 123 117 2 114 108 5 115 109 2 106 100 5 107 101 2 98 92 5 99 93 2 90 84 5 91 85 2 82 76 5 83 77 2 74 68 5 75 69 2 66 60 5 67 61 2 58 52 5 59 53 2 50 44 5 51 45 2 42 36 5 43 37 2 34 28 5 35 29 2 26 20 5 27 21 2 18 12 5 19 13 2 9 8 5 11 10 
[0.008] Exiting.

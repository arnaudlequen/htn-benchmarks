Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.002] Processed problem encoding.
[0.002] Calculated possible fact changes of composite elements.
[0.002] Initialized instantiation procedure.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     0   * * *
[0.002] *************************************
[0.002] Instantiated 732 initial clauses.
[0.002] The encoding contains a total of 435 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     1   * * *
[0.002] *************************************
[0.003] Computed next depth properties: array size of 85.
[0.003] Instantiated 380 transitional clauses.
[0.004] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.004] Instantiated 2,001 universal clauses.
[0.004] Instantiated and added clauses for a total of 3,113 clauses.
[0.004] The encoding contains a total of 943 distinct variables.
[0.004] Attempting solve with solver <glucose4> ...
c 85 assumptions
c last restart ## conflicts  :  0 140 
[0.005] Executed solver; result: SAT.
[0.005] Solver returned SAT; a solution has been found at makespan 1.
83
solution 115 1
114 108 5 115 109 2 106 100 5 107 101 2 98 92 5 99 93 2 90 84 5 91 85 2 82 76 5 83 77 2 74 68 5 75 69 2 66 60 5 67 61 2 58 52 5 59 53 2 50 44 5 51 45 2 42 36 5 43 37 2 34 28 5 35 29 2 26 20 5 27 21 2 18 12 5 19 13 2 9 8 5 11 10 
[0.005] Exiting.

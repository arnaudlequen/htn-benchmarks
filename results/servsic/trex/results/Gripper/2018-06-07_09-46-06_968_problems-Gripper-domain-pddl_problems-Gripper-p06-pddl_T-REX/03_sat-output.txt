Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.002] Instantiated 361 initial clauses.
[0.002] The encoding contains a total of 218 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 8 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     1   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 43.
[0.002] Instantiated 191 transitional clauses.
[0.003] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.003] Instantiated 993 universal clauses.
[0.003] Instantiated and added clauses for a total of 1,545 clauses.
[0.003] The encoding contains a total of 474 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 43 assumptions
c last restart ## conflicts  :  0 70 
[0.003] Executed solver; result: SAT.
[0.003] Solver returned SAT; a solution has been found at makespan 1.
41
solution 57 1
56 54 5 57 55 2 50 44 5 51 45 2 42 36 5 43 37 2 34 28 5 35 29 2 26 20 5 27 21 2 18 12 5 19 13 2 9 8 5 11 10 
[0.003] Exiting.

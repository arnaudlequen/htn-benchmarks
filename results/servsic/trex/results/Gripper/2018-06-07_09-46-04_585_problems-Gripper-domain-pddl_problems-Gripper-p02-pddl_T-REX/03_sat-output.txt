Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 149 initial clauses.
[0.001] The encoding contains a total of 94 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 19.
[0.001] Instantiated 83 transitional clauses.
[0.002] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.002] Instantiated 417 universal clauses.
[0.002] Instantiated and added clauses for a total of 649 clauses.
[0.002] The encoding contains a total of 206 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 19 assumptions
c last restart ## conflicts  :  0 30 
[0.002] Executed solver; result: SAT.
[0.002] Solver returned SAT; a solution has been found at makespan 1.
17
solution 27 1
26 20 5 27 21 2 18 12 5 19 13 2 9 8 5 11 10 
[0.002] Exiting.

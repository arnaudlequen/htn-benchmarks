Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.004] Parsed head comment information.
[0.233] Processed problem encoding.
[0.242] Calculated possible fact changes of composite elements.
[0.259] Initialized instantiation procedure.
[0.259] 
[0.259] *************************************
[0.259] * * *   M a k e s p a n     0   * * *
[0.259] *************************************
[0.277] Instantiated 52,420 initial clauses.
[0.277] The encoding contains a total of 50,637 distinct variables.
[0.277] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.278] Executed solver; result: UNSAT.
[0.278] 
[0.278] *************************************
[0.278] * * *   M a k e s p a n     1   * * *
[0.278] *************************************
[0.311] Computed next depth properties: array size of 71.
[0.388] Instantiated 261,298 transitional clauses.
[1.098] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.098] Instantiated 11,408,993 universal clauses.
[1.098] Instantiated and added clauses for a total of 11,722,711 clauses.
[1.098] The encoding contains a total of 71,416 distinct variables.
[1.098] Attempting solve with solver <glucose4> ...
c 71 assumptions
c last restart ## conflicts  :  1 5281 
[1.206] Executed solver; result: SAT.
[1.206] Solver returned SAT; a solution has been found at makespan 1.
70
solution 2854 1
384 118 12 119 14 1570 130 600 901 602 249 102 600 643 602 1994 41 2433 2447 2435 1353 76 603 2519 605 1029 69 603 2573 605 1685 135 2427 2658 2429 1078 7 600 2662 602 1812 95 2430 2755 2432 2296 27 2433 2783 2435 2092 60 8 2854 10 503 18 8 664 10 494 88 600 751 602 163 34 12 785 14 
[1.207] Exiting.

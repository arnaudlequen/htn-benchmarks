Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.003] Parsed head comment information.
[0.165] Processed problem encoding.
[0.171] Calculated possible fact changes of composite elements.
[0.181] Initialized instantiation procedure.
[0.181] 
[0.181] *************************************
[0.181] * * *   M a k e s p a n     0   * * *
[0.181] *************************************
[0.192] Instantiated 30,420 initial clauses.
[0.192] The encoding contains a total of 29,110 distinct variables.
[0.192] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.192] Executed solver; result: UNSAT.
[0.192] 
[0.192] *************************************
[0.192] * * *   M a k e s p a n     1   * * *
[0.192] *************************************
[0.210] Computed next depth properties: array size of 61.
[0.256] Instantiated 148,682 transitional clauses.
[0.540] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.540] Instantiated 4,435,579 universal clauses.
[0.540] Instantiated and added clauses for a total of 4,614,681 clauses.
[0.540] The encoding contains a total of 41,650 distinct variables.
[0.540] Attempting solve with solver <glucose4> ...
c 61 assumptions
c last restart ## conflicts  :  6 8180 
[0.593] Executed solver; result: SAT.
[0.593] Solver returned SAT; a solution has been found at makespan 1.
60
solution 1881 1
85 86 4 87 6 1446 25 363 526 365 343 114 360 411 362 1289 44 4 1603 6 188 39 417 433 419 923 58 414 1657 416 596 11 420 1686 422 999 34 12 1743 14 1098 51 360 1798 362 701 72 414 1855 416 1525 20 366 1881 368 288 69 366 494 368 
[0.594] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.022] Parsed head comment information.
[1.901] Processed problem encoding.
[1.999] Calculated possible fact changes of composite elements.
[2.139] Initialized instantiation procedure.
[2.139] 
[2.139] *************************************
[2.139] * * *   M a k e s p a n     0   * * *
[2.139] *************************************
[2.267] Instantiated 312,649 initial clauses.
[2.267] The encoding contains a total of 308,170 distinct variables.
[2.267] Attempting solve with solver <glucose4> ...
c 22 assumptions
[2.270] Executed solver; result: UNSAT.
[2.270] 
[2.270] *************************************
[2.270] * * *   M a k e s p a n     1   * * *
[2.270] *************************************
[2.536] Computed next depth properties: array size of 106.
[3.117] Instantiated 1,597,766 transitional clauses.
[15.654] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[15.654] Instantiated 159,285,167 universal clauses.
[15.654] Instantiated and added clauses for a total of 161,195,582 clauses.
[15.654] The encoding contains a total of 396,350 distinct variables.
[15.654] Attempting solve with solver <glucose4> ...
c 106 assumptions
c last restart ## conflicts  :  267 92499 
[16.400] Executed solver; result: SAT.
[16.400] Solver returned SAT; a solution has been found at makespan 1.
105
solution 8979 1
1035 170 16 171 18 2001 38 2026 2046 2028 3475 62 2035 2855 2037 7159 200 2026 7754 2028 5456 249 2712 7888 2714 3018 227 4 7990 6 7334 11 2032 8008 2034 4316 69 2712 8144 2714 3668 29 2706 8238 2708 5625 254 4 8450 6 154 161 16 2213 18 1719 20 2026 2262 2028 6369 146 2706 8514 2708 5948 139 2709 8623 2711 6926 119 4 8726 6 506 193 2029 2451 2031 763 236 4 2582 6 4654 85 2709 8823 2711 1460 215 16 2685 18 5001 184 2029 8979 2031 1305 74 2706 2742 2708 
[16.402] Exiting.

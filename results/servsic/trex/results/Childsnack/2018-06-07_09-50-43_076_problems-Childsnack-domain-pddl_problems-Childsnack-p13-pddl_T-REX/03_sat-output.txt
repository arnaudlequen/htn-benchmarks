Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.010] Parsed head comment information.
[0.734] Processed problem encoding.
[0.772] Calculated possible fact changes of composite elements.
[0.836] Initialized instantiation procedure.
[0.836] 
[0.836] *************************************
[0.836] * * *   M a k e s p a n     0   * * *
[0.836] *************************************
[0.891] Instantiated 148,582 initial clauses.
[0.891] The encoding contains a total of 145,571 distinct variables.
[0.891] Attempting solve with solver <glucose4> ...
c 18 assumptions
[0.893] Executed solver; result: UNSAT.
[0.893] 
[0.893] *************************************
[0.893] * * *   M a k e s p a n     1   * * *
[0.893] *************************************
[1.000] Computed next depth properties: array size of 86.
[1.258] Instantiated 750,467 transitional clauses.
[4.727] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.727] Instantiated 45,101,638 universal clauses.
[4.727] Instantiated and added clauses for a total of 46,000,687 clauses.
[4.727] The encoding contains a total of 189,411 distinct variables.
[4.727] Attempting solve with solver <glucose4> ...
c 86 assumptions
c last restart ## conflicts  :  59 26369 
[4.813] Executed solver; result: SAT.
[4.813] Solver returned SAT; a solution has been found at makespan 1.
85
solution 5235 1
2759 188 1031 1597 1033 563 26 16 27 18 2102 67 8 4398 10 118 125 1031 1081 1033 907 15 16 1125 18 380 35 1031 1225 1033 3048 98 1031 4504 1033 1754 38 1306 4569 1308 1971 128 1306 4701 1308 4233 202 8 4826 10 693 157 1309 1379 1311 2597 173 4 4905 6 3950 141 12 4983 14 3670 101 4 5057 6 3579 116 16 5156 18 3139 87 1312 5235 1314 876 146 1022 1466 1024 
[4.814] Exiting.

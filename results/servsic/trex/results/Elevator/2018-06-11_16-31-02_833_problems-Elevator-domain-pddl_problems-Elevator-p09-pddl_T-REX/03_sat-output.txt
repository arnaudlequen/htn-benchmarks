Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.000] Processed problem encoding.
[0.000] Calculated possible fact changes of composite elements.
[0.000] Initialized instantiation procedure.
[0.000] 
[0.000] *************************************
[0.000] * * *   M a k e s p a n     0   * * *
[0.000] *************************************
[0.000] Instantiated 44 initial clauses.
[0.000] The encoding contains a total of 31 distinct variables.
[0.000] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.000] Executed solver; result: UNSAT.
[0.000] 
[0.000] *************************************
[0.000] * * *   M a k e s p a n     1   * * *
[0.000] *************************************
[0.000] Computed next depth properties: array size of 5.
[0.001] Instantiated 28 transitional clauses.
[0.001] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.001] Instantiated 95 universal clauses.
[0.001] Instantiated and added clauses for a total of 167 clauses.
[0.001] The encoding contains a total of 65 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 5 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     2   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 9.
[0.001] Instantiated 58 transitional clauses.
[0.001] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.001] Instantiated 205 universal clauses.
[0.001] Instantiated and added clauses for a total of 430 clauses.
[0.001] The encoding contains a total of 123 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 9 assumptions
c last restart ## conflicts  :  0 15 
[0.001] Executed solver; result: SAT.
[0.001] Solver returned SAT; a solution has been found at makespan 2.
8
solution 14 1
7 2 13 10 6 3 14 11 
[0.001] Exiting.

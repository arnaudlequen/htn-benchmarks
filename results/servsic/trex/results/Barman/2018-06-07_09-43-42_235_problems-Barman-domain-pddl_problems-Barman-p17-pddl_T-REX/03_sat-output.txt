Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.002] Processed problem encoding.
[0.002] Calculated possible fact changes of composite elements.
[0.002] Initialized instantiation procedure.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     0   * * *
[0.002] *************************************
[0.002] Instantiated 1,006 initial clauses.
[0.002] The encoding contains a total of 592 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     1   * * *
[0.002] *************************************
[0.003] Computed next depth properties: array size of 48.
[0.003] Instantiated 234 transitional clauses.
[0.003] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.003] Instantiated 1,653 universal clauses.
[0.003] Instantiated and added clauses for a total of 2,893 clauses.
[0.003] The encoding contains a total of 1,054 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 48 assumptions
[0.003] Executed solver; result: UNSAT.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     2   * * *
[0.003] *************************************
[0.004] Computed next depth properties: array size of 158.
[0.004] Instantiated 806 transitional clauses.
[0.006] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.006] Instantiated 5,822 universal clauses.
[0.006] Instantiated and added clauses for a total of 9,521 clauses.
[0.006] The encoding contains a total of 2,112 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 158 assumptions
c last restart ## conflicts  :  8 190 
[0.007] Executed solver; result: SAT.
[0.007] Solver returned SAT; a solution has been found at makespan 2.
157
solution 295 1
2 30 96 97 26 185 102 27 228 250 251 252 230 231 4 36 106 107 34 189 104 32 228 238 276 239 230 231 6 40 112 113 42 193 118 38 228 272 278 273 230 231 9 46 125 126 49 199 121 47 233 258 281 259 235 236 10 50 130 131 54 201 128 51 228 260 282 261 230 231 12 56 138 139 60 205 136 57 228 246 284 247 230 231 14 66 144 145 62 209 150 63 228 242 286 243 230 231 16 72 154 155 70 213 152 68 228 268 288 269 230 231 18 78 160 161 76 217 166 74 228 264 290 265 230 231 21 83 171 172 85 223 175 81 233 234 293 237 235 236 22 88 86 25 95 179 180 92 227 183 93 233 253 295 255 235 236 
[0.007] Exiting.

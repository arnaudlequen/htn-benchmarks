Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.005] Processed problem encoding.
[0.005] Calculated possible fact changes of composite elements.
[0.005] Initialized instantiation procedure.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     0   * * *
[0.005] *************************************
[0.006] Instantiated 1,264 initial clauses.
[0.006] The encoding contains a total of 740 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 16 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.007] Computed next depth properties: array size of 60.
[0.007] Instantiated 294 transitional clauses.
[0.009] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.009] Instantiated 2,080 universal clauses.
[0.009] Instantiated and added clauses for a total of 3,638 clauses.
[0.009] The encoding contains a total of 1,322 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 60 assumptions
[0.009] Executed solver; result: UNSAT.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     2   * * *
[0.009] *************************************
[0.011] Computed next depth properties: array size of 200.
[0.012] Instantiated 1,022 transitional clauses.
[0.017] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.017] Instantiated 7,386 universal clauses.
[0.017] Instantiated and added clauses for a total of 12,046 clauses.
[0.017] The encoding contains a total of 2,665 distinct variables.
[0.017] Attempting solve with solver <glucose4> ...
c 200 assumptions
c last restart ## conflicts  :  11 225 
[0.018] Executed solver; result: SAT.
[0.018] Solver returned SAT; a solution has been found at makespan 2.
199
solution 364 1
3 37 125 126 34 225 121 35 293 337 338 339 295 296 4 40 128 129 42 228 227 38 288 314 340 315 290 291 6 48 136 137 46 233 134 44 288 318 342 319 290 291 8 52 142 143 54 238 237 50 288 322 344 323 290 291 10 60 150 151 56 243 148 57 288 330 346 331 290 291 13 67 161 162 64 249 157 65 293 328 349 329 295 296 14 68 164 165 72 251 170 69 288 289 350 292 290 291 16 74 172 173 78 255 178 75 288 302 352 303 290 291 18 82 180 181 84 259 186 80 288 298 354 299 290 291 20 88 188 189 90 264 263 86 288 310 356 311 290 291 22 94 194 195 96 270 269 92 288 306 358 307 290 291 24 98 200 201 102 275 206 99 288 302 360 303 290 291 26 106 208 209 108 280 279 104 288 310 362 311 290 291 28 112 110 30 118 216 217 114 285 214 115 288 330 364 331 290 291 
[0.019] Exiting.

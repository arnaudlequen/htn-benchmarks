Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.005] Processed problem encoding.
[0.005] Calculated possible fact changes of composite elements.
[0.005] Initialized instantiation procedure.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     0   * * *
[0.005] *************************************
[0.006] Instantiated 1,212 initial clauses.
[0.006] The encoding contains a total of 708 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.007] Computed next depth properties: array size of 56.
[0.007] Instantiated 274 transitional clauses.
[0.009] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.009] Instantiated 1,971 universal clauses.
[0.009] Instantiated and added clauses for a total of 3,457 clauses.
[0.009] The encoding contains a total of 1,250 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 56 assumptions
[0.009] Executed solver; result: UNSAT.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     2   * * *
[0.009] *************************************
[0.011] Computed next depth properties: array size of 186.
[0.012] Instantiated 950 transitional clauses.
[0.016] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.016] Instantiated 6,898 universal clauses.
[0.016] Instantiated and added clauses for a total of 11,305 clauses.
[0.016] The encoding contains a total of 2,498 distinct variables.
[0.016] Attempting solve with solver <glucose4> ...
c 186 assumptions
c last restart ## conflicts  :  11 206 
[0.018] Executed solver; result: SAT.
[0.018] Solver returned SAT; a solution has been found at makespan 2.
185
solution 342 1
2 34 112 113 32 217 118 30 268 298 299 300 270 271 5 41 123 124 38 223 127 39 273 280 321 281 275 276 6 46 130 131 44 225 128 42 268 282 322 283 270 271 9 51 141 142 53 231 137 49 273 318 325 319 275 276 10 58 146 147 56 233 144 54 268 304 326 305 270 271 12 60 154 155 64 237 152 61 268 286 328 287 270 271 14 70 160 161 68 241 166 66 268 269 330 272 270 271 16 76 168 169 74 245 174 72 268 312 332 313 270 271 18 82 176 177 78 249 182 79 268 308 334 309 270 271 20 86 186 187 88 253 184 84 268 290 336 291 270 271 22 90 192 193 94 257 198 91 268 294 338 295 270 271 24 96 200 201 100 261 206 97 268 294 340 295 270 271 26 106 210 211 104 265 208 102 268 304 342 305 270 271 28 110 108 
[0.019] Exiting.

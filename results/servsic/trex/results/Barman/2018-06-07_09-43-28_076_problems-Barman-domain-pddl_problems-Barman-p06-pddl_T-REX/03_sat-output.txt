Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.005] Processed problem encoding.
[0.005] Calculated possible fact changes of composite elements.
[0.005] Initialized instantiation procedure.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     0   * * *
[0.005] *************************************
[0.006] Instantiated 1,221 initial clauses.
[0.006] The encoding contains a total of 714 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.007] Computed next depth properties: array size of 57.
[0.007] Instantiated 282 transitional clauses.
[0.009] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.009] Instantiated 1,987 universal clauses.
[0.009] Instantiated and added clauses for a total of 3,490 clauses.
[0.009] The encoding contains a total of 1,278 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.009] Executed solver; result: UNSAT.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     2   * * *
[0.009] *************************************
[0.011] Computed next depth properties: array size of 197.
[0.012] Instantiated 1,010 transitional clauses.
[0.017] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.017] Instantiated 7,293 universal clauses.
[0.017] Instantiated and added clauses for a total of 11,793 clauses.
[0.017] The encoding contains a total of 2,612 distinct variables.
[0.017] Attempting solve with solver <glucose4> ...
c 197 assumptions
c last restart ## conflicts  :  8 219 
[0.018] Executed solver; result: SAT.
[0.018] Solver returned SAT; a solution has been found at makespan 2.
196
solution 358 1
2 34 116 117 32 227 114 30 282 296 297 298 284 285 5 38 127 128 41 233 123 39 287 328 335 329 289 290 7 45 135 136 47 237 131 43 287 332 337 333 289 290 8 48 138 139 52 239 144 49 282 314 338 315 284 285 10 58 146 147 56 243 152 54 282 322 340 323 284 285 13 65 157 158 62 249 161 63 287 308 343 309 289 290 14 70 162 163 66 251 168 67 282 302 344 303 284 285 16 76 172 173 74 255 170 72 282 292 346 293 284 285 18 80 178 179 82 259 184 78 282 283 348 286 284 285 20 88 186 187 86 263 192 84 282 318 350 319 284 285 22 94 194 195 90 267 200 91 282 310 352 311 284 285 24 100 202 203 98 271 208 96 282 318 354 319 284 285 26 104 210 211 106 275 216 102 282 283 356 286 284 285 28 108 220 221 112 279 218 109 282 326 358 327 284 285 
[0.018] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.003] Parsed head comment information.
[0.111] Processed problem encoding.
[0.121] Calculated possible fact changes of composite elements.
[0.130] Initialized instantiation procedure.
[0.130] 
[0.130] *************************************
[0.130] * * *   M a k e s p a n     0   * * *
[0.130] *************************************
[0.153] Instantiated 21,398 initial clauses.
[0.153] The encoding contains a total of 10,903 distinct variables.
[0.153] Attempting solve with solver <glucose4> ...
c 101 assumptions
[0.153] Executed solver; result: UNSAT.
[0.153] 
[0.153] *************************************
[0.153] * * *   M a k e s p a n     1   * * *
[0.153] *************************************
[0.192] Computed next depth properties: array size of 201.
[0.213] Instantiated 20,502 transitional clauses.
[0.279] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.279] Instantiated 91,297 universal clauses.
[0.279] Instantiated and added clauses for a total of 133,197 clauses.
[0.279] The encoding contains a total of 41,207 distinct variables.
[0.279] Attempting solve with solver <glucose4> ...
c 201 assumptions
[0.280] Executed solver; result: UNSAT.
[0.280] 
[0.280] *************************************
[0.280] * * *   M a k e s p a n     2   * * *
[0.280] *************************************
[0.356] Computed next depth properties: array size of 401.
[0.426] Instantiated 60,402 transitional clauses.
[0.585] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.585] Instantiated 1,152,797 universal clauses.
[0.585] Instantiated and added clauses for a total of 1,346,396 clauses.
[0.585] The encoding contains a total of 82,311 distinct variables.
[0.585] Attempting solve with solver <glucose4> ...
c 401 assumptions
c last restart ## conflicts  :  0 407 
[0.585] Executed solver; result: SAT.
[0.585] Solver returned SAT; a solution has been found at makespan 2.
399
solution 8966 1
2857 2 6174 6042 184 3 7359 6043 2949 4 7429 6044 3030 5 3233 6045 354 6 6284 6046 431 7 6277 6047 482 8 7541 6048 3148 9 6360 6049 589 10 2836 6050 3043 11 1957 6051 3257 12 7564 6052 3322 13 984 6053 3477 14 193 6054 3035 15 7635 6055 3516 16 2798 6056 683 17 2931 6057 3659 18 7598 6058 779 19 6499 6059 855 20 1500 6060 870 21 6557 6061 3393 22 7736 6062 3542 23 7686 6063 962 24 7763 6064 3890 25 7868 6065 3945 26 7903 6066 3971 27 8028 6067 4054 28 7453 6068 4154 29 2420 6069 3830 30 5780 6070 4247 31 8113 6071 4298 32 1531 6072 1218 33 6810 6073 3451 34 2235 6074 4361 35 8191 6075 911 36 6902 6076 1340 37 5953 6077 904 38 5635 6078 4429 39 8343 6079 4500 40 1668 6080 4407 41 5026 6081 1518 42 5287 6082 1587 43 8412 6083 4652 44 3696 6084 1708 45 5222 6085 4739 46 8495 6086 3528 47 616 6087 477 48 4608 6088 1775 49 6228 6089 1843 50 5995 6090 1947 51 6811 6091 3650 52 1804 6092 2899 53 7055 6093 1998 54 1552 6094 4929 55 8593 6095 4843 56 1156 6096 807 57 7150 6097 5045 58 3221 6098 59 7475 6099 1054 60 1338 6100 4436 61 5708 6101 2067 62 1567 6102 5126 63 7767 6103 2104 64 1917 6104 5069 65 8469 6105 3458 66 2536 6106 5159 67 2134 6107 4511 68 3303 6108 1685 69 4335 6109 3242 70 7309 6110 2226 71 4143 6111 2293 72 3593 6112 4076 73 355 6113 1640 74 5358 6114 422 75 3591 6115 5311 76 8205 6116 5414 77 8710 6117 5436 78 5344 6118 2398 79 604 6119 2441 80 7207 6120 324 81 4446 6121 5606 82 7597 6122 1315 83 8151 6123 3996 84 1052 6124 1642 85 5940 6125 2518 86 7224 6126 5695 87 3929 6127 5799 88 2840 6128 2609 89 5196 6129 550 90 876 6130 3174 91 770 6131 5054 92 5769 6132 612 93 702 6133 4303 94 7557 6134 5803 95 8860 6135 3873 96 8575 6136 2671 97 4405 6137 3768 98 8308 6138 5860 99 8903 6139 5959 100 533 6140 6013 101 8966 6141 
[0.590] Exiting.

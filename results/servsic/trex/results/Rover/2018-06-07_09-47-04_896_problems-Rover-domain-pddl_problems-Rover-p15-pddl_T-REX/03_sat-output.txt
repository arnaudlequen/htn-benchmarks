Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.001] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.003] Parsed head comment information.
[0.200] Processed problem encoding.
[0.355] Calculated possible fact changes of composite elements.
[0.357] Initialized instantiation procedure.
[0.357] 
[0.357] *************************************
[0.357] * * *   M a k e s p a n     0   * * *
[0.357] *************************************
[0.360] Instantiated 11,903 initial clauses.
[0.360] The encoding contains a total of 6,713 distinct variables.
[0.360] Attempting solve with solver <glucose4> ...
c 20 assumptions
[0.360] Executed solver; result: UNSAT.
[0.360] 
[0.360] *************************************
[0.360] * * *   M a k e s p a n     1   * * *
[0.360] *************************************
[0.381] Computed next depth properties: array size of 77.
[0.407] Instantiated 39,173 transitional clauses.
[0.438] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.438] Instantiated 170,347 universal clauses.
[0.438] Instantiated and added clauses for a total of 221,423 clauses.
[0.438] The encoding contains a total of 47,421 distinct variables.
[0.438] Attempting solve with solver <glucose4> ...
c 77 assumptions
[0.438] Executed solver; result: UNSAT.
[0.438] 
[0.438] *************************************
[0.438] * * *   M a k e s p a n     2   * * *
[0.438] *************************************
[0.504] Computed next depth properties: array size of 140.
[0.632] Instantiated 246,287 transitional clauses.
[0.734] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.734] Instantiated 703,981 universal clauses.
[0.734] Instantiated and added clauses for a total of 1,171,691 clauses.
[0.734] The encoding contains a total of 173,595 distinct variables.
[0.734] Attempting solve with solver <glucose4> ...
c 140 assumptions
[0.734] Executed solver; result: UNSAT.
[0.734] 
[0.734] *************************************
[0.734] * * *   M a k e s p a n     3   * * *
[0.734] *************************************
[0.846] Computed next depth properties: array size of 247.
[1.139] Instantiated 669,539 transitional clauses.
[1.379] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.379] Instantiated 2,385,131 universal clauses.
[1.379] Instantiated and added clauses for a total of 4,226,361 clauses.
[1.379] The encoding contains a total of 357,579 distinct variables.
[1.379] Attempting solve with solver <glucose4> ...
c 247 assumptions
[1.710] Executed solver; result: UNSAT.
[1.710] 
[1.710] *************************************
[1.710] * * *   M a k e s p a n     4   * * *
[1.710] *************************************
[1.866] Computed next depth properties: array size of 379.
[2.278] Instantiated 958,740 transitional clauses.
[2.740] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.740] Instantiated 5,830,339 universal clauses.
[2.740] Instantiated and added clauses for a total of 11,015,440 clauses.
[2.740] The encoding contains a total of 563,453 distinct variables.
[2.740] Attempting solve with solver <glucose4> ...
c 379 assumptions
c last restart ## conflicts  :  162 903 
[3.799] Executed solver; result: SAT.
[3.799] Solver returned SAT; a solution has been found at makespan 4.
243
solution 2723 1
73 233 7 171 8 74 703 43 210 7 173 8 44 814 37 361 11 331 12 38 726 69 386 70 893 63 225 7 172 8 64 684 713 53 217 7 173 8 54 831 35 651 33 642 15 624 16 34 36 711 45 660 15 623 16 46 1036 57 141 9 93 10 58 718 55 137 9 95 10 56 779 63 225 7 176 73 234 74 8 64 684 691 1070 11 330 37 364 38 12 685 698 19 344 37 362 38 20 860 29 492 7 470 8 30 1091 19 485 7 474 8 20 1233 9 549 10 1112 1375 55 584 56 688 1097 31 565 32 1291 33 643 35 648 3 610 4 36 34 689 1108 41 657 3 606 4 42 1352 11 182 12 684 1100 37 207 38 1193 55 580 56 688 1087 1374 11 616 12 2708 3 606 11 617 12 4 1718 7 614 8 2528 11 616 12 2708 1930 3 609 35 651 36 4 2539 33 643 35 648 36 34 2708 3 609 35 651 36 4 2234 33 642 15 624 16 34 2549 59 298 60 2634 1402 2362 63 519 64 2684 7 469 8 1845 81 462 82 2506 45 660 15 623 16 46 2723 33 643 34 1624 35 649 13 622 14 36 2526 
[3.801] Exiting.

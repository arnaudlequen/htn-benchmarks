
Compact plan:
0: (visit waypoint1)
1: (navigate rover0 waypoint1 waypoint2)
2: (visit waypoint2)
3: (navigate rover0 waypoint2 waypoint3)
4: (unvisit waypoint2)
5: (unvisit waypoint1)
6: (sample_soil rover0 rover0store waypoint3)
7: (communicate_soil_data rover0 general waypoint3 waypoint7)
8: (visit waypoint4)
9: (navigate rover1 waypoint4 waypoint6)
10: (unvisit waypoint4)
11: (sample_soil rover1 rover1store waypoint6)
12: (visit waypoint6)
13: (navigate rover1 waypoint6 waypoint8)
14: (unvisit waypoint6)
15: (communicate_soil_data rover1 general waypoint6 waypoint8 waypoint7)
16: (visit waypoint8)
17: (navigate rover1 waypoint8 waypoint6)
18: (visit waypoint6)
19: (navigate rover1 waypoint6 waypoint4)
20: (visit waypoint4)
21: (navigate rover1 waypoint4 waypoint5)
22: (unvisit waypoint4)
23: (unvisit waypoint6)
24: (unvisit waypoint8)
25: (drop rover1 rover1store)
26: (sample_rock rover1 rover1store waypoint5)
27: (communicate_rock_data rover1 general waypoint5 waypoint7)
28: (visit waypoint5)
29: (navigate rover1 waypoint5 waypoint4)
30: (unvisit waypoint5)
31: (drop rover1 rover1store)
32: (sample_rock rover1 rover1store waypoint4)
33: (visit waypoint4)
34: (navigate rover1 waypoint4 waypoint6)
35: (visit waypoint6)
36: (navigate rover1 waypoint6 waypoint8)
37: (unvisit waypoint6)
38: (unvisit waypoint4)
39: (communicate_rock_data rover1 general waypoint4 waypoint8 waypoint7)
40: (drop rover1 rover1store)
41: (sample_rock rover1 rover1store waypoint8)
42: (communicate_rock_data rover1 general waypoint8 waypoint7)
43: (calibrate rover3 camera0 objective2 waypoint2)
44: (visit waypoint2)
45: (navigate rover3 waypoint2 waypoint6)
46: (unvisit waypoint2)
47: (take_image rover3 waypoint6 objective0 camera0 colour)
48: (visit waypoint6)
49: (navigate rover3 waypoint6 waypoint8)
50: (unvisit waypoint6)
51: (communicate_image_data rover3 general objective0 colour waypoint8 waypoint7)
52: (visit waypoint8)
53: (navigate rover3 waypoint8 waypoint6)
54: (visit waypoint6)
55: (navigate rover3 waypoint6 waypoint0)
56: (unvisit waypoint6)
57: (unvisit waypoint8)
58: (calibrate rover3 camera0 objective2 waypoint0)
59: (visit waypoint0)
60: (navigate rover3 waypoint0 waypoint6)
61: (visit waypoint6)
62: (navigate rover3 waypoint6 waypoint2)
63: (visit waypoint2)
64: (navigate rover3 waypoint2 waypoint1)
65: (unvisit waypoint2)
66: (unvisit waypoint6)
67: (unvisit waypoint0)
68: (take_image rover3 waypoint1 objective2 camera0 low_res)
69: (communicate_image_data rover3 general objective2 low_res waypoint1 waypoint7)
70: (visit waypoint3)
71: (navigate rover0 waypoint3 waypoint2)
72: (visit waypoint2)
73: (navigate rover0 waypoint2 waypoint1)
74: (unvisit waypoint2)
75: (unvisit waypoint3)
76: (calibrate rover0 camera4 objective3 waypoint1)
77: (take_image rover0 waypoint1 objective0 camera4 low_res)
78: (communicate_image_data rover0 general objective0 low_res waypoint1 waypoint7)




Compact plan:
0: (visit waypoint1)
1: (navigate rover0 waypoint1 waypoint0)
2: (unvisit waypoint1)
3: (sample_soil rover0 rover0store waypoint0)
4: (communicate_soil_data rover0 general waypoint0 waypoint1)
5: (drop rover0 rover0store)
6: (sample_rock rover0 rover0store waypoint0)
7: (communicate_rock_data rover0 general waypoint0 waypoint1)
8: (calibrate rover0 camera0 objective0 waypoint0)
9: (take_image rover0 waypoint0 objective1 camera0 low_res)
10: (communicate_image_data rover0 general objective1 low_res waypoint0 waypoint1)



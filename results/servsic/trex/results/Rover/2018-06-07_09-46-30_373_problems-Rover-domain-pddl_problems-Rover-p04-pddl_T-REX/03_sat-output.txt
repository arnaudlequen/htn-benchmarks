Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.002] Processed problem encoding.
[0.002] Calculated possible fact changes of composite elements.
[0.002] Initialized instantiation procedure.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     0   * * *
[0.002] *************************************
[0.002] Instantiated 330 initial clauses.
[0.002] The encoding contains a total of 204 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 8 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     1   * * *
[0.002] *************************************
[0.003] Computed next depth properties: array size of 29.
[0.003] Instantiated 314 transitional clauses.
[0.003] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.003] Instantiated 990 universal clauses.
[0.004] Instantiated and added clauses for a total of 1,634 clauses.
[0.004] The encoding contains a total of 522 distinct variables.
[0.004] Attempting solve with solver <glucose4> ...
c 29 assumptions
[0.004] Executed solver; result: UNSAT.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     2   * * *
[0.004] *************************************
[0.004] Computed next depth properties: array size of 53.
[0.005] Instantiated 754 transitional clauses.
[0.006] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.006] Instantiated 2,223 universal clauses.
[0.006] Instantiated and added clauses for a total of 4,611 clauses.
[0.006] The encoding contains a total of 1,062 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 53 assumptions
c last restart ## conflicts  :  1 70 
[0.006] Executed solver; result: SAT.
[0.006] Solver returned SAT; a solution has been found at makespan 2.
37
solution 84 1
7 15 8 23 29 3 17 4 22 24 30 31 35 7 11 8 21 32 36 79 3 13 4 46 65 82 9 19 10 58 76 84 3 16 4 37 69 
[0.006] Exiting.

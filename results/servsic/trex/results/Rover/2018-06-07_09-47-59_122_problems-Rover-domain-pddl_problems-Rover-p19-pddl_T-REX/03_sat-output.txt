Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.010] Parsed head comment information.
[0.572] Processed problem encoding.
[1.700] Calculated possible fact changes of composite elements.
[1.712] Initialized instantiation procedure.
[1.712] 
[1.712] *************************************
[1.712] * * *   M a k e s p a n     0   * * *
[1.712] *************************************
[1.721] Instantiated 36,699 initial clauses.
[1.721] The encoding contains a total of 19,951 distinct variables.
[1.721] Attempting solve with solver <glucose4> ...
c 36 assumptions
[1.721] Executed solver; result: UNSAT.
[1.721] 
[1.721] *************************************
[1.721] * * *   M a k e s p a n     1   * * *
[1.721] *************************************
[1.837] Computed next depth properties: array size of 141.
[1.938] Instantiated 107,982 transitional clauses.
[2.071] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.071] Instantiated 576,252 universal clauses.
[2.071] Instantiated and added clauses for a total of 720,933 clauses.
[2.071] The encoding contains a total of 136,135 distinct variables.
[2.071] Attempting solve with solver <glucose4> ...
c 141 assumptions
[2.071] Executed solver; result: UNSAT.
[2.071] 
[2.071] *************************************
[2.071] * * *   M a k e s p a n     2   * * *
[2.071] *************************************
[2.390] Computed next depth properties: array size of 252.
[2.862] Instantiated 735,829 transitional clauses.
[3.236] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.236] Instantiated 2,078,122 universal clauses.
[3.236] Instantiated and added clauses for a total of 3,534,884 clauses.
[3.236] The encoding contains a total of 528,620 distinct variables.
[3.236] Attempting solve with solver <glucose4> ...
c 252 assumptions
[3.236] Executed solver; result: UNSAT.
[3.236] 
[3.236] *************************************
[3.236] * * *   M a k e s p a n     3   * * *
[3.236] *************************************
[3.829] Computed next depth properties: array size of 439.
[4.972] Instantiated 2,106,577 transitional clauses.
[6.079] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[6.079] Instantiated 11,974,870 universal clauses.
[6.079] Instantiated and added clauses for a total of 17,616,331 clauses.
[6.079] The encoding contains a total of 1,118,139 distinct variables.
[6.079] Attempting solve with solver <glucose4> ...
c 439 assumptions
[6.385] Executed solver; result: UNSAT.
[6.385] 
[6.385] *************************************
[6.385] * * *   M a k e s p a n     4   * * *
[6.385] *************************************
[7.274] Computed next depth properties: array size of 667.
[8.947] Instantiated 3,102,207 transitional clauses.
[11.358] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[11.358] Instantiated 31,843,050 universal clauses.
[11.358] Instantiated and added clauses for a total of 52,561,588 clauses.
[11.358] The encoding contains a total of 1,775,889 distinct variables.
[11.358] Attempting solve with solver <glucose4> ...
c 667 assumptions
c last restart ## conflicts  :  250 2786 
[13.953] Executed solver; result: SAT.
[13.953] Solver returned SAT; a solution has been found at makespan 4.
511
solution 6092 1
89 796 45 755 46 90 1565 75 785 45 757 46 76 1808 89 798 69 780 70 90 1527 1577 95 804 69 777 70 96 1826 11 254 107 346 108 12 1552 67 313 107 345 108 68 1649 43 860 5 825 6 44 1584 2442 75 1253 117 1175 45 1226 46 118 76 1615 115 1288 45 1220 46 116 2152 99 1503 5 1415 6 100 1593 101 1510 5 1413 6 102 2391 89 1490 5 1414 99 1509 100 6 90 1533 1575 121 1495 122 2364 117 1178 75 1254 76 118 1531 1537 7 1184 75 1256 76 8 2029 35 739 69 779 89 794 90 70 36 1527 1541 15 721 89 800 90 16 1773 113 817 89 796 45 758 46 90 114 1527 1601 105 809 45 747 46 106 1862 83 1378 35 1325 13 1299 14 36 84 1550 59 1350 13 1300 14 60 2187 99 1508 61 1470 62 100 1533 1599 103 1512 61 1469 62 104 2403 41 1216 42 1531 1561 71 1251 72 2069 99 1504 7 1421 8 100 1533 1611 113 1521 114 2417 109 465 41 394 42 110 2487 61 416 62 2736 117 701 45 757 46 118 1527 2524 89 793 5 709 6 90 3080 61 1001 21 962 13 948 14 22 62 2483 57 998 13 947 14 58 3163 21 147 3 126 4 22 2507 81 200 3 128 4 82 2629 53 1095 77 1119 17 1066 18 78 54 2561 109 1167 17 1065 18 110 3420 91 215 3 125 21 155 22 4 92 1523 2542 3573 35 1450 113 1519 7 1420 8 114 36 1533 2555 107 1514 7 1421 8 108 3559 105 228 106 1523 2458 21 154 22 2566 41 400 109 467 110 42 1525 2536 3576 103 458 109 463 11 364 12 110 104 1525 2494 73 423 11 365 12 74 2748 95 677 25 601 49 638 50 26 96 2502 75 662 49 639 50 76 2906 89 211 21 147 3 129 4 22 90 1523 2528 97 220 3 128 4 98 2655 91 437 11 360 13 370 14 12 92 1525 2473 3574 35 390 13 369 11 361 12 14 36 1525 2466 33 384 11 365 12 34 2709 91 215 3 127 4 92 1523 2514 85 204 86 2634 119 266 107 343 108 120 5829 7 246 107 344 11 248 12 108 8 4235 5511 113 1521 114 6092 35 1450 113 1519 114 36 5036 7 1419 8 5802 79 542 5 473 6 80 5900 31 491 5 476 99 562 100 6 32 4226 95 557 96 5585 99 1155 100 6033 17 1064 77 1123 78 18 5421 53 1097 54 5751 91 437 11 360 12 92 5842 13 369 11 366 12 14 4991 5557 5826 117 241 118 3852 63 307 64 5501 
[13.960] Exiting.

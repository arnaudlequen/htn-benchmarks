Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 155 initial clauses.
[0.001] The encoding contains a total of 98 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 13.
[0.002] Instantiated 92 transitional clauses.
[0.002] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.002] Instantiated 351 universal clauses.
[0.002] Instantiated and added clauses for a total of 598 clauses.
[0.002] The encoding contains a total of 212 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     2   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 23.
[0.002] Instantiated 241 transitional clauses.
[0.003] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.003] Instantiated 745 universal clauses.
[0.003] Instantiated and added clauses for a total of 1,584 clauses.
[0.003] The encoding contains a total of 413 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 23 assumptions
c last restart ## conflicts  :  0 29 
[0.003] Executed solver; result: SAT.
[0.003] Solver returned SAT; a solution has been found at makespan 2.
10
solution 37 1
21 25 7 16 8 27 29 37 31 34 
[0.003] Exiting.

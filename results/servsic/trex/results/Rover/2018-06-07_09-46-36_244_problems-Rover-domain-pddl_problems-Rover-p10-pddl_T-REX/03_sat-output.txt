Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.002] Parsed head comment information.
[0.105] Processed problem encoding.
[0.153] Calculated possible fact changes of composite elements.
[0.154] Initialized instantiation procedure.
[0.154] 
[0.154] *************************************
[0.154] * * *   M a k e s p a n     0   * * *
[0.154] *************************************
[0.156] Instantiated 6,803 initial clauses.
[0.156] The encoding contains a total of 3,892 distinct variables.
[0.156] Attempting solve with solver <glucose4> ...
c 21 assumptions
[0.156] Executed solver; result: UNSAT.
[0.156] 
[0.156] *************************************
[0.156] * * *   M a k e s p a n     1   * * *
[0.156] *************************************
[0.166] Computed next depth properties: array size of 81.
[0.177] Instantiated 17,368 transitional clauses.
[0.192] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.192] Instantiated 73,343 universal clauses.
[0.192] Instantiated and added clauses for a total of 97,514 clauses.
[0.192] The encoding contains a total of 21,780 distinct variables.
[0.192] Attempting solve with solver <glucose4> ...
c 81 assumptions
[0.192] Executed solver; result: UNSAT.
[0.192] 
[0.192] *************************************
[0.192] * * *   M a k e s p a n     2   * * *
[0.192] *************************************
[0.221] Computed next depth properties: array size of 146.
[0.267] Instantiated 103,866 transitional clauses.
[0.311] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.311] Instantiated 301,476 universal clauses.
[0.311] Instantiated and added clauses for a total of 502,856 clauses.
[0.311] The encoding contains a total of 78,311 distinct variables.
[0.311] Attempting solve with solver <glucose4> ...
c 146 assumptions
[0.311] Executed solver; result: UNSAT.
[0.311] 
[0.311] *************************************
[0.311] * * *   M a k e s p a n     3   * * *
[0.311] *************************************
[0.361] Computed next depth properties: array size of 256.
[0.481] Instantiated 285,686 transitional clauses.
[0.575] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.575] Instantiated 826,638 universal clauses.
[0.575] Instantiated and added clauses for a total of 1,615,180 clauses.
[0.575] The encoding contains a total of 160,131 distinct variables.
[0.575] Attempting solve with solver <glucose4> ...
c 256 assumptions
[0.592] Executed solver; result: UNSAT.
[0.592] 
[0.592] *************************************
[0.592] * * *   M a k e s p a n     4   * * *
[0.592] *************************************
[0.672] Computed next depth properties: array size of 391.
[0.844] Instantiated 406,941 transitional clauses.
[1.014] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.014] Instantiated 1,676,159 universal clauses.
[1.014] Instantiated and added clauses for a total of 3,698,280 clauses.
[1.014] The encoding contains a total of 251,609 distinct variables.
[1.014] Attempting solve with solver <glucose4> ...
c 391 assumptions
c last restart ## conflicts  :  79 713 
[1.605] Executed solver; result: SAT.
[1.605] Solver returned SAT; a solution has been found at makespan 4.
276
solution 1793 1
7 206 3 200 9 213 10 4 8 445 17 218 18 516 9 211 3 204 4 10 437 457 47 243 3 200 4 48 548 9 212 10 437 442 15 217 16 508 45 95 46 447 23 69 13 60 14 24 475 9 211 3 199 7 208 8 4 10 437 451 27 227 7 209 8 28 536 9 356 10 455 41 382 42 587 7 157 27 173 9 160 10 28 8 603 15 166 16 640 33 275 5 248 6 34 621 29 270 30 810 37 235 7 207 25 225 26 8 38 437 636 45 242 25 222 26 46 771 5 249 6 438 625 33 281 31 271 32 34 818 7 250 31 273 32 8 438 617 25 268 31 271 32 26 803 33 424 34 614 21 413 33 422 34 22 866 7 209 37 236 38 8 437 608 19 219 20 722 9 161 27 174 28 10 436 631 43 192 27 173 28 44 694 9 399 33 423 17 409 18 34 10 440 630 917 1783 9 351 13 361 14 10 1120 23 370 13 363 14 24 1654 31 374 32 1785 13 359 14 952 9 350 3 347 4 10 1648 47 387 3 344 4 48 1784 11 357 3 343 4 12 1187 9 350 3 342 4 10 1658 7 349 3 344 4 8 1784 11 357 3 342 4 12 1387 7 349 3 345 4 8 1677 1793 29 373 30 1276 3 342 4 1666 
[1.607] Exiting.

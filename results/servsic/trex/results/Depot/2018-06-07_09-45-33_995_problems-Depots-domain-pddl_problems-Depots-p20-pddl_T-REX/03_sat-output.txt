Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.004] Parsed head comment information.
[0.488] Processed problem encoding.
[1.046] Calculated possible fact changes of composite elements.
[1.116] Initialized instantiation procedure.
[1.116] 
[1.116] *************************************
[1.116] * * *   M a k e s p a n     0   * * *
[1.116] *************************************
[1.131] Instantiated 43,263 initial clauses.
[1.131] The encoding contains a total of 34,827 distinct variables.
[1.131] Attempting solve with solver <glucose4> ...
c 15 assumptions
[1.132] Executed solver; result: UNSAT.
[1.132] 
[1.132] *************************************
[1.132] * * *   M a k e s p a n     1   * * *
[1.132] *************************************
[1.161] Computed next depth properties: array size of 57.
[1.234] Instantiated 126,829 transitional clauses.
[1.303] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.303] Instantiated 585,667 universal clauses.
[1.303] Instantiated and added clauses for a total of 755,759 clauses.
[1.303] The encoding contains a total of 74,851 distinct variables.
[1.303] Attempting solve with solver <glucose4> ...
c 57 assumptions
[1.303] Executed solver; result: UNSAT.
[1.303] 
[1.303] *************************************
[1.303] * * *   M a k e s p a n     2   * * *
[1.303] *************************************
[1.395] Computed next depth properties: array size of 169.
[1.683] Instantiated 339,193 transitional clauses.
[2.041] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.041] Instantiated 4,035,489 universal clauses.
[2.041] Instantiated and added clauses for a total of 5,130,441 clauses.
[2.041] The encoding contains a total of 238,109 distinct variables.
[2.041] Attempting solve with solver <glucose4> ...
c 169 assumptions
[2.041] Executed solver; result: UNSAT.
[2.041] 
[2.041] *************************************
[2.041] * * *   M a k e s p a n     3   * * *
[2.041] *************************************
[2.264] Computed next depth properties: array size of 337.
[3.693] Instantiated 1,883,372 transitional clauses.
[5.570] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[5.570] Instantiated 27,170,700 universal clauses.
[5.570] Instantiated and added clauses for a total of 34,184,513 clauses.
[5.570] The encoding contains a total of 596,482 distinct variables.
[5.570] Attempting solve with solver <glucose4> ...
c 337 assumptions
[5.570] Executed solver; result: UNSAT.
[5.570] 
[5.570] *************************************
[5.570] * * *   M a k e s p a n     4   * * *
[5.570] *************************************
[5.941] Computed next depth properties: array size of 505.
[8.874] Instantiated 3,841,223 transitional clauses.
[13.882] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[13.882] Instantiated 76,896,461 universal clauses.
[13.882] Instantiated and added clauses for a total of 114,922,197 clauses.
[13.882] The encoding contains a total of 1,055,417 distinct variables.
[13.882] Attempting solve with solver <glucose4> ...
c 505 assumptions
c last restart ## conflicts  :  55 590 
[14.248] Executed solver; result: SAT.
[14.248] Solver returned SAT; a solution has been found at makespan 4.
83
solution 2540 1
2540 480 690 1866 487 677 646 325 648 317 65 348 1439 792 403 171 30 346 1629 360 609 1508 563 318 139 22 372 2044 358 1033 1026 1030 509 209 38 401 1276 1120 1066 1070 538 776 496 778 69 3 346 557 558 320 109 16 412 203 37 405 254 53 332 1151 674 364 1359 964 921 427 925 248 48 99 9 442 103 12 477 304 63 499 211 39 506 237 47 
[14.258] Exiting.

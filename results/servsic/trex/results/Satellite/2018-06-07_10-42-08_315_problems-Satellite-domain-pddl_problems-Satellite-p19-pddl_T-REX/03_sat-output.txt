Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.028] Parsed head comment information.
[1.266] Processed problem encoding.
[2.058] Calculated possible fact changes of composite elements.
[2.221] Initialized instantiation procedure.
[2.221] 
[2.221] *************************************
[2.221] * * *   M a k e s p a n     0   * * *
[2.221] *************************************
[2.783] Instantiated 413,883 initial clauses.
[2.783] The encoding contains a total of 208,881 distinct variables.
[2.783] Attempting solve with solver <glucose4> ...
c 188 assumptions
[2.786] Executed solver; result: UNSAT.
[2.786] 
[2.786] *************************************
[2.786] * * *   M a k e s p a n     1   * * *
[2.786] *************************************
[3.643] Computed next depth properties: array size of 372.
[4.322] Instantiated 6,800 transitional clauses.
[6.035] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[6.035] Instantiated 893,243 universal clauses.
[6.035] Instantiated and added clauses for a total of 1,313,926 clauses.
[6.035] The encoding contains a total of 415,015 distinct variables.
[6.035] Attempting solve with solver <glucose4> ...
c 372 assumptions
[6.035] Executed solver; result: UNSAT.
[6.035] 
[6.035] *************************************
[6.035] * * *   M a k e s p a n     2   * * *
[6.035] *************************************
[7.722] Computed next depth properties: array size of 556.
[9.481] Instantiated 1,467,443 transitional clauses.
[19.408] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[19.408] Instantiated 116,409,983 universal clauses.
[19.408] Instantiated and added clauses for a total of 119,191,352 clauses.
[19.408] The encoding contains a total of 1,509,864 distinct variables.
[19.408] Attempting solve with solver <glucose4> ...
c 556 assumptions
[19.409] Executed solver; result: UNSAT.
[19.409] 
[19.409] *************************************
[19.409] * * *   M a k e s p a n     3   * * *
[19.409] *************************************
[21.822] Computed next depth properties: array size of 924.
[24.815] Instantiated 3,991,658 transitional clauses.
Interrupt signal (15) received.

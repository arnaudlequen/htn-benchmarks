Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.002] Parsed head comment information.
[0.119] Processed problem encoding.
[0.136] Calculated possible fact changes of composite elements.
[0.139] Initialized instantiation procedure.
[0.139] 
[0.139] *************************************
[0.139] * * *   M a k e s p a n     0   * * *
[0.139] *************************************
[0.152] Instantiated 21,758 initial clauses.
[0.152] The encoding contains a total of 11,240 distinct variables.
[0.152] Attempting solve with solver <glucose4> ...
c 46 assumptions
[0.152] Executed solver; result: UNSAT.
[0.152] 
[0.152] *************************************
[0.152] * * *   M a k e s p a n     1   * * *
[0.152] *************************************
[0.164] Computed next depth properties: array size of 89.
[0.173] Instantiated 1,314 transitional clauses.
[0.198] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.198] Instantiated 46,646 universal clauses.
[0.199] Instantiated and added clauses for a total of 69,718 clauses.
[0.199] The encoding contains a total of 22,035 distinct variables.
[0.199] Attempting solve with solver <glucose4> ...
c 89 assumptions
[0.199] Executed solver; result: UNSAT.
[0.199] 
[0.199] *************************************
[0.199] * * *   M a k e s p a n     2   * * *
[0.199] *************************************
[0.228] Computed next depth properties: array size of 132.
[0.268] Instantiated 73,995 transitional clauses.
[0.385] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.385] Instantiated 1,165,725 universal clauses.
[0.385] Instantiated and added clauses for a total of 1,309,438 clauses.
[0.385] The encoding contains a total of 76,129 distinct variables.
[0.385] Attempting solve with solver <glucose4> ...
c 132 assumptions
[0.385] Executed solver; result: UNSAT.
[0.385] 
[0.385] *************************************
[0.385] * * *   M a k e s p a n     3   * * *
[0.385] *************************************
[0.421] Computed next depth properties: array size of 218.
[0.495] Instantiated 197,867 transitional clauses.
[0.616] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.616] Instantiated 1,128,848 universal clauses.
[0.616] Instantiated and added clauses for a total of 2,636,153 clauses.
[0.616] The encoding contains a total of 98,254 distinct variables.
[0.616] Attempting solve with solver <glucose4> ...
c 218 assumptions
[0.617] Executed solver; result: UNSAT.
[0.617] 
[0.617] *************************************
[0.617] * * *   M a k e s p a n     4   * * *
[0.617] *************************************
[0.663] Computed next depth properties: array size of 304.
[0.710] Instantiated 23,654 transitional clauses.
[0.866] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.866] Instantiated 1,171,027 universal clauses.
[0.866] Instantiated and added clauses for a total of 3,830,834 clauses.
[0.866] The encoding contains a total of 129,468 distinct variables.
[0.866] Attempting solve with solver <glucose4> ...
c 304 assumptions
[0.867] Executed solver; result: UNSAT.
[0.867] 
[0.867] *************************************
[0.867] * * *   M a k e s p a n     5   * * *
[0.867] *************************************
[0.949] Computed next depth properties: array size of 390.
[1.058] Instantiated 173,529 transitional clauses.
[1.513] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.513] Instantiated 5,228,642 universal clauses.
[1.513] Instantiated and added clauses for a total of 9,233,005 clauses.
[1.513] The encoding contains a total of 253,312 distinct variables.
[1.513] Attempting solve with solver <glucose4> ...
c 390 assumptions
c last restart ## conflicts  :  92 724 
[1.683] Executed solver; result: SAT.
[1.683] Solver returned SAT; a solution has been found at makespan 5.
187
solution 9497 1
285 8218 297 8265 7 280 3538 292 3636 11 278 1145 290 1347 16 1404 23 1457 29 1510 35 8578 45 282 5830 294 6287 50 1668 51 1724 56 303 283 5881 295 6445 65 304 282 5837 294 6495 73 1885 77 1941 83 1994 88 299 277 1214 289 2025 94 6776 105 303 283 5896 295 6809 110 304 282 5845 294 6859 116 298 278 1163 290 2231 119 2312 127 299 277 1220 289 2337 134 301 281 3534 293 4728 146 302 280 3562 292 4780 152 301 281 3563 293 4832 158 286 356 288 776 160 298 278 1169 290 2595 168 2683 173 9209 183 303 283 5898 295 7433 188 299 277 1227 289 2805 191 304 282 5857 294 7535 204 2947 206 9497 217 966 218 5437 231 302 280 3575 292 5456 238 301 279 3524 291 5507 244 300 281 3577 293 5560 251 302 280 3578 292 5612 255 303 283 5911 295 8005 262 301 281 3579 293 5716 269 302 279 3529 291 5767 273 3157 5610 
[1.691] Exiting.

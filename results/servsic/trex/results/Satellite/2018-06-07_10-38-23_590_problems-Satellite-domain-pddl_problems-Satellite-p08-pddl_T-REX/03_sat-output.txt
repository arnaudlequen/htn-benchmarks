Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.017] Processed problem encoding.
[0.019] Calculated possible fact changes of composite elements.
[0.020] Initialized instantiation procedure.
[0.020] 
[0.020] *************************************
[0.020] * * *   M a k e s p a n     0   * * *
[0.020] *************************************
[0.021] Instantiated 2,851 initial clauses.
[0.021] The encoding contains a total of 1,541 distinct variables.
[0.021] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.021] Executed solver; result: UNSAT.
[0.021] 
[0.021] *************************************
[0.021] * * *   M a k e s p a n     1   * * *
[0.021] *************************************
[0.022] Computed next depth properties: array size of 27.
[0.022] Instantiated 222 transitional clauses.
[0.025] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.025] Instantiated 5,455 universal clauses.
[0.025] Instantiated and added clauses for a total of 8,528 clauses.
[0.025] The encoding contains a total of 2,894 distinct variables.
[0.025] Attempting solve with solver <glucose4> ...
c 27 assumptions
[0.025] Executed solver; result: UNSAT.
[0.025] 
[0.025] *************************************
[0.025] * * *   M a k e s p a n     2   * * *
[0.025] *************************************
[0.028] Computed next depth properties: array size of 40.
[0.032] Instantiated 8,133 transitional clauses.
[0.040] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.040] Instantiated 59,114 universal clauses.
[0.040] Instantiated and added clauses for a total of 75,775 clauses.
[0.040] The encoding contains a total of 9,366 distinct variables.
[0.040] Attempting solve with solver <glucose4> ...
c 40 assumptions
[0.040] Executed solver; result: UNSAT.
[0.040] 
[0.040] *************************************
[0.040] * * *   M a k e s p a n     3   * * *
[0.040] *************************************
[0.043] Computed next depth properties: array size of 66.
[0.050] Instantiated 24,769 transitional clauses.
[0.057] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.057] Instantiated 55,986 universal clauses.
[0.057] Instantiated and added clauses for a total of 156,530 clauses.
[0.057] The encoding contains a total of 12,261 distinct variables.
[0.057] Attempting solve with solver <glucose4> ...
c 66 assumptions
[0.057] Executed solver; result: UNSAT.
[0.057] 
[0.057] *************************************
[0.057] * * *   M a k e s p a n     4   * * *
[0.057] *************************************
[0.060] Computed next depth properties: array size of 92.
[0.063] Instantiated 3,146 transitional clauses.
[0.072] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.072] Instantiated 61,311 universal clauses.
[0.072] Instantiated and added clauses for a total of 220,987 clauses.
[0.072] The encoding contains a total of 16,231 distinct variables.
[0.072] Attempting solve with solver <glucose4> ...
c 92 assumptions
[0.072] Executed solver; result: UNSAT.
[0.072] 
[0.072] *************************************
[0.072] * * *   M a k e s p a n     5   * * *
[0.072] *************************************
[0.078] Computed next depth properties: array size of 118.
[0.087] Instantiated 19,417 transitional clauses.
[0.112] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.112] Instantiated 235,168 universal clauses.
[0.112] Instantiated and added clauses for a total of 475,572 clauses.
[0.112] The encoding contains a total of 31,015 distinct variables.
[0.112] Attempting solve with solver <glucose4> ...
c 118 assumptions
c last restart ## conflicts  :  5 154 
[0.116] Executed solver; result: SAT.
[0.116] Solver returned SAT; a solution has been found at makespan 5.
67
solution 1250 1
57 101 70 143 2 82 59 98 72 167 6 84 58 124 71 192 9 83 59 101 72 215 14 247 17 66 1045 79 1082 24 84 57 103 70 287 26 68 1215 81 1250 35 82 59 108 72 335 36 84 57 111 70 359 39 401 45 82 59 113 72 407 49 84 58 138 71 432 52 
[0.117] Exiting.

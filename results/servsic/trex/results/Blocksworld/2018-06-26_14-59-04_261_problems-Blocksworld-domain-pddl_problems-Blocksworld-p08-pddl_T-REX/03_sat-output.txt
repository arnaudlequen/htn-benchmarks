Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.015] Processed problem encoding.
[0.019] Calculated possible fact changes of composite elements.
[0.020] Initialized instantiation procedure.
[0.020] 
[0.020] *************************************
[0.020] * * *   M a k e s p a n     0   * * *
[0.020] *************************************
[0.029] Instantiated 18,085 initial clauses.
[0.029] The encoding contains a total of 9,271 distinct variables.
[0.029] Attempting solve with solver <glucose4> ...
c 23 assumptions
[0.029] Executed solver; result: UNSAT.
[0.029] 
[0.029] *************************************
[0.029] * * *   M a k e s p a n     1   * * *
[0.029] *************************************
[0.032] Computed next depth properties: array size of 89.
[0.038] Instantiated 1,960 transitional clauses.
[0.058] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.058] Instantiated 61,469 universal clauses.
[0.058] Instantiated and added clauses for a total of 81,514 clauses.
[0.058] The encoding contains a total of 29,559 distinct variables.
[0.058] Attempting solve with solver <glucose4> ...
c 89 assumptions
[0.061] Executed solver; result: UNSAT.
[0.061] 
[0.061] *************************************
[0.061] * * *   M a k e s p a n     2   * * *
[0.061] *************************************
[0.071] Computed next depth properties: array size of 221.
[0.087] Instantiated 22,024 transitional clauses.
[0.137] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.137] Instantiated 197,715 universal clauses.
[0.137] Instantiated and added clauses for a total of 301,253 clauses.
[0.137] The encoding contains a total of 68,481 distinct variables.
[0.137] Attempting solve with solver <glucose4> ...
c 221 assumptions
[0.144] Executed solver; result: UNSAT.
[0.144] 
[0.144] *************************************
[0.144] * * *   M a k e s p a n     3   * * *
[0.144] *************************************
[0.160] Computed next depth properties: array size of 309.
[0.209] Instantiated 92,754 transitional clauses.
[0.433] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.433] Instantiated 2,726,395 universal clauses.
[0.433] Instantiated and added clauses for a total of 3,120,402 clauses.
[0.433] The encoding contains a total of 135,277 distinct variables.
[0.433] Attempting solve with solver <glucose4> ...
c 309 assumptions
[0.461] Executed solver; result: UNSAT.
[0.461] 
[0.461] *************************************
[0.461] * * *   M a k e s p a n     4   * * *
[0.461] *************************************
[0.479] Computed next depth properties: array size of 397.
[0.538] Instantiated 127,074 transitional clauses.
[0.958] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.958] Instantiated 5,524,751 universal clauses.
[0.958] Instantiated and added clauses for a total of 8,772,227 clauses.
[0.958] The encoding contains a total of 218,925 distinct variables.
[0.958] Attempting solve with solver <glucose4> ...
c 397 assumptions
[0.986] Executed solver; result: UNSAT.
[0.986] 
[0.986] *************************************
[0.986] * * *   M a k e s p a n     5   * * *
[0.986] *************************************
[1.005] Computed next depth properties: array size of 485.
[1.072] Instantiated 159,018 transitional clauses.
[1.681] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.681] Instantiated 8,323,107 universal clauses.
[1.681] Instantiated and added clauses for a total of 17,254,352 clauses.
[1.681] The encoding contains a total of 318,633 distinct variables.
[1.681] Attempting solve with solver <glucose4> ...
c 485 assumptions
[1.740] Executed solver; result: UNSAT.
[1.740] 
[1.740] *************************************
[1.740] * * *   M a k e s p a n     6   * * *
[1.740] *************************************
[1.769] Computed next depth properties: array size of 573.
[1.846] Instantiated 190,962 transitional clauses.
[2.650] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.650] Instantiated 11,121,463 universal clauses.
[2.650] Instantiated and added clauses for a total of 28,566,777 clauses.
[2.650] The encoding contains a total of 434,401 distinct variables.
[2.650] Attempting solve with solver <glucose4> ...
c 573 assumptions
c last restart ## conflicts  :  1 622 
[2.708] Executed solver; result: SAT.
[2.708] Solver returned SAT; a solution has been found at makespan 6.
68
solution 397 1
260 251 185 175 230 213 350 346 84 80 372 373 242 232 207 194 273 270 76 61 316 308 139 137 155 377 386 387 392 393 378 379 382 383 368 369 384 385 9 4 119 118 39 23 342 327 390 391 103 99 370 371 372 373 364 365 388 389 366 367 380 381 396 397 374 375 394 395 
[2.711] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.050] Processed problem encoding.
[0.087] Calculated possible fact changes of composite elements.
[0.089] Initialized instantiation procedure.
[0.089] 
[0.089] *************************************
[0.089] * * *   M a k e s p a n     0   * * *
[0.089] *************************************
[0.115] Instantiated 77,580 initial clauses.
[0.115] The encoding contains a total of 39,408 distinct variables.
[0.115] Attempting solve with solver <glucose4> ...
c 34 assumptions
[0.116] Executed solver; result: UNSAT.
[0.116] 
[0.116] *************************************
[0.116] * * *   M a k e s p a n     1   * * *
[0.116] *************************************
[0.123] Computed next depth properties: array size of 133.
[0.139] Instantiated 4,787 transitional clauses.
[0.191] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.191] Instantiated 251,593 universal clauses.
[0.191] Instantiated and added clauses for a total of 333,960 clauses.
[0.191] The encoding contains a total of 122,506 distinct variables.
[0.191] Attempting solve with solver <glucose4> ...
c 133 assumptions
[0.210] Executed solver; result: UNSAT.
[0.210] 
[0.210] *************************************
[0.210] * * *   M a k e s p a n     2   * * *
[0.210] *************************************
[0.242] Computed next depth properties: array size of 331.
[0.329] Instantiated 87,551 transitional clauses.
[0.507] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.507] Instantiated 835,264 universal clauses.
[0.507] Instantiated and added clauses for a total of 1,256,775 clauses.
[0.507] The encoding contains a total of 284,375 distinct variables.
[0.507] Attempting solve with solver <glucose4> ...
c 331 assumptions
[0.545] Executed solver; result: UNSAT.
[0.545] 
[0.545] *************************************
[0.545] * * *   M a k e s p a n     3   * * *
[0.545] *************************************
[0.609] Computed next depth properties: array size of 463.
[0.931] Instantiated 423,722 transitional clauses.
[3.475] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.475] Instantiated 36,435,598 universal clauses.
[3.475] Instantiated and added clauses for a total of 38,116,095 clauses.
[3.475] The encoding contains a total of 579,531 distinct variables.
[3.475] Attempting solve with solver <glucose4> ...
c 463 assumptions
[3.675] Executed solver; result: UNSAT.
[3.675] 
[3.675] *************************************
[3.675] * * *   M a k e s p a n     4   * * *
[3.675] *************************************
[3.763] Computed next depth properties: array size of 595.
[4.134] Instantiated 574,070 transitional clauses.
[9.339] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[9.339] Instantiated 74,248,252 universal clauses.
[9.339] Instantiated and added clauses for a total of 112,938,417 clauses.
[9.339] The encoding contains a total of 948,937 distinct variables.
[9.339] Attempting solve with solver <glucose4> ...
c 595 assumptions
[9.474] Executed solver; result: UNSAT.
[9.474] 
[9.475] *************************************
[9.475] * * *   M a k e s p a n     5   * * *
[9.475] *************************************
[9.576] Computed next depth properties: array size of 727.
[10.001] Instantiated 718,082 transitional clauses.
[18.019] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[18.019] Instantiated 112,060,906 universal clauses.
[18.019] Instantiated and added clauses for a total of 225,717,405 clauses.
[18.019] The encoding contains a total of 1,390,481 distinct variables.
[18.019] Attempting solve with solver <glucose4> ...
c 727 assumptions
c last restart ## conflicts  :  4 744 
[18.324] Executed solver; result: SAT.
[18.324] Solver returned SAT; a solution has been found at makespan 5.
104
solution 1145 1
1041 1027 480 466 750 1125 682 664 605 598 259 235 96 70 916 895 724 697 947 928 837 1131 41 37 1094 1095 505 499 213 202 412 400 443 1111 893 862 1069 1060 324 301 819 796 779 1127 848 829 630 631 7 4 165 136 1140 1141 744 730 1112 1113 1124 1125 1092 1093 1118 1119 1130 1131 1116 1117 383 1107 1142 1143 568 1115 1102 1103 1136 1137 296 268 1138 1139 1098 1099 1108 1109 1100 1101 1104 1105 1144 1145 1096 1097 1134 1135 1128 1129 1120 1121 1122 1123 1132 1133 
[18.332] Exiting.

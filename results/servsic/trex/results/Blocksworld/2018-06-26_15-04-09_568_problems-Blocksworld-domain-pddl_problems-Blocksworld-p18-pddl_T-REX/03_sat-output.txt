Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.048] Processed problem encoding.
[0.091] Calculated possible fact changes of composite elements.
[0.094] Initialized instantiation procedure.
[0.094] 
[0.094] *************************************
[0.094] * * *   M a k e s p a n     0   * * *
[0.094] *************************************
[0.131] Instantiated 171,395 initial clauses.
[0.131] The encoding contains a total of 86,564 distinct variables.
[0.131] Attempting solve with solver <glucose4> ...
c 54 assumptions
[0.133] Executed solver; result: UNSAT.
[0.133] 
[0.133] *************************************
[0.133] * * *   M a k e s p a n     1   * * *
[0.133] *************************************
[0.143] Computed next depth properties: array size of 213.
[0.168] Instantiated 8,959 transitional clauses.
[0.267] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.267] Instantiated 551,943 universal clauses.
[0.267] Instantiated and added clauses for a total of 732,297 clauses.
[0.267] The encoding contains a total of 268,994 distinct variables.
[0.267] Attempting solve with solver <glucose4> ...
c 213 assumptions
[0.308] Executed solver; result: UNSAT.
[0.308] 
[0.308] *************************************
[0.308] * * *   M a k e s p a n     2   * * *
[0.308] *************************************
[0.371] Computed next depth properties: array size of 531.
[0.482] Instantiated 190,855 transitional clauses.
[0.857] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.857] Instantiated 1,845,832 universal clauses.
[0.857] Instantiated and added clauses for a total of 2,768,984 clauses.
[0.857] The encoding contains a total of 625,635 distinct variables.
[0.857] Attempting solve with solver <glucose4> ...
c 531 assumptions
[0.951] Executed solver; result: UNSAT.
[0.951] 
[0.951] *************************************
[0.951] * * *   M a k e s p a n     3   * * *
[0.951] *************************************
[1.102] Computed next depth properties: array size of 743.
[1.978] Instantiated 952,730 transitional clauses.
[9.546] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[9.546] Instantiated 114,598,032 universal clauses.
[9.546] Instantiated and added clauses for a total of 118,319,746 clauses.
[9.546] The encoding contains a total of 1,284,747 distinct variables.
[9.546] Attempting solve with solver <glucose4> ...
c 743 assumptions
[10.097] Executed solver; result: UNSAT.
[10.097] 
[10.097] *************************************
[10.097] * * *   M a k e s p a n     4   * * *
[10.097] *************************************
[10.278] Computed next depth properties: array size of 955.
[11.284] Instantiated 1,287,690 transitional clauses.
[28.187] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[28.187] Instantiated 233,273,406 universal clauses.
[28.187] Instantiated and added clauses for a total of 352,880,842 clauses.
[28.187] The encoding contains a total of 2,109,537 distinct variables.
[28.187] Attempting solve with solver <glucose4> ...
c 955 assumptions
[28.488] Executed solver; result: UNSAT.
[28.489] 
[28.489] *************************************
[28.489] * * *   M a k e s p a n     5   * * *
[28.489] *************************************
[28.731] Computed next depth properties: array size of 1167.
[29.861] Instantiated 1,610,566 transitional clauses.
[55.863] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[55.863] Instantiated 351,948,780 universal clauses.
[55.863] Instantiated and added clauses for a total of 706,440,188 clauses.
[55.863] The encoding contains a total of 3,095,977 distinct variables.
[55.863] Attempting solve with solver <glucose4> ...
c 1167 assumptions
[56.282] Executed solver; result: UNSAT.
[56.282] 
[56.282] *************************************
[56.282] * * *   M a k e s p a n     6   * * *
[56.282] *************************************
[56.584] Computed next depth properties: aterminate called after throwing an instance of 'Glucose::OutOfMemoryException'
rray size of 1379.
[57.832] Instantiated 1,933,442 transitional clauses.
./interpreter_t-rex : ligne 9 : 33178 Abandon                 LD_LIBRARY_PATH=../lib/haribo/ ./interpreter_t-rex_binary_haribo $@

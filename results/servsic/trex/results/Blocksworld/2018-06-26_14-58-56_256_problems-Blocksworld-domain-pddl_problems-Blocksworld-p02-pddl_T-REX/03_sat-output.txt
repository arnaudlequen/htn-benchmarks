Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.002] Processed problem encoding.
[0.003] Calculated possible fact changes of composite elements.
[0.003] Initialized instantiation procedure.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     0   * * *
[0.003] *************************************
[0.003] Instantiated 858 initial clauses.
[0.003] The encoding contains a total of 471 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.003] Executed solver; result: UNSAT.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     1   * * *
[0.003] *************************************
[0.004] Computed next depth properties: array size of 25.
[0.004] Instantiated 248 transitional clauses.
[0.006] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.006] Instantiated 3,157 universal clauses.
[0.006] Instantiated and added clauses for a total of 4,263 clauses.
[0.006] The encoding contains a total of 1,543 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 25 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     2   * * *
[0.006] *************************************
[0.007] Computed next depth properties: array size of 61.
[0.008] Instantiated 1,256 transitional clauses.
[0.012] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.012] Instantiated 9,283 universal clauses.
[0.012] Instantiated and added clauses for a total of 14,802 clauses.
[0.012] The encoding contains a total of 3,521 distinct variables.
[0.012] Attempting solve with solver <glucose4> ...
c 61 assumptions
[0.013] Executed solver; result: UNSAT.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     3   * * *
[0.013] *************************************
[0.014] Computed next depth properties: array size of 85.
[0.017] Instantiated 3,410 transitional clauses.
[0.025] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.025] Instantiated 25,075 universal clauses.
[0.025] Instantiated and added clauses for a total of 43,287 clauses.
[0.025] The encoding contains a total of 6,333 distinct variables.
[0.025] Attempting solve with solver <glucose4> ...
c 85 assumptions
c last restart ## conflicts  :  0 93 
[0.027] Executed solver; result: SAT.
[0.027] Solver returned SAT; a solution has been found at makespan 3.
18
solution 63 1
22 18 43 39 31 32 56 57 60 61 9 53 54 55 58 59 62 63 
[0.027] Exiting.

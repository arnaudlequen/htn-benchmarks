Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.037] Processed problem encoding.
[0.059] Calculated possible fact changes of composite elements.
[0.061] Initialized instantiation procedure.
[0.061] 
[0.061] *************************************
[0.061] * * *   M a k e s p a n     0   * * *
[0.061] *************************************
[0.077] Instantiated 44,195 initial clauses.
[0.077] The encoding contains a total of 22,577 distinct variables.
[0.077] Attempting solve with solver <glucose4> ...
c 25 assumptions
[0.078] Executed solver; result: UNSAT.
[0.078] 
[0.078] *************************************
[0.078] * * *   M a k e s p a n     1   * * *
[0.078] *************************************
[0.083] Computed next depth properties: array size of 97.
[0.093] Instantiated 3,098 transitional clauses.
[0.130] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.130] Instantiated 144,097 universal clauses.
[0.130] Instantiated and added clauses for a total of 191,390 clauses.
[0.130] The encoding contains a total of 70,149 distinct variables.
[0.130] Attempting solve with solver <glucose4> ...
c 97 assumptions
[0.136] Executed solver; result: UNSAT.
[0.136] 
[0.136] *************************************
[0.136] * * *   M a k e s p a n     2   * * *
[0.136] *************************************
[0.155] Computed next depth properties: array size of 241.
[0.179] Instantiated 50,426 transitional clauses.
[0.260] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.260] Instantiated 475,369 universal clauses.
[0.260] Instantiated and added clauses for a total of 717,185 clauses.
[0.260] The encoding contains a total of 162,529 distinct variables.
[0.260] Attempting solve with solver <glucose4> ...
c 241 assumptions
[0.277] Executed solver; result: UNSAT.
[0.277] 
[0.277] *************************************
[0.277] * * *   M a k e s p a n     3   * * *
[0.277] *************************************
[0.311] Computed next depth properties: array size of 337.
[0.467] Instantiated 237,506 transitional clauses.
[1.503] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.503] Instantiated 15,786,649 universal clauses.
[1.503] Instantiated and added clauses for a total of 16,741,340 clauses.
[1.503] The encoding contains a total of 328,997 distinct variables.
[1.503] Attempting solve with solver <glucose4> ...
c 337 assumptions
[1.596] Executed solver; result: UNSAT.
[1.596] 
[1.596] *************************************
[1.596] * * *   M a k e s p a n     4   * * *
[1.596] *************************************
[1.638] Computed next depth properties: array size of 433.
[1.833] Instantiated 322,466 transitional clauses.
[3.958] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.958] Instantiated 32,180,521 universal clauses.
[3.958] Instantiated and added clauses for a total of 49,244,327 clauses.
[3.958] The encoding contains a total of 537,369 distinct variables.
[3.958] Attempting solve with solver <glucose4> ...
c 433 assumptions
[4.022] Executed solver; result: UNSAT.
[4.022] 
[4.022] *************************************
[4.022] * * *   M a k e s p a n     5   * * *
[4.022] *************************************
[4.081] Computed next depth properties: array size of 529.
[4.305] Instantiated 403,394 transitional clauses.
[7.403] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[7.403] Instantiated 48,574,393 universal clauses.
[7.403] Instantiated and added clauses for a total of 98,222,114 clauses.
[7.403] The encoding contains a total of 786,301 distinct variables.
[7.403] Attempting solve with solver <glucose4> ...
c 529 assumptions
c last restart ## conflicts  :  1 555 
[7.508] Executed solver; result: SAT.
[7.508] Solver returned SAT; a solution has been found at makespan 5.
80
solution 885 1
768 758 321 294 45 33 399 381 581 555 823 885 882 883 846 847 848 849 214 207 250 236 438 439 844 845 344 323 654 642 375 861 862 863 852 853 854 855 812 787 752 729 673 671 70 62 856 857 714 700 415 410 152 149 106 91 488 867 864 865 850 851 868 869 870 871 858 859 878 879 197 178 872 873 880 881 876 877 874 875 
[7.512] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.012] Processed problem encoding.
[0.012] Calculated possible fact changes of composite elements.
[0.013] Initialized instantiation procedure.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     0   * * *
[0.013] *************************************
[0.015] Instantiated 3,086 initial clauses.
[0.015] The encoding contains a total of 1,800 distinct variables.
[0.015] Attempting solve with solver <glucose4> ...
c 40 assumptions
[0.016] Executed solver; result: UNSAT.
[0.016] 
[0.016] *************************************
[0.016] * * *   M a k e s p a n     1   * * *
[0.016] *************************************
[0.019] Computed next depth properties: array size of 148.
[0.020] Instantiated 710 transitional clauses.
[0.026] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.026] Instantiated 5,180 universal clauses.
[0.026] Instantiated and added clauses for a total of 8,976 clauses.
[0.026] The encoding contains a total of 3,166 distinct variables.
[0.026] Attempting solve with solver <glucose4> ...
c 148 assumptions
[0.026] Executed solver; result: UNSAT.
[0.026] 
[0.026] *************************************
[0.026] * * *   M a k e s p a n     2   * * *
[0.026] *************************************
[0.035] Computed next depth properties: array size of 448.
[0.038] Instantiated 2,270 transitional clauses.
[0.054] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.054] Instantiated 16,550 universal clauses.
[0.054] Instantiated and added clauses for a total of 27,796 clauses.
[0.054] The encoding contains a total of 6,101 distinct variables.
[0.054] Attempting solve with solver <glucose4> ...
c 448 assumptions
c last restart ## conflicts  :  28 504 
[0.060] Executed solver; result: SAT.
[0.060] Solver returned SAT; a solution has been found at makespan 2.
447
solution 840 1
3 85 301 302 82 533 297 83 661 737 738 739 663 664 4 90 306 307 88 535 304 86 656 722 784 723 658 659 7 95 317 318 97 541 313 93 661 700 787 701 663 664 9 101 323 324 103 545 327 99 661 692 789 693 663 664 11 109 331 332 107 549 335 105 661 762 791 763 663 664 12 112 338 339 114 551 336 110 656 730 792 731 658 659 14 120 344 345 118 555 350 116 656 670 794 671 658 659 16 124 354 355 126 559 352 122 656 748 796 749 658 659 18 132 360 361 130 563 366 128 656 674 798 675 658 659 21 137 373 374 139 569 369 135 661 728 801 729 663 664 23 145 379 380 143 573 383 141 661 688 803 689 663 664 24 148 386 387 150 575 384 146 656 682 804 683 658 659 26 152 394 395 156 579 392 153 656 768 806 769 658 659 28 162 400 401 160 583 406 158 656 702 808 703 658 659 30 168 410 411 166 587 408 164 656 776 810 777 658 659 32 174 416 417 172 591 422 170 656 714 812 715 658 659 34 178 424 425 180 596 595 176 656 666 814 667 658 659 36 186 430 431 184 601 436 182 656 706 816 707 658 659 38 192 438 439 190 605 444 188 656 744 818 745 658 659 40 198 448 449 196 609 446 194 656 756 820 757 658 659 42 202 454 455 204 614 613 200 656 740 822 741 658 659 44 210 462 463 208 619 460 206 656 780 824 781 658 659 46 214 468 469 216 623 474 212 656 678 826 679 658 659 48 218 478 479 222 627 476 219 656 718 828 719 658 659 51 229 487 488 227 633 491 225 661 754 831 755 663 664 52 234 494 495 232 635 492 230 656 764 832 765 658 659 54 240 502 503 238 639 500 236 656 772 834 773 658 659 56 246 508 509 244 643 514 242 656 710 836 711 658 659 58 250 516 517 252 648 647 248 656 694 838 695 658 659 60 256 522 523 258 653 528 254 656 657 840 660 658 659 63 263 261 64 266 264 66 270 268 68 274 272 70 278 276 73 283 281 74 286 284 77 291 289 79 295 293 
[0.062] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.011] Processed problem encoding.
[0.011] Calculated possible fact changes of composite elements.
[0.012] Initialized instantiation procedure.
[0.012] 
[0.012] *************************************
[0.012] * * *   M a k e s p a n     0   * * *
[0.012] *************************************
[0.015] Instantiated 3,286 initial clauses.
[0.015] The encoding contains a total of 1,920 distinct variables.
[0.015] Attempting solve with solver <glucose4> ...
c 45 assumptions
[0.015] Executed solver; result: UNSAT.
[0.015] 
[0.015] *************************************
[0.015] * * *   M a k e s p a n     1   * * *
[0.015] *************************************
[0.018] Computed next depth properties: array size of 163.
[0.019] Instantiated 770 transitional clauses.
[0.025] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.025] Instantiated 5,635 universal clauses.
[0.025] Instantiated and added clauses for a total of 9,691 clauses.
[0.025] The encoding contains a total of 3,376 distinct variables.
[0.025] Attempting solve with solver <glucose4> ...
c 163 assumptions
[0.025] Executed solver; result: UNSAT.
[0.025] 
[0.025] *************************************
[0.025] * * *   M a k e s p a n     2   * * *
[0.025] *************************************
[0.034] Computed next depth properties: array size of 463.
[0.037] Instantiated 2,330 transitional clauses.
[0.052] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.052] Instantiated 17,005 universal clauses.
[0.052] Instantiated and added clauses for a total of 29,026 clauses.
[0.052] The encoding contains a total of 6,356 distinct variables.
[0.052] Attempting solve with solver <glucose4> ...
c 463 assumptions
c last restart ## conflicts  :  30 538 
[0.058] Executed solver; result: SAT.
[0.058] Solver returned SAT; a solution has been found at makespan 2.
462
solution 870 1
3 93 329 330 95 565 333 91 691 703 704 705 693 694 4 100 334 335 96 567 340 97 686 687 814 690 688 689 6 106 342 343 102 571 348 103 686 794 816 795 688 689 8 110 352 353 112 575 350 108 686 738 818 739 688 689 10 118 358 359 116 579 364 114 686 766 820 767 688 689 12 124 366 367 122 584 583 120 686 786 822 787 688 689 14 128 374 375 130 589 372 126 686 706 824 707 688 689 17 135 383 384 137 595 387 133 691 792 827 793 693 694 19 143 391 392 141 599 395 139 691 732 829 733 693 694 20 148 396 397 146 601 402 144 686 696 830 697 688 689 22 152 406 407 154 605 404 150 686 798 832 799 688 689 24 158 414 415 160 609 412 156 686 778 834 779 688 689 26 166 422 423 164 613 420 162 686 710 836 711 688 689 28 172 428 429 170 618 617 168 686 774 838 775 688 689 30 178 436 437 176 623 434 174 686 742 840 743 688 689 32 182 442 443 184 627 448 180 686 750 842 751 688 689 34 186 452 453 190 631 450 187 686 762 844 763 688 689 36 194 458 459 196 635 464 192 686 722 846 723 688 689 38 200 466 467 202 639 472 198 686 734 848 735 688 689 40 208 474 475 206 643 480 204 686 754 850 755 688 689 42 214 482 483 212 647 488 210 686 714 852 715 688 689 44 218 492 493 220 651 490 216 686 782 854 783 688 689 46 226 498 499 222 655 504 223 686 746 856 747 688 689 49 231 511 512 233 661 507 229 691 728 859 729 693 694 50 236 516 517 238 663 514 234 686 806 860 807 688 689 52 242 522 523 244 667 528 240 686 802 862 803 688 689 54 250 530 531 246 671 536 247 686 718 864 719 688 689 56 256 540 541 252 675 538 253 686 770 866 771 688 689 58 262 546 547 260 679 552 258 686 810 868 811 688 689 60 268 556 557 266 683 554 264 686 758 870 759 688 689 62 272 270 64 274 275 66 280 278 68 284 282 71 289 287 72 292 290 74 296 294 76 300 298 78 304 302 81 308 309 82 312 310 85 317 315 87 321 319 89 325 323 
[0.060] Exiting.

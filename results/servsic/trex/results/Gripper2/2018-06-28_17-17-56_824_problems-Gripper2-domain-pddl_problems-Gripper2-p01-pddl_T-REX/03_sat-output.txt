Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.003] Processed problem encoding.
[0.003] Calculated possible fact changes of composite elements.
[0.003] Initialized instantiation procedure.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     0   * * *
[0.003] *************************************
[0.004] Instantiated 1,315 initial clauses.
[0.004] The encoding contains a total of 776 distinct variables.
[0.004] Attempting solve with solver <glucose4> ...
c 26 assumptions
[0.004] Executed solver; result: UNSAT.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     1   * * *
[0.004] *************************************
[0.005] Computed next depth properties: array size of 151.
[0.006] Instantiated 677 transitional clauses.
[0.009] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.009] Instantiated 3,585 universal clauses.
[0.009] Instantiated and added clauses for a total of 5,577 clauses.
[0.009] The encoding contains a total of 1,680 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 151 assumptions
c last restart ## conflicts  :  0 250 
[0.009] Executed solver; result: SAT.
[0.009] Solver returned SAT; a solution has been found at makespan 1.
149
solution 201 1
3 4 5 6 7 2 14 16 5 15 17 2 22 24 5 23 25 2 30 32 5 31 33 2 38 40 5 39 41 2 46 48 5 47 49 2 54 56 5 55 57 2 62 64 5 63 65 2 70 72 5 71 73 2 78 80 5 79 81 2 86 88 5 87 89 2 94 96 5 95 97 2 102 104 5 103 105 2 110 112 5 111 113 2 118 120 5 119 121 2 126 128 5 127 129 2 134 136 5 135 137 2 142 144 5 143 145 2 150 152 5 151 153 2 158 160 5 159 161 2 166 168 5 167 169 2 174 176 5 175 177 2 182 184 5 183 185 2 190 192 5 191 193 2 198 200 5 199 201 
[0.009] Exiting.

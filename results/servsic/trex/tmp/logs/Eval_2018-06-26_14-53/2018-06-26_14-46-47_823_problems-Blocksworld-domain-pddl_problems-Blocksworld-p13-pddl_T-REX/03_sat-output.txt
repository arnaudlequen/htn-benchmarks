Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.036] Processed problem encoding.
[0.059] Calculated possible fact changes of composite elements.
[0.060] Initialized instantiation procedure.
[0.060] 
[0.060] *************************************
[0.060] * * *   M a k e s p a n     0   * * *
[0.060] *************************************
[0.077] Instantiated 44,195 initial clauses.
[0.077] The encoding contains a total of 22,577 distinct variables.
[0.077] Attempting solve with solver <glucose4> ...
c 25 assumptions
[0.077] Executed solver; result: UNSAT.
[0.077] 
[0.077] *************************************
[0.077] * * *   M a k e s p a n     1   * * *
[0.077] *************************************
[0.082] Computed next depth properties: array size of 97.
[0.093] Instantiated 3,098 transitional clauses.
[0.130] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.130] Instantiated 144,097 universal clauses.
[0.130] Instantiated and added clauses for a total of 191,390 clauses.
[0.130] The encoding contains a total of 70,149 distinct variables.
[0.130] Attempting solve with solver <glucose4> ...
c 97 assumptions
[0.137] Executed solver; result: UNSAT.
[0.137] 
[0.137] *************************************
[0.137] * * *   M a k e s p a n     2   * * *
[0.137] *************************************
[0.156] Computed next depth properties: array size of 241.
[0.182] Instantiated 50,426 transitional clauses.
[0.268] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.268] Instantiated 475,369 universal clauses.
[0.268] Instantiated and added clauses for a total of 717,185 clauses.
[0.268] The encoding contains a total of 162,529 distinct variables.
[0.268] Attempting solve with solver <glucose4> ...
c 241 assumptions
[0.291] Executed solver; result: UNSAT.
[0.291] 
[0.291] *************************************
[0.291] * * *   M a k e s p a n     3   * * *
[0.291] *************************************
[0.327] Computed next depth properties: array size of 337.
[0.487] Instantiated 237,506 transitional clauses.
[1.543] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.543] Instantiated 15,786,649 universal clauses.
[1.543] Instantiated and added clauses for a total of 16,741,340 clauses.
[1.543] The encoding contains a total of 328,997 distinct variables.
[1.543] Attempting solve with solver <glucose4> ...
c 337 assumptions
[1.636] Executed solver; result: UNSAT.
[1.636] 
[1.636] *************************************
[1.636] * * *   M a k e s p a n     4   * * *
[1.636] *************************************
[1.679] Computed next depth properties: array size of 433.
[1.876] Instantiated 322,466 transitional clauses.
[4.014] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.014] Instantiated 32,180,521 universal clauses.
[4.014] Instantiated and added clauses for a total of 49,244,327 clauses.
[4.014] The encoding contains a total of 537,369 distinct variables.
[4.014] Attempting solve with solver <glucose4> ...
c 433 assumptions
[4.079] Executed solver; result: UNSAT.
[4.079] 
[4.079] *************************************
[4.079] * * *   M a k e s p a n     5   * * *
[4.079] *************************************
[4.138] Computed next depth properties: array size of 529.
[4.364] Instantiated 403,394 transitional clauses.
[7.472] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[7.472] Instantiated 48,574,393 universal clauses.
[7.472] Instantiated and added clauses for a total of 98,222,114 clauses.
[7.472] The encoding contains a total of 786,301 distinct variables.
[7.472] Attempting solve with solver <glucose4> ...
c 529 assumptions
c last restart ## conflicts  :  1 555 
[7.576] Executed solver; result: SAT.
[7.576] Solver returned SAT; a solution has been found at makespan 5.
80
solution 885 1
768 758 321 294 45 33 399 381 581 555 823 885 882 883 846 847 848 849 214 207 250 236 438 439 844 845 344 323 654 642 375 861 862 863 852 853 854 855 812 787 752 729 673 671 70 62 856 857 714 700 415 410 152 149 106 91 488 867 864 865 850 851 868 869 870 871 858 859 878 879 197 178 872 873 880 881 876 877 874 875 
[7.581] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.003] Processed problem encoding.
[0.003] Calculated possible fact changes of composite elements.
[0.004] Initialized instantiation procedure.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     0   * * *
[0.004] *************************************
[0.004] Instantiated 1,122 initial clauses.
[0.004] The encoding contains a total of 620 distinct variables.
[0.004] Attempting solve with solver <glucose4> ...
c 6 assumptions
[0.004] Executed solver; result: UNSAT.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     1   * * *
[0.004] *************************************
[0.005] Computed next depth properties: array size of 21.
[0.005] Instantiated 247 transitional clauses.
[0.007] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.007] Instantiated 3,921 universal clauses.
[0.007] Instantiated and added clauses for a total of 5,290 clauses.
[0.007] The encoding contains a total of 1,934 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 21 assumptions
[0.007] Executed solver; result: UNSAT.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     2   * * *
[0.007] *************************************
[0.008] Computed next depth properties: array size of 51.
[0.010] Instantiated 1,507 transitional clauses.
[0.015] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.015] Instantiated 11,836 universal clauses.
[0.015] Instantiated and added clauses for a total of 18,633 clauses.
[0.015] The encoding contains a total of 4,383 distinct variables.
[0.015] Attempting solve with solver <glucose4> ...
c 51 assumptions
[0.016] Executed solver; result: UNSAT.
[0.016] 
[0.016] *************************************
[0.016] * * *   M a k e s p a n     3   * * *
[0.016] *************************************
[0.017] Computed next depth properties: array size of 71.
[0.021] Instantiated 4,682 transitional clauses.
[0.033] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.033] Instantiated 43,386 universal clauses.
[0.033] Instantiated and added clauses for a total of 66,701 clauses.
[0.033] The encoding contains a total of 8,067 distinct variables.
[0.033] Attempting solve with solver <glucose4> ...
c 71 assumptions
[0.034] Executed solver; result: UNSAT.
[0.034] 
[0.034] *************************************
[0.034] * * *   M a k e s p a n     4   * * *
[0.034] *************************************
[0.036] Computed next depth properties: array size of 91.
[0.041] Instantiated 6,582 transitional clauses.
[0.058] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.058] Instantiated 80,976 universal clauses.
[0.058] Instantiated and added clauses for a total of 154,259 clauses.
[0.058] The encoding contains a total of 12,681 distinct variables.
[0.058] Attempting solve with solver <glucose4> ...
c 91 assumptions
[0.061] Executed solver; result: UNSAT.
[0.061] 
[0.061] *************************************
[0.061] * * *   M a k e s p a n     5   * * *
[0.061] *************************************
[0.063] Computed next depth properties: array size of 111.
[0.067] Instantiated 8,242 transitional clauses.
[0.088] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.088] Instantiated 118,566 universal clauses.
[0.088] Instantiated and added clauses for a total of 281,067 clauses.
[0.088] The encoding contains a total of 18,145 distinct variables.
[0.088] Attempting solve with solver <glucose4> ...
c 111 assumptions
[0.089] Executed solver; result: UNSAT.
[0.089] 
[0.089] *************************************
[0.089] * * *   M a k e s p a n     6   * * *
[0.089] *************************************
[0.092] Computed next depth properties: array size of 131.
[0.096] Instantiated 9,902 transitional clauses.
[0.120] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.120] Instantiated 156,156 universal clauses.
[0.120] Instantiated and added clauses for a total of 447,125 clauses.
[0.120] The encoding contains a total of 24,459 distinct variables.
[0.120] Attempting solve with solver <glucose4> ...
c 131 assumptions
[0.123] Executed solver; result: UNSAT.
[0.123] 
[0.123] *************************************
[0.123] * * *   M a k e s p a n     7   * * *
[0.123] *************************************
[0.125] Computed next depth properties: array size of 151.
[0.130] Instantiated 11,562 transitional clauses.
[0.156] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.156] Instantiated 193,746 universal clauses.
[0.156] Instantiated and added clauses for a total of 652,433 clauses.
[0.156] The encoding contains a total of 31,623 distinct variables.
[0.156] Attempting solve with solver <glucose4> ...
c 151 assumptions
c last restart ## conflicts  :  0 167 
[0.160] Executed solver; result: SAT.
[0.160] Solver returned SAT; a solution has been found at makespan 7.
24
solution 93 1
59 58 16 13 47 40 83 76 66 67 5 4 52 49 88 89 92 93 86 87 84 85 90 91 
[0.160] Exiting.

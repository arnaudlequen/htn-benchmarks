Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.040] Processed problem encoding.
[0.067] Calculated possible fact changes of composite elements.
[0.070] Initialized instantiation procedure.
[0.070] 
[0.070] *************************************
[0.070] * * *   M a k e s p a n     0   * * *
[0.070] *************************************
[0.094] Instantiated 72,834 initial clauses.
[0.094] The encoding contains a total of 36,974 distinct variables.
[0.094] Attempting solve with solver <glucose4> ...
c 36 assumptions
[0.095] Executed solver; result: UNSAT.
[0.095] 
[0.095] *************************************
[0.095] * * *   M a k e s p a n     1   * * *
[0.095] *************************************
[0.102] Computed next depth properties: array size of 141.
[0.118] Instantiated 4,797 transitional clauses.
[0.166] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.166] Instantiated 237,651 universal clauses.
[0.166] Instantiated and added clauses for a total of 315,282 clauses.
[0.166] The encoding contains a total of 115,448 distinct variables.
[0.166] Attempting solve with solver <glucose4> ...
c 141 assumptions
[0.177] Executed solver; result: UNSAT.
[0.177] 
[0.177] *************************************
[0.177] * * *   M a k e s p a n     2   * * *
[0.177] *************************************
[0.202] Computed next depth properties: array size of 351.
[0.242] Instantiated 82,917 transitional clauses.
[0.381] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.381] Instantiated 786,626 universal clauses.
[0.381] Instantiated and added clauses for a total of 1,184,825 clauses.
[0.381] The encoding contains a total of 268,087 distinct variables.
[0.381] Attempting solve with solver <glucose4> ...
c 351 assumptions
[0.414] Executed solver; result: UNSAT.
[0.414] 
[0.414] *************************************
[0.414] * * *   M a k e s p a n     3   * * *
[0.414] *************************************
[0.470] Computed next depth properties: array size of 491.
[0.735] Instantiated 396,202 transitional clauses.
[2.673] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.673] Instantiated 30,069,586 universal clauses.
[2.673] Instantiated and added clauses for a total of 31,650,613 clauses.
[2.673] The encoding contains a total of 544,871 distinct variables.
[2.673] Attempting solve with solver <glucose4> ...
c 491 assumptions
[2.845] Executed solver; result: UNSAT.
[2.845] 
[2.845] *************************************
[2.845] * * *   M a k e s p a n     4   * * *
[2.845] *************************************
[2.923] Computed next depth properties: array size of 631.
[3.249] Instantiated 537,322 transitional clauses.
[7.124] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[7.124] Instantiated 61,289,796 universal clauses.
[7.124] Instantiated and added clauses for a total of 93,477,731 clauses.
[7.124] The encoding contains a total of 891,305 distinct variables.
[7.124] Attempting solve with solver <glucose4> ...
c 631 assumptions
[7.244] Executed solver; result: UNSAT.
[7.244] 
[7.244] *************************************
[7.244] * * *   M a k e s p a n     5   * * *
[7.244] *************************************
[7.335] Computed next depth properties: array size of 771.
[7.709] Instantiated 672,142 transitional clauses.
[13.560] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[13.560] Instantiated 92,510,006 universal clauses.
[13.560] Instantiated and added clauses for a total of 186,659,879 clauses.
[13.560] The encoding contains a total of 1,305,289 distinct variables.
[13.560] Attempting solve with solver <glucose4> ...
c 771 assumptions
[13.721] Executed solver; result: UNSAT.
[13.721] 
[13.721] *************************************
[13.721] * * *   M a k e s p a n     6   * * *
[13.721] *************************************
[13.830] Computed next depth properties: array size of 911.
[14.254] Instantiated 806,962 transitional clauses.
[22.079] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[22.079] Instantiated 123,730,216 universal clauses.
[22.079] Instantiated and added clauses for a total of 311,197,057 clauses.
[22.079] The encoding contains a total of 1,786,823 distinct variables.
[22.079] Attempting solve with solver <glucose4> ...
c 911 assumptions
[22.285] Executed solver; result: UNSAT.
[22.285] 
[22.285] *************************************
[22.285] * * *   M a k e s p a n     7   * * *
[22.285] *************************************
[22.438] Computed next depth properties: array size of 1051.
[22.910] Instantiated 941,782 transitional clauses.
[32.869] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[32.869] Instantiated 154,950,426 universal clauses.
[32.869] Instantiated and added clauses for a total of 467,089,265 clauses.
[32.869] The encoding contains a total of 2,335,907 distinct variables.
[32.869] Attempting solve with solver <glucose4> ...
c 1051 assumptions
[33.138] Executed solver; result: UNSAT.
[33.138] 
[33.138] *************************************
[33.138] * * *   M a k e s p a n     8   * * *
[33.138] *************************************
[33.318] Computed next depth properties: array size of 1191.
[33.840] Instantiated 1,076,602 transitional clauses.
[45.769] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[45.769] Instantiated 186,170,636 universal clauses.
[45.769] Instantiated and added clauses for a total of 654,336,503 clauses.
[45.769] The encoding contains a total of 2,952,541 distinct variables.
[45.770] Attempting solve with solver <glucose4> ...
c 1191 assumptions
c last restart ## conflicts  :  8 1225 
[46.489] Executed solver; result: SAT.
[46.489] Solver returned SAT; a solution has been found at makespan 8.
102
solution 1009 1
172 159 450 438 391 376 517 500 569 562 227 221 204 190 498 979 649 624 831 810 663 991 264 252 417 407 340 314 866 841 802 779 726 717 306 283 988 989 670 655 490 469 705 686 968 969 647 624 765 748 542 531 347 345 976 977 992 993 978 979 990 991 996 997 988 989 980 981 962 1009 208 190 621 987 968 969 68 965 1002 1003 1000 1001 876 1005 96 967 982 983 970 971 974 975 972 973 994 995 984 985 998 999 1006 1007 
[46.505] Exiting.

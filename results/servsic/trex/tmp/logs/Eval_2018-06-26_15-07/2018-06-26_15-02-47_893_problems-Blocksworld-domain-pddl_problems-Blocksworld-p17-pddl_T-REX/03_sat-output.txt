Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.066] Processed problem encoding.
[0.119] Calculated possible fact changes of composite elements.
[0.122] Initialized instantiation procedure.
[0.122] 
[0.122] *************************************
[0.122] * * *   M a k e s p a n     0   * * *
[0.122] *************************************
[0.151] Instantiated 96,882 initial clauses.
[0.151] The encoding contains a total of 49,200 distinct variables.
[0.151] Attempting solve with solver <glucose4> ...
c 34 assumptions
[0.151] Executed solver; result: UNSAT.
[0.151] 
[0.151] *************************************
[0.151] * * *   M a k e s p a n     1   * * *
[0.151] *************************************
[0.160] Computed next depth properties: array size of 133.
[0.175] Instantiated 5,315 transitional clauses.
[0.231] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.231] Instantiated 311,389 universal clauses.
[0.231] Instantiated and added clauses for a total of 413,586 clauses.
[0.231] The encoding contains a total of 152,098 distinct variables.
[0.231] Attempting solve with solver <glucose4> ...
c 133 assumptions
[0.251] Executed solver; result: UNSAT.
[0.251] 
[0.251] *************************************
[0.251] * * *   M a k e s p a n     2   * * *
[0.251] *************************************
[0.285] Computed next depth properties: array size of 331.
[0.348] Instantiated 107,879 transitional clauses.
[0.547] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.547] Instantiated 1,039,072 universal clauses.
[0.547] Instantiated and added clauses for a total of 1,560,537 clauses.
[0.547] The encoding contains a total of 353,039 distinct variables.
[0.547] Attempting solve with solver <glucose4> ...
c 331 assumptions
[0.592] Executed solver; result: UNSAT.
[0.592] 
[0.592] *************************************
[0.592] * * *   M a k e s p a n     3   * * *
[0.592] *************************************
[0.682] Computed next depth properties: array size of 463.
[1.137] Instantiated 533,546 transitional clauses.
[4.806] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.806] Instantiated 57,721,258 universal clauses.
[4.806] Instantiated and added clauses for a total of 59,815,341 clauses.
[4.806] The encoding contains a total of 722,907 distinct variables.
[4.807] Attempting solve with solver <glucose4> ...
c 463 assumptions
[5.097] Executed solver; result: UNSAT.
[5.097] 
[5.097] *************************************
[5.097] * * *   M a k e s p a n     4   * * *
[5.097] *************************************
[5.208] Computed next depth properties: array size of 595.
[5.735] Instantiated 721,646 transitional clauses.
[13.545] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[13.545] Instantiated 117,543,328 universal clauses.
[13.545] Instantiated and added clauses for a total of 178,080,315 clauses.
[13.545] The encoding contains a total of 1,185,769 distinct variables.
[13.545] Attempting solve with solver <glucose4> ...
c 595 assumptions
[13.720] Executed solver; result: UNSAT.
[13.720] 
[13.720] *************************************
[13.720] * * *   M a k e s p a n     5   * * *
[13.720] *************************************
[13.848] Computed next depth properties: array size of 727.
[14.445] Instantiated 902,618 transitional clauses.
[26.160] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[26.160] Instantiated 177,365,398 universal clauses.
[26.160] Instantiated and added clauses for a total of 356,348,331 clauses.
[26.160] The encoding contains a total of 1,739,249 distinct variables.
[26.160] Attempting solve with solver <glucose4> ...
c 727 assumptions
[26.389] Executed solver; result: UNSAT.
[26.389] 
[26.389] *************************************
[26.389] * * *   M a k e s p a n     6   * * *
[26.389] *************************************
[26.542] Computed next depth properties: array size of 859.
terminate called after throwing an instance of 'Glucose::OutOfMemoryException'
[27.205] Instantiated 1,083,590 transitional clauses.
[43.242] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[43.242] Instantiated 237,187,468 universal clauses.
[43.242] Instantiated and added clauses for a total of 594,619,389 clauses.
[43.242] The encoding contains a total of 2,383,347 distinct variables.
[43.242] Attempting solve with solver <glucose4> ...
c 859 assumptions
[43.518] Executed solver; result: UNSAT.
[43.518] 
[43.518] *************************************
[43.518] * * *   M a k e s p a n     7   * * *
[43.518] *************************************
[43.693] Computed next depth properties: array size of 991.
[44.430] Instantiated 1,264,562 transitional clauses.
[65.250] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[65.250] Instantiated 297,009,538 universal clauses.
[65.250] Instantiated and added clauses for a total of 892,893,489 clauses.
[65.250] The encoding contains a total of 3,118,063 distinct variables.
[65.251] Attempting solve with solver <glucose4> ...
c 991 assumptions
[65.607] Executed solver; result: UNSAT.
[65.607] 
[65.607] *************************************
[65.607] * * *   M a k e s p a n     8   * * *
[65.607] *************************************
[65.851] Computed next depth properties: array size of 1123.
[66.661] Instantiated 1,445,534 transitional clauses.
./interpreter_t-rex : ligne 9 : 26230 Abandon                 LD_LIBRARY_PATH=../lib/haribo/ ./interpreter_t-rex_binary_haribo $@

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.026] Processed problem encoding.
[0.038] Calculated possible fact changes of composite elements.
[0.039] Initialized instantiation procedure.
[0.039] 
[0.039] *************************************
[0.039] * * *   M a k e s p a n     0   * * *
[0.039] *************************************
[0.054] Instantiated 35,929 initial clauses.
[0.054] The encoding contains a total of 18,335 distinct variables.
[0.054] Attempting solve with solver <glucose4> ...
c 27 assumptions
[0.054] Executed solver; result: UNSAT.
[0.054] 
[0.054] *************************************
[0.054] * * *   M a k e s p a n     1   * * *
[0.054] *************************************
[0.059] Computed next depth properties: array size of 105.
[0.068] Instantiated 2,940 transitional clauses.
[0.103] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.103] Instantiated 118,977 universal clauses.
[0.103] Instantiated and added clauses for a total of 157,846 clauses.
[0.103] The encoding contains a total of 57,599 distinct variables.
[0.103] Attempting solve with solver <glucose4> ...
c 105 assumptions
[0.109] Executed solver; result: UNSAT.
[0.109] 
[0.109] *************************************
[0.109] * * *   M a k e s p a n     2   * * *
[0.109] *************************************
[0.128] Computed next depth properties: array size of 261.
[0.153] Instantiated 41,940 transitional clauses.
[0.225] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.225] Instantiated 389,351 universal clauses.
[0.225] Instantiated and added clauses for a total of 589,137 clauses.
[0.225] The encoding contains a total of 133,549 distinct variables.
[0.225] Attempting solve with solver <glucose4> ...
c 261 assumptions
[0.243] Executed solver; result: UNSAT.
[0.243] 
[0.243] *************************************
[0.243] * * *   M a k e s p a n     3   * * *
[0.243] *************************************
[0.271] Computed next depth properties: array size of 365.
[0.382] Instantiated 190,738 transitional clauses.
[1.053] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.053] Instantiated 9,465,379 universal clauses.
[1.053] Instantiated and added clauses for a total of 10,245,254 clauses.
[1.053] The encoding contains a total of 268,337 distinct variables.
[1.053] Attempting solve with solver <glucose4> ...
c 365 assumptions
[1.123] Executed solver; result: UNSAT.
[1.123] 
[1.123] *************************************
[1.123] * * *   M a k e s p a n     4   * * *
[1.123] *************************************
[1.157] Computed next depth properties: array size of 469.
[1.296] Instantiated 259,690 transitional clauses.
[2.595] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.595] Instantiated 19,284,591 universal clauses.
[2.595] Instantiated and added clauses for a total of 29,789,535 clauses.
[2.595] The encoding contains a total of 437,081 distinct variables.
[2.595] Attempting solve with solver <glucose4> ...
c 469 assumptions
[2.652] Executed solver; result: UNSAT.
[2.652] 
[2.652] *************************************
[2.652] * * *   M a k e s p a n     5   * * *
[2.652] *************************************
[2.699] Computed next depth properties: array size of 573.
[2.861] Instantiated 324,898 transitional clauses.
[4.864] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.864] Instantiated 29,103,803 universal clauses.
[4.864] Instantiated and added clauses for a total of 59,218,236 clauses.
[4.864] The encoding contains a total of 638,533 distinct variables.
[4.864] Attempting solve with solver <glucose4> ...
c 573 assumptions
[4.942] Executed solver; result: UNSAT.
[4.942] 
[4.942] *************************************
[4.942] * * *   M a k e s p a n     6   * * *
[4.942] *************************************
[4.998] Computed next depth properties: array size of 677.
[5.185] Instantiated 390,106 transitional clauses.
[7.787] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[7.787] Instantiated 38,923,015 universal clauses.
[7.787] Instantiated and added clauses for a total of 98,531,357 clauses.
[7.787] The encoding contains a total of 872,693 distinct variables.
[7.787] Attempting solve with solver <glucose4> ...
c 677 assumptions
[7.885] Executed solver; result: UNSAT.
[7.885] 
[7.885] *************************************
[7.885] * * *   M a k e s p a n     7   * * *
[7.885] *************************************
[7.947] Computed next depth properties: array size of 781.
[8.160] Instantiated 455,314 transitional clauses.
[11.426] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[11.427] Instantiated 48,742,227 universal clauses.
[11.427] Instantiated and added clauses for a total of 147,728,898 clauses.
[11.427] The encoding contains a total of 1,139,561 distinct variables.
[11.427] Attempting solve with solver <glucose4> ...
c 781 assumptions
[11.555] Executed solver; result: UNSAT.
[11.555] 
[11.555] *************************************
[11.555] * * *   M a k e s p a n     8   * * *
[11.555] *************************************
[11.623] Computed next depth properties: array size of 885.
[11.862] Instantiated 520,522 transitional clauses.
[15.789] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[15.789] Instantiated 58,561,439 universal clauses.
[15.789] Instantiated and added clauses for a total of 206,810,859 clauses.
[15.789] The encoding contains a total of 1,439,137 distinct variables.
[15.789] Attempting solve with solver <glucose4> ...
c 885 assumptions
[15.946] Executed solver; result: UNSAT.
[15.946] 
[15.946] *************************************
[15.946] * * *   M a k e s p a n     9   * * *
[15.946] *************************************
[16.023] Computed next depth properties: array size of 989.
[16.288] Instantiated 585,730 transitional clauses.
[20.890] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[20.890] Instantiated 68,380,651 universal clauses.
[20.890] Instantiated and added clauses for a total of 275,777,240 clauses.
[20.890] The encoding contains a total of 1,771,421 distinct variables.
[20.890] Attempting solve with solver <glucose4> ...
c 989 assumptions
[21.080] Executed solver; result: UNSAT.
[21.080] 
[21.080] *************************************
[21.080] * * *   M a k e s p a n    10   * * *
[21.080] *************************************
[21.184] Computed next depth properties: array size of 1093.
[21.470] Instantiated 650,938 transitional clauses.
[26.679] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[26.679] Instantiated 78,199,863 universal clauses.
[26.679] Instantiated and added clauses for a total of 354,628,041 clauses.
[26.679] The encoding contains a total of 2,136,413 distinct variables.
[26.679] Attempting solve with solver <glucose4> ...
c 1093 assumptions
c last restart ## conflicts  :  3 1134 
[26.980] Executed solver; result: SAT.
[26.980] Solver returned SAT; a solution has been found at makespan 10.
92
solution 669 1
517 504 333 329 125 104 566 554 320 304 296 279 461 454 181 179 68 54 394 379 430 655 445 429 407 404 78 79 13 629 654 655 272 254 501 479 593 579 374 354 552 529 609 604 134 129 162 154 237 229 644 645 5 4 632 633 628 629 660 661 636 637 656 657 642 643 664 665 652 653 668 669 650 651 646 647 634 635 638 639 666 667 648 649 640 641 630 631 662 663 658 659 
[26.990] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.007] Parsed head comment information.
[0.236] Processed problem encoding.
[0.268] Calculated possible fact changes of composite elements.
[0.286] Initialized instantiation procedure.
[0.286] 
[0.286] *************************************
[0.286] * * *   M a k e s p a n     0   * * *
[0.286] *************************************
[0.357] Instantiated 61,373 initial clauses.
[0.357] The encoding contains a total of 31,003 distinct variables.
[0.357] Attempting solve with solver <glucose4> ...
c 76 assumptions
[0.358] Executed solver; result: UNSAT.
[0.358] 
[0.358] *************************************
[0.358] * * *   M a k e s p a n     1   * * *
[0.358] *************************************
[0.472] Computed next depth properties: array size of 151.
[0.537] Instantiated 60,377 transitional clauses.
[0.791] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.791] Instantiated 270,972 universal clauses.
[0.791] Instantiated and added clauses for a total of 392,722 clauses.
[0.791] The encoding contains a total of 121,232 distinct variables.
[0.791] Attempting solve with solver <glucose4> ...
c 151 assumptions
[0.792] Executed solver; result: UNSAT.
[0.792] 
[0.792] *************************************
[0.792] * * *   M a k e s p a n     2   * * *
[0.792] *************************************
[1.005] Computed next depth properties: array size of 301.
[1.218] Instantiated 180,302 transitional clauses.
[1.970] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.970] Instantiated 12,452,097 universal clauses.
[1.970] Instantiated and added clauses for a total of 13,025,121 clauses.
[1.970] The encoding contains a total of 242,061 distinct variables.
[1.970] Attempting solve with solver <glucose4> ...
c 301 assumptions
c last restart ## conflicts  :  0 307 
[1.971] Executed solver; result: SAT.
[1.971] Solver returned SAT; a solution has been found at makespan 2.
300
solution 52314 1
15124 2 28091 28007 822 3 28442 28008 964 4 28589 28009 1372 5 26648 28010 1758 6 20926 28011 2042 7 28806 28012 2366 8 28969 28013 2627 9 29635 28014 2960 10 29721 28015 3104 11 22104 28016 3505 12 41972 28017 16077 13 29965 28018 4036 14 30321 28019 4089 15 30483 28020 4259 16 42370 28021 16907 17 3181 28022 17064 18 1458 28023 17333 19 42614 28024 4804 20 42897 28025 17821 21 31176 28026 5133 22 43343 28027 18466 23 23609 28028 18756 24 43499 28029 5774 25 43695 28030 18859 26 44210 28031 19328 27 44288 28032 6325 28 31999 28033 6588 29 32252 28034 6984 30 18265 28035 7235 31 32606 28036 7526 32 44708 28037 7638 33 45039 28038 20086 34 33207 28039 20583 35 45484 28040 6270 36 33585 28041 20667 37 45995 28042 20920 38 33952 28043 21301 39 46511 28044 21526 40 34354 28045 8797 41 21316 28046 9131 42 34535 28047 9442 43 34943 28048 22238 44 47288 28049 22495 45 35076 28050 9996 46 47348 28051 22888 47 35748 28052 10205 48 47745 28053 23444 49 7974 28054 10641 50 36046 28055 23623 51 48271 28056 11103 52 36312 28057 23895 53 12741 28058 24409 54 48993 28059 24566 55 36661 28060 11768 56 33099 28061 12076 57 36777 28062 25228 58 49342 28063 10120 59 37537 28064 12388 60 37660 28065 12543 61 49855 28066 25633 62 45609 28067 25785 63 37931 28068 13415 64 50327 28069 26015 65 38158 28070 11964 66 50824 28071 26065 67 51302 28072 13996 68 38656 28073 14516 69 51424 28074 26676 70 51739 28075 24095 71 51894 28076 14888 72 39708 28077 14939 73 19172 28078 27673 74 52064 28079 25237 75 52182 28080 27979 76 52314 28081 
[1.991] Exiting.

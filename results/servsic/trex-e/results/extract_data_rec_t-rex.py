import re
import numpy as np
import matplotlib.pyplot as plt
import os

MAX = 20

def main():
    pblist = list(map(str, os.listdir("."))) # get all files' and folders' names in the current directory

    if "Figures" in pblist:
        pblist.remove("Figures")
    if "Data" in pblist:
        pblist.remove("Data")

    for dirname in pblist:
        if not os.path.isdir(os.path.join(os.path.abspath("."), dirname)) or dirname == "Figures":
            continue
        print(dirname, end="...")
        plan_ids = []
        plan_length = []

        time_ids = []
        time = []

        filenames = os.listdir(dirname + "/") # get all files' and folders' names in the problem's directiry

        for filename in filenames: # loop through all the files and folders
            if os.path.isdir(os.path.join(os.path.abspath(dirname + "/"), filename)) and filename != "sysinfo": # check whether the current object is a folder or not
                substr = filename.split("-")
                p = re.compile("[p][0-9][0-9]")
                id = -1
                for s in substr:
                    if p.match(s):
                        id = int(re.sub("[p]", "", s))
                        break
                if id == -1:
                    #print("FAILED TO FETCH ID")
                    continue


                try:
                    in_file = open(dirname + "/" + filename + "/04_solution.txt", "rt")
                    ln = 0
                    for line in in_file:
                        l = line.split(":")
                        if len(l) > 1 and len(l[1]) > 2:
                            ln = int(l[0])
                    plan_length.append(ln)
                    plan_ids.append(id)

                except:
                    plan_length.append(-1)
                    plan_ids.append(id)

                try:
                    if (os.path.exists(dirname + "/" + filename + "/04_solution.txt")):
                        in_file = open(dirname + "/" + filename + "/general_output", "rt")
                        l = 0
                        for line in in_file:
                            if "Total execution time:" in line:
                                l = int(re.sub("[^0-9]", "", line))
                                l /= 1000
                                break

                        time.append(l)
                        time_ids.append(id)
                    else:
                        time.append(-1)
                        time_ids.append(id)


                except:
                    time.append(-1)
                    time_ids.append(id)

        if not os.path.exists("Figures/"):
            os.makedirs("Figures/")

        if not os.path.exists("Figures/" + dirname):
            os.makedirs("Figures/" + dirname)

        p = [(plan_ids[i], plan_length[i]) for i in range(len(plan_ids))]
        t = [(time_ids[i], time[i]) for i in range(len(time_ids))]

        p.sort()
        t.sort()

        plt.plot([x[0] for x in t if x[1] != -1], [x[1] for x in t if x[1] != -1], 'ro')
        plt.xlabel("Problem")
        plt.ylabel("Running Time (s)")
        plt.grid(True)
        plt.xticks(list(range(0, 21, 5)))
        plt.savefig("Figures/" + dirname + "/time.png")
        plt.clf()

        plt.plot([x[0] for x in p if x[1] != -1], [x[1] for x in p if x[1] != -1], 'ro')
        plt.xlabel("Problem")
        plt.ylabel("Plan length")
        plt.grid(True)
        plt.xticks(list(range(0, 21, 5)))
        plt.savefig("Figures/" + dirname + "/length.png")
        plt.clf()

        if not os.path.exists("Data/"):
            os.makedirs("Data/")

        if not os.path.exists("Data/" + dirname):
            os.makedirs("Data/" + dirname)

        f = open("Data/" + dirname + "/time.txt", "w+")
        for i in range(len(p)):
            f.write(str(t[i][1]) + "\n")
        f.close()

        f = open("Data/" + dirname + "/length.txt", "w+")
        for i in range(len(t)):
            f.write(str(p[i][1]) + "\n")
        f.close()

        print("done")


main()

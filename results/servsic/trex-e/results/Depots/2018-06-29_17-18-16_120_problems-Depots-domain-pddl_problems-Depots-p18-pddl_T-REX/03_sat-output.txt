Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.002] Parsed head comment information.
[0.723] Processed problem encoding.
[1.335] Calculated possible fact changes of composite elements.
[1.352] Initialized instantiation procedure.
[1.352] 
[1.352] *************************************
[1.352] * * *   M a k e s p a n     0   * * *
[1.352] *************************************
[1.359] Instantiated 21,396 initial clauses.
[1.359] The encoding contains a total of 14,105 distinct variables.
[1.359] Attempting solve with solver <glucose4> ...
c 15 assumptions
[1.359] Executed solver; result: UNSAT.
[1.359] 
[1.359] *************************************
[1.359] * * *   M a k e s p a n     1   * * *
[1.359] *************************************
[1.371] Computed next depth properties: array size of 57.
[1.407] Instantiated 45,294 transitional clauses.
[1.439] Instantiated 147,730 universal clauses.
[1.439] Instantiated and added clauses for a total of 214,420 clauses.
[1.439] The encoding contains a total of 49,567 distinct variables.
[1.439] Attempting solve with solver <glucose4> ...
c 57 assumptions
[1.439] Executed solver; result: UNSAT.
[1.439] 
[1.439] *************************************
[1.439] * * *   M a k e s p a n     2   * * *
[1.439] *************************************
[1.504] Computed next depth properties: array size of 169.
[1.864] Instantiated 263,294 transitional clauses.
[1.997] Instantiated 796,776 universal clauses.
[1.997] Instantiated and added clauses for a total of 1,274,490 clauses.
[1.997] The encoding contains a total of 202,037 distinct variables.
[1.997] Attempting solve with solver <glucose4> ...
c 169 assumptions
[1.997] Executed solver; result: UNSAT.
[1.997] 
[1.997] *************************************
[1.997] * * *   M a k e s p a n     3   * * *
[1.997] *************************************
[2.215] Computed next depth properties: array size of 337.
[4.466] Instantiated 1,388,634 transitional clauses.
[4.795] Instantiated 2,085,548 universal clauses.
[4.795] Instantiated and added clauses for a total of 4,748,672 clauses.
[4.795] The encoding contains a total of 522,927 distinct variables.
[4.795] Attempting solve with solver <glucose4> ...
c 337 assumptions
[4.797] Executed solver; result: UNSAT.
[4.797] 
[4.797] *************************************
[4.797] * * *   M a k e s p a n     4   * * *
[4.797] *************************************
[5.174] Computed next depth properties: array size of 505.
[9.808] Instantiated 2,658,400 transitional clauses.
[10.291] Instantiated 2,926,956 universal clauses.
[10.291] Instantiated and added clauses for a total of 10,334,028 clauses.
[10.291] The encoding contains a total of 889,209 distinct variables.
[10.291] Attempting solve with solver <glucose4> ...
c 505 assumptions
[10.295] Executed solver; result: UNSAT.
[10.295] 
[10.295] *************************************
[10.295] * * *   M a k e s p a n     5   * * *
[10.295] *************************************
[10.735] Computed next depth properties: array size of 673.
[15.627] Instantiated 2,741,056 transitional clauses.
[16.186] Instantiated 3,062,060 universal clauses.
[16.186] Instantiated and added clauses for a total of 16,137,144 clauses.
[16.186] The encoding contains a total of 1,257,955 distinct variables.
[16.186] Attempting solve with solver <glucose4> ...
c 673 assumptions
[16.812] Executed solver; result: UNSAT.
[16.812] 
[16.812] *************************************
[16.812] * * *   M a k e s p a n     6   * * *
[16.813] *************************************
[17.283] Computed next depth properties: array size of 841.
[22.291] Instantiated 2,741,056 transitional clauses.
[22.929] Instantiated 3,150,796 universal clauses.
[22.929] Instantiated and added clauses for a total of 22,028,996 clauses.
[22.929] The encoding contains a total of 1,626,869 distinct variables.
[22.929] Attempting solve with solver <glucose4> ...
c 841 assumptions
c last restart ## conflicts  :  21 9565 
[24.243] Executed solver; result: SAT.
[24.243] Solver returned SAT; a solution has been found at makespan 6.
75
solution 2504 1
336 2504 742 347 1901 512 1559 350 483 1094 344 395 371 372 316 66 331 2192 874 366 1241 579 526 332 530 261 51 323 69 5 326 1081 548 339 1368 336 657 756 757 184 33 279 59 112 18 349 941 942 307 63 344 177 29 1812 638 350 204 36 342 1037 333 780 271 54 358 1049 328 838 105 11 234 50 369 194 34 
[24.254] Exiting.

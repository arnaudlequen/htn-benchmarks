Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.012] Processed problem encoding.
[0.014] Calculated possible fact changes of composite elements.
[0.015] Initialized instantiation procedure.
[0.015] 
[0.015] *************************************
[0.015] * * *   M a k e s p a n     0   * * *
[0.015] *************************************
[0.016] Instantiated 1,537 initial clauses.
[0.016] The encoding contains a total of 966 distinct variables.
[0.016] Attempting solve with solver <glucose4> ...
c 6 assumptions
[0.016] Executed solver; result: UNSAT.
[0.016] 
[0.016] *************************************
[0.016] * * *   M a k e s p a n     1   * * *
[0.016] *************************************
[0.016] Computed next depth properties: array size of 21.
[0.018] Instantiated 1,879 transitional clauses.
[0.021] Instantiated 7,981 universal clauses.
[0.021] Instantiated and added clauses for a total of 11,397 clauses.
[0.021] The encoding contains a total of 3,078 distinct variables.
[0.021] Attempting solve with solver <glucose4> ...
c 21 assumptions
[0.021] Executed solver; result: UNSAT.
[0.021] 
[0.021] *************************************
[0.021] * * *   M a k e s p a n     2   * * *
[0.021] *************************************
[0.023] Computed next depth properties: array size of 61.
[0.027] Instantiated 5,449 transitional clauses.
[0.034] Instantiated 21,604 universal clauses.
[0.034] Instantiated and added clauses for a total of 38,450 clauses.
[0.034] The encoding contains a total of 7,298 distinct variables.
[0.034] Attempting solve with solver <glucose4> ...
c 61 assumptions
c last restart ## conflicts  :  6 84 
[0.036] Executed solver; result: SAT.
[0.036] Solver returned SAT; a solution has been found at makespan 2.
24
solution 153 1
153 86 39 20 7 119 32 73 100 3 37 106 35 81 23 8 16 6 40 56 58 33 29 11 
[0.036] Exiting.

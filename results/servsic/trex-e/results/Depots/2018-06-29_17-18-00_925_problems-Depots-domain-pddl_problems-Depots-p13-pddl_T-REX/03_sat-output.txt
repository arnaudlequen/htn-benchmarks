Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.030] Processed problem encoding.
[0.035] Calculated possible fact changes of composite elements.
[0.036] Initialized instantiation procedure.
[0.036] 
[0.036] *************************************
[0.036] * * *   M a k e s p a n     0   * * *
[0.036] *************************************
[0.037] Instantiated 2,688 initial clauses.
[0.037] The encoding contains a total of 1,661 distinct variables.
[0.037] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.037] Executed solver; result: UNSAT.
[0.037] 
[0.037] *************************************
[0.037] * * *   M a k e s p a n     1   * * *
[0.037] *************************************
[0.038] Computed next depth properties: array size of 25.
[0.041] Instantiated 2,658 transitional clauses.
[0.045] Instantiated 10,842 universal clauses.
[0.045] Instantiated and added clauses for a total of 16,188 clauses.
[0.045] The encoding contains a total of 4,570 distinct variables.
[0.045] Attempting solve with solver <glucose4> ...
c 25 assumptions
[0.045] Executed solver; result: UNSAT.
[0.045] 
[0.045] *************************************
[0.045] * * *   M a k e s p a n     2   * * *
[0.045] *************************************
[0.048] Computed next depth properties: array size of 73.
[0.056] Instantiated 9,096 transitional clauses.
[0.065] Instantiated 30,954 universal clauses.
[0.065] Instantiated and added clauses for a total of 56,238 clauses.
[0.065] The encoding contains a total of 9,906 distinct variables.
[0.065] Attempting solve with solver <glucose4> ...
c 73 assumptions
[0.066] Executed solver; result: UNSAT.
[0.066] 
[0.066] *************************************
[0.066] * * *   M a k e s p a n     3   * * *
[0.066] *************************************
[0.071] Computed next depth properties: array size of 145.
[0.085] Instantiated 17,856 transitional clauses.
[0.100] Instantiated 54,282 universal clauses.
[0.100] Instantiated and added clauses for a total of 128,376 clauses.
[0.100] The encoding contains a total of 18,464 distinct variables.
[0.100] Attempting solve with solver <glucose4> ...
c 145 assumptions
[0.100] Executed solver; result: UNSAT.
[0.100] 
[0.100] *************************************
[0.100] * * *   M a k e s p a n     4   * * *
[0.100] *************************************
[0.107] Computed next depth properties: array size of 217.
[0.129] Instantiated 36,378 transitional clauses.
[0.148] Instantiated 79,662 universal clauses.
[0.148] Instantiated and added clauses for a total of 244,416 clauses.
[0.148] The encoding contains a total of 28,576 distinct variables.
[0.148] Attempting solve with solver <glucose4> ...
c 217 assumptions
c last restart ## conflicts  :  2 1479 
[0.153] Executed solver; result: SAT.
[0.153] Solver returned SAT; a solution has been found at makespan 4.
27
solution 257 1
27 257 63 133 223 121 117 119 46 87 41 88 10 3 25 11 4 75 13 5 204 6 147 7 62 19 8 
[0.154] Exiting.

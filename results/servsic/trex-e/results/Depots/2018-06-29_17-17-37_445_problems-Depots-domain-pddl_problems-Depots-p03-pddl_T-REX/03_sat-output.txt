Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.015] Processed problem encoding.
[0.016] Calculated possible fact changes of composite elements.
[0.017] Initialized instantiation procedure.
[0.017] 
[0.017] *************************************
[0.017] * * *   M a k e s p a n     0   * * *
[0.017] *************************************
[0.018] Instantiated 1,670 initial clauses.
[0.018] The encoding contains a total of 1,042 distinct variables.
[0.018] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.018] Executed solver; result: UNSAT.
[0.018] 
[0.018] *************************************
[0.018] * * *   M a k e s p a n     1   * * *
[0.018] *************************************
[0.019] Computed next depth properties: array size of 25.
[0.021] Instantiated 2,229 transitional clauses.
[0.025] Instantiated 9,531 universal clauses.
[0.025] Instantiated and added clauses for a total of 13,430 clauses.
[0.025] The encoding contains a total of 3,566 distinct variables.
[0.025] Attempting solve with solver <glucose4> ...
c 25 assumptions
[0.025] Executed solver; result: UNSAT.
[0.025] 
[0.025] *************************************
[0.025] * * *   M a k e s p a n     2   * * *
[0.025] *************************************
[0.028] Computed next depth properties: array size of 73.
[0.034] Instantiated 6,786 transitional clauses.
[0.043] Instantiated 26,937 universal clauses.
[0.043] Instantiated and added clauses for a total of 47,153 clauses.
[0.043] The encoding contains a total of 8,818 distinct variables.
[0.043] Attempting solve with solver <glucose4> ...
c 73 assumptions
[0.044] Executed solver; result: UNSAT.
[0.044] 
[0.044] *************************************
[0.044] * * *   M a k e s p a n     3   * * *
[0.044] *************************************
[0.048] Computed next depth properties: array size of 145.
[0.062] Instantiated 18,609 transitional clauses.
[0.078] Instantiated 53,043 universal clauses.
[0.078] Instantiated and added clauses for a total of 118,805 clauses.
[0.078] The encoding contains a total of 17,301 distinct variables.
[0.078] Attempting solve with solver <glucose4> ...
c 145 assumptions
[0.079] Executed solver; result: UNSAT.
[0.079] 
[0.079] *************************************
[0.079] * * *   M a k e s p a n     4   * * *
[0.079] *************************************
[0.085] Computed next depth properties: array size of 217.
[0.103] Instantiated 30,858 transitional clauses.
[0.122] Instantiated 74,442 universal clauses.
[0.122] Instantiated and added clauses for a total of 224,105 clauses.
[0.122] The encoding contains a total of 27,143 distinct variables.
[0.122] Attempting solve with solver <glucose4> ...
c 217 assumptions
c last restart ## conflicts  :  24 1291 
[0.134] Executed solver; result: SAT.
[0.134] Solver returned SAT; a solution has been found at makespan 4.
32
solution 158 1
45 54 56 42 149 104 93 95 22 6 49 24 7 158 39 82 112 41 80 69 71 31 11 46 20 5 49 28 8 42 38 14 
[0.134] Exiting.

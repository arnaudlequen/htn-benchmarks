Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 32 initial clauses.
[0.001] The encoding contains a total of 20 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 8.
[0.001] Instantiated 79 transitional clauses.
[0.001] Instantiated 322 universal clauses.
[0.001] Instantiated and added clauses for a total of 433 clauses.
[0.001] The encoding contains a total of 120 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 8 assumptions
c last restart ## conflicts  :  0 46 
[0.001] Executed solver; result: SAT.
[0.001] Solver returned SAT; a solution has been found at makespan 1.
7
solution 32 1
2 4 11 18 25 32 3 
[0.001] Exiting.

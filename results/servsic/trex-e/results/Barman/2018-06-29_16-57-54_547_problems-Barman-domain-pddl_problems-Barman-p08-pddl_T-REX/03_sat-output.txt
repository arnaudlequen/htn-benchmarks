Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.005] Processed problem encoding.
[0.005] Calculated possible fact changes of composite elements.
[0.005] Initialized instantiation procedure.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     0   * * *
[0.005] *************************************
[0.006] Instantiated 1,266 initial clauses.
[0.006] The encoding contains a total of 743 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 17 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.007] Computed next depth properties: array size of 62.
[0.008] Instantiated 296 transitional clauses.
[0.009] Instantiated 2,498 universal clauses.
[0.009] Instantiated and added clauses for a total of 4,060 clauses.
[0.009] The encoding contains a total of 1,877 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 62 assumptions
[0.009] Executed solver; result: UNSAT.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     2   * * *
[0.009] *************************************
[0.012] Computed next depth properties: array size of 192.
[0.013] Instantiated 884 transitional clauses.
[0.018] Instantiated 9,964 universal clauses.
[0.018] Instantiated and added clauses for a total of 14,908 clauses.
[0.018] The encoding contains a total of 4,267 distinct variables.
[0.018] Attempting solve with solver <glucose4> ...
c 192 assumptions
c last restart ## conflicts  :  13 244 
[0.020] Executed solver; result: SAT.
[0.020] Solver returned SAT; a solution has been found at makespan 2.
191
solution 354 1
3 36 129 130 39 229 125 37 285 321 322 323 287 288 4 40 134 135 44 231 132 41 280 310 332 311 282 283 6 48 142 143 50 235 140 46 280 328 334 329 282 283 8 56 148 149 54 239 154 52 280 314 336 315 282 283 10 60 156 157 62 244 243 58 280 290 338 291 282 283 12 68 162 163 66 249 168 64 280 281 340 284 282 283 14 70 172 173 74 253 170 71 280 302 342 303 282 283 16 78 180 181 80 257 178 76 280 294 344 295 282 283 18 84 188 189 86 261 186 82 280 324 346 325 282 283 20 92 194 195 90 265 200 88 280 298 348 299 282 283 22 96 204 205 98 269 202 94 280 306 350 307 282 283 24 102 212 213 104 273 210 100 280 324 352 325 282 283 26 106 107 28 110 111 30 114 115 32 120 220 221 122 277 218 118 280 294 354 295 282 283 
[0.021] Exiting.

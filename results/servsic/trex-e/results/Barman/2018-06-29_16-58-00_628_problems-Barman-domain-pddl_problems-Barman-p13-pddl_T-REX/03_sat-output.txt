Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.006] Processed problem encoding.
[0.006] Calculated possible fact changes of composite elements.
[0.007] Initialized instantiation procedure.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     0   * * *
[0.007] *************************************
[0.008] Instantiated 1,348 initial clauses.
[0.008] The encoding contains a total of 787 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 17 assumptions
[0.008] Executed solver; result: UNSAT.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     1   * * *
[0.008] *************************************
[0.009] Computed next depth properties: array size of 64.
[0.009] Instantiated 312 transitional clauses.
[0.011] Instantiated 2,526 universal clauses.
[0.011] Instantiated and added clauses for a total of 4,186 clauses.
[0.011] The encoding contains a total of 1,983 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 64 assumptions
[0.011] Executed solver; result: UNSAT.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     2   * * *
[0.011] *************************************
[0.014] Computed next depth properties: array size of 214.
[0.015] Instantiated 1,020 transitional clauses.
[0.022] Instantiated 11,372 universal clauses.
[0.022] Instantiated and added clauses for a total of 16,578 clauses.
[0.022] The encoding contains a total of 4,733 distinct variables.
[0.022] Attempting solve with solver <glucose4> ...
c 214 assumptions
c last restart ## conflicts  :  13 262 
[0.025] Executed solver; result: SAT.
[0.025] Solver returned SAT; a solution has been found at makespan 2.
213
solution 386 1
3 39 133 134 36 251 129 37 313 357 358 359 315 316 4 44 136 137 42 253 142 40 308 326 360 327 310 311 6 48 146 147 50 257 144 46 308 350 362 351 310 311 8 52 152 153 56 261 158 53 308 346 364 347 310 311 10 58 160 161 62 265 166 59 308 330 366 331 310 311 12 64 168 169 68 269 174 65 308 334 368 335 310 311 14 70 176 177 74 273 182 71 308 309 370 312 310 311 16 80 186 187 76 277 184 77 308 342 372 343 310 311 18 86 192 193 84 281 198 82 308 338 374 339 310 311 20 92 200 201 90 285 206 88 308 322 376 323 310 311 22 98 208 209 96 289 214 94 308 318 378 319 310 311 24 104 216 217 102 293 222 100 308 322 380 323 310 311 26 110 224 225 108 297 230 106 308 322 382 323 310 311 28 112 232 233 116 301 238 113 308 309 384 312 310 311 30 120 118 32 122 240 241 126 305 246 123 308 334 386 335 310 311 
[0.025] Exiting.

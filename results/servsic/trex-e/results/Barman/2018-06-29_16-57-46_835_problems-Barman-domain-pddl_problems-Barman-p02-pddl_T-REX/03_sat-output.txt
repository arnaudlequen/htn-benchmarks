Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.005] Processed problem encoding.
[0.005] Calculated possible fact changes of composite elements.
[0.005] Initialized instantiation procedure.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     0   * * *
[0.005] *************************************
[0.006] Instantiated 1,130 initial clauses.
[0.006] The encoding contains a total of 663 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.007] Computed next depth properties: array size of 54.
[0.007] Instantiated 256 transitional clauses.
[0.009] Instantiated 2,214 universal clauses.
[0.009] Instantiated and added clauses for a total of 3,600 clauses.
[0.009] The encoding contains a total of 1,645 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 54 assumptions
[0.009] Executed solver; result: UNSAT.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     2   * * *
[0.009] *************************************
[0.010] Computed next depth properties: array size of 164.
[0.011] Instantiated 748 transitional clauses.
[0.016] Instantiated 8,488 universal clauses.
[0.016] Instantiated and added clauses for a total of 12,836 clauses.
[0.016] The encoding contains a total of 3,669 distinct variables.
[0.016] Attempting solve with solver <glucose4> ...
c 164 assumptions
c last restart ## conflicts  :  10 192 
[0.018] Executed solver; result: SAT.
[0.018] Solver returned SAT; a solution has been found at makespan 2.
163
solution 311 1
2 32 108 109 34 190 189 30 240 258 259 260 242 243 4 38 114 115 40 196 195 36 240 272 292 273 242 243 6 46 122 123 44 201 120 42 240 288 294 289 242 243 8 48 128 129 52 205 134 49 240 280 296 281 242 243 10 58 138 139 54 209 136 55 240 284 298 285 242 243 12 62 144 145 64 213 150 60 240 268 300 269 242 243 14 68 152 153 70 218 217 66 240 264 302 265 242 243 16 72 158 159 76 224 223 73 240 241 304 244 242 243 18 82 166 167 78 229 164 79 240 250 306 251 242 243 21 86 175 176 89 235 179 87 245 278 309 279 247 248 23 95 185 186 92 239 181 93 245 256 311 257 247 248 25 99 97 26 102 100 28 106 104 
[0.018] Exiting.

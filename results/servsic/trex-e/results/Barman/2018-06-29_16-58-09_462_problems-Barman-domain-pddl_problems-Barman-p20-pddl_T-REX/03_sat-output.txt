Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.005] Processed problem encoding.
[0.006] Calculated possible fact changes of composite elements.
[0.006] Initialized instantiation procedure.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     0   * * *
[0.006] *************************************
[0.007] Instantiated 1,264 initial clauses.
[0.007] The encoding contains a total of 739 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 16 assumptions
[0.007] Executed solver; result: UNSAT.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     1   * * *
[0.007] *************************************
[0.008] Computed next depth properties: array size of 60.
[0.008] Instantiated 292 transitional clauses.
[0.010] Instantiated 2,368 universal clauses.
[0.010] Instantiated and added clauses for a total of 3,924 clauses.
[0.010] The encoding contains a total of 1,859 distinct variables.
[0.010] Attempting solve with solver <glucose4> ...
c 60 assumptions
[0.010] Executed solver; result: UNSAT.
[0.010] 
[0.010] *************************************
[0.010] * * *   M a k e s p a n     2   * * *
[0.010] *************************************
[0.012] Computed next depth properties: array size of 200.
[0.014] Instantiated 952 transitional clauses.
[0.020] Instantiated 10,618 universal clauses.
[0.020] Instantiated and added clauses for a total of 15,494 clauses.
[0.020] The encoding contains a total of 4,426 distinct variables.
[0.020] Attempting solve with solver <glucose4> ...
c 200 assumptions
c last restart ## conflicts  :  12 243 
[0.022] Executed solver; result: SAT.
[0.022] Solver returned SAT; a solution has been found at makespan 2.
199
solution 364 1
3 35 125 126 37 233 121 33 293 309 310 311 295 296 4 40 130 131 42 235 128 38 288 302 340 303 290 291 6 48 138 139 46 239 136 44 288 316 342 317 290 291 8 54 144 145 52 243 150 50 288 289 344 292 290 291 10 58 152 153 60 247 158 56 288 324 346 325 290 291 12 64 162 163 66 251 160 62 288 328 348 329 290 291 14 72 168 169 70 255 174 68 288 320 350 321 290 291 16 76 178 179 78 259 176 74 288 336 352 337 290 291 18 84 184 185 82 263 190 80 288 298 354 299 290 291 20 90 192 193 86 268 267 87 288 332 356 333 290 291 22 96 198 199 94 273 204 92 288 312 358 313 290 291 24 100 98 26 104 206 207 106 277 212 102 288 324 360 325 290 291 28 110 216 217 112 281 214 108 288 336 362 337 290 291 30 118 222 223 116 285 228 114 288 289 364 292 290 291 
[0.022] Exiting.

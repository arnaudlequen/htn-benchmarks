Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.002] Processed problem encoding.
[0.002] Calculated possible fact changes of composite elements.
[0.003] Initialized instantiation procedure.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     0   * * *
[0.003] *************************************
[0.003] Instantiated 1,221 initial clauses.
[0.003] The encoding contains a total of 713 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.003] Executed solver; result: UNSAT.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     1   * * *
[0.003] *************************************
[0.003] Computed next depth properties: array size of 57.
[0.004] Instantiated 280 transitional clauses.
[0.004] Instantiated 2,224 universal clauses.
[0.004] Instantiated and added clauses for a total of 3,725 clauses.
[0.004] The encoding contains a total of 1,788 distinct variables.
[0.004] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.004] Executed solver; result: UNSAT.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     2   * * *
[0.004] *************************************
[0.005] Computed next depth properties: array size of 197.
[0.006] Instantiated 952 transitional clauses.
[0.009] Instantiated 10,568 universal clauses.
[0.009] Instantiated and added clauses for a total of 15,245 clauses.
[0.009] The encoding contains a total of 4,352 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 197 assumptions
c last restart ## conflicts  :  10 243 
[0.010] Executed solver; result: SAT.
[0.010] Solver returned SAT; a solution has been found at makespan 2.
196
solution 358 1
2 34 116 117 32 227 114 30 282 296 297 298 284 285 4 36 124 125 40 231 122 37 282 326 334 327 284 285 6 44 132 133 46 235 130 42 282 330 336 331 284 285 8 48 138 139 52 239 144 49 282 314 338 315 284 285 10 58 146 147 56 243 152 54 282 322 340 323 284 285 12 64 154 155 60 247 160 61 282 306 342 307 284 285 14 70 162 163 66 251 168 67 282 302 344 303 284 285 16 76 172 173 74 255 170 72 282 292 346 293 284 285 18 80 178 179 82 259 184 78 282 283 348 286 284 285 20 88 186 187 86 263 192 84 282 318 350 319 284 285 22 94 194 195 90 267 200 91 282 310 352 311 284 285 24 100 202 203 98 271 208 96 282 318 354 319 284 285 26 104 210 211 106 275 216 102 282 283 356 286 284 285 28 108 220 221 112 279 218 109 282 326 358 327 284 285 
[0.010] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.012] Parsed head comment information.
[0.658] Processed problem encoding.
[2.190] Calculated possible fact changes of composite elements.
[2.207] Initialized instantiation procedure.
[2.207] 
[2.207] *************************************
[2.207] * * *   M a k e s p a n     0   * * *
[2.207] *************************************
[2.220] Instantiated 51,364 initial clauses.
[2.220] The encoding contains a total of 27,655 distinct variables.
[2.220] Attempting solve with solver <glucose4> ...
c 45 assumptions
[2.220] Executed solver; result: UNSAT.
[2.220] 
[2.220] *************************************
[2.220] * * *   M a k e s p a n     1   * * *
[2.220] *************************************
[2.383] Computed next depth properties: array size of 177.
[2.526] Instantiated 142,866 transitional clauses.
[2.694] Instantiated 402,187 universal clauses.
[2.694] Instantiated and added clauses for a total of 596,417 clauses.
[2.694] The encoding contains a total of 185,417 distinct variables.
[2.694] Attempting solve with solver <glucose4> ...
c 177 assumptions
[2.694] Executed solver; result: UNSAT.
[2.694] 
[2.694] *************************************
[2.694] * * *   M a k e s p a n     2   * * *
[2.694] *************************************
[3.159] Computed next depth properties: array size of 316.
[3.818] Instantiated 998,323 transitional clauses.
[4.262] Instantiated 1,398,097 universal clauses.
[4.262] Instantiated and added clauses for a total of 2,992,837 clauses.
[4.262] The encoding contains a total of 735,841 distinct variables.
[4.262] Attempting solve with solver <glucose4> ...
c 316 assumptions
[4.262] Executed solver; result: UNSAT.
[4.262] 
[4.262] *************************************
[4.262] * * *   M a k e s p a n     3   * * *
[4.262] *************************************
[5.191] Computed next depth properties: array size of 550.
[6.760] Instantiated 2,932,667 transitional clauses.
[7.517] Instantiated 2,735,717 universal clauses.
[7.517] Instantiated and added clauses for a total of 8,661,221 clauses.
[7.517] The encoding contains a total of 1,567,288 distinct variables.
[7.517] Attempting solve with solver <glucose4> ...
c 550 assumptions
[7.518] Executed solver; result: UNSAT.
[7.518] 
[7.518] *************************************
[7.518] * * *   M a k e s p a n     4   * * *
[7.518] *************************************
[8.839] Computed next depth properties: array size of 835.
[11.127] Instantiated 4,303,439 transitional clauses.
[12.159] Instantiated 3,550,263 universal clauses.
[12.159] Instantiated and added clauses for a total of 16,514,923 clauses.
[12.159] The encoding contains a total of 2,446,837 distinct variables.
[12.159] Attempting solve with solver <glucose4> ...
c 835 assumptions
c last restart ## conflicts  :  54 4695 
[28.630] Executed solver; result: SAT.
[28.630] Solver returned SAT; a solution has been found at makespan 4.
651
solution 7338 1
43 1183 89 1234 90 44 1659 3 1141 89 1235 90 4 2051 105 1497 5 1402 95 1492 96 6 106 1743 125 1515 95 1490 96 126 2359 63 317 13 273 111 366 112 14 64 1720 101 353 111 362 112 102 1854 41 1176 89 1236 43 1179 44 90 42 1654 1688 33 1164 43 1177 44 34 2100 5 1403 105 1499 47 1442 48 106 6 1655 1684 21 1415 47 1441 48 22 2250 131 1393 47 1446 105 1497 106 48 132 1655 1665 2533 13 274 121 372 122 14 1648 1667 9 265 121 373 122 10 1765 5 1144 55 1204 56 6 1654 1698 53 1201 54 2123 1648 1672 13 271 63 318 64 14 1774 41 291 63 319 64 42 1648 1706 79 333 63 317 64 80 1827 107 877 108 1692 39 808 40 1964 5 1403 105 1500 61 1468 62 106 6 1655 1718 91 1488 61 1466 62 92 2317 13 270 61 312 62 14 1648 1711 85 346 61 311 62 86 1836 111 1624 5 1524 87 1595 88 6 112 1739 123 1642 87 1594 88 124 2510 59 830 39 810 40 60 1652 1702 77 844 39 808 40 78 1982 41 1433 61 1470 62 42 1655 1733 109 1504 61 1466 62 110 2344 55 1202 5 1146 113 1252 114 6 56 1654 1679 17 1152 113 1250 114 18 2082 13 271 63 319 79 334 80 64 14 1648 1725 103 354 79 332 80 104 1866 5 1142 43 1181 65 1213 66 44 6 1654 2677 109 1248 65 1211 66 110 3849 71 1082 97 1111 59 1065 60 98 72 2667 93 1108 94 3673 2647 81 720 49 687 50 82 3320 43 1177 5 1146 6 44 1654 2686 113 1250 114 3854 5 1142 43 1179 33 1165 34 44 6 1654 2614 45 1185 33 1164 34 46 3786 43 1177 5 1145 99 1242 100 6 44 1654 2641 77 1224 99 1241 100 78 3809 53 452 27 415 28 54 2573 23 406 24 2911 1655 2597 4223 5 1142 43 1180 47 1187 48 44 6 1654 2569 21 1154 47 1188 48 22 3743 127 633 131 514 51 565 52 132 128 2557 19 535 51 562 52 20 3062 43 1178 23 1155 131 1137 132 24 44 1654 2552 11 1149 12 3720 5 1524 6 1656 2661 87 1594 5 1522 6 88 4160 131 519 127 636 55 572 56 128 132 1650 2619 49 561 50 3129 43 1558 5 1523 49 1563 50 6 44 1656 2545 9 1533 49 1564 50 10 4046 55 575 127 634 7 525 8 128 56 1650 2699 121 629 122 3206 7 526 127 636 55 570 56 128 8 1650 2583 27 542 55 569 56 28 3090 55 1571 49 1562 5 1530 6 50 56 1656 2696 115 1635 116 4192 5 1522 6 1656 2607 4226 59 830 39 809 40 60 1652 2630 67 834 39 808 40 68 3477 61 1318 109 1368 110 62 7338 43 1301 44 5932 53 1309 54 6907 59 1066 97 1112 98 60 7307 71 1078 72 6369 45 1045 71 1082 72 46 6838 7338 43 1302 109 1366 110 44 4465 3 1267 109 1368 110 4 6871 7027 39 169 59 204 97 237 98 60 40 4258 7 136 8 6644 43 1302 109 1366 110 44 7318 3 1267 109 1367 110 4 5105 7 1269 109 1372 110 8 6892 5 521 6 7159 55 575 127 637 59 581 60 128 56 5672 6770 97 1112 98 7307 71 1077 131 1016 83 1094 84 132 72 4817 6814 
[28.638] Exiting.

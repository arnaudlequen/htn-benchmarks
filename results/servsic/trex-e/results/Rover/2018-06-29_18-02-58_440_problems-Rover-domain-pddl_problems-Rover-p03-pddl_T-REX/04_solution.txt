
Compact plan:
0: (sample_soil rover0 rover0store waypoint3)
1: (communicate_soil_data rover0 general waypoint3 waypoint2)
2: (visit waypoint2)
3: (navigate rover1 waypoint2 waypoint1)
4: (unvisit waypoint2)
5: (sample_rock rover1 rover1store waypoint1)
6: (communicate_rock_data rover1 general waypoint1 waypoint2)
7: (calibrate rover1 camera0 objective0 waypoint1)
8: (take_image rover1 waypoint1 objective0 camera0 high_res)
9: (communicate_image_data rover1 general objective0 high_res waypoint1 waypoint2)



Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.002] Parsed head comment information.
[0.143] Processed problem encoding.
[0.225] Calculated possible fact changes of composite elements.
[0.227] Initialized instantiation procedure.
[0.227] 
[0.227] *************************************
[0.227] * * *   M a k e s p a n     0   * * *
[0.227] *************************************
[0.229] Instantiated 7,601 initial clauses.
[0.229] The encoding contains a total of 4,392 distinct variables.
[0.229] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.229] Executed solver; result: UNSAT.
[0.229] 
[0.229] *************************************
[0.229] * * *   M a k e s p a n     1   * * *
[0.229] *************************************
[0.241] Computed next depth properties: array size of 57.
[0.258] Instantiated 25,129 transitional clauses.
[0.276] Instantiated 68,240 universal clauses.
[0.276] Instantiated and added clauses for a total of 100,970 clauses.
[0.276] The encoding contains a total of 29,914 distinct variables.
[0.276] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.276] Executed solver; result: UNSAT.
[0.276] 
[0.276] *************************************
[0.276] * * *   M a k e s p a n     2   * * *
[0.276] *************************************
[0.312] Computed next depth properties: array size of 102.
[0.378] Instantiated 148,250 transitional clauses.
[0.421] Instantiated 206,822 universal clauses.
[0.421] Instantiated and added clauses for a total of 456,042 clauses.
[0.421] The encoding contains a total of 105,966 distinct variables.
[0.421] Attempting solve with solver <glucose4> ...
c 102 assumptions
[0.422] Executed solver; result: UNSAT.
[0.422] 
[0.422] *************************************
[0.422] * * *   M a k e s p a n     3   * * *
[0.422] *************************************
[0.487] Computed next depth properties: array size of 178.
[0.658] Instantiated 395,215 transitional clauses.
[0.738] Instantiated 363,504 universal clauses.
[0.738] Instantiated and added clauses for a total of 1,214,761 clauses.
[0.738] The encoding contains a total of 212,814 distinct variables.
[0.738] Attempting solve with solver <glucose4> ...
c 178 assumptions
[0.881] Executed solver; result: UNSAT.
[0.881] 
[0.881] *************************************
[0.881] * * *   M a k e s p a n     4   * * *
[0.881] *************************************
[0.978] Computed next depth properties: array size of 271.
[1.202] Instantiated 542,780 transitional clauses.
[1.300] Instantiated 455,181 universal clauses.
[1.300] Instantiated and added clauses for a total of 2,212,722 clauses.
[1.300] The encoding contains a total of 324,744 distinct variables.
[1.300] Attempting solve with solver <glucose4> ...
c 271 assumptions
c last restart ## conflicts  :  52 1201 
[1.513] Executed solver; result: SAT.
[1.513] Solver returned SAT; a solution has been found at makespan 4.
180
solution 1999 1
21 433 22 512 47 458 48 841 41 180 3 145 4 42 480 855 63 271 64 496 29 243 63 267 64 30 684 51 325 21 306 22 52 516 69 336 21 300 22 70 777 19 88 7 81 61 131 62 8 20 503 37 106 61 127 62 38 562 11 293 21 305 51 327 52 22 12 476 501 33 312 51 322 52 34 751 29 442 47 457 21 430 22 48 30 478 493 25 438 26 804 71 142 72 474 485 13 154 14 596 71 281 51 328 52 72 476 867 49 321 51 322 52 50 907 17 360 25 373 26 18 872 67 403 25 371 26 68 943 17 362 63 399 64 18 477 864 977 1999 71 278 5 287 6 72 1452 39 315 40 1808 71 143 72 1926 47 190 71 141 72 48 996 3 145 4 1754 5 77 6 1899 61 128 7 79 8 62 1644 35 104 36 1747 
[1.515] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.007] Processed problem encoding.
[0.008] Calculated possible fact changes of composite elements.
[0.009] Initialized instantiation procedure.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     0   * * *
[0.009] *************************************
[0.012] Instantiated 3,965 initial clauses.
[0.012] The encoding contains a total of 2,325 distinct variables.
[0.012] Attempting solve with solver <glucose4> ...
c 76 assumptions
[0.013] Executed solver; result: UNSAT.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     1   * * *
[0.013] *************************************
[0.020] Computed next depth properties: array size of 451.
[0.024] Instantiated 2,025 transitional clauses.
[0.039] Instantiated 18,659 universal clauses.
[0.039] Instantiated and added clauses for a total of 24,649 clauses.
[0.039] The encoding contains a total of 9,537 distinct variables.
[0.039] Attempting solve with solver <glucose4> ...
c 451 assumptions
c last restart ## conflicts  :  0 758 
[0.040] Executed solver; result: SAT.
[0.040] Solver returned SAT; a solution has been found at makespan 1.
449
solution 601 1
3 4 5 6 7 2 14 16 5 15 17 2 22 24 5 23 25 2 30 32 5 31 33 2 38 40 5 39 41 2 46 48 5 47 49 2 54 56 5 55 57 2 62 64 5 63 65 2 70 72 5 71 73 2 78 80 5 79 81 2 86 88 5 87 89 2 94 96 5 95 97 2 102 104 5 103 105 2 110 112 5 111 113 2 118 120 5 119 121 2 126 128 5 127 129 2 134 136 5 135 137 2 142 144 5 143 145 2 150 152 5 151 153 2 158 160 5 159 161 2 166 168 5 167 169 2 174 176 5 175 177 2 182 184 5 183 185 2 190 192 5 191 193 2 198 200 5 199 201 2 206 208 5 207 209 2 214 216 5 215 217 2 222 224 5 223 225 2 230 232 5 231 233 2 238 240 5 239 241 2 246 248 5 247 249 2 254 256 5 255 257 2 262 264 5 263 265 2 270 272 5 271 273 2 278 280 5 279 281 2 286 288 5 287 289 2 294 296 5 295 297 2 302 304 5 303 305 2 310 312 5 311 313 2 318 320 5 319 321 2 326 328 5 327 329 2 334 336 5 335 337 2 342 344 5 343 345 2 350 352 5 351 353 2 358 360 5 359 361 2 366 368 5 367 369 2 374 376 5 375 377 2 382 384 5 383 385 2 390 392 5 391 393 2 398 400 5 399 401 2 406 408 5 407 409 2 414 416 5 415 417 2 422 424 5 423 425 2 430 432 5 431 433 2 438 440 5 439 441 2 446 448 5 447 449 2 454 456 5 455 457 2 462 464 5 463 465 2 470 472 5 471 473 2 478 480 5 479 481 2 486 488 5 487 489 2 494 496 5 495 497 2 502 504 5 503 505 2 510 512 5 511 513 2 518 520 5 519 521 2 526 528 5 527 529 2 534 536 5 535 537 2 542 544 5 543 545 2 550 552 5 551 553 2 558 560 5 559 561 2 566 568 5 567 569 2 574 576 5 575 577 2 582 584 5 583 585 2 590 592 5 591 593 2 598 600 5 599 601 
[0.041] Exiting.

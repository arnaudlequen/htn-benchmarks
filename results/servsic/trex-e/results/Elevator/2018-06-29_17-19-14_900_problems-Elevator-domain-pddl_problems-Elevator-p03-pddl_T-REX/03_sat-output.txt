Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.000] Processed problem encoding.
[0.000] Calculated possible fact changes of composite elements.
[0.000] Initialized instantiation procedure.
[0.000] 
[0.000] *************************************
[0.000] * * *   M a k e s p a n     0   * * *
[0.000] *************************************
[0.000] Instantiated 17 initial clauses.
[0.000] The encoding contains a total of 14 distinct variables.
[0.000] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.000] Executed solver; result: UNSAT.
[0.000] 
[0.000] *************************************
[0.000] * * *   M a k e s p a n     1   * * *
[0.000] *************************************
[0.000] Computed next depth properties: array size of 3.
[0.000] Instantiated 9 transitional clauses.
[0.000] Instantiated 33 universal clauses.
[0.000] Instantiated and added clauses for a total of 59 clauses.
[0.000] The encoding contains a total of 34 distinct variables.
[0.000] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.000] Executed solver; result: UNSAT.
[0.000] 
[0.000] *************************************
[0.000] * * *   M a k e s p a n     2   * * *
[0.000] *************************************
[0.000] Computed next depth properties: array size of 5.
[0.000] Instantiated 12 transitional clauses.
[0.001] Instantiated 64 universal clauses.
[0.001] Instantiated and added clauses for a total of 135 clauses.
[0.001] The encoding contains a total of 59 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 5 assumptions
c last restart ## conflicts  :  0 12 
[0.001] Executed solver; result: SAT.
[0.001] Solver returned SAT; a solution has been found at makespan 2.
4
solution 5 1
3 2 5 4 
[0.001] Exiting.

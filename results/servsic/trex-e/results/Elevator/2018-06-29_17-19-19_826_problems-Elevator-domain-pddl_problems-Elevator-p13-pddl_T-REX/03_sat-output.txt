Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 79 initial clauses.
[0.001] The encoding contains a total of 50 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 7.
[0.001] Instantiated 51 transitional clauses.
[0.001] Instantiated 227 universal clauses.
[0.001] Instantiated and added clauses for a total of 357 clauses.
[0.001] The encoding contains a total of 150 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     2   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 13.
[0.001] Instantiated 108 transitional clauses.
[0.001] Instantiated 548 universal clauses.
[0.001] Instantiated and added clauses for a total of 1,013 clauses.
[0.001] The encoding contains a total of 281 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 13 assumptions
c last restart ## conflicts  :  0 22 
[0.001] Executed solver; result: SAT.
[0.001] Solver returned SAT; a solution has been found at makespan 2.
12
solution 26 1
12 2 23 20 13 3 26 21 17 4 25 22 
[0.001] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 122 initial clauses.
[0.001] The encoding contains a total of 74 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 5 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 9.
[0.002] Instantiated 84 transitional clauses.
[0.002] Instantiated 384 universal clauses.
[0.002] Instantiated and added clauses for a total of 590 clauses.
[0.002] The encoding contains a total of 238 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 9 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     2   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 17.
[0.002] Instantiated 192 transitional clauses.
[0.003] Instantiated 1,012 universal clauses.
[0.003] Instantiated and added clauses for a total of 1,794 clauses.
[0.003] The encoding contains a total of 452 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 17 assumptions
c last restart ## conflicts  :  0 27 
[0.003] Executed solver; result: SAT.
[0.003] Solver returned SAT; a solution has been found at makespan 2.
15
solution 56 1
14 2 10 34 3 40 35 27 4 51 36 32 5 56 37 
[0.003] Exiting.

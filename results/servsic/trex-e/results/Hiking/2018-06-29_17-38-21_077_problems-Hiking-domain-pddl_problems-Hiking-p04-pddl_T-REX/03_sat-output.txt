Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.115] Processed problem encoding.
[0.132] Calculated possible fact changes of composite elements.
[0.132] Initialized instantiation procedure.
[0.132] 
[0.132] *************************************
[0.132] * * *   M a k e s p a n     0   * * *
[0.132] *************************************
[0.133] Instantiated 327 initial clauses.
[0.133] The encoding contains a total of 224 distinct variables.
[0.133] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.133] Executed solver; result: UNSAT.
[0.133] 
[0.133] *************************************
[0.133] * * *   M a k e s p a n     1   * * *
[0.133] *************************************
[0.133] Computed next depth properties: array size of 3.
[0.134] Instantiated 50 transitional clauses.
[0.134] Instantiated 422 universal clauses.
[0.134] Instantiated and added clauses for a total of 799 clauses.
[0.134] The encoding contains a total of 368 distinct variables.
[0.134] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.134] Executed solver; result: UNSAT.
[0.134] 
[0.134] *************************************
[0.134] * * *   M a k e s p a n     2   * * *
[0.134] *************************************
[0.136] Computed next depth properties: array size of 4.
[0.140] Instantiated 12,057 transitional clauses.
[0.145] Instantiated 55,046 universal clauses.
[0.145] Instantiated and added clauses for a total of 67,902 clauses.
[0.145] The encoding contains a total of 4,881 distinct variables.
[0.145] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.146] Executed solver; result: UNSAT.
[0.146] 
[0.146] *************************************
[0.146] * * *   M a k e s p a n     3   * * *
[0.146] *************************************
[0.148] Computed next depth properties: array size of 14.
[0.163] Instantiated 25,963 transitional clauses.
[0.169] Instantiated 63,687 universal clauses.
[0.169] Instantiated and added clauses for a total of 157,552 clauses.
[0.169] The encoding contains a total of 9,217 distinct variables.
[0.169] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.169] Executed solver; result: UNSAT.
[0.169] 
[0.169] *************************************
[0.169] * * *   M a k e s p a n     4   * * *
[0.169] *************************************
[0.172] Computed next depth properties: array size of 32.
[0.185] Instantiated 23,240 transitional clauses.
[0.193] Instantiated 30,846 universal clauses.
[0.193] Instantiated and added clauses for a total of 211,638 clauses.
[0.193] The encoding contains a total of 12,361 distinct variables.
[0.193] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.193] Executed solver; result: UNSAT.
[0.193] 
[0.193] *************************************
[0.193] * * *   M a k e s p a n     5   * * *
[0.193] *************************************
[0.197] Computed next depth properties: array size of 52.
[0.211] Instantiated 22,203 transitional clauses.
[0.221] Instantiated 30,243 universal clauses.
[0.221] Instantiated and added clauses for a total of 264,084 clauses.
[0.221] The encoding contains a total of 15,393 distinct variables.
[0.221] Attempting solve with solver <glucose4> ...
c 52 assumptions
[0.221] Executed solver; result: UNSAT.
[0.221] 
[0.221] *************************************
[0.221] * * *   M a k e s p a n     6   * * *
[0.221] *************************************
[0.227] Computed next depth properties: array size of 74.
[0.254] Instantiated 21,122 transitional clauses.
[0.278] Instantiated 29,335 universal clauses.
[0.278] Instantiated and added clauses for a total of 314,541 clauses.
[0.278] The encoding contains a total of 18,282 distinct variables.
[0.278] Attempting solve with solver <glucose4> ...
c 74 assumptions
[0.279] Executed solver; result: UNSAT.
[0.279] 
[0.279] *************************************
[0.279] * * *   M a k e s p a n     7   * * *
[0.279] *************************************
[0.293] Computed next depth properties: array size of 98.
[0.323] Instantiated 19,900 transitional clauses.
[0.342] Instantiated 27,425 universal clauses.
[0.342] Instantiated and added clauses for a total of 361,866 clauses.
[0.342] The encoding contains a total of 20,881 distinct variables.
[0.342] Attempting solve with solver <glucose4> ...
c 98 assumptions
[0.344] Executed solver; result: UNSAT.
[0.344] 
[0.344] *************************************
[0.344] * * *   M a k e s p a n     8   * * *
[0.344] *************************************
[0.357] Computed next depth properties: array size of 108.
[0.381] Instantiated 17,380 transitional clauses.
[0.394] Instantiated 23,499 universal clauses.
[0.394] Instantiated and added clauses for a total of 402,745 clauses.
[0.394] The encoding contains a total of 22,876 distinct variables.
[0.394] Attempting solve with solver <glucose4> ...
c 108 assumptions
[0.396] Executed solver; result: UNSAT.
[0.396] 
[0.396] *************************************
[0.396] * * *   M a k e s p a n     9   * * *
[0.396] *************************************
[0.405] Computed next depth properties: array size of 118.
[0.423] Instantiated 17,380 transitional clauses.
[0.434] Instantiated 24,051 universal clauses.
[0.434] Instantiated and added clauses for a total of 444,176 clauses.
[0.434] The encoding contains a total of 24,881 distinct variables.
[0.435] Attempting solve with solver <glucose4> ...
c 118 assumptions
[0.436] Executed solver; result: UNSAT.
[0.436] 
[0.436] *************************************
[0.436] * * *   M a k e s p a n    10   * * *
[0.436] *************************************
[0.446] Computed next depth properties: array size of 128.
[0.464] Instantiated 17,380 transitional clauses.
[0.477] Instantiated 24,603 universal clauses.
[0.477] Instantiated and added clauses for a total of 486,159 clauses.
[0.477] The encoding contains a total of 26,896 distinct variables.
[0.477] Attempting solve with solver <glucose4> ...
c 128 assumptions
c last restart ## conflicts  :  260 544 
[0.515] Executed solver; result: SAT.
[0.515] Solver returned SAT; a solution has been found at makespan 10.
56
solution 2264 1
2264 9 22 4 23 513 514 515 6 10 11 22 46 54 38 55 518 517 516 40 47 48 54 80 88 72 89 519 520 521 74 81 82 88 111 124 106 125 523 522 524 108 112 113 124 148 156 140 157 525 526 527 142 149 150 156 
[0.516] Exiting.

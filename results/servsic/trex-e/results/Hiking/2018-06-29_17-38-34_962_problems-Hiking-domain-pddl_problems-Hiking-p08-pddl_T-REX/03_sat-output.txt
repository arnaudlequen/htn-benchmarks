Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.053] Processed problem encoding.
[0.057] Calculated possible fact changes of composite elements.
[0.057] Initialized instantiation procedure.
[0.057] 
[0.057] *************************************
[0.057] * * *   M a k e s p a n     0   * * *
[0.057] *************************************
[0.058] Instantiated 228 initial clauses.
[0.058] The encoding contains a total of 155 distinct variables.
[0.058] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.058] Executed solver; result: UNSAT.
[0.058] 
[0.058] *************************************
[0.058] * * *   M a k e s p a n     1   * * *
[0.058] *************************************
[0.058] Computed next depth properties: array size of 3.
[0.058] Instantiated 26 transitional clauses.
[0.058] Instantiated 286 universal clauses.
[0.058] Instantiated and added clauses for a total of 540 clauses.
[0.058] The encoding contains a total of 261 distinct variables.
[0.058] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.058] Executed solver; result: UNSAT.
[0.058] 
[0.058] *************************************
[0.058] * * *   M a k e s p a n     2   * * *
[0.058] *************************************
[0.059] Computed next depth properties: array size of 4.
[0.061] Instantiated 4,690 transitional clauses.
[0.064] Instantiated 21,174 universal clauses.
[0.064] Instantiated and added clauses for a total of 26,404 clauses.
[0.064] The encoding contains a total of 2,166 distinct variables.
[0.064] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.065] Executed solver; result: UNSAT.
[0.065] 
[0.065] *************************************
[0.065] * * *   M a k e s p a n     3   * * *
[0.065] *************************************
[0.066] Computed next depth properties: array size of 14.
[0.073] Instantiated 13,557 transitional clauses.
[0.079] Instantiated 28,336 universal clauses.
[0.079] Instantiated and added clauses for a total of 68,297 clauses.
[0.079] The encoding contains a total of 4,547 distinct variables.
[0.079] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.079] Executed solver; result: UNSAT.
[0.079] 
[0.079] *************************************
[0.079] * * *   M a k e s p a n     4   * * *
[0.079] *************************************
[0.081] Computed next depth properties: array size of 32.
[0.089] Instantiated 14,146 transitional clauses.
[0.094] Instantiated 14,388 universal clauses.
[0.094] Instantiated and added clauses for a total of 96,831 clauses.
[0.094] The encoding contains a total of 6,000 distinct variables.
[0.094] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.094] Executed solver; result: UNSAT.
[0.094] 
[0.094] *************************************
[0.094] * * *   M a k e s p a n     5   * * *
[0.094] *************************************
[0.097] Computed next depth properties: array size of 36.
[0.102] Instantiated 7,248 transitional clauses.
[0.106] Instantiated 6,730 universal clauses.
[0.106] Instantiated and added clauses for a total of 110,809 clauses.
[0.106] The encoding contains a total of 6,616 distinct variables.
[0.106] Attempting solve with solver <glucose4> ...
c 36 assumptions
[0.106] Executed solver; result: UNSAT.
[0.106] 
[0.106] *************************************
[0.106] * * *   M a k e s p a n     6   * * *
[0.106] *************************************
[0.108] Computed next depth properties: array size of 40.
[0.114] Instantiated 7,248 transitional clauses.
[0.118] Instantiated 6,952 universal clauses.
[0.118] Instantiated and added clauses for a total of 125,009 clauses.
[0.118] The encoding contains a total of 7,236 distinct variables.
[0.118] Attempting solve with solver <glucose4> ...
c 40 assumptions
[0.121] Executed solver; result: UNSAT.
[0.121] 
[0.121] *************************************
[0.121] * * *   M a k e s p a n     7   * * *
[0.121] *************************************
[0.123] Computed next depth properties: array size of 44.
[0.128] Instantiated 7,248 transitional clauses.
[0.132] Instantiated 7,174 universal clauses.
[0.132] Instantiated and added clauses for a total of 139,431 clauses.
[0.132] The encoding contains a total of 7,860 distinct variables.
[0.132] Attempting solve with solver <glucose4> ...
c 44 assumptions
[0.135] Executed solver; result: UNSAT.
[0.135] 
[0.135] *************************************
[0.135] * * *   M a k e s p a n     8   * * *
[0.135] *************************************
[0.138] Computed next depth properties: array size of 48.
[0.144] Instantiated 7,248 transitional clauses.
[0.147] Instantiated 7,396 universal clauses.
[0.147] Instantiated and added clauses for a total of 154,075 clauses.
[0.147] The encoding contains a total of 8,488 distinct variables.
[0.147] Attempting solve with solver <glucose4> ...
c 48 assumptions
c last restart ## conflicts  :  28 304 
[0.150] Executed solver; result: SAT.
[0.150] Solver returned SAT; a solution has been found at makespan 8.
25
solution 1024 1
1024 317 291 318 293 398 396 397 395 319 295 296 291 365 339 366 341 401 400 402 399 367 343 344 339 
[0.150] Exiting.

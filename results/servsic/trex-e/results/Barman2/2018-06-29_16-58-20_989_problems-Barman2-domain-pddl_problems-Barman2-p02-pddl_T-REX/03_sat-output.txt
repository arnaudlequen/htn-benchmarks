Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.012] Processed problem encoding.
[0.012] Calculated possible fact changes of composite elements.
[0.013] Initialized instantiation procedure.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     0   * * *
[0.013] *************************************
[0.016] Instantiated 3,271 initial clauses.
[0.016] The encoding contains a total of 1,909 distinct variables.
[0.016] Attempting solve with solver <glucose4> ...
c 45 assumptions
[0.016] Executed solver; result: UNSAT.
[0.016] 
[0.016] *************************************
[0.016] * * *   M a k e s p a n     1   * * *
[0.016] *************************************
[0.020] Computed next depth properties: array size of 163.
[0.022] Instantiated 768 transitional clauses.
[0.029] Instantiated 6,992 universal clauses.
[0.029] Instantiated and added clauses for a total of 11,031 clauses.
[0.029] The encoding contains a total of 4,993 distinct variables.
[0.029] Attempting solve with solver <glucose4> ...
c 163 assumptions
[0.029] Executed solver; result: UNSAT.
[0.029] 
[0.029] *************************************
[0.029] * * *   M a k e s p a n     2   * * *
[0.029] *************************************
[0.039] Computed next depth properties: array size of 463.
[0.042] Instantiated 2,040 transitional clauses.
[0.059] Instantiated 24,312 universal clauses.
[0.059] Instantiated and added clauses for a total of 37,383 clauses.
[0.059] The encoding contains a total of 10,827 distinct variables.
[0.059] Attempting solve with solver <glucose4> ...
c 463 assumptions
c last restart ## conflicts  :  35 538 
[0.071] Executed solver; result: SAT.
[0.071] Solver returned SAT; a solution has been found at makespan 2.
462
solution 870 1
3 93 331 332 95 569 327 91 691 775 776 777 693 694 4 100 334 335 98 571 340 96 686 736 814 737 688 689 6 106 342 343 104 575 348 102 686 720 816 721 688 689 9 113 353 354 111 581 357 109 691 792 819 793 693 694 10 116 358 359 118 583 364 114 686 782 820 783 688 689 12 124 366 367 122 587 372 120 686 687 822 690 688 689 14 130 376 377 128 591 374 126 686 806 824 807 688 689 16 134 382 383 136 595 388 132 686 728 826 729 688 689 19 140 393 394 143 601 397 141 691 758 829 759 693 694 20 146 398 399 148 603 404 144 686 732 830 733 688 689 22 154 406 407 150 607 412 151 686 794 832 795 688 689 24 160 414 415 158 611 420 156 686 696 834 697 688 689 26 166 422 423 162 615 428 163 686 768 836 769 688 689 28 170 430 431 172 619 436 168 686 760 838 761 688 689 30 174 440 441 178 623 438 175 686 712 840 713 688 689 32 182 446 447 184 627 452 180 686 778 842 779 688 689 34 188 454 455 190 631 460 186 686 802 844 803 688 689 36 196 462 463 194 635 468 192 686 764 846 765 688 689 38 202 472 473 200 639 470 198 686 748 848 749 688 689 40 208 478 479 206 643 484 204 686 700 850 701 688 689 42 214 486 487 212 647 492 210 686 798 852 799 688 689 44 218 496 497 220 651 494 216 686 744 854 745 688 689 46 226 502 503 224 655 508 222 686 752 856 753 688 689 48 232 510 511 230 659 516 228 686 740 858 741 688 689 50 236 518 519 238 663 524 234 686 724 860 725 688 689 52 244 526 527 242 667 532 240 686 708 862 709 688 689 54 248 536 537 250 671 534 246 686 716 864 717 688 689 56 254 542 543 256 675 548 252 686 704 866 705 688 689 58 262 550 551 260 679 556 258 686 786 868 787 688 689 60 268 558 559 266 683 564 264 686 810 870 811 688 689 62 272 270 64 276 274 67 281 279 69 285 283 70 286 287 72 292 290 74 296 294 76 300 298 78 304 302 80 308 306 82 312 310 84 316 314 86 320 318 88 324 322 
[0.072] Exiting.

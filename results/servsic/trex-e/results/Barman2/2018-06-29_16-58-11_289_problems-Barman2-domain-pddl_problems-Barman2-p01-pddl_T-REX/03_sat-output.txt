Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.011] Processed problem encoding.
[0.012] Calculated possible fact changes of composite elements.
[0.013] Initialized instantiation procedure.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     0   * * *
[0.013] *************************************
[0.015] Instantiated 3,056 initial clauses.
[0.015] The encoding contains a total of 1,779 distinct variables.
[0.015] Attempting solve with solver <glucose4> ...
c 40 assumptions
[0.016] Executed solver; result: UNSAT.
[0.016] 
[0.016] *************************************
[0.016] * * *   M a k e s p a n     1   * * *
[0.016] *************************************
[0.019] Computed next depth properties: array size of 148.
[0.021] Instantiated 708 transitional clauses.
[0.027] Instantiated 6,242 universal clauses.
[0.027] Instantiated and added clauses for a total of 10,006 clauses.
[0.027] The encoding contains a total of 4,623 distinct variables.
[0.027] Attempting solve with solver <glucose4> ...
c 148 assumptions
[0.027] Executed solver; result: UNSAT.
[0.027] 
[0.027] *************************************
[0.027] * * *   M a k e s p a n     2   * * *
[0.027] *************************************
[0.036] Computed next depth properties: array size of 448.
[0.039] Instantiated 2,040 transitional clauses.
[0.057] Instantiated 24,062 universal clauses.
[0.057] Instantiated and added clauses for a total of 36,108 clauses.
[0.057] The encoding contains a total of 10,442 distinct variables.
[0.057] Attempting solve with solver <glucose4> ...
c 448 assumptions
c last restart ## conflicts  :  28 550 
[0.065] Executed solver; result: SAT.
[0.065] Solver returned SAT; a solution has been found at makespan 2.
447
solution 840 1
3 85 299 300 82 539 303 83 661 765 766 767 663 664 4 88 304 305 90 541 310 86 656 710 784 711 658 659 6 96 312 313 92 545 318 93 656 678 786 679 658 659 8 100 322 323 102 549 320 98 656 776 788 777 658 659 10 106 328 329 108 553 334 104 656 726 790 727 658 659 12 114 336 337 112 557 342 110 656 750 792 751 658 659 15 121 347 348 119 563 351 117 661 662 795 665 663 664 16 126 352 353 124 565 358 122 656 742 796 743 658 659 19 131 365 366 133 571 361 129 661 688 799 689 663 664 20 136 368 369 138 573 374 134 656 780 800 781 658 659 22 144 376 377 142 577 382 140 656 674 802 675 658 659 24 146 384 385 150 581 390 147 656 690 804 691 658 659 26 156 392 393 154 585 398 152 656 734 806 735 658 659 28 162 400 401 160 589 406 158 656 682 808 683 658 659 30 166 410 411 168 593 408 164 656 746 810 747 658 659 32 174 418 419 172 597 416 170 656 758 812 759 658 659 34 180 424 425 178 601 430 176 656 694 814 695 658 659 36 186 432 433 182 605 438 183 656 706 816 707 658 659 38 192 440 441 188 609 446 189 656 702 818 703 658 659 40 198 450 451 196 613 448 194 656 666 820 667 658 659 42 204 456 457 202 617 462 200 656 772 822 773 658 659 44 208 466 467 210 621 464 206 656 714 824 715 658 659 46 214 474 475 216 625 472 212 656 722 826 723 658 659 48 220 482 483 222 629 480 218 656 754 828 755 658 659 50 226 490 491 228 633 488 224 656 730 830 731 658 659 52 232 496 497 234 637 502 230 656 670 832 671 658 659 54 238 506 507 240 641 504 236 656 768 834 769 658 659 56 246 514 515 244 645 512 242 656 738 836 739 658 659 58 252 520 521 250 649 526 248 656 698 838 699 658 659 60 254 528 529 258 653 534 255 656 718 840 719 658 659 62 262 260 64 266 264 66 268 269 68 274 272 70 278 276 73 283 281 74 286 284 76 290 288 78 294 292 
[0.067] Exiting.

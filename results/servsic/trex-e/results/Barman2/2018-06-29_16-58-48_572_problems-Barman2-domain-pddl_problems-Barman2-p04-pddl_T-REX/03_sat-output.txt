Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.012] Processed problem encoding.
[0.013] Calculated possible fact changes of composite elements.
[0.014] Initialized instantiation procedure.
[0.014] 
[0.014] *************************************
[0.014] * * *   M a k e s p a n     0   * * *
[0.014] *************************************
[0.016] Instantiated 3,068 initial clauses.
[0.016] The encoding contains a total of 1,787 distinct variables.
[0.016] Attempting solve with solver <glucose4> ...
c 40 assumptions
[0.016] Executed solver; result: UNSAT.
[0.016] 
[0.016] *************************************
[0.016] * * *   M a k e s p a n     1   * * *
[0.016] *************************************
[0.020] Computed next depth properties: array size of 148.
[0.021] Instantiated 708 transitional clauses.
[0.028] Instantiated 6,250 universal clauses.
[0.028] Instantiated and added clauses for a total of 10,026 clauses.
[0.028] The encoding contains a total of 4,631 distinct variables.
[0.028] Attempting solve with solver <glucose4> ...
c 148 assumptions
[0.028] Executed solver; result: UNSAT.
[0.028] 
[0.028] *************************************
[0.028] * * *   M a k e s p a n     2   * * *
[0.028] *************************************
[0.037] Computed next depth properties: array size of 448.
[0.040] Instantiated 2,040 transitional clauses.
[0.058] Instantiated 24,070 universal clauses.
[0.058] Instantiated and added clauses for a total of 36,136 clauses.
[0.058] The encoding contains a total of 10,450 distinct variables.
[0.058] Attempting solve with solver <glucose4> ...
c 448 assumptions
c last restart ## conflicts  :  27 568 
[0.067] Executed solver; result: SAT.
[0.067] Solver returned SAT; a solution has been found at makespan 2.
447
solution 840 1
3 83 299 300 85 533 303 81 661 693 694 695 663 664 4 90 306 307 88 535 304 86 656 670 784 671 658 659 6 96 312 313 94 539 318 92 656 700 786 701 658 659 8 98 322 323 102 543 320 99 656 772 788 773 658 659 10 108 328 329 106 547 334 104 656 736 790 737 658 659 12 114 336 337 112 551 342 110 656 768 792 769 658 659 14 118 346 347 120 555 344 116 656 686 794 687 658 659 16 126 352 353 124 559 358 122 656 674 796 675 658 659 18 132 362 363 130 563 360 128 656 712 798 713 658 659 20 136 368 369 138 568 567 134 656 756 800 757 658 659 22 144 376 377 142 573 374 140 656 744 802 745 658 659 24 148 382 383 150 577 388 146 656 752 804 753 658 659 26 156 392 393 154 581 390 152 656 732 806 733 658 659 28 160 398 399 162 586 585 158 656 696 808 697 658 659 30 168 406 407 166 591 404 164 656 720 810 721 658 659 32 174 412 413 172 595 418 170 656 657 812 660 658 659 34 178 422 423 180 599 420 176 656 760 814 761 658 659 36 186 428 429 184 603 434 182 656 764 816 765 658 659 38 192 438 439 190 607 436 188 656 708 818 709 658 659 40 194 446 447 198 611 444 195 656 704 820 705 658 659 43 205 455 456 203 617 459 201 661 680 823 681 663 664 44 210 460 461 208 620 619 206 656 666 824 667 658 659 46 214 468 469 216 625 466 212 656 748 826 749 658 659 48 220 474 475 222 629 480 218 656 780 828 781 658 659 50 224 482 483 228 633 488 225 656 682 830 683 658 659 52 234 492 493 232 637 490 230 656 740 832 741 658 659 54 236 500 501 240 641 498 237 656 776 834 777 658 659 56 246 506 507 244 645 512 242 656 728 836 729 658 659 58 252 514 515 250 649 520 248 656 724 838 725 658 659 60 256 522 523 258 653 528 254 656 716 840 717 658 659 62 262 260 64 266 264 67 271 269 68 274 272 70 278 276 72 282 280 75 287 285 76 290 288 78 294 292 
[0.068] Exiting.

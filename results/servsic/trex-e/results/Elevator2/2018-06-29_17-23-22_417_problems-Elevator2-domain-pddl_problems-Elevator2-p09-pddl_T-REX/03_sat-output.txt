Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.004] Parsed head comment information.
[0.162] Processed problem encoding.
[0.174] Calculated possible fact changes of composite elements.
[0.183] Initialized instantiation procedure.
[0.183] 
[0.183] *************************************
[0.183] * * *   M a k e s p a n     0   * * *
[0.183] *************************************
[0.209] Instantiated 31,173 initial clauses.
[0.209] The encoding contains a total of 15,802 distinct variables.
[0.209] Attempting solve with solver <glucose4> ...
c 76 assumptions
[0.209] Executed solver; result: UNSAT.
[0.209] 
[0.209] *************************************
[0.209] * * *   M a k e s p a n     1   * * *
[0.209] *************************************
[0.267] Computed next depth properties: array size of 151.
[0.293] Instantiated 30,375 transitional clauses.
[0.400] Instantiated 138,221 universal clauses.
[0.400] Instantiated and added clauses for a total of 199,769 clauses.
[0.400] The encoding contains a total of 63,294 distinct variables.
[0.400] Attempting solve with solver <glucose4> ...
c 151 assumptions
[0.401] Executed solver; result: UNSAT.
[0.401] 
[0.401] *************************************
[0.401] * * *   M a k e s p a n     2   * * *
[0.401] *************************************
[0.514] Computed next depth properties: array size of 301.
[0.603] Instantiated 90,000 transitional clauses.
[0.823] Instantiated 693,596 universal clauses.
[0.823] Instantiated and added clauses for a total of 983,365 clauses.
[0.823] The encoding contains a total of 126,221 distinct variables.
[0.823] Attempting solve with solver <glucose4> ...
c 301 assumptions
c last restart ## conflicts  :  0 320 
[0.824] Executed solver; result: SAT.
[0.824] Solver returned SAT; a solution has been found at makespan 2.
300
solution 20313 1
6382 2 16497 11619 203 3 11791 11620 6599 4 11952 11621 437 5 16843 11622 560 6 12047 11623 730 7 12080 11624 6882 8 17315 11625 931 9 11965 11626 1143 10 12241 11627 1408 11 923 11628 7000 12 12416 11629 1776 13 12563 11630 1966 14 7919 11631 7127 15 17525 11632 7274 16 5070 11633 7575 17 2590 11634 7660 18 2931 11635 7814 19 17612 11636 2190 20 12602 11637 2479 21 10324 11638 1197 22 12891 11639 2589 23 10656 11640 244 24 17715 11641 8047 25 17960 11642 8295 26 18125 11643 8356 27 3677 11644 1669 28 9007 11645 2712 29 13225 11646 2937 30 13391 11647 2989 31 18385 11648 8826 32 13596 11649 2765 33 13690 11650 3195 34 13779 11651 8888 35 13899 11652 3372 36 14013 11653 2607 37 960 11654 8999 38 18832 11655 9096 39 14240 11656 3796 40 14337 11657 6917 41 6068 11658 1335 42 14482 11659 4043 43 14658 11660 4132 44 10510 11661 9362 45 1729 11662 9400 46 14034 11663 4414 47 19143 11664 9689 48 19320 11665 3926 49 7087 11666 4477 50 6013 11667 2502 51 10938 11668 4606 52 19348 11669 4702 53 11254 11670 5003 54 8598 11671 10045 55 17512 11672 8385 56 16591 11673 5115 57 924 11674 10159 58 12366 11675 4119 59 15136 11676 5377 60 18882 11677 8840 61 19523 11678 10474 62 19640 11679 5513 63 15456 11680 5698 64 15665 11681 5828 65 3702 11682 8841 66 771 11683 10741 67 19825 11684 10994 68 19996 11685 10029 69 16034 11686 6067 70 10386 11687 11241 71 16198 11688 11414 72 20173 11689 11502 73 20313 11690 7156 74 7588 11691 1773 75 16401 11692 6256 76 10605 11693 
[0.831] Exiting.

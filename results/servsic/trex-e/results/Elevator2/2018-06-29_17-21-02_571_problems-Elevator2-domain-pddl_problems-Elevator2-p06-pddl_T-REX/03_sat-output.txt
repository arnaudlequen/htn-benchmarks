Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.009] Parsed head comment information.
[0.275] Processed problem encoding.
[0.307] Calculated possible fact changes of composite elements.
[0.320] Initialized instantiation procedure.
[0.320] 
[0.320] *************************************
[0.320] * * *   M a k e s p a n     0   * * *
[0.320] *************************************
[0.367] Instantiated 61,248 initial clauses.
[0.367] The encoding contains a total of 31,002 distinct variables.
[0.367] Attempting solve with solver <glucose4> ...
c 51 assumptions
[0.367] Executed solver; result: UNSAT.
[0.367] 
[0.367] *************************************
[0.367] * * *   M a k e s p a n     1   * * *
[0.367] *************************************
[0.455] Computed next depth properties: array size of 101.
[0.500] Instantiated 60,250 transitional clauses.
[0.683] Instantiated 272,246 universal clauses.
[0.683] Instantiated and added clauses for a total of 393,744 clauses.
[0.683] The encoding contains a total of 122,770 distinct variables.
[0.683] Attempting solve with solver <glucose4> ...
c 101 assumptions
[0.685] Executed solver; result: UNSAT.
[0.685] 
[0.685] *************************************
[0.685] * * *   M a k e s p a n     2   * * *
[0.685] *************************************
[0.852] Computed next depth properties: array size of 201.
[0.996] Instantiated 180,000 transitional clauses.
[1.376] Instantiated 1,442,496 universal clauses.
[1.376] Instantiated and added clauses for a total of 2,016,240 clauses.
[1.376] The encoding contains a total of 244,822 distinct variables.
[1.376] Attempting solve with solver <glucose4> ...
c 201 assumptions
c last restart ## conflicts  :  0 221 
[1.377] Executed solver; result: SAT.
[1.377] Solver returned SAT; a solution has been found at makespan 2.
199
solution 51915 1
16592 2 518 27606 3 27788 27607 17363 4 40894 27608 17915 5 28580 27609 1379 6 28584 27610 1645 7 41397 27611 2027 8 29288 27612 2734 9 29774 27613 3003 10 29800 27614 18804 11 30500 27615 3634 12 42896 27616 3844 13 43063 27617 19744 14 1883 27618 4658 15 30992 27619 5502 16 22306 27620 20061 17 43859 27621 20103 18 31833 27622 6641 19 32152 27623 21017 20 32642 27624 7214 21 32825 27625 21534 22 17621 27626 7565 23 45274 27627 7749 24 45544 27628 22398 25 46207 27629 8448 26 33538 27630 8836 27 33827 27631 9623 28 34157 27632 5781 29 34505 27633 9814 30 27433 27634 10088 31 35163 27635 10512 32 47778 27636 10554 33 35405 27637 7964 34 48283 27638 24177 35 36035 27639 12021 36 8815 27640 24515 37 36689 27641 24019 38 49348 27642 22445 39 45295 27643 12636 40 36912 27644 12963 41 50134 27645 25145 42 13059 27646 13401 43 37581 27647 13780 44 50539 27648 13976 45 38270 27649 14750 46 36797 27650 26481 47 38823 27651 15541 48 39050 27652 26961 49 39639 27653 16001 50 51915 27654 27575 51 39893 27655 
[1.390] Exiting.

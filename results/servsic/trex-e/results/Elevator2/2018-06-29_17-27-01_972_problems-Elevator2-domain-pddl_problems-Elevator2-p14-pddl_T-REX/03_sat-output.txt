Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.016] Parsed head comment information.
[0.420] Processed problem encoding.
[0.507] Calculated possible fact changes of composite elements.
[0.542] Initialized instantiation procedure.
[0.542] 
[0.542] *************************************
[0.542] * * *   M a k e s p a n     0   * * *
[0.542] *************************************
[0.661] Instantiated 106,673 initial clauses.
[0.661] The encoding contains a total of 53,802 distinct variables.
[0.661] Attempting solve with solver <glucose4> ...
c 76 assumptions
[0.662] Executed solver; result: UNSAT.
[0.662] 
[0.662] *************************************
[0.662] * * *   M a k e s p a n     1   * * *
[0.662] *************************************
[0.891] Computed next depth properties: array size of 151.
[0.989] Instantiated 105,375 transitional clauses.
[1.443] Instantiated 476,021 universal clauses.
[1.443] Instantiated and added clauses for a total of 688,069 clauses.
[1.443] The encoding contains a total of 214,096 distinct variables.
[1.443] Attempting solve with solver <glucose4> ...
c 151 assumptions
[1.446] Executed solver; result: UNSAT.
[1.446] 
[1.446] *************************************
[1.446] * * *   M a k e s p a n     2   * * *
[1.446] *************************************
[1.884] Computed next depth properties: array size of 301.
[2.221] Instantiated 315,000 transitional clauses.
[3.149] Instantiated 2,628,896 universal clauses.
[3.149] Instantiated and added clauses for a total of 3,631,965 clauses.
[3.149] The encoding contains a total of 427,323 distinct variables.
[3.149] Attempting solve with solver <glucose4> ...
c 301 assumptions
c last restart ## conflicts  :  0 322 
[3.151] Executed solver; result: SAT.
[3.151] Solver returned SAT; a solution has been found at makespan 2.
300
solution 94856 1
25729 2 51376 51104 26213 3 72110 51105 26689 4 72259 51106 1012 5 72573 51107 27449 6 52221 51108 27716 7 73649 51109 2191 8 52982 51110 1902 9 73919 51111 2532 10 74436 51112 28660 11 74810 51113 29473 12 75530 51114 29809 13 54550 51115 30254 14 76306 51116 3749 15 55334 51117 4628 16 55729 51118 5429 17 56310 51119 5579 18 56408 51120 6621 19 56946 51121 7067 20 57213 51122 31117 21 57744 51123 8042 22 57887 51124 31717 23 79297 51125 31914 24 79614 51126 8933 25 79859 51127 9568 26 59058 51128 33101 27 80815 51129 33628 28 80994 51130 34234 29 81581 51131 34646 30 81932 51132 34863 31 82392 51133 35294 32 82846 51134 35487 33 83716 51135 11595 34 42954 51136 12269 35 61375 51137 37045 36 83922 51138 12696 37 84313 51139 37495 38 84705 51140 13402 39 84871 51141 38189 40 62967 51142 14226 41 63875 51143 39229 42 28028 51144 14777 43 61797 51145 15096 44 49889 51146 15788 45 63914 51147 39985 46 86792 51148 40776 47 86949 51149 345 48 64809 51150 16329 49 87646 51151 41276 50 88533 51152 42272 51 85530 51153 17279 52 32794 51154 17924 53 65358 51155 18522 54 28249 51156 43229 55 89265 51157 43425 56 89471 51158 44232 57 90121 51159 44583 58 66668 51160 19665 59 90316 51161 20073 60 67643 51162 45857 61 91169 51163 46125 62 66690 51164 20914 63 91463 51165 21240 64 68476 51166 46986 65 92284 51167 47456 66 92957 51168 22205 67 69203 51169 48411 68 77094 51170 48558 69 69865 51171 23340 70 93810 51172 23550 71 70381 51173 49313 72 81971 51174 49651 73 71002 51175 24719 74 78751 51176 50913 75 94856 51177 25227 76 74177 51178 
[3.188] Exiting.

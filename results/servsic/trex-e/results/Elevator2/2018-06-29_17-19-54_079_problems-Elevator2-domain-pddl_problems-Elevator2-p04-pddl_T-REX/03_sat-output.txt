Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.006] Parsed head comment information.
[0.207] Processed problem encoding.
[0.226] Calculated possible fact changes of composite elements.
[0.235] Initialized instantiation procedure.
[0.235] 
[0.235] *************************************
[0.235] * * *   M a k e s p a n     0   * * *
[0.235] *************************************
[0.277] Instantiated 41,048 initial clauses.
[0.277] The encoding contains a total of 20,802 distinct variables.
[0.277] Attempting solve with solver <glucose4> ...
c 51 assumptions
[0.277] Executed solver; result: UNSAT.
[0.277] 
[0.277] *************************************
[0.277] * * *   M a k e s p a n     1   * * *
[0.277] *************************************
[0.355] Computed next depth properties: array size of 101.
[0.390] Instantiated 40,250 transitional clauses.
[0.524] Instantiated 182,246 universal clauses.
[0.524] Instantiated and added clauses for a total of 263,544 clauses.
[0.524] The encoding contains a total of 82,570 distinct variables.
[0.524] Attempting solve with solver <glucose4> ...
c 101 assumptions
[0.525] Executed solver; result: UNSAT.
[0.525] 
[0.525] *************************************
[0.525] * * *   M a k e s p a n     2   * * *
[0.525] *************************************
[0.644] Computed next depth properties: array size of 201.
[0.751] Instantiated 120,000 transitional clauses.
[1.003] Instantiated 962,496 universal clauses.
[1.003] Instantiated and added clauses for a total of 1,346,040 clauses.
[1.003] The encoding contains a total of 164,622 distinct variables.
[1.003] Attempting solve with solver <glucose4> ...
c 201 assumptions
c last restart ## conflicts  :  0 221 
[1.004] Executed solver; result: SAT.
[1.004] Solver returned SAT; a solution has been found at makespan 2.
200
solution 35177 1
8776 2 18171 18007 337 3 27686 18008 573 4 18698 18009 9384 5 19062 18010 901 6 28159 18011 10097 7 19687 18012 1250 8 19801 18013 1583 9 20083 18014 1803 10 28525 18015 10570 11 16053 18016 2282 12 20524 18017 2417 13 20945 18018 2737 14 21287 18019 2822 15 19255 18020 3110 16 21424 18021 11752 17 29320 18022 3547 18 16777 18023 3995 19 21853 18024 4113 20 29680 18025 12194 21 22189 18026 4637 22 30089 18027 12600 23 22655 18028 12788 24 30630 18029 13128 25 23278 18030 5364 26 11999 18031 5503 27 23400 18032 13672 28 20583 18033 14015 29 31092 18034 5905 30 31269 18035 14412 31 31489 18036 14588 32 5872 18037 6473 33 23846 18038 9089 34 32139 18039 15035 35 32393 18040 15243 36 32841 18041 15619 37 24389 18042 7313 38 24823 18043 9172 39 33058 18044 4923 40 25345 18045 15990 41 33481 18046 1444 42 25648 18047 16462 43 33775 18048 7526 44 26231 18049 7982 45 21704 18050 16984 46 26734 18051 8238 47 26965 18052 8333 48 34352 18053 12625 49 34794 18054 17597 50 34857 18055 17708 51 35177 18056 
[1.013] Exiting.

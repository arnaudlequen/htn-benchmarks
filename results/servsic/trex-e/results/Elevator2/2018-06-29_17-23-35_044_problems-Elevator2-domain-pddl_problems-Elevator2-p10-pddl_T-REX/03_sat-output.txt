Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.006] Parsed head comment information.
[0.205] Processed problem encoding.
[0.226] Calculated possible fact changes of composite elements.
[0.240] Initialized instantiation procedure.
[0.240] 
[0.240] *************************************
[0.240] * * *   M a k e s p a n     0   * * *
[0.240] *************************************
[0.285] Instantiated 46,273 initial clauses.
[0.285] The encoding contains a total of 23,402 distinct variables.
[0.285] Attempting solve with solver <glucose4> ...
c 76 assumptions
[0.285] Executed solver; result: UNSAT.
[0.285] 
[0.285] *************************************
[0.285] * * *   M a k e s p a n     1   * * *
[0.285] *************************************
[0.376] Computed next depth properties: array size of 151.
[0.417] Instantiated 45,375 transitional clauses.
[0.592] Instantiated 205,871 universal clauses.
[0.592] Instantiated and added clauses for a total of 297,519 clauses.
[0.592] The encoding contains a total of 93,545 distinct variables.
[0.592] Attempting solve with solver <glucose4> ...
c 151 assumptions
[0.593] Executed solver; result: UNSAT.
[0.593] 
[0.593] *************************************
[0.593] * * *   M a k e s p a n     2   * * *
[0.593] *************************************
[0.775] Computed next depth properties: array size of 301.
[0.909] Instantiated 135,000 transitional clauses.
[1.262] Instantiated 1,083,746 universal clauses.
[1.262] Instantiated and added clauses for a total of 1,516,265 clauses.
[1.262] The encoding contains a total of 186,622 distinct variables.
[1.262] Attempting solve with solver <glucose4> ...
c 301 assumptions
c last restart ## conflicts  :  0 321 
[1.263] Executed solver; result: SAT.
[1.263] Solver returned SAT; a solution has been found at makespan 2.
300
solution 35657 1
9689 2 26672 18615 9906 3 18939 18616 474 4 26958 18617 10117 5 27280 18618 10202 6 27592 18619 937 7 27706 18620 10412 8 16797 18621 1369 9 19203 18622 1551 10 19420 18623 11038 11 28147 18624 1890 12 13623 18625 2007 13 19930 18626 2158 14 19997 18627 2367 15 20173 18628 2687 16 20507 18629 2874 17 28615 18630 11677 18 28858 18631 11722 19 20775 18632 2456 20 4040 18633 11851 21 29180 18634 12237 22 29438 18635 12355 23 21334 18636 3827 24 21428 18637 12579 25 29843 18638 4051 26 21692 18639 12860 27 30104 18640 4446 28 11360 18641 13005 29 2801 18642 13223 30 13716 18643 13478 31 30516 18644 13590 32 30653 18645 13848 33 22107 18646 5402 34 22356 18647 13991 35 31166 18648 14165 36 31294 18649 10761 37 22601 18650 14457 38 31654 18651 14676 39 31785 18652 14886 40 1878 18653 15055 41 8442 18654 5970 42 31890 18655 6152 43 23051 18656 15543 44 32410 18657 13898 45 23378 18658 1118 46 17430 18659 15639 47 22123 18660 6887 48 23607 18661 6983 49 10084 18662 11929 50 32840 18663 15787 51 33112 18664 16085 52 33215 18665 13226 53 16747 18666 7424 54 24037 18667 3583 55 33509 18668 16308 56 33685 18669 2547 57 33911 18670 7639 58 24402 18671 3507 59 31096 18672 16542 60 24656 18673 16767 61 34479 18674 16833 62 34635 18675 17083 63 3449 18676 8455 64 25036 18677 8654 65 25136 18678 8909 66 25305 18679 9078 67 35113 18680 15095 68 2267 18681 17792 69 29706 18682 8856 70 21580 18683 18120 71 35353 18684 18194 72 35487 18685 18491 73 35657 18686 9603 74 26048 18687 10852 75 26343 18688 1900 76 26409 18689 
[1.277] Exiting.

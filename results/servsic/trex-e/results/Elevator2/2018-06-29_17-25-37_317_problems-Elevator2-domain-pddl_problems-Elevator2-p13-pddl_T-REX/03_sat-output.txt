Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.014] Parsed head comment information.
[0.386] Processed problem encoding.
[0.447] Calculated possible fact changes of composite elements.
[0.476] Initialized instantiation procedure.
[0.476] 
[0.476] *************************************
[0.476] * * *   M a k e s p a n     0   * * *
[0.476] *************************************
[0.583] Instantiated 91,573 initial clauses.
[0.583] The encoding contains a total of 46,202 distinct variables.
[0.583] Attempting solve with solver <glucose4> ...
c 76 assumptions
[0.583] Executed solver; result: UNSAT.
[0.583] 
[0.583] *************************************
[0.583] * * *   M a k e s p a n     1   * * *
[0.583] *************************************
[0.777] Computed next depth properties: array size of 151.
[0.858] Instantiated 90,375 transitional clauses.
[1.251] Instantiated 408,521 universal clauses.
[1.251] Instantiated and added clauses for a total of 590,469 clauses.
[1.251] The encoding contains a total of 183,996 distinct variables.
[1.251] Attempting solve with solver <glucose4> ...
c 151 assumptions
[1.253] Executed solver; result: UNSAT.
[1.253] 
[1.253] *************************************
[1.253] * * *   M a k e s p a n     2   * * *
[1.253] *************************************
[1.623] Computed next depth properties: array size of 301.
[1.907] Instantiated 270,000 transitional clauses.
[2.712] Instantiated 2,253,896 universal clauses.
[2.712] Instantiated and added clauses for a total of 3,114,365 clauses.
[2.712] The encoding contains a total of 367,223 distinct variables.
[2.712] Attempting solve with solver <glucose4> ...
c 301 assumptions
c last restart ## conflicts  :  0 322 
[2.714] Executed solver; result: SAT.
[2.714] Solver returned SAT; a solution has been found at makespan 2.
300
solution 79800 1
23313 2 43320 43205 1169 3 33021 43206 23604 4 44145 43207 1528 5 61523 43208 24391 6 61585 43209 1815 7 61693 43210 25303 8 5251 43211 25741 9 62428 43212 2347 10 29007 43213 26003 11 62531 43214 3081 12 62984 43215 26622 13 46096 43216 4010 14 46255 43217 4277 15 46432 43218 4978 16 47020 43219 27480 17 64880 43220 27821 18 65226 43221 28184 19 47570 43222 5951 20 47889 43223 6487 21 48112 43224 6915 22 66150 43225 28788 23 66741 43226 7747 24 48623 43227 8184 25 67497 43228 29716 26 2661 43229 30024 27 67601 43230 8663 28 49865 43231 9019 29 68420 43232 30910 30 68875 43233 31308 31 69249 43234 9928 32 50786 43235 10087 33 66044 43236 32733 34 69473 43237 10526 35 51571 43238 11133 36 51910 43239 33084 37 34795 43240 12007 38 70378 43241 33469 39 52378 43242 13002 40 52670 43243 33768 41 71609 43244 13117 42 71719 43245 34820 43 53706 43246 13702 44 72420 43247 35344 45 54116 43248 36049 46 54710 43249 14524 47 34738 43250 14793 48 54933 43251 15007 49 73141 43252 15288 50 33176 43253 30563 51 73325 43254 37423 52 3971 43255 37669 53 73946 43256 37911 54 74761 43257 38428 55 74849 43258 38873 56 75272 43259 39068 57 75924 43260 17005 58 56597 43261 17749 59 76212 43262 39684 60 10433 43263 39942 61 5907 43264 40517 62 76762 43265 18907 63 57519 43266 19263 64 57874 43267 19689 65 77370 43268 20005 66 58385 43269 20686 67 58874 43270 41643 68 78518 43271 41806 69 59155 43272 16888 70 79218 43273 21738 71 56365 43274 22347 72 59755 43275 34791 73 60407 43276 42501 74 14078 43277 23041 75 39596 43278 42714 76 79800 43279 
[2.744] Exiting.

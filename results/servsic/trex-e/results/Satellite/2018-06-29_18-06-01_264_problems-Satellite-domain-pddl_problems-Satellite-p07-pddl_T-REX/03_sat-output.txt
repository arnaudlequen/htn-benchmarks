Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.061] Processed problem encoding.
[0.071] Calculated possible fact changes of composite elements.
[0.072] Initialized instantiation procedure.
[0.072] 
[0.072] *************************************
[0.072] * * *   M a k e s p a n     0   * * *
[0.072] *************************************
[0.077] Instantiated 8,702 initial clauses.
[0.077] The encoding contains a total of 4,657 distinct variables.
[0.077] Attempting solve with solver <glucose4> ...
c 24 assumptions
[0.077] Executed solver; result: UNSAT.
[0.077] 
[0.077] *************************************
[0.077] * * *   M a k e s p a n     1   * * *
[0.077] *************************************
[0.083] Computed next depth properties: array size of 43.
[0.086] Instantiated 924 transitional clauses.
[0.097] Instantiated 20,104 universal clauses.
[0.097] Instantiated and added clauses for a total of 29,730 clauses.
[0.097] The encoding contains a total of 9,284 distinct variables.
[0.097] Attempting solve with solver <glucose4> ...
c 43 assumptions
[0.097] Executed solver; result: UNSAT.
[0.097] 
[0.097] *************************************
[0.097] * * *   M a k e s p a n     2   * * *
[0.097] *************************************
[0.109] Computed next depth properties: array size of 62.
[0.124] Instantiated 24,330 transitional clauses.
[0.147] Instantiated 93,042 universal clauses.
[0.147] Instantiated and added clauses for a total of 147,102 clauses.
[0.147] The encoding contains a total of 28,098 distinct variables.
[0.147] Attempting solve with solver <glucose4> ...
c 62 assumptions
[0.147] Executed solver; result: UNSAT.
[0.147] 
[0.147] *************************************
[0.147] * * *   M a k e s p a n     3   * * *
[0.147] *************************************
[0.159] Computed next depth properties: array size of 100.
[0.177] Instantiated 63,981 transitional clauses.
[0.189] Instantiated 42,115 universal clauses.
[0.189] Instantiated and added clauses for a total of 253,198 clauses.
[0.189] The encoding contains a total of 33,858 distinct variables.
[0.189] Attempting solve with solver <glucose4> ...
c 100 assumptions
[0.189] Executed solver; result: UNSAT.
[0.189] 
[0.189] *************************************
[0.189] * * *   M a k e s p a n     4   * * *
[0.189] *************************************
[0.200] Computed next depth properties: array size of 138.
[0.207] Instantiated 1,638 transitional clauses.
[0.225] Instantiated 50,423 universal clauses.
[0.225] Instantiated and added clauses for a total of 305,259 clauses.
[0.225] The encoding contains a total of 41,795 distinct variables.
[0.225] Attempting solve with solver <glucose4> ...
c 138 assumptions
[0.225] Executed solver; result: UNSAT.
[0.225] 
[0.225] *************************************
[0.225] * * *   M a k e s p a n     5   * * *
[0.225] *************************************
[0.244] Computed next depth properties: array size of 176.
[0.267] Instantiated 49,025 transitional clauses.
[0.307] Instantiated 229,072 universal clauses.
[0.307] Instantiated and added clauses for a total of 583,356 clauses.
[0.307] The encoding contains a total of 80,343 distinct variables.
[0.307] Attempting solve with solver <glucose4> ...
c 176 assumptions
c last restart ## conflicts  :  14 864 
[0.319] Executed solver; result: SAT.
[0.319] Solver returned SAT; a solution has been found at makespan 5.
78
solution 3549 1
200 3132 226 3157 11 204 1386 214 1403 16 198 2756 224 2796 29 196 2348 222 2412 36 2442 48 2467 60 1432 63 243 195 2323 221 2483 74 242 196 2349 222 2508 85 243 195 2326 221 2531 92 242 196 2351 222 2556 108 3303 119 3338 126 243 195 2328 221 2603 133 2644 143 3363 155 242 196 2357 222 2676 164 3089 177 3391 181 2011 2744 3297 3549 
[0.320] Exiting.

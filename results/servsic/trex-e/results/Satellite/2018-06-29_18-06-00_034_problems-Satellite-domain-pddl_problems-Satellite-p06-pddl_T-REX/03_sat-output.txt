Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.034] Processed problem encoding.
[0.038] Calculated possible fact changes of composite elements.
[0.039] Initialized instantiation procedure.
[0.039] 
[0.039] *************************************
[0.039] * * *   M a k e s p a n     0   * * *
[0.039] *************************************
[0.042] Instantiated 5,099 initial clauses.
[0.042] The encoding contains a total of 2,732 distinct variables.
[0.042] Attempting solve with solver <glucose4> ...
c 20 assumptions
[0.042] Executed solver; result: UNSAT.
[0.042] 
[0.042] *************************************
[0.042] * * *   M a k e s p a n     1   * * *
[0.042] *************************************
[0.045] Computed next depth properties: array size of 36.
[0.046] Instantiated 502 transitional clauses.
[0.053] Instantiated 11,651 universal clauses.
[0.053] Instantiated and added clauses for a total of 17,252 clauses.
[0.053] The encoding contains a total of 5,540 distinct variables.
[0.053] Attempting solve with solver <glucose4> ...
c 36 assumptions
[0.053] Executed solver; result: UNSAT.
[0.053] 
[0.053] *************************************
[0.053] * * *   M a k e s p a n     2   * * *
[0.053] *************************************
[0.059] Computed next depth properties: array size of 52.
[0.067] Instantiated 11,337 transitional clauses.
[0.082] Instantiated 50,782 universal clauses.
[0.082] Instantiated and added clauses for a total of 79,371 clauses.
[0.082] The encoding contains a total of 14,634 distinct variables.
[0.082] Attempting solve with solver <glucose4> ...
c 52 assumptions
[0.083] Executed solver; result: UNSAT.
[0.083] 
[0.083] *************************************
[0.083] * * *   M a k e s p a n     3   * * *
[0.083] *************************************
[0.089] Computed next depth properties: array size of 84.
[0.102] Instantiated 26,264 transitional clauses.
[0.112] Instantiated 23,307 universal clauses.
[0.112] Instantiated and added clauses for a total of 128,942 clauses.
[0.112] The encoding contains a total of 17,986 distinct variables.
[0.112] Attempting solve with solver <glucose4> ...
c 84 assumptions
[0.112] Executed solver; result: UNSAT.
[0.112] 
[0.112] *************************************
[0.112] * * *   M a k e s p a n     4   * * *
[0.112] *************************************
[0.120] Computed next depth properties: array size of 116.
[0.124] Instantiated 803 transitional clauses.
[0.137] Instantiated 28,373 universal clauses.
[0.137] Instantiated and added clauses for a total of 158,118 clauses.
[0.137] The encoding contains a total of 22,537 distinct variables.
[0.137] Attempting solve with solver <glucose4> ...
c 116 assumptions
[0.137] Executed solver; result: UNSAT.
[0.137] 
[0.137] *************************************
[0.137] * * *   M a k e s p a n     5   * * *
[0.137] *************************************
[0.149] Computed next depth properties: array size of 148.
[0.159] Instantiated 21,613 transitional clauses.
[0.179] Instantiated 111,136 universal clauses.
[0.179] Instantiated and added clauses for a total of 290,867 clauses.
[0.179] The encoding contains a total of 40,115 distinct variables.
[0.179] Attempting solve with solver <glucose4> ...
c 148 assumptions
c last restart ## conflicts  :  64 761 
[0.191] Executed solver; result: SAT.
[0.191] Solver returned SAT; a solution has been found at makespan 5.
74
solution 2056 1
95 847 109 896 5 120 93 873 107 921 10 91 160 104 224 14 116 90 132 103 247 19 96 110 1326 28 121 97 1238 111 1351 33 1020 38 115 92 133 105 343 42 117 91 162 104 368 47 100 560 106 727 53 118 95 857 109 1064 60 120 93 886 107 1089 65 1481 72 1152 75 524 79 1202 86 506 1059 2056 
[0.192] Exiting.

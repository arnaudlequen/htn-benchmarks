Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.023] Parsed head comment information.
[1.441] Processed problem encoding.
[2.635] Calculated possible fact changes of composite elements.
[2.824] Initialized instantiation procedure.
[2.824] 
[2.824] *************************************
[2.824] * * *   M a k e s p a n     0   * * *
[2.824] *************************************
[3.385] Instantiated 461,192 initial clauses.
[3.385] The encoding contains a total of 232,880 distinct variables.
[3.385] Attempting solve with solver <glucose4> ...
c 179 assumptions
[3.388] Executed solver; result: UNSAT.
[3.388] 
[3.388] *************************************
[3.388] * * *   M a k e s p a n     1   * * *
[3.388] *************************************
[4.460] Computed next depth properties: array size of 354.
[5.096] Instantiated 7,894 transitional clauses.
[6.922] Instantiated 960,317 universal clauses.
[6.922] Instantiated and added clauses for a total of 1,429,403 clauses.
[6.922] The encoding contains a total of 468,824 distinct variables.
[6.922] Attempting solve with solver <glucose4> ...
c 354 assumptions
[6.922] Executed solver; result: UNSAT.
[6.922] 
[6.922] *************************************
[6.922] * * *   M a k e s p a n     2   * * *
[6.922] *************************************
[9.012] Computed next depth properties: array size of 529.
[10.847] Instantiated 1,818,295 transitional clauses.
[14.294] Instantiated 8,000,868 universal clauses.
[14.294] Instantiated and added clauses for a total of 11,248,566 clauses.
[14.294] The encoding contains a total of 1,829,677 distinct variables.
[14.294] Attempting solve with solver <glucose4> ...
c 529 assumptions
[14.295] Executed solver; result: UNSAT.
[14.295] 
[14.295] *************************************
[14.295] * * *   M a k e s p a n     3   * * *
[14.295] *************************************
[17.343] Computed next depth properties: array size of 879.
[20.314] Instantiated 4,803,971 transitional clauses.
[24.357] Instantiated 1,963,503 universal clauses.
[24.357] Instantiated and added clauses for a total of 18,016,040 clauses.
[24.357] The encoding contains a total of 2,077,128 distinct variables.
[24.357] Attempting solve with solver <glucose4> ...
c 879 assumptions
[24.358] Executed solver; result: UNSAT.
[24.358] 
[24.358] *************************************
[24.358] * * *   M a k e s p a n     4   * * *
[24.358] *************************************
[28.769] Computed next depth properties: array size of 1229.
[31.607] Instantiated 15,729 transitional clauses.
[37.488] Instantiated 2,775,621 universal clauses.
[37.488] Instantiated and added clauses for a total of 20,807,390 clauses.
[37.488] The encoding contains a total of 2,528,982 distinct variables.
[37.489] Attempting solve with solver <glucose4> ...
c 1229 assumptions
[37.490] Executed solver; result: UNSAT.
[37.490] 
[37.490] *************************************
[37.490] * * *   M a k e s p a n     5   * * *
[37.490] *************************************
[43.936] Computed next depth properties: array size of 1579.
[49.179] Instantiated 3,751,681 transitional clauses.
[58.560] Instantiated 20,215,972 universal clauses.
[58.560] Instantiated and added clauses for a total of 44,775,043 clauses.
[58.560] The encoding contains a total of 5,379,151 distinct variables.
[58.560] Attempting solve with solver <glucose4> ...
c 1579 assumptions
c |       63         0      158 | 5362172 44625446 113731233 |     2     6175      154     3760 |  0.316 % |
Interrupt signal (15) received.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.002] Parsed head comment information.
[0.117] Processed problem encoding.
[0.135] Calculated possible fact changes of composite elements.
[0.139] Initialized instantiation procedure.
[0.139] 
[0.139] *************************************
[0.139] * * *   M a k e s p a n     0   * * *
[0.139] *************************************
[0.151] Instantiated 21,758 initial clauses.
[0.151] The encoding contains a total of 11,239 distinct variables.
[0.151] Attempting solve with solver <glucose4> ...
c 46 assumptions
[0.152] Executed solver; result: UNSAT.
[0.152] 
[0.152] *************************************
[0.152] * * *   M a k e s p a n     1   * * *
[0.152] *************************************
[0.165] Computed next depth properties: array size of 89.
[0.173] Instantiated 1,312 transitional clauses.
[0.199] Instantiated 48,448 universal clauses.
[0.199] Instantiated and added clauses for a total of 71,518 clauses.
[0.199] The encoding contains a total of 23,278 distinct variables.
[0.199] Attempting solve with solver <glucose4> ...
c 89 assumptions
[0.199] Executed solver; result: UNSAT.
[0.199] 
[0.199] *************************************
[0.199] * * *   M a k e s p a n     2   * * *
[0.199] *************************************
[0.230] Computed next depth properties: array size of 132.
[0.266] Instantiated 73,231 transitional clauses.
[0.330] Instantiated 294,088 universal clauses.
[0.330] Instantiated and added clauses for a total of 438,837 clauses.
[0.330] The encoding contains a total of 77,591 distinct variables.
[0.330] Attempting solve with solver <glucose4> ...
c 132 assumptions
[0.330] Executed solver; result: UNSAT.
[0.330] 
[0.330] *************************************
[0.330] * * *   M a k e s p a n     3   * * *
[0.330] *************************************
[0.370] Computed next depth properties: array size of 218.
[0.429] Instantiated 178,835 transitional clauses.
[0.488] Instantiated 101,649 universal clauses.
[0.488] Instantiated and added clauses for a total of 719,321 clauses.
[0.488] The encoding contains a total of 91,403 distinct variables.
[0.488] Attempting solve with solver <glucose4> ...
c 218 assumptions
[0.488] Executed solver; result: UNSAT.
[0.488] 
[0.488] *************************************
[0.488] * * *   M a k e s p a n     4   * * *
[0.488] *************************************
[0.539] Computed next depth properties: array size of 304.
[0.572] Instantiated 2,658 transitional clauses.
[0.657] Instantiated 132,847 universal clauses.
[0.657] Instantiated and added clauses for a total of 854,826 clauses.
[0.657] The encoding contains a total of 113,322 distinct variables.
[0.657] Attempting solve with solver <glucose4> ...
c 304 assumptions
[0.657] Executed solver; result: UNSAT.
[0.657] 
[0.657] *************************************
[0.657] * * *   M a k e s p a n     5   * * *
[0.657] *************************************
[0.743] Computed next depth properties: array size of 390.
[0.834] Instantiated 150,945 transitional clauses.
[1.010] Instantiated 748,112 universal clauses.
[1.010] Instantiated and added clauses for a total of 1,753,883 clauses.
[1.010] The encoding contains a total of 227,336 distinct variables.
[1.010] Attempting solve with solver <glucose4> ...
c 390 assumptions
c last restart ## conflicts  :  13 2599 
[1.387] Executed solver; result: SAT.
[1.387] Solver returned SAT; a solution has been found at makespan 5.
167
solution 9616 1
278 1145 290 1243 3 1298 9 1351 16 279 3486 291 3739 24 283 5882 295 6133 32 3849 36 300 280 3539 292 3896 43 301 281 3540 293 3948 49 304 282 5825 294 6339 55 4062 59 285 8218 297 8629 66 286 356 288 412 67 306 284 8177 296 8732 82 1924 83 6664 92 6727 99 6780 105 299 277 1213 289 2129 106 298 278 1165 290 2179 112 2259 119 6989 131 9013 140 302 280 3548 292 4728 145 2468 150 7201 159 796 160 7309 172 2680 173 305 285 8241 297 9201 183 5123 187 306 284 8196 296 9304 197 5234 202 305 285 8250 297 9409 211 9501 217 3048 220 301 281 3571 293 5404 231 306 284 8201 296 9616 241 303 283 5906 295 7849 246 5604 251 3315 253 302 280 3578 292 5664 261 301 281 3580 293 5716 269 302 279 3529 291 5767 273 3163 5610 
[1.396] Exiting.

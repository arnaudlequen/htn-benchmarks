Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.002] Processed problem encoding.
[0.002] Calculated possible fact changes of composite elements.
[0.002] Initialized instantiation procedure.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     0   * * *
[0.002] *************************************
[0.002] Instantiated 185 initial clauses.
[0.002] The encoding contains a total of 109 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 6 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     1   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 11.
[0.002] Instantiated 36 transitional clauses.
[0.003] Instantiated 391 universal clauses.
[0.003] Instantiated and added clauses for a total of 612 clauses.
[0.003] The encoding contains a total of 280 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 11 assumptions
[0.003] Executed solver; result: UNSAT.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     2   * * *
[0.003] *************************************
[0.003] Computed next depth properties: array size of 16.
[0.003] Instantiated 351 transitional clauses.
[0.004] Instantiated 1,155 universal clauses.
[0.004] Instantiated and added clauses for a total of 2,118 clauses.
[0.004] The encoding contains a total of 585 distinct variables.
[0.004] Attempting solve with solver <glucose4> ...
c 16 assumptions
[0.004] Executed solver; result: UNSAT.
[0.004] 
[0.004] *************************************
[0.004] * * *   M a k e s p a n     3   * * *
[0.004] *************************************
[0.004] Computed next depth properties: array size of 26.
[0.005] Instantiated 795 transitional clauses.
[0.005] Instantiated 1,089 universal clauses.
[0.005] Instantiated and added clauses for a total of 4,002 clauses.
[0.005] The encoding contains a total of 835 distinct variables.
[0.005] Attempting solve with solver <glucose4> ...
c 26 assumptions
[0.005] Executed solver; result: UNSAT.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     4   * * *
[0.005] *************************************
[0.005] Computed next depth properties: array size of 36.
[0.006] Instantiated 111 transitional clauses.
[0.006] Instantiated 1,130 universal clauses.
[0.006] Instantiated and added clauses for a total of 5,243 clauses.
[0.006] The encoding contains a total of 1,116 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 36 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     5   * * *
[0.006] *************************************
[0.007] Computed next depth properties: array size of 46.
[0.008] Instantiated 768 transitional clauses.
[0.009] Instantiated 3,292 universal clauses.
[0.009] Instantiated and added clauses for a total of 9,303 clauses.
[0.009] The encoding contains a total of 1,805 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 46 assumptions
c last restart ## conflicts  :  3 171 
[0.010] Executed solver; result: SAT.
[0.010] Solver returned SAT; a solution has been found at makespan 5.
17
solution 66 1
11 21 14 33 2 16 12 27 15 41 5 50 6 58 8 66 10 
[0.010] Exiting.

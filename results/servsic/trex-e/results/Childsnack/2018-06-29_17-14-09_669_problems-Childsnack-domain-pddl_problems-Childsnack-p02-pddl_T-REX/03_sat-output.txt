Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.080] Processed problem encoding.
[0.083] Calculated possible fact changes of composite elements.
[0.090] Initialized instantiation procedure.
[0.090] 
[0.090] *************************************
[0.090] * * *   M a k e s p a n     0   * * *
[0.090] *************************************
[0.097] Instantiated 12,886 initial clauses.
[0.097] The encoding contains a total of 11,966 distinct variables.
[0.097] Attempting solve with solver <glucose4> ...
c 11 assumptions
[0.097] Executed solver; result: UNSAT.
[0.097] 
[0.097] *************************************
[0.097] * * *   M a k e s p a n     1   * * *
[0.097] *************************************
[0.105] Computed next depth properties: array size of 51.
[0.126] Instantiated 59,080 transitional clauses.
[0.142] Instantiated 88,144 universal clauses.
[0.142] Instantiated and added clauses for a total of 160,110 clauses.
[0.142] The encoding contains a total of 18,455 distinct variables.
[0.142] Attempting solve with solver <glucose4> ...
c 51 assumptions
c last restart ## conflicts  :  12 2367 
[0.148] Executed solver; result: SAT.
[0.148] Solver returned SAT; a solution has been found at makespan 1.
50
solution 1122 1
606 58 4 446 6 240 88 8 89 10 29 32 8 304 10 499 37 924 942 926 643 44 372 984 374 907 76 378 1037 380 153 23 4 339 6 782 20 378 1052 380 268 3 372 373 374 702 93 4 1122 6 
[0.148] Exiting.

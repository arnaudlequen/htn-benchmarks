Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.005] Parsed head comment information.
[0.274] Processed problem encoding.
[0.286] Calculated possible fact changes of composite elements.
[0.307] Initialized instantiation procedure.
[0.307] 
[0.307] *************************************
[0.307] * * *   M a k e s p a n     0   * * *
[0.307] *************************************
[0.329] Instantiated 60,931 initial clauses.
[0.329] The encoding contains a total of 58,907 distinct variables.
[0.329] Attempting solve with solver <glucose4> ...
c 16 assumptions
[0.330] Executed solver; result: UNSAT.
[0.330] 
[0.330] *************************************
[0.330] * * *   M a k e s p a n     1   * * *
[0.330] *************************************
[0.374] Computed next depth properties: array size of 76.
[0.470] Instantiated 304,290 transitional clauses.
[0.525] Instantiated 425,060 universal clauses.
[0.525] Instantiated and added clauses for a total of 790,281 clauses.
[0.525] The encoding contains a total of 83,845 distinct variables.
[0.525] Attempting solve with solver <glucose4> ...
c 76 assumptions
c last restart ## conflicts  :  159 9198 
[0.736] Executed solver; result: SAT.
[0.736] Solver returned SAT; a solution has been found at makespan 1.
75
solution 3299 1
150 25 8 26 10 2118 137 1158 1234 1160 2540 11 854 2842 856 1661 16 4 2903 6 835 58 848 875 850 1454 109 851 3003 853 658 83 12 946 14 377 76 854 1003 856 579 86 848 1067 850 325 130 851 1146 853 1366 55 1161 3040 1163 2225 46 8 3096 10 2676 123 851 3189 853 1884 39 1158 3213 1160 2333 100 1155 3299 1157 
[0.736] Exiting.

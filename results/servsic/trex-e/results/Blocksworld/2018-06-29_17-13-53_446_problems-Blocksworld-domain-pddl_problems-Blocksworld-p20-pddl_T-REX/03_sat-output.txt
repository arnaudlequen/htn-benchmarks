Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.092] Processed problem encoding.
[0.178] Calculated possible fact changes of composite elements.
[0.180] Initialized instantiation procedure.
[0.180] 
[0.180] *************************************
[0.180] * * *   M a k e s p a n     0   * * *
[0.180] *************************************
[0.213] Instantiated 141,474 initial clauses.
[0.213] The encoding contains a total of 71,744 distinct variables.
[0.213] Attempting solve with solver <glucose4> ...
c 37 assumptions
[0.214] Executed solver; result: UNSAT.
[0.214] 
[0.214] *************************************
[0.214] * * *   M a k e s p a n     1   * * *
[0.214] *************************************
[0.223] Computed next depth properties: array size of 145.
[0.245] Instantiated 6,660 transitional clauses.
[0.330] Instantiated 452,808 universal clauses.
[0.330] Instantiated and added clauses for a total of 600,942 clauses.
[0.330] The encoding contains a total of 222,309 distinct variables.
[0.330] Attempting solve with solver <glucose4> ...
c 145 assumptions
[0.362] Executed solver; result: UNSAT.
[0.362] 
[0.362] *************************************
[0.362] * * *   M a k e s p a n     2   * * *
[0.362] *************************************
[0.411] Computed next depth properties: array size of 361.
[0.504] Instantiated 154,908 transitional clauses.
[0.781] Instantiated 1,419,300 universal clauses.
[0.781] Instantiated and added clauses for a total of 2,175,150 clauses.
[0.781] The encoding contains a total of 516,323 distinct variables.
[0.781] Attempting solve with solver <glucose4> ...
c 361 assumptions
[0.846] Executed solver; result: UNSAT.
[0.846] 
[0.846] *************************************
[0.846] * * *   M a k e s p a n     3   * * *
[0.846] *************************************
[0.961] Computed next depth properties: array size of 505.
[1.691] Instantiated 768,528 transitional clauses.
[2.372] Instantiated 4,395,024 universal clauses.
[2.372] Instantiated and added clauses for a total of 7,338,702 clauses.
[2.372] The encoding contains a total of 1,051,429 distinct variables.
[2.372] Attempting solve with solver <glucose4> ...
c 505 assumptions
[2.575] Executed solver; result: UNSAT.
[2.575] 
[2.575] *************************************
[2.575] * * *   M a k e s p a n     4   * * *
[2.575] *************************************
[2.707] Computed next depth properties: array size of 649.
[3.466] Instantiated 783,648 transitional clauses.
[4.344] Instantiated 5,006,880 universal clauses.
[4.344] Instantiated and added clauses for a total of 13,129,230 clauses.
[4.344] The encoding contains a total of 1,592,727 distinct variables.
[4.344] Attempting solve with solver <glucose4> ...
c 649 assumptions
[4.587] Executed solver; result: UNSAT.
[4.587] 
[4.587] *************************************
[4.587] * * *   M a k e s p a n     5   * * *
[4.587] *************************************
[4.731] Computed next depth properties: array size of 793.
[5.497] Instantiated 783,648 transitional clauses.
[6.545] Instantiated 5,558,256 universal clauses.
[6.545] Instantiated and added clauses for a total of 19,471,134 clauses.
[6.545] The encoding contains a total of 2,134,169 distinct variables.
[6.545] Attempting solve with solver <glucose4> ...
c 793 assumptions
[6.854] Executed solver; result: UNSAT.
[6.854] 
[6.854] *************************************
[6.854] * * *   M a k e s p a n     6   * * *
[6.854] *************************************
[7.011] Computed next depth properties: array size of 937.
[7.772] Instantiated 783,648 transitional clauses.
[9.013] Instantiated 6,109,632 universal clauses.
[9.013] Instantiated and added clauses for a total of 26,364,414 clauses.
[9.013] The encoding contains a total of 2,675,755 distinct variables.
[9.013] Attempting solve with solver <glucose4> ...
c 937 assumptions
[9.380] Executed solver; result: UNSAT.
[9.380] 
[9.380] *************************************
[9.380] * * *   M a k e s p a n     7   * * *
[9.380] *************************************
[9.558] Computed next depth properties: array size of 1081.
[10.329] Instantiated 783,648 transitional clauses.
[11.765] Instantiated 6,661,008 universal clauses.
[11.765] Instantiated and added clauses for a total of 33,809,070 clauses.
[11.765] The encoding contains a total of 3,217,485 distinct variables.
[11.765] Attempting solve with solver <glucose4> ...
c 1081 assumptions
c last restart ## conflicts  :  0 11342 
[12.366] Executed solver; result: SAT.
[12.366] Solver returned SAT; a solution has been found at makespan 7.
130
solution 1923 1
1153 1122 1418 1380 1240 1208 1446 1423 1009 993 716 692 1091 1079 556 520 1918 1919 1655 1638 740 1883 1522 1509 582 563 873 864 405 391 648 649 3 1853 50 47 182 176 312 305 369 348 252 219 1890 1891 1878 1879 1333 1294 1765 1921 1898 1899 789 778 507 477 1372 1337 1566 1552 629 1877 1860 1861 1884 1885 1922 1923 1916 1917 1866 1867 288 262 1193 1897 110 90 926 907 861 821 1880 1881 1868 1869 1908 1909 1469 1466 1858 1859 1902 1903 1854 1855 1906 1907 1874 1875 1886 1887 1888 1889 1862 1863 1872 1873 1904 1905 1894 1895 1910 1911 1856 1857 1864 1865 1900 1901 1870 1871 1892 1893 1914 1915 1912 1913 
[12.380] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.012] Processed problem encoding.
[0.015] Calculated possible fact changes of composite elements.
[0.015] Initialized instantiation procedure.
[0.015] 
[0.015] *************************************
[0.015] * * *   M a k e s p a n     0   * * *
[0.015] *************************************
[0.022] Instantiated 12,057 initial clauses.
[0.022] The encoding contains a total of 6,214 distinct variables.
[0.022] Attempting solve with solver <glucose4> ...
c 19 assumptions
[0.022] Executed solver; result: UNSAT.
[0.022] 
[0.022] *************************************
[0.022] * * *   M a k e s p a n     1   * * *
[0.022] *************************************
[0.024] Computed next depth properties: array size of 73.
[0.028] Instantiated 1,458 transitional clauses.
[0.044] Instantiated 42,264 universal clauses.
[0.044] Instantiated and added clauses for a total of 55,779 clauses.
[0.044] The encoding contains a total of 20,517 distinct variables.
[0.044] Attempting solve with solver <glucose4> ...
c 73 assumptions
[0.046] Executed solver; result: UNSAT.
[0.046] 
[0.046] *************************************
[0.046] * * *   M a k e s p a n     2   * * *
[0.046] *************************************
[0.053] Computed next depth properties: array size of 181.
[0.065] Instantiated 14,742 transitional clauses.
[0.102] Instantiated 133,938 universal clauses.
[0.102] Instantiated and added clauses for a total of 204,459 clauses.
[0.102] The encoding contains a total of 47,501 distinct variables.
[0.102] Attempting solve with solver <glucose4> ...
c 181 assumptions
[0.106] Executed solver; result: UNSAT.
[0.106] 
[0.106] *************************************
[0.106] * * *   M a k e s p a n     3   * * *
[0.106] *************************************
[0.117] Computed next depth properties: array size of 253.
[0.152] Instantiated 56,664 transitional clauses.
[0.203] Instantiated 325,296 universal clauses.
[0.203] Instantiated and added clauses for a total of 586,419 clauses.
[0.203] The encoding contains a total of 90,271 distinct variables.
[0.203] Attempting solve with solver <glucose4> ...
c 253 assumptions
[0.215] Executed solver; result: UNSAT.
[0.215] 
[0.215] *************************************
[0.215] * * *   M a k e s p a n     4   * * *
[0.215] *************************************
[0.223] Computed next depth properties: array size of 325.
[0.252] Instantiated 59,544 transitional clauses.
[0.316] Instantiated 381,096 universal clauses.
[0.316] Instantiated and added clauses for a total of 1,027,059 clauses.
[0.316] The encoding contains a total of 134,265 distinct variables.
[0.316] Attempting solve with solver <glucose4> ...
c 325 assumptions
c last restart ## conflicts  :  3 1754 
[0.335] Executed solver; result: SAT.
[0.335] Solver returned SAT; a solution has been found at makespan 4.
60
solution 317 1
15 4 215 208 130 123 306 307 280 276 85 72 248 242 312 313 292 293 179 309 116 106 194 191 57 55 260 259 29 21 296 297 298 299 310 311 294 295 173 174 15 4 221 208 314 315 312 313 292 293 308 309 300 301 101 303 304 305 316 317 
[0.336] Exiting.

import re
import numpy as np
import matplotlib.pyplot as plt
import os

MAX = 20

def main():

    dirlist = next(os.walk("."))[1]

    if "Figures" in dirlist:
        dirlist.remove("Figures")

    if "Data" in dirlist:
        dirlist.remove("Data")

    for dirname in dirlist:
        print(dirname, end=".. ")
        plan_length = [-1]
        time = [-1]
        for i in range(1, MAX + 1):
            pad = str(i).zfill(2)
            in_file = open(dirname + "/result" + pad, "rt")
            tsk = False
            plan_length.append(0)
            time.append(0)
            for line in in_file:
                if "Timeout after " in line:
                    plan_length[i] = -1
                    time[i] = -1
                    break

                if "actions in the plan" in line:
                    plan_length[i] = int(re.sub("[^0-9]", "", line))

                if "real	" in line:
                    t = line.split("m")
                    m = float(re.sub("[^0-9]", "", t[0]))
                    n = float(re.sub("[^0-9,^0-9]", "", t[1]).replace(",", "."))
                    time[i] = 60*m + n
            in_file.close()

        plan_length.remove(-1)
        time.remove(-1)

        if not os.path.exists("Figures/"):
            os.makedirs("Figures/")

        if not os.path.exists("Figures/" + dirname):
            os.makedirs("Figures/" + dirname)

        plt.plot(list(range(1, 21)), time, 'ro')
        plt.xlabel("Problem")
        plt.ylabel("Running Time (s)")
        plt.grid(True)
        plt.xticks(list(range(0, 21, 5)))
        plt.savefig("Figures/" + dirname + "/time.png")
        plt.clf()

        plt.plot(list(range(1, 21)), plan_length, 'ro')
        plt.xlabel("Problem")
        plt.ylabel("Plan length")
        plt.grid(True)
        plt.xticks(list(range(0, 21, 5)))
        plt.savefig("Figures/" + dirname + "/length.png")
        plt.clf()

        if not os.path.exists("Data/"):
            os.makedirs("Data/")

        if not os.path.exists("Data/" + dirname):
            os.makedirs("Data/" + dirname)

        f = open("Data/" + dirname + "/time.txt", "w+")
        for l in time:
            f.write(str(l) + "\n")
        f.close()

        f = open("Data/" + dirname + "/length.txt", "w+")
        for l in plan_length:
            f.write(str(l) + "\n")
        f.close()

        print("done")

main()

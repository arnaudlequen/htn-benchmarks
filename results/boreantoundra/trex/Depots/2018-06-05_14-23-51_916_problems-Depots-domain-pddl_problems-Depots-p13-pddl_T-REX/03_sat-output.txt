Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.008] Processed problem encoding.
[0.010] Calculated possible fact changes of composite elements.
[0.010] Initialized instantiation procedure.
[0.010] 
[0.010] *************************************
[0.010] * * *   M a k e s p a n     0   * * *
[0.010] *************************************
[0.011] Instantiated 2,688 initial clauses.
[0.011] The encoding contains a total of 1,662 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.011] Executed solver; result: UNSAT.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     1   * * *
[0.011] *************************************
[0.012] Computed next depth properties: array size of 25.
[0.013] Instantiated 2,660 transitional clauses.
[0.015] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.015] Instantiated 9,957 universal clauses.
[0.015] Instantiated and added clauses for a total of 15,305 clauses.
[0.015] The encoding contains a total of 4,348 distinct variables.
[0.015] Attempting solve with solver <glucose4> ...
c 25 assumptions
[0.015] Executed solver; result: UNSAT.
[0.015] 
[0.015] *************************************
[0.015] * * *   M a k e s p a n     2   * * *
[0.015] *************************************
[0.017] Computed next depth properties: array size of 73.
[0.022] Instantiated 9,506 transitional clauses.
[0.028] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.028] Instantiated 43,697 universal clauses.
[0.028] Instantiated and added clauses for a total of 68,508 clauses.
[0.028] The encoding contains a total of 9,452 distinct variables.
[0.028] Attempting solve with solver <glucose4> ...
c 73 assumptions
[0.028] Executed solver; result: UNSAT.
[0.028] 
[0.028] *************************************
[0.028] * * *   M a k e s p a n     3   * * *
[0.028] *************************************
[0.031] Computed next depth properties: array size of 145.
[0.041] Instantiated 20,864 transitional clauses.
[0.055] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.055] Instantiated 92,867 universal clauses.
[0.055] Instantiated and added clauses for a total of 182,239 clauses.
[0.055] The encoding contains a total of 18,576 distinct variables.
[0.055] Attempting solve with solver <glucose4> ...
c 145 assumptions
[0.055] Executed solver; result: UNSAT.
[0.055] 
[0.055] *************************************
[0.055] * * *   M a k e s p a n     4   * * *
[0.055] *************************************
[0.060] Computed next depth properties: array size of 217.
[0.079] Instantiated 43,790 transitional clauses.
[0.104] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.104] Instantiated 198,677 universal clauses.
[0.104] Instantiated and added clauses for a total of 424,706 clauses.
[0.104] The encoding contains a total of 30,862 distinct variables.
[0.104] Attempting solve with solver <glucose4> ...
c 217 assumptions
c last restart ## conflicts  :  4 236 
[0.116] Executed solver; result: SAT.
[0.116] Solver returned SAT; a solution has been found at makespan 4.
27
solution 257 1
27 257 63 133 223 121 117 119 46 87 41 88 10 3 25 11 4 75 13 5 204 6 147 7 62 19 8 
[0.117] Exiting.

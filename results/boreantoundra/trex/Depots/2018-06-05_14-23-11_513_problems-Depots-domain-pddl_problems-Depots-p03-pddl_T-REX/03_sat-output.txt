Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.004] Processed problem encoding.
[0.005] Calculated possible fact changes of composite elements.
[0.005] Initialized instantiation procedure.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     0   * * *
[0.005] *************************************
[0.005] Instantiated 1,670 initial clauses.
[0.005] The encoding contains a total of 1,043 distinct variables.
[0.005] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.005] Executed solver; result: UNSAT.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     1   * * *
[0.005] *************************************
[0.006] Computed next depth properties: array size of 25.
[0.007] Instantiated 2,231 transitional clauses.
[0.009] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.009] Instantiated 9,271 universal clauses.
[0.009] Instantiated and added clauses for a total of 13,172 clauses.
[0.009] The encoding contains a total of 3,369 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 25 assumptions
[0.009] Executed solver; result: UNSAT.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     2   * * *
[0.009] *************************************
[0.010] Computed next depth properties: array size of 73.
[0.013] Instantiated 7,208 transitional clauses.
[0.019] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.019] Instantiated 31,954 universal clauses.
[0.019] Instantiated and added clauses for a total of 52,334 clauses.
[0.019] The encoding contains a total of 8,425 distinct variables.
[0.019] Attempting solve with solver <glucose4> ...
c 73 assumptions
[0.019] Executed solver; result: UNSAT.
[0.019] 
[0.019] *************************************
[0.019] * * *   M a k e s p a n     3   * * *
[0.019] *************************************
[0.021] Computed next depth properties: array size of 145.
[0.029] Instantiated 21,071 transitional clauses.
[0.041] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.041] Instantiated 86,323 universal clauses.
[0.041] Instantiated and added clauses for a total of 159,728 clauses.
[0.041] The encoding contains a total of 17,300 distinct variables.
[0.041] Attempting solve with solver <glucose4> ...
c 145 assumptions
[0.042] Executed solver; result: UNSAT.
[0.042] 
[0.042] *************************************
[0.042] * * *   M a k e s p a n     4   * * *
[0.042] *************************************
[0.046] Computed next depth properties: array size of 217.
[0.061] Instantiated 37,076 transitional clauses.
[0.082] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.082] Instantiated 175,297 universal clauses.
[0.082] Instantiated and added clauses for a total of 372,101 clauses.
[0.082] The encoding contains a total of 29,151 distinct variables.
[0.082] Attempting solve with solver <glucose4> ...
c 217 assumptions
c last restart ## conflicts  :  7 241 
[0.092] Executed solver; result: SAT.
[0.092] Solver returned SAT; a solution has been found at makespan 4.
32
solution 158 1
45 54 56 42 149 104 93 40 94 22 6 47 23 7 158 50 83 112 80 69 71 32 11 46 20 5 49 28 8 42 38 14 
[0.093] Exiting.

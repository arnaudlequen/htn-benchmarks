Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.001] This is T-REX interpreter, prototype version 04/2018.
[0.001] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.067] Processed problem encoding.
[0.109] Calculated possible fact changes of composite elements.
[0.118] Initialized instantiation procedure.
[0.118] 
[0.118] *************************************
[0.118] * * *   M a k e s p a n     0   * * *
[0.118] *************************************
[0.122] Instantiated 10,463 initial clauses.
[0.122] The encoding contains a total of 8,379 distinct variables.
[0.122] Attempting solve with solver <glucose4> ...
c 8 assumptions
[0.122] Executed solver; result: UNSAT.
[0.122] 
[0.122] *************************************
[0.122] * * *   M a k e s p a n     1   * * *
[0.122] *************************************
[0.127] Computed next depth properties: array size of 29.
[0.142] Instantiated 31,029 transitional clauses.
[0.162] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.162] Instantiated 207,106 universal clauses.
[0.162] Instantiated and added clauses for a total of 248,598 clauses.
[0.162] The encoding contains a total of 18,666 distinct variables.
[0.162] Attempting solve with solver <glucose4> ...
c 29 assumptions
[0.162] Executed solver; result: UNSAT.
[0.162] 
[0.162] *************************************
[0.162] * * *   M a k e s p a n     2   * * *
[0.162] *************************************
[0.173] Computed next depth properties: array size of 85.
[0.218] Instantiated 75,293 transitional clauses.
[0.304] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.304] Instantiated 948,603 universal clauses.
[0.304] Instantiated and added clauses for a total of 1,272,494 clauses.
[0.304] The encoding contains a total of 47,537 distinct variables.
[0.304] Attempting solve with solver <glucose4> ...
c 85 assumptions
[0.306] Executed solver; result: UNSAT.
[0.306] 
[0.306] *************************************
[0.306] * * *   M a k e s p a n     3   * * *
[0.306] *************************************
[0.333] Computed next depth properties: array size of 169.
[0.472] Instantiated 253,487 transitional clauses.
[0.674] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.674] Instantiated 2,413,823 universal clauses.
[0.674] Instantiated and added clauses for a total of 3,939,804 clauses.
[0.674] The encoding contains a total of 105,404 distinct variables.
[0.674] Attempting solve with solver <glucose4> ...
c 169 assumptions
[0.697] Executed solver; result: UNSAT.
[0.697] 
[0.697] *************************************
[0.697] * * *   M a k e s p a n     4   * * *
[0.697] *************************************
[0.736] Computed next depth properties: array size of 253.
[0.981] Instantiated 492,163 transitional clauses.
[1.332] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.332] Instantiated 4,774,611 universal clauses.
[1.332] Instantiated and added clauses for a total of 9,206,578 clauses.
[1.332] The encoding contains a total of 181,669 distinct variables.
[1.332] Attempting solve with solver <glucose4> ...
c 253 assumptions
c last restart ## conflicts  :  6 307 
[1.405] Executed solver; result: SAT.
[1.405] Solver returned SAT; a solution has been found at makespan 4.
41
solution 904 1
396 397 146 533 534 107 22 366 686 627 337 904 266 590 578 580 74 13 215 518 521 245 132 28 88 19 258 690 485 232 31 3 312 411 412 176 35 4 295 53 11 
[1.407] Exiting.

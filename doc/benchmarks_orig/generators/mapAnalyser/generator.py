#! /usr/bin/env python

## MAPANALYSER DOMAIN GENERATOR. 
## IPC 2014
## authors: Mauro Vallati, Lukas Chrpa -- University of Huddersfield

import string
import sys
import random

def help():
	print 'usage: generator.py <rows> <columns> <n_cars> <n_garage>'
	print '\t row and columns indicate the grid size. They should be > 1 '
	print '\t n_cars indicates how many cars have to go through the network'
	print '\t n_garage indicates the number of starting garages'
	print '\t sparse is used for designing networks with \'holes\', i.e. junctions that cannot be used'
	sys.exit(2)




to_print_car_pos=""


if len(sys.argv) != 6:
	help()
row=int(sys.argv[1])
column=int(sys.argv[2])
car=int(sys.argv[3])
garage=int(sys.argv[4])
sparse=int(sys.argv[5])
road=int(sys.argv[1]) + 2


matrix = [ [ "free" for i in range(column)] for j in range(row)  ]


for posizionate in range(car):
	x=random.randint(0,garage-1)
	to_print_car_pos=to_print_car_pos+'(starting car'+str(posizionate)+' garage'+str(x)+')\n'

				
	#mix up!	
# Finally, time for printing everything out.

print '(define (problem citycar-'+str(row)+'-'+str(column)+'-'+str(car)+')'
print '(:domain mapanalyzer)'
print '(:objects ',

for i in range(row):
	print ''
	for j in range(column):
		x='junction'+str(i)+'-'+str(j)
		print x,
print '- junction' 

for i in range(car):
	x='car'+str(i)
	print x,
print '- car'

for i in range(garage):
	x='garage'+str(i)
	print x,
print '- garage'

for i in range(road):
	x='road'+str(i)
	print x,
print '- road'

print ')\n(:init'

print '\t(=(build-time) 5)'
print '\t(=(remove-time) 3)'
print '\t(=(arrived-time) 1)'
print '\t(=(start-time) 1)'

for i in range(car):
	value=random.randint(1,15)
	print '\t(=(speed car'+str(i)+') '+str(value)+')'

for i in range(road):
	print '(available road'+str(i)+')'

# connected cells on the same line
for i in range(row):
	for j in range(column-1):
		print '(connected junction'+str(i)+'-'+str(j)+' junction'+str(i)+'-'+str(j+1)+')'
		print '(connected junction'+str(i)+'-'+str(j+1)+' junction'+str(i)+'-'+str(j)+')'
		value=random.randint(1,100)
		print '(=(distance junction'+str(i)+'-'+str(j)+' junction'+str(i)+'-'+str(j+1)+') '+str(value)+')'
		print '(=(distance junction'+str(i)+'-'+str(j+1)+' junction'+str(i)+'-'+str(j)+') '+str(value)+')'

# connected cells on the same column
for i in range(column):
	for j in range(row-1):
		print '(connected junction'+str(j)+'-'+str(i)+' junction'+str(j+1)+'-'+str(i)+')'
		print '(connected junction'+str(j+1)+'-'+str(i)+' junction'+str(j)+'-'+str(i)+')'
		value=random.randint(1,100)
		print '(=(distance junction'+str(j)+'-'+str(i)+' junction'+str(j+1)+'-'+str(i)+') '+str(value)+')'
		print '(=(distance junction'+str(j+1)+'-'+str(i)+' junction'+str(j)+'-'+str(i)+') '+str(value)+')'

#free cells
for i in range(row):
	for j in range(column):
		if sparse != 0 and i != 0 and i < row -1 and j != 0 and j < column -1:
			x=random.randint(0,10)
			if x < 8:
				print '(clear junction'+str(i)+'-'+str(j)+')'
		else:
			print '(clear junction'+str(i)+'-'+str(j)+')'

for i in range(garage):
	x=random.randint(0,(column-1))
	print '(at_garage garage'+str(i)+' junction0'+'-'+str(x)+')'


#occupied cells
print to_print_car_pos,
print ')\n(:goal\n(and'

for i in range(car):
	x=random.randint(0,(column-1))
	print '(arrived car'+str(i)+' junction'+str(row-1)+'-'+str(x)+')'

print ')\n)\n(:metric minimize (total-time))\n)'




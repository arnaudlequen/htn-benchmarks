


(define (problem logistics-c3-s3-p3-a3)
(:domain logistics)
(:objects airplane0 airplane1 airplane2  - airplane
          city0 city1 city2  - city
          truck0 truck1 truck2  - truck
          loc0-0 loc1-0 loc2-0  - airport
          loc0-1 loc0-2 loc1-1 loc1-2 loc2-1 loc2-2  - location
          p0 p1 p2  - package
)
(:init
(in-city  loc0-0 city0)
(in-city  loc0-1 city0)
(in-city  loc0-2 city0)
(in-city  loc1-0 city1)
(in-city  loc1-1 city1)
(in-city  loc1-2 city1)
(in-city  loc2-0 city2)
(in-city  loc2-1 city2)
(in-city  loc2-2 city2)
(at truck0 loc0-0)
(at truck1 loc1-0)
(at truck2 loc2-2)
(at p0 loc0-2)
(at p1 loc0-0)
(at p2 loc1-1)
(at airplane0 loc0-0)
(at airplane1 loc0-0)
(at airplane2 loc2-0)
(= (total-cost) 0)
)
(:goal
(and
(at p0 loc2-0)
(at p1 loc1-0)
(at p2 loc0-0)
)
)
(:metric minimize (total-cost))

)



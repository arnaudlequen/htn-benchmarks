


(define (problem logistics-c5-s5-p5-a5)
(:domain logistics)
(:objects airplane0 airplane1 airplane2 airplane3 airplane4  - airplane
          city0 city1 city2 city3 city4  - city
          truck0 truck1 truck2 truck3 truck4  - truck
          loc0-0 loc1-0 loc2-0 loc3-0 loc4-0  - airport
          loc0-1 loc0-2 loc0-3 loc0-4 loc1-1 loc1-2 loc1-3 loc1-4 loc2-1 loc2-2 loc2-3 loc2-4 loc3-1 loc3-2 loc3-3 loc3-4 loc4-1 loc4-2 loc4-3 loc4-4  - location
          p0 p1 p2 p3 p4  - package
)
(:init
(in-city  loc0-0 city0)
(in-city  loc0-1 city0)
(in-city  loc0-2 city0)
(in-city  loc0-3 city0)
(in-city  loc0-4 city0)
(in-city  loc1-0 city1)
(in-city  loc1-1 city1)
(in-city  loc1-2 city1)
(in-city  loc1-3 city1)
(in-city  loc1-4 city1)
(in-city  loc2-0 city2)
(in-city  loc2-1 city2)
(in-city  loc2-2 city2)
(in-city  loc2-3 city2)
(in-city  loc2-4 city2)
(in-city  loc3-0 city3)
(in-city  loc3-1 city3)
(in-city  loc3-2 city3)
(in-city  loc3-3 city3)
(in-city  loc3-4 city3)
(in-city  loc4-0 city4)
(in-city  loc4-1 city4)
(in-city  loc4-2 city4)
(in-city  loc4-3 city4)
(in-city  loc4-4 city4)
(at truck0 loc0-3)
(at truck1 loc1-3)
(at truck2 loc2-3)
(at truck3 loc3-0)
(at truck4 loc4-2)
(at p0 loc3-1)
(at p1 loc3-1)
(at p2 loc0-3)
(at p3 loc1-0)
(at p4 loc4-0)
(at airplane0 loc3-0)
(at airplane1 loc2-0)
(at airplane2 loc2-0)
(at airplane3 loc1-0)
(at airplane4 loc0-0)
(= (total-cost) 0)
)
(:goal
(and
(at p0 loc0-0)
(at p1 loc2-4)
(at p2 loc3-0)
(at p3 loc4-3)
(at p4 loc0-1)
)
)
(:metric minimize (total-cost))

)



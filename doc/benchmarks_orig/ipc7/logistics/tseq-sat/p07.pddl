


(define (problem logistics-c8-s8-p8-a8)
(:domain logistics)
(:objects airplane0 airplane1 airplane2 airplane3 airplane4 airplane5 airplane6 airplane7  - airplane
          city0 city1 city2 city3 city4 city5 city6 city7  - city
          truck0 truck1 truck2 truck3 truck4 truck5 truck6 truck7  - truck
          loc0-0 loc1-0 loc2-0 loc3-0 loc4-0 loc5-0 loc6-0 loc7-0  - airport
          loc0-1 loc0-2 loc0-3 loc0-4 loc0-5 loc0-6 loc0-7 loc1-1 loc1-2 loc1-3 loc1-4 loc1-5 loc1-6 loc1-7 loc2-1 loc2-2 loc2-3 loc2-4 loc2-5 loc2-6 loc2-7 loc3-1 loc3-2 loc3-3 loc3-4 loc3-5 loc3-6 loc3-7 loc4-1 loc4-2 loc4-3 loc4-4 loc4-5 loc4-6 loc4-7 loc5-1 loc5-2 loc5-3 loc5-4 loc5-5 loc5-6 loc5-7 loc6-1 loc6-2 loc6-3 loc6-4 loc6-5 loc6-6 loc6-7 loc7-1 loc7-2 loc7-3 loc7-4 loc7-5 loc7-6 loc7-7  - location
          p0 p1 p2 p3 p4 p5 p6 p7  - package
)
(:init
(in-city  loc0-0 city0)
(in-city  loc0-1 city0)
(in-city  loc0-2 city0)
(in-city  loc0-3 city0)
(in-city  loc0-4 city0)
(in-city  loc0-5 city0)
(in-city  loc0-6 city0)
(in-city  loc0-7 city0)
(in-city  loc1-0 city1)
(in-city  loc1-1 city1)
(in-city  loc1-2 city1)
(in-city  loc1-3 city1)
(in-city  loc1-4 city1)
(in-city  loc1-5 city1)
(in-city  loc1-6 city1)
(in-city  loc1-7 city1)
(in-city  loc2-0 city2)
(in-city  loc2-1 city2)
(in-city  loc2-2 city2)
(in-city  loc2-3 city2)
(in-city  loc2-4 city2)
(in-city  loc2-5 city2)
(in-city  loc2-6 city2)
(in-city  loc2-7 city2)
(in-city  loc3-0 city3)
(in-city  loc3-1 city3)
(in-city  loc3-2 city3)
(in-city  loc3-3 city3)
(in-city  loc3-4 city3)
(in-city  loc3-5 city3)
(in-city  loc3-6 city3)
(in-city  loc3-7 city3)
(in-city  loc4-0 city4)
(in-city  loc4-1 city4)
(in-city  loc4-2 city4)
(in-city  loc4-3 city4)
(in-city  loc4-4 city4)
(in-city  loc4-5 city4)
(in-city  loc4-6 city4)
(in-city  loc4-7 city4)
(in-city  loc5-0 city5)
(in-city  loc5-1 city5)
(in-city  loc5-2 city5)
(in-city  loc5-3 city5)
(in-city  loc5-4 city5)
(in-city  loc5-5 city5)
(in-city  loc5-6 city5)
(in-city  loc5-7 city5)
(in-city  loc6-0 city6)
(in-city  loc6-1 city6)
(in-city  loc6-2 city6)
(in-city  loc6-3 city6)
(in-city  loc6-4 city6)
(in-city  loc6-5 city6)
(in-city  loc6-6 city6)
(in-city  loc6-7 city6)
(in-city  loc7-0 city7)
(in-city  loc7-1 city7)
(in-city  loc7-2 city7)
(in-city  loc7-3 city7)
(in-city  loc7-4 city7)
(in-city  loc7-5 city7)
(in-city  loc7-6 city7)
(in-city  loc7-7 city7)
(at truck0 loc0-3)
(at truck1 loc1-0)
(at truck2 loc2-2)
(at truck3 loc3-4)
(at truck4 loc4-2)
(at truck5 loc5-3)
(at truck6 loc6-6)
(at truck7 loc7-6)
(at p0 loc6-1)
(at p1 loc6-1)
(at p2 loc5-7)
(at p3 loc0-1)
(at p4 loc5-7)
(at p5 loc3-7)
(at p6 loc7-1)
(at p7 loc4-5)
(at airplane0 loc0-0)
(at airplane1 loc4-0)
(at airplane2 loc6-0)
(at airplane3 loc5-0)
(at airplane4 loc3-0)
(at airplane5 loc7-0)
(at airplane6 loc6-0)
(at airplane7 loc0-0)
(= (total-cost) 0)
)
(:goal
(and
(at p0 loc0-4)
(at p1 loc7-1)
(at p2 loc0-5)
(at p3 loc2-3)
(at p4 loc5-4)
(at p5 loc0-0)
(at p6 loc7-6)
(at p7 loc6-5)
)
)
(:metric minimize (total-cost))

)



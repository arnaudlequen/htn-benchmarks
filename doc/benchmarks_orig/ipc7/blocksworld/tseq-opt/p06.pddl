

(define (problem BW-rand-8)
(:domain blocksworld)
(:objects b1 b2 b3 b4 b5 b6 b7 b8  - block)
(:init
(handempty)
(ontable b1)
(on b2 b4)
(on b3 b1)
(ontable b4)
(ontable b5)
(ontable b6)
(on b7 b3)
(on b8 b5)
(clear b2)
(clear b6)
(clear b7)
(clear b8)
(= (total-cost) 0)
)
(:goal
(and
(on b3 b1)
(on b4 b5)
(on b5 b2)
(on b6 b3))
)
(:metric minimize (total-cost)))






(define (problem BW-rand-7)
(:domain blocksworld)
(:objects b1 b2 b3 b4 b5 b6 b7  - block)
(:init
(handempty)
(on b1 b2)
(ontable b2)
(ontable b3)
(on b4 b7)
(on b5 b6)
(ontable b6)
(on b7 b5)
(clear b1)
(clear b3)
(clear b4)
(= (total-cost) 0)
)
(:goal
(and
(on b1 b7)
(on b2 b4)
(on b3 b2)
(on b4 b6))
)
(:metric minimize (total-cost)))




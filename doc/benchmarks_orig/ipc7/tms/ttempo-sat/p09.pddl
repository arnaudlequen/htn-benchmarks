(define (problem pfile8)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 pone3 pone4 pone5 pone6 pone7 pone8 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 ptwo4 ptwo5 ptwo6 ptwo7 ptwo8 ptwo9 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 pthree5 pthree6 pthree7 pthree8 pthree9 pthree10 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure ptwo2 ptwo8)
     (baked-structure pthree5 pthree8)
     (baked-structure pone3 pone7)
     (baked-structure ptwo4 pone6)
     (baked-structure pthree10 ptwo6)
     (baked-structure ptwo3 pthree2)
     (baked-structure pone1 pone0)
     (baked-structure pthree0 pthree3)
     (baked-structure pone2 pone8)
     (baked-structure pthree7 ptwo0)
     (baked-structure pone4 ptwo9)
     (baked-structure pthree4 pthree9)
     (baked-structure ptwo5 pthree6)
     (baked-structure ptwo1 pone5)
     (baked-structure ptwo7 pthree1)
))
 (:metric minimize (total-time))
)

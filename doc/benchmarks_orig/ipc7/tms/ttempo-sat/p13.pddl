(define (problem pfile12)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 pone3 pone4 pone5 pone6 pone7 pone8 pone9 pone10 pone11 pone12 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 ptwo4 ptwo5 ptwo6 ptwo7 ptwo8 ptwo9 ptwo10 ptwo11 ptwo12 ptwo13 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 pthree5 pthree6 pthree7 pthree8 pthree9 pthree10 pthree11 pthree12 pthree13 pthree14 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure ptwo11 pthree5)
     (baked-structure ptwo4 ptwo6)
     (baked-structure pone2 pthree9)
     (baked-structure ptwo9 pone8)
     (baked-structure ptwo12 pthree0)
     (baked-structure pthree3 pthree13)
     (baked-structure ptwo1 ptwo13)
     (baked-structure ptwo7 pthree4)
     (baked-structure pone12 pthree10)
     (baked-structure pone3 pthree14)
     (baked-structure ptwo2 pone1)
     (baked-structure pone10 pthree12)
     (baked-structure pone0 pthree11)
     (baked-structure ptwo5 pthree8)
     (baked-structure pone6 pone9)
     (baked-structure pthree7 pone5)
     (baked-structure ptwo3 pthree6)
     (baked-structure pone7 pone11)
     (baked-structure pthree1 pthree2)
     (baked-structure ptwo0 ptwo8)
     (baked-structure pone4 ptwo10)
))
 (:metric minimize (total-time))
)

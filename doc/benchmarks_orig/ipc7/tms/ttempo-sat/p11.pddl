(define (problem pfile10)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 pone3 pone4 pone5 pone6 pone7 pone8 pone9 pone10 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 ptwo4 ptwo5 ptwo6 ptwo7 ptwo8 ptwo9 ptwo10 ptwo11 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 pthree5 pthree6 pthree7 pthree8 pthree9 pthree10 pthree11 pthree12 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure pone2 pone4)
     (baked-structure ptwo2 ptwo11)
     (baked-structure pthree6 pone7)
     (baked-structure ptwo4 pthree2)
     (baked-structure ptwo0 pone3)
     (baked-structure pone8 pthree7)
     (baked-structure ptwo8 pthree10)
     (baked-structure pthree12 pone0)
     (baked-structure pthree5 pone6)
     (baked-structure pthree4 pthree11)
     (baked-structure pthree9 pthree3)
     (baked-structure ptwo3 ptwo6)
     (baked-structure pone1 pthree1)
     (baked-structure ptwo9 pone5)
     (baked-structure ptwo7 pone9)
     (baked-structure pthree8 ptwo10)
     (baked-structure pone10 pthree0)
     (baked-structure ptwo1 ptwo5)
))
 (:metric minimize (total-time))
)

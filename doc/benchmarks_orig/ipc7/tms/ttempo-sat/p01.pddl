(define (problem pfile0)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 - piecetype1
 ptwo0 ptwo1 - piecetype2
 pthree0 pthree1 pthree2 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure pthree0 pthree2)
     (baked-structure pone0 ptwo1)
     (baked-structure pthree1 ptwo0)
))
 (:metric minimize (total-time))
)

(define (problem pfile3)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 pone3 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 ptwo4 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 pthree5 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure ptwo2 pone2)
     (baked-structure pone3 ptwo3)
     (baked-structure ptwo1 pthree3)
     (baked-structure ptwo4 pone1)
     (baked-structure ptwo0 pthree4)
     (baked-structure pthree2 pthree1)
     (baked-structure pone0 pthree5)
))
 (:metric minimize (total-time))
)

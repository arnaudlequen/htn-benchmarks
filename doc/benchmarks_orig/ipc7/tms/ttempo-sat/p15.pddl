(define (problem pfile14)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 pone3 pone4 pone5 pone6 pone7 pone8 pone9 pone10 pone11 pone12 pone13 pone14 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 ptwo4 ptwo5 ptwo6 ptwo7 ptwo8 ptwo9 ptwo10 ptwo11 ptwo12 ptwo13 ptwo14 ptwo15 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 pthree5 pthree6 pthree7 pthree8 pthree9 pthree10 pthree11 pthree12 pthree13 pthree14 pthree15 pthree16 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure pthree7 pone13)
     (baked-structure ptwo12 pone6)
     (baked-structure pone2 ptwo5)
     (baked-structure ptwo9 pthree8)
     (baked-structure pthree11 ptwo13)
     (baked-structure pone3 pthree3)
     (baked-structure pone1 pone7)
     (baked-structure ptwo6 ptwo7)
     (baked-structure ptwo11 pone12)
     (baked-structure pthree1 pone5)
     (baked-structure pone8 pone0)
     (baked-structure pthree9 pthree4)
     (baked-structure ptwo14 pthree15)
     (baked-structure pthree5 pthree2)
     (baked-structure pone11 pthree14)
     (baked-structure pone10 ptwo15)
     (baked-structure ptwo10 ptwo1)
     (baked-structure pthree16 pthree10)
     (baked-structure pthree6 ptwo8)
     (baked-structure pone4 ptwo0)
     (baked-structure pthree0 ptwo3)
     (baked-structure pone14 pthree13)
     (baked-structure ptwo4 pone9)
     (baked-structure ptwo2 pthree12)
))
 (:metric minimize (total-time))
)

(define (problem pfile9)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 pone3 pone4 pone5 pone6 pone7 pone8 pone9 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 ptwo4 ptwo5 ptwo6 ptwo7 ptwo8 ptwo9 ptwo10 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 pthree5 pthree6 pthree7 pthree8 pthree9 pthree10 pthree11 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure pone0 pone8)
     (baked-structure pthree7 pone3)
     (baked-structure pthree11 pthree2)
     (baked-structure pone9 pthree6)
     (baked-structure pone4 ptwo5)
     (baked-structure pthree1 ptwo6)
     (baked-structure ptwo9 ptwo7)
     (baked-structure pone5 pone1)
     (baked-structure pthree8 pthree4)
     (baked-structure ptwo0 ptwo3)
     (baked-structure ptwo10 ptwo4)
     (baked-structure ptwo8 pthree9)
     (baked-structure pthree10 pone7)
     (baked-structure ptwo2 ptwo1)
     (baked-structure pone6 pthree5)
     (baked-structure pthree3 pthree0)
))
 (:metric minimize (total-time))
)

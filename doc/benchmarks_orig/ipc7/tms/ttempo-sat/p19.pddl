(define (problem pfile18)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 pone3 pone4 pone5 pone6 pone7 pone8 pone9 pone10 pone11 pone12 pone13 pone14 pone15 pone16 pone17 pone18 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 ptwo4 ptwo5 ptwo6 ptwo7 ptwo8 ptwo9 ptwo10 ptwo11 ptwo12 ptwo13 ptwo14 ptwo15 ptwo16 ptwo17 ptwo18 ptwo19 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 pthree5 pthree6 pthree7 pthree8 pthree9 pthree10 pthree11 pthree12 pthree13 pthree14 pthree15 pthree16 pthree17 pthree18 pthree19 pthree20 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure pone17 pthree3)
     (baked-structure ptwo3 pone9)
     (baked-structure ptwo4 pone14)
     (baked-structure ptwo9 ptwo18)
     (baked-structure ptwo7 pone2)
     (baked-structure pthree4 pthree6)
     (baked-structure ptwo2 pthree12)
     (baked-structure pone5 pone0)
     (baked-structure pthree16 ptwo15)
     (baked-structure pone8 ptwo1)
     (baked-structure pone6 ptwo10)
     (baked-structure pone4 ptwo14)
     (baked-structure ptwo12 pthree20)
     (baked-structure ptwo6 pthree7)
     (baked-structure pone11 pthree1)
     (baked-structure pthree19 ptwo0)
     (baked-structure pthree15 ptwo19)
     (baked-structure ptwo17 ptwo5)
     (baked-structure pthree14 pthree9)
     (baked-structure ptwo13 ptwo16)
     (baked-structure pthree0 pthree10)
     (baked-structure pone12 pthree13)
     (baked-structure pthree2 pone18)
     (baked-structure pthree18 pthree5)
     (baked-structure pone1 ptwo11)
     (baked-structure pone3 pone15)
     (baked-structure pone13 ptwo8)
     (baked-structure pthree17 pone10)
     (baked-structure pone16 pthree11)
     (baked-structure pone7 pthree8)
))
 (:metric minimize (total-time))
)

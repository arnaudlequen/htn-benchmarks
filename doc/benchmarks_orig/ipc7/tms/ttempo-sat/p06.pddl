(define (problem pfile5)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 pone3 pone4 pone5 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 ptwo4 ptwo5 ptwo6 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 pthree5 pthree6 pthree7 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure pthree3 pone3)
     (baked-structure ptwo2 ptwo4)
     (baked-structure ptwo3 ptwo0)
     (baked-structure ptwo1 ptwo6)
     (baked-structure pthree4 pthree6)
     (baked-structure pthree5 ptwo5)
     (baked-structure pone0 pone2)
     (baked-structure pone4 pthree1)
     (baked-structure pthree7 pone1)
     (baked-structure pthree0 pthree2)
))
 (:metric minimize (total-time))
)

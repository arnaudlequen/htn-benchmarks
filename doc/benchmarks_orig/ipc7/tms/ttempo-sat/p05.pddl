(define (problem pfile4)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 pone3 pone4 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 ptwo4 ptwo5 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 pthree5 pthree6 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure pthree3 pthree5)
     (baked-structure ptwo4 pthree1)
     (baked-structure pthree4 pthree2)
     (baked-structure pone4 pthree6)
     (baked-structure ptwo5 pthree0)
     (baked-structure pone0 pone1)
     (baked-structure ptwo2 pone3)
     (baked-structure ptwo0 ptwo3)
     (baked-structure pone2 ptwo1)
))
 (:metric minimize (total-time))
)

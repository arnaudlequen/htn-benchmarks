(define (problem pfile16)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 pone3 pone4 pone5 pone6 pone7 pone8 pone9 pone10 pone11 pone12 pone13 pone14 pone15 pone16 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 ptwo4 ptwo5 ptwo6 ptwo7 ptwo8 ptwo9 ptwo10 ptwo11 ptwo12 ptwo13 ptwo14 ptwo15 ptwo16 ptwo17 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 pthree5 pthree6 pthree7 pthree8 pthree9 pthree10 pthree11 pthree12 pthree13 pthree14 pthree15 pthree16 pthree17 pthree18 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure pthree6 pthree14)
     (baked-structure pone10 pone5)
     (baked-structure pthree3 ptwo5)
     (baked-structure pone12 pthree13)
     (baked-structure pone15 pthree1)
     (baked-structure pone2 pone16)
     (baked-structure ptwo2 ptwo4)
     (baked-structure pone13 pthree16)
     (baked-structure ptwo14 ptwo11)
     (baked-structure pthree17 ptwo9)
     (baked-structure pone6 pone9)
     (baked-structure pthree8 ptwo7)
     (baked-structure pone3 ptwo1)
     (baked-structure pone8 pthree10)
     (baked-structure ptwo8 pone11)
     (baked-structure pthree0 pone1)
     (baked-structure ptwo17 ptwo16)
     (baked-structure ptwo6 pthree4)
     (baked-structure ptwo3 ptwo15)
     (baked-structure pthree5 pthree18)
     (baked-structure ptwo0 pone14)
     (baked-structure pone4 pthree9)
     (baked-structure ptwo12 ptwo10)
     (baked-structure pthree12 ptwo13)
     (baked-structure pthree7 pthree2)
     (baked-structure pone0 pone7)
     (baked-structure pthree11 pthree15)
))
 (:metric minimize (total-time))
)

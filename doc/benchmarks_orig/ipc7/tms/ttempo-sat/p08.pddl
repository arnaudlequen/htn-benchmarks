(define (problem pfile7)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 pone3 pone4 pone5 pone6 pone7 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 ptwo4 ptwo5 ptwo6 ptwo7 ptwo8 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 pthree5 pthree6 pthree7 pthree8 pthree9 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure pthree0 pone3)
     (baked-structure pthree4 pthree7)
     (baked-structure ptwo4 pthree8)
     (baked-structure pone5 ptwo3)
     (baked-structure ptwo8 ptwo0)
     (baked-structure pone1 pone2)
     (baked-structure pthree1 pone4)
     (baked-structure pone0 pthree5)
     (baked-structure ptwo7 ptwo6)
     (baked-structure ptwo2 ptwo5)
     (baked-structure pone7 pone6)
     (baked-structure pthree3 pthree9)
     (baked-structure pthree2 pthree6)
))
 (:metric minimize (total-time))
)

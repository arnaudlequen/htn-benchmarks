(define (problem pfile2)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure ptwo3 pthree2)
     (baked-structure pthree4 pthree0)
     (baked-structure pone2 pthree1)
     (baked-structure ptwo0 ptwo1)
     (baked-structure ptwo2 pthree3)
     (baked-structure pone1 pone0)
))
 (:metric minimize (total-time))
)

(define (problem pfile15)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 pone3 pone4 pone5 pone6 pone7 pone8 pone9 pone10 pone11 pone12 pone13 pone14 pone15 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 ptwo4 ptwo5 ptwo6 ptwo7 ptwo8 ptwo9 ptwo10 ptwo11 ptwo12 ptwo13 ptwo14 ptwo15 ptwo16 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 pthree5 pthree6 pthree7 pthree8 pthree9 pthree10 pthree11 pthree12 pthree13 pthree14 pthree15 pthree16 pthree17 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure ptwo2 pthree1)
     (baked-structure ptwo0 pthree9)
     (baked-structure ptwo10 pone13)
     (baked-structure pone0 pthree7)
     (baked-structure pone12 pone2)
     (baked-structure pthree0 pthree8)
     (baked-structure pone5 pone10)
     (baked-structure pone14 pone15)
     (baked-structure ptwo1 pthree10)
     (baked-structure pthree6 ptwo11)
     (baked-structure pthree17 ptwo12)
     (baked-structure ptwo7 pthree11)
     (baked-structure pthree13 ptwo6)
     (baked-structure pthree16 pone6)
     (baked-structure pone1 pthree3)
     (baked-structure pone11 pone8)
     (baked-structure ptwo3 pthree2)
     (baked-structure ptwo9 pthree15)
     (baked-structure ptwo8 pone7)
     (baked-structure pone4 pone9)
     (baked-structure pthree4 ptwo16)
     (baked-structure pthree5 ptwo5)
     (baked-structure ptwo14 ptwo15)
     (baked-structure pthree14 ptwo13)
     (baked-structure ptwo4 pone3)
))
 (:metric minimize (total-time))
)

(define (problem pfile11)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 pone3 pone4 pone5 pone6 pone7 pone8 pone9 pone10 pone11 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 ptwo4 ptwo5 ptwo6 ptwo7 ptwo8 ptwo9 ptwo10 ptwo11 ptwo12 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 pthree5 pthree6 pthree7 pthree8 pthree9 pthree10 pthree11 pthree12 pthree13 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure ptwo9 pthree9)
     (baked-structure pthree2 pthree3)
     (baked-structure pthree0 pone11)
     (baked-structure pone8 ptwo12)
     (baked-structure pthree8 pone0)
     (baked-structure ptwo7 ptwo10)
     (baked-structure pone1 pone3)
     (baked-structure pthree5 pthree7)
     (baked-structure ptwo8 pone5)
     (baked-structure ptwo11 ptwo5)
     (baked-structure ptwo3 pthree4)
     (baked-structure ptwo6 pone7)
     (baked-structure pthree10 pthree1)
     (baked-structure ptwo0 pone4)
     (baked-structure ptwo4 pone2)
     (baked-structure pone10 pone9)
     (baked-structure pthree12 ptwo1)
     (baked-structure pthree11 ptwo2)
     (baked-structure pone6 pthree6)
))
 (:metric minimize (total-time))
)

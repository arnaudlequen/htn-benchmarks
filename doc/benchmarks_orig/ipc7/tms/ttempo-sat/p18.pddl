(define (problem pfile17)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 pone3 pone4 pone5 pone6 pone7 pone8 pone9 pone10 pone11 pone12 pone13 pone14 pone15 pone16 pone17 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 ptwo4 ptwo5 ptwo6 ptwo7 ptwo8 ptwo9 ptwo10 ptwo11 ptwo12 ptwo13 ptwo14 ptwo15 ptwo16 ptwo17 ptwo18 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 pthree5 pthree6 pthree7 pthree8 pthree9 pthree10 pthree11 pthree12 pthree13 pthree14 pthree15 pthree16 pthree17 pthree18 pthree19 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure ptwo0 ptwo10)
     (baked-structure pthree2 pone8)
     (baked-structure ptwo14 pone14)
     (baked-structure pone1 pthree17)
     (baked-structure pthree8 pthree16)
     (baked-structure ptwo15 pthree10)
     (baked-structure pone5 pthree1)
     (baked-structure ptwo13 pone15)
     (baked-structure ptwo3 pone17)
     (baked-structure ptwo11 pthree11)
     (baked-structure pthree0 pone10)
     (baked-structure ptwo12 pone3)
     (baked-structure ptwo2 pthree4)
     (baked-structure pone6 ptwo9)
     (baked-structure pthree6 pone13)
     (baked-structure pthree15 pthree18)
     (baked-structure ptwo6 ptwo1)
     (baked-structure ptwo7 pone2)
     (baked-structure ptwo16 pone4)
     (baked-structure pone0 pthree12)
     (baked-structure pone7 pone11)
     (baked-structure pthree19 pthree13)
     (baked-structure pthree5 pthree14)
     (baked-structure ptwo17 pone12)
     (baked-structure ptwo18 ptwo4)
     (baked-structure pthree9 ptwo5)
     (baked-structure pthree7 pone9)
     (baked-structure pthree3 pone16)
))
 (:metric minimize (total-time))
)

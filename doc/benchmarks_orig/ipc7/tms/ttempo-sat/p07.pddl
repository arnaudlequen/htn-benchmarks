(define (problem pfile6)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 pone3 pone4 pone5 pone6 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 ptwo4 ptwo5 ptwo6 ptwo7 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 pthree5 pthree6 pthree7 pthree8 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure pthree5 ptwo2)
     (baked-structure pone6 ptwo6)
     (baked-structure ptwo7 pone1)
     (baked-structure ptwo1 pthree1)
     (baked-structure pone4 pthree7)
     (baked-structure pone2 pone3)
     (baked-structure ptwo4 pthree8)
     (baked-structure pthree3 pone0)
     (baked-structure pthree6 pthree0)
     (baked-structure pthree4 ptwo5)
     (baked-structure ptwo0 pone5)
     (baked-structure pthree2 ptwo3)
))
 (:metric minimize (total-time))
)

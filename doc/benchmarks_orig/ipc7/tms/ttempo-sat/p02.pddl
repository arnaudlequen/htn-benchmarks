(define (problem pfile1)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 - piecetype1
 ptwo0 ptwo1 ptwo2 - piecetype2
 pthree0 pthree1 pthree2 pthree3 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure ptwo0 ptwo1)
     (baked-structure pthree2 pthree3)
     (baked-structure ptwo2 pthree1)
     (baked-structure pone1 pthree0)
))
 (:metric minimize (total-time))
)

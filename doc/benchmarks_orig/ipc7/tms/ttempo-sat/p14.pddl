(define (problem pfile13)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 pone3 pone4 pone5 pone6 pone7 pone8 pone9 pone10 pone11 pone12 pone13 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 ptwo4 ptwo5 ptwo6 ptwo7 ptwo8 ptwo9 ptwo10 ptwo11 ptwo12 ptwo13 ptwo14 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 pthree5 pthree6 pthree7 pthree8 pthree9 pthree10 pthree11 pthree12 pthree13 pthree14 pthree15 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure ptwo7 ptwo14)
     (baked-structure ptwo3 pone11)
     (baked-structure pthree10 ptwo1)
     (baked-structure pthree5 pthree7)
     (baked-structure ptwo5 ptwo8)
     (baked-structure pone0 pone4)
     (baked-structure pone10 pone8)
     (baked-structure pthree6 ptwo0)
     (baked-structure pthree13 ptwo11)
     (baked-structure pthree8 ptwo13)
     (baked-structure pone13 pone2)
     (baked-structure ptwo12 pthree4)
     (baked-structure pone9 pthree11)
     (baked-structure pone3 pone6)
     (baked-structure pthree3 ptwo2)
     (baked-structure pone5 pthree0)
     (baked-structure ptwo10 pone1)
     (baked-structure pthree14 pthree15)
     (baked-structure ptwo6 pthree2)
     (baked-structure pone7 pthree9)
     (baked-structure pthree12 pone12)
     (baked-structure pthree1 ptwo9)
))
 (:metric minimize (total-time))
)

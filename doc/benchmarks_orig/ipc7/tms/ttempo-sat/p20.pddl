(define (problem pfile19)
 (:domain domain-tms-2-3-light)
 (:objects 
 kiln0 - kiln8
 kiln0 - kiln20
 pone0 pone1 pone2 pone3 pone4 pone5 pone6 pone7 pone8 pone9 pone10 pone11 pone12 pone13 pone14 pone15 pone16 pone17 pone18 pone19 - piecetype1
 ptwo0 ptwo1 ptwo2 ptwo3 ptwo4 ptwo5 ptwo6 ptwo7 ptwo8 ptwo9 ptwo10 ptwo11 ptwo12 ptwo13 ptwo14 ptwo15 ptwo16 ptwo17 ptwo18 ptwo19 ptwo20 - piecetype2
 pthree0 pthree1 pthree2 pthree3 pthree4 pthree5 pthree6 pthree7 pthree8 pthree9 pthree10 pthree11 pthree12 pthree13 pthree14 pthree15 pthree16 pthree17 pthree18 pthree19 pthree20 pthree21 - piecetype3
)
 (:init 
  (energy)
)
 (:goal
  (and
     (baked-structure ptwo10 pone4)
     (baked-structure ptwo15 pthree10)
     (baked-structure ptwo20 pone11)
     (baked-structure pone0 ptwo3)
     (baked-structure pthree4 ptwo14)
     (baked-structure ptwo5 pone5)
     (baked-structure pthree19 ptwo0)
     (baked-structure pthree8 pthree0)
     (baked-structure pthree16 ptwo18)
     (baked-structure pthree18 pone10)
     (baked-structure pone15 ptwo13)
     (baked-structure pthree6 pthree9)
     (baked-structure ptwo9 pthree13)
     (baked-structure pthree7 pthree11)
     (baked-structure pthree21 ptwo19)
     (baked-structure pone14 ptwo2)
     (baked-structure pone2 pone19)
     (baked-structure ptwo8 ptwo16)
     (baked-structure ptwo6 pone1)
     (baked-structure pone18 pthree5)
     (baked-structure pthree17 ptwo12)
     (baked-structure pone6 ptwo1)
     (baked-structure pthree14 ptwo17)
     (baked-structure pone9 ptwo4)
     (baked-structure pthree12 ptwo7)
     (baked-structure pthree3 pone8)
     (baked-structure pone12 pone16)
     (baked-structure pone3 pone13)
     (baked-structure pone17 pthree1)
     (baked-structure pthree15 ptwo11)
     (baked-structure pthree2 pthree20)
))
 (:metric minimize (total-time))
)

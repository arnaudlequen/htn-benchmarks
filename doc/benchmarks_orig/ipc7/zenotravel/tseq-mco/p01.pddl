(define (problem ZTRAVEL-1-1)
(:domain zeno-travel)
(:objects
	plane1 - aircraft
	person1 - person
	city0 - city
	city1 - city
	fl0 - fuel-amount
	fl1 - fuel-amount
	fl2 - fuel-amount
	fl3 - fuel-amount
	fl4 - fuel-amount
	fl5 - fuel-amount
	fl6 - fuel-amount
	)
(:init
	(at plane1 city0)
	(fuel-level plane1 fl0)
	(at person1 city0)
	(= (zoom-cost city0 city0) 91)
	(= (fly-cost city0 city0) 63)
	(= (zoom-cost city0 city1) 71)
	(= (fly-cost city0 city1) 14)
	(= (zoom-cost city1 city0) 60)
	(= (fly-cost city1 city0) 1)
	(= (zoom-cost city1 city1) 24)
	(= (fly-cost city1 city1) 13)
	(next fl0 fl1)
	(next fl1 fl2)
	(next fl2 fl3)
	(next fl3 fl4)
	(next fl4 fl5)
	(next fl5 fl6)
	(= (total-cost) 0)
)
(:goal (and
	(at person1 city1)
	))

(:metric minimize (total-cost))
)

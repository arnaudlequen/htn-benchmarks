(define (problem ZTRAVEL-2-2)
(:domain zeno-travel)
(:objects
	plane1 - aircraft
	plane2 - aircraft
	person1 - person
	person2 - person
	city0 - city
	city1 - city
	city2 - city
	fl0 - fuel-amount
	fl1 - fuel-amount
	fl2 - fuel-amount
	fl3 - fuel-amount
	fl4 - fuel-amount
	fl5 - fuel-amount
	fl6 - fuel-amount
	)
(:init
	(at plane1 city2)
	(fuel-level plane1 fl0)
	(at plane2 city2)
	(fuel-level plane2 fl0)
	(at person1 city1)
	(at person2 city2)
	(= (zoom-cost city0 city0) 83)
	(= (fly-cost city0 city0) 61)
	(= (zoom-cost city0 city1) 29)
	(= (fly-cost city0 city1) 63)
	(= (zoom-cost city0 city2) 52)
	(= (fly-cost city0 city2) 49)
	(= (zoom-cost city1 city0) 97)
	(= (fly-cost city1 city0) 29)
	(= (zoom-cost city1 city1) 77)
	(= (fly-cost city1 city1) 52)
	(= (zoom-cost city1 city2) 76)
	(= (fly-cost city1 city2) 40)
	(= (zoom-cost city2 city0) 89)
	(= (fly-cost city2 city0) 28)
	(= (zoom-cost city2 city1) 35)
	(= (fly-cost city2 city1) 80)
	(= (zoom-cost city2 city2) 91)
	(= (fly-cost city2 city2) 6)
	(next fl0 fl1)
	(next fl1 fl2)
	(next fl2 fl3)
	(next fl3 fl4)
	(next fl4 fl5)
	(next fl5 fl6)
	(= (total-cost) 0)
)
(:goal (and
	(at person1 city0)
	(at person2 city0)
	))

(:metric minimize (total-cost))
)

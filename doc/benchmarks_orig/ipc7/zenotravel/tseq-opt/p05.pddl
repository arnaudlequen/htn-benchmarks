(define (problem ZTRAVEL-5-5)
(:domain zeno-travel)
(:objects
	plane1 - aircraft
	plane2 - aircraft
	plane3 - aircraft
	plane4 - aircraft
	plane5 - aircraft
	person1 - person
	person2 - person
	person3 - person
	person4 - person
	person5 - person
	city0 - city
	city1 - city
	city2 - city
	city3 - city
	city4 - city
	city5 - city
	fl0 - fuel-amount
	fl1 - fuel-amount
	fl2 - fuel-amount
	fl3 - fuel-amount
	fl4 - fuel-amount
	fl5 - fuel-amount
	fl6 - fuel-amount
	)
(:init
	(at plane1 city5)
	(fuel-level plane1 fl0)
	(at plane2 city0)
	(fuel-level plane2 fl0)
	(at plane3 city5)
	(fuel-level plane3 fl0)
	(at plane4 city0)
	(fuel-level plane4 fl0)
	(at plane5 city0)
	(fuel-level plane5 fl0)
	(at person1 city3)
	(at person2 city5)
	(at person3 city1)
	(at person4 city2)
	(at person5 city2)
	(= (zoom-cost city0 city0) 33)
	(= (fly-cost city0 city0) 22)
	(= (zoom-cost city0 city1) 89)
	(= (fly-cost city0 city1) 35)
	(= (zoom-cost city0 city2) 68)
	(= (fly-cost city0 city2) 95)
	(= (zoom-cost city0 city3) 58)
	(= (fly-cost city0 city3) 65)
	(= (zoom-cost city0 city4) 85)
	(= (fly-cost city0 city4) 43)
	(= (zoom-cost city0 city5) 92)
	(= (fly-cost city0 city5) 39)
	(= (zoom-cost city1 city0) 81)
	(= (fly-cost city1 city0) 68)
	(= (zoom-cost city1 city1) 91)
	(= (fly-cost city1 city1) 48)
	(= (zoom-cost city1 city2) 21)
	(= (fly-cost city1 city2) 95)
	(= (zoom-cost city1 city3) 92)
	(= (fly-cost city1 city3) 14)
	(= (zoom-cost city1 city4) 88)
	(= (fly-cost city1 city4) 64)
	(= (zoom-cost city1 city5) 43)
	(= (fly-cost city1 city5) 61)
	(= (zoom-cost city2 city0) 28)
	(= (fly-cost city2 city0) 78)
	(= (zoom-cost city2 city1) 30)
	(= (fly-cost city2 city1) 44)
	(= (zoom-cost city2 city2) 22)
	(= (fly-cost city2 city2) 18)
	(= (zoom-cost city2 city3) 27)
	(= (fly-cost city2 city3) 55)
	(= (zoom-cost city2 city4) 41)
	(= (fly-cost city2 city4) 16)
	(= (zoom-cost city2 city5) 90)
	(= (fly-cost city2 city5) 10)
	(= (zoom-cost city3 city0) 12)
	(= (fly-cost city3 city0) 49)
	(= (zoom-cost city3 city1) 76)
	(= (fly-cost city3 city1) 98)
	(= (zoom-cost city3 city2) 93)
	(= (fly-cost city3 city2) 68)
	(= (zoom-cost city3 city3) 38)
	(= (fly-cost city3 city3) 74)
	(= (zoom-cost city3 city4) 36)
	(= (fly-cost city3 city4) 29)
	(= (zoom-cost city3 city5) 23)
	(= (fly-cost city3 city5) 58)
	(= (zoom-cost city4 city0) 24)
	(= (fly-cost city4 city0) 15)
	(= (zoom-cost city4 city1) 73)
	(= (fly-cost city4 city1) 12)
	(= (zoom-cost city4 city2) 79)
	(= (fly-cost city4 city2) 16)
	(= (zoom-cost city4 city3) 74)
	(= (fly-cost city4 city3) 7)
	(= (zoom-cost city4 city4) 95)
	(= (fly-cost city4 city4) 5)
	(= (zoom-cost city4 city5) 52)
	(= (fly-cost city4 city5) 17)
	(= (zoom-cost city5 city0) 24)
	(= (fly-cost city5 city0) 79)
	(= (zoom-cost city5 city1) 73)
	(= (fly-cost city5 city1) 65)
	(= (zoom-cost city5 city2) 96)
	(= (fly-cost city5 city2) 63)
	(= (zoom-cost city5 city3) 75)
	(= (fly-cost city5 city3) 9)
	(= (zoom-cost city5 city4) 13)
	(= (fly-cost city5 city4) 52)
	(= (zoom-cost city5 city5) 7)
	(= (fly-cost city5 city5) 6)
	(next fl0 fl1)
	(next fl1 fl2)
	(next fl2 fl3)
	(next fl3 fl4)
	(next fl4 fl5)
	(next fl5 fl6)
	(= (total-cost) 0)
)
(:goal (and
	(at plane3 city1)
	(at person1 city0)
	(at person2 city5)
	(at person3 city4)
	(at person4 city4)
	(at person5 city5)
	))

(:metric minimize (total-cost))
)

(define (problem ZTRAVEL-4-4)
(:domain zeno-travel)
(:objects
	plane1 - aircraft
	plane2 - aircraft
	plane3 - aircraft
	plane4 - aircraft
	person1 - person
	person2 - person
	person3 - person
	person4 - person
	city0 - city
	city1 - city
	city2 - city
	city3 - city
	city4 - city
	fl0 - fuel-amount
	fl1 - fuel-amount
	fl2 - fuel-amount
	fl3 - fuel-amount
	fl4 - fuel-amount
	fl5 - fuel-amount
	fl6 - fuel-amount
	)
(:init
	(at plane1 city2)
	(fuel-level plane1 fl0)
	(at plane2 city1)
	(fuel-level plane2 fl0)
	(at plane3 city3)
	(fuel-level plane3 fl0)
	(at plane4 city4)
	(fuel-level plane4 fl0)
	(at person1 city4)
	(at person2 city0)
	(at person3 city1)
	(at person4 city4)
	(= (zoom-cost city0 city0) 37)
	(= (fly-cost city0 city0) 76)
	(= (zoom-cost city0 city1) 51)
	(= (fly-cost city0 city1) 66)
	(= (zoom-cost city0 city2) 53)
	(= (fly-cost city0 city2) 3)
	(= (zoom-cost city0 city3) 43)
	(= (fly-cost city0 city3) 93)
	(= (zoom-cost city0 city4) 93)
	(= (fly-cost city0 city4) 72)
	(= (zoom-cost city1 city0) 28)
	(= (fly-cost city1 city0) 73)
	(= (zoom-cost city1 city1) 63)
	(= (fly-cost city1 city1) 35)
	(= (zoom-cost city1 city2) 68)
	(= (fly-cost city1 city2) 16)
	(= (zoom-cost city1 city3) 44)
	(= (fly-cost city1 city3) 88)
	(= (zoom-cost city1 city4) 82)
	(= (fly-cost city1 city4) 33)
	(= (zoom-cost city2 city0) 22)
	(= (fly-cost city2 city0) 89)
	(= (zoom-cost city2 city1) 35)
	(= (fly-cost city2 city1) 68)
	(= (zoom-cost city2 city2) 95)
	(= (fly-cost city2 city2) 58)
	(= (zoom-cost city2 city3) 65)
	(= (fly-cost city2 city3) 85)
	(= (zoom-cost city2 city4) 43)
	(= (fly-cost city2 city4) 92)
	(= (zoom-cost city3 city0) 39)
	(= (fly-cost city3 city0) 81)
	(= (zoom-cost city3 city1) 68)
	(= (fly-cost city3 city1) 91)
	(= (zoom-cost city3 city2) 48)
	(= (fly-cost city3 city2) 21)
	(= (zoom-cost city3 city3) 95)
	(= (fly-cost city3 city3) 92)
	(= (zoom-cost city3 city4) 14)
	(= (fly-cost city3 city4) 88)
	(= (zoom-cost city4 city0) 64)
	(= (fly-cost city4 city0) 43)
	(= (zoom-cost city4 city1) 61)
	(= (fly-cost city4 city1) 28)
	(= (zoom-cost city4 city2) 78)
	(= (fly-cost city4 city2) 30)
	(= (zoom-cost city4 city3) 44)
	(= (fly-cost city4 city3) 22)
	(= (zoom-cost city4 city4) 18)
	(= (fly-cost city4 city4) 27)
	(next fl0 fl1)
	(next fl1 fl2)
	(next fl2 fl3)
	(next fl3 fl4)
	(next fl4 fl5)
	(next fl5 fl6)
	(= (total-cost) 0)
)
(:goal (and
	(at plane2 city0)
	(at person1 city1)
	(at person2 city2)
	(at person3 city4)
	(at person4 city1)
	))

(:metric minimize (total-cost))
)

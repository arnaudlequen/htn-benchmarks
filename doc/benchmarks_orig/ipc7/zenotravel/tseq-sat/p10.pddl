(define (problem ZTRAVEL-10-10)
(:domain zeno-travel)
(:objects
	plane1 - aircraft
	plane2 - aircraft
	plane3 - aircraft
	plane4 - aircraft
	plane5 - aircraft
	plane6 - aircraft
	plane7 - aircraft
	plane8 - aircraft
	plane9 - aircraft
	plane10 - aircraft
	person1 - person
	person2 - person
	person3 - person
	person4 - person
	person5 - person
	person6 - person
	person7 - person
	person8 - person
	person9 - person
	person10 - person
	city0 - city
	city1 - city
	city2 - city
	city3 - city
	city4 - city
	city5 - city
	city6 - city
	city7 - city
	city8 - city
	city9 - city
	city10 - city
	fl0 - fuel-amount
	fl1 - fuel-amount
	fl2 - fuel-amount
	fl3 - fuel-amount
	fl4 - fuel-amount
	fl5 - fuel-amount
	fl6 - fuel-amount
	)
(:init
	(at plane1 city3)
	(fuel-level plane1 fl0)
	(at plane2 city4)
	(fuel-level plane2 fl0)
	(at plane3 city8)
	(fuel-level plane3 fl0)
	(at plane4 city3)
	(fuel-level plane4 fl0)
	(at plane5 city10)
	(fuel-level plane5 fl0)
	(at plane6 city8)
	(fuel-level plane6 fl0)
	(at plane7 city1)
	(fuel-level plane7 fl0)
	(at plane8 city6)
	(fuel-level plane8 fl0)
	(at plane9 city5)
	(fuel-level plane9 fl0)
	(at plane10 city5)
	(fuel-level plane10 fl0)
	(at person1 city2)
	(at person2 city10)
	(at person3 city0)
	(at person4 city6)
	(at person5 city4)
	(at person6 city3)
	(at person7 city4)
	(at person8 city5)
	(at person9 city10)
	(at person10 city8)
	(= (zoom-cost city0 city0) 74)
	(= (fly-cost city0 city0) 83)
	(= (zoom-cost city0 city1) 92)
	(= (fly-cost city0 city1) 87)
	(= (zoom-cost city0 city2) 83)
	(= (fly-cost city0 city2) 97)
	(= (zoom-cost city0 city3) 74)
	(= (fly-cost city0 city3) 90)
	(= (zoom-cost city0 city4) 98)
	(= (fly-cost city0 city4) 66)
	(= (zoom-cost city0 city5) 49)
	(= (fly-cost city0 city5) 16)
	(= (zoom-cost city0 city6) 83)
	(= (fly-cost city0 city6) 88)
	(= (zoom-cost city0 city7) 7)
	(= (fly-cost city0 city7) 64)
	(= (zoom-cost city0 city8) 24)
	(= (fly-cost city0 city8) 62)
	(= (zoom-cost city0 city9) 22)
	(= (fly-cost city0 city9) 70)
	(= (zoom-cost city0 city10) 31)
	(= (fly-cost city0 city10) 32)
	(= (zoom-cost city1 city0) 23)
	(= (fly-cost city1 city0) 7)
	(= (zoom-cost city1 city1) 63)
	(= (fly-cost city1 city1) 22)
	(= (zoom-cost city1 city2) 65)
	(= (fly-cost city1 city2) 51)
	(= (zoom-cost city1 city3) 97)
	(= (fly-cost city1 city3) 28)
	(= (zoom-cost city1 city4) 54)
	(= (fly-cost city1 city4) 71)
	(= (zoom-cost city1 city5) 11)
	(= (fly-cost city1 city5) 47)
	(= (zoom-cost city1 city6) 59)
	(= (fly-cost city1 city6) 94)
	(= (zoom-cost city1 city7) 45)
	(= (fly-cost city1 city7) 33)
	(= (zoom-cost city1 city8) 84)
	(= (fly-cost city1 city8) 43)
	(= (zoom-cost city1 city9) 0)
	(= (fly-cost city1 city9) 34)
	(= (zoom-cost city1 city10) 59)
	(= (fly-cost city1 city10) 83)
	(= (zoom-cost city2 city0) 23)
	(= (fly-cost city2 city0) 67)
	(= (zoom-cost city2 city1) 48)
	(= (fly-cost city2 city1) 48)
	(= (zoom-cost city2 city2) 30)
	(= (fly-cost city2 city2) 71)
	(= (zoom-cost city2 city3) 18)
	(= (fly-cost city2 city3) 62)
	(= (zoom-cost city2 city4) 4)
	(= (fly-cost city2 city4) 41)
	(= (zoom-cost city2 city5) 69)
	(= (fly-cost city2 city5) 67)
	(= (zoom-cost city2 city6) 63)
	(= (fly-cost city2 city6) 34)
	(= (zoom-cost city2 city7) 18)
	(= (fly-cost city2 city7) 60)
	(= (zoom-cost city2 city8) 62)
	(= (fly-cost city2 city8) 73)
	(= (zoom-cost city2 city9) 32)
	(= (fly-cost city2 city9) 74)
	(= (zoom-cost city2 city10) 20)
	(= (fly-cost city2 city10) 92)
	(= (zoom-cost city3 city0) 68)
	(= (fly-cost city3 city0) 65)
	(= (zoom-cost city3 city1) 25)
	(= (fly-cost city3 city1) 53)
	(= (zoom-cost city3 city2) 8)
	(= (fly-cost city3 city2) 26)
	(= (zoom-cost city3 city3) 87)
	(= (fly-cost city3 city3) 68)
	(= (zoom-cost city3 city4) 9)
	(= (fly-cost city3 city4) 11)
	(= (zoom-cost city3 city5) 36)
	(= (fly-cost city3 city5) 57)
	(= (zoom-cost city3 city6) 59)
	(= (fly-cost city3 city6) 66)
	(= (zoom-cost city3 city7) 28)
	(= (fly-cost city3 city7) 77)
	(= (zoom-cost city3 city8) 28)
	(= (fly-cost city3 city8) 32)
	(= (zoom-cost city3 city9) 18)
	(= (fly-cost city3 city9) 98)
	(= (zoom-cost city3 city10) 0)
	(= (fly-cost city3 city10) 82)
	(= (zoom-cost city4 city0) 33)
	(= (fly-cost city4 city0) 18)
	(= (zoom-cost city4 city1) 43)
	(= (fly-cost city4 city1) 95)
	(= (zoom-cost city4 city2) 91)
	(= (fly-cost city4 city2) 76)
	(= (zoom-cost city4 city3) 69)
	(= (fly-cost city4 city3) 12)
	(= (zoom-cost city4 city4) 68)
	(= (fly-cost city4 city4) 38)
	(= (zoom-cost city4 city5) 77)
	(= (fly-cost city4 city5) 94)
	(= (zoom-cost city4 city6) 91)
	(= (fly-cost city4 city6) 86)
	(= (zoom-cost city4 city7) 20)
	(= (fly-cost city4 city7) 79)
	(= (zoom-cost city4 city8) 54)
	(= (fly-cost city4 city8) 29)
	(= (zoom-cost city4 city9) 90)
	(= (fly-cost city4 city9) 90)
	(= (zoom-cost city4 city10) 87)
	(= (fly-cost city4 city10) 49)
	(= (zoom-cost city5 city0) 57)
	(= (fly-cost city5 city0) 16)
	(= (zoom-cost city5 city1) 27)
	(= (fly-cost city5 city1) 86)
	(= (zoom-cost city5 city2) 49)
	(= (fly-cost city5 city2) 46)
	(= (zoom-cost city5 city3) 84)
	(= (fly-cost city5 city3) 49)
	(= (zoom-cost city5 city4) 29)
	(= (fly-cost city5 city4) 18)
	(= (zoom-cost city5 city5) 68)
	(= (fly-cost city5 city5) 72)
	(= (zoom-cost city5 city6) 13)
	(= (fly-cost city5 city6) 60)
	(= (zoom-cost city5 city7) 49)
	(= (fly-cost city5 city7) 83)
	(= (zoom-cost city5 city8) 72)
	(= (fly-cost city5 city8) 17)
	(= (zoom-cost city5 city9) 22)
	(= (fly-cost city5 city9) 49)
	(= (zoom-cost city5 city10) 12)
	(= (fly-cost city5 city10) 13)
	(= (zoom-cost city6 city0) 36)
	(= (fly-cost city6 city0) 32)
	(= (zoom-cost city6 city1) 93)
	(= (fly-cost city6 city1) 90)
	(= (zoom-cost city6 city2) 62)
	(= (fly-cost city6 city2) 83)
	(= (zoom-cost city6 city3) 81)
	(= (fly-cost city6 city3) 49)
	(= (zoom-cost city6 city4) 33)
	(= (fly-cost city6 city4) 39)
	(= (zoom-cost city6 city5) 65)
	(= (fly-cost city6 city5) 60)
	(= (zoom-cost city6 city6) 25)
	(= (fly-cost city6 city6) 15)
	(= (zoom-cost city6 city7) 7)
	(= (fly-cost city6 city7) 10)
	(= (zoom-cost city6 city8) 64)
	(= (fly-cost city6 city8) 36)
	(= (zoom-cost city6 city9) 28)
	(= (fly-cost city6 city9) 33)
	(= (zoom-cost city6 city10) 9)
	(= (fly-cost city6 city10) 42)
	(= (zoom-cost city7 city0) 93)
	(= (fly-cost city7 city0) 58)
	(= (zoom-cost city7 city1) 26)
	(= (fly-cost city7 city1) 65)
	(= (zoom-cost city7 city2) 76)
	(= (fly-cost city7 city2) 48)
	(= (zoom-cost city7 city3) 15)
	(= (fly-cost city7 city3) 88)
	(= (zoom-cost city7 city4) 62)
	(= (fly-cost city7 city4) 51)
	(= (zoom-cost city7 city5) 20)
	(= (fly-cost city7 city5) 55)
	(= (zoom-cost city7 city6) 42)
	(= (fly-cost city7 city6) 82)
	(= (zoom-cost city7 city7) 39)
	(= (fly-cost city7 city7) 24)
	(= (zoom-cost city7 city8) 32)
	(= (fly-cost city7 city8) 72)
	(= (zoom-cost city7 city9) 63)
	(= (fly-cost city7 city9) 98)
	(= (zoom-cost city7 city10) 33)
	(= (fly-cost city7 city10) 89)
	(= (zoom-cost city8 city0) 13)
	(= (fly-cost city8 city0) 41)
	(= (zoom-cost city8 city1) 0)
	(= (fly-cost city8 city1) 78)
	(= (zoom-cost city8 city2) 77)
	(= (fly-cost city8 city2) 29)
	(= (zoom-cost city8 city3) 11)
	(= (fly-cost city8 city3) 86)
	(= (zoom-cost city8 city4) 72)
	(= (fly-cost city8 city4) 4)
	(= (zoom-cost city8 city5) 44)
	(= (fly-cost city8 city5) 98)
	(= (zoom-cost city8 city6) 70)
	(= (fly-cost city8 city6) 21)
	(= (zoom-cost city8 city7) 47)
	(= (fly-cost city8 city7) 86)
	(= (zoom-cost city8 city8) 9)
	(= (fly-cost city8 city8) 9)
	(= (zoom-cost city8 city9) 38)
	(= (fly-cost city8 city9) 30)
	(= (zoom-cost city8 city10) 65)
	(= (fly-cost city8 city10) 80)
	(= (zoom-cost city9 city0) 13)
	(= (fly-cost city9 city0) 5)
	(= (zoom-cost city9 city1) 5)
	(= (fly-cost city9 city1) 45)
	(= (zoom-cost city9 city2) 78)
	(= (fly-cost city9 city2) 69)
	(= (zoom-cost city9 city3) 44)
	(= (fly-cost city9 city3) 11)
	(= (zoom-cost city9 city4) 58)
	(= (fly-cost city9 city4) 57)
	(= (zoom-cost city9 city5) 52)
	(= (fly-cost city9 city5) 59)
	(= (zoom-cost city9 city6) 36)
	(= (fly-cost city9 city6) 30)
	(= (zoom-cost city9 city7) 88)
	(= (fly-cost city9 city7) 47)
	(= (zoom-cost city9 city8) 16)
	(= (fly-cost city9 city8) 60)
	(= (zoom-cost city9 city9) 52)
	(= (fly-cost city9 city9) 61)
	(= (zoom-cost city9 city10) 59)
	(= (fly-cost city9 city10) 23)
	(= (zoom-cost city10 city0) 82)
	(= (fly-cost city10 city0) 7)
	(= (zoom-cost city10 city1) 9)
	(= (fly-cost city10 city1) 92)
	(= (zoom-cost city10 city2) 16)
	(= (fly-cost city10 city2) 48)
	(= (zoom-cost city10 city3) 22)
	(= (fly-cost city10 city3) 82)
	(= (zoom-cost city10 city4) 29)
	(= (fly-cost city10 city4) 35)
	(= (zoom-cost city10 city5) 87)
	(= (fly-cost city10 city5) 34)
	(= (zoom-cost city10 city6) 81)
	(= (fly-cost city10 city6) 65)
	(= (zoom-cost city10 city7) 3)
	(= (fly-cost city10 city7) 25)
	(= (zoom-cost city10 city8) 77)
	(= (fly-cost city10 city8) 62)
	(= (zoom-cost city10 city9) 83)
	(= (fly-cost city10 city9) 30)
	(= (zoom-cost city10 city10) 22)
	(= (fly-cost city10 city10) 19)
	(next fl0 fl1)
	(next fl1 fl2)
	(next fl2 fl3)
	(next fl3 fl4)
	(next fl4 fl5)
	(next fl5 fl6)
	(= (total-cost) 0)
)
(:goal (and
	(at plane3 city7)
	(at plane10 city0)
	(at person1 city9)
	(at person2 city0)
	(at person3 city0)
	(at person4 city1)
	(at person5 city10)
	(at person6 city6)
	(at person7 city7)
	(at person8 city8)
	(at person9 city6)
	(at person10 city6)
	))

(:metric minimize (total-cost))
)

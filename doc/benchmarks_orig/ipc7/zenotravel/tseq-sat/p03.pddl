(define (problem ZTRAVEL-3-3)
(:domain zeno-travel)
(:objects
	plane1 - aircraft
	plane2 - aircraft
	plane3 - aircraft
	person1 - person
	person2 - person
	person3 - person
	city0 - city
	city1 - city
	city2 - city
	city3 - city
	fl0 - fuel-amount
	fl1 - fuel-amount
	fl2 - fuel-amount
	fl3 - fuel-amount
	fl4 - fuel-amount
	fl5 - fuel-amount
	fl6 - fuel-amount
	)
(:init
	(at plane1 city1)
	(fuel-level plane1 fl0)
	(at plane2 city2)
	(fuel-level plane2 fl0)
	(at plane3 city3)
	(fuel-level plane3 fl0)
	(at person1 city3)
	(at person2 city1)
	(at person3 city1)
	(= (zoom-cost city0 city0) 6)
	(= (fly-cost city0 city0) 94)
	(= (zoom-cost city0 city1) 52)
	(= (fly-cost city0 city1) 8)
	(= (zoom-cost city0 city2) 19)
	(= (fly-cost city0 city2) 66)
	(= (zoom-cost city0 city3) 89)
	(= (fly-cost city0 city3) 34)
	(= (zoom-cost city1 city0) 6)
	(= (fly-cost city1 city0) 2)
	(= (zoom-cost city1 city1) 45)
	(= (fly-cost city1 city1) 6)
	(= (zoom-cost city1 city2) 23)
	(= (fly-cost city1 city2) 97)
	(= (zoom-cost city1 city3) 90)
	(= (fly-cost city1 city3) 85)
	(= (zoom-cost city2 city0) 26)
	(= (fly-cost city2 city0) 53)
	(= (zoom-cost city2 city1) 37)
	(= (fly-cost city2 city1) 76)
	(= (zoom-cost city2 city2) 51)
	(= (fly-cost city2 city2) 66)
	(= (zoom-cost city2 city3) 53)
	(= (fly-cost city2 city3) 3)
	(= (zoom-cost city3 city0) 43)
	(= (fly-cost city3 city0) 93)
	(= (zoom-cost city3 city1) 93)
	(= (fly-cost city3 city1) 72)
	(= (zoom-cost city3 city2) 28)
	(= (fly-cost city3 city2) 73)
	(= (zoom-cost city3 city3) 63)
	(= (fly-cost city3 city3) 35)
	(next fl0 fl1)
	(next fl1 fl2)
	(next fl2 fl3)
	(next fl3 fl4)
	(next fl4 fl5)
	(next fl5 fl6)
	(= (total-cost) 0)
)
(:goal (and
	(at person1 city2)
	(at person2 city3)
	(at person3 city3)
	))

(:metric minimize (total-cost))
)

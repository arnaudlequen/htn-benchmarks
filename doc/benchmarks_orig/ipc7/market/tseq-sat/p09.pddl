(define (problem trader-9-3-5-4-20-100-120-0)
(:domain Trader)
(:objects
    Quito Bamako Accra - market
    batteries sugar phones oats tea - goods
    salesman - trader
)

(:init

    ; Defining what is on sale, and prices, for city Quito
    (on-sale oats Quito)
    (= (price oats Quito) 30.32)
    (on-sale sugar Quito)
    (= (price sugar Quito) 45.33)
    (on-sale phones Quito)
    (= (price phones Quito) 176.64)
    (on-sale batteries Quito)
    (= (price batteries Quito) 76.19)

    ; Defining what is on sale, and prices, for city Bamako
    (on-sale tea Bamako)
    (= (price tea Bamako) 44.9)
    (on-sale batteries Bamako)
    (= (price batteries Bamako) 106.6)
    (on-sale sugar Bamako)
    (= (price sugar Bamako) 44.15)
    (on-sale phones Bamako)
    (= (price phones Bamako) 162.75)

    ; Defining what is on sale, and prices, for city Accra
    (on-sale sugar Accra)
    (= (price sugar Accra) 44.35)
    (on-sale phones Accra)
    (= (price phones Accra) 163.07)
    (on-sale oats Accra)
    (= (price oats Accra) 29.46)
    (on-sale batteries Accra)
    (= (price batteries Accra) 77.52)

    ; Defining links between cities
    (can-drive Bamako Quito)
    (can-drive Quito Bamako)
    (= (drive-cost Bamako Quito) 2.3)
    (= (drive-cost Quito Bamako) 2.3)
    (can-drive Accra Quito)
    (can-drive Quito Accra)
    (= (drive-cost Accra Quito) 4.15)
    (= (drive-cost Quito Accra) 4.15)
    (can-drive Accra Bamako)
    (can-drive Bamako Accra)
    (= (drive-cost Accra Bamako) 1.85)
    (= (drive-cost Bamako Accra) 1.85)

    ; Defining initial state of salesman
    (at salesman Accra)
    (= (capacity) 20)
    (= (money) 100)
    (= (bought batteries) 0)
    (= (bought sugar) 0)
    (= (bought phones) 0)
    (= (bought oats) 0)
    (= (bought tea) 0)

)
(:goal
    (and
        (>= (money) 120)
    )
)
)
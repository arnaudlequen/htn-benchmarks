(define (problem trader-1-3-5-4-20-100-120-0)
(:domain Trader)
(:objects
    Sucre Kingstown Ankara - market
    batteries salt tea rice wine - goods
    salesman - trader
)

(:init

    ; Defining what is on sale, and prices, for city Sucre
    (on-sale rice Sucre)
    (= (price rice Sucre) 39.78)
    (on-sale tea Sucre)
    (= (price tea Sucre) 55.66)
    (on-sale wine Sucre)
    (= (price wine Sucre) 88.46)
    (on-sale batteries Sucre)
    (= (price batteries Sucre) 106.25)

    ; Defining what is on sale, and prices, for city Kingstown
    (on-sale rice Kingstown)
    (= (price rice Kingstown) 39.23)
    (on-sale tea Kingstown)
    (= (price tea Kingstown) 59.66)
    (on-sale wine Kingstown)
    (= (price wine Kingstown) 115.93)
    (on-sale salt Kingstown)
    (= (price salt Kingstown) 35.08)

    ; Defining what is on sale, and prices, for city Ankara
    (on-sale wine Ankara)
    (= (price wine Ankara) 110.12)
    (on-sale rice Ankara)
    (= (price rice Ankara) 31.09)
    (on-sale batteries Ankara)
    (= (price batteries Ankara) 104.82)
    (on-sale tea Ankara)
    (= (price tea Ankara) 56.57)

    ; Defining links between cities
    (can-drive Kingstown Sucre)
    (can-drive Sucre Kingstown)
    (= (drive-cost Kingstown Sucre) 5.71)
    (= (drive-cost Sucre Kingstown) 5.71)
    (can-drive Ankara Sucre)
    (can-drive Sucre Ankara)
    (= (drive-cost Ankara Sucre) 5.96)
    (= (drive-cost Sucre Ankara) 5.96)
    (can-drive Ankara Kingstown)
    (can-drive Kingstown Ankara)
    (= (drive-cost Ankara Kingstown) 5.18)
    (= (drive-cost Kingstown Ankara) 5.18)

    ; Defining initial state of salesman
    (at salesman Ankara)
    (= (capacity) 20)
    (= (money) 100)
    (= (bought batteries) 0)
    (= (bought salt) 0)
    (= (bought tea) 0)
    (= (bought rice) 0)
    (= (bought wine) 0)

)
(:goal
    (and
        (>= (money) 120)
    )
)
)
(define (problem trader-8-3-5-4-20-100-120-0)
(:domain Trader)
(:objects
    Abuja Islamabad Belgrade - market
    olive-oil computers oats potatoes rice - goods
    salesman - trader
)

(:init

    ; Defining what is on sale, and prices, for city Abuja
    (on-sale rice Abuja)
    (= (price rice Abuja) 28.99)
    (on-sale olive-oil Abuja)
    (= (price olive-oil Abuja) 64.53)
    (on-sale potatoes Abuja)
    (= (price potatoes Abuja) 28.73)
    (on-sale oats Abuja)
    (= (price oats Abuja) 29.94)

    ; Defining what is on sale, and prices, for city Islamabad
    (on-sale potatoes Islamabad)
    (= (price potatoes Islamabad) 41.71)
    (on-sale rice Islamabad)
    (= (price rice Islamabad) 40.18)
    (on-sale olive-oil Islamabad)
    (= (price olive-oil Islamabad) 92.86)
    (on-sale computers Islamabad)
    (= (price computers Islamabad) 222.71)

    ; Defining what is on sale, and prices, for city Belgrade
    (on-sale olive-oil Belgrade)
    (= (price olive-oil Belgrade) 89.62)
    (on-sale oats Belgrade)
    (= (price oats Belgrade) 28.85)
    (on-sale rice Belgrade)
    (= (price rice Belgrade) 31.18)
    (on-sale potatoes Belgrade)
    (= (price potatoes Belgrade) 29.74)

    ; Defining links between cities
    (can-drive Islamabad Abuja)
    (can-drive Abuja Islamabad)
    (= (drive-cost Islamabad Abuja) 2.39)
    (= (drive-cost Abuja Islamabad) 2.39)
    (can-drive Belgrade Abuja)
    (can-drive Abuja Belgrade)
    (= (drive-cost Belgrade Abuja) 3.75)
    (= (drive-cost Abuja Belgrade) 3.75)
    (can-drive Belgrade Islamabad)
    (can-drive Islamabad Belgrade)
    (= (drive-cost Belgrade Islamabad) 3.26)
    (= (drive-cost Islamabad Belgrade) 3.26)

    ; Defining initial state of salesman
    (at salesman Islamabad)
    (= (capacity) 20)
    (= (money) 100)
    (= (bought olive-oil) 0)
    (= (bought computers) 0)
    (= (bought oats) 0)
    (= (bought potatoes) 0)
    (= (bought rice) 0)

)
(:goal
    (and
        (>= (money) 120)
    )
)
)
(define (problem trader-5-3-5-4-20-100-120-0)
(:domain Trader)
(:objects
    Sanaa Dili Malabo - market
    potatoes sugar medicine oats salt - goods
    salesman - trader
)

(:init

    ; Defining what is on sale, and prices, for city Sanaa
    (on-sale sugar Sanaa)
    (= (price sugar Sanaa) 32.59)
    (on-sale medicine Sanaa)
    (= (price medicine Sanaa) 175.99)
    (on-sale potatoes Sanaa)
    (= (price potatoes Sanaa) 29.18)
    (on-sale salt Sanaa)
    (= (price salt Sanaa) 33.51)

    ; Defining what is on sale, and prices, for city Dili
    (on-sale sugar Dili)
    (= (price sugar Dili) 33.54)
    (on-sale medicine Dili)
    (= (price medicine Dili) 129.16)
    (on-sale salt Dili)
    (= (price salt Dili) 26.05)
    (on-sale potatoes Dili)
    (= (price potatoes Dili) 41.22)

    ; Defining what is on sale, and prices, for city Malabo
    (on-sale oats Malabo)
    (= (price oats Malabo) 40.42)
    (on-sale medicine Malabo)
    (= (price medicine Malabo) 172.22)
    (on-sale salt Malabo)
    (= (price salt Malabo) 34.84)
    (on-sale sugar Malabo)
    (= (price sugar Malabo) 47.4)

    ; Defining links between cities
    (can-drive Dili Sanaa)
    (can-drive Sanaa Dili)
    (= (drive-cost Dili Sanaa) 5.63)
    (= (drive-cost Sanaa Dili) 5.63)
    (can-drive Malabo Sanaa)
    (can-drive Sanaa Malabo)
    (= (drive-cost Malabo Sanaa) 6.55)
    (= (drive-cost Sanaa Malabo) 6.55)
    (can-drive Malabo Dili)
    (can-drive Dili Malabo)
    (= (drive-cost Malabo Dili) 1.83)
    (= (drive-cost Dili Malabo) 1.83)

    ; Defining initial state of salesman
    (at salesman Malabo)
    (= (capacity) 20)
    (= (money) 100)
    (= (bought potatoes) 0)
    (= (bought sugar) 0)
    (= (bought medicine) 0)
    (= (bought oats) 0)
    (= (bought salt) 0)

)
(:goal
    (and
        (>= (money) 120)
    )
)
)
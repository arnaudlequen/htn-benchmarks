(define (problem trader-3-3-5-4-20-100-120-0)
(:domain Trader)
(:objects
    Edinburgh Tashkent Saipan - market
    salt sugar potatoes rice olive-oil - goods
    salesman - trader
)

(:init

    ; Defining what is on sale, and prices, for city Edinburgh
    (on-sale potatoes Edinburgh)
    (= (price potatoes Edinburgh) 40.63)
    (on-sale salt Edinburgh)
    (= (price salt Edinburgh) 24.2)
    (on-sale sugar Edinburgh)
    (= (price sugar Edinburgh) 47.62)
    (on-sale rice Edinburgh)
    (= (price rice Edinburgh) 41.95)

    ; Defining what is on sale, and prices, for city Tashkent
    (on-sale olive-oil Tashkent)
    (= (price olive-oil Tashkent) 68.69)
    (on-sale rice Tashkent)
    (= (price rice Tashkent) 40.81)
    (on-sale potatoes Tashkent)
    (= (price potatoes Tashkent) 29.9)
    (on-sale salt Tashkent)
    (= (price salt Tashkent) 26.28)

    ; Defining what is on sale, and prices, for city Saipan
    (on-sale sugar Saipan)
    (= (price sugar Saipan) 47.12)
    (on-sale olive-oil Saipan)
    (= (price olive-oil Saipan) 69.6)
    (on-sale salt Saipan)
    (= (price salt Saipan) 34.89)
    (on-sale rice Saipan)
    (= (price rice Saipan) 31.21)

    ; Defining links between cities
    (can-drive Tashkent Edinburgh)
    (can-drive Edinburgh Tashkent)
    (= (drive-cost Tashkent Edinburgh) 5.63)
    (= (drive-cost Edinburgh Tashkent) 5.63)
    (can-drive Saipan Edinburgh)
    (can-drive Edinburgh Saipan)
    (= (drive-cost Saipan Edinburgh) 1.09)
    (= (drive-cost Edinburgh Saipan) 1.09)
    (can-drive Saipan Tashkent)
    (can-drive Tashkent Saipan)
    (= (drive-cost Saipan Tashkent) 4.64)
    (= (drive-cost Tashkent Saipan) 4.64)

    ; Defining initial state of salesman
    (at salesman Edinburgh)
    (= (capacity) 20)
    (= (money) 100)
    (= (bought salt) 0)
    (= (bought sugar) 0)
    (= (bought potatoes) 0)
    (= (bought rice) 0)
    (= (bought olive-oil) 0)

)
(:goal
    (and
        (>= (money) 120)
    )
)
)
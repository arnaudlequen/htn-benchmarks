(define (problem trader-10-3-5-4-20-100-120-0)
(:domain Trader)
(:objects
    Lima Sanaa Tehran - market
    rice carrots batteries flour oats - goods
    salesman - trader
)

(:init

    ; Defining what is on sale, and prices, for city Lima
    (on-sale flour Lima)
    (= (price flour Lima) 47.82)
    (on-sale batteries Lima)
    (= (price batteries Lima) 105.59)
    (on-sale oats Lima)
    (= (price oats Lima) 40.4)
    (on-sale carrots Lima)
    (= (price carrots Lima) 20.93)

    ; Defining what is on sale, and prices, for city Sanaa
    (on-sale rice Sanaa)
    (= (price rice Sanaa) 38.91)
    (on-sale carrots Sanaa)
    (= (price carrots Sanaa) 21.97)
    (on-sale batteries Sanaa)
    (= (price batteries Sanaa) 103.23)
    (on-sale oats Sanaa)
    (= (price oats Sanaa) 40.71)

    ; Defining what is on sale, and prices, for city Tehran
    (on-sale batteries Tehran)
    (= (price batteries Tehran) 107.74)
    (on-sale carrots Tehran)
    (= (price carrots Tehran) 29.84)
    (on-sale flour Tehran)
    (= (price flour Tehran) 35.7)
    (on-sale rice Tehran)
    (= (price rice Tehran) 30.55)

    ; Defining links between cities
    (can-drive Sanaa Lima)
    (can-drive Lima Sanaa)
    (= (drive-cost Sanaa Lima) 2.24)
    (= (drive-cost Lima Sanaa) 2.24)
    (can-drive Tehran Sanaa)
    (can-drive Sanaa Tehran)
    (= (drive-cost Tehran Sanaa) 6.11)
    (= (drive-cost Sanaa Tehran) 6.11)

    ; Defining initial state of salesman
    (at salesman Tehran)
    (= (capacity) 20)
    (= (money) 100)
    (= (bought rice) 0)
    (= (bought carrots) 0)
    (= (bought batteries) 0)
    (= (bought flour) 0)
    (= (bought oats) 0)

)
(:goal
    (and
        (>= (money) 120)
    )
)
)
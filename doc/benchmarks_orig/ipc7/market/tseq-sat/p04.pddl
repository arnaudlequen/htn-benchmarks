(define (problem trader-4-3-5-4-20-100-120-0)
(:domain Trader)
(:objects
    Apia MataUtu Nouakchott - market
    medicine rice carrots potatoes computers - goods
    salesman - trader
)

(:init

    ; Defining what is on sale, and prices, for city Apia
    (on-sale rice Apia)
    (= (price rice Apia) 39.3)
    (on-sale medicine Apia)
    (= (price medicine Apia) 169.98)
    (on-sale potatoes Apia)
    (= (price potatoes Apia) 30.5)
    (on-sale carrots Apia)
    (= (price carrots Apia) 27.69)

    ; Defining what is on sale, and prices, for city MataUtu
    (on-sale medicine MataUtu)
    (= (price medicine MataUtu) 172.7)
    (on-sale rice MataUtu)
    (= (price rice MataUtu) 38.58)
    (on-sale computers MataUtu)
    (= (price computers MataUtu) 202.85)
    (on-sale potatoes MataUtu)
    (= (price potatoes MataUtu) 31.47)

    ; Defining what is on sale, and prices, for city Nouakchott
    (on-sale carrots Nouakchott)
    (= (price carrots Nouakchott) 29.16)
    (on-sale potatoes Nouakchott)
    (= (price potatoes Nouakchott) 30.13)
    (on-sale medicine Nouakchott)
    (= (price medicine Nouakchott) 166.83)
    (on-sale rice Nouakchott)
    (= (price rice Nouakchott) 28.11)

    ; Defining links between cities
    (can-drive MataUtu Apia)
    (can-drive Apia MataUtu)
    (= (drive-cost MataUtu Apia) 5.62)
    (= (drive-cost Apia MataUtu) 5.62)
    (can-drive Nouakchott Apia)
    (can-drive Apia Nouakchott)
    (= (drive-cost Nouakchott Apia) 2.45)
    (= (drive-cost Apia Nouakchott) 2.45)
    (can-drive Nouakchott MataUtu)
    (can-drive MataUtu Nouakchott)
    (= (drive-cost Nouakchott MataUtu) 3.24)
    (= (drive-cost MataUtu Nouakchott) 3.24)

    ; Defining initial state of salesman
    (at salesman MataUtu)
    (= (capacity) 20)
    (= (money) 100)
    (= (bought medicine) 0)
    (= (bought rice) 0)
    (= (bought carrots) 0)
    (= (bought potatoes) 0)
    (= (bought computers) 0)

)
(:goal
    (and
        (>= (money) 120)
    )
)
)
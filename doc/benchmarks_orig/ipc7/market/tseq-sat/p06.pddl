(define (problem trader-6-3-5-4-20-100-120-0)
(:domain Trader)
(:objects
    Mamoudzou Yamoussoukro Jakarta - market
    sugar computers coffee potatoes carrots - goods
    salesman - trader
)

(:init

    ; Defining what is on sale, and prices, for city Mamoudzou
    (on-sale sugar Mamoudzou)
    (= (price sugar Mamoudzou) 34.11)
    (on-sale potatoes Mamoudzou)
    (= (price potatoes Mamoudzou) 38.96)
    (on-sale carrots Mamoudzou)
    (= (price carrots Mamoudzou) 27.59)
    (on-sale computers Mamoudzou)
    (= (price computers Mamoudzou) 281.53)

    ; Defining what is on sale, and prices, for city Yamoussoukro
    (on-sale coffee Yamoussoukro)
    (= (price coffee Yamoussoukro) 58.57)
    (on-sale computers Yamoussoukro)
    (= (price computers Yamoussoukro) 293.82)
    (on-sale sugar Yamoussoukro)
    (= (price sugar Yamoussoukro) 33.01)
    (on-sale potatoes Yamoussoukro)
    (= (price potatoes Yamoussoukro) 40.42)

    ; Defining what is on sale, and prices, for city Jakarta
    (on-sale carrots Jakarta)
    (= (price carrots Jakarta) 28.58)
    (on-sale coffee Jakarta)
    (= (price coffee Jakarta) 42.13)
    (on-sale potatoes Jakarta)
    (= (price potatoes Jakarta) 31.12)
    (on-sale computers Jakarta)
    (= (price computers Jakarta) 293.36)

    ; Defining links between cities
    (can-drive Yamoussoukro Mamoudzou)
    (can-drive Mamoudzou Yamoussoukro)
    (= (drive-cost Yamoussoukro Mamoudzou) 2.69)
    (= (drive-cost Mamoudzou Yamoussoukro) 2.69)
    (can-drive Jakarta Yamoussoukro)
    (can-drive Yamoussoukro Jakarta)
    (= (drive-cost Jakarta Yamoussoukro) 6.07)
    (= (drive-cost Yamoussoukro Jakarta) 6.07)

    ; Defining initial state of salesman
    (at salesman Mamoudzou)
    (= (capacity) 20)
    (= (money) 100)
    (= (bought sugar) 0)
    (= (bought computers) 0)
    (= (bought coffee) 0)
    (= (bought potatoes) 0)
    (= (bought carrots) 0)

)
(:goal
    (and
        (>= (money) 120)
    )
)
)
(define (problem trader-2-3-5-4-20-100-120-0)
(:domain Trader)
(:objects
    Mbabane Bishkek Tunis - market
    rice carrots salt oats phones - goods
    salesman - trader
)

(:init

    ; Defining what is on sale, and prices, for city Mbabane
    (on-sale oats Mbabane)
    (= (price oats Mbabane) 41.95)
    (on-sale phones Mbabane)
    (= (price phones Mbabane) 169.34)
    (on-sale carrots Mbabane)
    (= (price carrots Mbabane) 28.82)
    (on-sale salt Mbabane)
    (= (price salt Mbabane) 35.69)

    ; Defining what is on sale, and prices, for city Bishkek
    (on-sale oats Bishkek)
    (= (price oats Bishkek) 39.59)
    (on-sale phones Bishkek)
    (= (price phones Bishkek) 174.06)
    (on-sale salt Bishkek)
    (= (price salt Bishkek) 26.92)
    (on-sale carrots Bishkek)
    (= (price carrots Bishkek) 28.66)

    ; Defining what is on sale, and prices, for city Tunis
    (on-sale rice Tunis)
    (= (price rice Tunis) 41.63)
    (on-sale phones Tunis)
    (= (price phones Tunis) 224.15)
    (on-sale carrots Tunis)
    (= (price carrots Tunis) 27.84)
    (on-sale salt Tunis)
    (= (price salt Tunis) 26.4)

    ; Defining links between cities
    (can-drive Bishkek Mbabane)
    (can-drive Mbabane Bishkek)
    (= (drive-cost Bishkek Mbabane) 5.66)
    (= (drive-cost Mbabane Bishkek) 5.66)
    (can-drive Tunis Mbabane)
    (can-drive Mbabane Tunis)
    (= (drive-cost Tunis Mbabane) 0.46)
    (= (drive-cost Mbabane Tunis) 0.46)
    (can-drive Tunis Bishkek)
    (can-drive Bishkek Tunis)
    (= (drive-cost Tunis Bishkek) 6.05)
    (= (drive-cost Bishkek Tunis) 6.05)

    ; Defining initial state of salesman
    (at salesman Mbabane)
    (= (capacity) 20)
    (= (money) 100)
    (= (bought rice) 0)
    (= (bought carrots) 0)
    (= (bought salt) 0)
    (= (bought oats) 0)
    (= (bought phones) 0)

)
(:goal
    (and
        (>= (money) 120)
    )
)
)
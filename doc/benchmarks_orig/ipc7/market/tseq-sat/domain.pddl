(define (domain Trader)
(:requirements :typing :fluents) 
(:types market - place 
	trader goods - locatable)

(:predicates (at ?t - trader ?p - place)
             (can-drive ?from - place ?to - place)
             (on-sale ?g - goods ?m - market)
)

(:functions 
	    (drive-cost ?p1 ?p2 - place)
	    (price ?g - goods ?m - market)
	    (bought ?g - goods)
	    (money)
	    (capacity)
)

(:action travel
 :parameters (?t - trader ?from ?to - market)
 :precondition (and
			(can-drive ?from ?to)
			(>= (money) (drive-cost ?from ?to))
			(at ?t ?from)
		)
 :effect (and
		(decrease (money) (drive-cost ?from ?to))
		(at ?t ?to)
		(not (at ?t ?from))
	 )
	  
)


(:action buy
 :parameters (?t - trader ?g - goods ?m - market)
 :precondition (and
                        (at ?t ?m)
		        (<= (+ 10 (price ?g ?m)) (money))
			(>= (capacity) 1) 
			(on-sale ?g ?m)
               )
 :effect (and
              (decrease (capacity) 1)
              (increase (bought ?g) 1)
              (decrease (money) (price ?g ?m))
        )
)


(:action sell
 :parameters (?t - trader ?g - goods ?m - market)
 :precondition (and
			(at ?t ?m)
			(>= (bought ?g) 1) 
		)
 :effect (and
		(increase (capacity) 1)
		(decrease (bought ?g) 1)
	      	(increase (money) (price ?g ?m))
	)
)

)

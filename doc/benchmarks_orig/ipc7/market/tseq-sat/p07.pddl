(define (problem trader-7-3-5-4-20-100-120-0)
(:domain Trader)
(:objects
    Damascus Palikir Dhaka - market
    phones salt wine oats sugar - goods
    salesman - trader
)

(:init

    ; Defining what is on sale, and prices, for city Damascus
    (on-sale phones Damascus)
    (= (price phones Damascus) 178.13)
    (on-sale sugar Damascus)
    (= (price sugar Damascus) 33.86)
    (on-sale salt Damascus)
    (= (price salt Damascus) 34.24)
    (on-sale wine Damascus)
    (= (price wine Damascus) 113.52)

    ; Defining what is on sale, and prices, for city Palikir
    (on-sale oats Palikir)
    (= (price oats Palikir) 28.64)
    (on-sale wine Palikir)
    (= (price wine Palikir) 88.82)
    (on-sale phones Palikir)
    (= (price phones Palikir) 223.55)
    (on-sale sugar Palikir)
    (= (price sugar Palikir) 34.71)

    ; Defining what is on sale, and prices, for city Dhaka
    (on-sale phones Dhaka)
    (= (price phones Dhaka) 226.37)
    (on-sale oats Dhaka)
    (= (price oats Dhaka) 40.82)
    (on-sale sugar Dhaka)
    (= (price sugar Dhaka) 33.6)
    (on-sale salt Dhaka)
    (= (price salt Dhaka) 34.85)

    ; Defining links between cities
    (can-drive Palikir Damascus)
    (can-drive Damascus Palikir)
    (= (drive-cost Palikir Damascus) 2.52)
    (= (drive-cost Damascus Palikir) 2.52)
    (can-drive Dhaka Damascus)
    (can-drive Damascus Dhaka)
    (= (drive-cost Dhaka Damascus) 2.33)
    (= (drive-cost Damascus Dhaka) 2.33)
    (can-drive Dhaka Palikir)
    (can-drive Palikir Dhaka)
    (= (drive-cost Dhaka Palikir) 4.67)
    (= (drive-cost Palikir Dhaka) 4.67)

    ; Defining initial state of salesman
    (at salesman Damascus)
    (= (capacity) 20)
    (= (money) 100)
    (= (bought phones) 0)
    (= (bought salt) 0)
    (= (bought wine) 0)
    (= (bought oats) 0)
    (= (bought sugar) 0)

)
(:goal
    (and
        (>= (money) 120)
    )
)
)
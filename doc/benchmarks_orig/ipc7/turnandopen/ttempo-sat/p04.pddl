


(define (problem turnandopen-2-5-6)
(:domain turnandopen-strips)
(:objects robot1 robot2 - robot
rgripper1 lgripper1 rgripper2 lgripper2 - gripper
room1 room2 room3 room4 room5 - room
door1 door2 door3 door4 - door
ball1 ball2 ball3 ball4 ball5 ball6 - object)
(:init
(closed door1)
(closed door2)
(closed door3)
(closed door4)
(connected room1 room2 door1)
(connected room2 room1 door1)
(connected room2 room3 door2)
(connected room3 room2 door2)
(connected room3 room4 door3)
(connected room4 room3 door3)
(connected room4 room5 door4)
(connected room5 room4 door4)
(at-robby robot1 room5)
(free robot1 rgripper1)
(free robot1 lgripper1)
(at-robby robot2 room4)
(free robot2 rgripper2)
(free robot2 lgripper2)
(at ball1 room1)
(at ball2 room2)
(at ball3 room5)
(at ball4 room1)
(at ball5 room2)
(at ball6 room2)
)
(:goal
(and
(at ball1 room3)
(at ball2 room3)
(at ball3 room3)
(at ball4 room1)
(at ball5 room2)
(at ball6 room1)
)
)
(:metric minimize (total-time))

)



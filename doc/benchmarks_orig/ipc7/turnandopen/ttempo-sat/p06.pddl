


(define (problem turnandopen-3-6-8)
(:domain turnandopen-strips)
(:objects robot1 robot2 robot3 - robot
rgripper1 lgripper1 rgripper2 lgripper2 rgripper3 lgripper3 - gripper
room1 room2 room3 room4 room5 room6 - room
door1 door2 door3 door4 door5 - door
ball1 ball2 ball3 ball4 ball5 ball6 ball7 ball8 - object)
(:init
(closed door1)
(closed door2)
(closed door3)
(closed door4)
(closed door5)
(connected room1 room2 door1)
(connected room2 room1 door1)
(connected room2 room3 door2)
(connected room3 room2 door2)
(connected room3 room4 door3)
(connected room4 room3 door3)
(connected room4 room5 door4)
(connected room5 room4 door4)
(connected room5 room6 door5)
(connected room6 room5 door5)
(at-robby robot1 room4)
(free robot1 rgripper1)
(free robot1 lgripper1)
(at-robby robot2 room3)
(free robot2 rgripper2)
(free robot2 lgripper2)
(at-robby robot3 room1)
(free robot3 rgripper3)
(free robot3 lgripper3)
(at ball1 room3)
(at ball2 room2)
(at ball3 room5)
(at ball4 room3)
(at ball5 room2)
(at ball6 room6)
(at ball7 room5)
(at ball8 room1)
)
(:goal
(and
(at ball1 room2)
(at ball2 room6)
(at ball3 room3)
(at ball4 room1)
(at ball5 room6)
(at ball6 room3)
(at ball7 room2)
(at ball8 room2)
)
)
(:metric minimize (total-time))

)



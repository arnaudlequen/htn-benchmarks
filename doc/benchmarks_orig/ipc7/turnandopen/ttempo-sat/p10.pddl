


(define (problem turnandopen-3-6-12)
(:domain turnandopen-strips)
(:objects robot1 robot2 robot3 - robot
rgripper1 lgripper1 rgripper2 lgripper2 rgripper3 lgripper3 - gripper
room1 room2 room3 room4 room5 room6 - room
door1 door2 door3 door4 door5 - door
ball1 ball2 ball3 ball4 ball5 ball6 ball7 ball8 ball9 ball10 ball11 ball12 - object)
(:init
(closed door1)
(closed door2)
(closed door3)
(closed door4)
(closed door5)
(connected room1 room2 door1)
(connected room2 room1 door1)
(connected room2 room3 door2)
(connected room3 room2 door2)
(connected room3 room4 door3)
(connected room4 room3 door3)
(connected room4 room5 door4)
(connected room5 room4 door4)
(connected room5 room6 door5)
(connected room6 room5 door5)
(at-robby robot1 room2)
(free robot1 rgripper1)
(free robot1 lgripper1)
(at-robby robot2 room6)
(free robot2 rgripper2)
(free robot2 lgripper2)
(at-robby robot3 room5)
(free robot3 rgripper3)
(free robot3 lgripper3)
(at ball1 room5)
(at ball2 room3)
(at ball3 room4)
(at ball4 room4)
(at ball5 room1)
(at ball6 room5)
(at ball7 room2)
(at ball8 room1)
(at ball9 room1)
(at ball10 room6)
(at ball11 room1)
(at ball12 room1)
)
(:goal
(and
(at ball1 room5)
(at ball2 room3)
(at ball3 room3)
(at ball4 room6)
(at ball5 room1)
(at ball6 room4)
(at ball7 room4)
(at ball8 room1)
(at ball9 room5)
(at ball10 room4)
(at ball11 room3)
(at ball12 room1)
)
)
(:metric minimize (total-time))

)



import re
import numpy as np
import matplotlib.pyplot as plt
import os

MAX = 20

def main():

    dirlist = next(os.walk("."))[1]

    if "Figures" in dirlist:
        dirlist.remove("Figures")

    for dirname in dirlist:
        print(dirname, end=".. ")
        plan_length = [-1]
        time = [-1]
        for i in range(1, MAX + 1):
            pad = str(i).zfill(2)
            in_file = open(dirname + "/result" + pad, "rt")
            tsk = False
            plan_length.append(0)
            time.append(0)
            for line in in_file:
                if len(line) <= 1 and tsk:
                    tsk = False

                if tsk:
                    taskid, _ = line.split(":")
                    taskid = re.sub("[^0-9]", "", taskid)
                    plan_length[i] = int(taskid)

                if "Tasks:" in line:
                    tsk = True


                if "seconds total time" in line:
                    n = float(re.sub("[^0-9,^0-9]", "", line).replace(",", "."))
                    time[i] = n
            in_file.close()

        plan_length.remove(-1)
        time.remove(-1)

        if not os.path.exists("Figures/"):
            os.makedirs("Figures/")

        if not os.path.exists("Figures/" + dirname):
            os.makedirs("Figures/" + dirname)

        plt.plot(list(range(1, 21)), time, 'ro')
        plt.xlabel("Problem")
        plt.ylabel("Running Time (s)")
        plt.grid(True)
        plt.xticks(list(range(0, 21, 5)))
        plt.savefig("Figures/" + dirname + "/time.png")
        plt.clf()

        plt.plot(list(range(1, 21)), plan_length, 'ro')
        plt.xlabel("Problem")
        plt.ylabel("Plan length")
        plt.grid(True)
        plt.xticks(list(range(0, 21, 5)))
        plt.savefig("Figures/" + dirname + "/length.png")
        plt.clf()

        print("done")

main()

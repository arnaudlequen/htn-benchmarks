set terminal pdf 
set decimalsign "."
set output "IJAIT_search_time_SHOP_GTOHP_barman_same_scale.pdf"
set xlabel "Problems" font "Times-Roman,10"
set ylabel "Search Time (s)" font "Times-Roman,10"
set grid y
set xrange['0':'20']
set monochrome
#set logscale x
set yrange[0:'90']

plot "~/Workspace/Thesis_Projects/CoRe-Planner-1.0/tests_results/data/barman.data" using 0:2 with linespoints title "SHOP" pt 8, \
    "~/Workspace/Bitbucket_Projetcs/pddl4j_IJAIT/IJAIT_results/data/IJAIT_barman.data" using 0:9 with linespoints title "total GTOHP" pt 5, \
    "~/Workspace/Bitbucket_Projetcs/pddl4j_IJAIT/IJAIT_results/data/IJAIT_barman.data" using 0:8 with linespoints title "search GTOHP" pt 6, \

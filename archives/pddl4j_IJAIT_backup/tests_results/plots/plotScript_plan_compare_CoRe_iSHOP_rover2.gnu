set terminal pdf 
set decimalsign "."
set output "Plan_CoRe_iSHOP_rover.pdf"
set xlabel "Problems" font "Helvetica,6"
set ylabel "Plan length (action)" font "Helvetica,6"
set grid y
#set xrange['0':'22']
#set logscale x
#set yrange[0:'1100']

plot "~/Workspace/Thesis_Projects/CoRe-Planner-1.0/tests_results/data/rover2.data" using 0:3 with linespoints title "CoRe", \
    "~/Workspace/Bitbucket_Projetcs/pddl4j/tests_results/data/rover2.data" using 0:13 with linespoints title "iSHOP", \

set terminal pdf 
set decimalsign "."
set output "time_satellite_iSHOP.pdf"
set title "Processing on satellite with iSHOP " font "Helvetica,7"
set xlabel "Problem" font "Helvetica,6"
set ylabel "Processing Time (s)" font "Helvetica,6"
set grid y
#set xrange['0':'18']
#set logscale x
#set yrange[0:'1100']

plot"../data/satellite.data" using 0:6 with linespoints title "Preprocessing", \
    "../data/satellite.data" using 0:8 with linespoints title "Searching", \
    "../data/satellite.data" using 0:9 with linespoints title "Total"

set terminal pdf 
set decimalsign "."
set output "time_rover_2_fast.pdf"
set title "Processing rover 2(fast) " font "Helvetica,7"
set xlabel "Problem" font "Helvetica,6"
set ylabel "Processing Time (s)" font "Helvetica,6"
set grid y
set xrange['0':'18']
#set logscale x
#set yrange[0:'1100']

plot "../data/results_rover2_fast.data" using 0:6 with linespoints title "Preprocessing", \
    "../data/results_rover2_fast.data" using 0:8 with linespoints title "Searching", \
    "../data/results_rover2_fast.data" using 0:9 with linespoints title "Total"

"""

Barman Problem Generator

"""
import argparse
import random

parser = argparse.ArgumentParser(description='Generate a problem for Barman.')
parser.add_argument('cocktails', metavar='cocktails', type=int,
                   help='number of cocktails to do')
parser.add_argument('ingredients', metavar='ingredients', type=int,
                  help='number of ingredients')
parser.add_argument('shots', metavar='shots', type=int,
                  help='number of shots')
parser.add_argument('-o', metavar="output", help="name of the output file", type=str, default="p00.pddl")

args = parser.parse_args()

def main():
    shotlist = ''.join(['shot' + str(i) + ' ' for i in range(1, max(args.shots+1, args.cocktails + 2))])
    ingrlist = ''.join(['ingredient' + str(i) + ' ' for i in range(1, args.ingredients+1)])
    cocklist = ''.join(['cocktail' + str(i) + ' ' for i in range(1, args.cocktails+1)])
    displist = ''.join(['dispenser' + str(i) + ' ' for i in range(1, args.ingredients+1)])

    shotpos   = ''.join(['       (ontable shot' + str(i) + ')\n' for i in range(1, args.shots+1)])
    disppos   = ''.join(['       (dispenses dispenser' + str(i) + ' ingredient' + str(i) + ')\n' for i in range(1, args.ingredients+1)])
    shotclean = ''.join(['       (clean shot' + str(i) + ')\n' for i in range(1, args.shots+1)])
    shotempty = ''.join(['       (empty shot' + str(i) + ')\n' for i in range(1, args.shots+1)])

    part1 = []
    part2 = []
    for i in range(args.cocktails):
        part1.append(random.randint(1, args.ingredients))
        part2.append(random.randint(1, args.ingredients))
        while part2[-1] == part1[-1]:
            part2[-1] = random.randint(1, args.ingredients)

    cockpart1 = ''.join(['       (cocktail-part1 cocktail' + str(i) + ' ingredient' + str(part1[i-1]) + ')\n' for i in range(1, args.cocktails+1)])
    cockpart2 = ''.join(['       (cocktail-part2 cocktail' + str(i) + ' ingredient' + str(part2[i-1]) + ')\n' for i in range(1, args.cocktails+1)])

    shotcont = list(range(1, args.cocktails+1))
    random.shuffle(shotcont)
    shotcont.extend([random.randint(1, args.ingredients) for _ in range(args.cocktails, args.shots+1)])

    tasks   = ''.join(['              (tag t' + str(i) + ' (do_cocktail_in_shot shot' + str(i) + ' cocktail' + str(shotcont[i-1]) + '))\n' for i in range(1, args.cocktails+1)])
    constraints = ''.join(['              (contains shot' + str(i) + ' cocktail' + str(shotcont[i-1]) + ')\n' for i in range(1, args.cocktails+1)])


    tasks   += ''.join(['              (tag t' + str(i) + ' (do_cocktail_in_shot shot' + str(i) + ' ingredient' + str(shotcont[i-1]) + '))\n' for i in range(args.cocktails+1, args.shots)])
    constraints += ''.join(['              (contains shot' + str(i) + ' ingredient' + str(shotcont[i-1]) + ')\n' for i in range(args.cocktails+1, args.shots)])


    write_file  = "(define (problem barman-auto-" + str(args.cocktails) + "-" + str(args.ingredients) + "-" + str(args.shots) + " )\n(:domain barman )\n(:requirements :strips :typing :negative-preconditions :htn :equality)\n\n"
    write_file += "(:objects shaker1 - shaker\n"
    write_file += "          left right - hand\n"
    write_file += "          " + shotlist + " - shot\n"
    write_file += "          " + ingrlist + " - ingredient\n"
    write_file += "          " + cocklist + " - cocktail\n"
    write_file += "          " + displist + " - dispenser\n"
    write_file += "          l0 l1 l2 - level\n)\n\n"

    write_file += "(:init (ontable shaker1)\n"
    write_file += shotpos
    write_file += disppos
    write_file += "       (clean shaker1)\n"
    write_file += shotclean
    write_file += "       (empty shaker1)\n"
    write_file += shotempty
    write_file += "       (handempty left)\n       (handempty right)\n       (shaker-empty-level shaker1 l0)\n"
    write_file += "       (shaker-level shaker1 l0)\n       (next l0 l1)\n       (next l1 l2)\n"
    write_file += cockpart1
    write_file += cockpart2

    write_file += ")\n"

    write_file += "(:goal\n     :tasks  (\n"
    write_file += tasks
    write_file += "             )\n"

    write_file += "     :constraints\n"
    write_file += "             (and (after (and\n"
    write_file += constraints
    write_file += "             ) t" + str(args.shots-1) + "))\n"

    write_file += ")\n)"

    in_file = open(args.o, "w+")
    in_file.write(write_file)
    in_file.close()


main()

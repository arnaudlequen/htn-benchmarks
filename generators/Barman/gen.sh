i=0
for cocktails in `seq 30 15 60`; do
  for ingredients in `seq 10 5 20`; do
    for shots in `seq 10 5 15`; do
      let "i=$i+1"
      printf -v n "%02d" $i;
      python3 generator.py $cocktails $ingredients $(($shots + $cocktails)) -o np$n.pddl
    done
  done
done

import subprocess
import argparse
import random

parser = argparse.ArgumentParser(description='Generate a problem for Rover.')
parser.add_argument('rovers', metavar='rovers', type=int,
                   help='number of rovers')
parser.add_argument('waypoints', metavar='waypoints', type=int,
                  help='number of waypoints')
parser.add_argument('objectives', metavar='objectives', type=int,
                  help='number of objectives')
parser.add_argument('cameras', metavar='cameras', type=int,
                  help='number of cameras')
parser.add_argument('ngoals', metavar='ngoals', type=int,
                  help='number of goals')
parser.add_argument('-o', metavar="output", help="name of the output file", type=str, default="p00.pddl")
parser.add_argument('-s', type=int, default="-1")

args = parser.parse_args()

def main():
    seed = random.randint(1, 2**10)
    if args.s >= 0:
        seed = args.s

    command = './rovgen ' + str(seed) + ' ' + str(args.rovers) + ' ' + str(args.waypoints) + ' ' + str(args.objectives) + ' ' + str(args.cameras) + ' ' + str(args.ngoals)

    f = open("rover-generator/tmp.pddl", "w+")
    process = subprocess.Popen(command.split(), stdout=f, cwd='rover-generator/')
    process.communicate()

    command = 'python rover_convert_to_htn.py tmp.pddl'
    process = subprocess.Popen(command.split(), cwd='rover-generator/')
    process.communicate()

    command = 'mv htn_tmp.pddl ../' + str(args.o)
    process = subprocess.Popen(command.split(), cwd='rover-generator/')

    command = 'rm tmp.pddl'
    process = subprocess.Popen(command.split(), cwd='rover-generator/')

main()

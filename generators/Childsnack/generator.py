"""

Childsnack Problem Generator

"""
import argparse
import random
from math import ceil

parser = argparse.ArgumentParser(description='Generate a problem for Childsnack.')
parser.add_argument('children', metavar='children', type=int,
                   help='children to serve')
parser.add_argument('gluten', metavar='gluten-factor', type=float,
                  help='percentage of children who are allergic to gluten')
parser.add_argument('surplus', metavar='surplus-factor', type=float,
                  help='factor multiplying the amount of additional sandwiches')
parser.add_argument('-o', metavar="output", help="name of the output file", type=str, default="p00.pddl")

args = parser.parse_args()

def main():

    if not 0 <= args.gluten <= 1.0:
        print("gluten-factor must be between 0 and 1")
        return
    if not 0 <= args.surplus:
        print("surplus must be non-negative")
        return

    chldlist = ''.join(['child' + str(i) + ' ' for i in range(1, args.children+1)])
    brdslist = ''.join(['bread' + str(i) + ' ' for i in range(1, args.children+1)])
    contlist = ''.join(['content' + str(i) + ' ' for i in range(1, args.children+1)])
    sandlist = ''.join(['sandw' + str(i) + ' ' for i in range(1, int(args.children * (1 + args.surplus) + 1))])

    brdspos  = ''.join(['       (at_kitchen_bread bread' + str(i) + ')\n' for i in range(1, args.children+1)])
    contpos  = ''.join(['       (at_kitchen_content content' + str(i) + ')\n' for i in range(1, args.children+1)])

    allergics = int(ceil(args.children * args.gluten))

    noglutenbreads = random.sample(list(range(1, args.children + 1)), allergics)
    noglubrd = ''.join(['       (no_gluten_bread bread' + str(i) + ')\n' for i in noglutenbreads])

    noglutencontents = random.sample(list(range(1, args.children + 1)), allergics)
    noglucnt = ''.join(['       (no_gluten_content content' + str(i) + ')\n' for i in noglutencontents])

    noglutenchildren = random.sample(list(range(1, args.children + 1)), allergics)
    nogluchd = ''.join(['       (allergic_gluten child' + str(i) + ')\n' for i in noglutenchildren])
    otherchd = ''.join(['       (not_allergic_gluten child' + str(i) + ')\n' for i in list(set(range(1, args.children + 1)) - set(noglutenchildren))])

    chldpos  = ''.join(['       (waiting child' + str(i) + ' table' + str(random.randint(1, 3)) + ')\n' for i in range(1, args.children+1)])
    sandexs  = ''.join(['       (notexist sandw' + str(i) + ')\n' for i in range(1, int(args.children * (1 + args.surplus) + 1))])


    tasks    = ''.join(['              (tag t' + str(i) + ' (serve child' + str(i) + '))\n' for i in range(1, args.children+1)])
    constraints = ''.join(['              (served child' + str(i) + ')\n' for i in range(1, args.children+1)])

    write_file  = "(define (problem childsnack-auto-" + str(args.children) + "-" + str(allergics) + "-" + str(int(args.children * (1 + args.surplus) + 1)) + " )\n(:domain child-snack)\n(:requirements :strips :typing :negative-preconditions :htn)\n\n"
    write_file += "(:objects \n"
    write_file += "          " + chldlist + " - child\n"
    write_file += "          " + brdslist + " - bread-portion\n"
    write_file += "          " + contlist + " - content-portion\n"
    write_file += "          tray1 tray2 tray3 tray4 - tray\n"
    write_file += "          table1 table2 table3 - place\n"
    write_file += "          " + sandlist + " - sandwich\n"
    write_file += ")\n\n"

    write_file += "(:init \n"
    write_file += "       (at tray1 kitchen)\n"
    write_file += "       (at tray2 kitchen)\n"
    write_file += "       (at tray3 kitchen)\n"
    write_file += "       (at tray4 kitchen)\n"
    write_file += brdspos
    write_file += contpos
    write_file += noglubrd
    write_file += noglucnt
    write_file += nogluchd
    write_file += otherchd
    write_file += chldpos
    write_file += sandexs


    write_file += ")\n"

    write_file += "(:goal\n     :tasks  (\n"
    write_file += tasks
    write_file += "             )\n"

    write_file += "     :constraints\n"
    write_file += "             (and (after (and\n"
    write_file += constraints
    write_file += "             ) t1))\n"

    write_file += ")\n)"

    in_file = open(args.o, "w+")
    in_file.write(write_file)
    in_file.close()


main()

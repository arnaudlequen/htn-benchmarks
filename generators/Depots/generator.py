import subprocess
import argparse
import random

parser = argparse.ArgumentParser(description='Generate a problem for Depots.')
parser.add_argument('depots', metavar='depots', type=int,
                   help='number of depots')
parser.add_argument('distributors', metavar='distributors', type=int,
                  help='number of distributors')
parser.add_argument('trucks', metavar='trucks', type=int,
                  help='number of trucks')
parser.add_argument('pallets', metavar='pallets', type=int,
                  help='number of pallets')
parser.add_argument('hoists', metavar='hoists', type=int,
                  help='number of hoists')
parser.add_argument('crates', metavar='crates', type=int,
                  help='number of crates')
parser.add_argument('-o', metavar="output", help="name of the output file", type=str, default="p00.pddl")
args = parser.parse_args()

def main():

    command = './depots -e ' + str(args.depots) + ' -i ' + str(args.distributors) + ' -t ' + str(args.trucks) + ' -p ' + str(args.pallets) + ' -h ' + str(args.hoists) + ' -c ' + str(args.crates)
    f = open("depots-generator/tmp.pddl", "w+")
    process = subprocess.Popen(command.split(), stdout=f, cwd='depots-generator/')
    process.communicate()

    command = 'python depots_convert_to_htn.py tmp.pddl'
    process = subprocess.Popen(command.split(), cwd='depots-generator/')
    process.communicate()

    command = 'mv htn_tmp.pddl ../' + str(args.o)
    process = subprocess.Popen(command.split(), cwd='depots-generator/')

    command = 'rm tmp.pddl'
    process = subprocess.Popen(command.split(), cwd='depots-generator/')

main()

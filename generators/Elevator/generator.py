"""

Elevator (Miconic) Problem Generator

"""
import argparse
import random

parser = argparse.ArgumentParser(description='Generate a problem for Elevator.')
parser.add_argument('people', metavar='people', type=int,
                   help='number of persons to serve')
parser.add_argument('floors', metavar='floors', type=int,
                   help='height of the building')
parser.add_argument('-o', metavar="output", help="name of the output file", type=str, default="p00.pddl")
args = parser.parse_args()

def main():
    paslist = ''.join(['p' + str(i) + ' ' for i in range(args.people)])
    flolist = ''.join(['f' + str(i) + ' ' for i in range(args.floors)])

    flopos = ""
    for i in range(args.floors):
        for j in range(i + 1, args.floors):
            flopos += "(above f" + str(i) + " f" + str(j) + ")\n"
        flopos += "\n"

    pplpos = ""
    for p in range(args.people):
        o = random.randint(0, args.floors-1)
        d = random.randint(0, args.floors-1)
        while o == d:
            d = random.randint(0, args.floors)
        pplpos += "(origin p" + str(p) + " f" + str(o) + ")\n"
        pplpos += "(destin p" + str(p) + " f" + str(d) + ")\n\n"

    tasks   = ''.join(['              (tag t' + str(i) + ' (give_a_ride p' + str(i) + '))\n' for i in range(args.people)])


    constraints = ''.join(['              (served p' + str(i) + ')\n' for i in range(args.people)])

    write_file  = "(define (problem elevator-auto-" + str(args.floors) + "-" + str(args.people) + " )\n(:domain miconic )\n(:requirements :strips :typing :negative-preconditions :htn :equality)\n\n"
    write_file += "(:objects \n"
    write_file += "          " + paslist + " - passenger\n"
    write_file += "          " + flolist + " - floor)\n"
    write_file += "(:init \n"
    write_file += flopos
    write_file += pplpos
    write_file += "(lift-at f0)\n"
    write_file += ")\n"

    write_file += "(:goal\n     :tasks  (\n"
    write_file += tasks
    write_file += "             )\n"

    write_file += "     :constraints\n"
    write_file += "             (and (after (and\n"
    write_file += constraints
    write_file += "             ) t" + str(args.people - 1) + "))\n"

    write_file += ")\n)"

    in_file = open(args.o, "w+")
    in_file.write(write_file)
    in_file.close()


main()

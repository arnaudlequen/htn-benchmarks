i=0
for people in `seq 50 25 100`; do
  for floors in `seq 100 100 700`; do
    let "i=$i+1"
    printf -v n "%02d" $i;
    python3 generator.py $people $floors -o np$n.pddl
  done
done

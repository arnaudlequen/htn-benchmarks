import subprocess
import argparse
import random

parser = argparse.ArgumentParser(description='Generate a problem for Satellite.')
parser.add_argument('satellites', metavar='satellites', type=int,
                   help='number of satellites')
parser.add_argument('instruments', metavar='instruments', type=int,
                  help='max instruments per satellite')
parser.add_argument('modes', metavar='modes', type=int,
                  help='number of modes')
parser.add_argument('targets', metavar='targets', type=int,
                  help='number of targets')
parser.add_argument('observations', metavar='observations', type=int,
                  help='number of observations')
parser.add_argument('-o', metavar="output", help="name of the output file", type=str, default="p00.pddl")
parser.add_argument('-s', type=int, default="-1")

args = parser.parse_args()

def main():
    seed = random.randint(1, 2**10)
    if args.s >= 0:
        seed = args.s

    command = './satgen ' + str(seed) + ' ' + str(args.satellites) + ' ' + str(args.instruments) + ' ' + str(args.modes) + ' ' + str(args.targets) + ' ' + str(args.observations) + ' > tmp.pddl'

    f = open("satellite-generator/tmp.pddl", "w+")
    process = subprocess.Popen(command.split(), stdout=f, cwd='satellite-generator/')
    process.communicate()

    command = 'python satellite_convert_to_htn.py tmp.pddl'
    process = subprocess.Popen(command.split(), cwd='satellite-generator/')
    process.communicate()

    command = 'mv htn_tmp.pddl ../' + str(args.o)
    process = subprocess.Popen(command.split(), cwd='satellite-generator/')

    command = 'rm tmp.pddl'
    process = subprocess.Popen(command.split(), cwd='satellite-generator/')

main()

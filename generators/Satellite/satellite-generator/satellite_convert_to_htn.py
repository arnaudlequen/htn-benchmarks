import re
import numpy as np
import matplotlib.pyplot as plt
import os
import sys


def main():
    taskList = []

    write_file = ""
    write_file_after = ""
    if len(sys.argv) <= 1:
        return "Please provide an input file"
    in_file = open(sys.argv[1], "rt")
    tsk = False
    for line in in_file:
        if len(line) <= 4 and tsk:
            tsk = False
            write_file += "  :tasks (\n"
            taskList.sort()
            for j in range(1, len(taskList)+1):
                write_file += "             (tag t" + str(j) + " " + taskList[j-1] + ")\n"
            write_file += "         )\n  :constraints\n         (and (after (and\n"
            write_file += write_file_after
            write_file += "         ) t" + str(len(taskList)) + "))\n)\n)"

            break


        if tsk:
            write_file_after += line
            parsedLine = line.replace("(", "").replace(")", "").split()
            if parsedLine[0] == "have_image":
                taskList.append("(do_mission " + parsedLine[1] + " " + parsedLine[2] + ")")
            elif parsedLine[0] == "pointing":
                taskList.append("(do_turning " + parsedLine[1] + " " + parsedLine[2] + ")")


        if ":goal" in line:
            tsk = True
            write_file += "(:goal\n"

        if not tsk:
            write_file += line

        if ":domain" in line:
            write_file += "     (:requirements :strips :typing :htn :negative-preconditions :equality)\n"

    in_file.close()

    in_file = open("htn_" + sys.argv[1], "w+")
    in_file.write(write_file)
    in_file.close()

main()

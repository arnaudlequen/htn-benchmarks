#!/bin/bash

problemfile=$1

# Extract the goal predicates from the problem
cat $problemfile |awk '/:goal/{y=1;next}y' |grep -oE "\(on b[0-9]+ b[0-9]+\)" | sed 's/\(on \|(\|)\)//g' > block_goal

# Generate "tasks" file containing the correctly ordered 
# sequence of initial tasks
python3 sort_tasks.py
rm block_goal
mv tasks ${1}_tasks

problemfile_nonhtn=${problemfile}_nonhtn
cat $problemfile | grep -vE "^$" > $problemfile_nonhtn
rm $problemfile

# Integrate HTN information into problem file
while read -r line ; do
    if echo $line | grep -q "(:domain" ; then
        # Add requirements
        echo $line >> $problemfile
        echo "(:requirements :strips :typing :negative-preconditions :htn :equality)" >> $problemfile
    elif echo $line | grep -q "(:goal" ; then
        # Add initial tasks
        echo "(:goal :tasks (" >> $problemfile
        cat ${problemfile}_tasks >> $problemfile
        echo ") :constraints(and (after " >> $problemfile
    elif echo $line | grep -qE "))$" ; then
        # Add ending of constraints definition
        echo $line" t`cat ${problemfile}_tasks|wc -l`)" >> $problemfile
    else
        # Add remaining information
        echo $line >> $problemfile
    fi
done < $problemfile_nonhtn

# Missing bracket
echo ")" >> $problemfile

rm ${problemfile}_{tasks,nonhtn}


# Read tasks and build initial tower components

towers = []
f_out = open("tasks", 'w')

for line in open("block_goal", 'r').readlines():
    
    words = line.replace('\n', '').split(" ")
    block_top = words[0]
    block_bot = words[1]
    new_pair = (block_top, block_bot)
    
    if len(towers) == 0:
        towers += [[new_pair]]
    else:
        inserted = False
        for tower in towers:
            if not inserted:
                for i in range(len(tower)):
                    old_pair = tower[i]
                    if old_pair[1] == block_top:
                        tower.insert(i+1, new_pair)
                        inserted = True
                        break
                    elif old_pair[0] == block_bot:
                        tower.insert(i, new_pair)
                        inserted = True
                        break
        if not inserted:
            towers += [[new_pair]]




# Merge towers as necessary

merged = True

while merged:
    merged = False
    delete = None
    
    for tower1 in towers:
        for tower2idx in range(len(towers)):
            tower2 = towers[tower2idx]
            
            if merged:
                break
            
            # Should tower1 be stacked on tower2?
            if tower1[-1][1] == tower2[0][0]:
                tower1 += tower2
                delete = tower2idx
                merged = True
    
    if delete:
        del towers[delete]




# Output final tasks in correct order

idx = 1
for tower in towers:
    for pair in tower[::-1]:
        f_out.write("(tag t" + str(idx) + " (do_put_on " + pair[0] + " " + pair[1] + "))\n")
        idx += 1
    #f_out.write("\n")
    

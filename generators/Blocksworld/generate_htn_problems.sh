#!/bin/bash

# Must be at least 2
minblocks=40
# Must be larger/equals to minblocks
maxblocks=50
# Must be at least 1
instances_per_size=1

function create_new_file() {
    n=$1
    i=$2
    c=$3
    new_file=new_problems/p${c}.pddl
    ./blocksworld $n > $new_file
    cp $new_file new_problems/htn/p${c}.pddl
    bash add_htn.sh $new_file
    echo $new_file
}

rm new_problems/p*.pddl* new_problems/htn/p*.pddl*

c=1
for n in `seq -w $minblocks $maxblocks` ; do
    for i in `seq 1 $instances_per_size`; do
        new_file=`create_new_file $n $i $c`
        while grep -q "(and)" $new_file ; do
            new_file=`create_new_file $n $i $c`
        done
        c=$((c+1))
    done
done

rm states.blo 2>/dev/null

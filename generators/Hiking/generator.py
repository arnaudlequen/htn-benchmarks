"""

Hiking Problem Generator

"""
import argparse
import random

parser = argparse.ArgumentParser(description='Generate a problem for Hiking.')
parser.add_argument('cars', metavar='cars', type=int,
                   help='available cars')
parser.add_argument('tents', metavar='tents', type=int,
                  help='available tents')
parser.add_argument('couples', metavar='couples', type=int,
                  help='number of couples who go hiking')
parser.add_argument('places', metavar='places', type=int,
                  help='length of the hiking path')
parser.add_argument('-o', metavar="output", help="name of the output file", type=str, default="p00.pddl")
args = parser.parse_args()

def randomUpDown():
    c = random.randint(0, 1)
    if c == 1:
        return "down"
    return "up"

def main():
    carslist = ''.join(['car' + str(i) + ' ' for i in range(args.cars)])
    tentlist = ''.join(['tent' + str(i) + ' ' for i in range(args.tents)])
    cplslist = ''.join(['couple' + str(i) + ' ' for i in range(args.couples)])
    perslist = ''.join(['guy' + str(i) + ' girl' + str(i) + ' ' for i in range(args.couples)])
    plcelist = ''.join(['place' + str(i) + ' ' for i in range(args.places)])

    perspos   = ''.join(['       (partners couple' + str(i) + ' guy' + str(i) + ' girl' + str(i) + ')\n       (at_person guy' + str(i) + ' place0)\n       (at_person girl' + str(i) + ' place0)\n       (walked couple' + str(i) + ' place0)\n' for i in range(args.couples)])
    tentpos   = ''.join(['       (at_tent tent' + str(i) + ' place0)\n       (' + randomUpDown() + ' tent' + str(i) + ')\n' for i in range(args.tents)])
    carspos   = ''.join(['       (at_car car' + str(i) + ' place0)\n' for i in range(args.cars)])
    plcepos   = ''.join(['       (next place' + str(i) + ' place' + str(i+1) + ')\n' for i in range(args.places-1)])

    tasks   = '              (tag t1 (everyone_go_hiking place' + str(args.places-1) + '))'
    constraints = ''.join(['              (walked couple' + str(i) + ' place' + str(args.places-1) + ')\n' for i in range(args.couples)])

    write_file  = "(define (problem hiking-auto-" + str(args.cars) + "-" + str(args.tents) + "-" + str(args.couples) + "-" + str(args.places) + " )\n(:domain hiking)\n(:requirements :strips :typing :negative-preconditions :htn :equality)\n\n"
    write_file += "(:objects \n"
    write_file += "          " + carslist + " - car\n"
    write_file += "          " + tentlist + " - tent\n"
    write_file += "          " + cplslist + " - couple\n"
    write_file += "          " + plcelist + " - place\n"
    write_file += "          " + perslist + " - person\n"
    write_file += ")\n\n"

    write_file += "(:init \n"
    write_file += perspos
    write_file += tentpos
    write_file += carspos
    write_file += plcepos


    write_file += ")\n"

    write_file += "(:goal\n     :tasks  (\n"
    write_file += tasks
    write_file += "             )\n"

    write_file += "     :constraints\n"
    write_file += "             (and (after (and\n"
    write_file += constraints
    write_file += "             ) t1))\n"

    write_file += ")\n)"

    in_file = open(args.o, "w+")
    in_file.write(write_file)
    in_file.close()


main()

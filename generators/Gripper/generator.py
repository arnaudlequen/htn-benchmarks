"""

Gripper Problem Generator

"""
import argparse

parser = argparse.ArgumentParser(description='Generate a problem for Gripper.')
parser.add_argument('balls', metavar='balls', type=int,
                   help='number of balls involved')
parser.add_argument('-o', metavar="output", help="name of the output file", type=str, default="p00.pddl")

args = parser.parse_args()

def main():
    objlist = ''.join(['ball' + str(i) + ' ' for i in range(1, args.balls+1)])
    objpos  = ''.join(['       (at ball' + str(i) + ' rooma)\n' for i in range(1, args.balls+1)])

    tasks   = ''.join(['              (tag t' + str(i-1) + ' (move_two_balls ball' + str(2*i - 1) + ' ball' + str(2*i) + ' roomb))\n' for i in range(1, args.balls//2 + 1)])
    if args.balls % 2 == 1:
        tasks  += '              (tag t' + str(args.balls//2) + ' (move_one_ball ball' + str(args.balls) + ' roomb))\n'

    constraints = ''.join(['              (at ball' + str(i) + ' roomb)\n' for i in range(1, args.balls + 1)])

    write_file  = "(define (problem gripper-auto-" + str(args.balls) + " )\n(:domain gripper-typed )\n(:requirements :strips :typing :negative-preconditions :htn :equality)\n\n"
    write_file += "(:objects rooma roomb - room\n"
    write_file += "          " + objlist + " - ball)\n"
    write_file += "(:init (at-robby rooma)\n"
    write_file += "       (free left)\n"
    write_file += "       (free right)\n"
    write_file += objpos
    write_file += ")\n"

    write_file += "(:goal\n     :tasks  (\n"
    write_file += tasks
    write_file += "             )\n"

    write_file += "     :constraints\n"
    write_file += "             (and (after (and\n"
    write_file += constraints
    write_file += "             ) t" + str(args.balls//2 + args.balls % 2 - 1) + "))\n"

    write_file += ")\n)"

    in_file = open(args.o, "w+")
    in_file.write(write_file)
    in_file.close()


main()

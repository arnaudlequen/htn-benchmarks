i=0
for balls in `seq 50 50 1000`; do
	let "i=$i+1"
	printf -v n "%02d" $i;
	python3 generator.py $balls -o np$n.pddl
done

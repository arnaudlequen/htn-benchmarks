set terminal pdf 
set decimalsign "."
set output "compare_nb_iground_meths_rover_logscale_same_scale.pdf"
set xlabel "Problems" font "Times-Roman,8"
set ylabel "Nb ground methods" font "Times-Roman,8"
set grid y
set logscale y
#set style line 3 lt 5 lw 2 pt 6 ps 0.5
set xrange['0':'23']

set yrange[1:'100000000']
set monochrome

plot "~/Workspace/Bitbucket_Projetcs/pddl4j_IJAIT/IJAIT_results/data/IJAIT_rover.data" using 0:14 with linespoints title "without simplification" pt 8, \
"~/Workspace/Bitbucket_Projetcs/pddl4j_IJAIT/IJAIT_results/data/IJAIT_rover.data" using 0:5 with linespoints title "with simplification" pt 5, \

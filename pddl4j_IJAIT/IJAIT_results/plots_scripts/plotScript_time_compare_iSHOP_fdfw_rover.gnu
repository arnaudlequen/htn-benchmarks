set terminal pdf 
set decimalsign "."
set output "time_fdwd_iSHOP_rover2_final_reduced.pdf"
set xlabel "Problems" font "Helvetica,6"
set ylabel "Search Time (s)" font "Helvetica,6"
set grid y
#set xrange['0':'22']
#set logscale x
#set yrange[0:'1100']

 plot   "~/Workspace/Bitbucket_Projetcs/pddl4j/tests_results/data/rover2_reduced.data" using 0:9 with linespoints title "total iSHOP", \
    "~/Workspace/Bitbucket_Projetcs/pddl4j/tests_results/data/rover2_reduced.data" using 0:8 with linespoints title "search iSHOP", \
    "~/Workspace/Bitbucket_Projetcs/pddl4j/tests_results/fDownward/data/rover_reduced.data" using 0:6 with linespoints title "real total fDownward", \

set terminal pdf 
set decimalsign "."
set output "IJAIT_plan_length_SHOP_GTOHP_rover.pdf"
set xlabel "Problems" font "Times-Roman,10"
set ylabel "Plan length (action)" font "Times-Roman,10"
set grid y
set monochrome
set xrange['0':'23']
#set logscale x
#set yrange[0:'1100']

plot "~/Workspace/Thesis_Projects/CoRe-Planner-1.0/tests_results/data/rover2.data" using 0:3 with linespoints title "SHOP" pt 8, \
    "~/Workspace/Bitbucket_Projetcs/pddl4j_IJAIT/IJAIT_results/data/IJAIT_rover.data" using 0:13 with linespoints title "GTOHP" pt 5, \

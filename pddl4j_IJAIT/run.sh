#!/bin/bash

if [ -z "$1" ]; then
	echo "Please provide a domain"
else
	mkdir results/$1



	for i in $(seq 1 1 20);
	do
	echo Test $i;
	printf -v i "%02d" $i;
	java -javaagent:lib/iSHOP.jar -server -Xms8048m -Xmx8048m -classpath lib/iSHOP.jar pddl4j.examples.ISHOP.ISHOP -o problems/$1/domain.pddl -f problems/$1/p$i.pddl > results/$1/result$i;
	done;
	echo DONE
fi

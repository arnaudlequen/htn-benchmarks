(define (domain sokoban-sequential)
  (:requirements :typing :equality :htn :negative-preconditions)
  (:types player stone location direction - object)
  (:predicates (clear ?l - location)
	       (at_stone ?s - stone ?l - location)
         (at_player ?p - player ?l - location)
	       (at_goal ?s - stone)
	       (is_goal ?l - location)
	       (is_nongoal ?l - location)
               (movedir ?from ?to - location ?dir - direction))


  (:action move
   :parameters (?p - player ?from ?to - location ?dir - direction)
   :precondition (and (at_player ?p ?from)
                      (clear ?to)
                      (movedir ?from ?to ?dir)
                      )
   :effect       (and (not (at_player ?p ?from))
                      (not (clear ?to))
                      (at_player ?p ?to)
                      (clear ?from)
                      )
   )

  (:action push-to-nongoal
   :parameters (?p - player ?s - stone
                ?ppos ?from ?to - location
                ?dir - direction)
   :precondition (and (at_player ?p ?ppos)
                      (at_stone ?s ?from)
                      (clear ?to)
                      (movedir ?ppos ?from ?dir)
                      (movedir ?from ?to ?dir)
                      (is_nongoal ?to)
                      )
   :effect       (and (not (at_player ?p ?ppos))
                      (not (at_stone ?s ?from))
                      (not (clear ?to))
                      (at_player ?p ?from)
                      (at_stone ?s ?to)
                      (clear ?ppos)
                      (not (at_goal ?s))

                      )
   )

  (:action push-to-goal
   :parameters (?p - player ?s - stone
                ?ppos ?from ?to - location
                ?dir - direction)
   :precondition (and (at_player ?p ?ppos)
                      (at_stone ?s ?from)
                      (clear ?to)
                      (movedir ?ppos ?from ?dir)
                      (movedir ?from ?to ?dir)
                      (is_goal ?to)
                      )
   :effect       (and (not (at_player ?p ?ppos))
                      (not (at_stone ?s ?from))
                      (not (clear ?to))
                      (at_player ?p ?from)
                      (at_stone ?s ?to)
                      (clear ?ppos)
                      (at_goal ?s)

                      )
  )

  (:action nop
   :parameters ()
   :precondition ()
   :effect       (and)
  )

 ;------------------------------------------------------------------
 ;                               Methods
 ;------------------------------------------------------------------

 ;------------------------------------------------------------------
 ;                              Go somewhere
 ;------------------------------------------------------------------
 (:method goto
         :parameters   (?p - player ?from - location ?to - location)
         :expansion    (     (tag t1 (nop))
                       )
         :constraints  (and  (before (and
                             (= ?from ?to)
                             (at_player ?p ?from)
                             )
                         t1)
                       )
 )


  (:method goto
          :parameters   (?p - player ?from - location ?to - location)
          :expansion    (     (tag t1 (move ?p ?from ?mid ?dir))
                              (tag t2 (goto ?p ?mid ?to))
                        )
          :constraints  (and  (before (and
                              (not (= ?from ?to))
                              (not (= ?from ?mid))
                              (at_player ?p ?from)
                              (clear ?to)
                              (clear ?mid)
                              (movedir ?from ?mid ?dir)
                              )
                          t1)
                        )
  )

 ;------------------------------------------------------------------
 ;                     Push a stone somewhere
 ;------------------------------------------------------------------

 (:method get_stone_to
         :parameters   (?p - player ?s - stone ?from_stone ?to_stone - location)
         :expansion    (     (tag t1 (goto ?p ?from_player ?push_pos))
                             (tag t2 (push-to-nongoal ?p ?s ?push_pos ?from_stone ?to_stone ?dir_push))
                       )
         :constraints  (and  (before (and
                             (is_nongoal ?to_stone)

                             (at_stone ?s ?from_stone)
                             (at_player ?p ?from_player)
                             (movedir ?from_stone ?to_stone ?dir_stone)
                             (movedir ?push_pos ?from_stone ?dir_push)
                             (not (= ?from_stone ?to_stone))
                             (not (= ?from_stone ?push_pos))
                             (not (= ?to_stone ?push_pos))
                             (not (= ?from_player ?to_stone))
                             (not (= ?from_player ?from_stone))
                             (clear ?to_stone)
                             (clear ?push_pos)
                             )
                         t1)
                       )
 )

  (:method get_stone_to
          :parameters   (?p - player ?s - stone ?from_stone ?to_stone - location)
          :expansion    (     (tag t1 (goto ?p ?from_player ?push_pos))
                              (tag t2 (push-to-goal ?p ?s ?push_pos ?from_stone ?to_stone ?dir_push))
                        )
          :constraints  (and  (before (and
                              (is_goal ?to_stone)

                              (at_stone ?s ?from_stone)
                              (at_player ?p ?from_player)
                              (movedir ?from_stone ?to_stone ?dir_stone)
                              (movedir ?push_pos ?from_stone ?dir_push)
                              (not (= ?from_stone ?to_stone))
                              (not (= ?from_stone ?push_pos))
                              (not (= ?to_stone ?push_pos))
                              (not (= ?from_player ?to_stone))
                              (not (= ?from_player ?from_stone))
                              (clear ?to_stone)
                              (clear ?push_pos)
                              )
                          t1)
                        )
  )

 (:method get_stone_to
         :parameters   (?p - player ?s - stone ?from_stone ?to_stone - location)
         :expansion    (     (tag t1 (get_stone_to ?p ?s ?from_stone ?mid))
                             (tag t2 (get_stone_to ?p ?s ?mid ?to_stone))
                       )
         :constraints  (and  (before (and
                             (not (= ?from_stone ?mid))
                             (not (= ?mid ?to_stone))
                             (not (= ?from_stone ?to_stone))
                             (movedir ?from_stone ?mid ?whatever_dir)
                             )
                         t1)
                       )
 )

 ;------------------------------------------------------------------
 ;                      Move a stone to a goal position
 ;------------------------------------------------------------------

  (:method move_stone_to_goal
          :parameters   (?s - stone)
          :expansion    (     (tag t1 (get_stone_to ?p ?s ?from_stone ?to_stone))
                        )
          :constraints  (and  (before (and
                              (is_goal ?to_stone)
                              )
                          t1)
                        )
  )






)

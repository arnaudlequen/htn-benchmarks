;; #####
;; #   ##
;; # $  #
;; ## $ ####
;;  ###@.  #
;;   #  .# #
;;   #     #
;;   #######

(define (problem p012_microban_sequential)
  (:domain sokoban_sequential)
  (:requirements :typing :equality :htn :negative-preconditions)
  (:objects
    down - direction
    left - direction
    right - direction
    up - direction
    player01 - player
    stone01 - stone
    pos11 - location
    pos12 - location
    pos13 - location
    pos14 - location
    pos21 - location
    pos22 - location
    pos23 - location
    pos24 - location
    pos31 - location
    pos32 - location
    pos33 - location
    pos34 - location
    pos41 - location
    pos42 - location
    pos43 - location
    pos44 - location
  )
  (:init
    (movedir pos11 pos12 down)
(movedir pos12 pos13 down)
(movedir pos13 pos14 down)
(movedir pos21 pos22 down)
(movedir pos22 pos23 down)
(movedir pos23 pos24 down)
(movedir pos31 pos32 down)
(movedir pos32 pos33 down)
(movedir pos33 pos34 down)
(movedir pos41 pos42 down)
(movedir pos42 pos43 down)
(movedir pos43 pos44 down)
(movedir pos12 pos11 up)
(movedir pos13 pos12 up)
(movedir pos14 pos13 up)
(movedir pos22 pos21 up)
(movedir pos23 pos22 up)
(movedir pos24 pos23 up)
(movedir pos32 pos31 up)
(movedir pos33 pos32 up)
(movedir pos34 pos33 up)
(movedir pos42 pos41 up)
(movedir pos43 pos42 up)
(movedir pos44 pos43 up)
(movedir pos11 pos21 right)
(movedir pos12 pos22 right)
(movedir pos13 pos23 right)
(movedir pos14 pos24 right)
(movedir pos21 pos31 right)
(movedir pos22 pos32 right)
(movedir pos23 pos33 right)
(movedir pos24 pos34 right)
(movedir pos31 pos41 right)
(movedir pos32 pos42 right)
(movedir pos33 pos43 right)
(movedir pos34 pos44 right)
(movedir pos21 pos11 left)
(movedir pos22 pos12 left)
(movedir pos23 pos13 left)
(movedir pos24 pos14 left)
(movedir pos31 pos21 left)
(movedir pos32 pos22 left)
(movedir pos33 pos23 left)
(movedir pos34 pos24 left)
(movedir pos41 pos31 left)
(movedir pos42 pos32 left)
(movedir pos43 pos33 left)
(movedir pos44 pos34 left)
(is_nongoal pos11)
(is_nongoal pos12)
(is_nongoal pos13)
(clear pos13)
(is_nongoal pos14)
(clear pos14)
(is_nongoal pos21)
(clear pos21)
(is_nongoal pos22)
(clear pos22)
(is_nongoal pos23)
(clear pos23)
(is_nongoal pos24)
(clear pos24)
(is_nongoal pos31)
(clear pos31)
(is_nongoal pos32)
(clear pos32)
(is_nongoal pos33)
(clear pos33)
(is_nongoal pos34)
(clear pos34)
(is_nongoal pos41)
(clear pos41)
(is_nongoal pos42)
(clear pos42)
(is_nongoal pos43)
(clear pos43)
(is_nongoal pos44)
(clear pos44)

    (at_player player01 pos11)
    (at_stone stone01 pos12)


)

(:goal
		:tasks	    (

                     (tag t1 (push-to-nongoal player01 stone01 pos11 pos12 pos13 down))
		            )
		:constraints(and  (after (and

					            ) t1)
		            )
)

)

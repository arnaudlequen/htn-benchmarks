;; #####
;; #   ##
;; # $  #
;; ## $ ####
;;  ###@.  #
;;   #  .# #
;;   #     #
;;   #######

(define (problem p012_microban_sequential)
  (:domain sokoban_sequential)
  (:requirements :typing :equality :htn :negative-preconditions)
  (:objects
    down - direction
    left - direction
    right - direction
    up - direction
    player01 - player
    pos11 - location
    pos12 - location
    pos13 - location
    pos14 - location
    pos15 - location
    pos16 - location
    pos17 - location
    pos18 - location
    pos21 - location
    pos22 - location
    pos23 - location
    pos24 - location
    pos25 - location
    pos26 - location
    pos27 - location
    pos28 - location
    pos31 - location
    pos32 - location
    pos33 - location
    pos34 - location
    pos35 - location
    pos36 - location
    pos37 - location
    pos38 - location
    pos41 - location
    pos42 - location
    pos43 - location
    pos44 - location
    pos45 - location
    pos46 - location
    pos47 - location
    pos48 - location
    pos51 - location
    pos52 - location
    pos53 - location
    pos54 - location
    pos55 - location
    pos56 - location
    pos57 - location
    pos58 - location
    pos61 - location
    pos62 - location
    pos63 - location
    pos64 - location
    pos65 - location
    pos66 - location
    pos67 - location
    pos68 - location
    pos71 - location
    pos72 - location
    pos73 - location
    pos74 - location
    pos75 - location
    pos76 - location
    pos77 - location
    pos78 - location
    pos81 - location
    pos82 - location
    pos83 - location
    pos84 - location
    pos85 - location
    pos86 - location
    pos87 - location
    pos88 - location
    pos91 - location
    pos92 - location
    pos93 - location
    pos94 - location
    pos95 - location
    pos96 - location
    pos97 - location
    pos98 - location
    stone01 - stone
    stone02 - stone
  )
  (:init
    (is_goal pos65)
    (is_goal pos66)
    (is_nongoal pos11)
    (is_nongoal pos12)
    (is_nongoal pos13)
    (is_nongoal pos14)
    (is_nongoal pos15)
    (is_nongoal pos16)
    (is_nongoal pos17)
    (is_nongoal pos18)
    (is_nongoal pos21)
    (is_nongoal pos22)
    (is_nongoal pos23)
    (is_nongoal pos24)
    (is_nongoal pos25)
    (is_nongoal pos26)
    (is_nongoal pos27)
    (is_nongoal pos28)
    (is_nongoal pos31)
    (is_nongoal pos32)
    (is_nongoal pos33)
    (is_nongoal pos34)
    (is_nongoal pos35)
    (is_nongoal pos36)
    (is_nongoal pos37)
    (is_nongoal pos38)
    (is_nongoal pos41)
    (is_nongoal pos42)
    (is_nongoal pos43)
    (is_nongoal pos44)
    (is_nongoal pos45)
    (is_nongoal pos46)
    (is_nongoal pos47)
    (is_nongoal pos48)
    (is_nongoal pos51)
    (is_nongoal pos52)
    (is_nongoal pos53)
    (is_nongoal pos54)
    (is_nongoal pos55)
    (is_nongoal pos56)
    (is_nongoal pos57)
    (is_nongoal pos58)
    (is_nongoal pos61)
    (is_nongoal pos62)
    (is_nongoal pos63)
    (is_nongoal pos64)
    (is_nongoal pos67)
    (is_nongoal pos68)
    (is_nongoal pos71)
    (is_nongoal pos72)
    (is_nongoal pos73)
    (is_nongoal pos74)
    (is_nongoal pos75)
    (is_nongoal pos76)
    (is_nongoal pos77)
    (is_nongoal pos78)
    (is_nongoal pos81)
    (is_nongoal pos82)
    (is_nongoal pos83)
    (is_nongoal pos84)
    (is_nongoal pos85)
    (is_nongoal pos86)
    (is_nongoal pos87)
    (is_nongoal pos88)
    (is_nongoal pos91)
    (is_nongoal pos92)
    (is_nongoal pos93)
    (is_nongoal pos94)
    (is_nongoal pos95)
    (is_nongoal pos96)
    (is_nongoal pos97)
    (is_nongoal pos98)
    (movedir pos15 pos16 down)
    (movedir pos16 pos15 up)
    (movedir pos16 pos17 down)
    (movedir pos16 pos26 right)
    (movedir pos17 pos16 up)
    (movedir pos17 pos18 down)
    (movedir pos17 pos27 right)
    (movedir pos18 pos17 up)
    (movedir pos18 pos28 right)
    (movedir pos22 pos23 down)
    (movedir pos22 pos32 right)
    (movedir pos23 pos22 up)
    (movedir pos23 pos33 right)
    (movedir pos26 pos16 left)
    (movedir pos26 pos27 down)
    (movedir pos27 pos17 left)
    (movedir pos27 pos26 up)
    (movedir pos27 pos28 down)
    (movedir pos28 pos18 left)
    (movedir pos28 pos27 up)
    (movedir pos32 pos22 left)
    (movedir pos32 pos33 down)
    (movedir pos32 pos42 right)
    (movedir pos33 pos23 left)
    (movedir pos33 pos32 up)
    (movedir pos33 pos34 down)
    (movedir pos33 pos43 right)
    (movedir pos34 pos33 up)
    (movedir pos34 pos44 right)
    (movedir pos42 pos32 left)
    (movedir pos42 pos43 down)
    (movedir pos43 pos33 left)
    (movedir pos43 pos42 up)
    (movedir pos43 pos44 down)
    (movedir pos43 pos53 right)
    (movedir pos44 pos34 left)
    (movedir pos44 pos43 up)
    (movedir pos44 pos54 right)
    (movedir pos46 pos47 down)
    (movedir pos46 pos56 right)
    (movedir pos47 pos46 up)
    (movedir pos47 pos57 right)
    (movedir pos53 pos43 left)
    (movedir pos53 pos54 down)
    (movedir pos54 pos44 left)
    (movedir pos54 pos53 up)
    (movedir pos54 pos55 down)
    (movedir pos55 pos54 up)
    (movedir pos55 pos56 down)
    (movedir pos55 pos65 right)
    (movedir pos56 pos46 left)
    (movedir pos56 pos55 up)
    (movedir pos56 pos57 down)
    (movedir pos56 pos66 right)
    (movedir pos57 pos47 left)
    (movedir pos57 pos56 up)
    (movedir pos57 pos67 right)
    (movedir pos61 pos71 right)
    (movedir pos65 pos55 left)
    (movedir pos65 pos66 down)
    (movedir pos65 pos75 right)
    (movedir pos66 pos56 left)
    (movedir pos66 pos65 up)
    (movedir pos66 pos67 down)
    (movedir pos67 pos57 left)
    (movedir pos67 pos66 up)
    (movedir pos67 pos77 right)
    (movedir pos71 pos61 left)
    (movedir pos71 pos72 down)
    (movedir pos71 pos81 right)
    (movedir pos72 pos71 up)
    (movedir pos72 pos73 down)
    (movedir pos72 pos82 right)
    (movedir pos73 pos72 up)
    (movedir pos73 pos83 right)
    (movedir pos75 pos65 left)
    (movedir pos75 pos85 right)
    (movedir pos77 pos67 left)
    (movedir pos77 pos87 right)
    (movedir pos81 pos71 left)
    (movedir pos81 pos82 down)
    (movedir pos81 pos91 right)
    (movedir pos82 pos72 left)
    (movedir pos82 pos81 up)
    (movedir pos82 pos83 down)
    (movedir pos82 pos92 right)
    (movedir pos83 pos73 left)
    (movedir pos83 pos82 up)
    (movedir pos83 pos93 right)
    (movedir pos85 pos75 left)
    (movedir pos85 pos86 down)
    (movedir pos86 pos85 up)
    (movedir pos86 pos87 down)
    (movedir pos87 pos77 left)
    (movedir pos87 pos86 up)
    (movedir pos91 pos81 left)
    (movedir pos91 pos92 down)
    (movedir pos92 pos82 left)
    (movedir pos92 pos91 up)
    (movedir pos92 pos93 down)
    (movedir pos93 pos83 left)
    (movedir pos93 pos92 up)
    (at_player player01 pos55)
    (at_stone stone01 pos33)
    (at_stone stone02 pos44)
    (clear pos15)
    (clear pos16)
    (clear pos17)
    (clear pos18)
    (clear pos22)
    (clear pos23)
    (clear pos26)
    (clear pos27)
    (clear pos28)
    (clear pos32)
    (clear pos34)
    (clear pos42)
    (clear pos43)
    (clear pos46)
    (clear pos47)
    (clear pos53)
    (clear pos54)
    (clear pos56)
    (clear pos57)
    (clear pos61)
    (clear pos65)
    (clear pos66)
    (clear pos67)
    (clear pos71)
    (clear pos72)
    (clear pos73)
    (clear pos75)
    (clear pos77)
    (clear pos81)
    (clear pos82)
    (clear pos83)
    (clear pos85)
    (clear pos86)
    (clear pos87)
    (clear pos91)
    (clear pos92)
    (clear pos93)
)

(:goal
		:tasks	    (
					           (tag t1 (move player01 pos55 pos45 up))
                     (tag t2 (push-to-nongoal player01 stone02 pos45 pos44 pos43 left))
		            )
		:constraints(and  (after (and

					            ) t1)
		            )
)

)

#!/bin/bash

rm selection/htn_pb*.pddl 2>/dev/null

for i in `seq 1 5 150`; do
    rand=`shuf -i 0-4 -n 1`
    num=$(($i + $rand))
    
    file1=htn_pb$num.pddl
    file2=htn_pb0$num.pddl
    
    cp $file1 selection/ 2>/dev/null
    cp $file2 selection/ 2>/dev/null
done

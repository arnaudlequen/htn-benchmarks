(define (domain parking)
 (:requirements :strips :typing :htn :equality :negative-preconditions)
 (:types car curb)
 (:predicates
    (at-curb ?car - car)
    (at-curb-num ?car - car ?curb - curb)
    (behind-car ?car ?front-car - car)
    (car-clear ?car - car)
    (curb-clear ?curb - curb)
 )



	(:action move-curb-to-curb
		:parameters (?car - car ?curbsrc ?curbdest - curb)
		:precondition (and
			(car-clear ?car)
			(curb-clear ?curbdest)
			(at-curb-num ?car ?curbsrc)
		)
		:effect (and
			(not (curb-clear ?curbdest))
			(curb-clear ?curbsrc)
			(at-curb-num ?car ?curbdest)
			(not (at-curb-num ?car ?curbsrc))

		)
	)

	(:action move-curb-to-car
		:parameters (?car - car ?curbsrc - curb ?cardest - car)
		:precondition (and
			(car-clear ?car)
			(car-clear ?cardest)
			(at-curb-num ?car ?curbsrc)
			(at-curb ?cardest)
		)
		:effect (and
			(not (car-clear ?cardest))
			(curb-clear ?curbsrc)
			(behind-car ?car ?cardest)
			(not (at-curb-num ?car ?curbsrc))
			(not (at-curb ?car))

		)
	)

	(:action move-car-to-curb
		:parameters (?car - car ?carsrc - car ?curbdest - curb)
		:precondition (and
			(car-clear ?car)
			(curb-clear ?curbdest)
			(behind-car ?car ?carsrc)
		)
		:effect (and
			(not (curb-clear ?curbdest))
			(car-clear ?carsrc)
			(at-curb-num ?car ?curbdest)
			(not (behind-car ?car ?carsrc))
			(at-curb ?car)

		)
	)

	(:action move-car-to-car
		:parameters (?car - car ?carsrc - car ?cardest - car)
		:precondition (and
			(car-clear ?car)
			(car-clear ?cardest)
			(behind-car ?car ?carsrc)
			(at-curb ?cardest)
		)
		:effect (and
			(not (car-clear ?cardest))
			(car-clear ?carsrc)
			(behind-car ?car ?cardest)
			(not (behind-car ?car ?carsrc))

		)
	)

  (:action nop
         :parameters    ()
         :precondition  ()
         :effect        (and)
)

  ;------------------------------------------------------------------
  ;                               Methods
  ;------------------------------------------------------------------
  ;                 Flip two cars in the same spot
  ;------------------------------------------------------------------
  (:method flip
          :parameters   (?curb - curb)
          :expansion    (     (tag t1 (move-car-to-car ?cartop ?carbot ?car1))
                              (tag t2 (move-curb-to-car ?carbot ?curb ?car2))
                              (tag t3 (move-car-to-curb ?cartop ?car1 ?curb))
                              (tag t4 (move-car-to-car ?carbot ?car2 ?cartop))
                        )
          :constraints  (and  (before (and
                              (car-clear ?car1)
                              (car-clear ?car2)
                              (not (= ?car1 ?car2))
                              (at-curb-num ?carbot ?curb)
                              (behind-car ?cartop ?carbot)
                              ;(behind-car ?cartop ?carbot)
                              ;(at-curb-num ?carbot ?curb)
                              )
                          t1)
                        )
  )
  ;(:method flip
  ;        :parameters   (?curb - curb)
  ;        :expansion    (     (tag t1 (move-curb-to-car ?car1 ?curb1 ?car2))
  ;                            (tag t2 (flip ?curb))
  ;                            (tag t3 (move-car-to-curb ?car1 ?car2 ?curb1))
  ;                      )
  ;        :constraints  (and  (before (and
  ;                            (car-clear ?car1)
  ;                            (car-clear ?car2)
  ;                            (not (= ?car1 ?car2))
  ;                            (at-curb-num ?car1 ?curb1)
  ;                            (not (= ?curb1 ?curb))
  ;                            )
  ;                        t1)
  ;                      )
  ;)

  ;This weird method, instead of reducing to the previous case,
  ;is a way to divide by 2.5 the amount of methods created, although it creates
  ;more nodes to explore
  ;It also has some side effects
  (:method flip
          :parameters   (?curb - curb)
          :expansion    (     (tag t1 (transfert ?curb ?freecurb))
                              (tag t2 (transfert ?transcurb ?curb))
                              (tag t3 (transfert ?freecurb ?transcurb))
                              (tag t4 (transfert ?curb ?freecurb))
                              (tag t5 (transfert ?transcurb ?curb))
                              (tag t6 (transfert ?freecurb ?transcurb))
                        )
          :constraints  (and  (before (and
                              (curb-clear ?freecurb)
                              (not (curb-clear ?transcurb))
                              )
                          t1)
                        )
  )

  ; Make sure a whole spot is free
  (:method free_curb
          :parameters   ()
          :expansion    (     (tag t1 (move-curb-to-car ?car1 ?curb ?car2))
                        )
          :constraints  (and  (before (and
                              (at-curb ?car1)
                              (at-curb-num ?car1 ?curb)
                              (car-clear ?car1)
                              (at-curb ?car2)
                              (car-clear ?car2)
                              )
                          t1)
                        )
  )

  (:method free_curb
          :parameters   ()
          :expansion    (     (tag t1 (nop))
                        )
          :constraints  (and  (before (and
                              (curb-clear ?curb)
                              )
                          t1)
                        )
  )


  ; Move the whole curb elsewhere
  (:method transfert_elsewhere
          :parameters   (?curb - curb)
          :expansion    (     (tag t1 (transfert ?curb ?freecurb))
                        )
          :constraints  (and  (before (and
                              (curb-clear ?freecurb)
                              )
                          t1)
                        )
  )

  ;------------------------------------------------------------------
  ;             Transfert two cars in the same spot to another
  ;                   Cars are flipped along the way
  ;------------------------------------------------------------------
  (:method transfert
          :parameters   (?curbsrc ?curbdest - curb)
          :expansion    (     (tag t1 (move-car-to-curb ?car2 ?car1 ?curbdest))
                              (tag t2 (move-curb-to-car ?car1 ?curbsrc ?car2))
                        )
          :constraints  (and  (before (and
                              (curb-clear ?curbdest)
                              (car-clear ?car2)
                              (behind-car ?car2 ?car1)
                              (at-curb-num ?car1 ?curbsrc)
                              )
                          t1)
                        )
  )

  ;------------------------------------------------------------------
  ;                       Park a car on a curb
  ;------------------------------------------------------------------
  ; The wrong spot is free
  (:method park-curb
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (move-car-to-curb ?car2 ?car1 ?transcurb))
                              (tag t2 (move-curb-to-car ?car1 ?curb ?car2))
                              (tag t3 (park-curb-2 ?car ?curb))
                        )
          :constraints  (and  (before (and
                                (not (curb-clear ?curb))
                                (curb-clear ?transcurb)
                                (at-curb-num ?car1 ?curb)
                                (behind-car ?car2 ?car1)
                              )
                          t1)
                        )
  )

  ; Two half free spots
  (:method park-curb
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (move-curb-to-car ?car1 ?curb ?car2))
                              (tag t2 (park-curb-2 ?car ?curb))
                        )
          :constraints  (and  (before (and
                                (not (curb-clear ?curb))
                                (car-clear ?car1)
                                (car-clear ?car2)
                                (at-curb-num ?car1 ?curb)
                              )
                          t1)
                        )
  )

  ; The spot is already free
  (:method park-curb
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (park-curb-2 ?car ?curb))
                        )
          :constraints  (and  (before (and
                                (curb-clear ?curb)
                              )
                          t1)
                        )
  )


  ;
  ;
  ; UTILISER UNE FONCTION QUI LIBERE LA PREMIERE PLACE D'UN EMPLACEMENT
  ;
  ;

  ; Sometimes, we only have to flip cars because it's already on spot
  (:method park-curb
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (flip ?curb))
                        )
          :constraints  (and  (before (and
                                (car-clear ?car)
                                (behind-car ?car ?car2)
                                (at-curb-num ?car2 ?curb)
                              )
                          t1)
                        )
  )

  ; If everything is fine already
  (:method park-curb
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (nop))
                        )
          :constraints  (and  (before (and
                                (at-curb ?car)
                                (at-curb-num ?car ?curb)
                              )
                          t1)
                        )
  )



  ;Make sure the car is free for the next step
  (:method park-curb-2
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (park-curb-3 ?car ?curb))
                        )
          :constraints  (and  (before (and
                                (car-clear ?car)
                              )
                          t1)
                        )
  )



;
;
; TRANSFÉRER LA PILE VERS UN AUTRE ENDROIT
; OU/ET
; UTILISER UNE FONCTION QUI PERMET DE LIBÉRER UN EMPLACEMENT COMPLET EN EMPILANT DEUX VOITURES
;
;
  (:method park-curb-2
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (free_curb))
                              (tag t2 (transfert_elsewhere ?curb))
                              (tag t3 (park-curb-3 ?car ?curb))
                        )
          :constraints  (and  (before (and
                                (not (car-clear ?car))
                                (at-curb-num ?car ?currcurb)
                                (behind-car ?car2 ?car)
                              )
                          t1)
                        )
  )


  ;Move the car to the newly freed curb
  (:method park-curb-3
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (move-car-to-curb ?car ?currcar ?curb))
                        )
          :constraints  (and  (before (and
                                (behind-car ?car ?currcar)
                                (not (at-curb ?car))
                                (car-clear ?car)
                                (curb-clear ?curb)
                              )
                          t1)
                        )
  )

  (:method park-curb-3
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (move-curb-to-curb ?car ?currcurb ?curb))
                        )
          :constraints  (and  (before (and
                                (at-curb ?car)
                                (car-clear ?car)
                                (at-curb-num ?car ?currcurb)
                              )
                          t1)
                        )
  )

  (:method park-curb-3
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (nop))
                        )
          :constraints  (and  (before (and
                                (at-curb ?car)
                                (at-curb-num ?car ?curb)
                              )
                          t1)
                        )
  )

  ;------------------------------------------------------------------
  ;                       Park a car on another car
  ;------------------------------------------------------------------
  ; Should only be used if every car that is supposed to be parked at a curb
  ; is already parked

  ; Make sure the spot is free
  ; The spot is not free
  ; It doesn't matter if the other free spot is the one with ?car
  (:method park-car
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (move-car-to-car ?car2 ?destcar ?transcar))
                              (tag t2 (park-car-2 ?car ?destcar))
                        )
          :constraints  (and  (before (and
                                (not (car-clear ?destcar))
                                (behind-car ?car2 ?destcar)
                                (car-clear ?car2)
                                (car-clear ?transcar)
                                (at-curb ?transcar)
                              )
                          t1)
                        )
  )

  ; A whole curb is free
  (:method park-car
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (move-car-to-curb ?car2 ?destcar ?transcurb))
                              (tag t2 (park-car-2 ?car ?destcar))
                        )
          :constraints  (and  (before (and
                                (not (car-clear ?destcar))
                                (curb-clear ?transcurb)
                                (behind-car ?car2 ?destcar)
                                (car-clear ?car2)
                              )
                          t1)
                        )
  )

  ; The spot is already free
  (:method park-car
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (park-car-2 ?car ?destcar))
                        )
          :constraints  (and  (before (and
                                (at-curb-num ?car1 ?curb)
                                (car-clear ?destcar)
                              )
                          t1)
                        )
  )

  ; If there is nothing to do
  (:method park-car
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (nop))
                        )
          :constraints  (and  (before (and
                                (behind-car ?car ?destcar)
                              )
                          t1)
                        )
  )


  ; Make sure the car is free for the next step
  (:method park-car-2
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (park-car-3 ?car ?destcar))
                        )
          :constraints  (and  (before (and
                                (car-clear ?car)
                              )
                          t1)
                        )
  )

  (:method park-car-2
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (flip ?currcurb))
                              (tag t2 (park-car-3 ?car ?destcar))
                        )
          :constraints  (and  (before (and
                                (not (car-clear ?car))
                                (at-curb-num ?car ?currcurb)
                                (behind-car ?car2 ?car)
                              )
                          t1)
                        )
  )


  ;Move the car to the newly freed car
  (:method park-car-3
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (move-car-to-car ?car ?currcar ?destcar))
                        )
          :constraints  (and  (before (and
                                (behind-car ?car ?currcar)
                                (not (at-curb ?car))
                                (car-clear ?car)
                                (car-clear ?destcar)
                              )
                          t1)
                        )
  )

  (:method park-car-3
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (move-curb-to-car ?car ?currcurb ?destcar))
                        )
          :constraints  (and  (before (and
                                (at-curb ?car)
                                (car-clear ?car)
                                (at-curb-num ?car ?currcurb)
                              )
                          t1)
                        )
  )

  (:method park-car-3
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (nop))
                        )
          :constraints  (and  (before (and
                                (behind-car ?car ?destcar)
                              )
                          t1)
                        )
  )

)

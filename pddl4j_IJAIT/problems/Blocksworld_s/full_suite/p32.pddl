(define (problem BW-rand-36)
(:domain blocksworld)
(:requirements :strips :typing :negative-preconditions :htn :equality)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20 b21 b22 b23 b24 b25 b26 b27 b28 b29 b30 b31 b32 b33 b34 b35 b36 - block)
(:init
(handempty)
(on b1 b16)
(on b2 b25)
(ontable b3)
(ontable b4)
(on b5 b30)
(on b6 b33)
(on b7 b34)
(on b8 b6)
(on b9 b22)
(on b10 b24)
(on b11 b35)
(on b12 b31)
(on b13 b8)
(ontable b14)
(on b15 b13)
(ontable b16)
(on b17 b36)
(ontable b18)
(on b19 b15)
(ontable b20)
(on b21 b28)
(on b22 b26)
(on b23 b11)
(on b24 b17)
(on b25 b20)
(on b26 b32)
(on b27 b12)
(on b28 b2)
(on b29 b14)
(on b30 b23)
(on b31 b1)
(on b32 b19)
(on b33 b29)
(on b34 b9)
(on b35 b18)
(on b36 b21)
(clear b3)
(clear b4)
(clear b5)
(clear b7)
(clear b10)
(clear b27)
)
(:goal :tasks (
(tag t1 (do_put_on b3 b31))
(tag t2 (do_put_on b10 b3))
(tag t3 (do_put_on b35 b10))
(tag t4 (do_put_on b17 b35))
(tag t5 (do_put_on b26 b17))
(tag t6 (do_put_on b13 b26))
(tag t7 (do_put_on b12 b13))
(tag t8 (do_put_on b8 b12))
(tag t9 (do_put_on b9 b8))
(tag t10 (do_put_on b33 b9))
(tag t11 (do_put_on b1 b33))
(tag t12 (do_put_on b25 b1))
(tag t13 (do_put_on b34 b25))
(tag t14 (do_put_on b3 b31))
(tag t15 (do_put_on b10 b3))
(tag t16 (do_put_on b35 b10))
(tag t17 (do_put_on b17 b35))
(tag t18 (do_put_on b26 b17))
(tag t19 (do_put_on b13 b26))
(tag t20 (do_put_on b12 b13))
(tag t21 (do_put_on b8 b12))
(tag t22 (do_put_on b9 b8))
(tag t23 (do_put_on b33 b9))
(tag t24 (do_put_on b1 b33))
(tag t25 (do_put_on b25 b1))
(tag t26 (do_put_on b34 b25))
(tag t27 (do_put_on b6 b34))
(tag t28 (do_put_on b2 b6))
(tag t29 (do_put_on b32 b2))
(tag t30 (do_put_on b7 b32))
(tag t31 (do_put_on b20 b7))
(tag t32 (do_put_on b36 b20))
(tag t33 (do_put_on b5 b36))
(tag t34 (do_put_on b28 b5))
(tag t35 (do_put_on b30 b28))
(tag t36 (do_put_on b18 b30))
(tag t37 (do_put_on b15 b18))
(tag t38 (do_put_on b22 b15))
(tag t39 (do_put_on b11 b22))
(tag t40 (do_put_on b29 b11))
(tag t41 (do_put_on b4 b29))
(tag t42 (do_put_on b21 b4))
(tag t43 (do_put_on b14 b16))
(tag t44 (do_put_on b23 b14))
(tag t45 (do_put_on b24 b23))
) :constraints(and (after 
(and
(on b1 b33)
(on b2 b6)
(on b3 b31)
(on b4 b29)
(on b5 b36)
(on b6 b34)
(on b7 b32)
(on b8 b12)
(on b9 b8)
(on b10 b3)
(on b11 b22)
(on b12 b13)
(on b13 b26)
(on b14 b16)
(on b15 b18)
(on b17 b35)
(on b18 b30)
(on b20 b7)
(on b21 b4)
(on b22 b15)
(on b23 b14)
(on b24 b23)
(on b25 b1)
(on b26 b17)
(on b28 b5)
(on b29 b11)
(on b30 b28)
(on b32 b2)
(on b33 b9)
(on b34 b25)
(on b35 b10)
(on b36 b20)) t45)
)
)
)

(define (problem strips-sat-x-1)
(:domain satellite)
     (:requirements :strips :typing :htn :negative-preconditions :equality)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	instrument1 - instrument
	instrument2 - instrument
	instrument3 - instrument
	instrument4 - instrument
	satellite1 - satellite
	instrument5 - instrument
	instrument6 - instrument
	instrument7 - instrument
	instrument8 - instrument
	satellite2 - satellite
	instrument9 - instrument
	instrument10 - instrument
	instrument11 - instrument
	instrument12 - instrument
	satellite3 - satellite
	instrument13 - instrument
	instrument14 - instrument
	satellite4 - satellite
	instrument15 - instrument
	instrument16 - instrument
	instrument17 - instrument
	image3 - mode
	infrared4 - mode
	infrared2 - mode
	image5 - mode
	infrared1 - mode
	thermograph0 - mode
	Star1 - direction
	GroundStation4 - direction
	Star0 - direction
	GroundStation2 - direction
	Star3 - direction
	Star5 - direction
	Planet6 - direction
	Phenomenon7 - direction
	Star8 - direction
	Phenomenon9 - direction
	Planet10 - direction
	Phenomenon11 - direction
	Star12 - direction
	Star13 - direction
	Phenomenon14 - direction
	Star15 - direction
	Planet16 - direction
	Phenomenon17 - direction
	Phenomenon18 - direction
	Planet19 - direction
)
(:init
	(supports instrument0 infrared4)
	(supports instrument0 infrared1)
	(supports instrument0 thermograph0)
	(calibration_target instrument0 GroundStation4)
	(supports instrument1 infrared2)
	(calibration_target instrument1 GroundStation2)
	(supports instrument2 thermograph0)
	(calibration_target instrument2 GroundStation2)
	(supports instrument3 thermograph0)
	(supports instrument3 infrared1)
	(calibration_target instrument3 GroundStation4)
	(supports instrument4 thermograph0)
	(calibration_target instrument4 Star3)
	(on_board instrument0 satellite0)
	(on_board instrument1 satellite0)
	(on_board instrument2 satellite0)
	(on_board instrument3 satellite0)
	(on_board instrument4 satellite0)
	(power_avail satellite0)
	(pointing satellite0 Phenomenon7)
	(supports instrument5 image3)
	(supports instrument5 infrared2)
	(calibration_target instrument5 GroundStation2)
	(supports instrument6 infrared1)
	(supports instrument6 image5)
	(supports instrument6 thermograph0)
	(calibration_target instrument6 GroundStation2)
	(supports instrument7 infrared1)
	(supports instrument7 infrared4)
	(supports instrument7 thermograph0)
	(calibration_target instrument7 Star1)
	(supports instrument8 thermograph0)
	(supports instrument8 image3)
	(calibration_target instrument8 Star3)
	(on_board instrument5 satellite1)
	(on_board instrument6 satellite1)
	(on_board instrument7 satellite1)
	(on_board instrument8 satellite1)
	(power_avail satellite1)
	(pointing satellite1 Planet16)
	(supports instrument9 infrared1)
	(supports instrument9 image5)
	(calibration_target instrument9 GroundStation2)
	(supports instrument10 infrared1)
	(supports instrument10 image5)
	(calibration_target instrument10 GroundStation4)
	(supports instrument11 infrared4)
	(supports instrument11 image3)
	(calibration_target instrument11 Star3)
	(supports instrument12 infrared2)
	(supports instrument12 image5)
	(calibration_target instrument12 GroundStation2)
	(on_board instrument9 satellite2)
	(on_board instrument10 satellite2)
	(on_board instrument11 satellite2)
	(on_board instrument12 satellite2)
	(power_avail satellite2)
	(pointing satellite2 Star0)
	(supports instrument13 infrared4)
	(supports instrument13 image3)
	(supports instrument13 thermograph0)
	(calibration_target instrument13 Star0)
	(supports instrument14 infrared1)
	(supports instrument14 image5)
	(supports instrument14 infrared2)
	(calibration_target instrument14 GroundStation2)
	(on_board instrument13 satellite3)
	(on_board instrument14 satellite3)
	(power_avail satellite3)
	(pointing satellite3 Star3)
	(supports instrument15 image5)
	(calibration_target instrument15 Star3)
	(supports instrument16 thermograph0)
	(supports instrument16 image5)
	(supports instrument16 infrared1)
	(calibration_target instrument16 GroundStation2)
	(supports instrument17 thermograph0)
	(supports instrument17 infrared1)
	(supports instrument17 image5)
	(calibration_target instrument17 Star3)
	(on_board instrument15 satellite4)
	(on_board instrument16 satellite4)
	(on_board instrument17 satellite4)
	(power_avail satellite4)
	(pointing satellite4 Star3)
)
(:goal
  :tasks (
             (tag t1 (do_mission Phenomenon11 infrared1))
             (tag t2 (do_mission Phenomenon14 image5))
             (tag t3 (do_mission Phenomenon14 infrared2))
             (tag t4 (do_mission Phenomenon17 image3))
             (tag t5 (do_mission Phenomenon18 infrared2))
             (tag t6 (do_mission Phenomenon18 infrared4))
             (tag t7 (do_mission Phenomenon7 image3))
             (tag t8 (do_mission Phenomenon9 image5))
             (tag t9 (do_mission Phenomenon9 infrared4))
             (tag t10 (do_mission Planet10 image5))
             (tag t11 (do_mission Planet10 infrared2))
             (tag t12 (do_mission Planet16 image5))
             (tag t13 (do_mission Planet6 infrared1))
             (tag t14 (do_mission Planet6 infrared4))
             (tag t15 (do_mission Star12 thermograph0))
             (tag t16 (do_mission Star13 infrared4))
             (tag t17 (do_mission Star15 image3))
             (tag t18 (do_mission Star15 thermograph0))
             (tag t19 (do_mission Star5 image3))
             (tag t20 (do_mission Star5 image5))
             (tag t21 (do_mission Star8 infrared1))
             (tag t22 (do_mission Star8 infrared4))
             (tag t23 (do_turning satellite4 Star1))
         )
  :constraints
         (and (after (and
	(pointing satellite4 Star1)
	(have_image Star5 image5)
	(have_image Star5 image3)
	(have_image Planet6 infrared1)
	(have_image Planet6 infrared4)
	(have_image Phenomenon7 image3)
	(have_image Star8 infrared4)
	(have_image Star8 infrared1)
	(have_image Phenomenon9 image5)
	(have_image Phenomenon9 infrared4)
	(have_image Planet10 infrared2)
	(have_image Planet10 image5)
	(have_image Phenomenon11 infrared1)
	(have_image Star12 thermograph0)
	(have_image Star13 infrared4)
	(have_image Phenomenon14 infrared2)
	(have_image Phenomenon14 image5)
	(have_image Star15 image3)
	(have_image Star15 thermograph0)
	(have_image Planet16 image5)
	(have_image Phenomenon17 image3)
	(have_image Phenomenon18 infrared2)
	(have_image Phenomenon18 infrared4)
         ) t23))
)
)

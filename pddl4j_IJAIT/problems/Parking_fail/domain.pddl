(define (domain parking)
 (:requirements :strips :typing :htn :equality :negative-preconditions)
 (:types car curb)
 (:predicates
    (at-curb ?car - car)
    (at-curb-num ?car - car ?curb - curb)
    (behind-car ?car ?front-car - car)
    (car-clear ?car - car)
    (curb-clear ?curb - curb)
 )



	(:action move-curb-to-curb
		:parameters (?car - car ?curbsrc ?curbdest - curb)
		:precondition (and
			(car-clear ?car)
			(curb-clear ?curbdest)
			(at-curb-num ?car ?curbsrc)
		)
		:effect (and
			(not (curb-clear ?curbdest))
			(curb-clear ?curbsrc)
			(at-curb-num ?car ?curbdest)
			(not (at-curb-num ?car ?curbsrc))

		)
	)

	(:action move-curb-to-car
		:parameters (?car - car ?curbsrc - curb ?cardest - car)
		:precondition (and
			(car-clear ?car)
			(car-clear ?cardest)
			(at-curb-num ?car ?curbsrc)
			(at-curb ?cardest)
		)
		:effect (and
			(not (car-clear ?cardest))
			(curb-clear ?curbsrc)
			(behind-car ?car ?cardest)
			(not (at-curb-num ?car ?curbsrc))
			(not (at-curb ?car))

		)
	)

	(:action move-car-to-curb
		:parameters (?car - car ?carsrc - car ?curbdest - curb)
		:precondition (and
			(car-clear ?car)
			(curb-clear ?curbdest)
			(behind-car ?car ?carsrc)
		)
		:effect (and
			(not (curb-clear ?curbdest))
			(car-clear ?carsrc)
			(at-curb-num ?car ?curbdest)
			(not (behind-car ?car ?carsrc))
			(at-curb ?car)

		)
	)

	(:action move-car-to-car
		:parameters (?car - car ?carsrc - car ?cardest - car)
		:precondition (and
			(car-clear ?car)
			(car-clear ?cardest)
			(behind-car ?car ?carsrc)
			(at-curb ?cardest)
		)
		:effect (and
			(not (car-clear ?cardest))
			(car-clear ?carsrc)
			(behind-car ?car ?cardest)
			(not (behind-car ?car ?carsrc))

		)
	)

  (:action nop
         :parameters    ()
         :precondition  ()
         :effect        (and)
)

  ;------------------------------------------------------------------
  ;                               Methods
  ;------------------------------------------------------------------
  ;             Transfert two cars in the same spot to another
  ;                   Cars are flipped along the way
  ;------------------------------------------------------------------
  (:method transfert
          :parameters   (?curbsrc ?curbdest - curb)
          :expansion    (     (tag t1 (move-car-to-curb ?car2 ?car1 ?curbdest))
                              (tag t2 (move-curb-to-car ?car1 ?curbsrc ?car2))
                        )
          :constraints  (and  (before (and
                              (curb-clear ?curbdest)
                              (car-clear ?car2)
                              (behind-car ?car2 ?car1)
                              (at-curb-num ?car1 ?curbsrc)
                              )
                          t1)
                        )
  )

  ;------------------------------------------------------------------
  ;                       Free a car at a curb
  ;------------------------------------------------------------------
  ; A whole spot is free
  (:method free-car
          :parameters   (?car - car)
          :expansion    (     (tag t1 (move-car-to-curb ?car2 ?car ?freecurb))
                        )
          :constraints  (and  (before (and
                                (behind-car ?car2 ?car)
                                (curb-clear ?freecurb)
                              )
                          t1)
                        )
  )

  ; Only a spot next to another car is free
  (:method free-car
          :parameters   (?car - car)
          :expansion    (     (tag t1 (move-car-to-car ?car2 ?car ?freecar))
                        )
          :constraints  (and  (before (and
                                (behind-car ?car2 ?car)
                                (car-clear ?freecar)
                                (at-curb ?freecar)
                              )
                          t1)
                        )
  )

  ;------------------------------------------------------------------
  ;                         Free a curb
  ;------------------------------------------------------------------
  ; If there are two cars at the spot
  ; Two half-spots are free
  (:method free-curb
          :parameters   (?curb - curb)
          :expansion    (     (tag t1 (free-car ?car))
                              (tag t2 (free-curb ?curb))
                        )
          :constraints  (and  (before (and
                                (at-curb-num ?car ?curb)
                              )
                          t1)
                        )
  )

  ; A whole spot is free
  (:method free-curb
          :parameters   (?curb - curb)
          :expansion    (     (tag t1 (transfert ?curb ?freecurb))
                        )
          :constraints  (and  (before (and
                                (curb-clear ?freecurb)
                              )
                          t1)
                        )
  )

  ; If there is only one car
  ; Two half-spots are free
  (:method free-curb
          :parameters   (?curb - curb)
          :expansion    (     (tag t1 (move-curb-to-car ?car ?curb ?car2))
                        )
          :constraints  (and  (before (and
                                (at-curb-num ?car ?curb)
                                (at-curb ?car2)
                                (car-clear ?car2)
                              )
                          t1)
                        )
  )

  ; A whole spot is free
  (:method free-curb
          :parameters   (?curb - curb)
          :expansion    (     (tag t1 (move-curb-to-curb ?car ?curb ?freecurb))
                        )
          :constraints  (and  (before (and
                                (curb-clear ?freecurb)
                                (at-curb-num ?car ?curb)
                              )
                          t1)
                        )
  )

  ;------------------------------------------------------------------
  ;             Put a car on top of another without touching cars at curbs
  ;             Make sure the car is not on the destination curb
  ;------------------------------------------------------------------
  ; If the car is on the right spot but not at the curb
  ; If there is one completely free spot
  (:method put-on-top
          :parameters   (?car - car ?destcurb - curb)
          :expansion    (     (tag t1 (free-car ?transcar))
                              (tag t2 (move-car-to-car ?car ?car2 ?transcar))
                        )
          :constraints  (and  (before (and
                                (not (at-curb ?car))
                                (behind-car ?car ?car2)
                                (at-curb-num ?car2 ?destcurb)
                                (curb-clear ?freecurb)
                                (at-curb ?transcar)
                                (not (car-clear ?transcar))
                              )
                          t1)
                        )
  )

  ; If the car is on the right spot but not at the curb
  ; If there is one free car
  (:method put-on-top
          :parameters   (?car - car ?destcurb - curb)
          :expansion    (     (tag t1 (move-car-to-car ?car ?car2 ?transcar))
                        )
          :constraints  (and  (before (and
                                (not (at-curb ?car))
                                (behind-car ?car ?car2)
                                (at-curb-num ?car2 ?destcurb)
                                (curb-clear ?freecurb)
                                (at-curb ?transcar)
                                (car-clear ?transcar)
                              )
                          t1)
                        )
  )

  ; If there is no free car available
  (:method put-on-top
          :parameters   (?car - car ?destcurb - curb)
          :expansion    (     (tag t1 (free-car ?car))
                              (tag t2 (free-car ?transcar))
                              (tag t3 (move-curb-to-car ?car ?curb ?transcar))
                        )
          :constraints  (and  (before (and
                                (at-curb ?car)
                                (not (car-clear ?car))
                                (at-curb ?transcar)
                                (not (car-clear ?transcar))
                                (not (= ?transcar ?car))
                                (at-curb-num ?car ?curb)
                              )
                          t1)
                        )
  )

  ; If there is a free car available and the car is at a curb
  (:method put-on-top
          :parameters   (?car - car ?destcurb - curb)
          :expansion    (     (tag t1 (move-curb-to-car ?car ?curb ?freecar))
                        )
          :constraints  (and  (before (and
                                (car-clear ?car)
                                (at-curb ?car)
                                (at-curb-num ?car ?curb)
                                (at-curb ?freecar)
                                (car-clear ?freecar)
                                (not (= ?freecar ?car))
                                (not (at-curb-num ?freecar ?destcurb))
                              )
                          t1)
                        )
  )

  ;if the car is clear and doesn't have to be moved
  (:method put-on-top
          :parameters   (?car - car ?destcurb - curb)
          :expansion    (     (tag t1 (nop))
                        )
          :constraints  (and  (before (and
                                (car-clear ?car)
                                (behind-car ?car ?car2)
                                (not (at-curb-num ?car2 ?destcurb))
                              )
                          t1)
                        )
  )

  ;------------------------------------------------------------------
  ;                       Park a car on a curb
  ;------------------------------------------------------------------
  ; First, make sure the car is free, that is, on top of another
  ; There is a car above
  (:method park-curb
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (put-on-top ?car ?curb))
                              (tag t2 (park-curb-2 ?car ?curb))
                        )
          :constraints  (and  (before (and
                                (not (car-clear ?car))
                              )
                          t1)
                        )
  )

  ; There is no car above and the car is at a curb
  (:method park-curb
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (put-on-top ?car ?curb))
                              (tag t2 (park-curb-2 ?car ?curb))
                        )
          :constraints  (and  (before (and
                                (car-clear ?car)
                                (at-curb ?car)
                              )
                          t1)
                        )
  )

  ; The car is already on top of another
  (:method park-curb
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (park-curb-2 ?car ?curb))
                        )
          :constraints  (and  (before (and
                                (car-clear ?car)
                                (not (at-curb ?car))
                              )
                          t1)
                        )
  )

  ; If everything is fine already
  (:method park-curb
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (nop))
                        )
          :constraints  (and  (before (and
                                (at-curb ?car)
                                (at-curb-num ?car ?curb)
                              )
                          t1)
                        )
  )



  ; Make sure the spot is free for the next step
  (:method park-curb-2
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (park-curb-3 ?car ?curb))
                        )
          :constraints  (and  (before (and
                                (curb-clear ?curb)
                              )
                          t1)
                        )
  )

  (:method park-curb-2
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (free-curb ?curb))
                              (tag t2 (park-curb-3 ?car ?curb))
                        )
          :constraints  (and  (before (and
                                (not (curb-clear ?curb))
                              )
                          t1)
                        )
  )


  ;Move the car to the newly freed curb
  (:method park-curb-3
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (move-car-to-curb ?car ?currcar ?curb))
                        )
          :constraints  (and  (before (and
                                (behind-car ?car ?currcar)
                                (not (at-curb ?car))
                                (car-clear ?car)
                                (curb-clear ?curb)
                              )
                          t1)
                        )
  )

  (:method park-curb-3
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (move-curb-to-curb ?car ?currcurb ?curb))
                        )
          :constraints  (and  (before (and
                                (at-curb ?car)
                                (car-clear ?car)
                                (at-curb-num ?car ?currcurb)
                              )
                          t1)
                        )
  )

  (:method park-curb-3
          :parameters   (?car - car ?curb - curb)
          :expansion    (     (tag t1 (nop))
                        )
          :constraints  (and  (before (and
                                (at-curb ?car)
                                (at-curb-num ?car ?curb)
                              )
                          t1)
                        )
  )

  ;------------------------------------------------------------------
  ;                       Park a car on another car
  ;------------------------------------------------------------------
  ; Should only be used if every car that is supposed to be parked at a curb
  ; is already parked

  ; Make sure the spot is free
  ; The spot is not free
  ; It doesn't matter if the other free spot is the one with ?car
  (:method park-car
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (move-car-to-car ?car2 ?destcar ?transcar))
                              (tag t2 (park-car-2 ?car ?destcar))
                        )
          :constraints  (and  (before (and
                                (not (car-clear ?destcar))
                                (behind-car ?car2 ?destcar)
                                (car-clear ?car2)
                                (car-clear ?transcar)
                                (at-curb ?transcar)
                              )
                          t1)
                        )
  )

  ; A whole curb is free
  (:method park-car
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (move-car-to-curb ?car2 ?destcar ?transcurb))
                              (tag t2 (park-car-2 ?car ?destcar))
                        )
          :constraints  (and  (before (and
                                (not (car-clear ?destcar))
                                (curb-clear ?transcurb)
                                (behind-car ?car2 ?destcar)
                                (car-clear ?car2)
                              )
                          t1)
                        )
  )

  ; The spot is already free
  (:method park-car
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (park-car-2 ?car ?destcar))
                        )
          :constraints  (and  (before (and
                                (at-curb-num ?car1 ?curb)
                                (car-clear ?destcar)
                              )
                          t1)
                        )
  )

  ; If there is nothing to do
  (:method park-car
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (nop))
                        )
          :constraints  (and  (before (and
                                (behind-car ?car ?destcar)
                              )
                          t1)
                        )
  )


  ; Make sure the car is free for the next step
  (:method park-car-2
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (park-car-3 ?car ?destcar))
                        )
          :constraints  (and  (before (and
                                (car-clear ?car)
                              )
                          t1)
                        )
  )

  (:method park-car-2
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (free-car ?car))
                              (tag t2 (park-car-3 ?car ?destcar))
                        )
          :constraints  (and  (before (and
                                (not (car-clear ?car))
                              )
                          t1)
                        )
  )


  ;Move the car to the newly freed car
  (:method park-car-3
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (move-car-to-car ?car ?currcar ?destcar))
                        )
          :constraints  (and  (before (and
                                (behind-car ?car ?currcar)
                                (not (at-curb ?car))
                                (car-clear ?car)
                                (car-clear ?destcar)
                              )
                          t1)
                        )
  )

  (:method park-car-3
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (move-curb-to-car ?car ?currcurb ?destcar))
                        )
          :constraints  (and  (before (and
                                (at-curb ?car)
                                (car-clear ?car)
                                (at-curb-num ?car ?currcurb)
                              )
                          t1)
                        )
  )

  (:method park-car-3
          :parameters   (?car ?destcar - car)
          :expansion    (     (tag t1 (nop))
                        )
          :constraints  (and  (before (and
                                (behind-car ?car ?destcar)
                              )
                          t1)
                        )
  )

)

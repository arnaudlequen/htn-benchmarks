(define (problem BW-test)
(:domain blocksworld)
(:requirements :strips :equality :typing :htn :negative-preconditions)
(:objects b1 b2 b3 - block)
(:init
  (handempty)
  (clear b1)
  (clear b3)
  (ontable b1)
  (ontable b2)
  (on b3 b2)
)
(:goal
		:tasks	    (
					           (tag t1 (insert_from_top b1 b2 b3))
		            )
		:constraints(and  (after (and

					            ) t1)
		            )
)
)

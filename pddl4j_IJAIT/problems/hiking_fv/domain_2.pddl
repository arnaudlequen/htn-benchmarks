;  (c) 2001 Copyright (c) University of Huddersfield
;  Automatically produced from GIPO from the domain hiking
;  All rights reserved. Use of this software is permitted for non-commercial
;  research purposes, and it may be copied only for that use.  All copies must
;  include this copyright message.  This software is made available AS IS, and
;  neither the GIPO team nor the University of Huddersfield make any warranty about
;  the software or its performance.

(define (domain hiking)
  (:requirements :strips :equality :typing :htn :negative-preconditions)
  (:types car tent person couple place )
  (:predicates
              (at_tent ?t - tent ?pos - place)
              (at_person ?p - person ?pos - place)
              (at_car ?car - car ?pos - place)
              (partners ?cpl - couple ?p1 - person ?p2 - person)
              (up ?t - tent)
              (down ?t - tent)
              (walked ?cpl - couple ?pos - place)
              (next ?from - place ?to - place)
)

(:action put_down
         :parameters    (?p - person ?pos - place ?t - tent)
         :precondition  (and  (at_person ?p ?pos)
                              (at_tent ?t ?pos)
                              (up ?t)
                        )
         :effect        (and  (down ?t)
                              (not (up ?t))
                        )
)

(:action put_up
         :parameters    (?p - person ?pos - place ?t - tent)
         :precondition  (and  (at_person ?p ?pos)
                              (at_tent ?t ?pos)
                              (down ?t)
                        )
         :effect        (and  (up ?t)
                              (not (down ?t))
                        )
)

(:action drive_passenger
         :parameters    (?p - person ?from - place ?to - place ?car - car ?pas - person)
         :precondition  (and  (at_person ?p ?from)
                              (at_car ?car ?from)
                              (at_person ?pas ?from)
                              (not (= ?p ?pas))
                        )
         :effect        (and  (at_person ?p ?to)
                              (not (at_person ?p ?from))
                              (at_car ?car ?to)
                              (not (at_car ?car ?from))
                              (at_person ?pas ?to)
                              (not (at_person ?pas ?from))
                        )
)

(:action drive
         :parameters    (?p - person ?from - place ?to - place ?car - car)
         :precondition  (and  (at_person ?p ?from)
                              (at_car ?car ?from)
                        )
         :effect        (and  (at_person ?p ?to)
                              (not (at_person ?p ?from))
                              (at_car ?car ?to)
                              (not (at_car ?car ?from))
                        )
)

(:action drive_tent
         :parameters    (?p - person ?from - place ?to - place ?car - car ?t - tent)
         :precondition  (and  (at_person ?p ?from)
                              (at_car ?car ?from)
                              (at_tent ?t ?from)
                              (down ?t)
                        )
         :effect        (and  (at_person ?p ?to)
                              (not (at_person ?p ?from))
                              (at_car ?car ?to)
                              (not (at_car ?car ?from))
                              (at_tent ?t ?to)
                              (not (at_tent ?t ?from))
                        )
)

(:action drive_tent_passenger
         :parameters    (?p - person ?from - place ?to - place ?car - car ?t - tent ?pas - person)
         :precondition  (and  (at_person ?p ?from)
                              (at_car ?car ?from)
                              (at_tent ?t ?from)
                              (down ?t)
                              (at_person ?pas ?from)
                              (not (= ?p ?pas))
                        )
         :effect        (and  (at_person ?p ?to)
                              (not (at_person ?p ?from))
                              (at_car ?car ?to)
                              (not (at_car ?car ?from))
                              (at_tent ?t ?to)
                              (not (at_tent ?t ?from))
                              (at_person ?pas ?to)
                              (not (at_person ?pas ?from))
                        )
)

(:action walk_together
         :parameters    (?t - tent ?to - place ?p1 - person ?from - place ?p2 - person ?cpl - couple)
         :precondition  (and  (at_tent ?t ?to)
                              (up ?t)
                              (at_person ?p1 ?from)
                              (next ?from ?to)
                              (at_person ?p2 ?from)
                              (not (= ?p1 ?p2))
                              (walked ?cpl ?from)
                              (partners ?cpl ?p1 ?p2)
                        )
         :effect        (and  (at_person ?p1 ?to)
                              (not (at_person ?p1 ?from))
                              (at_person ?p2 ?to)
                              (not (at_person ?p2 ?from))
                              (walked ?cpl ?to)
                              (not (walked ?cpl ?from))
                        )
)

(:action nop
         :parameters    ()
         :precondition  ()
         :effect        (and)
)


;------------------------------------------------------------------
;                               Methods
;------------------------------------------------------------------

;------------------------------------------------------------------
;                     Bring a tent somewhere specific
;------------------------------------------------------------------

;or we need to go and fetch one from somwhere. It can be up or down

;if there is no one nearby,
(:method set_up_tent
        :parameters   (?to - place)
        :expansion    (     (tag t1 (move_person ?p ?from))
                            (tag t2 (put_down ?p ?from ?t))
                            (tag t3 (drive_tent ?p ?from ?to ?car ?t))
                            (tag t4 (put_up ?p ?to ?t))
                            (tag t5 (move_person ?p ?fromp))
                      )
        :constraints  (and  (before (and
                            (up ?t)
                            (at_tent ?t ?from)
                            (at_car ?car ?fromp)
                            (at_person ?p ?fromp)
                            (not (= ?fromp ?from))
                            )
                        t1)
                      )
)

(:method set_up_tent
        :parameters   (?to - place)
        :expansion    (     (tag t1 (move_person ?p ?from))
                            (tag t2 (drive_tent ?p ?from ?to ?car ?t))
                            (tag t3 (put_up ?p ?to ?t))
                            (tag t4 (move_person ?p ?fromp))
                      )


        :constraints  (and  (before (and
                            (down ?t)
                            (at_tent ?t ?from)
                            (at_car ?car ?fromp)
                            (at_person ?p ?fromp)
                            (not (= ?fromp ?from))
                            )
                        t1)
                      )
)

;if someone is already on spot
(:method set_up_tent
        :parameters   (?to - place)
        :expansion    (     (tag t1 (put_down ?p ?from ?t))
                            (tag t2 (drive_tent ?p ?from ?to ?car ?t))
                            (tag t3 (put_up ?p ?to ?t))
                            (tag t4 (move_person_or_not ?p ?from))
                      )
        :constraints  (and  (before (and
                            (up ?t)
                            (at_tent ?t ?from)
                            (at_person ?p ?from)
                            (at_car ?car ?from)
                            )
                        t1)
                      )
)

(:method set_up_tent
        :parameters   (?to - place)
        :expansion    (     (tag t1 (drive_tent ?p ?from ?to ?car ?t))
                            (tag t2 (put_up ?p ?to ?t))
                            (tag t3 (move_person_or_not ?p ?from))
                      )
        :constraints  (and  (before (and
                            (down ?t)
                            (at_tent ?t ?from)
                            (at_person ?p ?from)
                            (at_car ?car ?from)
                            )
                        t1)
                      )
)

;or there is already a tent where we want
(:method set_up_tent
        :parameters   (?to - place)
        :expansion    (     (tag t1 (nop))
                      )
        :constraints  (and  (before (and
                            (at_tent ?t ?to)
                            (up ?t)
                            )
                        t1)
                      )
)

;------------------------------------------------------------------
;                     Go for a trip
;------------------------------------------------------------------

(:method go_hiking
        :parameters   (?cpl - couple ?to - place)
        :expansion    (     (tag t1 (go_hiking ?cpl ?from ?to))
                      )
        :constraints  (and  (before (and
                            (partners ?cpl ?p1 ?p2)
                            (at_person ?p1 ?from)
                            (at_person ?p2 ?from)
                            )
                        t1)
                      )
)

(:method go_hiking
        :parameters   (?cpl - couple ?from - place ?to - place)
        :expansion    (     (tag t1 (nop))
                      )
        :constraints  (and  (before (and
                            (= ?from ?to)
                            )
                        t1)
                      )
)

(:method go_hiking
        :parameters   (?cpl - couple ?from - place ?to - place)
        :expansion    (     (tag t1 (go_hiking ?cpl ?from ?mid))
                            (tag t2 (go_hiking ?cpl ?mid ?to))
                      )
        :constraints  (and  (before (and
                            (next ?from ?mid)
                            )
                        t1)
                      )
)

(:method go_hiking
        :parameters   (?cpl - couple ?from - place ?to - place)
        :expansion    (     (tag t1 (set_up_tent ?to))
                            (tag t2 (move_person ?p1 ?from))
                            (tag t3 (move_person ?p2 ?from))
                            (tag t4 (walk_together ?t ?to ?p1 ?from ?p2 ?cpl))
                      )
        :constraints  (and  (before (and
                            (next ?from ?to)
                            (partners ?cpl ?p1 ?p2)
                            (at_person ?p1 ?from)
                            (at_person ?p2 ?from)
                            )
                        t1)
                      )
)

;------------------------------------------------------------------
;                     Move a specific person somewhere
;------------------------------------------------------------------

(:method move_person
        :parameters   (?p - person ?to - place)
        :expansion    (     (tag t1 (nop))
                      )
        :constraints  (and  (before (and
                            (at_person ?p ?to)
                            )
                        t1)
                      )
)

(:method move_person
        :parameters   (?p - person ?to - place)
        :expansion    (     (tag t1 (drive ?p ?from ?to ?car))
                      )
        :constraints  (and  (before (and
                            (not (at_person ?p ?to))
                            (at_person ?p ?from)
                            (at_car ?car ?from)
                            )
                        t1)
                      )
)

(:method move_person
        :parameters   (?p - person ?to - place)
        :expansion    (     (tag t1 (drive ?driver ?from ?rally ?car))
                            (tag t2 (drive_passenger ?driver ?rally ?to ?car ?p))
                            (tag t3 (drive ?driver ?to ?from ?car))
                      )
        :constraints  (and  (before (and
                            (not (at_person ?p ?to))
                            (at_person ?p ?rally)
                            (at_person ?driver ?from)
                            (at_car ?car ?from)
                            (not (= ?p ?driver))
                            )
                        t1)
                      )
)

;------------------------------------------------------------------
;                     Move a car somewhere
;------------------------------------------------------------------

;(:method move_car
;        :parameters   (?to - place)
;        :expansion    (     (tag t1 (drive ?p1 ?from1 ?to ?c1))
;                            (tag t2 (drive ?p2 ?from2 ?to ?c2))
;                            (tag t3 (drive_passenger ?p2 ?to ?from1 ?c2 ?p1))
;                            (tag t4 (drive ?p2 ?from1 ?from2 ?c2))
;
;                      )
;        :constraints  (and  (before (and
;                            (not (at_person ?p1 ?to))
;                            (not (at_person ?p2 ?to))
;                            (not (= ?from1 ?to))
;                            (not (= ?from2 ?to))
;                            (at_person ?p1 ?from1)
;                            (at_person ?p2 ?from2)
;                            (at_car ?c1 ?from1)
;                            (at_car ?c2 ?from2)
;                            (not (= ?p1 ?p2))
;                            )
;                        t1)
;                      )
;)

(:method move_car
        :parameters   (?to - place)
        :expansion    (     (tag t1 (drive ?p ?from ?to ?car))
                            (tag t2 (move_person_or_not ?p ?to))
                      )
        :constraints  (and  (before (and
                            (at_person ?p ?from)
                            (at_car ?car ?from)
                            (not (= ?from ?to))
                            )
                        t1)
                      )
)

;------------------------------------------------------------------
;                     Move someone or not
;------------------------------------------------------------------

(:method move_person_or_not
        :parameters   (?p - person ?to - place)
        :expansion    (     (tag t1 (move_person ?p ?to))
                      )
        :constraints  (and  (before (and
                            (not (at_person ?p ?to))
                            )
                        t1)
                      )
)

(:method move_person_or_not
        :parameters   (?p - person ?to - place)
        :expansion    (     (tag t1 (nop))
                      )
        :constraints  (and)
)



;------------------------------------------------------------------
;                     Move the car or not
;------------------------------------------------------------------

(:method move_car_or_not
        :parameters   (?to - place)
        :expansion    (     (tag t1 (move_car ?to))
                      )
        :constraints  (and)
)

(:method move_car_or_not
        :parameters   (?to - place)
        :expansion    (     (tag t1 (nop))
                      )
        :constraints  (and)
)




)

set terminal pdf 
set decimalsign "."
set output "time_childsnack_iSHOP.pdf"
set title "Processing on childsnack with iSHOP " font "Helvetica,7"
set xlabel "Problem" font "Helvetica,6"
set ylabel "Processing Time (s)" font "Helvetica,6"
set grid y
#set xrange['0':'18']
#set logscale x
#set yrange[0:'1100']

plot "../data/childsnack.data" using 0:6 with linespoints title "Preprocessing", \
    "../data/childsnack.data" using 0:8 with linespoints title "Searching", \
    "../data/childsnack.data" using 0:9 with linespoints title "Total"

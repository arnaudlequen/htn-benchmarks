set terminal pdf 
set decimalsign "."
set output "compare_nb_iground_meths.pdf"
set xlabel "Problems" font "Helvetica,6"
set ylabel "Nb ground methods" font "Helvetica,6"
set grid y
set logscale y
#set style line 3 lt 5 lw 2 pt 6 ps 0.5
#set xrange['0':'21']
#set logscale x
#set yrange[0:'1100']

show style line

plot "~/Workspace/Bitbucket_Projetcs/pddl4j_IJAIT/tests_results/data/IJAIT_rover.data" using 0:14 with linespoints title "without simplification" pt 8, \
"~/Workspace/Bitbucket_Projetcs/pddl4j_IJAIT/tests_results/data/IJAIT_rover.data" using 0:5 with linespoints title "with simplification" pt 5, \
"~/Workspace/Bitbucket_Projetcs/pddl4j_IJAIT/tests_results/data/IJAIT_rover.data" using 0:15 with linespoints title "with simplification" pt 6, \

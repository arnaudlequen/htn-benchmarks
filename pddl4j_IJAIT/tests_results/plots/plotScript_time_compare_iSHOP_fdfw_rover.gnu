set terminal pdf 
set decimalsign "."
set output "time_fdwd_iSHOP_rover2_final.pdf"
set title "Processing time comparison on rover domain between fdwd and iSHOP" font "Helvetica,7"
set xlabel "Problems" font "Helvetica,6"
set ylabel "Search Time (s)" font "Helvetica,6"
set grid y
#set xrange['0':'22']
#set logscale x
#set yrange[0:'1100']

 plot   "~/Workspace/Bitbucket_Projetcs/pddl4j/tests_results/data/rover2_new.data" using 0:9 with linespoints title "total iSHOP", \
    "~/Workspace/Bitbucket_Projetcs/pddl4j/tests_results/data/rover2_new.data" using 0:8 with linespoints title "search iSHOP", \
    "~/Workspace/Bitbucket_Projetcs/pddl4j/tests_results/fDownward/data/rover.data" using 0:5 with linespoints title "declared total fDownward", \
    "~/Workspace/Bitbucket_Projetcs/pddl4j/tests_results/fDownward/data/rover.data" using 0:6 with linespoints title "real total fDownward", \
    "~/Workspace/Bitbucket_Projetcs/pddl4j/tests_results/fDownward/data/rover.data" using 0:4 with linespoints title "search fDownward", \

set terminal pdf 
set decimalsign "."
set output "search_time_CoRe_iSHOP_rover2_final.pdf"
set xlabel "Problems" font "Helvetica,6"
set ylabel "Search Time (s)" font "Helvetica,6"
set grid y
#set xrange['0':'22']
#set logscale x
#set yrange[0:'1100']

plot "~/Workspace/Thesis_Projects/CoRe-Planner-1.0/tests_results/data/rover2.data" using 0:2 with linespoints title "CoRe", \
    "~/Workspace/Bitbucket_Projetcs/pddl4j/tests_results/data/rover2.data" using 0:9 with linespoints title "total iSHOP", \
    "~/Workspace/Bitbucket_Projetcs/pddl4j/tests_results/data/rover2.data" using 0:8 with linespoints title "search iSHOP", \

set terminal pdf 
set decimalsign "."
set output "stats_rover_duplicated_meths.pdf"
set title "Rover with duplicated meths" font "Helvetica,7"
set xlabel "Problem" font "Helvetica,6"
set ylabel "Number (s)" font "Helvetica,6"
set grid y
#set xrange[0:'18']
#set logscale x
#set yrange[0:'1100']

plot "file.data" using 0:2 with linespoints title "Facts", \
    "file.data" using 0:3 with linespoints title "Tasks", \
    "file.data" using 0:4 with linespoints title "Operators", \
    "file.data" using 0:5 with linespoints title "Methods"

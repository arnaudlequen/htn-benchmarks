set terminal pdf 
set decimalsign "."
set output "plans_length_CoRe_iSHOP_rover.pdf"
set title "Plans length comparison on rover domain between CoRe and iSHOP" font "Helvetica,7"
set xlabel "Problem" font "Helvetica,6"
set ylabel "Plans length (action)" font "Helvetica,6"
set grid y
set xrange['0':'22']
#set logscale x
#set yrange[0:'1100']

plot "~/Workspace/Thesis_Projects/CoRe-Planner-1.0/tests_results/data/rover.data" using 0:3 with linespoints title "CoRe", \
    "~/Workspace/Bitbucket_Projetcs/pddl4j/tests_results/data/rover2.data" using 0:13 with linespoints title "iSHOP", \

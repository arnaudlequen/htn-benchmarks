#!/bin/bash

dir=$1
if [ ! dir ]; then
    echo "Please provide a destination directory."
    exit
fi

mkdir -p $dir

dest=$dir/checksums
> $dest

for f in htnsat.jar solve_madagascar.sh solve_madagascar_incremental.sh glucose incplan-glucose4 M Mp MpC MpC_Incplan interpreter_t-rex ; do
    md5sum $f >> $dest
done

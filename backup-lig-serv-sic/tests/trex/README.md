# HTN Evaluations

In the following, I describe how to run evaluations within the framework used during my Master project.

## Defining problem instances to run

Create a file `evaluation_instances.csv` in this directory.
Each line contains exactly one instance, specified as follows:

```
<Domain> <Instance> <Approach> <Additional parameters>
```

* `Domain`: The domain of the instance. In the directory `./problems`, a directory of this name must exists, containing the domain specification `domain.pddl` as well as all instances of this domain to test.
* `Instance`: The index of the instance. In the domain directory `./problems/<Domain>/`, a problem specification `p<Instance>.pddl` must exist.
* `Approach`: The planner / planning strategy to employ.
* `Additional parameters`: The parameters for the previously specified planning strategy, if necessary.

For example, the file may look like this:

```
Rover 01 GTOHP
Barman 10 T-REX -i {-P,-b8192}
Barman 10 T-REX -i {-P,-b8192,-o,-l-1}
```

The first line employs the HTN planner GTOHP on the problem Rover01.
The second line employs the HTN-SAT planner with the T-REX approach on the problem Barman10. (The parametrization specified on that line has been found to work particularly well, so you can generally just use this one if you want to test T-REX.)
The third line also employs T-REX, but additionally performs an iterative plan optimization after finding an initial plan.

## Preparing the evaluations

First, to randomize the instances, execute `bash create_full_eval_instances.sh`.

This creates a file `full_eval_instances_randomized.csv` containing the previously specified evaluation instances in a random order.
If multiple executions (repetitions) of each instance are desired, edit the file `num_repetitions` to contain (only) the amount of needed repetitions per instance, and re-run `bash create_full_eval_instances.sh`.

Edit the file `timeout_seconds` to (only) contain the amount of seconds after which a run should be cut off.
Similarly, the number inside the file `memlimit_kb` specifies the maximum amount of kilobytes of RAM that may be allocated by the run after which it is cut off.

If desired (although I would not really recommend it), the amount of instances to be run _in parallel_ can be modified in the variable `num_jobs` in the script `run_all.sh`.

## Running the evaluations

Execute `bash run_all.sh`.

With each additional run, this will spawn temporal directories `./proc_*` containing logs and symbolic links to the binaries and scripts in this directory.
In addition, a file `./runlog` is created, and some information about the run instances is logged here.

As soon as the evaluation finishes, these directories and files will vanish and all produced log information is moved to a new directory `logs/Eval_<Date and time>/`.

## Cancelling and recovering evaluations

When cancelling an evaluation, make sure that all involved processes are terminated, e.g. by using `htop`.

If an evaluation is prematurely cancelled or fails, the log information generated so far can be recovered by copying the file `runlog` as well as all existing directories `./proc_*/logs/*/` into a new log directory.

To resume a started evaluation, the line number of the first un-finished instance can be specified in the variable `start_instance` in the script `run_all.sh`. The evaluation process will then only do the evaluations from this line until the last line of instances.

## Getting results

As a first step towards analyzing and visualizing results, `bash analysis/report_times.sh logs/<your log directory>` can be executed to create a file `report_times.csv` in your log directory containing the run times for each instance.
This file can then be further processed for visualizations or computations.

## Questions, errors and issues

Send me a mail: `mail@dominikschreiber.de`

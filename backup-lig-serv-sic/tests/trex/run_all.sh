#!/bin/bash

instance_file=full_eval_instances_randomized.csv
num_instances=`cat $instance_file | wc -l`

num_jobs=1
start_instance=1

# Execute
seq $start_instance $num_instances | parallel --tmpdir ~/.parallel/tmp --progress -j $num_jobs bash run_single.sh {} > runlog 2>&1

# Write metadata into new log directory of this evaluation
date=`date +%Y-%m-%d_%H-%M`
eval_dir=logs/Eval_${date}/
mkdir -p $eval_dir
mv runlog $eval_dir
bash performance_metadata.sh $eval_dir
bash create_checksums.sh $eval_dir
cp timeout_seconds full_eval_instances_randomized.csv $eval_dir

# Move logs, and tidy up
for i in `seq 1 $num_instances`; do
    mv proc_$i/logs/* $eval_dir
    rm -rf proc_$i
done
for f in carj.json f.cnf SOLUTION.txt ; do
    if [ -f $f ] ; then
        rm $f
    fi
done

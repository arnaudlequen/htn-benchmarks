import re
import numpy as np
import matplotlib.pyplot as plt
import os

MAX = 20


def main():
    people = [-1]

    for i in range(1, 151):
        write_file = ""
        pad = str(i).zfill(2)
        in_file = open("p" + pad + ".pddl", "rt")
        tsk = False
        for line in in_file:
            if len(line) <= 4 and tsk:
                tsk = False
                write_file += "  :tasks (\n"
                for j in people[i]:
                    write_file += "             (tag t" + j + " (give_a_ride p" + j + "))\n"
                write_file += "         )\n  :constraints\n         (and (after (and\n"
                for j in people[i]:
                    write_file += "             (served p" + j + ")\n"
                write_file += "         ) t" + people[i][-1] + "))\n)\n"


            if tsk:
                taskid = line
                taskid = re.sub("[^0-9]", "", taskid)
                people[i].append(taskid)

            if ":goal" in line:
                tsk = True
                people.append([])
                write_file += "(:goal\n"

            if not tsk:
                write_file += line

            if ":domain" in line:
                write_file += "     (:requirements :strips :typing :htn :negative-preconditions :equality)\n"

        in_file.close()

        in_file = open("htn_p" + pad + ".pddl", "w+")
        in_file.write(write_file)
        in_file.close()

main()

(define   (problem parking)
  (:domain parking)
     (:requirements :strips :typing :htn :negative-preconditions :equality)
  (:objects
     car_00  car_01  car_02 car_03 - car
     curb_00 curb_01 curb_02 - curb
  )
  (:init

    (at-curb car_00)
    (at-curb-num car_00 curb_00)
    (behind-car car_01 car_00)
    (car-clear car_01)
    (at-curb car_02)
    (at-curb-num car_02 curb_01)
    (behind-car car_03 car_02)
    (car-clear car_03)
    (curb-clear curb_02)
  )
(:goal
  :tasks (
             (tag t1 (park-curb car_01 curb_00))
             (tag t2 (park-curb car_00 curb_01))
             (tag t3 (park-curb car_03 curb_02))
             (tag t4 (park-car car_02 car_01))

         )
  :constraints
         (and (after (and
             (at-curb-num car_01 curb_00)
             (at-curb-num car_00 curb_01)
             (at-curb-num car_03 curb_02)
             (behind-car car_02 car_01)
         ) t4))
)
  )

)
; =========== INIT ===========
;  curb_00: car_00 car_01
;  curb_01: car_02 car_03
;  curb_02:
; ========== /INIT ===========

; =========== GOAL ===========
;  curb_00: car_01 car_02
;  curb_01: car_00
;  curb_02: car_03
; =========== /GOAL ===========

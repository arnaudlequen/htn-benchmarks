#!/bin/bash
command="$1 $2 $3 -o 04_solution.txt"

echo "Command: $command"

directory="logs/`date +%Y-%m-%d_%H-%M-%S_%N`_$6_`echo $4|sed 's/\//-/g'`_$5"
mkdir $directory

$command | tee ${directory}/00_log.txt

#!/bin/bash

if [ ! $1 ]; then
    echo "Please provide a destination directory."
    exit
fi
dir=$1

timeout_seconds=`cat $dir/timeout_seconds`

reportfile=$dir/report_times.csv
> $reportfile

count_valid=0
count_invalid=0

for d in $dir/*/; do
    
    if [ -f $d/args ]; then
    
        problem=(`cat $d/args`)
        domain=${problem[0]}
        index=${problem[1]}
        
        echo "*******************************"
        echo $d
        echo $domain $index
        echo "*******************************"
        
        file_solution=$d/04_solution.txt
        if [ ! -f $file_solution ]; then
            echo "No solution file found."
            echo ""
        else
        
            cat $file_solution|grep -E "^[0-9]+: " > plan
            
            file_domain=../problems/non_htn/$domain/domain.pddl
            file_problem=../problems/non_htn/$domain/p${index}.pddl
            file_plan=plan
            
            val_output=`../bin/VAL_validate $file_domain $file_problem $file_plan`
            if echo $val_output|grep -q "Plan valid" ; then
                echo "Plan valid."
                echo ""
                count_valid=$((count_valid+1))
            else
                echo "Plan INVALID."
                echo ""
                count_invalid=$((count_invalid+1))
            fi
        fi
    fi
done

echo "$count_valid/$((count_valid+count_invalid)) found plans identified as valid."

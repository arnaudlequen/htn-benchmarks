#!/bin/bash
sat_executable="./incplan-glucose4"
sat_executable_fallback="./incplan-minisat220"
command="$sat_executable f.cnf"
command_fallback="$sat_executable_fallback f.cnf"
if $command ; then
    # Successful execution
    exit
else
    # Primary solver failed
    echo "WARNING: Launching fallback solver process."
    $command_fallback
fi

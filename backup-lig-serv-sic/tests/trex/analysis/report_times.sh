#!/bin/bash

if [ ! $1 ]; then
    echo "Please provide a destination directory."
    exit
fi
dir=$1

timeout_seconds=`cat $dir/timeout_seconds`

reportfile=$dir/report_times.csv
> $reportfile

olddir=`pwd`
for d in $dir/*/; do
    cd $d

    if [ -f args ]; then
        problem=`cat args`
        problem=`echo $problem|sed 's/ -i /_/g'`
    fi
    
    solution_found=false
    
    if [ -f 00_log.txt ]; then
    
        # Htnsat
        if grep -q "Reported result" 00_log.txt; then
        
            solution_found=true
        
            # SAT Solving time only
            #times=`cat 00_log.txt|grep "Executed SAT solver"|grep -oE "[0-9]+"|tr '\n' ' '`
            
            # Complete execution time
            #times=`cat 00_log.txt|grep "Total execution time"|grep -oE "[0-9]+"|tr '\n' ' '`
        
        # Madagascar
        elif grep -q "PLAN FOUND" 00_log.txt; then
        
            solution_found=true
        
            # Complete execution time
            #times=`cat 00_log.txt|grep -oE "total time [0-9]+\.[0-9]+"|grep -oE "[0-9]+\.[0-9]+"|tr '\n' ' '`
            #times=`echo "$times*1000" | bc`
            
        # Incplan-Madagascar
        elif grep -q "Final Makespan:" 00_log.txt; then
        
            solution_found=true
        fi
        
    elif [ -f general_output ]; then
        
        if grep -q "Exiting." general_output; then
        
            # Isolated T-REX run
            solution_found=true
            
        elif grep -q "Found plan as follows:" general_output; then
            
            # HTN-SAT with GTOHP
            solution_found=true
        fi
    fi
        
    if $solution_found ; then
        #times=`cat time|grep -oE "[0-9]+\.[0-9]+user"|sed 's/user//g'`
        #times=`echo "$times * 1000"|bc`
        times=`cat time|grep -oE "[0-9]+:[0-9]+\.[0-9]+elapsed"`
        minutes=`echo $times|grep -oE "[0-9]+:"|sed 's/://g'`
        milliseconds=`echo $times|grep -oE ":[0-9]+\.[0-9]+"|sed 's/://g'|sed 's/\.//g'`0
        times=`echo "$minutes * 60 * 1000 + $milliseconds"|bc`
    else
        times=${timeout_seconds}000
    fi
    
    cd $olddir
    
    echo "$problem $times" >> $reportfile
done

sort $reportfile -o $reportfile

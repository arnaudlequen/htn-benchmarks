Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.004] Parsed head comment information.
[0.544] Processed problem encoding.
[0.863] Calculated possible fact changes of composite elements.
[0.863] Initialized instantiation procedure.
[0.863] 
[0.863] *************************************
[0.863] * * *   M a k e s p a n     0   * * *
[0.863] *************************************
[0.867] Instantiated 614 initial clauses.
[0.867] The encoding contains a total of 419 distinct variables.
[0.867] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.867] Executed solver; result: UNSAT.
[0.867] 
[0.867] *************************************
[0.867] * * *   M a k e s p a n     1   * * *
[0.867] *************************************
[0.868] Computed next depth properties: array size of 3.
[0.869] Instantiated 96 transitional clauses.
[0.872] Instantiated 794 universal clauses.
[0.872] Instantiated and added clauses for a total of 1,504 clauses.
[0.872] The encoding contains a total of 658 distinct variables.
[0.872] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.872] Executed solver; result: UNSAT.
[0.872] 
[0.872] *************************************
[0.872] * * *   M a k e s p a n     2   * * *
[0.872] *************************************
[0.877] Computed next depth properties: array size of 4.
[0.898] Instantiated 69,399 transitional clauses.
[0.922] Instantiated 342,047 universal clauses.
[0.922] Instantiated and added clauses for a total of 412,950 clauses.
[0.922] The encoding contains a total of 25,671 distinct variables.
[0.922] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.924] Executed solver; result: UNSAT.
[0.924] 
[0.924] *************************************
[0.924] * * *   M a k e s p a n     3   * * *
[0.924] *************************************
[0.939] Computed next depth properties: array size of 14.
[1.072] Instantiated 193,973 transitional clauses.
[1.106] Instantiated 366,698 universal clauses.
[1.106] Instantiated and added clauses for a total of 973,621 clauses.
[1.106] The encoding contains a total of 46,955 distinct variables.
[1.106] Attempting solve with solver <glucose4> ...
c 14 assumptions
[1.108] Executed solver; result: UNSAT.
[1.108] 
[1.108] *************************************
[1.108] * * *   M a k e s p a n     4   * * *
[1.108] *************************************
[1.127] Computed next depth properties: array size of 32.
[1.266] Instantiated 186,501 transitional clauses.
[1.303] Instantiated 129,108 universal clauses.
[1.303] Instantiated and added clauses for a total of 1,289,230 clauses.
[1.303] The encoding contains a total of 58,772 distinct variables.
[1.303] Attempting solve with solver <glucose4> ...
c 32 assumptions
[1.306] Executed solver; result: UNSAT.
[1.306] 
[1.306] *************************************
[1.306] * * *   M a k e s p a n     5   * * *
[1.306] *************************************
[1.333] Computed next depth properties: array size of 52.
[1.480] Instantiated 179,192 transitional clauses.
[1.525] Instantiated 123,094 universal clauses.
[1.525] Instantiated and added clauses for a total of 1,591,516 clauses.
[1.525] The encoding contains a total of 69,804 distinct variables.
[1.525] Attempting solve with solver <glucose4> ...
c 52 assumptions
[1.529] Executed solver; result: UNSAT.
[1.529] 
[1.529] *************************************
[1.529] * * *   M a k e s p a n     6   * * *
[1.529] *************************************
[1.564] Computed next depth properties: array size of 74.
[1.717] Instantiated 171,775 transitional clauses.
[1.770] Instantiated 116,291 universal clauses.
[1.770] Instantiated and added clauses for a total of 1,879,582 clauses.
[1.770] The encoding contains a total of 79,978 distinct variables.
[1.770] Attempting solve with solver <glucose4> ...
c 74 assumptions
[1.773] Executed solver; result: UNSAT.
[1.773] 
[1.773] *************************************
[1.773] * * *   M a k e s p a n     7   * * *
[1.773] *************************************
[1.817] Computed next depth properties: array size of 98.
[1.980] Instantiated 164,258 transitional clauses.
[2.040] Instantiated 108,669 universal clauses.
[2.040] Instantiated and added clauses for a total of 2,152,509 clauses.
[2.040] The encoding contains a total of 89,231 distinct variables.
[2.040] Attempting solve with solver <glucose4> ...
c 98 assumptions
[2.049] Executed solver; result: UNSAT.
[2.049] 
[2.049] *************************************
[2.049] * * *   M a k e s p a n     8   * * *
[2.049] *************************************
[2.102] Computed next depth properties: array size of 124.
[2.279] Instantiated 155,896 transitional clauses.
[2.345] Instantiated 95,645 universal clauses.
[2.345] Instantiated and added clauses for a total of 2,404,050 clauses.
[2.345] The encoding contains a total of 96,726 distinct variables.
[2.345] Attempting solve with solver <glucose4> ...
c 124 assumptions
[2.348] Executed solver; result: UNSAT.
[2.348] 
[2.348] *************************************
[2.348] * * *   M a k e s p a n     9   * * *
[2.348] *************************************
[2.408] Computed next depth properties: array size of 136.
[2.592] Instantiated 140,388 transitional clauses.
[2.661] Instantiated 81,759 universal clauses.
[2.661] Instantiated and added clauses for a total of 2,626,197 clauses.
[2.661] The encoding contains a total of 103,017 distinct variables.
[2.661] Attempting solve with solver <glucose4> ...
c 136 assumptions
[2.674] Executed solver; result: UNSAT.
[2.674] 
[2.674] *************************************
[2.674] * * *   M a k e s p a n    10   * * *
[2.674] *************************************
[2.739] Computed next depth properties: array size of 148.
[2.931] Instantiated 140,388 transitional clauses.
[3.005] Instantiated 82,943 universal clauses.
[3.005] Instantiated and added clauses for a total of 2,849,528 clauses.
[3.005] The encoding contains a total of 109,320 distinct variables.
[3.005] Attempting solve with solver <glucose4> ...
c 148 assumptions
[3.039] Executed solver; result: UNSAT.
[3.039] 
[3.039] *************************************
[3.039] * * *   M a k e s p a n    11   * * *
[3.039] *************************************
[3.108] Computed next depth properties: array size of 160.
[3.303] Instantiated 140,388 transitional clauses.
[3.381] Instantiated 84,127 universal clauses.
[3.381] Instantiated and added clauses for a total of 3,074,043 clauses.
[3.381] The encoding contains a total of 115,635 distinct variables.
[3.381] Attempting solve with solver <glucose4> ...
c 160 assumptions
[3.415] Executed solver; result: UNSAT.
[3.415] 
[3.415] *************************************
[3.415] * * *   M a k e s p a n    12   * * *
[3.415] *************************************
[3.489] Computed next depth properties: array size of 172.
[3.688] Instantiated 140,388 transitional clauses.
[3.771] Instantiated 85,311 universal clauses.
[3.771] Instantiated and added clauses for a total of 3,299,742 clauses.
[3.771] The encoding contains a total of 121,962 distinct variables.
[3.771] Attempting solve with solver <glucose4> ...
c 172 assumptions
[3.808] Executed solver; result: UNSAT.
[3.808] 
[3.808] *************************************
[3.808] * * *   M a k e s p a n    13   * * *
[3.808] *************************************
[3.888] Computed next depth properties: array size of 184.
[4.097] Instantiated 140,388 transitional clauses.
[4.185] Instantiated 86,495 universal clauses.
[4.185] Instantiated and added clauses for a total of 3,526,625 clauses.
[4.185] The encoding contains a total of 128,301 distinct variables.
[4.185] Attempting solve with solver <glucose4> ...
c 184 assumptions
c last restart ## conflicts  :  40 1081 
[25.689] Executed solver; result: SAT.
[25.689] Solver returned SAT; a solution has been found at makespan 13.
79
solution 12317 1
12317 1217 1182 1211 1183 2045 2043 2046 2047 2044 1212 1176 1177 1182 1281 1246 1275 1247 2048 2050 2051 2052 2049 1276 1240 1241 1246 1345 1310 1339 1311 2053 2054 2055 2057 2056 1340 1304 1305 1310 1409 1374 1403 1375 2062 2060 2058 2061 2059 1404 1368 1369 1374 1471 1411 1467 1413 2066 2063 2064 2067 2065 1468 1424 1425 1411 1535 1475 1531 1477 2069 2072 2071 2068 2070 1532 1488 1489 1475 
[25.695] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.003] Parsed head comment information.
[0.397] Processed problem encoding.
[0.578] Calculated possible fact changes of composite elements.
[0.578] Initialized instantiation procedure.
[0.578] 
[0.578] *************************************
[0.578] * * *   M a k e s p a n     0   * * *
[0.578] *************************************
[0.580] Instantiated 563 initial clauses.
[0.580] The encoding contains a total of 385 distinct variables.
[0.580] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.580] Executed solver; result: UNSAT.
[0.580] 
[0.580] *************************************
[0.580] * * *   M a k e s p a n     1   * * *
[0.580] *************************************
[0.581] Computed next depth properties: array size of 3.
[0.581] Instantiated 91 transitional clauses.
[0.584] Instantiated 731 universal clauses.
[0.584] Instantiated and added clauses for a total of 1,385 clauses.
[0.584] The encoding contains a total of 610 distinct variables.
[0.584] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.584] Executed solver; result: UNSAT.
[0.584] 
[0.584] *************************************
[0.584] * * *   M a k e s p a n     2   * * *
[0.584] *************************************
[0.587] Computed next depth properties: array size of 4.
[0.602] Instantiated 50,620 transitional clauses.
[0.619] Instantiated 248,462 universal clauses.
[0.619] Instantiated and added clauses for a total of 300,467 clauses.
[0.619] The encoding contains a total of 18,983 distinct variables.
[0.619] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.621] Executed solver; result: UNSAT.
[0.621] 
[0.621] *************************************
[0.621] * * *   M a k e s p a n     3   * * *
[0.621] *************************************
[0.631] Computed next depth properties: array size of 14.
[0.716] Instantiated 123,677 transitional clauses.
[0.744] Instantiated 268,983 universal clauses.
[0.744] Instantiated and added clauses for a total of 693,127 clauses.
[0.744] The encoding contains a total of 34,334 distinct variables.
[0.744] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.745] Executed solver; result: UNSAT.
[0.745] 
[0.745] *************************************
[0.745] * * *   M a k e s p a n     4   * * *
[0.745] *************************************
[0.758] Computed next depth properties: array size of 32.
[0.847] Instantiated 111,002 transitional clauses.
[0.874] Instantiated 98,086 universal clauses.
[0.874] Instantiated and added clauses for a total of 902,215 clauses.
[0.874] The encoding contains a total of 43,160 distinct variables.
[0.874] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.876] Executed solver; result: UNSAT.
[0.876] 
[0.876] *************************************
[0.876] * * *   M a k e s p a n     5   * * *
[0.876] *************************************
[0.895] Computed next depth properties: array size of 52.
[0.989] Instantiated 107,998 transitional clauses.
[1.022] Instantiated 96,642 universal clauses.
[1.022] Instantiated and added clauses for a total of 1,106,855 clauses.
[1.022] The encoding contains a total of 51,715 distinct variables.
[1.022] Attempting solve with solver <glucose4> ...
c 52 assumptions
[1.024] Executed solver; result: UNSAT.
[1.024] 
[1.024] *************************************
[1.024] * * *   M a k e s p a n     6   * * *
[1.024] *************************************
[1.049] Computed next depth properties: array size of 74.
[1.151] Instantiated 104,922 transitional clauses.
[1.190] Instantiated 94,704 universal clauses.
[1.190] Instantiated and added clauses for a total of 1,306,481 clauses.
[1.190] The encoding contains a total of 59,949 distinct variables.
[1.190] Attempting solve with solver <glucose4> ...
c 74 assumptions
[1.193] Executed solver; result: UNSAT.
[1.193] 
[1.193] *************************************
[1.193] * * *   M a k e s p a n     7   * * *
[1.193] *************************************
[1.224] Computed next depth properties: array size of 98.
[1.335] Instantiated 101,782 transitional clauses.
[1.382] Instantiated 92,248 universal clauses.
[1.382] Instantiated and added clauses for a total of 1,500,511 clauses.
[1.382] The encoding contains a total of 67,820 distinct variables.
[1.382] Attempting solve with solver <glucose4> ...
c 98 assumptions
[1.385] Executed solver; result: UNSAT.
[1.385] 
[1.385] *************************************
[1.385] * * *   M a k e s p a n     8   * * *
[1.385] *************************************
[1.422] Computed next depth properties: array size of 124.
[1.539] Instantiated 98,578 transitional clauses.
[1.590] Instantiated 89,250 universal clauses.
[1.590] Instantiated and added clauses for a total of 1,688,339 clauses.
[1.590] The encoding contains a total of 75,286 distinct variables.
[1.590] Attempting solve with solver <glucose4> ...
c 124 assumptions
[1.592] Executed solver; result: UNSAT.
[1.592] 
[1.592] *************************************
[1.592] * * *   M a k e s p a n     9   * * *
[1.592] *************************************
[1.638] Computed next depth properties: array size of 152.
[1.763] Instantiated 94,994 transitional clauses.
[1.820] Instantiated 83,724 universal clauses.
[1.820] Instantiated and added clauses for a total of 1,867,057 clauses.
[1.820] The encoding contains a total of 81,966 distinct variables.
[1.820] Attempting solve with solver <glucose4> ...
c 152 assumptions
[1.828] Executed solver; result: UNSAT.
[1.828] 
[1.828] *************************************
[1.828] * * *   M a k e s p a n    10   * * *
[1.828] *************************************
[1.879] Computed next depth properties: array size of 166.
[2.009] Instantiated 88,088 transitional clauses.
[2.069] Instantiated 75,818 universal clauses.
[2.069] Instantiated and added clauses for a total of 2,030,963 clauses.
[2.069] The encoding contains a total of 87,769 distinct variables.
[2.069] Attempting solve with solver <glucose4> ...
c 166 assumptions
[2.077] Executed solver; result: UNSAT.
[2.077] 
[2.077] *************************************
[2.077] * * *   M a k e s p a n    11   * * *
[2.077] *************************************
[2.133] Computed next depth properties: array size of 180.
[2.270] Instantiated 88,088 transitional clauses.
[2.334] Instantiated 77,010 universal clauses.
[2.334] Instantiated and added clauses for a total of 2,196,061 clauses.
[2.334] The encoding contains a total of 93,586 distinct variables.
[2.334] Attempting solve with solver <glucose4> ...
c 180 assumptions
[2.352] Executed solver; result: UNSAT.
[2.352] 
[2.352] *************************************
[2.352] * * *   M a k e s p a n    12   * * *
[2.352] *************************************
[2.412] Computed next depth properties: array size of 194.
[2.550] Instantiated 88,088 transitional clauses.
[2.617] Instantiated 78,202 universal clauses.
[2.617] Instantiated and added clauses for a total of 2,362,351 clauses.
[2.617] The encoding contains a total of 99,417 distinct variables.
[2.617] Attempting solve with solver <glucose4> ...
c 194 assumptions
[2.629] Executed solver; result: UNSAT.
[2.629] 
[2.629] *************************************
[2.629] * * *   M a k e s p a n    13   * * *
[2.629] *************************************
[2.693] Computed next depth properties: array size of 208.
[2.835] Instantiated 88,088 transitional clauses.
[2.906] Instantiated 79,394 universal clauses.
[2.906] Instantiated and added clauses for a total of 2,529,833 clauses.
[2.906] The encoding contains a total of 105,262 distinct variables.
[2.906] Attempting solve with solver <glucose4> ...
c 208 assumptions
c last restart ## conflicts  :  39 1246 
[17.840] Executed solver; result: SAT.
[17.840] Solver returned SAT; a solution has been found at makespan 13.
85
solution 8952 1
8952 372 361 373 362 1405 1403 1404 1406 374 343 344 361 424 403 421 404 1408 1407 1410 1409 422 397 398 403 472 451 469 452 1412 1411 1414 1413 470 445 446 451 520 499 517 500 1415 1418 1416 1417 518 493 494 499 568 547 565 548 1419 1422 1420 1421 566 541 542 547 616 595 613 596 1426 1424 1423 1425 614 589 590 595 660 649 661 650 1427 1428 1429 1430 662 631 632 649 
[17.846] Exiting.

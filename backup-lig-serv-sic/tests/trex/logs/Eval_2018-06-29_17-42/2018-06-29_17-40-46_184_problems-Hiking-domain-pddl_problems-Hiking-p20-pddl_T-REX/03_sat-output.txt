Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.005] Parsed head comment information.
[0.759] Processed problem encoding.
[1.347] Calculated possible fact changes of composite elements.
[1.348] Initialized instantiation procedure.
[1.348] 
[1.348] *************************************
[1.348] * * *   M a k e s p a n     0   * * *
[1.348] *************************************
[1.354] Instantiated 697 initial clauses.
[1.354] The encoding contains a total of 476 distinct variables.
[1.354] Attempting solve with solver <glucose4> ...
c 2 assumptions
[1.354] Executed solver; result: UNSAT.
[1.354] 
[1.354] *************************************
[1.354] * * *   M a k e s p a n     1   * * *
[1.354] *************************************
[1.355] Computed next depth properties: array size of 3.
[1.356] Instantiated 112 transitional clauses.
[1.362] Instantiated 904 universal clauses.
[1.362] Instantiated and added clauses for a total of 1,713 clauses.
[1.362] The encoding contains a total of 745 distinct variables.
[1.362] Attempting solve with solver <glucose4> ...
c 3 assumptions
[1.362] Executed solver; result: UNSAT.
[1.362] 
[1.362] *************************************
[1.362] * * *   M a k e s p a n     2   * * *
[1.362] *************************************
[1.368] Computed next depth properties: array size of 4.
[1.397] Instantiated 93,861 transitional clauses.
[1.432] Instantiated 477,580 universal clauses.
[1.432] Instantiated and added clauses for a total of 573,154 clauses.
[1.432] The encoding contains a total of 34,415 distinct variables.
[1.432] Attempting solve with solver <glucose4> ...
c 4 assumptions
[1.436] Executed solver; result: UNSAT.
[1.436] 
[1.436] *************************************
[1.436] * * *   M a k e s p a n     3   * * *
[1.436] *************************************
[1.457] Computed next depth properties: array size of 14.
[1.668] Instantiated 260,393 transitional clauses.
[1.713] Instantiated 507,138 universal clauses.
[1.713] Instantiated and added clauses for a total of 1,340,685 clauses.
[1.713] The encoding contains a total of 62,063 distinct variables.
[1.713] Attempting solve with solver <glucose4> ...
c 14 assumptions
[1.715] Executed solver; result: UNSAT.
[1.715] 
[1.715] *************************************
[1.715] * * *   M a k e s p a n     4   * * *
[1.715] *************************************
[1.742] Computed next depth properties: array size of 32.
[1.959] Instantiated 244,346 transitional clauses.
[2.008] Instantiated 166,545 universal clauses.
[2.008] Instantiated and added clauses for a total of 1,751,576 clauses.
[2.008] The encoding contains a total of 76,741 distinct variables.
[2.008] Attempting solve with solver <glucose4> ...
c 32 assumptions
[2.012] Executed solver; result: UNSAT.
[2.012] 
[2.012] *************************************
[2.012] * * *   M a k e s p a n     5   * * *
[2.012] *************************************
[2.049] Computed next depth properties: array size of 52.
[2.276] Instantiated 237,137 transitional clauses.
[2.336] Instantiated 161,421 universal clauses.
[2.336] Instantiated and added clauses for a total of 2,150,134 clauses.
[2.336] The encoding contains a total of 90,701 distinct variables.
[2.336] Attempting solve with solver <glucose4> ...
c 52 assumptions
[2.341] Executed solver; result: UNSAT.
[2.341] 
[2.341] *************************************
[2.341] * * *   M a k e s p a n     6   * * *
[2.341] *************************************
[2.389] Computed next depth properties: array size of 74.
[2.629] Instantiated 229,820 transitional clauses.
[2.699] Instantiated 155,513 universal clauses.
[2.699] Instantiated and added clauses for a total of 2,535,467 clauses.
[2.699] The encoding contains a total of 103,870 distinct variables.
[2.699] Attempting solve with solver <glucose4> ...
c 74 assumptions
[2.704] Executed solver; result: UNSAT.
[2.704] 
[2.704] *************************************
[2.704] * * *   M a k e s p a n     7   * * *
[2.704] *************************************
[2.764] Computed next depth properties: array size of 98.
[3.016] Instantiated 222,403 transitional clauses.
[3.096] Instantiated 148,791 universal clauses.
[3.096] Instantiated and added clauses for a total of 2,906,661 clauses.
[3.096] The encoding contains a total of 116,185 distinct variables.
[3.096] Attempting solve with solver <glucose4> ...
c 98 assumptions
[3.101] Executed solver; result: UNSAT.
[3.101] 
[3.101] *************************************
[3.101] * * *   M a k e s p a n     8   * * *
[3.101] *************************************
[3.173] Computed next depth properties: array size of 124.
[3.446] Instantiated 214,886 transitional clauses.
[3.537] Instantiated 141,225 universal clauses.
[3.537] Instantiated and added clauses for a total of 3,262,772 clauses.
[3.537] The encoding contains a total of 127,583 distinct variables.
[3.537] Attempting solve with solver <glucose4> ...
c 124 assumptions
[3.550] Executed solver; result: UNSAT.
[3.550] 
[3.550] *************************************
[3.550] * * *   M a k e s p a n     9   * * *
[3.550] *************************************
[3.635] Computed next depth properties: array size of 152.
[3.934] Instantiated 206,524 transitional clauses.
[4.034] Instantiated 128,232 universal clauses.
[4.034] Instantiated and added clauses for a total of 3,597,528 clauses.
[4.034] The encoding contains a total of 137,227 distinct variables.
[4.034] Attempting solve with solver <glucose4> ...
c 152 assumptions
[4.038] Executed solver; result: UNSAT.
[4.038] 
[4.038] *************************************
[4.038] * * *   M a k e s p a n    10   * * *
[4.038] *************************************
[4.134] Computed next depth properties: array size of 166.
[4.450] Instantiated 191,016 transitional clauses.
[4.555] Instantiated 113,984 universal clauses.
[4.555] Instantiated and added clauses for a total of 3,902,528 clauses.
[4.555] The encoding contains a total of 145,655 distinct variables.
[4.555] Attempting solve with solver <glucose4> ...
c 166 assumptions
[4.587] Executed solver; result: UNSAT.
[4.587] 
[4.587] *************************************
[4.587] * * *   M a k e s p a n    11   * * *
[4.587] *************************************
[4.691] Computed next depth properties: array size of 180.
[5.011] Instantiated 191,016 transitional clauses.
[5.122] Instantiated 115,470 universal clauses.
[5.122] Instantiated and added clauses for a total of 4,209,014 clauses.
[5.122] The encoding contains a total of 154,097 distinct variables.
[5.122] Attempting solve with solver <glucose4> ...
c 180 assumptions
[5.169] Executed solver; result: UNSAT.
[5.169] 
[5.169] *************************************
[5.169] * * *   M a k e s p a n    12   * * *
[5.169] *************************************
[5.280] Computed next depth properties: array size of 194.
[5.609] Instantiated 191,016 transitional clauses.
[5.727] Instantiated 116,956 universal clauses.
[5.727] Instantiated and added clauses for a total of 4,516,986 clauses.
[5.727] The encoding contains a total of 162,553 distinct variables.
[5.727] Attempting solve with solver <glucose4> ...
c 194 assumptions
[5.797] Executed solver; result: UNSAT.
[5.797] 
[5.797] *************************************
[5.797] * * *   M a k e s p a n    13   * * *
[5.797] *************************************
[5.915] Computed next depth properties: array size of 208.
[6.251] Instantiated 191,016 transitional clauses.
[6.375] Instantiated 118,442 universal clauses.
[6.375] Instantiated and added clauses for a total of 4,826,444 clauses.
[6.375] The encoding contains a total of 171,023 distinct variables.
[6.375] Attempting solve with solver <glucose4> ...
c 208 assumptions
[6.462] Executed solver; result: UNSAT.
[6.462] 
[6.462] *************************************
[6.462] * * *   M a k e s p a n    14   * * *
[6.462] *************************************
[6.588] Computed next depth properties: array size of 222.
[6.932] Instantiated 191,016 transitional clauses.
[7.063] Instantiated 119,928 universal clauses.
[7.063] Instantiated and added clauses for a total of 5,137,388 clauses.
[7.063] The encoding contains a total of 179,507 distinct variables.
[7.063] Attempting solve with solver <glucose4> ...
c 222 assumptions
c last restart ## conflicts  :  154 1532 
[92.375] Executed solver; result: SAT.
[92.375] Solver returned SAT; a solution has been found at makespan 14.
92
solution 16441 1
16441 1857 1820 1851 1821 2384 2383 2385 2386 2387 1852 1816 1817 1820 1918 1859 1915 1861 2388 2390 2392 2389 2391 1916 1869 1870 1859 1982 1923 1979 1925 2394 2393 2397 2396 2395 1980 1933 1934 1923 2046 1987 2043 1989 2400 2398 2399 2402 2401 2044 1997 1998 1987 2110 2051 2107 2053 2404 2405 2407 2403 2406 2108 2061 2062 2051 2177 2140 2171 2141 2412 2411 2410 2409 2408 2172 2136 2137 2140 2238 2179 2235 2181 2414 2416 2413 2417 2415 2236 2189 2190 2179 
[92.385] Exiting.

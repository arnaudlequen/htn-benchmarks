Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.002] Parsed head comment information.
[0.290] Processed problem encoding.
[0.404] Calculated possible fact changes of composite elements.
[0.404] Initialized instantiation procedure.
[0.404] 
[0.404] *************************************
[0.404] * * *   M a k e s p a n     0   * * *
[0.404] *************************************
[0.407] Instantiated 496 initial clauses.
[0.407] The encoding contains a total of 339 distinct variables.
[0.407] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.407] Executed solver; result: UNSAT.
[0.407] 
[0.407] *************************************
[0.407] * * *   M a k e s p a n     1   * * *
[0.407] *************************************
[0.408] Computed next depth properties: array size of 3.
[0.408] Instantiated 78 transitional clauses.
[0.410] Instantiated 642 universal clauses.
[0.410] Instantiated and added clauses for a total of 1,216 clauses.
[0.410] The encoding contains a total of 539 distinct variables.
[0.410] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.410] Executed solver; result: UNSAT.
[0.410] 
[0.410] *************************************
[0.410] * * *   M a k e s p a n     2   * * *
[0.410] *************************************
[0.413] Computed next depth properties: array size of 4.
[0.424] Instantiated 37,434 transitional clauses.
[0.437] Instantiated 177,794 universal clauses.
[0.437] Instantiated and added clauses for a total of 216,444 clauses.
[0.437] The encoding contains a total of 14,178 distinct variables.
[0.437] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.438] Executed solver; result: UNSAT.
[0.438] 
[0.438] *************************************
[0.438] * * *   M a k e s p a n     3   * * *
[0.438] *************************************
[0.445] Computed next depth properties: array size of 14.
[0.506] Instantiated 92,053 transitional clauses.
[0.523] Instantiated 194,038 universal clauses.
[0.523] Instantiated and added clauses for a total of 502,535 clauses.
[0.523] The encoding contains a total of 25,971 distinct variables.
[0.523] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.524] Executed solver; result: UNSAT.
[0.524] 
[0.524] *************************************
[0.524] * * *   M a k e s p a n     4   * * *
[0.524] *************************************
[0.534] Computed next depth properties: array size of 32.
[0.599] Instantiated 84,654 transitional clauses.
[0.620] Instantiated 75,522 universal clauses.
[0.620] Instantiated and added clauses for a total of 662,711 clauses.
[0.620] The encoding contains a total of 33,063 distinct variables.
[0.620] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.622] Executed solver; result: UNSAT.
[0.622] 
[0.622] *************************************
[0.622] * * *   M a k e s p a n     5   * * *
[0.622] *************************************
[0.636] Computed next depth properties: array size of 52.
[0.701] Instantiated 81,586 transitional clauses.
[0.726] Instantiated 73,454 universal clauses.
[0.726] Instantiated and added clauses for a total of 817,751 clauses.
[0.726] The encoding contains a total of 39,838 distinct variables.
[0.726] Attempting solve with solver <glucose4> ...
c 52 assumptions
[0.728] Executed solver; result: UNSAT.
[0.728] 
[0.728] *************************************
[0.728] * * *   M a k e s p a n     6   * * *
[0.728] *************************************
[0.747] Computed next depth properties: array size of 74.
[0.822] Instantiated 78,446 transitional clauses.
[0.852] Instantiated 70,884 universal clauses.
[0.852] Instantiated and added clauses for a total of 967,081 clauses.
[0.852] The encoding contains a total of 46,246 distinct variables.
[0.852] Attempting solve with solver <glucose4> ...
c 74 assumptions
[0.853] Executed solver; result: UNSAT.
[0.853] 
[0.853] *************************************
[0.853] * * *   M a k e s p a n     7   * * *
[0.853] *************************************
[0.877] Computed next depth properties: array size of 98.
[0.953] Instantiated 75,242 transitional clauses.
[0.987] Instantiated 67,788 universal clauses.
[0.987] Instantiated and added clauses for a total of 1,110,111 clauses.
[0.987] The encoding contains a total of 52,245 distinct variables.
[0.987] Attempting solve with solver <glucose4> ...
c 98 assumptions
[0.988] Executed solver; result: UNSAT.
[0.988] 
[0.988] *************************************
[0.988] * * *   M a k e s p a n     8   * * *
[0.988] *************************************
[1.017] Computed next depth properties: array size of 124.
[1.099] Instantiated 71,658 transitional clauses.
[1.136] Instantiated 62,180 universal clauses.
[1.136] Instantiated and added clauses for a total of 1,243,949 clauses.
[1.136] The encoding contains a total of 57,454 distinct variables.
[1.136] Attempting solve with solver <glucose4> ...
c 124 assumptions
[1.141] Executed solver; result: UNSAT.
[1.141] 
[1.141] *************************************
[1.141] * * *   M a k e s p a n     9   * * *
[1.141] *************************************
[1.173] Computed next depth properties: array size of 136.
[1.260] Instantiated 64,752 transitional clauses.
[1.301] Instantiated 54,438 universal clauses.
[1.301] Instantiated and added clauses for a total of 1,363,139 clauses.
[1.301] The encoding contains a total of 61,798 distinct variables.
[1.301] Attempting solve with solver <glucose4> ...
c 136 assumptions
[1.308] Executed solver; result: UNSAT.
[1.308] 
[1.308] *************************************
[1.308] * * *   M a k e s p a n    10   * * *
[1.308] *************************************
[1.342] Computed next depth properties: array size of 148.
[1.430] Instantiated 64,752 transitional clauses.
[1.472] Instantiated 55,388 universal clauses.
[1.472] Instantiated and added clauses for a total of 1,483,279 clauses.
[1.472] The encoding contains a total of 66,154 distinct variables.
[1.472] Attempting solve with solver <glucose4> ...
c 148 assumptions
[1.481] Executed solver; result: UNSAT.
[1.481] 
[1.481] *************************************
[1.481] * * *   M a k e s p a n    11   * * *
[1.481] *************************************
[1.519] Computed next depth properties: array size of 160.
[1.613] Instantiated 64,752 transitional clauses.
[1.657] Instantiated 56,338 universal clauses.
[1.657] Instantiated and added clauses for a total of 1,604,369 clauses.
[1.657] The encoding contains a total of 70,522 distinct variables.
[1.657] Attempting solve with solver <glucose4> ...
c 160 assumptions
[1.667] Executed solver; result: UNSAT.
[1.667] 
[1.667] *************************************
[1.667] * * *   M a k e s p a n    12   * * *
[1.667] *************************************
[1.707] Computed next depth properties: array size of 172.
[1.803] Instantiated 64,752 transitional clauses.
[1.850] Instantiated 57,288 universal clauses.
[1.850] Instantiated and added clauses for a total of 1,726,409 clauses.
[1.850] The encoding contains a total of 74,902 distinct variables.
[1.850] Attempting solve with solver <glucose4> ...
c 172 assumptions
c last restart ## conflicts  :  117 918 
[2.095] Executed solver; result: SAT.
[2.095] Solver returned SAT; a solution has been found at makespan 12.
73
solution 6733 1
6733 605 601 606 602 1182 1179 1180 1181 607 583 584 601 657 643 654 644 1183 1184 1185 1186 655 637 638 643 705 691 702 692 1188 1189 1187 1190 703 685 686 691 749 745 750 746 1192 1191 1194 1193 751 727 728 745 801 787 798 788 1195 1198 1196 1197 799 781 782 787 849 835 846 836 1202 1201 1199 1200 847 829 830 835 
[2.098] Exiting.

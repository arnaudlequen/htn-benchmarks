Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.149] Processed problem encoding.
[0.179] Calculated possible fact changes of composite elements.
[0.179] Initialized instantiation procedure.
[0.179] 
[0.179] *************************************
[0.179] * * *   M a k e s p a n     0   * * *
[0.179] *************************************
[0.180] Instantiated 378 initial clauses.
[0.180] The encoding contains a total of 259 distinct variables.
[0.180] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.180] Executed solver; result: UNSAT.
[0.180] 
[0.180] *************************************
[0.180] * * *   M a k e s p a n     1   * * *
[0.180] *************************************
[0.180] Computed next depth properties: array size of 3.
[0.181] Instantiated 60 transitional clauses.
[0.181] Instantiated 490 universal clauses.
[0.181] Instantiated and added clauses for a total of 928 clauses.
[0.181] The encoding contains a total of 420 distinct variables.
[0.181] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.181] Executed solver; result: UNSAT.
[0.181] 
[0.181] *************************************
[0.181] * * *   M a k e s p a n     2   * * *
[0.181] *************************************
[0.183] Computed next depth properties: array size of 4.
[0.187] Instantiated 17,183 transitional clauses.
[0.193] Instantiated 78,425 universal clauses.
[0.193] Instantiated and added clauses for a total of 96,536 clauses.
[0.193] The encoding contains a total of 6,815 distinct variables.
[0.193] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.194] Executed solver; result: UNSAT.
[0.194] 
[0.194] *************************************
[0.194] * * *   M a k e s p a n     3   * * *
[0.194] *************************************
[0.197] Computed next depth properties: array size of 14.
[0.217] Instantiated 36,697 transitional clauses.
[0.226] Instantiated 88,880 universal clauses.
[0.226] Instantiated and added clauses for a total of 222,113 clauses.
[0.226] The encoding contains a total of 12,655 distinct variables.
[0.226] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.227] Executed solver; result: UNSAT.
[0.227] 
[0.227] *************************************
[0.227] * * *   M a k e s p a n     4   * * *
[0.227] *************************************
[0.231] Computed next depth properties: array size of 32.
[0.254] Instantiated 31,865 transitional clauses.
[0.266] Instantiated 40,080 universal clauses.
[0.266] Instantiated and added clauses for a total of 294,058 clauses.
[0.266] The encoding contains a total of 16,656 distinct variables.
[0.266] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.267] Executed solver; result: UNSAT.
[0.267] 
[0.267] *************************************
[0.267] * * *   M a k e s p a n     5   * * *
[0.267] *************************************
[0.273] Computed next depth properties: array size of 52.
[0.295] Instantiated 30,864 transitional clauses.
[0.308] Instantiated 39,928 universal clauses.
[0.308] Instantiated and added clauses for a total of 364,850 clauses.
[0.308] The encoding contains a total of 20,572 distinct variables.
[0.308] Attempting solve with solver <glucose4> ...
c 52 assumptions
[0.309] Executed solver; result: UNSAT.
[0.309] 
[0.309] *************************************
[0.309] * * *   M a k e s p a n     6   * * *
[0.309] *************************************
[0.317] Computed next depth properties: array size of 74.
[0.343] Instantiated 29,819 transitional clauses.
[0.359] Instantiated 39,489 universal clauses.
[0.359] Instantiated and added clauses for a total of 434,158 clauses.
[0.359] The encoding contains a total of 24,372 distinct variables.
[0.359] Attempting solve with solver <glucose4> ...
c 74 assumptions
[0.360] Executed solver; result: UNSAT.
[0.360] 
[0.360] *************************************
[0.360] * * *   M a k e s p a n     7   * * *
[0.360] *************************************
[0.371] Computed next depth properties: array size of 98.
[0.400] Instantiated 28,738 transitional clauses.
[0.416] Instantiated 38,745 universal clauses.
[0.416] Instantiated and added clauses for a total of 501,641 clauses.
[0.416] The encoding contains a total of 28,031 distinct variables.
[0.416] Attempting solve with solver <glucose4> ...
c 98 assumptions
[0.417] Executed solver; result: UNSAT.
[0.417] 
[0.417] *************************************
[0.417] * * *   M a k e s p a n     8   * * *
[0.417] *************************************
[0.430] Computed next depth properties: array size of 124.
[0.458] Instantiated 27,516 transitional clauses.
[0.476] Instantiated 36,999 universal clauses.
[0.476] Instantiated and added clauses for a total of 566,156 clauses.
[0.476] The encoding contains a total of 31,402 distinct variables.
[0.476] Attempting solve with solver <glucose4> ...
c 124 assumptions
[0.478] Executed solver; result: UNSAT.
[0.478] 
[0.478] *************************************
[0.478] * * *   M a k e s p a n     9   * * *
[0.478] *************************************
[0.494] Computed next depth properties: array size of 136.
[0.526] Instantiated 24,996 transitional clauses.
[0.546] Instantiated 33,237 universal clauses.
[0.546] Instantiated and added clauses for a total of 624,389 clauses.
[0.546] The encoding contains a total of 34,171 distinct variables.
[0.546] Attempting solve with solver <glucose4> ...
c 136 assumptions
[0.549] Executed solver; result: UNSAT.
[0.549] 
[0.549] *************************************
[0.549] * * *   M a k e s p a n    10   * * *
[0.549] *************************************
[0.567] Computed next depth properties: array size of 148.
[0.598] Instantiated 24,996 transitional clauses.
[0.618] Instantiated 33,953 universal clauses.
[0.618] Instantiated and added clauses for a total of 683,338 clauses.
[0.618] The encoding contains a total of 36,952 distinct variables.
[0.618] Attempting solve with solver <glucose4> ...
c 148 assumptions
[0.622] Executed solver; result: UNSAT.
[0.622] 
[0.622] *************************************
[0.622] * * *   M a k e s p a n    11   * * *
[0.622] *************************************
[0.639] Computed next depth properties: array size of 160.
[0.670] Instantiated 24,996 transitional clauses.
[0.693] Instantiated 34,669 universal clauses.
[0.693] Instantiated and added clauses for a total of 743,003 clauses.
[0.693] The encoding contains a total of 39,745 distinct variables.
[0.693] Attempting solve with solver <glucose4> ...
c 160 assumptions
c last restart ## conflicts  :  102 773 
[0.726] Executed solver; result: SAT.
[0.726] Solver returned SAT; a solution has been found at makespan 11.
67
solution 3148 1
3148 237 220 235 221 653 652 651 236 214 215 220 268 258 269 259 656 655 654 270 245 246 258 302 292 303 293 657 658 659 304 279 280 292 336 326 337 327 661 662 660 338 313 314 326 370 360 371 361 665 664 663 372 347 348 360 404 394 405 395 668 667 666 406 381 382 394 
[0.728] Exiting.

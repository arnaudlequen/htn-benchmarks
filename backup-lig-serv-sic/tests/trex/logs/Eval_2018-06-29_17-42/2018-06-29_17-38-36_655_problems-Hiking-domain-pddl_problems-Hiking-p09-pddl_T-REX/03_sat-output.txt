Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.092] Processed problem encoding.
[0.102] Calculated possible fact changes of composite elements.
[0.102] Initialized instantiation procedure.
[0.102] 
[0.102] *************************************
[0.102] * * *   M a k e s p a n     0   * * *
[0.102] *************************************
[0.102] Instantiated 295 initial clauses.
[0.102] The encoding contains a total of 201 distinct variables.
[0.102] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.102] Executed solver; result: UNSAT.
[0.102] 
[0.102] *************************************
[0.102] * * *   M a k e s p a n     1   * * *
[0.102] *************************************
[0.102] Computed next depth properties: array size of 3.
[0.103] Instantiated 39 transitional clauses.
[0.103] Instantiated 375 universal clauses.
[0.103] Instantiated and added clauses for a total of 709 clauses.
[0.103] The encoding contains a total of 332 distinct variables.
[0.103] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.103] Executed solver; result: UNSAT.
[0.103] 
[0.103] *************************************
[0.103] * * *   M a k e s p a n     2   * * *
[0.103] *************************************
[0.104] Computed next depth properties: array size of 4.
[0.108] Instantiated 9,876 transitional clauses.
[0.113] Instantiated 45,570 universal clauses.
[0.113] Instantiated and added clauses for a total of 56,155 clauses.
[0.113] The encoding contains a total of 4,091 distinct variables.
[0.113] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.113] Executed solver; result: UNSAT.
[0.113] 
[0.113] *************************************
[0.113] * * *   M a k e s p a n     3   * * *
[0.113] *************************************
[0.115] Computed next depth properties: array size of 14.
[0.128] Instantiated 25,981 transitional clauses.
[0.135] Instantiated 55,009 universal clauses.
[0.135] Instantiated and added clauses for a total of 137,145 clauses.
[0.135] The encoding contains a total of 8,110 distinct variables.
[0.135] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.135] Executed solver; result: UNSAT.
[0.135] 
[0.135] *************************************
[0.135] * * *   M a k e s p a n     4   * * *
[0.135] *************************************
[0.138] Computed next depth properties: array size of 32.
[0.149] Instantiated 26,730 transitional clauses.
[0.155] Instantiated 28,282 universal clauses.
[0.155] Instantiated and added clauses for a total of 192,157 clauses.
[0.155] The encoding contains a total of 10,996 distinct variables.
[0.155] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.156] Executed solver; result: UNSAT.
[0.156] 
[0.156] *************************************
[0.156] * * *   M a k e s p a n     5   * * *
[0.156] *************************************
[0.159] Computed next depth properties: array size of 52.
[0.172] Instantiated 23,154 transitional clauses.
[0.179] Instantiated 22,252 universal clauses.
[0.179] Instantiated and added clauses for a total of 237,563 clauses.
[0.179] The encoding contains a total of 13,092 distinct variables.
[0.179] Attempting solve with solver <glucose4> ...
c 52 assumptions
[0.179] Executed solver; result: UNSAT.
[0.179] 
[0.179] *************************************
[0.179] * * *   M a k e s p a n     6   * * *
[0.179] *************************************
[0.184] Computed next depth properties: array size of 58.
[0.196] Instantiated 16,248 transitional clauses.
[0.202] Instantiated 14,334 universal clauses.
[0.202] Instantiated and added clauses for a total of 268,145 clauses.
[0.202] The encoding contains a total of 14,331 distinct variables.
[0.202] Attempting solve with solver <glucose4> ...
c 58 assumptions
[0.204] Executed solver; result: UNSAT.
[0.204] 
[0.204] *************************************
[0.204] * * *   M a k e s p a n     7   * * *
[0.204] *************************************
[0.209] Computed next depth properties: array size of 64.
[0.220] Instantiated 16,248 transitional clauses.
[0.227] Instantiated 14,702 universal clauses.
[0.227] Instantiated and added clauses for a total of 299,095 clauses.
[0.227] The encoding contains a total of 15,576 distinct variables.
[0.227] Attempting solve with solver <glucose4> ...
c 64 assumptions
[0.229] Executed solver; result: UNSAT.
[0.229] 
[0.229] *************************************
[0.229] * * *   M a k e s p a n     8   * * *
[0.229] *************************************
[0.235] Computed next depth properties: array size of 70.
[0.246] Instantiated 16,248 transitional clauses.
[0.253] Instantiated 15,070 universal clauses.
[0.253] Instantiated and added clauses for a total of 330,413 clauses.
[0.253] The encoding contains a total of 16,827 distinct variables.
[0.253] Attempting solve with solver <glucose4> ...
c 70 assumptions
[0.260] Executed solver; result: UNSAT.
[0.260] 
[0.260] *************************************
[0.260] * * *   M a k e s p a n     9   * * *
[0.260] *************************************
[0.266] Computed next depth properties: array size of 76.
[0.277] Instantiated 16,248 transitional clauses.
[0.285] Instantiated 15,438 universal clauses.
[0.285] Instantiated and added clauses for a total of 362,099 clauses.
[0.285] The encoding contains a total of 18,084 distinct variables.
[0.285] Attempting solve with solver <glucose4> ...
c 76 assumptions
c last restart ## conflicts  :  9 350 
[0.338] Executed solver; result: SAT.
[0.338] Solver returned SAT; a solution has been found at makespan 9.
37
solution 1970 1
1970 471 451 469 452 603 604 605 606 470 442 443 451 519 499 517 500 607 608 610 609 518 490 491 499 564 551 565 552 612 614 611 613 566 535 536 551 
[0.338] Exiting.

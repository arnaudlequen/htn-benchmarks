Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.024] Processed problem encoding.
[0.025] Calculated possible fact changes of composite elements.
[0.025] Initialized instantiation procedure.
[0.025] 
[0.025] *************************************
[0.025] * * *   M a k e s p a n     0   * * *
[0.025] *************************************
[0.025] Instantiated 174 initial clauses.
[0.025] The encoding contains a total of 119 distinct variables.
[0.025] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.025] Executed solver; result: UNSAT.
[0.025] 
[0.025] *************************************
[0.025] * * *   M a k e s p a n     1   * * *
[0.025] *************************************
[0.025] Computed next depth properties: array size of 3.
[0.025] Instantiated 20 transitional clauses.
[0.026] Instantiated 218 universal clauses.
[0.026] Instantiated and added clauses for a total of 412 clauses.
[0.026] The encoding contains a total of 206 distinct variables.
[0.026] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.026] Executed solver; result: UNSAT.
[0.026] 
[0.026] *************************************
[0.026] * * *   M a k e s p a n     2   * * *
[0.026] *************************************
[0.026] Computed next depth properties: array size of 4.
[0.027] Instantiated 2,151 transitional clauses.
[0.029] Instantiated 9,313 universal clauses.
[0.029] Instantiated and added clauses for a total of 11,876 clauses.
[0.029] The encoding contains a total of 1,087 distinct variables.
[0.029] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.029] Executed solver; result: UNSAT.
[0.029] 
[0.029] *************************************
[0.029] * * *   M a k e s p a n     3   * * *
[0.029] *************************************
[0.030] Computed next depth properties: array size of 14.
[0.034] Instantiated 5,281 transitional clauses.
[0.037] Instantiated 13,190 universal clauses.
[0.037] Instantiated and added clauses for a total of 30,347 clauses.
[0.037] The encoding contains a total of 2,331 distinct variables.
[0.037] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.037] Executed solver; result: UNSAT.
[0.037] 
[0.037] *************************************
[0.037] * * *   M a k e s p a n     4   * * *
[0.037] *************************************
[0.038] Computed next depth properties: array size of 32.
[0.042] Instantiated 5,324 transitional clauses.
[0.046] Instantiated 8,091 universal clauses.
[0.046] Instantiated and added clauses for a total of 43,762 clauses.
[0.046] The encoding contains a total of 3,322 distinct variables.
[0.046] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.046] Executed solver; result: UNSAT.
[0.046] 
[0.046] *************************************
[0.046] * * *   M a k e s p a n     5   * * *
[0.046] *************************************
[0.048] Computed next depth properties: array size of 36.
[0.051] Instantiated 2,812 transitional clauses.
[0.054] Instantiated 4,229 universal clauses.
[0.054] Instantiated and added clauses for a total of 50,803 clauses.
[0.054] The encoding contains a total of 3,737 distinct variables.
[0.054] Attempting solve with solver <glucose4> ...
c 36 assumptions
[0.054] Executed solver; result: UNSAT.
[0.054] 
[0.054] *************************************
[0.054] * * *   M a k e s p a n     6   * * *
[0.054] *************************************
[0.055] Computed next depth properties: array size of 40.
[0.058] Instantiated 2,812 transitional clauses.
[0.061] Instantiated 4,397 universal clauses.
[0.061] Instantiated and added clauses for a total of 58,012 clauses.
[0.061] The encoding contains a total of 4,156 distinct variables.
[0.061] Attempting solve with solver <glucose4> ...
c 40 assumptions
[0.063] Executed solver; result: UNSAT.
[0.063] 
[0.063] *************************************
[0.063] * * *   M a k e s p a n     7   * * *
[0.063] *************************************
[0.065] Computed next depth properties: array size of 44.
[0.067] Instantiated 2,812 transitional clauses.
[0.071] Instantiated 4,565 universal clauses.
[0.071] Instantiated and added clauses for a total of 65,389 clauses.
[0.071] The encoding contains a total of 4,579 distinct variables.
[0.071] Attempting solve with solver <glucose4> ...
c 44 assumptions
c last restart ## conflicts  :  86 192 
[0.079] Executed solver; result: SAT.
[0.079] Solver returned SAT; a solution has been found at makespan 7.
22
solution 224 1
166 139 167 141 220 219 221 168 143 144 139 205 186 201 187 223 224 222 202 188 189 186 
[0.079] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.004] Processed problem encoding.
[0.005] Calculated possible fact changes of composite elements.
[0.005] Initialized instantiation procedure.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     0   * * *
[0.005] *************************************
[0.006] Instantiated 1,168 initial clauses.
[0.006] The encoding contains a total of 683 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.006] Computed next depth properties: array size of 55.
[0.007] Instantiated 264 transitional clauses.
[0.008] Instantiated 2,226 universal clauses.
[0.008] Instantiated and added clauses for a total of 3,658 clauses.
[0.008] The encoding contains a total of 1,696 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 55 assumptions
[0.008] Executed solver; result: UNSAT.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     2   * * *
[0.008] *************************************
[0.010] Computed next depth properties: array size of 175.
[0.011] Instantiated 816 transitional clauses.
[0.016] Instantiated 9,190 universal clauses.
[0.016] Instantiated and added clauses for a total of 13,664 clauses.
[0.016] The encoding contains a total of 3,900 distinct variables.
[0.016] Attempting solve with solver <glucose4> ...
c 175 assumptions
c last restart ## conflicts  :  11 208 
[0.018] Executed solver; result: SAT.
[0.018] Solver returned SAT; a solution has been found at makespan 2.
174
solution 326 1
2 32 112 113 34 207 110 30 254 296 297 298 256 257 4 40 118 119 36 211 124 37 254 292 306 293 256 257 6 44 126 127 46 215 132 42 254 288 308 289 256 257 8 50 134 135 52 219 140 48 254 255 310 258 256 257 10 56 142 143 58 223 148 54 254 272 312 273 256 257 12 64 150 151 60 227 156 61 254 280 314 281 256 257 14 68 158 159 70 231 164 66 254 284 316 285 256 257 16 76 166 167 72 235 172 73 254 276 318 277 256 257 18 80 174 175 82 239 180 78 254 264 320 265 256 257 20 88 182 183 84 243 188 85 254 268 322 269 256 257 22 90 192 193 94 247 190 91 254 302 324 303 256 257 25 99 97 26 102 198 199 104 251 204 100 254 255 326 258 256 257 28 108 106 
[0.018] Exiting.

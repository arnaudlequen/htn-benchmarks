Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.005] Processed problem encoding.
[0.005] Calculated possible fact changes of composite elements.
[0.005] Initialized instantiation procedure.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     0   * * *
[0.005] *************************************
[0.006] Instantiated 1,212 initial clauses.
[0.006] The encoding contains a total of 707 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.006] Computed next depth properties: array size of 56.
[0.007] Instantiated 272 transitional clauses.
[0.009] Instantiated 2,242 universal clauses.
[0.009] Instantiated and added clauses for a total of 3,726 clauses.
[0.009] The encoding contains a total of 1,751 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 56 assumptions
[0.009] Executed solver; result: UNSAT.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     2   * * *
[0.009] *************************************
[0.010] Computed next depth properties: array size of 186.
[0.012] Instantiated 884 transitional clauses.
[0.017] Instantiated 9,896 universal clauses.
[0.017] Instantiated and added clauses for a total of 14,506 clauses.
[0.017] The encoding contains a total of 4,135 distinct variables.
[0.017] Attempting solve with solver <glucose4> ...
c 186 assumptions
c last restart ## conflicts  :  12 226 
[0.018] Executed solver; result: SAT.
[0.018] Solver returned SAT; a solution has been found at makespan 2.
185
solution 342 1
2 34 112 113 32 217 118 30 268 298 299 300 270 271 4 40 120 121 36 221 126 37 268 278 320 279 270 271 6 46 130 131 44 225 128 42 268 282 322 283 270 271 8 50 138 139 52 229 136 48 268 316 324 317 270 271 10 58 146 147 56 233 144 54 268 304 326 305 270 271 12 60 154 155 64 237 152 61 268 286 328 287 270 271 14 70 160 161 68 241 166 66 268 269 330 272 270 271 16 76 168 169 74 245 174 72 268 312 332 313 270 271 18 82 176 177 78 249 182 79 268 308 334 309 270 271 20 86 186 187 88 253 184 84 268 290 336 291 270 271 22 90 192 193 94 257 198 91 268 294 338 295 270 271 24 96 200 201 100 261 206 97 268 294 340 295 270 271 26 106 210 211 104 265 208 102 268 304 342 305 270 271 28 110 108 
[0.019] Exiting.

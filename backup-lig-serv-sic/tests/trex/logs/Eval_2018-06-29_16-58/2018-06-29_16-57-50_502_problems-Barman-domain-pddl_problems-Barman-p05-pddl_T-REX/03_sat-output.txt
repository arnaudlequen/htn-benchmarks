Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.005] Processed problem encoding.
[0.005] Calculated possible fact changes of composite elements.
[0.006] Initialized instantiation procedure.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     0   * * *
[0.006] *************************************
[0.007] Instantiated 1,177 initial clauses.
[0.007] The encoding contains a total of 689 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.007] Executed solver; result: UNSAT.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     1   * * *
[0.007] *************************************
[0.007] Computed next depth properties: array size of 56.
[0.008] Instantiated 272 transitional clauses.
[0.009] Instantiated 2,208 universal clauses.
[0.010] Instantiated and added clauses for a total of 3,657 clauses.
[0.010] The encoding contains a total of 1,733 distinct variables.
[0.010] Attempting solve with solver <glucose4> ...
c 56 assumptions
[0.010] Executed solver; result: UNSAT.
[0.010] 
[0.010] *************************************
[0.010] * * *   M a k e s p a n     2   * * *
[0.010] *************************************
[0.012] Computed next depth properties: array size of 186.
[0.013] Instantiated 884 transitional clauses.
[0.018] Instantiated 9,862 universal clauses.
[0.018] Instantiated and added clauses for a total of 14,403 clauses.
[0.018] The encoding contains a total of 4,117 distinct variables.
[0.018] Attempting solve with solver <glucose4> ...
c 186 assumptions
c last restart ## conflicts  :  10 232 
[0.020] Executed solver; result: SAT.
[0.020] Solver returned SAT; a solution has been found at makespan 2.
185
solution 342 1
2 30 114 115 34 217 112 31 268 306 307 308 270 271 4 40 122 123 38 221 120 36 268 298 320 299 270 271 6 44 128 129 46 225 134 42 268 282 322 283 270 271 9 51 139 140 53 231 143 49 273 304 325 305 275 276 10 58 144 145 56 233 150 54 268 278 326 279 270 271 12 64 154 155 62 237 152 60 268 286 328 287 270 271 14 68 162 163 70 241 160 66 268 312 330 313 270 271 16 74 168 169 76 245 174 72 268 269 332 272 270 271 18 82 178 179 80 249 176 78 268 290 334 291 270 271 20 88 184 185 84 253 190 85 268 294 336 295 270 271 22 92 192 193 94 257 198 90 268 316 338 317 270 271 24 96 202 203 100 261 200 97 268 306 340 308 270 271 26 102 103 28 108 208 209 110 265 214 106 268 316 342 317 270 271 
[0.020] Exiting.

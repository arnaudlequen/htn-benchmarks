Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.006] Processed problem encoding.
[0.006] Calculated possible fact changes of composite elements.
[0.006] Initialized instantiation procedure.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     0   * * *
[0.006] *************************************
[0.007] Instantiated 1,305 initial clauses.
[0.007] The encoding contains a total of 761 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 16 assumptions
[0.007] Executed solver; result: UNSAT.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     1   * * *
[0.007] *************************************
[0.008] Computed next depth properties: array size of 61.
[0.009] Instantiated 300 transitional clauses.
[0.011] Instantiated 2,382 universal clauses.
[0.011] Instantiated and added clauses for a total of 3,987 clauses.
[0.011] The encoding contains a total of 1,912 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 61 assumptions
[0.011] Executed solver; result: UNSAT.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     2   * * *
[0.011] *************************************
[0.013] Computed next depth properties: array size of 211.
[0.015] Instantiated 1,020 transitional clauses.
[0.021] Instantiated 11,322 universal clauses.
[0.021] Instantiated and added clauses for a total of 16,329 clauses.
[0.021] The encoding contains a total of 4,659 distinct variables.
[0.021] Attempting solve with solver <glucose4> ...
c 211 assumptions
c last restart ## conflicts  :  12 248 
[0.024] Executed solver; result: SAT.
[0.024] Solver returned SAT; a solution has been found at makespan 2.
210
solution 380 1
3 37 127 128 35 245 123 33 307 335 336 337 309 310 4 38 130 131 42 247 136 39 302 346 354 347 304 305 6 46 138 139 48 251 144 44 302 324 356 325 304 305 8 52 146 147 54 255 152 50 302 320 358 321 304 305 10 60 154 155 56 259 160 57 302 303 360 306 304 305 12 62 162 163 66 263 168 63 302 328 362 329 304 305 14 68 170 171 72 267 176 69 302 316 364 317 304 305 16 74 180 181 78 271 178 75 302 350 366 351 304 305 18 84 188 189 82 275 186 80 302 338 368 339 304 305 20 88 196 197 90 279 194 86 302 312 370 313 304 305 22 92 202 203 96 283 208 93 302 342 372 343 304 305 24 102 212 213 100 287 210 98 302 332 374 334 304 305 26 108 220 221 106 291 218 104 302 332 376 334 304 305 28 112 228 229 114 295 226 110 302 312 378 313 304 305 30 116 234 235 120 299 240 117 302 342 380 343 304 305 
[0.024] Exiting.

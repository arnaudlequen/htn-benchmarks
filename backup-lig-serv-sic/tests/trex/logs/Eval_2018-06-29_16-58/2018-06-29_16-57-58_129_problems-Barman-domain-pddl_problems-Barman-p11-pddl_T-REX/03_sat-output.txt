Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.005] Processed problem encoding.
[0.005] Calculated possible fact changes of composite elements.
[0.005] Initialized instantiation procedure.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     0   * * *
[0.005] *************************************
[0.006] Instantiated 1,130 initial clauses.
[0.006] The encoding contains a total of 663 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.007] Computed next depth properties: array size of 54.
[0.007] Instantiated 256 transitional clauses.
[0.009] Instantiated 2,214 universal clauses.
[0.009] Instantiated and added clauses for a total of 3,600 clauses.
[0.009] The encoding contains a total of 1,645 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 54 assumptions
[0.009] Executed solver; result: UNSAT.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     2   * * *
[0.009] *************************************
[0.011] Computed next depth properties: array size of 164.
[0.012] Instantiated 748 transitional clauses.
[0.017] Instantiated 8,488 universal clauses.
[0.017] Instantiated and added clauses for a total of 12,836 clauses.
[0.017] The encoding contains a total of 3,669 distinct variables.
[0.017] Attempting solve with solver <glucose4> ...
c 164 assumptions
c last restart ## conflicts  :  10 192 
[0.018] Executed solver; result: SAT.
[0.018] Solver returned SAT; a solution has been found at makespan 2.
163
solution 311 1
2 32 108 109 34 193 114 30 240 254 255 256 242 243 4 40 116 117 36 198 197 37 240 272 292 273 242 243 6 46 122 123 42 204 203 43 240 241 294 244 242 243 8 52 128 129 50 209 134 48 240 268 296 269 242 243 10 56 136 137 58 213 142 54 240 260 298 261 242 243 12 64 146 147 62 217 144 60 240 284 300 285 242 243 14 70 154 155 68 221 152 66 240 280 302 281 242 243 16 74 162 163 76 225 160 72 240 276 304 277 242 243 18 82 168 169 80 229 174 78 240 264 306 265 242 243 21 89 181 182 87 235 177 85 245 290 309 291 247 248 23 95 189 190 93 239 185 91 245 252 311 253 247 248 25 98 99 26 102 100 28 106 104 
[0.019] Exiting.

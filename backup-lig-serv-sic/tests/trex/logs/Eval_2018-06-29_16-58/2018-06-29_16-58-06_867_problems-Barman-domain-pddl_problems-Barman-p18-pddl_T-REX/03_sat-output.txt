Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.005] Processed problem encoding.
[0.005] Calculated possible fact changes of composite elements.
[0.005] Initialized instantiation procedure.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     0   * * *
[0.005] *************************************
[0.006] Instantiated 1,136 initial clauses.
[0.006] The encoding contains a total of 667 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.007] Computed next depth properties: array size of 55.
[0.007] Instantiated 264 transitional clauses.
[0.009] Instantiated 2,194 universal clauses.
[0.009] Instantiated and added clauses for a total of 3,594 clauses.
[0.009] The encoding contains a total of 1,680 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 55 assumptions
[0.009] Executed solver; result: UNSAT.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     2   * * *
[0.009] *************************************
[0.011] Computed next depth properties: array size of 175.
[0.012] Instantiated 816 transitional clauses.
[0.017] Instantiated 9,158 universal clauses.
[0.017] Instantiated and added clauses for a total of 13,568 clauses.
[0.017] The encoding contains a total of 3,884 distinct variables.
[0.017] Attempting solve with solver <glucose4> ...
c 175 assumptions
c last restart ## conflicts  :  10 224 
[0.019] Executed solver; result: SAT.
[0.019] Solver returned SAT; a solution has been found at makespan 2.
174
solution 326 1
2 32 112 113 34 207 110 30 254 296 297 298 256 257 4 38 120 121 40 211 118 36 254 292 306 293 256 257 6 44 128 129 46 215 126 42 254 284 308 285 256 257 8 48 134 135 52 219 140 49 254 255 310 258 256 257 10 58 144 145 54 223 142 55 254 288 312 289 256 257 12 64 152 153 60 227 150 61 254 268 314 269 256 257 14 66 158 159 70 231 164 67 254 272 316 273 256 257 16 76 168 169 72 235 166 73 254 302 318 303 256 257 18 78 174 175 82 239 180 79 254 264 320 265 256 257 20 88 182 183 86 243 188 84 254 276 322 277 256 257 22 94 190 191 92 247 196 90 254 280 324 281 256 257 25 98 99 26 102 100 28 106 200 201 108 251 198 104 254 296 326 298 256 257 
[0.020] Exiting.

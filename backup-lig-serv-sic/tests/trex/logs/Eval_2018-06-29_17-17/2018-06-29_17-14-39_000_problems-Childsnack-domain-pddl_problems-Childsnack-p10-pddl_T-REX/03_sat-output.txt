Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.005] Parsed head comment information.
[0.267] Processed problem encoding.
[0.278] Calculated possible fact changes of composite elements.
[0.298] Initialized instantiation procedure.
[0.298] 
[0.298] *************************************
[0.298] * * *   M a k e s p a n     0   * * *
[0.298] *************************************
[0.319] Instantiated 60,903 initial clauses.
[0.319] The encoding contains a total of 58,893 distinct variables.
[0.319] Attempting solve with solver <glucose4> ...
c 16 assumptions
[0.320] Executed solver; result: UNSAT.
[0.320] 
[0.320] *************************************
[0.320] * * *   M a k e s p a n     1   * * *
[0.320] *************************************
[0.360] Computed next depth properties: array size of 76.
[0.449] Instantiated 304,290 transitional clauses.
[0.502] Instantiated 425,032 universal clauses.
[0.502] Instantiated and added clauses for a total of 790,225 clauses.
[0.502] The encoding contains a total of 83,831 distinct variables.
[0.502] Attempting solve with solver <glucose4> ...
c 76 assumptions
c last restart ## conflicts  :  18 10169 
[0.529] Executed solver; result: SAT.
[0.529] Solver returned SAT; a solution has been found at makespan 1.
75
solution 3263 1
2274 111 1161 1219 1163 723 114 4 115 6 531 32 8 858 10 1271 88 1158 2874 1160 761 104 12 949 14 1970 79 1155 2930 1157 2679 142 1155 3017 1157 1537 132 1034 3073 1036 1678 135 1028 3134 1030 455 62 12 991 14 324 121 1028 1082 1030 1822 23 4 3146 6 2472 93 4 3236 6 2581 16 4 3263 6 2 3 4 1094 6 
[0.529] Exiting.

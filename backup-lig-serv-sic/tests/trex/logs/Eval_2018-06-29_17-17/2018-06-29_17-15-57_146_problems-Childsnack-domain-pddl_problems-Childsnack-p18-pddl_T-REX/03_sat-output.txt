Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.022] Parsed head comment information.
[1.911] Processed problem encoding.
[2.019] Calculated possible fact changes of composite elements.
[2.172] Initialized instantiation procedure.
[2.172] 
[2.172] *************************************
[2.172] * * *   M a k e s p a n     0   * * *
[2.172] *************************************
[2.312] Instantiated 312,649 initial clauses.
[2.312] The encoding contains a total of 308,169 distinct variables.
[2.312] Attempting solve with solver <glucose4> ...
c 22 assumptions
[2.316] Executed solver; result: UNSAT.
[2.316] 
[2.316] *************************************
[2.316] * * *   M a k e s p a n     1   * * *
[2.316] *************************************
[2.611] Computed next depth properties: array size of 106.
[3.208] Instantiated 1,597,764 transitional clauses.
[3.473] Instantiated 1,799,992 universal clauses.
[3.473] Instantiated and added clauses for a total of 3,710,405 clauses.
[3.473] The encoding contains a total of 397,831 distinct variables.
[3.473] Attempting solve with solver <glucose4> ...
c 106 assumptions
c last restart ## conflicts  :  40 38296 
[3.965] Executed solver; result: SAT.
[3.965] Solver returned SAT; a solution has been found at makespan 1.
105
solution 8935 1
911 60 12 61 14 1421 110 2026 2078 2028 5891 132 2032 2894 2034 7159 200 2026 7754 2028 5841 182 2706 7858 2708 3423 96 12 7932 14 3012 173 2026 8078 2028 4173 38 2706 8130 2708 3975 20 2706 8234 2708 6768 213 12 8432 14 1686 231 12 2244 14 1934 191 2026 2338 2028 7570 119 2706 8502 2708 5208 29 2706 8574 2708 6519 238 8 8779 10 502 161 2035 2437 2037 401 254 4 2590 6 4758 3 2706 8790 2708 904 245 4 2698 6 4626 85 2029 8935 2031 1305 74 2706 2742 2708 
[3.967] Exiting.

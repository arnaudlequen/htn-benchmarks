Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.011] Parsed head comment information.
[0.317] Processed problem encoding.
[0.362] Calculated possible fact changes of composite elements.
[0.387] Initialized instantiation procedure.
[0.387] 
[0.387] *************************************
[0.387] * * *   M a k e s p a n     0   * * *
[0.387] *************************************
[0.470] Instantiated 76,473 initial clauses.
[0.470] The encoding contains a total of 38,602 distinct variables.
[0.470] Attempting solve with solver <glucose4> ...
c 76 assumptions
[0.471] Executed solver; result: UNSAT.
[0.471] 
[0.471] *************************************
[0.471] * * *   M a k e s p a n     1   * * *
[0.471] *************************************
[0.630] Computed next depth properties: array size of 151.
[0.699] Instantiated 75,375 transitional clauses.
[1.011] Instantiated 340,871 universal clauses.
[1.011] Instantiated and added clauses for a total of 492,719 clauses.
[1.011] The encoding contains a total of 153,745 distinct variables.
[1.011] Attempting solve with solver <glucose4> ...
c 151 assumptions
[1.013] Executed solver; result: UNSAT.
[1.013] 
[1.013] *************************************
[1.013] * * *   M a k e s p a n     2   * * *
[1.013] *************************************
[1.318] Computed next depth properties: array size of 301.
[1.553] Instantiated 225,000 transitional clauses.
[2.192] Instantiated 1,803,746 universal clauses.
[2.192] Instantiated and added clauses for a total of 2,521,465 clauses.
[2.192] The encoding contains a total of 306,822 distinct variables.
[2.192] Attempting solve with solver <glucose4> ...
c 301 assumptions
c last restart ## conflicts  :  0 321 
[2.194] Executed solver; result: SAT.
[2.194] Solver returned SAT; a solution has been found at makespan 2.
300
solution 64798 1
19286 2 51761 35007 403 3 35482 35008 1041 4 35791 35009 1496 5 52150 35010 19862 6 36291 35011 2073 7 52788 35012 20399 8 53104 35013 2357 9 29524 35014 2628 10 53179 35015 2828 11 37673 35016 3315 12 37930 35017 3781 13 53583 35018 21563 14 25890 35019 4626 15 38319 35020 21776 16 38876 35021 4924 17 39399 35022 5217 18 39705 35023 5615 19 39920 35024 22967 20 40615 35025 5861 21 54767 35026 23652 22 41239 35027 6074 23 55109 35028 6429 24 41601 35029 22680 25 42044 35030 6774 26 37717 35031 6899 27 52380 35032 24836 28 42417 35033 7326 29 37647 35034 25790 30 56145 35035 25967 31 56317 35036 26442 32 43138 35037 7999 33 56780 35038 26779 34 43439 35039 27187 35 55445 35040 27238 36 43552 35041 9501 37 44066 35042 9985 38 44514 35043 27519 39 45074 35044 10471 40 57712 35045 27851 41 58108 35046 10870 42 45569 35047 11419 43 27488 35048 11709 44 46061 35049 11936 45 58705 35050 19878 46 59199 35051 11969 47 46450 35052 12466 48 59648 35053 12591 49 59870 35054 29403 50 47076 35055 13505 51 47867 35056 13551 52 60602 35057 30268 53 60874 35058 13930 54 48289 35059 30665 55 23293 35060 14656 56 48534 35061 31295 57 9952 35062 31470 58 59101 35063 31730 59 61747 35064 24805 60 49261 35065 15603 61 49498 35066 32324 62 62333 35067 15722 63 50082 35068 8402 64 3610 35069 32949 65 50214 35070 16502 66 50456 35071 16900 67 50521 35072 17258 68 50704 35073 33305 69 43815 35074 18144 70 50982 35075 18378 71 64237 35076 33786 72 35331 35077 34278 73 54340 35078 18900 74 64664 35079 12265 75 64798 35080 34824 76 51653 35081 
[2.219] Exiting.

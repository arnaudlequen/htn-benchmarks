Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.002] Parsed head comment information.
[0.088] Processed problem encoding.
[0.095] Calculated possible fact changes of composite elements.
[0.102] Initialized instantiation procedure.
[0.102] 
[0.102] *************************************
[0.102] * * *   M a k e s p a n     0   * * *
[0.102] *************************************
[0.117] Instantiated 16,073 initial clauses.
[0.117] The encoding contains a total of 8,202 distinct variables.
[0.117] Attempting solve with solver <glucose4> ...
c 76 assumptions
[0.117] Executed solver; result: UNSAT.
[0.117] 
[0.117] *************************************
[0.117] * * *   M a k e s p a n     1   * * *
[0.117] *************************************
[0.154] Computed next depth properties: array size of 151.
[0.166] Instantiated 15,375 transitional clauses.
[0.210] Instantiated 70,421 universal clauses.
[0.210] Instantiated and added clauses for a total of 101,869 clauses.
[0.210] The encoding contains a total of 32,892 distinct variables.
[0.210] Attempting solve with solver <glucose4> ...
c 151 assumptions
[0.210] Executed solver; result: UNSAT.
[0.210] 
[0.210] *************************************
[0.210] * * *   M a k e s p a n     2   * * *
[0.210] *************************************
[0.260] Computed next depth properties: array size of 301.
[0.299] Instantiated 45,000 transitional clauses.
[0.387] Instantiated 318,296 universal clauses.
[0.387] Instantiated and added clauses for a total of 465,165 clauses.
[0.387] The encoding contains a total of 65,519 distinct variables.
[0.387] Attempting solve with solver <glucose4> ...
c 301 assumptions
c last restart ## conflicts  :  0 318 
[0.388] Executed solver; result: SAT.
[0.388] Solver returned SAT; a solution has been found at makespan 2.
300
solution 7892 1
2874 2 5401 5324 199 3 836 5325 97 4 5497 5326 220 5 6748 5327 3026 6 5547 5328 335 7 6806 5329 3101 8 1196 5330 3223 9 4092 5331 439 10 293 5332 3334 11 6838 5333 3367 12 5552 5334 584 13 5616 5335 208 14 6930 5336 3482 15 7023 5337 3530 16 5086 5338 714 17 5729 5339 824 18 5799 5340 937 19 5862 5341 175 20 7165 5342 2986 21 7236 5343 3606 22 1433 5344 1006 23 792 5345 1083 24 6011 5346 1202 25 7281 5347 3754 26 1874 5348 1253 27 1991 5349 1287 28 2203 5350 1022 29 4734 5351 1394 30 6142 5352 1440 31 4576 5353 1155 32 4656 5354 4014 33 7164 5355 4056 34 7454 5356 4135 35 5448 5357 1072 36 4818 5358 1563 37 3945 5359 4222 38 2408 5360 4313 39 2107 5361 4370 40 93 5362 4383 41 749 5363 2957 42 6273 5364 1838 43 6327 5365 604 44 1307 5366 655 45 4180 5367 1338 46 4567 5368 1865 47 1221 5369 4614 48 7521 5370 4689 49 3247 5371 4784 50 686 5372 3963 51 2000 5373 550 52 2898 5374 3515 53 7597 5375 4795 54 5432 5376 2027 55 3155 5377 4960 56 4279 5378 2131 57 5407 5379 2193 58 5190 5380 4682 59 7688 5381 5011 60 6994 5382 84 61 6433 5383 2281 62 5731 5384 2369 63 7781 5385 5111 64 1014 5386 3869 65 393 5387 2514 66 4971 5388 2954 67 7892 5389 3707 68 6571 5390 2629 69 6627 5391 1181 70 6571 5392 2687 71 6037 5393 5281 72 2324 5394 5296 73 6507 5395 2809 74 6520 5396 1673 75 5613 5397 417 76 7668 5398 
[0.391] Exiting.

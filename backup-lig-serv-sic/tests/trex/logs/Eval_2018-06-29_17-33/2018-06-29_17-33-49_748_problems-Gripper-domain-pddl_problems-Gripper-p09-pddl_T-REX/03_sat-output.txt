Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.001] Processed problem encoding.
[0.002] Calculated possible fact changes of composite elements.
[0.002] Initialized instantiation procedure.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     0   * * *
[0.002] *************************************
[0.002] Instantiated 520 initial clauses.
[0.002] The encoding contains a total of 310 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 11 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     1   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 61.
[0.003] Instantiated 270 transitional clauses.
[0.004] Instantiated 2,144 universal clauses.
[0.004] Instantiated and added clauses for a total of 2,934 clauses.
[0.004] The encoding contains a total of 1,099 distinct variables.
[0.004] Attempting solve with solver <glucose4> ...
c 61 assumptions
c last restart ## conflicts  :  0 105 
[0.004] Executed solver; result: SAT.
[0.004] Solver returned SAT; a solution has been found at makespan 1.
59
solution 83 1
82 76 5 83 77 2 74 68 5 75 69 2 66 60 5 67 61 2 58 52 5 59 53 2 50 44 5 51 45 2 42 36 5 43 37 2 34 28 5 35 29 2 26 20 5 27 21 2 18 12 5 19 13 2 9 8 5 11 10 
[0.004] Exiting.

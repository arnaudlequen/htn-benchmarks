Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.003] Parsed head comment information.
[0.124] Processed problem encoding.
[0.132] Calculated possible fact changes of composite elements.
[0.139] Initialized instantiation procedure.
[0.139] 
[0.139] *************************************
[0.139] * * *   M a k e s p a n     0   * * *
[0.139] *************************************
[0.154] Instantiated 20,848 initial clauses.
[0.154] The encoding contains a total of 10,602 distinct variables.
[0.154] Attempting solve with solver <glucose4> ...
c 51 assumptions
[0.154] Executed solver; result: UNSAT.
[0.154] 
[0.154] *************************************
[0.154] * * *   M a k e s p a n     1   * * *
[0.154] *************************************
[0.183] Computed next depth properties: array size of 101.
[0.198] Instantiated 20,250 transitional clauses.
[0.257] Instantiated 92,146 universal clauses.
[0.257] Instantiated and added clauses for a total of 133,244 clauses.
[0.257] The encoding contains a total of 42,269 distinct variables.
[0.257] Attempting solve with solver <glucose4> ...
c 101 assumptions
[0.258] Executed solver; result: UNSAT.
[0.258] 
[0.258] *************************************
[0.258] * * *   M a k e s p a n     2   * * *
[0.258] *************************************
[0.313] Computed next depth properties: array size of 201.
[0.359] Instantiated 60,000 transitional clauses.
[0.528] Instantiated 462,396 universal clauses.
[0.528] Instantiated and added clauses for a total of 655,640 clauses.
[0.528] The encoding contains a total of 84,221 distinct variables.
[0.528] Attempting solve with solver <glucose4> ...
c 201 assumptions
c last restart ## conflicts  :  0 220 
[0.528] Executed solver; result: SAT.
[0.528] Solver returned SAT; a solution has been found at makespan 2.
200
solution 16798 1
4744 2 9506 9405 256 3 5136 9406 439 4 13407 9407 4851 5 9546 9408 759 6 9661 9409 911 7 9898 9410 987 8 2505 9411 5148 9 8440 9412 1264 10 9945 9413 1364 11 4904 9414 1528 12 9988 9415 1704 13 4794 9416 5407 14 14097 9417 1913 15 10205 9418 2038 16 14182 9419 5818 17 14256 9420 6094 18 2990 9421 2143 19 9593 9422 6287 20 14384 9423 6334 21 10663 9424 5982 22 14623 9425 2411 23 11006 9426 2615 24 11057 9427 2655 25 14770 9428 6678 26 14874 9429 2863 27 237 9430 6823 28 15107 9431 6989 29 11334 9432 3202 30 15312 9433 7078 31 3932 9434 7228 32 7634 9435 3517 33 11478 9436 7448 34 10319 9437 3710 35 15647 9438 7671 36 15783 9439 7969 37 15848 9440 8140 38 875 9441 3916 39 11809 9442 4034 40 15983 9443 8324 41 12054 9444 8449 42 12237 9445 4319 43 12414 9446 4367 44 16276 9447 8823 45 12700 9448 1987 46 16347 9449 4420 47 4826 9450 5353 48 12791 9451 9147 49 16641 9452 4611 50 13063 9453 9371 51 16798 9454 
[0.532] Exiting.

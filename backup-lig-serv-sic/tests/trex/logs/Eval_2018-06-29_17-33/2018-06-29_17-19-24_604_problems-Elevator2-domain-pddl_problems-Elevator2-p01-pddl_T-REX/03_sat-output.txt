Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.002] Parsed head comment information.
[0.065] Processed problem encoding.
[0.070] Calculated possible fact changes of composite elements.
[0.074] Initialized instantiation procedure.
[0.074] 
[0.074] *************************************
[0.074] * * *   M a k e s p a n     0   * * *
[0.074] *************************************
[0.084] Instantiated 10,748 initial clauses.
[0.084] The encoding contains a total of 5,502 distinct variables.
[0.084] Attempting solve with solver <glucose4> ...
c 51 assumptions
[0.084] Executed solver; result: UNSAT.
[0.084] 
[0.084] *************************************
[0.084] * * *   M a k e s p a n     1   * * *
[0.084] *************************************
[0.106] Computed next depth properties: array size of 101.
[0.116] Instantiated 10,250 transitional clauses.
[0.149] Instantiated 46,946 universal clauses.
[0.149] Instantiated and added clauses for a total of 67,944 clauses.
[0.149] The encoding contains a total of 21,967 distinct variables.
[0.149] Attempting solve with solver <glucose4> ...
c 101 assumptions
[0.150] Executed solver; result: UNSAT.
[0.150] 
[0.150] *************************************
[0.150] * * *   M a k e s p a n     2   * * *
[0.150] *************************************
[0.175] Computed next depth properties: array size of 201.
[0.195] Instantiated 30,000 transitional clauses.
[0.244] Instantiated 212,196 universal clauses.
[0.244] Instantiated and added clauses for a total of 310,140 clauses.
[0.244] The encoding contains a total of 43,719 distinct variables.
[0.244] Attempting solve with solver <glucose4> ...
c 201 assumptions
c last restart ## conflicts  :  0 218 
[0.244] Executed solver; result: SAT.
[0.244] Solver returned SAT; a solution has been found at makespan 2.
200
solution 6257 1
2027 2 5224 4012 102 3 521 4013 2124 4 5294 4014 228 5 2362 4015 302 6 917 4016 344 7 5384 4017 2311 8 4167 4018 2415 9 5494 4019 2482 10 1651 4020 2585 11 574 4021 2622 12 4312 4022 548 13 4332 4023 2301 14 4405 4024 631 15 4490 4025 670 16 4544 4026 740 17 5378 4027 2805 18 5693 4028 2826 19 5789 4029 2858 20 3999 4030 942 21 4679 4031 991 22 1737 4032 1031 23 4683 4033 1154 24 5918 4034 3106 25 2944 4035 1263 26 1804 4036 3213 27 5971 4037 2889 28 4901 4038 307 29 987 4039 3262 30 5470 4040 1421 31 2034 4041 3334 32 3862 4042 1496 33 6086 4043 2545 34 897 4044 2286 35 4987 4045 1481 36 1863 4046 329 37 4177 4047 1557 38 4273 4048 3523 39 5080 4049 1627 40 5153 4050 732 41 4399 4051 1706 42 2466 4052 1784 43 3950 4053 1840 44 2381 4054 1875 45 6209 4055 1735 46 2134 4056 380 47 3158 4057 1923 48 6257 4058 3780 49 97 4059 3916 50 1191 4060 3942 51 1144 4061 
[0.246] Exiting.

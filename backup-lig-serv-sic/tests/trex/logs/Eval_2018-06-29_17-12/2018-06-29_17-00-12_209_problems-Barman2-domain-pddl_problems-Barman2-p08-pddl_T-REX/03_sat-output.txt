Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.012] Processed problem encoding.
[0.012] Calculated possible fact changes of composite elements.
[0.013] Initialized instantiation procedure.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     0   * * *
[0.013] *************************************
[0.016] Instantiated 3,086 initial clauses.
[0.016] The encoding contains a total of 1,799 distinct variables.
[0.016] Attempting solve with solver <glucose4> ...
c 40 assumptions
[0.016] Executed solver; result: UNSAT.
[0.016] 
[0.016] *************************************
[0.016] * * *   M a k e s p a n     1   * * *
[0.016] *************************************
[0.020] Computed next depth properties: array size of 148.
[0.021] Instantiated 708 transitional clauses.
[0.028] Instantiated 6,262 universal clauses.
[0.028] Instantiated and added clauses for a total of 10,056 clauses.
[0.028] The encoding contains a total of 4,643 distinct variables.
[0.028] Attempting solve with solver <glucose4> ...
c 148 assumptions
[0.028] Executed solver; result: UNSAT.
[0.028] 
[0.028] *************************************
[0.028] * * *   M a k e s p a n     2   * * *
[0.028] *************************************
[0.037] Computed next depth properties: array size of 448.
[0.040] Instantiated 2,040 transitional clauses.
[0.058] Instantiated 24,082 universal clauses.
[0.058] Instantiated and added clauses for a total of 36,178 clauses.
[0.058] The encoding contains a total of 10,462 distinct variables.
[0.058] Attempting solve with solver <glucose4> ...
c 448 assumptions
c last restart ## conflicts  :  31 516 
[0.067] Executed solver; result: SAT.
[0.067] Solver returned SAT; a solution has been found at makespan 2.
447
solution 840 1
3 85 301 302 82 533 297 83 661 737 738 739 663 664 4 90 306 307 88 535 304 86 656 722 784 723 658 659 7 95 317 318 97 541 313 93 661 700 787 701 663 664 8 100 320 321 102 543 326 98 656 690 788 691 658 659 10 108 328 329 106 547 334 104 656 760 790 761 658 659 12 112 338 339 114 551 336 110 656 730 792 731 658 659 15 121 347 348 119 557 351 117 661 672 795 673 663 664 16 124 354 355 126 559 352 122 656 748 796 749 658 659 18 132 360 361 130 563 366 128 656 674 798 675 658 659 20 136 370 371 138 567 368 134 656 726 800 727 658 659 22 144 376 377 142 571 382 140 656 686 802 687 658 659 24 148 386 387 150 575 384 146 656 682 804 683 658 659 26 152 394 395 156 579 392 153 656 768 806 769 658 659 28 162 400 401 160 583 406 158 656 702 808 703 658 659 30 168 410 411 166 587 408 164 656 776 810 777 658 659 32 174 416 417 172 591 422 170 656 714 812 715 658 659 34 178 424 425 180 596 595 176 656 666 814 667 658 659 36 186 430 431 184 601 436 182 656 706 816 707 658 659 38 192 438 439 190 605 444 188 656 744 818 745 658 659 40 198 448 449 196 609 446 194 656 756 820 757 658 659 42 202 454 455 204 614 613 200 656 740 822 741 658 659 44 210 462 463 208 619 460 206 656 780 824 781 658 659 46 214 468 469 216 623 474 212 656 678 826 679 658 659 48 218 478 479 222 627 476 219 656 718 828 719 658 659 50 228 484 485 226 631 490 224 656 752 830 753 658 659 52 234 494 495 232 635 492 230 656 764 832 765 658 659 54 240 502 503 238 639 500 236 656 772 834 773 658 659 56 246 508 509 244 643 514 242 656 710 836 711 658 659 58 250 516 517 252 648 647 248 656 694 838 695 658 659 60 256 522 523 258 653 528 254 656 657 840 660 658 659 62 262 260 64 266 264 66 270 268 69 275 273 70 278 276 72 282 280 74 286 284 76 290 288 78 294 292 
[0.069] Exiting.

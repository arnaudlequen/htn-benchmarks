
Compact plan:
0: (grasp right shot1)
1: (fill-shot shot1 ingredient17 right left dispenser17)
2: (pour-shot-to-clean-shaker shot1 ingredient17 shaker1 right l0 l1)
3: (clean-shot shot1 ingredient17 right left)
4: (fill-shot shot1 ingredient1 right left dispenser1)
5: (pour-shot-to-used-shaker shot1 ingredient1 shaker1 right l1 l2)
6: (clean-shot shot1 ingredient1 right left)
7: (leave right shot1)
8: (grasp left shaker1)
9: (shake cocktail19 ingredient17 ingredient1 shaker1 left right)
10: (pour-shaker-to-shot cocktail19 shot1 left shaker1 l2 l1)
11: (empty-shaker left shaker1 cocktail19 l1 l0)
12: (clean-shaker left right shaker1)
13: (leave left shaker1)
14: (grasp left shot2)
15: (fill-shot shot2 ingredient19 left right dispenser19)
16: (pour-shot-to-clean-shaker shot2 ingredient19 shaker1 left l0 l1)
17: (clean-shot shot2 ingredient19 left right)
18: (fill-shot shot2 ingredient13 left right dispenser13)
19: (pour-shot-to-used-shaker shot2 ingredient13 shaker1 left l1 l2)
20: (clean-shot shot2 ingredient13 left right)
21: (leave left shot2)
22: (grasp right shaker1)
23: (shake cocktail16 ingredient19 ingredient13 shaker1 right left)
24: (pour-shaker-to-shot cocktail16 shot2 right shaker1 l2 l1)
25: (empty-shaker right shaker1 cocktail16 l1 l0)
26: (clean-shaker right left shaker1)
27: (leave right shaker1)
28: (grasp right shot3)
29: (fill-shot shot3 ingredient2 right left dispenser2)
30: (pour-shot-to-clean-shaker shot3 ingredient2 shaker1 right l0 l1)
31: (clean-shot shot3 ingredient2 right left)
32: (fill-shot shot3 ingredient14 right left dispenser14)
33: (pour-shot-to-used-shaker shot3 ingredient14 shaker1 right l1 l2)
34: (clean-shot shot3 ingredient14 right left)
35: (leave right shot3)
36: (grasp left shaker1)
37: (shake cocktail10 ingredient2 ingredient14 shaker1 left right)
38: (pour-shaker-to-shot cocktail10 shot3 left shaker1 l2 l1)
39: (empty-shaker left shaker1 cocktail10 l1 l0)
40: (clean-shaker left right shaker1)
41: (leave left shaker1)
42: (grasp left shot4)
43: (fill-shot shot4 ingredient14 left right dispenser14)
44: (pour-shot-to-clean-shaker shot4 ingredient14 shaker1 left l0 l1)
45: (clean-shot shot4 ingredient14 left right)
46: (fill-shot shot4 ingredient16 left right dispenser16)
47: (pour-shot-to-used-shaker shot4 ingredient16 shaker1 left l1 l2)
48: (clean-shot shot4 ingredient16 left right)
49: (leave left shot4)
50: (grasp right shaker1)
51: (shake cocktail8 ingredient14 ingredient16 shaker1 right left)
52: (pour-shaker-to-shot cocktail8 shot4 right shaker1 l2 l1)
53: (empty-shaker right shaker1 cocktail8 l1 l0)
54: (clean-shaker right left shaker1)
55: (leave right shaker1)
56: (grasp left shot5)
57: (fill-shot shot5 ingredient13 left right dispenser13)
58: (pour-shot-to-clean-shaker shot5 ingredient13 shaker1 left l0 l1)
59: (clean-shot shot5 ingredient13 left right)
60: (fill-shot shot5 ingredient10 left right dispenser10)
61: (pour-shot-to-used-shaker shot5 ingredient10 shaker1 left l1 l2)
62: (clean-shot shot5 ingredient10 left right)
63: (leave left shot5)
64: (grasp right shaker1)
65: (shake cocktail25 ingredient13 ingredient10 shaker1 right left)
66: (pour-shaker-to-shot cocktail25 shot5 right shaker1 l2 l1)
67: (empty-shaker right shaker1 cocktail25 l1 l0)
68: (clean-shaker right left shaker1)
69: (leave right shaker1)
70: (grasp left shot6)
71: (fill-shot shot6 ingredient8 left right dispenser8)
72: (pour-shot-to-clean-shaker shot6 ingredient8 shaker1 left l0 l1)
73: (clean-shot shot6 ingredient8 left right)
74: (fill-shot shot6 ingredient12 left right dispenser12)
75: (pour-shot-to-used-shaker shot6 ingredient12 shaker1 left l1 l2)
76: (clean-shot shot6 ingredient12 left right)
77: (leave left shot6)
78: (grasp right shaker1)
79: (shake cocktail18 ingredient8 ingredient12 shaker1 right left)
80: (pour-shaker-to-shot cocktail18 shot6 right shaker1 l2 l1)
81: (empty-shaker right shaker1 cocktail18 l1 l0)
82: (clean-shaker right left shaker1)
83: (leave right shaker1)
84: (grasp right shot7)
85: (fill-shot shot7 ingredient9 right left dispenser9)
86: (pour-shot-to-clean-shaker shot7 ingredient9 shaker1 right l0 l1)
87: (clean-shot shot7 ingredient9 right left)
88: (fill-shot shot7 ingredient3 right left dispenser3)
89: (pour-shot-to-used-shaker shot7 ingredient3 shaker1 right l1 l2)
90: (clean-shot shot7 ingredient3 right left)
91: (leave right shot7)
92: (grasp left shaker1)
93: (shake cocktail3 ingredient9 ingredient3 shaker1 left right)
94: (pour-shaker-to-shot cocktail3 shot7 left shaker1 l2 l1)
95: (empty-shaker left shaker1 cocktail3 l1 l0)
96: (clean-shaker left right shaker1)
97: (leave left shaker1)
98: (grasp left shot8)
99: (fill-shot shot8 ingredient7 left right dispenser7)
100: (pour-shot-to-clean-shaker shot8 ingredient7 shaker1 left l0 l1)
101: (clean-shot shot8 ingredient7 left right)
102: (fill-shot shot8 ingredient11 left right dispenser11)
103: (pour-shot-to-used-shaker shot8 ingredient11 shaker1 left l1 l2)
104: (clean-shot shot8 ingredient11 left right)
105: (leave left shot8)
106: (grasp right shaker1)
107: (shake cocktail22 ingredient7 ingredient11 shaker1 right left)
108: (pour-shaker-to-shot cocktail22 shot8 right shaker1 l2 l1)
109: (empty-shaker right shaker1 cocktail22 l1 l0)
110: (clean-shaker right left shaker1)
111: (leave right shaker1)
112: (grasp left shot9)
113: (fill-shot shot9 ingredient13 left right dispenser13)
114: (pour-shot-to-clean-shaker shot9 ingredient13 shaker1 left l0 l1)
115: (clean-shot shot9 ingredient13 left right)
116: (fill-shot shot9 ingredient3 left right dispenser3)
117: (pour-shot-to-used-shaker shot9 ingredient3 shaker1 left l1 l2)
118: (clean-shot shot9 ingredient3 left right)
119: (leave left shot9)
120: (grasp right shaker1)
121: (shake cocktail4 ingredient13 ingredient3 shaker1 right left)
122: (pour-shaker-to-shot cocktail4 shot9 right shaker1 l2 l1)
123: (empty-shaker right shaker1 cocktail4 l1 l0)
124: (clean-shaker right left shaker1)
125: (leave right shaker1)
126: (grasp left shot10)
127: (fill-shot shot10 ingredient12 left right dispenser12)
128: (pour-shot-to-clean-shaker shot10 ingredient12 shaker1 left l0 l1)
129: (clean-shot shot10 ingredient12 left right)
130: (fill-shot shot10 ingredient14 left right dispenser14)
131: (pour-shot-to-used-shaker shot10 ingredient14 shaker1 left l1 l2)
132: (clean-shot shot10 ingredient14 left right)
133: (leave left shot10)
134: (grasp right shaker1)
135: (shake cocktail17 ingredient12 ingredient14 shaker1 right left)
136: (pour-shaker-to-shot cocktail17 shot10 right shaker1 l2 l1)
137: (empty-shaker right shaker1 cocktail17 l1 l0)
138: (clean-shaker right left shaker1)
139: (leave right shaker1)
140: (grasp left shot11)
141: (fill-shot shot11 ingredient16 left right dispenser16)
142: (pour-shot-to-clean-shaker shot11 ingredient16 shaker1 left l0 l1)
143: (clean-shot shot11 ingredient16 left right)
144: (fill-shot shot11 ingredient6 left right dispenser6)
145: (pour-shot-to-used-shaker shot11 ingredient6 shaker1 left l1 l2)
146: (clean-shot shot11 ingredient6 left right)
147: (leave left shot11)
148: (grasp right shaker1)
149: (shake cocktail7 ingredient16 ingredient6 shaker1 right left)
150: (pour-shaker-to-shot cocktail7 shot11 right shaker1 l2 l1)
151: (empty-shaker right shaker1 cocktail7 l1 l0)
152: (clean-shaker right left shaker1)
153: (leave right shaker1)
154: (grasp left shot12)
155: (fill-shot shot12 ingredient3 left right dispenser3)
156: (pour-shot-to-clean-shaker shot12 ingredient3 shaker1 left l0 l1)
157: (clean-shot shot12 ingredient3 left right)
158: (fill-shot shot12 ingredient9 left right dispenser9)
159: (pour-shot-to-used-shaker shot12 ingredient9 shaker1 left l1 l2)
160: (clean-shot shot12 ingredient9 left right)
161: (leave left shot12)
162: (grasp right shaker1)
163: (shake cocktail6 ingredient3 ingredient9 shaker1 right left)
164: (pour-shaker-to-shot cocktail6 shot12 right shaker1 l2 l1)
165: (empty-shaker right shaker1 cocktail6 l1 l0)
166: (clean-shaker right left shaker1)
167: (leave right shaker1)
168: (grasp left shot13)
169: (fill-shot shot13 ingredient1 left right dispenser1)
170: (pour-shot-to-clean-shaker shot13 ingredient1 shaker1 left l0 l1)
171: (clean-shot shot13 ingredient1 left right)
172: (fill-shot shot13 ingredient13 left right dispenser13)
173: (pour-shot-to-used-shaker shot13 ingredient13 shaker1 left l1 l2)
174: (clean-shot shot13 ingredient13 left right)
175: (leave left shot13)
176: (grasp right shaker1)
177: (shake cocktail27 ingredient1 ingredient13 shaker1 right left)
178: (pour-shaker-to-shot cocktail27 shot13 right shaker1 l2 l1)
179: (empty-shaker right shaker1 cocktail27 l1 l0)
180: (clean-shaker right left shaker1)
181: (leave right shaker1)
182: (grasp left shot14)
183: (fill-shot shot14 ingredient11 left right dispenser11)
184: (pour-shot-to-clean-shaker shot14 ingredient11 shaker1 left l0 l1)
185: (clean-shot shot14 ingredient11 left right)
186: (fill-shot shot14 ingredient6 left right dispenser6)
187: (pour-shot-to-used-shaker shot14 ingredient6 shaker1 left l1 l2)
188: (clean-shot shot14 ingredient6 left right)
189: (leave left shot14)
190: (grasp right shaker1)
191: (shake cocktail11 ingredient11 ingredient6 shaker1 right left)
192: (pour-shaker-to-shot cocktail11 shot14 right shaker1 l2 l1)
193: (empty-shaker right shaker1 cocktail11 l1 l0)
194: (clean-shaker right left shaker1)
195: (leave right shaker1)
196: (grasp left shot15)
197: (fill-shot shot15 ingredient18 left right dispenser18)
198: (pour-shot-to-clean-shaker shot15 ingredient18 shaker1 left l0 l1)
199: (clean-shot shot15 ingredient18 left right)
200: (fill-shot shot15 ingredient13 left right dispenser13)
201: (pour-shot-to-used-shaker shot15 ingredient13 shaker1 left l1 l2)
202: (clean-shot shot15 ingredient13 left right)
203: (leave left shot15)
204: (grasp right shaker1)
205: (shake cocktail29 ingredient18 ingredient13 shaker1 right left)
206: (pour-shaker-to-shot cocktail29 shot15 right shaker1 l2 l1)
207: (empty-shaker right shaker1 cocktail29 l1 l0)
208: (clean-shaker right left shaker1)
209: (leave right shaker1)
210: (grasp left shot16)
211: (fill-shot shot16 ingredient10 left right dispenser10)
212: (pour-shot-to-clean-shaker shot16 ingredient10 shaker1 left l0 l1)
213: (clean-shot shot16 ingredient10 left right)
214: (fill-shot shot16 ingredient7 left right dispenser7)
215: (pour-shot-to-used-shaker shot16 ingredient7 shaker1 left l1 l2)
216: (clean-shot shot16 ingredient7 left right)
217: (leave left shot16)
218: (grasp right shaker1)
219: (shake cocktail14 ingredient10 ingredient7 shaker1 right left)
220: (pour-shaker-to-shot cocktail14 shot16 right shaker1 l2 l1)
221: (empty-shaker right shaker1 cocktail14 l1 l0)
222: (clean-shaker right left shaker1)
223: (leave right shaker1)
224: (grasp left shot17)
225: (fill-shot shot17 ingredient14 left right dispenser14)
226: (pour-shot-to-clean-shaker shot17 ingredient14 shaker1 left l0 l1)
227: (clean-shot shot17 ingredient14 left right)
228: (fill-shot shot17 ingredient20 left right dispenser20)
229: (pour-shot-to-used-shaker shot17 ingredient20 shaker1 left l1 l2)
230: (clean-shot shot17 ingredient20 left right)
231: (leave left shot17)
232: (grasp right shaker1)
233: (shake cocktail2 ingredient14 ingredient20 shaker1 right left)
234: (pour-shaker-to-shot cocktail2 shot17 right shaker1 l2 l1)
235: (empty-shaker right shaker1 cocktail2 l1 l0)
236: (clean-shaker right left shaker1)
237: (leave right shaker1)
238: (grasp left shot18)
239: (fill-shot shot18 ingredient10 left right dispenser10)
240: (pour-shot-to-clean-shaker shot18 ingredient10 shaker1 left l0 l1)
241: (clean-shot shot18 ingredient10 left right)
242: (fill-shot shot18 ingredient6 left right dispenser6)
243: (pour-shot-to-used-shaker shot18 ingredient6 shaker1 left l1 l2)
244: (clean-shot shot18 ingredient6 left right)
245: (leave left shot18)
246: (grasp right shaker1)
247: (shake cocktail12 ingredient10 ingredient6 shaker1 right left)
248: (pour-shaker-to-shot cocktail12 shot18 right shaker1 l2 l1)
249: (empty-shaker right shaker1 cocktail12 l1 l0)
250: (clean-shaker right left shaker1)
251: (leave right shaker1)
252: (grasp left shot19)
253: (fill-shot shot19 ingredient10 left right dispenser10)
254: (pour-shot-to-clean-shaker shot19 ingredient10 shaker1 left l0 l1)
255: (clean-shot shot19 ingredient10 left right)
256: (fill-shot shot19 ingredient4 left right dispenser4)
257: (pour-shot-to-used-shaker shot19 ingredient4 shaker1 left l1 l2)
258: (clean-shot shot19 ingredient4 left right)
259: (leave left shot19)
260: (grasp right shaker1)
261: (shake cocktail21 ingredient10 ingredient4 shaker1 right left)
262: (pour-shaker-to-shot cocktail21 shot19 right shaker1 l2 l1)
263: (empty-shaker right shaker1 cocktail21 l1 l0)
264: (clean-shaker right left shaker1)
265: (leave right shaker1)
266: (grasp left shot20)
267: (fill-shot shot20 ingredient13 left right dispenser13)
268: (pour-shot-to-clean-shaker shot20 ingredient13 shaker1 left l0 l1)
269: (clean-shot shot20 ingredient13 left right)
270: (fill-shot shot20 ingredient9 left right dispenser9)
271: (pour-shot-to-used-shaker shot20 ingredient9 shaker1 left l1 l2)
272: (clean-shot shot20 ingredient9 left right)
273: (leave left shot20)
274: (grasp right shaker1)
275: (shake cocktail24 ingredient13 ingredient9 shaker1 right left)
276: (pour-shaker-to-shot cocktail24 shot20 right shaker1 l2 l1)
277: (empty-shaker right shaker1 cocktail24 l1 l0)
278: (clean-shaker right left shaker1)
279: (leave right shaker1)
280: (grasp left shot21)
281: (fill-shot shot21 ingredient5 left right dispenser5)
282: (pour-shot-to-clean-shaker shot21 ingredient5 shaker1 left l0 l1)
283: (clean-shot shot21 ingredient5 left right)
284: (fill-shot shot21 ingredient20 left right dispenser20)
285: (pour-shot-to-used-shaker shot21 ingredient20 shaker1 left l1 l2)
286: (clean-shot shot21 ingredient20 left right)
287: (leave left shot21)
288: (grasp right shaker1)
289: (shake cocktail20 ingredient5 ingredient20 shaker1 right left)
290: (pour-shaker-to-shot cocktail20 shot21 right shaker1 l2 l1)
291: (empty-shaker right shaker1 cocktail20 l1 l0)
292: (clean-shaker right left shaker1)
293: (leave right shaker1)
294: (grasp left shot22)
295: (fill-shot shot22 ingredient6 left right dispenser6)
296: (pour-shot-to-clean-shaker shot22 ingredient6 shaker1 left l0 l1)
297: (clean-shot shot22 ingredient6 left right)
298: (fill-shot shot22 ingredient4 left right dispenser4)
299: (pour-shot-to-used-shaker shot22 ingredient4 shaker1 left l1 l2)
300: (clean-shot shot22 ingredient4 left right)
301: (leave left shot22)
302: (grasp right shaker1)
303: (shake cocktail30 ingredient6 ingredient4 shaker1 right left)
304: (pour-shaker-to-shot cocktail30 shot22 right shaker1 l2 l1)
305: (empty-shaker right shaker1 cocktail30 l1 l0)
306: (clean-shaker right left shaker1)
307: (leave right shaker1)
308: (grasp left shot23)
309: (fill-shot shot23 ingredient13 left right dispenser13)
310: (pour-shot-to-clean-shaker shot23 ingredient13 shaker1 left l0 l1)
311: (clean-shot shot23 ingredient13 left right)
312: (fill-shot shot23 ingredient18 left right dispenser18)
313: (pour-shot-to-used-shaker shot23 ingredient18 shaker1 left l1 l2)
314: (clean-shot shot23 ingredient18 left right)
315: (leave left shot23)
316: (grasp right shaker1)
317: (shake cocktail5 ingredient13 ingredient18 shaker1 right left)
318: (pour-shaker-to-shot cocktail5 shot23 right shaker1 l2 l1)
319: (empty-shaker right shaker1 cocktail5 l1 l0)
320: (clean-shaker right left shaker1)
321: (leave right shaker1)
322: (grasp left shot24)
323: (fill-shot shot24 ingredient1 left right dispenser1)
324: (pour-shot-to-clean-shaker shot24 ingredient1 shaker1 left l0 l1)
325: (clean-shot shot24 ingredient1 left right)
326: (fill-shot shot24 ingredient16 left right dispenser16)
327: (pour-shot-to-used-shaker shot24 ingredient16 shaker1 left l1 l2)
328: (clean-shot shot24 ingredient16 left right)
329: (leave left shot24)
330: (grasp right shaker1)
331: (shake cocktail15 ingredient1 ingredient16 shaker1 right left)
332: (pour-shaker-to-shot cocktail15 shot24 right shaker1 l2 l1)
333: (empty-shaker right shaker1 cocktail15 l1 l0)
334: (clean-shaker right left shaker1)
335: (leave right shaker1)
336: (grasp left shot25)
337: (fill-shot shot25 ingredient14 left right dispenser14)
338: (pour-shot-to-clean-shaker shot25 ingredient14 shaker1 left l0 l1)
339: (clean-shot shot25 ingredient14 left right)
340: (fill-shot shot25 ingredient10 left right dispenser10)
341: (pour-shot-to-used-shaker shot25 ingredient10 shaker1 left l1 l2)
342: (clean-shot shot25 ingredient10 left right)
343: (leave left shot25)
344: (grasp right shaker1)
345: (shake cocktail23 ingredient14 ingredient10 shaker1 right left)
346: (pour-shaker-to-shot cocktail23 shot25 right shaker1 l2 l1)
347: (empty-shaker right shaker1 cocktail23 l1 l0)
348: (clean-shaker right left shaker1)
349: (leave right shaker1)
350: (grasp left shot26)
351: (fill-shot shot26 ingredient4 left right dispenser4)
352: (pour-shot-to-clean-shaker shot26 ingredient4 shaker1 left l0 l1)
353: (clean-shot shot26 ingredient4 left right)
354: (fill-shot shot26 ingredient2 left right dispenser2)
355: (pour-shot-to-used-shaker shot26 ingredient2 shaker1 left l1 l2)
356: (clean-shot shot26 ingredient2 left right)
357: (leave left shot26)
358: (grasp right shaker1)
359: (shake cocktail26 ingredient4 ingredient2 shaker1 right left)
360: (pour-shaker-to-shot cocktail26 shot26 right shaker1 l2 l1)
361: (empty-shaker right shaker1 cocktail26 l1 l0)
362: (clean-shaker right left shaker1)
363: (leave right shaker1)
364: (grasp left shot27)
365: (fill-shot shot27 ingredient17 left right dispenser17)
366: (pour-shot-to-clean-shaker shot27 ingredient17 shaker1 left l0 l1)
367: (clean-shot shot27 ingredient17 left right)
368: (fill-shot shot27 ingredient11 left right dispenser11)
369: (pour-shot-to-used-shaker shot27 ingredient11 shaker1 left l1 l2)
370: (clean-shot shot27 ingredient11 left right)
371: (leave left shot27)
372: (grasp right shaker1)
373: (shake cocktail28 ingredient17 ingredient11 shaker1 right left)
374: (pour-shaker-to-shot cocktail28 shot27 right shaker1 l2 l1)
375: (empty-shaker right shaker1 cocktail28 l1 l0)
376: (clean-shaker right left shaker1)
377: (leave right shaker1)
378: (grasp left shot28)
379: (fill-shot shot28 ingredient5 left right dispenser5)
380: (pour-shot-to-clean-shaker shot28 ingredient5 shaker1 left l0 l1)
381: (clean-shot shot28 ingredient5 left right)
382: (fill-shot shot28 ingredient4 left right dispenser4)
383: (pour-shot-to-used-shaker shot28 ingredient4 shaker1 left l1 l2)
384: (clean-shot shot28 ingredient4 left right)
385: (leave left shot28)
386: (grasp right shaker1)
387: (shake cocktail13 ingredient5 ingredient4 shaker1 right left)
388: (pour-shaker-to-shot cocktail13 shot28 right shaker1 l2 l1)
389: (empty-shaker right shaker1 cocktail13 l1 l0)
390: (clean-shaker right left shaker1)
391: (leave right shaker1)
392: (grasp left shot29)
393: (fill-shot shot29 ingredient5 left right dispenser5)
394: (pour-shot-to-clean-shaker shot29 ingredient5 shaker1 left l0 l1)
395: (clean-shot shot29 ingredient5 left right)
396: (fill-shot shot29 ingredient15 left right dispenser15)
397: (pour-shot-to-used-shaker shot29 ingredient15 shaker1 left l1 l2)
398: (clean-shot shot29 ingredient15 left right)
399: (leave left shot29)
400: (grasp right shaker1)
401: (shake cocktail9 ingredient5 ingredient15 shaker1 right left)
402: (pour-shaker-to-shot cocktail9 shot29 right shaker1 l2 l1)
403: (empty-shaker right shaker1 cocktail9 l1 l0)
404: (clean-shaker right left shaker1)
405: (leave right shaker1)
406: (grasp left shot30)
407: (fill-shot shot30 ingredient14 left right dispenser14)
408: (pour-shot-to-clean-shaker shot30 ingredient14 shaker1 left l0 l1)
409: (clean-shot shot30 ingredient14 left right)
410: (fill-shot shot30 ingredient16 left right dispenser16)
411: (pour-shot-to-used-shaker shot30 ingredient16 shaker1 left l1 l2)
412: (clean-shot shot30 ingredient16 left right)
413: (leave left shot30)
414: (grasp right shaker1)
415: (shake cocktail1 ingredient14 ingredient16 shaker1 right left)
416: (pour-shaker-to-shot cocktail1 shot30 right shaker1 l2 l1)
417: (empty-shaker right shaker1 cocktail1 l1 l0)
418: (clean-shaker right left shaker1)
419: (leave right shaker1)
420: (grasp left shot31)
421: (fill-shot shot31 ingredient6 left right dispenser6)
422: (leave left shot31)
423: (grasp left shot32)
424: (fill-shot shot32 ingredient19 left right dispenser19)
425: (leave left shot32)
426: (grasp left shot33)
427: (fill-shot shot33 ingredient2 left right dispenser2)
428: (leave left shot33)
429: (grasp right shot34)
430: (fill-shot shot34 ingredient7 right left dispenser7)
431: (leave right shot34)
432: (grasp left shot35)
433: (fill-shot shot35 ingredient7 left right dispenser7)
434: (leave left shot35)
435: (grasp left shot36)
436: (fill-shot shot36 ingredient2 left right dispenser2)
437: (leave left shot36)
438: (grasp left shot37)
439: (fill-shot shot37 ingredient14 left right dispenser14)
440: (leave left shot37)
441: (grasp left shot38)
442: (fill-shot shot38 ingredient8 left right dispenser8)
443: (leave left shot38)
444: (grasp left shot39)
445: (fill-shot shot39 ingredient14 left right dispenser14)
446: (leave left shot39)



Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.026] Processed problem encoding.
[0.038] Calculated possible fact changes of composite elements.
[0.039] Initialized instantiation procedure.
[0.039] 
[0.039] *************************************
[0.039] * * *   M a k e s p a n     0   * * *
[0.039] *************************************
[0.054] Instantiated 35,929 initial clauses.
[0.054] The encoding contains a total of 18,334 distinct variables.
[0.054] Attempting solve with solver <glucose4> ...
c 27 assumptions
[0.055] Executed solver; result: UNSAT.
[0.055] 
[0.055] *************************************
[0.055] * * *   M a k e s p a n     1   * * *
[0.055] *************************************
[0.059] Computed next depth properties: array size of 105.
[0.069] Instantiated 2,938 transitional clauses.
[0.104] Instantiated 120,484 universal clauses.
[0.104] Instantiated and added clauses for a total of 159,351 clauses.
[0.104] The encoding contains a total of 58,646 distinct variables.
[0.104] Attempting solve with solver <glucose4> ...
c 105 assumptions
[0.110] Executed solver; result: UNSAT.
[0.110] 
[0.110] *************************************
[0.110] * * *   M a k e s p a n     2   * * *
[0.110] *************************************
[0.129] Computed next depth properties: array size of 261.
[0.153] Instantiated 41,678 transitional clauses.
[0.220] Instantiated 381,758 universal clauses.
[0.220] Instantiated and added clauses for a total of 582,787 clauses.
[0.220] The encoding contains a total of 136,050 distinct variables.
[0.220] Attempting solve with solver <glucose4> ...
c 261 assumptions
[0.233] Executed solver; result: UNSAT.
[0.233] 
[0.233] *************************************
[0.233] * * *   M a k e s p a n     3   * * *
[0.233] *************************************
[0.257] Computed next depth properties: array size of 365.
[0.362] Instantiated 182,520 transitional clauses.
[0.516] Instantiated 1,043,328 universal clauses.
[0.516] Instantiated and added clauses for a total of 1,808,635 clauses.
[0.516] The encoding contains a total of 267,820 distinct variables.
[0.516] Attempting solve with solver <glucose4> ...
c 365 assumptions
[0.563] Executed solver; result: UNSAT.
[0.563] 
[0.563] *************************************
[0.563] * * *   M a k e s p a n     4   * * *
[0.563] *************************************
[0.590] Computed next depth properties: array size of 469.
[0.704] Instantiated 188,760 transitional clauses.
[0.912] Instantiated 1,205,048 universal clauses.
[0.912] Instantiated and added clauses for a total of 3,202,443 clauses.
[0.912] The encoding contains a total of 402,190 distinct variables.
[0.912] Attempting solve with solver <glucose4> ...
c 469 assumptions
[0.971] Executed solver; result: UNSAT.
[0.971] 
[0.971] *************************************
[0.971] * * *   M a k e s p a n     5   * * *
[0.971] *************************************
[1.003] Computed next depth properties: array size of 573.
[1.118] Instantiated 188,760 transitional clauses.
[1.369] Instantiated 1,343,056 universal clauses.
[1.369] Instantiated and added clauses for a total of 4,734,259 clauses.
[1.369] The encoding contains a total of 536,664 distinct variables.
[1.369] Attempting solve with solver <glucose4> ...
c 573 assumptions
[1.448] Executed solver; result: UNSAT.
[1.448] 
[1.448] *************************************
[1.448] * * *   M a k e s p a n     6   * * *
[1.448] *************************************
[1.484] Computed next depth properties: array size of 677.
[1.598] Instantiated 188,760 transitional clauses.
[1.895] Instantiated 1,481,064 universal clauses.
[1.895] Instantiated and added clauses for a total of 6,404,083 clauses.
[1.895] The encoding contains a total of 671,242 distinct variables.
[1.895] Attempting solve with solver <glucose4> ...
c 677 assumptions
[1.979] Executed solver; result: UNSAT.
[1.979] 
[1.979] *************************************
[1.979] * * *   M a k e s p a n     7   * * *
[1.979] *************************************
[2.018] Computed next depth properties: array size of 781.
[2.136] Instantiated 188,760 transitional clauses.
[2.481] Instantiated 1,619,072 universal clauses.
[2.481] Instantiated and added clauses for a total of 8,211,915 clauses.
[2.481] The encoding contains a total of 805,924 distinct variables.
[2.481] Attempting solve with solver <glucose4> ...
c 781 assumptions
[2.587] Executed solver; result: UNSAT.
[2.587] 
[2.587] *************************************
[2.587] * * *   M a k e s p a n     8   * * *
[2.587] *************************************
[2.630] Computed next depth properties: array size of 885.
[2.745] Instantiated 188,760 transitional clauses.
[3.136] Instantiated 1,757,080 universal clauses.
[3.136] Instantiated and added clauses for a total of 10,157,755 clauses.
[3.136] The encoding contains a total of 940,710 distinct variables.
[3.136] Attempting solve with solver <glucose4> ...
c 885 assumptions
[3.259] Executed solver; result: UNSAT.
[3.259] 
[3.259] *************************************
[3.259] * * *   M a k e s p a n     9   * * *
[3.259] *************************************
[3.304] Computed next depth properties: array size of 989.
[3.420] Instantiated 188,760 transitional clauses.
[3.857] Instantiated 1,895,088 universal clauses.
[3.857] Instantiated and added clauses for a total of 12,241,603 clauses.
[3.857] The encoding contains a total of 1,075,600 distinct variables.
[3.857] Attempting solve with solver <glucose4> ...
c 989 assumptions
[3.997] Executed solver; result: UNSAT.
[3.997] 
[3.997] *************************************
[3.997] * * *   M a k e s p a n    10   * * *
[3.997] *************************************
[4.048] Computed next depth properties: array size of 1093.
[4.164] Instantiated 188,760 transitional clauses.
[4.653] Instantiated 2,033,096 universal clauses.
[4.653] Instantiated and added clauses for a total of 14,463,459 clauses.
[4.653] The encoding contains a total of 1,210,594 distinct variables.
[4.653] Attempting solve with solver <glucose4> ...
c 1093 assumptions
c last restart ## conflicts  :  7 13801 
[4.930] Executed solver; result: SAT.
[4.930] Solver returned SAT; a solution has been found at makespan 10.
94
solution 669 1
517 504 333 329 125 104 566 554 320 304 296 279 461 454 181 179 68 54 394 379 430 655 445 429 407 404 78 79 13 4 660 661 654 655 272 254 501 479 593 579 374 354 552 529 609 604 134 129 162 154 237 229 644 645 632 633 503 504 628 629 660 661 636 637 656 657 642 643 664 665 652 653 668 669 650 651 646 647 634 635 638 639 666 667 648 649 640 641 630 631 662 663 658 659 
[4.935] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.002] Instantiated 266 initial clauses.
[0.002] The encoding contains a total of 157 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     1   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 13.
[0.002] Instantiated 99 transitional clauses.
[0.003] Instantiated 1,050 universal clauses.
[0.003] Instantiated and added clauses for a total of 1,415 clauses.
[0.003] The encoding contains a total of 567 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.003] Executed solver; result: UNSAT.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     2   * * *
[0.003] *************************************
[0.003] Computed next depth properties: array size of 31.
[0.003] Instantiated 369 transitional clauses.
[0.005] Instantiated 3,081 universal clauses.
[0.005] Instantiated and added clauses for a total of 4,865 clauses.
[0.005] The encoding contains a total of 1,268 distinct variables.
[0.005] Attempting solve with solver <glucose4> ...
c 31 assumptions
[0.005] Executed solver; result: UNSAT.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     3   * * *
[0.005] *************************************
[0.005] Computed next depth properties: array size of 43.
[0.006] Instantiated 660 transitional clauses.
[0.008] Instantiated 4,608 universal clauses.
[0.008] Instantiated and added clauses for a total of 10,133 clauses.
[0.008] The encoding contains a total of 2,026 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 43 assumptions
[0.008] Executed solver; result: UNSAT.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     4   * * *
[0.008] *************************************
[0.009] Computed next depth properties: array size of 55.
[0.010] Instantiated 780 transitional clauses.
[0.012] Instantiated 5,772 universal clauses.
[0.012] Instantiated and added clauses for a total of 16,685 clauses.
[0.012] The encoding contains a total of 2,844 distinct variables.
[0.012] Attempting solve with solver <glucose4> ...
c 55 assumptions
c last restart ## conflicts  :  0 236 
[0.013] Executed solver; result: SAT.
[0.013] Solver returned SAT; a solution has been found at makespan 4.
12
solution 33 1
10 9 17 14 27 24 18 33 28 29 30 31 
[0.013] Exiting.

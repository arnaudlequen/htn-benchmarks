Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.018] Processed problem encoding.
[0.025] Calculated possible fact changes of composite elements.
[0.026] Initialized instantiation procedure.
[0.026] 
[0.026] *************************************
[0.026] * * *   M a k e s p a n     0   * * *
[0.026] *************************************
[0.034] Instantiated 17,979 initial clauses.
[0.034] The encoding contains a total of 9,254 distinct variables.
[0.034] Attempting solve with solver <glucose4> ...
c 19 assumptions
[0.034] Executed solver; result: UNSAT.
[0.034] 
[0.034] *************************************
[0.034] * * *   M a k e s p a n     1   * * *
[0.034] *************************************
[0.037] Computed next depth properties: array size of 73.
[0.043] Instantiated 1,746 transitional clauses.
[0.064] Instantiated 61,056 universal clauses.
[0.064] Instantiated and added clauses for a total of 80,781 clauses.
[0.064] The encoding contains a total of 29,749 distinct variables.
[0.064] Attempting solve with solver <glucose4> ...
c 73 assumptions
[0.067] Executed solver; result: UNSAT.
[0.067] 
[0.067] *************************************
[0.067] * * *   M a k e s p a n     2   * * *
[0.067] *************************************
[0.078] Computed next depth properties: array size of 181.
[0.093] Instantiated 21,222 transitional clauses.
[0.144] Instantiated 192,546 universal clauses.
[0.144] Instantiated and added clauses for a total of 294,549 clauses.
[0.144] The encoding contains a total of 68,829 distinct variables.
[0.144] Attempting solve with solver <glucose4> ...
c 181 assumptions
[0.151] Executed solver; result: UNSAT.
[0.151] 
[0.151] *************************************
[0.151] * * *   M a k e s p a n     3   * * *
[0.151] *************************************
[0.163] Computed next depth properties: array size of 253.
[0.208] Instantiated 88,056 transitional clauses.
[0.284] Instantiated 495,072 universal clauses.
[0.284] Instantiated and added clauses for a total of 877,677 clauses.
[0.284] The encoding contains a total of 133,487 distinct variables.
[0.284] Attempting solve with solver <glucose4> ...
c 253 assumptions
[0.309] Executed solver; result: UNSAT.
[0.309] 
[0.309] *************************************
[0.309] * * *   M a k e s p a n     4   * * *
[0.309] *************************************
[0.323] Computed next depth properties: array size of 325.
[0.372] Instantiated 91,656 transitional clauses.
[0.470] Instantiated 576,216 universal clauses.
[0.470] Instantiated and added clauses for a total of 1,545,549 clauses.
[0.470] The encoding contains a total of 199,657 distinct variables.
[0.470] Attempting solve with solver <glucose4> ...
c 325 assumptions
[0.493] Executed solver; result: UNSAT.
[0.493] 
[0.493] *************************************
[0.493] * * *   M a k e s p a n     5   * * *
[0.493] *************************************
[0.508] Computed next depth properties: array size of 397.
[0.556] Instantiated 91,656 transitional clauses.
[0.674] Instantiated 644,400 universal clauses.
[0.674] Instantiated and added clauses for a total of 2,281,605 clauses.
[0.674] The encoding contains a total of 265,899 distinct variables.
[0.674] Attempting solve with solver <glucose4> ...
c 397 assumptions
[0.707] Executed solver; result: UNSAT.
[0.707] 
[0.707] *************************************
[0.707] * * *   M a k e s p a n     6   * * *
[0.707] *************************************
[0.723] Computed next depth properties: array size of 469.
[0.771] Instantiated 91,656 transitional clauses.
[0.912] Instantiated 712,584 universal clauses.
[0.912] Instantiated and added clauses for a total of 3,085,845 clauses.
[0.912] The encoding contains a total of 332,213 distinct variables.
[0.912] Attempting solve with solver <glucose4> ...
c 469 assumptions
c last restart ## conflicts  :  4 3894 
[0.968] Executed solver; result: SAT.
[0.968] Solver returned SAT; a solution has been found at makespan 6.
60
solution 473 1
66 67 9 4 137 451 187 172 352 340 269 463 262 256 129 130 302 298 90 88 444 445 450 451 462 463 458 459 56 447 169 151 420 403 366 361 123 109 320 319 460 461 448 449 470 471 433 473 210 457 452 453 454 455 468 469 466 467 464 465 
[0.969] Exiting.

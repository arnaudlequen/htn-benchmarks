Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.042] Processed problem encoding.
[0.071] Calculated possible fact changes of composite elements.
[0.073] Initialized instantiation procedure.
[0.073] 
[0.073] *************************************
[0.073] * * *   M a k e s p a n     0   * * *
[0.073] *************************************
[0.099] Instantiated 72,834 initial clauses.
[0.099] The encoding contains a total of 36,973 distinct variables.
[0.099] Attempting solve with solver <glucose4> ...
c 36 assumptions
[0.100] Executed solver; result: UNSAT.
[0.100] 
[0.100] *************************************
[0.100] * * *   M a k e s p a n     1   * * *
[0.100] *************************************
[0.108] Computed next depth properties: array size of 141.
[0.123] Instantiated 4,795 transitional clauses.
[0.180] Instantiated 239,680 universal clauses.
[0.180] Instantiated and added clauses for a total of 317,309 clauses.
[0.180] The encoding contains a total of 116,855 distinct variables.
[0.180] Attempting solve with solver <glucose4> ...
c 141 assumptions
[0.200] Executed solver; result: UNSAT.
[0.200] 
[0.200] *************************************
[0.200] * * *   M a k e s p a n     2   * * *
[0.200] *************************************
[0.225] Computed next depth properties: array size of 351.
[0.266] Instantiated 82,565 transitional clauses.
[0.412] Instantiated 754,145 universal clauses.
[0.412] Instantiated and added clauses for a total of 1,154,019 clauses.
[0.412] The encoding contains a total of 271,452 distinct variables.
[0.412] Attempting solve with solver <glucose4> ...
c 351 assumptions
[0.460] Executed solver; result: UNSAT.
[0.460] 
[0.460] *************************************
[0.460] * * *   M a k e s p a n     3   * * *
[0.460] *************************************
[0.512] Computed next depth properties: array size of 491.
[0.777] Instantiated 382,620 transitional clauses.
[1.125] Instantiated 2,157,120 universal clauses.
[1.125] Instantiated and added clauses for a total of 3,693,759 clauses.
[1.125] The encoding contains a total of 542,914 distinct variables.
[1.125] Attempting solve with solver <glucose4> ...
c 491 assumptions
[1.265] Executed solver; result: UNSAT.
[1.265] 
[1.265] *************************************
[1.265] * * *   M a k e s p a n     4   * * *
[1.265] *************************************
[1.327] Computed next depth properties: array size of 631.
[1.614] Instantiated 393,120 transitional clauses.
[2.050] Instantiated 2,479,400 universal clauses.
[2.050] Instantiated and added clauses for a total of 6,566,279 clauses.
[2.050] The encoding contains a total of 818,716 distinct variables.
[2.050] Attempting solve with solver <glucose4> ...
c 631 assumptions
[2.173] Executed solver; result: UNSAT.
[2.173] 
[2.173] *************************************
[2.173] * * *   M a k e s p a n     5   * * *
[2.173] *************************************
[2.239] Computed next depth properties: array size of 771.
[2.515] Instantiated 393,120 transitional clauses.
[3.031] Instantiated 2,761,780 universal clauses.
[3.031] Instantiated and added clauses for a total of 9,721,179 clauses.
[3.031] The encoding contains a total of 1,094,658 distinct variables.
[3.031] Attempting solve with solver <glucose4> ...
c 771 assumptions
[3.195] Executed solver; result: UNSAT.
[3.195] 
[3.195] *************************************
[3.195] * * *   M a k e s p a n     6   * * *
[3.195] *************************************
[3.268] Computed next depth properties: array size of 911.
[3.542] Instantiated 393,120 transitional clauses.
[4.163] Instantiated 3,044,160 universal clauses.
[4.163] Instantiated and added clauses for a total of 13,158,459 clauses.
[4.163] The encoding contains a total of 1,370,740 distinct variables.
[4.163] Attempting solve with solver <glucose4> ...
c 911 assumptions
[4.362] Executed solver; result: UNSAT.
[4.362] 
[4.362] *************************************
[4.362] * * *   M a k e s p a n     7   * * *
[4.362] *************************************
[4.446] Computed next depth properties: array size of 1051.
[4.728] Instantiated 393,120 transitional clauses.
[5.440] Instantiated 3,326,540 universal clauses.
[5.440] Instantiated and added clauses for a total of 16,878,119 clauses.
[5.440] The encoding contains a total of 1,646,962 distinct variables.
[5.440] Attempting solve with solver <glucose4> ...
c 1051 assumptions
[5.688] Executed solver; result: UNSAT.
[5.688] 
[5.688] *************************************
[5.688] * * *   M a k e s p a n     8   * * *
[5.688] *************************************
[5.778] Computed next depth properties: array size of 1191.
[6.055] Instantiated 393,120 transitional clauses.
[6.862] Instantiated 3,608,920 universal clauses.
[6.862] Instantiated and added clauses for a total of 20,880,159 clauses.
[6.862] The encoding contains a total of 1,923,324 distinct variables.
[6.862] Attempting solve with solver <glucose4> ...
c 1191 assumptions
c last restart ## conflicts  :  22 13257 
[7.879] Executed solver; result: SAT.
[7.879] Solver returned SAT; a solution has been found at makespan 8.
98
solution 1009 1
172 159 450 438 705 993 391 376 517 500 569 562 227 221 204 969 649 624 831 810 663 655 264 252 417 407 340 314 866 841 802 779 726 717 306 283 765 748 542 531 347 345 68 66 1002 1003 700 686 976 977 992 993 498 979 990 991 996 997 988 989 980 981 962 1009 208 190 621 987 968 969 843 841 964 965 1002 1003 1000 1001 876 1005 96 967 982 983 970 971 974 975 972 973 994 995 984 985 998 999 1006 1007 
[7.888] Exiting.

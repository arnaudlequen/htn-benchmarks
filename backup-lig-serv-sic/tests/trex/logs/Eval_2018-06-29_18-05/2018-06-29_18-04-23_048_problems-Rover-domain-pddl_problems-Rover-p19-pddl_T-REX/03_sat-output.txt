Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.010] Parsed head comment information.
[0.558] Processed problem encoding.
[1.716] Calculated possible fact changes of composite elements.
[1.728] Initialized instantiation procedure.
[1.728] 
[1.728] *************************************
[1.728] * * *   M a k e s p a n     0   * * *
[1.728] *************************************
[1.737] Instantiated 36,699 initial clauses.
[1.737] The encoding contains a total of 19,950 distinct variables.
[1.737] Attempting solve with solver <glucose4> ...
c 36 assumptions
[1.737] Executed solver; result: UNSAT.
[1.737] 
[1.737] *************************************
[1.737] * * *   M a k e s p a n     1   * * *
[1.737] *************************************
[1.854] Computed next depth properties: array size of 141.
[1.955] Instantiated 107,980 transitional clauses.
[2.074] Instantiated 300,352 universal clauses.
[2.074] Instantiated and added clauses for a total of 445,031 clauses.
[2.074] The encoding contains a total of 137,965 distinct variables.
[2.074] Attempting solve with solver <glucose4> ...
c 141 assumptions
[2.074] Executed solver; result: UNSAT.
[2.074] 
[2.074] *************************************
[2.074] * * *   M a k e s p a n     2   * * *
[2.074] *************************************
[2.396] Computed next depth properties: array size of 252.
[2.861] Instantiated 731,087 transitional clauses.
[3.173] Instantiated 1,000,642 universal clauses.
[3.173] Instantiated and added clauses for a total of 2,176,760 clauses.
[3.173] The encoding contains a total of 529,521 distinct variables.
[3.173] Attempting solve with solver <glucose4> ...
c 252 assumptions
[3.173] Executed solver; result: UNSAT.
[3.173] 
[3.173] *************************************
[3.173] * * *   M a k e s p a n     3   * * *
[3.173] *************************************
[3.777] Computed next depth properties: array size of 439.
[4.896] Instantiated 2,083,411 transitional clauses.
[5.425] Instantiated 1,917,563 universal clauses.
[5.425] Instantiated and added clauses for a total of 6,177,734 clauses.
[5.425] The encoding contains a total of 1,111,281 distinct variables.
[5.425] Attempting solve with solver <glucose4> ...
c 439 assumptions
[5.736] Executed solver; result: UNSAT.
[5.736] 
[5.736] *************************************
[5.736] * * *   M a k e s p a n     4   * * *
[5.736] *************************************
[6.633] Computed next depth properties: array size of 667.
[8.231] Instantiated 3,005,723 transitional clauses.
[8.947] Instantiated 2,489,031 universal clauses.
[8.947] Instantiated and added clauses for a total of 11,672,488 clauses.
[8.947] The encoding contains a total of 1,727,884 distinct variables.
[8.948] Attempting solve with solver <glucose4> ...
c 667 assumptions
c last restart ## conflicts  :  224 11916 
[24.128] Executed solver; result: SAT.
[24.128] Solver returned SAT; a solution has been found at makespan 4.
488
solution 6052 1
99 1504 7 1417 8 100 1569 75 1478 7 1419 8 76 2354 89 798 69 780 70 90 1577 95 804 69 779 70 96 1828 11 254 107 346 108 12 1552 67 313 107 345 108 68 1649 83 1377 27 1312 15 1305 16 28 84 1586 2450 89 796 45 759 46 90 1527 1613 115 818 45 757 46 116 1884 75 1253 117 1174 19 1197 20 118 76 1591 101 1277 19 1195 20 102 2115 99 1393 15 1303 27 1316 28 16 100 1532 1574 121 1387 27 1318 28 122 2229 103 1395 27 1314 75 1362 76 28 104 1532 1538 7 1294 75 1361 76 8 2162 99 1506 100 1533 1545 15 1432 16 2314 89 796 45 758 46 90 1527 1601 105 809 45 757 46 106 1866 43 862 47 867 13 833 14 48 44 1548 59 878 13 830 14 60 1912 117 1289 75 1363 27 1318 28 76 118 1532 1598 2451 117 1178 75 1256 41 1216 42 76 118 1531 1561 71 1251 72 2069 89 800 90 1527 1607 2441 21 152 22 2486 61 184 21 155 22 62 2605 113 817 114 1527 2524 89 796 45 747 46 90 3075 61 1001 21 962 13 948 14 22 62 2483 57 998 13 947 14 58 3163 105 228 21 147 3 126 4 22 106 1523 2507 81 200 3 128 4 82 2629 99 1505 11 1429 12 100 1533 2562 109 1516 11 1428 12 110 3566 99 1505 11 1425 55 1466 56 12 100 1533 2548 3597 109 463 11 367 12 110 2550 107 461 11 366 12 108 2826 91 215 3 125 4 92 1523 2458 21 154 22 2566 99 448 11 368 109 467 110 12 100 1525 2536 103 458 109 465 110 104 2805 117 701 45 754 46 118 1527 2496 73 783 45 758 46 74 3043 41 397 42 1525 2501 75 427 76 2756 89 211 21 147 3 129 4 22 90 1523 2528 97 220 3 128 4 98 2655 95 680 96 2474 3578 35 624 95 679 96 36 1526 2467 33 618 95 683 96 34 2856 41 398 42 1525 2515 85 433 86 2776 6052 53 1095 54 4550 77 1119 17 1065 18 78 5722 99 1155 100 6033 17 1064 77 1122 45 1085 46 78 18 5009 5736 117 1055 45 1086 46 118 6036 21 1069 45 1085 46 22 3886 5706 79 545 111 578 112 80 5914 59 520 111 581 79 543 80 112 60 5389 43 502 44 5613 5871 41 400 109 463 110 42 4676 11 366 12 5557 105 809 106 5946 45 757 89 793 90 46 3614 5 707 89 800 90 6 5625 
[24.135] Exiting.

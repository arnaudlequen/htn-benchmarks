Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.042] Processed problem encoding.
[0.052] Calculated possible fact changes of composite elements.
[0.055] Initialized instantiation procedure.
[0.055] 
[0.055] *************************************
[0.055] * * *   M a k e s p a n     0   * * *
[0.055] *************************************
[0.058] Instantiated 5,210 initial clauses.
[0.058] The encoding contains a total of 3,198 distinct variables.
[0.058] Attempting solve with solver <glucose4> ...
c 11 assumptions
[0.058] Executed solver; result: UNSAT.
[0.058] 
[0.058] *************************************
[0.058] * * *   M a k e s p a n     1   * * *
[0.058] *************************************
[0.061] Computed next depth properties: array size of 41.
[0.067] Instantiated 6,873 transitional clauses.
[0.077] Instantiated 29,825 universal clauses.
[0.077] Instantiated and added clauses for a total of 41,908 clauses.
[0.077] The encoding contains a total of 11,207 distinct variables.
[0.077] Attempting solve with solver <glucose4> ...
c 41 assumptions
[0.077] Executed solver; result: UNSAT.
[0.077] 
[0.077] *************************************
[0.077] * * *   M a k e s p a n     2   * * *
[0.077] *************************************
[0.085] Computed next depth properties: array size of 121.
[0.105] Instantiated 25,066 transitional clauses.
[0.129] Instantiated 102,249 universal clauses.
[0.129] Instantiated and added clauses for a total of 169,223 clauses.
[0.129] The encoding contains a total of 31,427 distinct variables.
[0.129] Attempting solve with solver <glucose4> ...
c 121 assumptions
[0.130] Executed solver; result: UNSAT.
[0.130] 
[0.130] *************************************
[0.130] * * *   M a k e s p a n     3   * * *
[0.130] *************************************
[0.145] Computed next depth properties: array size of 241.
[0.192] Instantiated 104,009 transitional clauses.
[0.231] Instantiated 246,116 universal clauses.
[0.231] Instantiated and added clauses for a total of 519,348 clauses.
[0.231] The encoding contains a total of 68,242 distinct variables.
[0.231] Attempting solve with solver <glucose4> ...
c 241 assumptions
[0.231] Executed solver; result: UNSAT.
[0.231] 
[0.231] *************************************
[0.231] * * *   M a k e s p a n     4   * * *
[0.231] *************************************
[0.249] Computed next depth properties: array size of 361.
[0.326] Instantiated 181,598 transitional clauses.
[0.385] Instantiated 359,812 universal clauses.
[0.385] Instantiated and added clauses for a total of 1,060,758 clauses.
[0.385] The encoding contains a total of 111,948 distinct variables.
[0.385] Attempting solve with solver <glucose4> ...
c 361 assumptions
[0.414] Executed solver; result: UNSAT.
[0.414] 
[0.414] *************************************
[0.414] * * *   M a k e s p a n     5   * * *
[0.414] *************************************
[0.435] Computed next depth properties: array size of 481.
[0.517] Instantiated 189,806 transitional clauses.
[0.591] Instantiated 395,538 universal clauses.
[0.591] Instantiated and added clauses for a total of 1,646,102 clauses.
[0.591] The encoding contains a total of 156,359 distinct variables.
[0.591] Attempting solve with solver <glucose4> ...
c 481 assumptions
c last restart ## conflicts  :  39 3974 
[0.655] Executed solver; result: SAT.
[0.655] Solver returned SAT; a solution has been found at makespan 5.
50
solution 382 1
82 338 176 85 261 100 206 98 90 91 74 26 78 253 164 196 155 147 76 148 42 10 84 382 86 143 354 140 135 136 33 6 79 55 17 43 11 81 52 14 27 3 38 7 77 65 21 82 72 25 
[0.657] Exiting.

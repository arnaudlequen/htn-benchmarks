Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.022] Processed problem encoding.
[0.026] Calculated possible fact changes of composite elements.
[0.027] Initialized instantiation procedure.
[0.027] 
[0.027] *************************************
[0.027] * * *   M a k e s p a n     0   * * *
[0.027] *************************************
[0.028] Instantiated 2,368 initial clauses.
[0.028] The encoding contains a total of 1,464 distinct variables.
[0.028] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.029] Executed solver; result: UNSAT.
[0.029] 
[0.029] *************************************
[0.029] * * *   M a k e s p a n     1   * * *
[0.029] *************************************
[0.030] Computed next depth properties: array size of 25.
[0.033] Instantiated 2,865 transitional clauses.
[0.037] Instantiated 12,921 universal clauses.
[0.037] Instantiated and added clauses for a total of 18,154 clauses.
[0.037] The encoding contains a total of 4,949 distinct variables.
[0.037] Attempting solve with solver <glucose4> ...
c 25 assumptions
[0.037] Executed solver; result: UNSAT.
[0.037] 
[0.037] *************************************
[0.037] * * *   M a k e s p a n     2   * * *
[0.037] *************************************
[0.041] Computed next depth properties: array size of 73.
[0.050] Instantiated 9,684 transitional clauses.
[0.062] Instantiated 40,053 universal clauses.
[0.062] Instantiated and added clauses for a total of 67,891 clauses.
[0.062] The encoding contains a total of 12,781 distinct variables.
[0.062] Attempting solve with solver <glucose4> ...
c 73 assumptions
[0.062] Executed solver; result: UNSAT.
[0.062] 
[0.062] *************************************
[0.062] * * *   M a k e s p a n     3   * * *
[0.062] *************************************
[0.068] Computed next depth properties: array size of 145.
[0.091] Instantiated 33,525 transitional clauses.
[0.111] Instantiated 88,032 universal clauses.
[0.111] Instantiated and added clauses for a total of 189,448 clauses.
[0.111] The encoding contains a total of 26,286 distinct variables.
[0.111] Attempting solve with solver <glucose4> ...
c 145 assumptions
[0.111] Executed solver; result: UNSAT.
[0.111] 
[0.111] *************************************
[0.111] * * *   M a k e s p a n     4   * * *
[0.111] *************************************
[0.119] Computed next depth properties: array size of 217.
[0.150] Instantiated 57,996 transitional clauses.
[0.171] Instantiated 127,044 universal clauses.
[0.171] Instantiated and added clauses for a total of 374,488 clauses.
[0.171] The encoding contains a total of 42,176 distinct variables.
[0.171] Attempting solve with solver <glucose4> ...
c 217 assumptions
[0.171] Executed solver; result: UNSAT.
[0.171] 
[0.171] *************************************
[0.171] * * *   M a k e s p a n     5   * * *
[0.171] *************************************
[0.178] Computed next depth properties: array size of 289.
[0.202] Instantiated 61,188 transitional clauses.
[0.226] Instantiated 141,498 universal clauses.
[0.226] Instantiated and added clauses for a total of 577,174 clauses.
[0.226] The encoding contains a total of 58,369 distinct variables.
[0.226] Attempting solve with solver <glucose4> ...
c 289 assumptions
c last restart ## conflicts  :  5 2278 
[0.244] Executed solver; result: SAT.
[0.244] Solver returned SAT; a solution has been found at makespan 5.
34
solution 247 1
47 213 73 147 49 65 123 55 51 53 37 14 40 247 118 48 81 82 35 13 204 44 115 108 109 33 12 30 9 48 22 7 18 3 
[0.244] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.153] Processed problem encoding.
[0.193] Calculated possible fact changes of composite elements.
[0.201] Initialized instantiation procedure.
[0.201] 
[0.201] *************************************
[0.201] * * *   M a k e s p a n     0   * * *
[0.201] *************************************
[0.204] Instantiated 10,463 initial clauses.
[0.204] The encoding contains a total of 8,378 distinct variables.
[0.204] Attempting solve with solver <glucose4> ...
c 8 assumptions
[0.204] Executed solver; result: UNSAT.
[0.204] 
[0.204] *************************************
[0.204] * * *   M a k e s p a n     1   * * *
[0.204] *************************************
[0.208] Computed next depth properties: array size of 29.
[0.219] Instantiated 31,027 transitional clauses.
[0.229] Instantiated 58,588 universal clauses.
[0.229] Instantiated and added clauses for a total of 100,078 clauses.
[0.229] The encoding contains a total of 18,982 distinct variables.
[0.229] Attempting solve with solver <glucose4> ...
c 29 assumptions
[0.229] Executed solver; result: UNSAT.
[0.229] 
[0.229] *************************************
[0.229] * * *   M a k e s p a n     2   * * *
[0.229] *************************************
[0.239] Computed next depth properties: array size of 85.
[0.274] Instantiated 72,027 transitional clauses.
[0.303] Instantiated 191,104 universal clauses.
[0.303] Instantiated and added clauses for a total of 363,209 clauses.
[0.303] The encoding contains a total of 47,178 distinct variables.
[0.303] Attempting solve with solver <glucose4> ...
c 85 assumptions
[0.305] Executed solver; result: UNSAT.
[0.305] 
[0.305] *************************************
[0.305] * * *   M a k e s p a n     3   * * *
[0.305] *************************************
[0.330] Computed next depth properties: array size of 169.
[0.444] Instantiated 233,897 transitional clauses.
[0.504] Instantiated 363,504 universal clauses.
[0.504] Instantiated and added clauses for a total of 960,610 clauses.
[0.504] The encoding contains a total of 98,068 distinct variables.
[0.504] Attempting solve with solver <glucose4> ...
c 169 assumptions
[0.527] Executed solver; result: UNSAT.
[0.527] 
[0.527] *************************************
[0.527] * * *   M a k e s p a n     4   * * *
[0.527] *************************************
[0.569] Computed next depth properties: array size of 253.
[0.776] Instantiated 442,636 transitional clauses.
[0.853] Instantiated 481,556 universal clauses.
[0.853] Instantiated and added clauses for a total of 1,884,802 clauses.
[0.853] The encoding contains a total of 156,052 distinct variables.
[0.853] Attempting solve with solver <glucose4> ...
c 253 assumptions
c last restart ## conflicts  :  9 1718 
[0.897] Executed solver; result: SAT.
[0.897] Solver returned SAT; a solution has been found at makespan 4.
42
solution 904 1
396 397 146 533 364 537 107 22 218 686 626 330 904 259 589 578 266 580 73 13 293 518 520 238 124 28 80 19 293 690 486 239 32 3 298 411 414 190 37 4 53 11 
[0.898] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.002] Processed problem encoding.
[0.002] Calculated possible fact changes of composite elements.
[0.002] Initialized instantiation procedure.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     0   * * *
[0.002] *************************************
[0.002] Instantiated 216 initial clauses.
[0.002] The encoding contains a total of 142 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     1   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 9.
[0.003] Instantiated 200 transitional clauses.
[0.003] Instantiated 952 universal clauses.
[0.003] Instantiated and added clauses for a total of 1,368 clauses.
[0.003] The encoding contains a total of 432 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 9 assumptions
[0.003] Executed solver; result: UNSAT.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     2   * * *
[0.003] *************************************
[0.003] Computed next depth properties: array size of 25.
[0.004] Instantiated 514 transitional clauses.
[0.005] Instantiated 2,220 universal clauses.
[0.005] Instantiated and added clauses for a total of 4,102 clauses.
[0.005] The encoding contains a total of 942 distinct variables.
[0.005] Attempting solve with solver <glucose4> ...
c 25 assumptions
[0.005] Executed solver; result: UNSAT.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     3   * * *
[0.005] *************************************
[0.006] Computed next depth properties: array size of 49.
[0.006] Instantiated 510 transitional clauses.
[0.008] Instantiated 2,902 universal clauses.
[0.008] Instantiated and added clauses for a total of 7,514 clauses.
[0.008] The encoding contains a total of 1,574 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 49 assumptions
c last restart ## conflicts  :  0 152 
[0.008] Executed solver; result: SAT.
[0.008] Solver returned SAT; a solution has been found at makespan 3.
12
solution 29 1
17 24 25 9 27 11 29 7 4 16 6 3 
[0.008] Exiting.

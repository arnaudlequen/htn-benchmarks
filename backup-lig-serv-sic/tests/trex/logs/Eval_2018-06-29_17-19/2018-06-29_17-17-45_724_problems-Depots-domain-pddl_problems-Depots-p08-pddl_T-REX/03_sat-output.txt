Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.032] Processed problem encoding.
[0.040] Calculated possible fact changes of composite elements.
[0.041] Initialized instantiation procedure.
[0.041] 
[0.041] *************************************
[0.041] * * *   M a k e s p a n     0   * * *
[0.041] *************************************
[0.043] Instantiated 3,772 initial clauses.
[0.043] The encoding contains a total of 2,264 distinct variables.
[0.043] Attempting solve with solver <glucose4> ...
c 8 assumptions
[0.043] Executed solver; result: UNSAT.
[0.043] 
[0.043] *************************************
[0.043] * * *   M a k e s p a n     1   * * *
[0.043] *************************************
[0.044] Computed next depth properties: array size of 29.
[0.048] Instantiated 3,540 transitional clauses.
[0.054] Instantiated 17,946 universal clauses.
[0.054] Instantiated and added clauses for a total of 25,258 clauses.
[0.054] The encoding contains a total of 7,378 distinct variables.
[0.054] Attempting solve with solver <glucose4> ...
c 29 assumptions
[0.054] Executed solver; result: UNSAT.
[0.054] 
[0.054] *************************************
[0.054] * * *   M a k e s p a n     2   * * *
[0.054] *************************************
[0.059] Computed next depth properties: array size of 85.
[0.070] Instantiated 12,844 transitional clauses.
[0.085] Instantiated 56,042 universal clauses.
[0.085] Instantiated and added clauses for a total of 94,144 clauses.
[0.085] The encoding contains a total of 18,719 distinct variables.
[0.085] Attempting solve with solver <glucose4> ...
c 85 assumptions
[0.085] Executed solver; result: UNSAT.
[0.085] 
[0.085] *************************************
[0.085] * * *   M a k e s p a n     3   * * *
[0.085] *************************************
[0.095] Computed next depth properties: array size of 169.
[0.125] Instantiated 49,847 transitional clauses.
[0.150] Instantiated 129,524 universal clauses.
[0.150] Instantiated and added clauses for a total of 273,515 clauses.
[0.150] The encoding contains a total of 38,862 distinct variables.
[0.150] Attempting solve with solver <glucose4> ...
c 169 assumptions
[0.150] Executed solver; result: UNSAT.
[0.150] 
[0.150] *************************************
[0.150] * * *   M a k e s p a n     4   * * *
[0.150] *************************************
[0.159] Computed next depth properties: array size of 253.
[0.197] Instantiated 90,248 transitional clauses.
[0.227] Instantiated 191,736 universal clauses.
[0.227] Instantiated and added clauses for a total of 555,499 clauses.
[0.227] The encoding contains a total of 62,834 distinct variables.
[0.227] Attempting solve with solver <glucose4> ...
c 253 assumptions
[0.238] Executed solver; result: UNSAT.
[0.238] 
[0.238] *************************************
[0.238] * * *   M a k e s p a n     5   * * *
[0.238] *************************************
[0.249] Computed next depth properties: array size of 337.
[0.290] Instantiated 94,010 transitional clauses.
[0.327] Instantiated 213,086 universal clauses.
[0.327] Instantiated and added clauses for a total of 862,595 clauses.
[0.327] The encoding contains a total of 87,169 distinct variables.
[0.327] Attempting solve with solver <glucose4> ...
c 337 assumptions
c last restart ## conflicts  :  11 2685 
[0.351] Executed solver; result: SAT.
[0.351] Solver returned SAT; a solution has been found at makespan 5.
40
solution 314 1
43 135 137 46 54 40 55 35 13 39 297 131 47 314 36 97 205 94 81 82 31 11 41 28 10 45 148 79 41 142 43 122 14 3 262 12 160 8 18 6 
[0.352] Exiting.

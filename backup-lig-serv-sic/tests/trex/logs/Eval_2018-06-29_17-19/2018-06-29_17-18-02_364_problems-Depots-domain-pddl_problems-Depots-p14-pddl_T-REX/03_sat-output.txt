Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.070] Processed problem encoding.
[0.092] Calculated possible fact changes of composite elements.
[0.096] Initialized instantiation procedure.
[0.096] 
[0.096] *************************************
[0.096] * * *   M a k e s p a n     0   * * *
[0.096] *************************************
[0.099] Instantiated 6,236 initial clauses.
[0.099] The encoding contains a total of 4,173 distinct variables.
[0.099] Attempting solve with solver <glucose4> ...
c 8 assumptions
[0.099] Executed solver; result: UNSAT.
[0.099] 
[0.099] *************************************
[0.099] * * *   M a k e s p a n     1   * * *
[0.099] *************************************
[0.101] Computed next depth properties: array size of 29.
[0.107] Instantiated 9,904 transitional clauses.
[0.115] Instantiated 28,808 universal clauses.
[0.115] Instantiated and added clauses for a total of 44,948 clauses.
[0.115] The encoding contains a total of 11,203 distinct variables.
[0.115] Attempting solve with solver <glucose4> ...
c 29 assumptions
[0.115] Executed solver; result: UNSAT.
[0.115] 
[0.115] *************************************
[0.115] * * *   M a k e s p a n     2   * * *
[0.115] *************************************
[0.122] Computed next depth properties: array size of 85.
[0.140] Instantiated 27,625 transitional clauses.
[0.156] Instantiated 93,224 universal clauses.
[0.156] Instantiated and added clauses for a total of 165,797 clauses.
[0.156] The encoding contains a total of 27,643 distinct variables.
[0.156] Attempting solve with solver <glucose4> ...
c 85 assumptions
[0.156] Executed solver; result: UNSAT.
[0.156] 
[0.156] *************************************
[0.156] * * *   M a k e s p a n     3   * * *
[0.156] *************************************
[0.168] Computed next depth properties: array size of 169.
[0.208] Instantiated 102,074 transitional clauses.
[0.240] Instantiated 215,134 universal clauses.
[0.240] Instantiated and added clauses for a total of 483,005 clauses.
[0.240] The encoding contains a total of 58,961 distinct variables.
[0.240] Attempting solve with solver <glucose4> ...
c 169 assumptions
c last restart ## conflicts  :  13 638 
[0.250] Executed solver; result: SAT.
[0.250] Solver returned SAT; a solution has been found at makespan 3.
33
solution 685 1
82 685 252 247 249 100 168 169 29 7 37 9 66 462 229 85 43 15 64 377 130 58 26 6 69 312 119 62 302 108 231 21 3 
[0.251] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.162] Processed problem encoding.
[0.229] Calculated possible fact changes of composite elements.
[0.245] Initialized instantiation procedure.
[0.245] 
[0.245] *************************************
[0.245] * * *   M a k e s p a n     0   * * *
[0.245] *************************************
[0.251] Instantiated 17,023 initial clauses.
[0.251] The encoding contains a total of 11,749 distinct variables.
[0.251] Attempting solve with solver <glucose4> ...
c 12 assumptions
[0.251] Executed solver; result: UNSAT.
[0.251] 
[0.251] *************************************
[0.251] * * *   M a k e s p a n     1   * * *
[0.251] *************************************
[0.257] Computed next depth properties: array size of 45.
[0.273] Instantiated 32,146 transitional clauses.
[0.292] Instantiated 83,324 universal clauses.
[0.292] Instantiated and added clauses for a total of 132,493 clauses.
[0.292] The encoding contains a total of 31,884 distinct variables.
[0.292] Attempting solve with solver <glucose4> ...
c 45 assumptions
[0.292] Executed solver; result: UNSAT.
[0.292] 
[0.292] *************************************
[0.292] * * *   M a k e s p a n     2   * * *
[0.292] *************************************
[0.315] Computed next depth properties: array size of 133.
[0.373] Instantiated 95,608 transitional clauses.
[0.435] Instantiated 336,760 universal clauses.
[0.435] Instantiated and added clauses for a total of 564,861 clauses.
[0.435] The encoding contains a total of 94,970 distinct variables.
[0.435] Attempting solve with solver <glucose4> ...
c 133 assumptions
[0.435] Executed solver; result: UNSAT.
[0.435] 
[0.435] *************************************
[0.435] * * *   M a k e s p a n     3   * * *
[0.435] *************************************
[0.496] Computed next depth properties: array size of 265.
[0.765] Instantiated 526,272 transitional clauses.
[0.922] Instantiated 942,392 universal clauses.
[0.922] Instantiated and added clauses for a total of 2,033,525 clauses.
[0.922] The encoding contains a total of 225,290 distinct variables.
[0.922] Attempting solve with solver <glucose4> ...
c 265 assumptions
[0.922] Executed solver; result: UNSAT.
[0.922] 
[0.922] *************************************
[0.922] * * *   M a k e s p a n     4   * * *
[0.922] *************************************
[1.012] Computed next depth properties: array size of 397.
[1.535] Instantiated 1,017,042 transitional clauses.
[1.771] Instantiated 1,457,254 universal clauses.
[1.771] Instantiated and added clauses for a total of 4,507,821 clauses.
[1.771] The encoding contains a total of 382,855 distinct variables.
[1.771] Attempting solve with solver <glucose4> ...
c 397 assumptions
[1.868] Executed solver; result: UNSAT.
[1.868] 
[1.868] *************************************
[1.868] * * *   M a k e s p a n     5   * * *
[1.868] *************************************
[1.977] Computed next depth properties: array size of 529.
[2.513] Instantiated 1,050,992 transitional clauses.
[2.778] Instantiated 1,554,752 universal clauses.
[2.778] Instantiated and added clauses for a total of 7,113,565 clauses.
[2.778] The encoding contains a total of 542,148 distinct variables.
[2.778] Attempting solve with solver <glucose4> ...
c 529 assumptions
c last restart ## conflicts  :  12 4591 
[2.973] Executed solver; result: SAT.
[2.973] Solver returned SAT; a solution has been found at makespan 5.
67
solution 1445 1
167 1317 268 895 262 554 247 136 82 22 130 1290 447 177 234 235 112 37 99 31 144 1445 406 172 121 39 145 1202 436 183 97 29 155 1117 433 1032 140 429 779 427 524 418 183 78 18 155 407 409 185 395 397 45 3 174 665 334 327 329 164 68 14 94 27 110 36 51 9 
[2.977] Exiting.

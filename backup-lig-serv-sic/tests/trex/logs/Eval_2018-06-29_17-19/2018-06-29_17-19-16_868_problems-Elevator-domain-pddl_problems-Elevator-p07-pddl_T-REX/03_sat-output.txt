Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.000] Processed problem encoding.
[0.000] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 44 initial clauses.
[0.001] The encoding contains a total of 30 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 5.
[0.001] Instantiated 26 transitional clauses.
[0.001] Instantiated 110 universal clauses.
[0.001] Instantiated and added clauses for a total of 180 clauses.
[0.001] The encoding contains a total of 82 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 5 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     2   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 9.
[0.001] Instantiated 48 transitional clauses.
[0.001] Instantiated 240 universal clauses.
[0.001] Instantiated and added clauses for a total of 468 clauses.
[0.001] The encoding contains a total of 150 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 9 assumptions
c last restart ## conflicts  :  0 17 
[0.001] Executed solver; result: SAT.
[0.001] Solver returned SAT; a solution has been found at makespan 2.
7
solution 12 1
2 12 10 8 3 6 11 
[0.001] Exiting.

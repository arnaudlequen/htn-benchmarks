Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.024] Processed problem encoding.
[0.027] Calculated possible fact changes of composite elements.
[0.028] Initialized instantiation procedure.
[0.028] 
[0.028] *************************************
[0.028] * * *   M a k e s p a n     0   * * *
[0.028] *************************************
[0.029] Instantiated 1,973 initial clauses.
[0.029] The encoding contains a total of 1,332 distinct variables.
[0.029] Attempting solve with solver <glucose4> ...
c 5 assumptions
[0.029] Executed solver; result: UNSAT.
[0.029] 
[0.029] *************************************
[0.029] * * *   M a k e s p a n     1   * * *
[0.029] *************************************
[0.030] Computed next depth properties: array size of 17.
[0.032] Instantiated 3,111 transitional clauses.
[0.035] Instantiated 9,806 universal clauses.
[0.035] Instantiated and added clauses for a total of 14,890 clauses.
[0.035] The encoding contains a total of 3,608 distinct variables.
[0.035] Attempting solve with solver <glucose4> ...
c 17 assumptions
[0.035] Executed solver; result: UNSAT.
[0.035] 
[0.035] *************************************
[0.035] * * *   M a k e s p a n     2   * * *
[0.035] *************************************
[0.037] Computed next depth properties: array size of 49.
[0.043] Instantiated 8,326 transitional clauses.
[0.051] Instantiated 27,994 universal clauses.
[0.051] Instantiated and added clauses for a total of 51,210 clauses.
[0.051] The encoding contains a total of 8,260 distinct variables.
[0.051] Attempting solve with solver <glucose4> ...
c 49 assumptions
[0.053] Executed solver; result: UNSAT.
[0.053] 
[0.053] *************************************
[0.053] * * *   M a k e s p a n     3   * * *
[0.053] *************************************
[0.057] Computed next depth properties: array size of 97.
[0.069] Instantiated 20,237 transitional clauses.
[0.081] Instantiated 51,328 universal clauses.
[0.081] Instantiated and added clauses for a total of 122,775 clauses.
[0.081] The encoding contains a total of 15,854 distinct variables.
[0.081] Attempting solve with solver <glucose4> ...
c 97 assumptions
c last restart ## conflicts  :  7 229 
[0.085] Executed solver; result: SAT.
[0.085] Solver returned SAT; a solution has been found at makespan 3.
29
solution 279 1
44 279 184 80 93 94 26 10 34 180 181 83 155 57 157 24 9 67 135 137 59 29 11 61 108 109 44 17 8 
[0.085] Exiting.

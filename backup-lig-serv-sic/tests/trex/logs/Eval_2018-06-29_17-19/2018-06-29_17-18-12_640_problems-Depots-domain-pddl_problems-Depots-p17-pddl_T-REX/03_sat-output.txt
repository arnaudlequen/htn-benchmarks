Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.251] Processed problem encoding.
[0.375] Calculated possible fact changes of composite elements.
[0.378] Initialized instantiation procedure.
[0.378] 
[0.378] *************************************
[0.378] * * *   M a k e s p a n     0   * * *
[0.378] *************************************
[0.380] Instantiated 6,550 initial clauses.
[0.380] The encoding contains a total of 4,344 distinct variables.
[0.380] Attempting solve with solver <glucose4> ...
c 8 assumptions
[0.380] Executed solver; result: UNSAT.
[0.380] 
[0.380] *************************************
[0.380] * * *   M a k e s p a n     1   * * *
[0.380] *************************************
[0.382] Computed next depth properties: array size of 29.
[0.391] Instantiated 13,648 transitional clauses.
[0.400] Instantiated 45,128 universal clauses.
[0.400] Instantiated and added clauses for a total of 65,326 clauses.
[0.400] The encoding contains a total of 14,967 distinct variables.
[0.400] Attempting solve with solver <glucose4> ...
c 29 assumptions
[0.400] Executed solver; result: UNSAT.
[0.400] 
[0.400] *************************************
[0.400] * * *   M a k e s p a n     2   * * *
[0.400] *************************************
[0.413] Computed next depth properties: array size of 85.
[0.487] Instantiated 68,183 transitional clauses.
[0.514] Instantiated 189,868 universal clauses.
[0.514] Instantiated and added clauses for a total of 323,377 clauses.
[0.514] The encoding contains a total of 48,952 distinct variables.
[0.514] Attempting solve with solver <glucose4> ...
c 85 assumptions
[0.515] Executed solver; result: UNSAT.
[0.515] 
[0.515] *************************************
[0.515] * * *   M a k e s p a n     3   * * *
[0.515] *************************************
[0.553] Computed next depth properties: array size of 169.
[0.881] Instantiated 269,305 transitional clauses.
[0.943] Instantiated 440,050 universal clauses.
[0.943] Instantiated and added clauses for a total of 1,032,732 clauses.
[0.943] The encoding contains a total of 115,627 distinct variables.
[0.943] Attempting solve with solver <glucose4> ...
c 169 assumptions
c last restart ## conflicts  :  13 652 
[0.978] Executed solver; result: SAT.
[0.978] Solver returned SAT; a solution has been found at makespan 3.
34
solution 1168 1
411 412 155 78 17 145 1168 574 652 517 180 91 18 584 153 454 42 5 155 609 176 210 127 27 70 9 145 1095 569 179 98 22 592 3 
[0.979] Exiting.

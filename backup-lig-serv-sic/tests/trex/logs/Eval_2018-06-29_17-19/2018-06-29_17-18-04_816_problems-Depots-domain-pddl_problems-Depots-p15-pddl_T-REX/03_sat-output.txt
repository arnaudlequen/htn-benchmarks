Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.002] Parsed head comment information.
[0.180] Processed problem encoding.
[0.264] Calculated possible fact changes of composite elements.
[0.282] Initialized instantiation procedure.
[0.282] 
[0.282] *************************************
[0.282] * * *   M a k e s p a n     0   * * *
[0.282] *************************************
[0.289] Instantiated 22,886 initial clauses.
[0.289] The encoding contains a total of 15,540 distinct variables.
[0.289] Attempting solve with solver <glucose4> ...
c 16 assumptions
[0.290] Executed solver; result: UNSAT.
[0.290] 
[0.290] *************************************
[0.290] * * *   M a k e s p a n     1   * * *
[0.290] *************************************
[0.297] Computed next depth properties: array size of 61.
[0.318] Instantiated 41,054 transitional clauses.
[0.341] Instantiated 109,114 universal clauses.
[0.341] Instantiated and added clauses for a total of 173,054 clauses.
[0.341] The encoding contains a total of 42,366 distinct variables.
[0.341] Attempting solve with solver <glucose4> ...
c 61 assumptions
[0.342] Executed solver; result: UNSAT.
[0.342] 
[0.342] *************************************
[0.342] * * *   M a k e s p a n     2   * * *
[0.342] *************************************
[0.372] Computed next depth properties: array size of 181.
[0.447] Instantiated 121,340 transitional clauses.
[0.528] Instantiated 427,380 universal clauses.
[0.528] Instantiated and added clauses for a total of 721,774 clauses.
[0.528] The encoding contains a total of 122,383 distinct variables.
[0.528] Attempting solve with solver <glucose4> ...
c 181 assumptions
[0.528] Executed solver; result: UNSAT.
[0.528] 
[0.528] *************************************
[0.528] * * *   M a k e s p a n     3   * * *
[0.528] *************************************
[0.606] Computed next depth properties: array size of 361.
[0.953] Instantiated 648,785 transitional clauses.
[1.164] Instantiated 1,186,060 universal clauses.
[1.164] Instantiated and added clauses for a total of 2,556,619 clauses.
[1.164] The encoding contains a total of 288,199 distinct variables.
[1.164] Attempting solve with solver <glucose4> ...
c 361 assumptions
[1.164] Executed solver; result: UNSAT.
[1.164] 
[1.164] *************************************
[1.164] * * *   M a k e s p a n     4   * * *
[1.164] *************************************
[1.304] Computed next depth properties: array size of 541.
[1.981] Instantiated 1,281,800 transitional clauses.
[2.304] Instantiated 1,852,752 universal clauses.
[2.304] Instantiated and added clauses for a total of 5,691,171 clauses.
[2.304] The encoding contains a total of 489,443 distinct variables.
[2.304] Attempting solve with solver <glucose4> ...
c 541 assumptions
c last restart ## conflicts  :  17 4030 
[2.504] Executed solver; result: SAT.
[2.504] Solver returned SAT; a solution has been found at makespan 4.
68
solution 1450 1
210 835 195 343 741 314 515 311 178 78 15 158 1067 281 276 278 80 16 171 1450 431 692 425 396 398 98 25 199 487 272 171 91 21 201 507 377 191 111 31 202 1043 473 212 135 43 123 37 75 13 202 617 455 438 440 94 23 148 50 209 96 24 372 26 170 62 6 149 51 
[2.509] Exiting.

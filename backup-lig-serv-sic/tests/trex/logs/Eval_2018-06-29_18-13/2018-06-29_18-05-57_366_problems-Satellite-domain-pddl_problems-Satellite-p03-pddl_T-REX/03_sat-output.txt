Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.002] Processed problem encoding.
[0.002] Calculated possible fact changes of composite elements.
[0.002] Initialized instantiation procedure.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     0   * * *
[0.002] *************************************
[0.002] Instantiated 318 initial clauses.
[0.002] The encoding contains a total of 185 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 6 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     1   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 10.
[0.002] Instantiated 52 transitional clauses.
[0.002] Instantiated 656 universal clauses.
[0.002] Instantiated and added clauses for a total of 1,026 clauses.
[0.002] The encoding contains a total of 389 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 10 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     2   * * *
[0.002] *************************************
[0.003] Computed next depth properties: array size of 14.
[0.003] Instantiated 378 transitional clauses.
[0.003] Instantiated 1,632 universal clauses.
[0.003] Instantiated and added clauses for a total of 3,036 clauses.
[0.003] The encoding contains a total of 745 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.003] Executed solver; result: UNSAT.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     3   * * *
[0.003] *************************************
[0.004] Computed next depth properties: array size of 22.
[0.004] Instantiated 829 transitional clauses.
[0.005] Instantiated 1,331 universal clauses.
[0.005] Instantiated and added clauses for a total of 5,196 clauses.
[0.005] The encoding contains a total of 1,007 distinct variables.
[0.005] Attempting solve with solver <glucose4> ...
c 22 assumptions
[0.005] Executed solver; result: UNSAT.
[0.005] 
[0.005] *************************************
[0.005] * * *   M a k e s p a n     4   * * *
[0.005] *************************************
[0.005] Computed next depth properties: array size of 30.
[0.005] Instantiated 85 transitional clauses.
[0.006] Instantiated 1,440 universal clauses.
[0.006] Instantiated and added clauses for a total of 6,721 clauses.
[0.006] The encoding contains a total of 1,280 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 30 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     5   * * *
[0.006] *************************************
[0.006] Computed next depth properties: array size of 38.
[0.007] Instantiated 646 transitional clauses.
[0.008] Instantiated 3,322 universal clauses.
[0.008] Instantiated and added clauses for a total of 10,689 clauses.
[0.008] The encoding contains a total of 1,919 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 38 assumptions
c last restart ## conflicts  :  2 133 
[0.008] Executed solver; result: SAT.
[0.008] Solver returned SAT; a solution has been found at makespan 5.
18
solution 79 1
12 31 16 37 3 13 17 79 6 20 11 23 15 49 7 61 9 55 
[0.008] Exiting.

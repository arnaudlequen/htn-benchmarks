Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.006] Parsed head comment information.
[0.299] Processed problem encoding.
[0.363] Calculated possible fact changes of composite elements.
[0.377] Initialized instantiation procedure.
[0.377] 
[0.377] *************************************
[0.377] * * *   M a k e s p a n     0   * * *
[0.377] *************************************
[0.429] Instantiated 75,506 initial clauses.
[0.429] The encoding contains a total of 38,593 distinct variables.
[0.429] Attempting solve with solver <glucose4> ...
c 71 assumptions
[0.430] Executed solver; result: UNSAT.
[0.430] 
[0.430] *************************************
[0.430] * * *   M a k e s p a n     1   * * *
[0.430] *************************************
[0.498] Computed next depth properties: array size of 137.
[0.541] Instantiated 3,656 transitional clauses.
[0.686] Instantiated 168,306 universal clauses.
[0.686] Instantiated and added clauses for a total of 247,468 clauses.
[0.686] The encoding contains a total of 78,697 distinct variables.
[0.686] Attempting solve with solver <glucose4> ...
c 137 assumptions
[0.686] Executed solver; result: UNSAT.
[0.686] 
[0.686] *************************************
[0.686] * * *   M a k e s p a n     2   * * *
[0.686] *************************************
[0.830] Computed next depth properties: array size of 203.
[1.016] Instantiated 275,394 transitional clauses.
[1.345] Instantiated 1,141,666 universal clauses.
[1.345] Instantiated and added clauses for a total of 1,664,528 clauses.
[1.345] The encoding contains a total of 277,633 distinct variables.
[1.345] Attempting solve with solver <glucose4> ...
c 203 assumptions
[1.345] Executed solver; result: UNSAT.
[1.345] 
[1.345] *************************************
[1.345] * * *   M a k e s p a n     3   * * *
[1.345] *************************************
[1.543] Computed next depth properties: array size of 335.
[1.821] Instantiated 662,004 transitional clauses.
[2.149] Instantiated 347,944 universal clauses.
[2.149] Instantiated and added clauses for a total of 2,674,476 clauses.
[2.149] The encoding contains a total of 322,243 distinct variables.
[2.149] Attempting solve with solver <glucose4> ...
c 335 assumptions
[2.149] Executed solver; result: UNSAT.
[2.149] 
[2.149] *************************************
[2.149] * * *   M a k e s p a n     4   * * *
[2.149] *************************************
[2.419] Computed next depth properties: array size of 467.
[2.599] Instantiated 6,978 transitional clauses.
[3.042] Instantiated 460,672 universal clauses.
[3.042] Instantiated and added clauses for a total of 3,142,126 clauses.
[3.042] The encoding contains a total of 394,771 distinct variables.
[3.042] Attempting solve with solver <glucose4> ...
c 467 assumptions
[3.043] Executed solver; result: UNSAT.
[3.043] 
[3.043] *************************************
[3.043] * * *   M a k e s p a n     5   * * *
[3.043] *************************************
[3.466] Computed next depth properties: array size of 599.
[3.901] Instantiated 554,046 transitional clauses.
[4.692] Instantiated 2,756,542 universal clauses.
[4.692] Instantiated and added clauses for a total of 6,452,714 clauses.
[4.692] The encoding contains a total of 800,845 distinct variables.
[4.692] Attempting solve with solver <glucose4> ...
c 599 assumptions
c last restart ## conflicts  :  70 3457 
[6.313] Executed solver; result: SAT.
[6.313] Solver returned SAT; a solution has been found at makespan 5.
285
solution 34566 1
776 15438 792 15505 8 771 835 787 1031 15 802 770 890 786 1104 26 801 771 819 787 1175 39 1253 51 773 5720 789 6215 64 802 770 893 786 1392 73 801 771 823 787 1463 85 804 772 5718 788 6431 97 802 770 896 786 1608 107 801 771 826 787 1679 120 6658 132 803 773 5725 789 6719 144 1909 154 16514 168 16602 181 802 770 904 786 2112 187 804 772 5726 788 7079 200 2276 210 801 771 837 787 2327 223 802 770 910 786 2400 234 782 29998 799 31632 255 775 10623 791 12264 260 813 783 30072 800 31777 278 814 781 30074 798 31849 288 806 774 10633 790 12480 294 812 783 30075 800 31993 313 784 20278 794 22271 319 7893 327 22448 344 17683 353 22594 367 807 777 15466 793 17809 378 32527 395 17989 402 22884 415 801 771 839 787 3551 421 32822 442 8625 445 23176 462 808 776 15470 792 18385 471 805 775 10636 791 13632 481 4025 490 802 770 934 786 4056 501 4176 513 23612 532 801 771 864 787 4271 538 33546 560 33628 571 814 782 30028 799 33648 582 802 770 938 786 4560 584 801 771 870 787 4631 597 14470 613 4832 621 778 25083 795 29111 638 9859 645 5050 656 803 773 5771 789 9959 669 802 770 948 786 5136 678 801 771 878 787 5207 690 29603 709 809 779 25145 796 29615 720 810 778 25146 795 29687 730 809 779 25147 796 29759 744 29901 756 804 772 5773 788 10535 761 2751 5854 29757 34566 
[6.355] Exiting.


Compact plan:
0: (switch_on instrument0 satellite0)
1: (turn_to satellite0 star0 planet4)
2: (calibrate satellite0 instrument0 star0)
3: (turn_to satellite0 planet3 star0)
4: (take_image satellite0 planet3 instrument0 infrared0)
5: (switch_off instrument0 satellite0)
6: (switch_on instrument1 satellite0)
7: (turn_to satellite0 groundstation2 planet3)
8: (calibrate satellite0 instrument1 groundstation2)
9: (turn_to satellite0 planet4 groundstation2)
10: (take_image satellite0 planet4 instrument1 infrared0)
11: (turn_to satellite0 phenomenon5 planet4)
12: (take_image satellite0 phenomenon5 instrument1 image2)
13: (turn_to satellite0 phenomenon6 phenomenon5)
14: (take_image satellite0 phenomenon6 instrument1 infrared0)
15: (turn_to satellite0 star7 phenomenon6)
16: (take_image satellite0 star7 instrument1 infrared0)



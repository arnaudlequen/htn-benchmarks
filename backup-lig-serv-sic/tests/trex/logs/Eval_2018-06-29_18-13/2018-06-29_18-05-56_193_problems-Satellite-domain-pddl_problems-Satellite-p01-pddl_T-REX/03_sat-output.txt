Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.001] Processed problem encoding.
[0.001] Calculated possible fact changes of composite elements.
[0.001] Initialized instantiation procedure.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     0   * * *
[0.001] *************************************
[0.001] Instantiated 94 initial clauses.
[0.001] The encoding contains a total of 58 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     1   * * *
[0.001] *************************************
[0.001] Computed next depth properties: array size of 7.
[0.001] Instantiated 12 transitional clauses.
[0.001] Instantiated 163 universal clauses.
[0.001] Instantiated and added clauses for a total of 269 clauses.
[0.001] The encoding contains a total of 137 distinct variables.
[0.001] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.001] Executed solver; result: UNSAT.
[0.001] 
[0.001] *************************************
[0.001] * * *   M a k e s p a n     2   * * *
[0.001] *************************************
[0.002] Computed next depth properties: array size of 10.
[0.002] Instantiated 93 transitional clauses.
[0.002] Instantiated 430 universal clauses.
[0.002] Instantiated and added clauses for a total of 792 clauses.
[0.002] The encoding contains a total of 238 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 10 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     3   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 13.
[0.002] Instantiated 78 transitional clauses.
[0.002] Instantiated 280 universal clauses.
[0.002] Instantiated and added clauses for a total of 1,150 clauses.
[0.002] The encoding contains a total of 288 distinct variables.
[0.002] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.002] Executed solver; result: UNSAT.
[0.002] 
[0.002] *************************************
[0.002] * * *   M a k e s p a n     4   * * *
[0.002] *************************************
[0.002] Computed next depth properties: array size of 16.
[0.002] Instantiated 21 transitional clauses.
[0.003] Instantiated 316 universal clauses.
[0.003] Instantiated and added clauses for a total of 1,487 clauses.
[0.003] The encoding contains a total of 359 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 16 assumptions
[0.003] Executed solver; result: UNSAT.
[0.003] 
[0.003] *************************************
[0.003] * * *   M a k e s p a n     5   * * *
[0.003] *************************************
[0.003] Computed next depth properties: array size of 19.
[0.003] Instantiated 96 transitional clauses.
[0.003] Instantiated 586 universal clauses.
[0.003] Instantiated and added clauses for a total of 2,169 clauses.
[0.003] The encoding contains a total of 472 distinct variables.
[0.003] Attempting solve with solver <glucose4> ...
c 19 assumptions
c last restart ## conflicts  :  0 68 
[0.003] Executed solver; result: SAT.
[0.003] Solver returned SAT; a solution has been found at makespan 5.
9
solution 31 1
5 13 7 16 2 24 3 31 4 
[0.003] Exiting.

Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.017] Processed problem encoding.
[0.018] Calculated possible fact changes of composite elements.
[0.019] Initialized instantiation procedure.
[0.019] 
[0.019] *************************************
[0.019] * * *   M a k e s p a n     0   * * *
[0.019] *************************************
[0.020] Instantiated 2,330 initial clauses.
[0.020] The encoding contains a total of 1,264 distinct variables.
[0.020] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.020] Executed solver; result: UNSAT.
[0.020] 
[0.020] *************************************
[0.020] * * *   M a k e s p a n     1   * * *
[0.020] *************************************
[0.022] Computed next depth properties: array size of 24.
[0.022] Instantiated 242 transitional clauses.
[0.026] Instantiated 5,023 universal clauses.
[0.026] Instantiated and added clauses for a total of 7,595 clauses.
[0.026] The encoding contains a total of 2,572 distinct variables.
[0.026] Attempting solve with solver <glucose4> ...
c 24 assumptions
[0.026] Executed solver; result: UNSAT.
[0.026] 
[0.026] *************************************
[0.026] * * *   M a k e s p a n     2   * * *
[0.026] *************************************
[0.029] Computed next depth properties: array size of 35.
[0.033] Instantiated 4,605 transitional clauses.
[0.040] Instantiated 19,720 universal clauses.
[0.040] Instantiated and added clauses for a total of 31,920 clauses.
[0.040] The encoding contains a total of 6,373 distinct variables.
[0.040] Attempting solve with solver <glucose4> ...
c 35 assumptions
[0.040] Executed solver; result: UNSAT.
[0.040] 
[0.040] *************************************
[0.040] * * *   M a k e s p a n     3   * * *
[0.040] *************************************
[0.043] Computed next depth properties: array size of 57.
[0.049] Instantiated 10,706 transitional clauses.
[0.055] Instantiated 11,146 universal clauses.
[0.055] Instantiated and added clauses for a total of 53,772 clauses.
[0.055] The encoding contains a total of 8,078 distinct variables.
[0.055] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.055] Executed solver; result: UNSAT.
[0.055] 
[0.055] *************************************
[0.055] * * *   M a k e s p a n     4   * * *
[0.055] *************************************
[0.059] Computed next depth properties: array size of 79.
[0.061] Instantiated 534 transitional clauses.
[0.068] Instantiated 13,516 universal clauses.
[0.068] Instantiated and added clauses for a total of 67,822 clauses.
[0.068] The encoding contains a total of 10,452 distinct variables.
[0.068] Attempting solve with solver <glucose4> ...
c 79 assumptions
[0.068] Executed solver; result: UNSAT.
[0.068] 
[0.068] *************************************
[0.068] * * *   M a k e s p a n     5   * * *
[0.068] *************************************
[0.074] Computed next depth properties: array size of 101.
[0.081] Instantiated 9,520 transitional clauses.
[0.095] Instantiated 47,516 universal clauses.
[0.095] Instantiated and added clauses for a total of 124,858 clauses.
[0.095] The encoding contains a total of 18,394 distinct variables.
[0.095] Attempting solve with solver <glucose4> ...
c 101 assumptions
c last restart ## conflicts  :  33 381 
[0.101] Executed solver; result: SAT.
[0.101] Solver returned SAT; a solution has been found at makespan 5.
55
solution 908 1
58 344 70 379 3 81 59 364 71 396 8 82 58 349 70 411 13 431 18 81 59 367 71 444 26 61 569 73 666 31 62 753 74 890 36 85 64 787 76 908 42 82 58 352 70 507 44 534 48 56 249 68 329 51 868 
[0.102] Exiting.

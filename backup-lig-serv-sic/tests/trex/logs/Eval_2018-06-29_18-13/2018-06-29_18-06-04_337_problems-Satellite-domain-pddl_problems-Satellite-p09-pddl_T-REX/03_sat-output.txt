Command : ./interpreter_t-rex_binary_haribo -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.088] Processed problem encoding.
[0.101] Calculated possible fact changes of composite elements.
[0.103] Initialized instantiation procedure.
[0.103] 
[0.103] *************************************
[0.103] * * *   M a k e s p a n     0   * * *
[0.103] *************************************
[0.108] Instantiated 10,476 initial clauses.
[0.108] The encoding contains a total of 5,466 distinct variables.
[0.108] Attempting solve with solver <glucose4> ...
c 42 assumptions
[0.108] Executed solver; result: UNSAT.
[0.108] 
[0.108] *************************************
[0.108] * * *   M a k e s p a n     1   * * *
[0.108] *************************************
[0.118] Computed next depth properties: array size of 82.
[0.125] Instantiated 842 transitional clauses.
[0.138] Instantiated 23,311 universal clauses.
[0.138] Instantiated and added clauses for a total of 34,629 clauses.
[0.138] The encoding contains a total of 11,658 distinct variables.
[0.138] Attempting solve with solver <glucose4> ...
c 82 assumptions
[0.138] Executed solver; result: UNSAT.
[0.138] 
[0.138] *************************************
[0.138] * * *   M a k e s p a n     2   * * *
[0.138] *************************************
[0.159] Computed next depth properties: array size of 122.
[0.183] Instantiated 46,312 transitional clauses.
[0.212] Instantiated 148,374 universal clauses.
[0.212] Instantiated and added clauses for a total of 229,315 clauses.
[0.212] The encoding contains a total of 52,981 distinct variables.
[0.212] Attempting solve with solver <glucose4> ...
c 122 assumptions
[0.212] Executed solver; result: UNSAT.
[0.212] 
[0.212] *************************************
[0.212] * * *   M a k e s p a n     3   * * *
[0.212] *************************************
[0.233] Computed next depth properties: array size of 202.
[0.286] Instantiated 187,538 transitional clauses.
[0.308] Instantiated 63,124 universal clauses.
[0.308] Instantiated and added clauses for a total of 479,977 clauses.
[0.308] The encoding contains a total of 61,835 distinct variables.
[0.308] Attempting solve with solver <glucose4> ...
c 202 assumptions
[0.308] Executed solver; result: UNSAT.
[0.308] 
[0.308] *************************************
[0.308] * * *   M a k e s p a n     4   * * *
[0.308] *************************************
[0.332] Computed next depth properties: array size of 282.
[0.354] Instantiated 2,022 transitional clauses.
[0.385] Instantiated 67,212 universal clauses.
[0.385] Instantiated and added clauses for a total of 549,211 clauses.
[0.385] The encoding contains a total of 73,358 distinct variables.
[0.385] Attempting solve with solver <glucose4> ...
c 282 assumptions
[0.385] Executed solver; result: UNSAT.
[0.385] 
[0.385] *************************************
[0.385] * * *   M a k e s p a n     5   * * *
[0.385] *************************************
[0.433] Computed next depth properties: array size of 362.
[0.492] Instantiated 95,488 transitional clauses.
[0.565] Instantiated 363,072 universal clauses.
[0.565] Instantiated and added clauses for a total of 1,007,771 clauses.
[0.565] The encoding contains a total of 158,985 distinct variables.
[0.565] Attempting solve with solver <glucose4> ...
c 362 assumptions
c last restart ## conflicts  :  99 1854 
[0.645] Executed solver; result: SAT.
[0.645] Solver returned SAT; a solution has been found at makespan 5.
210
solution 2375 1
211 928 240 937 6 207 313 236 407 18 269 213 914 242 937 13 227 2173 256 2210 30 265 206 386 235 434 20 219 1711 248 1754 25 271 209 890 238 984 32 218 1427 247 1465 35 42 484 45 267 211 916 240 1009 47 264 207 317 236 479 53 269 209 893 238 1032 59 265 206 389 235 530 60 264 207 319 236 551 69 267 210 918 239 1081 76 285 225 2115 254 2280 68 265 205 368 234 577 80 283 221 2120 250 2328 90 262 206 393 235 602 84 276 215 1396 244 1512 94 264 207 322 236 623 98 279 222 2098 251 2375 108 268 213 920 242 1201 112 1924 120 273 217 1450 246 1538 134 277 220 1693 249 1943 125 708 136 275 215 1407 244 1560 143 271 210 925 239 1249 149 760 152 278 219 1719 248 1994 168 265 202 353 231 768 154 259 208 354 237 768 161 266 202 354 231 792 172 259 206 403 235 794 170 264 202 355 231 816 186 1649 183 259 206 404 235 842 192 277 220 1698 249 2063 197 
[0.647] Exiting.

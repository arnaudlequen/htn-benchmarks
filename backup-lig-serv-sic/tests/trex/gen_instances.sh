for d in $@; do
  > full_eval_instances_randomized.csv
  for i in {1..20}; do
    printf -v i "%02d" $i
    echo "$d $i T-REX -i {-P,-b8192}" >> full_eval_instances_randomized.csv
  done
  sh run_all.sh
done

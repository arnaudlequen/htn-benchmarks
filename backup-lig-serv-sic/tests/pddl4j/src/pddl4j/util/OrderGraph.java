/*
 * Copyright (c) 2010 by Abdeldjalil Ramoul <abdeldjalil.ramoul@imag.fr>.
 *
 * This file is part of PDDL4J library.
 *
 * PDDL4J is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PDDL4J is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PDDL4J.  If not, see <http://www.gnu.org/licenses/>
 */

package pddl4j.util;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Stack;

/**
 * This class implements an adjacency matrix to represent constraints and order graphs
 * 
 * @author Ramoul
 */
public class OrderGraph {
	
	/**
	 * The number of nodes in the order graph
	 */
	public int _size;
	
	/**
	 * The adjacency matrix
	 */
	private Hashtable<Integer, LinkedNodes> _graph;
	
	/**
	 * Creates a new orderGraph of size <code> size </code>
	 * 
	 * @param size The size of the Order graph
	 */
	public OrderGraph(final int size) {
		_size = size;
		_graph = new Hashtable<>(size);
	}
	
	/**
	 * Creates a new orderGraph from an other orderGraph
	 * 
	 * @param oldGraph the graph from which we need to create a new order graph
	 */
	public OrderGraph(final OrderGraph oldGraph) {
		_size = oldGraph._size;
		_graph = new Hashtable<>(oldGraph._graph);
	}
	
	/**
	 * Adds a new out link in the order graph
	 * 
	 * @param source the first node of the added link
	 * @param out the second node of the added link
	 * @return <code> true </code> if the link is added and <code> false </false> otherwise
	 */
	public boolean addPositiveLink(final Integer source, final int out) {
		final LinkedNodes linkedNode = _graph.get(source);
		if (linkedNode == null) {
			final LinkedNodes newLinkedNode = new LinkedNodes(out, -1);
			_graph.put(source, newLinkedNode);
		} else {
			linkedNode.addOutNode(out);
		}
		if (testCircuit(source)) {
			return true;
		} else {
			_graph.get(source).removeOutNode(out);
			removeEmptySource(source);
			return false;
		}
	}
	
	/**
	 * Adds a new in link in the order graph
	 * 
	 * @param source the first node of the added link
	 * @param in the second node of the added link
	 * @return <code> true </code> if the link is added and <code> false </false> otherwise
	 */
	public boolean addNegativeLink(final Integer source, final int in) {
		final LinkedNodes linkedNode = _graph.get(source);
		if (linkedNode == null) {
			final LinkedNodes newLinkedNode = new LinkedNodes(-1, in);
			_graph.put(source, newLinkedNode);
		} else {
			linkedNode.addInNode(in);
		}
		if (testCircuit(source)) {
			return true;
		} else {
			_graph.get(source).removeInNode(in);
			removeEmptySource(source);
			return false;
		}
	}
	
	/**
	 * Removes a link in the order graph
	 * 
	 * @param source the source node of the link
	 * @param target the target node of the link
	 */
	public void removeLink(final int source, final int target) {
		if (!(_graph.get(source) == null)) {
			_graph.get(source).removeOutNode(target);
		}
		removeEmptySource(source);
	}
	
	/**
	 * Tests if the new link creates a circuit in the graph
	 * 
	 * @param source The source of tested link
	 * @return <code> true </code> if the link creates a circuit and <code> false </false> otherwise
	 */
	public boolean testCircuit(final int source) {
		final Stack<Integer> outNodes = new Stack<>();
		if (!(_graph.get(source) == null)) {
			outNodes.addAll(_graph.get(source).get_outNodes());
		}
		int outNode = -1;
		while (!outNodes.isEmpty() && outNode != source) {
			outNode = outNodes.pop();
			if (!(_graph.get(outNode) == null)) {
				outNodes.addAll(_graph.get(outNode).get_outNodes());
			}
		}
		if (outNode == source) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Gets the indexes of direct predecessors of a specified node in a set of indexes
	 * 
	 * @param source the node for which we need predecessors
	 * @param in The task list in which we look for predecessors
	 * @return the list of predecessors tasks indexes
	 */
	public List<Integer> getPredecessorsIn(final int source, final List<Integer> in) {
		final ArrayList<Integer> predecessors = new ArrayList<>();
		if (!(_graph.get(source) == null)) {
			final ArrayList<Integer> inNodes = _graph.get(source).get_inNodes();
			for (final int i : in) {
				if (inNodes.contains(i)) {
					predecessors.add(i);
				}
			}
		}
		return predecessors;
	}
	
	/**
	 * Gets the indexes of direct successors of a specified node in a set of indexes
	 * 
	 * @param source the node for which we need successors
	 * @param in The task list in which we look for successors
	 * @return the list of successors tasks indexes
	 */
	public List<Integer> getSuccessorsIn(final int source, final List<Integer> in) {
		final ArrayList<Integer> successors = new ArrayList<>();
		if (!(_graph.get(source) == null)) {
			final ArrayList<Integer> outNodes = _graph.get(source).get_outNodes();
			for (final int i : in) {
				if (outNodes.contains(i)) {
					successors.add(i);
				}
			}
		}
		return successors;
	}
	
	/**
	 * Says if Some node in firstList has successor in secondList
	 * 
	 * @param firstList The first tasks list
	 * @param secondList The second tasks list
	 * @return <code> true </code> if there is at least a link between a node in firstList and secondList and <code> false </code> otherwise
	 */
	public boolean someSuccessorsIn(final List<Integer> firstList, final List<Integer> secondList) {
		for (final int firstTask : firstList) {
			if (!getSuccessorsIn(firstTask, secondList).isEmpty()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Gets the tasks of A0 which don't have predecessors in A0 union A1
	 * 
	 * @param A0 The first task list in an order constraint
	 * @param A1 The second task list in an order constraint
	 * @return The set Oi containing tasks without predecessors in A0 union A1
	 */
	public List<Integer> getOi(final List<Integer> A0, final List<Integer> A1) {
		final ArrayList<Integer> Oi = new ArrayList<>();
		final ArrayList<Integer> A = new ArrayList<>(A0);
		A.addAll(A1);
		for (final int task : A0) {
			if (getPredecessorsIn(task, A).isEmpty()) {
				Oi.add(task);
			}
		}
		return Oi;
	}
	
	/**
	 * Gets the tasks of A0 which don't have successors in A0 union A1
	 * 
	 * @param A0 The first task list in an order constraint
	 * @param A1 The second task list in an order constraint
	 * @return The set Li containing tasks without successors in A0 union A1
	 */
	public List<Integer> getLi(final List<Integer> A0, final List<Integer> A1) {
		final ArrayList<Integer> Li = new ArrayList<>();
		final ArrayList<Integer> A = new ArrayList<>(A0);
		A.addAll(A1);
		for (final int task : A0) {
			if (getSuccessorsIn(task, A).isEmpty()) {
				Li.add(task);
			}
		}
		return Li;
	}
	
	/**
	 * Gets the tasks of A0 which don't have predecessors in A1 and there exists at least one node in A1 that is not their predecessor.
	 * 
	 * @param A0 The first task list in an order constraint
	 * @param A1 The second task list in an order constraint
	 * @return The set Bi containing tasks as defined in UMCP
	 */
	public List<Integer> getBi(final List<Integer> A0, final List<Integer> A1) {
		final ArrayList<Integer> Bi = new ArrayList<>();
		for (final int task : A0) {
			if (getPredecessorsIn(task, A1).isEmpty() && getPredecessorsIn(task, A0).size() < A0.size() - 1) {
				Bi.add(task);
			}
		}
		return Bi;
	}
	
	/**
	 * Gets the tasks of A0 which don't have successors in A1 and there exists at least one node in A1 that is not their successor.
	 * 
	 * @param A0 The first task list in an order constraint
	 * @param A1 The second task list in an order constraint
	 * @return The set Ci containing tasks as defined in UMCP
	 */
	public List<Integer> getCi(final List<Integer> A0, final List<Integer> A1) {
		final ArrayList<Integer> Ci = new ArrayList<>();
		for (final int task : A0) {
			if (getSuccessorsIn(task, A1).isEmpty() && getSuccessorsIn(task, A0).size() < A0.size() - 1) {
				Ci.add(task);
			}
		}
		return Ci;
	}
	
	/**
	 * Tests if a node is ordered after an other node
	 * 
	 * @param secondNode The node tested to be after first node
	 * @param firstNode The node tested to be before second node
	 * @return <code> true </code> if secondNode is ordered after firstNode <code> false </code> otherwise
	 */
	public boolean isOrderedAfter(final int secondNode, final int firstNode) {
		final Stack<Integer> successorsStack = new Stack<>();
		if (!(_graph.get(firstNode) == null)) {
			successorsStack.addAll(_graph.get(firstNode).get_outNodes());
		}
		int successor = -1;
		while (!successorsStack.isEmpty() && successor != secondNode) {
			successor = successorsStack.pop();
			if (!(_graph.get(successor) == null)) {
				successorsStack.addAll(_graph.get(successor).get_outNodes());
			}
		}
		if (successor == secondNode) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * get all nodes ordered after a specified node
	 * 
	 * @param source The specified node
	 * @return The list of nodes ordered after the specified source node
	 */
	public List<Integer> allTasksOrderedAfter(final int source) {
		final List<Integer> orderedAfterTasks = new ArrayList<>();
		final Stack<Integer> successorsStack = new Stack<>();
		if (_graph.get(source) != null) {
			successorsStack.addAll(_graph.get(source).get_outNodes());
		}
		while (!successorsStack.isEmpty()) {
			final int successor = successorsStack.pop();
			if (!orderedAfterTasks.contains(successor)) {
				orderedAfterTasks.add(successor);
			}
			if (_graph.get(successor) != null) {
				successorsStack.addAll(_graph.get(successor).get_outNodes());
			}
		}
		return orderedAfterTasks;
	}
	
	/**
	 * get all nodes ordered after a specified node and not directly connected to it
	 * 
	 * @param source The specified node
	 * @return The list of nodes ordered after the specified source node get all nodes ordered after a specified node
	 */
	public List<Integer> indirectlyOrderedAfter(final int source) {
		final List<Integer> orderedAfterTasks = new ArrayList<>();
		final Stack<Integer> successorsStack = new Stack<>();
		if (_graph.get(source) != null) {
			successorsStack.addAll(_graph.get(source).get_outNodes());
		}
		while (!successorsStack.isEmpty()) {
			final int successor = successorsStack.pop();
			if (!orderedAfterTasks.contains(successor)) {
				orderedAfterTasks.add(successor);
			}
			if (_graph.get(successor) != null) {
				successorsStack.addAll(_graph.get(successor).get_outNodes());
			}
		}
		return orderedAfterTasks;
	}
	
	/**
	 * get all nodes ordered before a specified node
	 * 
	 * @param source The specified node
	 * @return The list of nodes ordered before the specified source node
	 */
	public List<Integer> allTasksOrderedBefore(final int source) {
		final List<Integer> orderedBeforeTasks = new ArrayList<>();
		final Stack<Integer> predecessorsStack = new Stack<>();
		if (_graph.get(source) != null) {
			predecessorsStack.addAll(_graph.get(source).get_inNodes());
		}
		while (!predecessorsStack.isEmpty()) {
			final int predecessor = predecessorsStack.pop();
			if (!orderedBeforeTasks.contains(predecessor)) {
				orderedBeforeTasks.add(predecessor);
			}
			if (_graph.get(predecessor) != null) {
				predecessorsStack.addAll(_graph.get(predecessor).get_inNodes());
			}
		}
		return orderedBeforeTasks;
	}
	
	//	/**
	//	 * Clean duplicated outgoing order constrains for a specified node
	//	 * 
	//	 * @param source The specified node
	//	 * @param node The HitnaNode generating order graph
	//	 */
	//	public void cleanOrderCons(final int source, final HitnaNode node) {
	//		final List<Integer> orderedAfterTasks = new ArrayList<>();
	//		final Stack<Integer> successorsStack = new Stack<>();
	//		if (_graph.get(source) != null) {
	//			successorsStack.addAll(_graph.get(source).get_outNodes());
	//		}
	//		while (!successorsStack.isEmpty()) {
	//			final int successor = successorsStack.pop();
	//			if (orderedAfterTasks.contains(successor)) {
	//				_graph.get(source).removeOutNode(successor);
	//				_graph.get(successor).removeInNode(source);
	//			} else {
	//				orderedAfterTasks.add(successor);
	//			}
	//			if (_graph.get(successor) != null) {
	//				successorsStack.addAll(_graph.get(successor).get_outNodes());
	//			}
	//		}
	//	}
	//	
	//	/**
	//	 * Clean duplicated order constrains in the Order graph
	//	 * 
	//	 * @param node The HitnaNode generating order graph
	//	 */
	//	public void cleanOrderCons(final HitnaNode node) {
	//		
	//		for (final int key : _graph.keySet()) {
	//			cleanOrderCons(key, node);
	//		}
	//		final List<OrderConstraint> orderCons = node.getOrderingConstraints();
	//		final List<OrderConstraint> cloneOrderCons = new ArrayList<>(orderCons);
	//		for (final OrderConstraint orderCon : cloneOrderCons) {
	//			final int source = orderCon.getFirstTask().getUniqueTask();
	//			final int dest = orderCon.getSecondTask().getUniqueTask();
	//			if (!_graph.get(source).get_outNodes().contains(dest)) {
	//				orderCons.remove(orderCon);
	//			}
	//		}
	//	}
	
	/**
	 * Computes the list of shadowed tasks -> tasks ordered between a landmark task and another task in the task list
	 * 
	 * @param taskList The list of tasks
	 * @param task The landmark task
	 * @return The list of shadowed tasks
	 */
	public List<Integer> shadowedTasksList(final List<Integer> taskList, final int task) {
		
		final List<Integer> shadowedTasksList = new ArrayList<>();
		for (final int shadowerTask : taskList) {
			for (final int shadowedTask : taskList) {
				if (shadowerTask != shadowedTask && isOrderedBetween(shadowerTask, shadowedTask, task)) {
					shadowedTasksList.add(shadowedTask);
				}
			}
		}
		return shadowedTasksList;
	}
	
	/**
	 * Tests if a task is ordered before an other task
	 *
	 * @param firstTask The task tested to be before second task
	 * @param secondTask The task tested to be after first task
	 * @return <code> true </code> if first is ordered before secondTask task <code> false </code> otherwise
	 */
	public boolean isOrderedBefore(final int firstTask, final int secondTask) {
		
		if (secondTask != firstTask) {
			return isOrderedAfter(secondTask, firstTask);
		} else {
			return false;
		}
	}
	
	/**
	 * Tests if a task is ordered between two other tasks
	 * 
	 * @param task The task tested to be between the other tasks
	 * @param firstTask The tested to be the first task
	 * @param secondTask The task tested to be the second task
	 * @return <code> true </code> if task is ordered between firstTask and secondTask <code> false </code> otherwise
	 */
	public boolean isOrderedBetween(final int task, final int firstTask, final int secondTask) {
		if (isOrderedAfter(task, firstTask) && isOrderedAfter(secondTask, task)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Evaluates a FIRST/LAST task list and removes tasks that are not first/last
	 * 
	 * @param taskList The treated task list
	 * @return <code> true </code> if the task list is updated and <code> false </code> otherwise
	 */
	public boolean evaluateTaskList(final TaskList taskList) {
		boolean updated = false;
		final TaskList tempTaskList = new TaskList(taskList);
		if (tempTaskList.getLabel()) {
			for (final int task : tempTaskList.getTaskList()) {
				if (!getPredecessorsIn(task, tempTaskList.getTaskList()).isEmpty()) {
					taskList.getTaskList().remove((Integer) task);
					updated = true;
				}
			}
		} else {
			for (final int task : tempTaskList.getTaskList()) {
				if (!getSuccessorsIn(task, tempTaskList.getTaskList()).isEmpty()) {
					taskList.getTaskList().remove((Integer) task);
					updated = true;
				}
			}
		}
		return updated;
	}
	
	/**
	 * Removes source from the graph if it does not have any link
	 * 
	 * @param source The source of links
	 * @return <code> true </code> if source is removed and <code> false </code> otherwise
	 */
	public boolean removeEmptySource(final int source) {
		if (!(_graph.get(source) == null)) {
			if (_graph.get(source).get_inNodes().isEmpty() && _graph.get(source).get_outNodes().isEmpty()) {
				_graph.remove(source);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	/**
	 * @return the _size
	 */
	public int get_size() {
		return _size;
	}
	
	/**
	 * @param _size the _size to set
	 */
	public void set_size(final int _size) {
		this._size = _size;
	}
	
	/**
	 * @return the _graph
	 */
	public Hashtable<Integer, LinkedNodes> get_graph() {
		return _graph;
	}
	
	/**
	 * @param _graph the _graph to set
	 */
	public void set_graph(final Hashtable<Integer, LinkedNodes> _graph) {
		this._graph = _graph;
	}
	
	/**
	 * Represents the order graph in a string mode
	 * 
	 * @return A String representation of the adjacency matrix
	 */
	@Override
	public String toString() {
		final StringBuffer str = new StringBuffer();
		str.append("Order Graph: \n");
		final Enumeration<Integer> keys = _graph.keys();
		int key = -1;
		LinkedNodes links = new LinkedNodes();
		while (keys.hasMoreElements()) {
			key = keys.nextElement();
			links = _graph.get(key);
			str.append("Node: " + key + " | out: " + links.get_outNodes() + " | ins: " + links.get_inNodes() + "\n");
		}
		return str.toString();
	}
	
}

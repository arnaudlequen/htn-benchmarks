set terminal pdf 
set decimalsign "."
set output "preprocessing_time_comparison_rover.pdf"
set title "PreProcessing comparison on rover domain" font "Helvetica,7"
set xlabel "Problem" font "Helvetica,6"
set ylabel "Processing Time (s)" font "Helvetica,6"
set grid y
set xrange['0':'18']
#set logscale x
#set yrange[0:'1100']

plot "../data/results_rover_fast.data" using 0:6 with linespoints title "Preprocessing fast", \
    "../data/results_rover.data" using 0:6 with linespoints title "Preprocessing classic", \

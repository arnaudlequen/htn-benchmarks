set terminal pdf 
set decimalsign "."
set output "nb_instanciated_meths_without_simplification_logscale.pdf"
set xlabel "Problems" font "Times-Roman,8"
set ylabel "Nb instancited methods" font "Times-Roman,8"
set grid y
set xrange['0':'23']
set logscale y
set monochrome
#set yrange[0:'1100']

plot "~/Workspace/Bitbucket_Projetcs/pddl4j_IJAIT/IJAIT_results/data/IJAIT_rover.data" using 0:14 with linespoints title "without simplification" pt 8, \

#!/bin/bash

n="4"


for i in $(seq 1 1 $((n))); do
	for j in $(seq 1 1 $((n))); do
		echo "    pos$i$j - location"
	done;
done;

for i in $(seq 1 1 $((n))); do
	for j in $(seq 1 1 $((n-1))); do
		echo "    (movedir pos$i$j pos$i$((j+1)) down)";
	done;
done;

for i in $(seq 1 1 $((n))); do
	for j in $(seq 1 1 $((n-1))); do
		echo "    (movedir pos$i$((j+1)) pos$i$j up)";
	done;
done;

for i in $(seq 1 1 $((n-1))); do
	for j in $(seq 1 1 $((n))); do
		echo "    (movedir pos$i$j pos$((i+1))$j right)";
	done;
done;

for i in $(seq 1 1 $((n-1))); do
	for j in $(seq 1 1 $((n))); do
		echo "    (movedir pos$((i+1))$j pos$i$j left)";
	done;
done;

for i in $(seq 1 1 $((n))); do
	for j in $(seq 1 1 $((n))); do
		echo "    (is_nongoal pos$i$j)"
		echo "    (clear pos$i$j)"
	done;
done;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BlocksWorld , version HTN,
;;; Version 1.0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;-------------------------------------------------------------------
;								Actions
;-------------------------------------------------------------------
(define (domain BLOCKS)
  (:requirements :strips :typing :htn :negative-preconditions :equality)
  (:types block)
  (:predicates (on ?x - block ?y - block)
	       (ontable ?x - block)
	       (clear ?x - block)
	       (handempty)
	       (holding ?x - block)
	       )

  (:action pick-up
	     :parameters (?x - block)
	     :precondition (and (clear ?x) (ontable ?x) (handempty))
	     :effect
	     (and (not (ontable ?x))
		   (not (clear ?x))
		   (not (handempty))
		   (holding ?x)))

  (:action put-down
	     :parameters (?x - block)
	     :precondition (holding ?x)
	     :effect
	     (and (not (holding ?x))
		   (clear ?x)
		   (handempty)
		   (ontable ?x)))

  (:action stack
	     :parameters (?x - block ?y - block)
	     :precondition (and (holding ?x) (clear ?y))
	     :effect
	     (and (not (holding ?x))
		   (not (clear ?y))
		   (clear ?x)
		   (handempty)
		   (on ?x ?y)))

  (:action unstack
	     :parameters (?x - block ?y - block)
	     :precondition (and (on ?x ?y) (clear ?x) (handempty))
	     :effect
	     (and (holding ?x)
		   (clear ?y)
		   (not (clear ?x))
		   (not (handempty))
		   (not (on ?x ?y))))
  (:action nop
	     :parameters ()
	     :precondition (and )
	     :effect (and )
	)

;-------------------------------------------------------------------
;			               				Methods
;-------------------------------------------------------------------
;           Move a block below a column of blocks
;-------------------------------------------------------------------
	(:method insert_from_top
		:parameters (?obj ?bb ?current - block)
		:expansion 	(
						        (tag t1 (do_stack ?bb ?obj))
					      )
		:constraints
					(and (before (and
                  (= ?bb ?current)
							)
						t1)
					)
	)

	(:method insert_from_top
		:parameters (?obj ?bb ?current - block)
		:expansion 	(
						        (tag t1 (do_unstack ?current ?b))
                    (tag t2 (insert_from_top ?obj ?bb ?current))
                    (tag t3 (free ?b))
                    (tag t4 (do_stack ?current ?b))
					      )
		:constraints
					(and (before (and
                  (not (= ?bb ?current))
                  (on ?b ?current)
							)
						t1)
					)
	)


;-------------------------------------------------------------------
;                     Put ?obj below ?bb's column
;               Recursively get to the top, then call insert_from_top
;-------------------------------------------------------------------
(:method insert
  :parameters (?obj ?bb - block)
  :expansion 	(
                  (tag t1 (do_stack ?obj ?bb))
              )
  :constraints
        (and (before (and
                (clear ?bb)
            )
          t1)
        )
)

(:method insert
  :parameters (?obj ?bb - block)
  :expansion 	(
                  (tag t1 (insert ?obj ?bb ?bb))
              )
  :constraints
        (and (before (and
                (not (clear ?bb))
            )
          t1)
        )
)

(:method insert
  :parameters (?obj ?bb ?current - block)
  :expansion 	(
                  (tag t1 (insert_from_top ?obj ?bb ?current))
              )
  :constraints
        (and (before (and
               ;(forall (?b - block) (not (on ?current ?b)))
            )
          t1)
        )
)

(:method insert
  :parameters (?obj ?bb ?current - block)
  :expansion 	(
                  (tag t1 (insert ?obj ?bb ?b))
              )
  :constraints
        (and (before (and
               (on ?current ?b)
            )
          t1)
        )
)


;-------------------------------------------------------------------
;                     Puts every block above ?b on the table
;-------------------------------------------------------------------
(:method free
  :parameters (?b - block)
  :expansion 	(
                  (tag t1 (free ?a))
                  (tag t2 (do_unstack ?b ?a))
              )
  :constraints
        (and (before (and
                (not (clear ?b))
                (on ?a ?b)
            )
          t1)
        )
)

(:method free
  :parameters (?b - block)
  :expansion 	(
                  (tag t1 (nop))
              )
  :constraints
        (and (before (and
                (clear ?b)
            )
          t1)
        )
)


;-------------------------------------------------------------------
;                 Stack a block from the table
;-------------------------------------------------------------------
(:method do_stack
  :parameters (?b ?bb - block)
  :expansion 	(
                  (tag t1 (pick-up ?b))
                  (tag t2 (stack ?b ?bb))
              )
  :constraints
        (and (before (and
                (clear ?b)
            )
          t1)
        )
)


;-------------------------------------------------------------------
;                 Put a block on the table from a stack
;               Might have quite some side effects
;-------------------------------------------------------------------
(:method do_stack
  :parameters (?b ?bb - block)
  :expansion 	(
                  (tag t1 (unstack ))
                  (tag t2 (stack ?b ?bb))
              )
  :constraints
        (and (before (and
                (clear ?b)
                (on ?b ?a)
            )
          t1)
        )
)
)

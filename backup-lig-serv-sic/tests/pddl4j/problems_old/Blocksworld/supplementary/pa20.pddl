(define (problem BW-rand-20)
    (:domain BLOCKS)
    (:requirements :strips :typing :negative-preconditions :htn :equality)
    (:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11 b12 b13 b14 b15 b16 b17 b18 b19 b20  - block)
    (:init
        (handempty)
        
        (clear b9)
        (on b9 b5)
        (on b5 b12)
        (on b12 b7)
        (on b7 b16)
        (on b16 b20)
        (on b20 b17)
        (on b17 b1)
        (ontable b1)
        
        (clear b2)
        (ontable b2)
        
        (clear b3)
        (ontable b3)
        
        (clear b8)
        (ontable b8)
        
        (clear b14)
        (ontable b14)
        
        (clear b13)
        (on b13 b15)
        (on b15 b11)
        (ontable b11)
        
        (clear b18)
        (on b18 b4)
        (on b4 b19)
        (on b19 b10)
        (on b10 b6)
        (ontable b6)
    )
    (:goal
        :tasks  (
            (tag t14 (do_put_on b3 b10))
            (tag t1 (do_put_on b20 b3))
            (tag t6 (do_put_on b14 b20))
            
            (tag t9 (do_put_on b9 b2))
            (tag t7 (do_put_on b13 b9))
            
            (tag t13 (do_put_on b4 b11))
            (tag t2 (do_put_on b18 b4))
            (tag t10 (do_put_on b7 b18))
            
            (tag t3 (do_put_on b17 b19))
            (tag t8 (do_put_on b12 b17))
            (tag t4 (do_put_on b16 b12))
            (tag t12 (do_put_on b5 b16))
            (tag t5 (do_put_on b15 b5))
            (tag t15 (do_put_on b1 b15))
            (tag t11 (do_put_on b6 b1))
        )
        :constraints(and 
            (after
                (and
                    (on b6 b1)
                    (on b1 b15)
                    (on b15 b5)
                    (on b5 b16)
                    (on b16 b12)
                    (on b12 b17)
                    (on b17 b19)
                    
                    (on b7 b18)
                    (on b18 b4)
                    (on b4 b11)
                    
                    (on b13 b9)
                    (on b9 b2)
                    
                    (on b14 b20)
                    (on b20 b3)
                    (on b3 b10)
                )
            t11)
        )
    )
)



(define (problem BW-rand-8)
(:domain blocksworld)
(:requirements :strips :typing :negative-preconditions :htn :equality)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 - block)
(:init
(handempty)
(on b1 b8)
(on b2 b3)
(ontable b3)
(on b4 b1)
(on b5 b4)
(on b6 b7)
(on b7 b2)
(on b8 b6)
(clear b5)
)
(:goal :tasks (
(tag t1 (do_put_on b6 b5))
(tag t2 (do_put_on b3 b6))
(tag t3 (do_put_on b4 b3))
(tag t4 (do_put_on b1 b4))
(tag t5 (do_put_on b2 b1))
) :constraints(and (after 
(and
(on b1 b4)
(on b2 b1)
(on b3 b6)
(on b4 b3)
(on b6 b5)) t5)
)
)
)

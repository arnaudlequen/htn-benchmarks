Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.009] Processed problem encoding.
[0.009] Calculated possible fact changes of composite elements.
[0.009] Initialized instantiation procedure.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     0   * * *
[0.009] *************************************
[0.010] Instantiated 562 initial clauses.
[0.010] The encoding contains a total of 466 distinct variables.
[0.010] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.010] Executed solver; result: UNSAT.
[0.010] 
[0.010] *************************************
[0.010] * * *   M a k e s p a n     1   * * *
[0.010] *************************************
[0.010] Computed next depth properties: array size of 5.
[0.011] Instantiated 972 transitional clauses.
[0.016] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.016] Instantiated 34,020 universal clauses.
[0.016] Instantiated and added clauses for a total of 35,554 clauses.
[0.016] The encoding contains a total of 912 distinct variables.
[0.016] Attempting solve with solver <glucose4> ...
c 5 assumptions
[0.016] Executed solver; result: UNSAT.
[0.016] 
[0.016] *************************************
[0.016] * * *   M a k e s p a n     2   * * *
[0.016] *************************************
[0.016] Computed next depth properties: array size of 6.
[0.017] Instantiated 2,763 transitional clauses.
[0.023] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.023] Instantiated 92,053 universal clauses.
[0.023] Instantiated and added clauses for a total of 130,370 clauses.
[0.023] The encoding contains a total of 2,042 distinct variables.
[0.023] Attempting solve with solver <glucose4> ...
c 6 assumptions
[0.023] Executed solver; result: UNSAT.
[0.023] 
[0.023] *************************************
[0.023] * * *   M a k e s p a n     3   * * *
[0.023] *************************************
[0.023] Computed next depth properties: array size of 16.
[0.025] Instantiated 5,694 transitional clauses.
[0.031] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.031] Instantiated 147,934 universal clauses.
[0.031] Instantiated and added clauses for a total of 283,998 clauses.
[0.031] The encoding contains a total of 3,539 distinct variables.
[0.031] Attempting solve with solver <glucose4> ...
c 16 assumptions
[0.031] Executed solver; result: UNSAT.
[0.031] 
[0.031] *************************************
[0.031] * * *   M a k e s p a n     4   * * *
[0.031] *************************************
[0.032] Computed next depth properties: array size of 33.
[0.034] Instantiated 5,814 transitional clauses.
[0.040] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.040] Instantiated 154,533 universal clauses.
[0.040] Instantiated and added clauses for a total of 444,345 clauses.
[0.040] The encoding contains a total of 5,186 distinct variables.
[0.040] Attempting solve with solver <glucose4> ...
c 33 assumptions
c last restart ## conflicts  :  9 83 
[0.040] Executed solver; result: SAT.
[0.040] Solver returned SAT; a solution has been found at makespan 4.
19
solution 349 1
30 3 26 5 27 28 14 15 3 68 60 63 61 64 65 56 57 60 349 
[0.041] Exiting.

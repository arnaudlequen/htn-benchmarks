Command : ./interpreter_t-rex_binary -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.006] Processed problem encoding.
[0.007] Calculated possible fact changes of composite elements.
[0.007] Initialized instantiation procedure.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     0   * * *
[0.007] *************************************
[0.007] Instantiated 156 initial clauses.
[0.007] The encoding contains a total of 107 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.007] Executed solver; result: UNSAT.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     1   * * *
[0.007] *************************************
[0.007] Computed next depth properties: array size of 3.
[0.007] Instantiated 8 transitional clauses.
[0.008] Instantiated 198 universal clauses.
[0.008] Instantiated and added clauses for a total of 362 clauses.
[0.008] The encoding contains a total of 190 distinct variables.
[0.008] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.008] Executed solver; result: UNSAT.
[0.008] 
[0.008] *************************************
[0.008] * * *   M a k e s p a n     2   * * *
[0.008] *************************************
[0.008] Computed next depth properties: array size of 4.
[0.009] Instantiated 3,225 transitional clauses.
[0.013] Instantiated 12,777 universal clauses.
[0.013] Instantiated and added clauses for a total of 16,364 clauses.
[0.013] The encoding contains a total of 2,422 distinct variables.
[0.013] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.013] Executed solver; result: UNSAT.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     3   * * *
[0.013] *************************************
[0.014] Computed next depth properties: array size of 15.
[0.015] Instantiated 6,153 transitional clauses.
[0.017] Instantiated 19,872 universal clauses.
[0.017] Instantiated and added clauses for a total of 42,389 clauses.
[0.017] The encoding contains a total of 3,812 distinct variables.
[0.017] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.017] Executed solver; result: UNSAT.
[0.017] 
[0.017] *************************************
[0.017] * * *   M a k e s p a n     4   * * *
[0.017] *************************************
[0.018] Computed next depth properties: array size of 31.
[0.019] Instantiated 874 transitional clauses.
[0.020] Instantiated 2,760 universal clauses.
[0.020] Instantiated and added clauses for a total of 46,023 clauses.
[0.020] The encoding contains a total of 4,268 distinct variables.
[0.020] Attempting solve with solver <glucose4> ...
c 31 assumptions
c last restart ## conflicts  :  1 88 
[0.020] Executed solver; result: SAT.
[0.020] Solver returned SAT; a solution has been found at makespan 4.
18
solution 74 1
37 21 33 22 34 35 14 15 21 74 58 70 59 71 72 51 52 58 
[0.020] Exiting.

Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.009] Processed problem encoding.
[0.009] Calculated possible fact changes of composite elements.
[0.009] Initialized instantiation procedure.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     0   * * *
[0.009] *************************************
[0.010] Instantiated 307 initial clauses.
[0.010] The encoding contains a total of 211 distinct variables.
[0.010] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.010] Executed solver; result: UNSAT.
[0.010] 
[0.010] *************************************
[0.010] * * *   M a k e s p a n     1   * * *
[0.010] *************************************
[0.010] Computed next depth properties: array size of 5.
[0.011] Instantiated 1,369 transitional clauses.
[0.011] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.011] Instantiated 2,015 universal clauses.
[0.011] Instantiated and added clauses for a total of 3,691 clauses.
[0.011] The encoding contains a total of 397 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 5 assumptions
[0.011] Executed solver; result: UNSAT.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     2   * * *
[0.011] *************************************
[0.012] Computed next depth properties: array size of 7.
[0.014] Instantiated 3,525 transitional clauses.
[0.019] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.019] Instantiated 61,671 universal clauses.
[0.019] Instantiated and added clauses for a total of 68,887 clauses.
[0.019] The encoding contains a total of 1,363 distinct variables.
[0.019] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.019] Executed solver; result: UNSAT.
[0.019] 
[0.019] *************************************
[0.019] * * *   M a k e s p a n     3   * * *
[0.019] *************************************
[0.020] Computed next depth properties: array size of 18.
[0.023] Instantiated 6,556 transitional clauses.
[0.028] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.029] Instantiated 119,175 universal clauses.
[0.029] Instantiated and added clauses for a total of 194,618 clauses.
[0.029] The encoding contains a total of 2,747 distinct variables.
[0.029] Attempting solve with solver <glucose4> ...
c 18 assumptions
[0.029] Executed solver; result: UNSAT.
[0.029] 
[0.029] *************************************
[0.029] * * *   M a k e s p a n     4   * * *
[0.029] *************************************
[0.029] Computed next depth properties: array size of 36.
[0.031] Instantiated 6,776 transitional clauses.
[0.040] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.040] Instantiated 127,397 universal clauses.
[0.040] Instantiated and added clauses for a total of 328,791 clauses.
[0.040] The encoding contains a total of 4,332 distinct variables.
[0.040] Attempting solve with solver <glucose4> ...
c 36 assumptions
c last restart ## conflicts  :  81 98 
[0.042] Executed solver; result: SAT.
[0.042] Solver returned SAT; a solution has been found at makespan 4.
18
solution 68 1
31 21 26 22 27 28 19 20 21 68 58 63 59 64 65 56 57 58 
[0.042] Exiting.

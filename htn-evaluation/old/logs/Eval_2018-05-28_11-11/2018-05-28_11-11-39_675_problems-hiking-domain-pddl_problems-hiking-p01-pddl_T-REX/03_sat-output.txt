Command : ./interpreter_t-rex_binary -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.047] Processed problem encoding.
[0.054] Calculated possible fact changes of composite elements.
[0.054] Initialized instantiation procedure.
[0.054] 
[0.054] *************************************
[0.054] * * *   M a k e s p a n     0   * * *
[0.054] *************************************
[0.055] Instantiated 156 initial clauses.
[0.055] The encoding contains a total of 107 distinct variables.
[0.055] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.055] Executed solver; result: UNSAT.
[0.055] 
[0.055] *************************************
[0.055] * * *   M a k e s p a n     1   * * *
[0.055] *************************************
[0.055] Computed next depth properties: array size of 2.
[0.055] Instantiated 6 transitional clauses.
[0.055] Instantiated 110 universal clauses.
[0.055] Instantiated and added clauses for a total of 272 clauses.
[0.055] The encoding contains a total of 136 distinct variables.
[0.055] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.055] Executed solver; result: UNSAT.
[0.055] 
[0.055] *************************************
[0.055] * * *   M a k e s p a n     2   * * *
[0.055] *************************************
[0.056] Computed next depth properties: array size of 5.
[0.057] Instantiated 870 transitional clauses.
[0.058] Instantiated 4,063 universal clauses.
[0.058] Instantiated and added clauses for a total of 5,205 clauses.
[0.058] The encoding contains a total of 985 distinct variables.
[0.058] Attempting solve with solver <glucose4> ...
c 5 assumptions
[0.058] Executed solver; result: UNSAT.
[0.058] 
[0.058] *************************************
[0.058] * * *   M a k e s p a n     3   * * *
[0.058] *************************************
[0.060] Computed next depth properties: array size of 14.
[0.071] Instantiated 21,891 transitional clauses.
[0.074] Instantiated 37,130 universal clauses.
[0.074] Instantiated and added clauses for a total of 64,226 clauses.
[0.075] The encoding contains a total of 5,349 distinct variables.
[0.075] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.075] Executed solver; result: UNSAT.
[0.075] 
[0.075] *************************************
[0.075] * * *   M a k e s p a n     4   * * *
[0.075] *************************************
[0.077] Computed next depth properties: array size of 34.
[0.093] Instantiated 51,816 transitional clauses.
[0.104] Instantiated 106,472 universal clauses.
[0.104] Instantiated and added clauses for a total of 222,514 clauses.
[0.104] The encoding contains a total of 14,501 distinct variables.
[0.104] Attempting solve with solver <glucose4> ...
c 34 assumptions
c last restart ## conflicts  :  0 215 
[0.122] Executed solver; result: SAT.
[0.122] Solver returned SAT; a solution has been found at makespan 4.
6
solution 385 1
209 189 381 315 302 385 
[0.122] Exiting.

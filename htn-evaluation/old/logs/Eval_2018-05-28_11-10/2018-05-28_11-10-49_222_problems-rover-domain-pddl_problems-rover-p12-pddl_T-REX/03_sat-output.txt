Command : ./interpreter_t-rex_binary -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.088] Processed problem encoding.
[0.209] Calculated possible fact changes of composite elements.
[0.211] Initialized instantiation procedure.
[0.211] 
[0.211] *************************************
[0.211] * * *   M a k e s p a n     0   * * *
[0.211] *************************************
[0.213] Instantiated 7,900 initial clauses.
[0.213] The encoding contains a total of 4,443 distinct variables.
[0.213] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.213] Executed solver; result: UNSAT.
[0.213] 
[0.213] *************************************
[0.213] * * *   M a k e s p a n     1   * * *
[0.213] *************************************
[0.225] Computed next depth properties: array size of 57.
[0.239] Instantiated 18,703 transitional clauses.
[0.256] Instantiated 54,758 universal clauses.
[0.256] Instantiated and added clauses for a total of 81,361 clauses.
[0.256] The encoding contains a total of 25,135 distinct variables.
[0.256] Attempting solve with solver <glucose4> ...
c 57 assumptions
[0.256] Executed solver; result: UNSAT.
[0.256] 
[0.256] *************************************
[0.256] * * *   M a k e s p a n     2   * * *
[0.256] *************************************
[0.292] Computed next depth properties: array size of 102.
[0.356] Instantiated 111,238 transitional clauses.
[0.398] Instantiated 162,833 universal clauses.
[0.398] Instantiated and added clauses for a total of 355,432 clauses.
[0.398] The encoding contains a total of 83,052 distinct variables.
[0.398] Attempting solve with solver <glucose4> ...
c 102 assumptions
[0.398] Executed solver; result: UNSAT.
[0.398] 
[0.398] *************************************
[0.398] * * *   M a k e s p a n     3   * * *
[0.398] *************************************
[0.472] Computed next depth properties: array size of 178.
[0.636] Instantiated 289,022 transitional clauses.
[0.710] Instantiated 295,806 universal clauses.
[0.710] Instantiated and added clauses for a total of 940,260 clauses.
[0.710] The encoding contains a total of 164,334 distinct variables.
[0.710] Attempting solve with solver <glucose4> ...
c 178 assumptions
[0.772] Executed solver; result: UNSAT.
[0.772] 
[0.772] *************************************
[0.772] * * *   M a k e s p a n     4   * * *
[0.772] *************************************
[0.865] Computed next depth properties: array size of 271.
[1.048] Instantiated 389,933 transitional clauses.
[1.140] Instantiated 395,355 universal clauses.
[1.140] Instantiated and added clauses for a total of 1,725,548 clauses.
[1.140] The encoding contains a total of 251,727 distinct variables.
[1.140] Attempting solve with solver <glucose4> ...
c 271 assumptions
c last restart ## conflicts  :  59 1018 
[1.394] Executed solver; result: SAT.
[1.394] Solver returned SAT; a solution has been found at makespan 4.
184
solution 1877 1
59 515 21 487 22 60 682 1062 7 408 43 447 44 8 651 5 405 43 448 44 6 815 7 406 21 425 31 437 32 22 8 639 645 3 404 4 810 19 596 20 672 25 607 19 597 20 26 1030 33 208 34 661 23 199 33 210 34 24 714 49 626 19 593 5 582 6 20 50 642 678 43 620 5 579 6 44 1034 31 438 21 421 22 32 639 657 1059 37 212 33 207 5 185 6 34 38 636 685 51 224 5 184 6 52 743 37 326 23 314 24 38 1070 13 296 23 315 24 14 1150 49 222 50 636 1081 1239 37 325 21 306 22 38 638 1076 41 332 21 309 22 42 1160 17 591 18 1877 61 577 17 592 5 581 6 18 62 1430 33 614 5 579 6 34 1812 1838 7 234 55 280 56 8 1522 1731 49 338 50 1849 21 300 7 291 8 22 1338 27 317 28 1738 
[1.396] Exiting.

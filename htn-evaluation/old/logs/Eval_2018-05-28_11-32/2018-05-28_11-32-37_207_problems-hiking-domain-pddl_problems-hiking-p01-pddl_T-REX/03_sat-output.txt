Command : ./interpreter_t-rex_binary -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.012] Processed problem encoding.
[0.013] Calculated possible fact changes of composite elements.
[0.014] Initialized instantiation procedure.
[0.014] 
[0.014] *************************************
[0.014] * * *   M a k e s p a n     0   * * *
[0.014] *************************************
[0.015] Instantiated 1,128 initial clauses.
[0.015] The encoding contains a total of 1,029 distinct variables.
[0.015] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.015] Executed solver; result: UNSAT.
[0.015] 
[0.015] *************************************
[0.015] * * *   M a k e s p a n     1   * * *
[0.015] *************************************
[0.015] Computed next depth properties: array size of 6.
[0.016] Instantiated 1,845 transitional clauses.
[0.017] Instantiated 2,009 universal clauses.
[0.017] Instantiated and added clauses for a total of 4,982 clauses.
[0.017] The encoding contains a total of 1,272 distinct variables.
[0.017] Attempting solve with solver <glucose4> ...
c 6 assumptions
[0.017] Executed solver; result: UNSAT.
[0.017] 
[0.017] *************************************
[0.017] * * *   M a k e s p a n     2   * * *
[0.017] *************************************
[0.018] Computed next depth properties: array size of 7.
[0.019] Instantiated 3,223 transitional clauses.
[0.022] Instantiated 12,936 universal clauses.
[0.022] Instantiated and added clauses for a total of 21,141 clauses.
[0.022] The encoding contains a total of 3,506 distinct variables.
[0.022] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.022] Executed solver; result: UNSAT.
[0.022] 
[0.022] *************************************
[0.022] * * *   M a k e s p a n     3   * * *
[0.022] *************************************
[0.023] Computed next depth properties: array size of 18.
[0.024] Instantiated 6,065 transitional clauses.
[0.026] Instantiated 19,455 universal clauses.
[0.026] Instantiated and added clauses for a total of 46,661 clauses.
[0.026] The encoding contains a total of 4,786 distinct variables.
[0.026] Attempting solve with solver <glucose4> ...
c 18 assumptions
c last restart ## conflicts  :  2 92 
[0.026] Executed solver; result: SAT.
[0.026] Solver returned SAT; a solution has been found at makespan 3.
11
solution 507 1
38 22 34 23 35 36 15 16 22 2 507 
[0.026] Exiting.

Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.006] Processed problem encoding.
[0.006] Calculated possible fact changes of composite elements.
[0.006] Initialized instantiation procedure.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     0   * * *
[0.006] *************************************
[0.007] Instantiated 1,374 initial clauses.
[0.007] The encoding contains a total of 1,086 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 8 assumptions
[0.007] Executed solver; result: UNSAT.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     1   * * *
[0.007] *************************************
[0.007] Computed next depth properties: array size of 13.
[0.009] Instantiated 4,730 transitional clauses.
[0.015] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.015] Instantiated 71,999 universal clauses.
[0.015] Instantiated and added clauses for a total of 78,103 clauses.
[0.015] The encoding contains a total of 2,261 distinct variables.
[0.015] Attempting solve with solver <glucose4> ...
c 13 assumptions
[0.015] Executed solver; result: UNSAT.
[0.015] 
[0.015] *************************************
[0.015] * * *   M a k e s p a n     2   * * *
[0.015] *************************************
[0.016] Computed next depth properties: array size of 32.
[0.018] Instantiated 7,257 transitional clauses.
[0.026] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.026] Instantiated 135,166 universal clauses.
[0.026] Instantiated and added clauses for a total of 220,526 clauses.
[0.026] The encoding contains a total of 4,583 distinct variables.
[0.026] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.026] Executed solver; result: UNSAT.
[0.026] 
[0.026] *************************************
[0.026] * * *   M a k e s p a n     3   * * *
[0.026] *************************************
[0.028] Computed next depth properties: array size of 76.
[0.033] Instantiated 11,422 transitional clauses.
[0.050] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.050] Instantiated 196,517 universal clauses.
[0.050] Instantiated and added clauses for a total of 428,465 clauses.
[0.050] The encoding contains a total of 7,566 distinct variables.
[0.050] Attempting solve with solver <glucose4> ...
c 76 assumptions
[0.050] Executed solver; result: UNSAT.
[0.050] 
[0.050] *************************************
[0.050] * * *   M a k e s p a n     4   * * *
[0.050] *************************************
[0.053] Computed next depth properties: array size of 95.
[0.060] Instantiated 10,386 transitional clauses.
[0.078] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.078] Instantiated 206,362 universal clauses.
[0.078] Instantiated and added clauses for a total of 645,213 clauses.
[0.078] The encoding contains a total of 10,469 distinct variables.
[0.078] Attempting solve with solver <glucose4> ...
c 95 assumptions
c last restart ## conflicts  :  41 201 
[0.080] Executed solver; result: SAT.
[0.080] Solver returned SAT; a solution has been found at makespan 4.
58
solution 544 1
25 23 26 24 27 28 8 9 23 67 54 63 55 64 65 51 52 54 291 76 77 78 79 80 81 82 83 77 113 114 115 116 117 118 119 120 114 544 534 517 185 169 181 170 182 183 162 163 169 221 208 218 209 219 220 196 197 208 
[0.081] Exiting.

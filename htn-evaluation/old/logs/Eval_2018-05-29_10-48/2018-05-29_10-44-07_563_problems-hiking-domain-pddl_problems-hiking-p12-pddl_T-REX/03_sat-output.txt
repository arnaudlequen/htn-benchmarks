Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.158] Processed problem encoding.
[0.295] Calculated possible fact changes of composite elements.
[0.296] Initialized instantiation procedure.
[0.296] 
[0.296] *************************************
[0.296] * * *   M a k e s p a n     0   * * *
[0.296] *************************************
[0.299] Instantiated 496 initial clauses.
[0.299] The encoding contains a total of 340 distinct variables.
[0.299] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.299] Executed solver; result: UNSAT.
[0.299] 
[0.299] *************************************
[0.299] * * *   M a k e s p a n     1   * * *
[0.299] *************************************
[0.300] Computed next depth properties: array size of 3.
[0.300] Instantiated 80 transitional clauses.
[0.305] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.305] Instantiated 643 universal clauses.
[0.305] Instantiated and added clauses for a total of 1,219 clauses.
[0.305] The encoding contains a total of 503 distinct variables.
[0.305] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.305] Executed solver; result: UNSAT.
[0.305] 
[0.305] *************************************
[0.305] * * *   M a k e s p a n     2   * * *
[0.305] *************************************
[0.308] Computed next depth properties: array size of 4.
[0.321] Instantiated 37,436 transitional clauses.
[0.743] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.743] Instantiated 17,816,521 universal clauses.
[0.743] Instantiated and added clauses for a total of 17,855,176 clauses.
[0.743] The encoding contains a total of 14,131 distinct variables.
[0.743] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.748] Executed solver; result: UNSAT.
[0.748] 
[0.748] *************************************
[0.748] * * *   M a k e s p a n     3   * * *
[0.748] *************************************
[0.756] Computed next depth properties: array size of 14.
[0.827] Instantiated 103,771 transitional clauses.
[1.654] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.654] Instantiated 35,698,774 universal clauses.
[1.654] Instantiated and added clauses for a total of 53,657,721 clauses.
[1.654] The encoding contains a total of 31,558 distinct variables.
[1.654] Attempting solve with solver <glucose4> ...
c 14 assumptions
[1.655] Executed solver; result: UNSAT.
[1.655] 
[1.655] *************************************
[1.655] * * *   M a k e s p a n     4   * * *
[1.655] *************************************
[1.666] Computed next depth properties: array size of 32.
[1.737] Instantiated 111,474 transitional clauses.
[2.630] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.630] Instantiated 36,909,334 universal clauses.
[2.630] Instantiated and added clauses for a total of 90,678,529 clauses.
[2.630] The encoding contains a total of 51,827 distinct variables.
[2.630] Attempting solve with solver <glucose4> ...
c 32 assumptions
[2.631] Executed solver; result: UNSAT.
[2.631] 
[2.631] *************************************
[2.631] * * *   M a k e s p a n     5   * * *
[2.631] *************************************
[2.646] Computed next depth properties: array size of 52.
[2.724] Instantiated 114,066 transitional clauses.
[3.687] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.687] Instantiated 38,082,520 universal clauses.
[3.687] Instantiated and added clauses for a total of 128,875,115 clauses.
[3.687] The encoding contains a total of 74,503 distinct variables.
[3.687] Attempting solve with solver <glucose4> ...
c 52 assumptions
[3.688] Executed solver; result: UNSAT.
[3.688] 
[3.688] *************************************
[3.688] * * *   M a k e s p a n     6   * * *
[3.688] *************************************
[3.707] Computed next depth properties: array size of 74.
[3.793] Instantiated 116,210 transitional clauses.
[4.824] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.824] Instantiated 39,217,996 universal clauses.
[4.824] Instantiated and added clauses for a total of 168,209,321 clauses.
[4.824] The encoding contains a total of 99,424 distinct variables.
[4.824] Attempting solve with solver <glucose4> ...
c 74 assumptions
[4.827] Executed solver; result: UNSAT.
[4.827] 
[4.827] *************************************
[4.827] * * *   M a k e s p a n     7   * * *
[4.827] *************************************
[4.849] Computed next depth properties: array size of 98.
[4.940] Instantiated 118,026 transitional clauses.
[6.033] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[6.033] Instantiated 40,323,218 universal clauses.
[6.033] Instantiated and added clauses for a total of 208,650,565 clauses.
[6.033] The encoding contains a total of 126,420 distinct variables.
[6.033] Attempting solve with solver <glucose4> ...
c 98 assumptions
[6.036] Executed solver; result: UNSAT.
[6.036] 
[6.036] *************************************
[6.036] * * *   M a k e s p a n     8   * * *
[6.036] *************************************
[6.064] Computed next depth properties: array size of 124.
[6.161] Instantiated 119,174 transitional clauses.
[7.317] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[7.317] Instantiated 41,404,256 universal clauses.
[7.317] Instantiated and added clauses for a total of 250,173,995 clauses.
[7.317] The encoding contains a total of 154,966 distinct variables.
[7.317] Attempting solve with solver <glucose4> ...
c 124 assumptions
[7.321] Executed solver; result: UNSAT.
[7.321] 
[7.321] *************************************
[7.321] * * *   M a k e s p a n     9   * * *
[7.321] *************************************
[7.352] Computed next depth properties: array size of 136.
[7.453] Instantiated 116,512 transitional clauses.
[8.672] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[8.672] Instantiated 42,466,626 universal clauses.
[8.672] Instantiated and added clauses for a total of 292,757,133 clauses.
[8.672] The encoding contains a total of 185,035 distinct variables.
[8.672] Attempting solve with solver <glucose4> ...
c 136 assumptions
[8.789] Executed solver; result: UNSAT.
[8.789] 
[8.789] *************************************
[8.789] * * *   M a k e s p a n    10   * * *
[8.789] *************************************
[8.823] Computed next depth properties: array size of 148.
[8.927] Instantiated 120,112 transitional clauses.
[10.209] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[10.209] Instantiated 43,528,996 universal clauses.
[10.209] Instantiated and added clauses for a total of 336,406,241 clauses.
[10.209] The encoding contains a total of 216,916 distinct variables.
[10.209] Attempting solve with solver <glucose4> ...
c 148 assumptions
[10.223] Executed solver; result: UNSAT.
[10.223] 
[10.223] *************************************
[10.223] * * *   M a k e s p a n    11   * * *
[10.223] *************************************
[10.259] Computed next depth properties: array size of 160.
[10.365] Instantiated 123,712 transitional clauses.
[11.705] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[11.705] Instantiated 44,591,366 universal clauses.
[11.705] Instantiated and added clauses for a total of 381,121,319 clauses.
[11.705] The encoding contains a total of 250,609 distinct variables.
[11.705] Attempting solve with solver <glucose4> ...
c 160 assumptions
[11.722] Executed solver; result: UNSAT.
[11.722] 
[11.722] *************************************
[11.722] * * *   M a k e s p a n    12   * * *
[11.722] *************************************
[11.760] Computed next depth properties: array size of 172.
[11.873] Instantiated 127,312 transitional clauses.
[13.279] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[13.279] Instantiated 45,653,736 universal clauses.
[13.279] Instantiated and added clauses for a total of 426,902,367 clauses.
[13.279] The encoding contains a total of 286,114 distinct variables.
[13.279] Attempting solve with solver <glucose4> ...
c 172 assumptions
c last restart ## conflicts  :  98 289 
[14.274] Executed solver; result: SAT.
[14.274] Solver returned SAT; a solution has been found at makespan 12.
73
solution 6732 1
6732 336 291 332 293 1229 1228 1227 1230 333 304 305 291 384 339 380 341 1232 1231 1233 1234 381 352 353 339 433 411 428 412 1235 1236 1238 1237 429 405 406 411 480 435 476 437 1239 1240 1241 1242 477 448 449 435 528 483 524 485 1243 1246 1245 1244 525 496 497 483 576 531 572 533 1249 1250 1248 1247 573 544 545 531 
[14.278] Exiting.

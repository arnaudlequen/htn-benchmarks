Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.162] Processed problem encoding.
[0.212] Calculated possible fact changes of composite elements.
[0.212] Initialized instantiation procedure.
[0.212] 
[0.212] *************************************
[0.212] * * *   M a k e s p a n     0   * * *
[0.212] *************************************
[0.214] Instantiated 429 initial clauses.
[0.214] The encoding contains a total of 294 distinct variables.
[0.214] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.214] Executed solver; result: UNSAT.
[0.214] 
[0.214] *************************************
[0.214] * * *   M a k e s p a n     1   * * *
[0.214] *************************************
[0.215] Computed next depth properties: array size of 3.
[0.215] Instantiated 67 transitional clauses.
[0.218] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.218] Instantiated 554 universal clauses.
[0.218] Instantiated and added clauses for a total of 1,050 clauses.
[0.218] The encoding contains a total of 435 distinct variables.
[0.218] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.218] Executed solver; result: UNSAT.
[0.218] 
[0.218] *************************************
[0.218] * * *   M a k e s p a n     2   * * *
[0.218] *************************************
[0.220] Computed next depth properties: array size of 4.
[0.229] Instantiated 26,250 transitional clauses.
[0.456] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.456] Instantiated 8,725,487 universal clauses.
[0.456] Instantiated and added clauses for a total of 8,752,787 clauses.
[0.456] The encoding contains a total of 10,050 distinct variables.
[0.456] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.459] Executed solver; result: UNSAT.
[0.459] 
[0.459] *************************************
[0.459] * * *   M a k e s p a n     3   * * *
[0.459] *************************************
[0.466] Computed next depth properties: array size of 14.
[0.510] Instantiated 73,395 transitional clauses.
[0.973] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.973] Instantiated 17,407,992 universal clauses.
[0.973] Instantiated and added clauses for a total of 26,234,174 clauses.
[0.973] The encoding contains a total of 22,649 distinct variables.
[0.973] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.974] Executed solver; result: UNSAT.
[0.974] 
[0.974] *************************************
[0.974] * * *   M a k e s p a n     4   * * *
[0.974] *************************************
[0.982] Computed next depth properties: array size of 32.
[1.036] Instantiated 80,662 transitional clauses.
[1.493] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.493] Instantiated 18,029,294 universal clauses.
[1.493] Instantiated and added clauses for a total of 44,344,130 clauses.
[1.493] The encoding contains a total of 37,370 distinct variables.
[1.493] Attempting solve with solver <glucose4> ...
c 32 assumptions
[1.494] Executed solver; result: UNSAT.
[1.494] 
[1.494] *************************************
[1.494] * * *   M a k e s p a n     5   * * *
[1.494] *************************************
[1.504] Computed next depth properties: array size of 52.
[1.554] Instantiated 81,886 transitional clauses.
[2.062] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.062] Instantiated 18,619,142 universal clauses.
[2.062] Instantiated and added clauses for a total of 63,045,158 clauses.
[2.062] The encoding contains a total of 53,802 distinct variables.
[2.062] Attempting solve with solver <glucose4> ...
c 52 assumptions
[2.063] Executed solver; result: UNSAT.
[2.063] 
[2.063] *************************************
[2.063] * * *   M a k e s p a n     6   * * *
[2.063] *************************************
[2.076] Computed next depth properties: array size of 74.
[2.135] Instantiated 82,630 transitional clauses.
[2.674] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.674] Instantiated 19,178,976 universal clauses.
[2.674] Instantiated and added clauses for a total of 82,306,764 clauses.
[2.674] The encoding contains a total of 71,767 distinct variables.
[2.674] Attempting solve with solver <glucose4> ...
c 74 assumptions
[2.676] Executed solver; result: UNSAT.
[2.676] 
[2.676] *************************************
[2.676] * * *   M a k e s p a n     7   * * *
[2.676] *************************************
[2.692] Computed next depth properties: array size of 98.
[2.746] Instantiated 82,698 transitional clauses.
[3.319] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.319] Instantiated 19,714,626 universal clauses.
[3.319] Instantiated and added clauses for a total of 102,104,088 clauses.
[3.319] The encoding contains a total of 90,740 distinct variables.
[3.319] Attempting solve with solver <glucose4> ...
c 98 assumptions
[3.322] Executed solver; result: UNSAT.
[3.322] 
[3.322] *************************************
[3.322] * * *   M a k e s p a n     8   * * *
[3.322] *************************************
[3.342] Computed next depth properties: array size of 108.
[3.409] Instantiated 78,956 transitional clauses.
[4.022] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.022] Instantiated 20,231,608 universal clauses.
[4.022] Instantiated and added clauses for a total of 122,414,652 clauses.
[4.022] The encoding contains a total of 110,694 distinct variables.
[4.022] Attempting solve with solver <glucose4> ...
c 108 assumptions
[4.030] Executed solver; result: UNSAT.
[4.030] 
[4.030] *************************************
[4.030] * * *   M a k e s p a n     9   * * *
[4.030] *************************************
[4.052] Computed next depth properties: array size of 118.
[4.124] Instantiated 81,476 transitional clauses.
[4.761] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.761] Instantiated 20,748,590 universal clauses.
[4.761] Instantiated and added clauses for a total of 143,244,718 clauses.
[4.761] The encoding contains a total of 131,918 distinct variables.
[4.761] Attempting solve with solver <glucose4> ...
c 118 assumptions
[4.824] Executed solver; result: UNSAT.
[4.824] 
[4.824] *************************************
[4.824] * * *   M a k e s p a n    10   * * *
[4.824] *************************************
[4.845] Computed next depth properties: array size of 128.
[4.907] Instantiated 83,996 transitional clauses.
[5.561] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[5.561] Instantiated 21,265,572 universal clauses.
[5.561] Instantiated and added clauses for a total of 164,594,286 clauses.
[5.561] The encoding contains a total of 154,412 distinct variables.
[5.561] Attempting solve with solver <glucose4> ...
c 128 assumptions
[5.581] Executed solver; result: UNSAT.
[5.581] 
[5.581] *************************************
[5.581] * * *   M a k e s p a n    11   * * *
[5.581] *************************************
[5.604] Computed next depth properties: array size of 138.
[5.665] Instantiated 86,516 transitional clauses.
[6.327] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[6.327] Instantiated 21,782,554 universal clauses.
[6.327] Instantiated and added clauses for a total of 186,463,356 clauses.
[6.327] The encoding contains a total of 178,176 distinct variables.
[6.327] Attempting solve with solver <glucose4> ...
c 138 assumptions
c last restart ## conflicts  :  81 689 
[6.638] Executed solver; result: SAT.
[6.638] Solver returned SAT; a solution has been found at makespan 11.
61
solution 4814 1
4814 482 505 484 506 963 964 965 966 486 487 488 505 540 547 532 548 967 969 968 970 534 541 542 547 578 601 580 602 972 974 973 971 582 583 584 601 636 643 628 644 975 977 978 976 630 637 638 643 684 691 676 692 982 981 979 980 678 685 686 691 
[6.641] Exiting.

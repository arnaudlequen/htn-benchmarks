Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.088] Processed problem encoding.
[0.153] Calculated possible fact changes of composite elements.
[0.153] Initialized instantiation procedure.
[0.153] 
[0.153] *************************************
[0.153] * * *   M a k e s p a n     0   * * *
[0.153] *************************************
[0.155] Instantiated 429 initial clauses.
[0.155] The encoding contains a total of 295 distinct variables.
[0.155] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.155] Executed solver; result: UNSAT.
[0.155] 
[0.155] *************************************
[0.155] * * *   M a k e s p a n     1   * * *
[0.155] *************************************
[0.155] Computed next depth properties: array size of 3.
[0.156] Instantiated 72 transitional clauses.
[0.158] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.158] Instantiated 559 universal clauses.
[0.158] Instantiated and added clauses for a total of 1,060 clauses.
[0.158] The encoding contains a total of 439 distinct variables.
[0.158] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.158] Executed solver; result: UNSAT.
[0.158] 
[0.158] *************************************
[0.158] * * *   M a k e s p a n     2   * * *
[0.158] *************************************
[0.161] Computed next depth properties: array size of 4.
[0.171] Instantiated 23,223 transitional clauses.
[0.377] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.377] Instantiated 6,739,794 universal clauses.
[0.377] Instantiated and added clauses for a total of 6,764,077 clauses.
[0.377] The encoding contains a total of 9,042 distinct variables.
[0.377] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.379] Executed solver; result: UNSAT.
[0.379] 
[0.379] *************************************
[0.379] * * *   M a k e s p a n     3   * * *
[0.379] *************************************
[0.386] Computed next depth properties: array size of 14.
[0.427] Instantiated 56,539 transitional clauses.
[0.833] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.833] Instantiated 13,759,536 universal clauses.
[0.833] Instantiated and added clauses for a total of 20,580,152 clauses.
[0.833] The encoding contains a total of 20,038 distinct variables.
[0.833] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.833] Executed solver; result: UNSAT.
[0.833] 
[0.833] *************************************
[0.833] * * *   M a k e s p a n     4   * * *
[0.833] *************************************
[0.839] Computed next depth properties: array size of 32.
[0.879] Instantiated 58,842 transitional clauses.
[1.273] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.273] Instantiated 14,517,950 universal clauses.
[1.273] Instantiated and added clauses for a total of 35,156,944 clauses.
[1.273] The encoding contains a total of 33,281 distinct variables.
[1.273] Attempting solve with solver <glucose4> ...
c 32 assumptions
[1.274] Executed solver; result: UNSAT.
[1.274] 
[1.274] *************************************
[1.274] * * *   M a k e s p a n     5   * * *
[1.274] *************************************
[1.282] Computed next depth properties: array size of 52.
[1.328] Instantiated 61,983 transitional clauses.
[1.780] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.780] Instantiated 15,266,600 universal clauses.
[1.780] Instantiated and added clauses for a total of 50,485,527 clauses.
[1.780] The encoding contains a total of 48,447 distinct variables.
[1.780] Attempting solve with solver <glucose4> ...
c 52 assumptions
[1.781] Executed solver; result: UNSAT.
[1.781] 
[1.781] *************************************
[1.781] * * *   M a k e s p a n     6   * * *
[1.781] *************************************
[1.793] Computed next depth properties: array size of 74.
[1.844] Instantiated 64,898 transitional clauses.
[2.342] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.342] Instantiated 16,004,583 universal clauses.
[2.342] Instantiated and added clauses for a total of 66,555,008 clauses.
[2.342] The encoding contains a total of 65,458 distinct variables.
[2.342] Attempting solve with solver <glucose4> ...
c 74 assumptions
[2.343] Executed solver; result: UNSAT.
[2.343] 
[2.343] *************************************
[2.343] * * *   M a k e s p a n     7   * * *
[2.343] *************************************
[2.358] Computed next depth properties: array size of 98.
[2.414] Instantiated 67,657 transitional clauses.
[2.944] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.944] Instantiated 16,733,393 universal clauses.
[2.944] Instantiated and added clauses for a total of 83,356,058 clauses.
[2.944] The encoding contains a total of 84,233 distinct variables.
[2.944] Attempting solve with solver <glucose4> ...
c 98 assumptions
[2.946] Executed solver; result: UNSAT.
[2.946] 
[2.946] *************************************
[2.946] * * *   M a k e s p a n     8   * * *
[2.946] *************************************
[2.966] Computed next depth properties: array size of 124.
[3.035] Instantiated 70,250 transitional clauses.
[3.602] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.602] Instantiated 17,454,785 universal clauses.
[3.602] Instantiated and added clauses for a total of 100,881,093 clauses.
[3.602] The encoding contains a total of 104,682 distinct variables.
[3.602] Attempting solve with solver <glucose4> ...
c 124 assumptions
[3.605] Executed solver; result: UNSAT.
[3.605] 
[3.605] *************************************
[3.605] * * *   M a k e s p a n     9   * * *
[3.605] *************************************
[3.626] Computed next depth properties: array size of 152.
[3.683] Instantiated 72,554 transitional clauses.
[4.351] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.351] Instantiated 18,169,916 universal clauses.
[4.351] Instantiated and added clauses for a total of 119,123,563 clauses.
[4.351] The encoding contains a total of 126,584 distinct variables.
[4.351] Attempting solve with solver <glucose4> ...
c 152 assumptions
[4.431] Executed solver; result: UNSAT.
[4.431] 
[4.431] *************************************
[4.431] * * *   M a k e s p a n    10   * * *
[4.431] *************************************
[4.456] Computed next depth properties: array size of 166.
[4.523] Instantiated 73,290 transitional clauses.
[5.187] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[5.187] Instantiated 18,879,711 universal clauses.
[5.187] Instantiated and added clauses for a total of 138,076,564 clauses.
[5.187] The encoding contains a total of 149,734 distinct variables.
[5.187] Attempting solve with solver <glucose4> ...
c 166 assumptions
[5.195] Executed solver; result: UNSAT.
[5.195] 
[5.195] *************************************
[5.195] * * *   M a k e s p a n    11   * * *
[5.195] *************************************
[5.225] Computed next depth properties: array size of 180.
[5.294] Instantiated 76,202 transitional clauses.
[5.971] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[5.971] Instantiated 19,589,506 universal clauses.
[5.971] Instantiated and added clauses for a total of 157,742,272 clauses.
[5.971] The encoding contains a total of 174,354 distinct variables.
[5.971] Attempting solve with solver <glucose4> ...
c 180 assumptions
[5.978] Executed solver; result: UNSAT.
[5.978] 
[5.978] *************************************
[5.978] * * *   M a k e s p a n    12   * * *
[5.978] *************************************
[6.006] Computed next depth properties: array size of 194.
[6.074] Instantiated 79,114 transitional clauses.
[6.830] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[6.830] Instantiated 20,299,301 universal clauses.
[6.830] Instantiated and added clauses for a total of 178,120,687 clauses.
[6.830] The encoding contains a total of 200,444 distinct variables.
[6.830] Attempting solve with solver <glucose4> ...
c 194 assumptions
c last restart ## conflicts  :  67 265 
[8.171] Executed solver; result: SAT.
[8.171] Solver returned SAT; a solution has been found at makespan 12.
78
solution 4155 1
4155 491 492 480 493 717 719 718 482 494 495 492 512 513 514 515 722 720 721 516 517 518 513 559 560 548 561 725 723 724 550 562 563 560 580 581 582 583 727 726 728 584 585 586 581 614 615 616 617 729 730 731 618 619 620 615 648 649 650 651 733 732 734 652 653 654 649 682 683 684 685 736 737 735 686 687 688 683 
[8.174] Exiting.

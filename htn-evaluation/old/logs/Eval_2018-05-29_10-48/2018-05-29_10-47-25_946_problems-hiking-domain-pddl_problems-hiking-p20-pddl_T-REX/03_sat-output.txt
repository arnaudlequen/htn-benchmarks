Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.002] Parsed head comment information.
[0.471] Processed problem encoding.
[1.083] Calculated possible fact changes of composite elements.
[1.083] Initialized instantiation procedure.
[1.083] 
[1.083] *************************************
[1.083] * * *   M a k e s p a n     0   * * *
[1.083] *************************************
[1.089] Instantiated 697 initial clauses.
[1.089] The encoding contains a total of 477 distinct variables.
[1.089] Attempting solve with solver <glucose4> ...
c 2 assumptions
[1.089] Executed solver; result: UNSAT.
[1.089] 
[1.089] *************************************
[1.089] * * *   M a k e s p a n     1   * * *
[1.089] *************************************
[1.091] Computed next depth properties: array size of 3.
[1.092] Instantiated 114 transitional clauses.
[1.100] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.100] Instantiated 905 universal clauses.
[1.100] Instantiated and added clauses for a total of 1,716 clauses.
[1.100] The encoding contains a total of 703 distinct variables.
[1.100] Attempting solve with solver <glucose4> ...
c 3 assumptions
[1.100] Executed solver; result: UNSAT.
[1.100] 
[1.100] *************************************
[1.100] * * *   M a k e s p a n     2   * * *
[1.100] *************************************
[1.106] Computed next depth properties: array size of 4.
[1.141] Instantiated 93,863 transitional clauses.
[1.180] 75.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.180] Instantiated 462,515 universal clauses.
[1.180] Instantiated and added clauses for a total of 558,094 clauses.
[1.180] The encoding contains a total of 34,374 distinct variables.
[1.180] Attempting solve with solver <glucose4> ...
c 4 assumptions
[1.183] Executed solver; result: UNSAT.
[1.183] 
[1.183] *************************************
[1.183] * * *   M a k e s p a n     3   * * *
[1.183] *************************************
[1.203] Computed next depth properties: array size of 14.
[1.435] Instantiated 290,149 transitional clauses.
[1.752] 85.7% quadratic At-Most-One encodings, the rest being binary encoded.
[1.752] Instantiated 5,147,446 universal clauses.
[1.752] Instantiated and added clauses for a total of 5,995,689 clauses.
[1.752] The encoding contains a total of 76,604 distinct variables.
[1.752] Attempting solve with solver <glucose4> ...
c 14 assumptions
[1.752] Executed solver; result: UNSAT.
[1.752] 
[1.752] *************************************
[1.752] * * *   M a k e s p a n     4   * * *
[1.752] *************************************
[1.780] Computed next depth properties: array size of 32.
[2.029] Instantiated 310,554 transitional clauses.
[2.675] 93.8% quadratic At-Most-One encodings, the rest being binary encoded.
[2.675] Instantiated 10,178,048 universal clauses.
[2.675] Instantiated and added clauses for a total of 16,484,291 clauses.
[2.675] The encoding contains a total of 124,145 distinct variables.
[2.675] Attempting solve with solver <glucose4> ...
c 32 assumptions
[2.677] Executed solver; result: UNSAT.
[2.677] 
[2.677] *************************************
[2.677] * * *   M a k e s p a n     5   * * *
[2.677] *************************************
[2.720] Computed next depth properties: array size of 52.
[3.013] Instantiated 314,459 transitional clauses.
[3.935] 96.2% quadratic At-Most-One encodings, the rest being binary encoded.
[3.935] Instantiated 15,064,092 universal clauses.
[3.935] Instantiated and added clauses for a total of 31,862,842 clauses.
[3.935] The encoding contains a total of 176,375 distinct variables.
[3.935] Attempting solve with solver <glucose4> ...
c 52 assumptions
[3.939] Executed solver; result: UNSAT.
[3.939] 
[3.939] *************************************
[3.939] * * *   M a k e s p a n     6   * * *
[3.939] *************************************
[3.985] Computed next depth properties: array size of 74.
[4.261] Instantiated 317,682 transitional clauses.
[5.478] 97.3% quadratic At-terminate called after throwing an instance of 'Glucose::OutOfMemoryException'
Most-One encodings, the rest being binary encoded.
[5.478] Instantiated 19,803,453 universal clauses.
[5.478] Instantiated and added clauses for a total of 51,983,977 clauses.
[5.478] The encoding contains a total of 233,050 distinct variables.
[5.478] Attempting solve with solver <glucose4> ...
c 74 assumptions
[5.630] Executed solver; result: UNSAT.
[5.630] 
[5.630] *************************************
[5.630] * * *   M a k e s p a n     7   * * *
[5.630] *************************************
[5.689] Computed next depth properties: array size of 98.
[5.983] Instantiated 320,405 transitional clauses.
[7.467] 98.0% quadratic At-Most-One encodings, the rest being binary encoded.
[7.467] Instantiated 24,420,451 universal clauses.
[7.467] Instantiated and added clauses for a total of 76,724,833 clauses.
[7.467] The encoding contains a total of 293,911 distinct variables.
[7.467] Attempting solve with solver <glucose4> ...
c 98 assumptions
[7.474] Executed solver; result: UNSAT.
[7.474] 
[7.474] *************************************
[7.474] * * *   M a k e s p a n     8   * * *
[7.474] *************************************
[7.545] Computed next depth properties: array size of 124.
[7.851] Instantiated 322,586 transitional clauses.
[9.631] 98.4% quadratic At-Most-One encodings, the rest being binary encoded.
[9.631] Instantiated 28,940,781 universal clauses.
[9.631] Instantiated and added clauses for a total of 105,988,200 clauses.
[9.631] The encoding contains a total of 358,674 distinct variables.
[9.631] Attempting solve with solver <glucose4> ...
c 124 assumptions
[9.639] Executed solver; result: UNSAT.
[9.639] 
[9.639] *************************************
[9.639] * * *   M a k e s p a n     9   * * *
[9.639] *************************************
[9.720] Computed next depth properties: array size of 152.
[10.052] Instantiated 323,430 transitional clauses.
[12.440] 98.7% quadratic At-Most-One encodings, the rest being binary encoded.
[12.441] Instantiated 33,386,210 universal clauses.
[12.441] Instantiated and added clauses for a total of 139,697,840 clauses.
[12.441] The encoding contains a total of 426,256 distinct variables.
[12.442] Attempting solve with solver <glucose4> ...
c 152 assumptions
[12.455] Executed solver; result: UNSAT.
[12.455] 
[12.455] *************************************
[12.455] * * *   M a k e s p a n    10   * * *
[12.455] *************************************
[12.583] Computed next depth properties: array size of 166.
[12.949] Instantiated 316,338 transitional clauses.
[16.290] 98.8% quadratic At-Most-One encodings, the rest being binary encoded.
[16.293] Instantiated 37,774,603 universal clauses.
[16.293] Instantiated and added clauses for a total of 177,788,781 clauses.
[16.293] The encoding contains a total of 497,164 distinct variables.
[16.298] Attempting solve with solver <glucose4> ...
c 166 assumptions
[16.339] Executed solver; result: UNSAT.
[16.339] 
[16.339] *************************************
[16.339] * * *   M a k e s p a n    11   * * *
[16.339] *************************************
[16.452] Computed next depth properties: array size of 180.
[17.252] Instantiated 323,674 transitional clauses.
[21.235] 98.9% quadratic At-Most-One encodings, the rest being binary encoded.
[21.250] Instantiated 42,162,996 universal clauses.
[21.250] Instantiated and added clauses for a total of 220,275,451 clauses.
[21.250] The encoding contains a total of 571,754 distinct variables.
[21.252] Attempting solve with solver <glucose4> ...
c 180 assumptions
[21.829] Executed solver; result: UNSAT.
[21.829] 
[21.829] *************************************
[21.829] * * *   M a k e s p a n    12   * * *
[21.829] *************************************
[22.414] Computed next depth properties: array size of 194.
[23.153] Instantiated 331,010 transitional clauses.
./interpreter_t-rex: line 3: 18059 Aborted                 LD_LIBRARY_PATH=../lib/1_62/ ./interpreter_t-rex_binary $@

Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.016] Processed problem encoding.
[0.019] Calculated possible fact changes of composite elements.
[0.019] Initialized instantiation procedure.
[0.019] 
[0.019] *************************************
[0.019] * * *   M a k e s p a n     0   * * *
[0.019] *************************************
[0.019] Instantiated 228 initial clauses.
[0.019] The encoding contains a total of 156 distinct variables.
[0.019] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.019] Executed solver; result: UNSAT.
[0.019] 
[0.019] *************************************
[0.019] * * *   M a k e s p a n     1   * * *
[0.019] *************************************
[0.019] Computed next depth properties: array size of 3.
[0.019] Instantiated 28 transitional clauses.
[0.020] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.020] Instantiated 287 universal clauses.
[0.020] Instantiated and added clauses for a total of 543 clauses.
[0.020] The encoding contains a total of 231 distinct variables.
[0.020] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.020] Executed solver; result: UNSAT.
[0.020] 
[0.020] *************************************
[0.020] * * *   M a k e s p a n     2   * * *
[0.020] *************************************
[0.020] Computed next depth properties: array size of 4.
[0.021] Instantiated 4,692 transitional clauses.
[0.029] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.029] Instantiated 261,569 universal clauses.
[0.029] Instantiated and added clauses for a total of 266,804 clauses.
[0.029] The encoding contains a total of 2,127 distinct variables.
[0.029] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.029] Executed solver; result: UNSAT.
[0.029] 
[0.029] *************************************
[0.029] * * *   M a k e s p a n     3   * * *
[0.029] *************************************
[0.030] Computed next depth properties: array size of 14.
[0.036] Instantiated 14,907 transitional clauses.
[0.051] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.051] Instantiated 516,750 universal clauses.
[0.051] Instantiated and added clauses for a total of 798,461 clauses.
[0.051] The encoding contains a total of 5,042 distinct variables.
[0.051] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.052] Executed solver; result: UNSAT.
[0.052] 
[0.052] *************************************
[0.052] * * *   M a k e s p a n     4   * * *
[0.052] *************************************
[0.053] Computed next depth properties: array size of 32.
[0.059] Instantiated 17,670 transitional clauses.
[0.078] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.078] Instantiated 549,068 universal clauses.
[0.078] Instantiated and added clauses for a total of 1,365,199 clauses.
[0.078] The encoding contains a total of 8,060 distinct variables.
[0.078] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.078] Executed solver; result: UNSAT.
[0.078] 
[0.078] *************************************
[0.078] * * *   M a k e s p a n     5   * * *
[0.078] *************************************
[0.080] Computed next depth properties: array size of 36.
[0.085] Instantiated 12,016 transitional clauses.
[0.105] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.105] Instantiated 564,494 universal clauses.
[0.105] Instantiated and added clauses for a total of 1,941,709 clauses.
[0.105] The encoding contains a total of 11,017 distinct variables.
[0.105] Attempting solve with solver <glucose4> ...
c 36 assumptions
[0.105] Executed solver; result: UNSAT.
[0.105] 
[0.105] *************************************
[0.105] * * *   M a k e s p a n     6   * * *
[0.105] *************************************
[0.107] Computed next depth properties: array size of 40.
[0.113] Instantiated 12,448 transitional clauses.
[0.133] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.133] Instantiated 579,920 universal clauses.
[0.133] Instantiated and added clauses for a total of 2,534,077 clauses.
[0.133] The encoding contains a total of 14,194 distinct variables.
[0.133] Attempting solve with solver <glucose4> ...
c 40 assumptions
[0.136] Executed solver; result: UNSAT.
[0.136] 
[0.136] *************************************
[0.136] * * *   M a k e s p a n     7   * * *
[0.136] *************************************
[0.138] Computed next depth properties: array size of 44.
[0.143] Instantiated 12,880 transitional clauses.
[0.165] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.165] Instantiated 595,346 universal clauses.
[0.165] Instantiated and added clauses for a total of 3,142,303 clauses.
[0.165] The encoding contains a total of 17,591 distinct variables.
[0.165] Attempting solve with solver <glucose4> ...
c 44 assumptions
[0.168] Executed solver; result: UNSAT.
[0.168] 
[0.168] *************************************
[0.168] * * *   M a k e s p a n     8   * * *
[0.168] *************************************
[0.170] Computed next depth properties: array size of 48.
[0.175] Instantiated 13,312 transitional clauses.
[0.197] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.197] Instantiated 610,772 universal clauses.
[0.197] Instantiated and added clauses for a total of 3,766,387 clauses.
[0.197] The encoding contains a total of 21,208 distinct variables.
[0.197] Attempting solve with solver <glucose4> ...
c 48 assumptions
c last restart ## conflicts  :  309 189 
[0.211] Executed solver; result: SAT.
[0.211] Solver returned SAT; a solution has been found at makespan 8.
25
solution 1024 1
1024 142 99 140 101 414 413 412 411 141 106 107 99 193 167 188 168 417 416 415 418 189 165 166 167 
[0.212] Exiting.

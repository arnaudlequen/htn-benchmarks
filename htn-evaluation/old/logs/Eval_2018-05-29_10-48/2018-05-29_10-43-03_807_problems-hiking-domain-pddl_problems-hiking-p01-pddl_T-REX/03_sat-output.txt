Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.006] Processed problem encoding.
[0.006] Calculated possible fact changes of composite elements.
[0.006] Initialized instantiation procedure.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     0   * * *
[0.006] *************************************
[0.006] Instantiated 174 initial clauses.
[0.006] The encoding contains a total of 120 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.006] Computed next depth properties: array size of 3.
[0.007] Instantiated 22 transitional clauses.
[0.007] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.007] Instantiated 219 universal clauses.
[0.007] Instantiated and added clauses for a total of 415 clauses.
[0.007] The encoding contains a total of 179 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.007] Executed solver; result: UNSAT.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     2   * * *
[0.007] *************************************
[0.007] Computed next depth properties: array size of 4.
[0.007] Instantiated 2,153 transitional clauses.
[0.010] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.010] Instantiated 58,704 universal clauses.
[0.010] Instantiated and added clauses for a total of 61,272 clauses.
[0.010] The encoding contains a total of 1,052 distinct variables.
[0.010] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.010] Executed solver; result: UNSAT.
[0.010] 
[0.010] *************************************
[0.010] * * *   M a k e s p a n     3   * * *
[0.010] *************************************
[0.010] Computed next depth properties: array size of 14.
[0.012] Instantiated 5,899 transitional clauses.
[0.016] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.016] Instantiated 116,756 universal clauses.
[0.016] Instantiated and added clauses for a total of 183,927 clauses.
[0.016] The encoding contains a total of 2,488 distinct variables.
[0.016] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.016] Executed solver; result: UNSAT.
[0.016] 
[0.016] *************************************
[0.016] * * *   M a k e s p a n     4   * * *
[0.016] *************************************
[0.017] Computed next depth properties: array size of 32.
[0.019] Instantiated 7,032 transitional clauses.
[0.025] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.025] Instantiated 127,666 universal clauses.
[0.025] Instantiated and added clauses for a total of 318,625 clauses.
[0.025] The encoding contains a total of 4,154 distinct variables.
[0.025] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.025] Executed solver; result: UNSAT.
[0.025] 
[0.025] *************************************
[0.025] * * *   M a k e s p a n     5   * * *
[0.025] *************************************
[0.026] Computed next depth properties: array size of 36.
[0.029] Instantiated 5,230 transitional clauses.
[0.036] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.036] Instantiated 133,756 universal clauses.
[0.036] Instantiated and added clauses for a total of 457,611 clauses.
[0.036] The encoding contains a total of 5,739 distinct variables.
[0.036] Attempting solve with solver <glucose4> ...
c 36 assumptions
[0.036] Executed solver; result: UNSAT.
[0.036] 
[0.036] *************************************
[0.036] * * *   M a k e s p a n     6   * * *
[0.036] *************************************
[0.037] Computed next depth properties: array size of 40.
[0.039] Instantiated 5,492 transitional clauses.
[0.047] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.047] Instantiated 139,846 universal clauses.
[0.047] Instantiated and added clauses for a total of 602,949 clauses.
[0.047] The encoding contains a total of 7,459 distinct variables.
[0.047] Attempting solve with solver <glucose4> ...
c 40 assumptions
[0.049] Executed solver; result: UNSAT.
[0.049] 
[0.049] *************************************
[0.049] * * *   M a k e s p a n     7   * * *
[0.049] *************************************
[0.049] Computed next depth properties: array size of 44.
[0.052] Instantiated 5,754 transitional clauses.
[0.060] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.060] Instantiated 145,936 universal clauses.
[0.060] Instantiated and added clauses for a total of 754,639 clauses.
[0.060] The encoding contains a total of 9,314 distinct variables.
[0.060] Attempting solve with solver <glucose4> ...
c 44 assumptions
c last restart ## conflicts  :  30 120 
[0.062] Executed solver; result: SAT.
[0.062] Solver returned SAT; a solution has been found at makespan 7.
22
solution 218 1
92 71 93 73 214 215 213 94 75 76 71 126 105 127 107 217 216 218 128 109 110 105 
[0.063] Exiting.

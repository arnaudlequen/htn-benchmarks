Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.130] Processed problem encoding.
[0.205] Calculated possible fact changes of composite elements.
[0.206] Initialized instantiation procedure.
[0.206] 
[0.206] *************************************
[0.206] * * *   M a k e s p a n     0   * * *
[0.206] *************************************
[0.209] Instantiated 448 initial clauses.
[0.209] The encoding contains a total of 306 distinct variables.
[0.209] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.209] Executed solver; result: UNSAT.
[0.209] 
[0.209] *************************************
[0.209] * * *   M a k e s p a n     1   * * *
[0.209] *************************************
[0.209] Computed next depth properties: array size of 3.
[0.210] Instantiated 66 transitional clauses.
[0.216] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.216] Instantiated 575 universal clauses.
[0.216] Instantiated and added clauses for a total of 1,089 clauses.
[0.216] The encoding contains a total of 451 distinct variables.
[0.216] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.216] Executed solver; result: UNSAT.
[0.216] 
[0.216] *************************************
[0.216] * * *   M a k e s p a n     2   * * *
[0.216] *************************************
[0.218] Computed next depth properties: array size of 4.
[0.230] Instantiated 31,637 transitional clauses.
[0.540] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.540] Instantiated 12,588,638 universal clauses.
[0.540] Instantiated and added clauses for a total of 12,621,364 clauses.
[0.540] The encoding contains a total of 12,100 distinct variables.
[0.540] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.541] Executed solver; result: UNSAT.
[0.541] 
[0.541] *************************************
[0.541] * * *   M a k e s p a n     3   * * *
[0.541] *************************************
[0.548] Computed next depth properties: array size of 14.
[0.604] Instantiated 101,179 transitional clauses.
[1.238] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.238] Instantiated 24,891,446 universal clauses.
[1.238] Instantiated and added clauses for a total of 37,613,989 clauses.
[1.238] The encoding contains a total of 27,860 distinct variables.
[1.238] Attempting solve with solver <glucose4> ...
c 14 assumptions
[1.238] Executed solver; result: UNSAT.
[1.238] 
[1.238] *************************************
[1.238] * * *   M a k e s p a n     4   * * *
[1.238] *************************************
[1.247] Computed next depth properties: array size of 32.
[1.308] Instantiated 116,199 transitional clauses.
[1.934] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.934] Instantiated 25,584,585 universal clauses.
[1.934] Instantiated and added clauses for a total of 63,314,773 clauses.
[1.934] The encoding contains a total of 45,634 distinct variables.
[1.934] Attempting solve with solver <glucose4> ...
c 32 assumptions
[1.936] Executed solver; result: UNSAT.
[1.936] 
[1.936] *************************************
[1.936] * * *   M a k e s p a n     5   * * *
[1.936] *************************************
[1.950] Computed next depth properties: array size of 52.
[2.010] Instantiated 113,744 transitional clauses.
[2.682] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.682] Instantiated 26,194,876 universal clauses.
[2.682] Instantiated and added clauses for a total of 89,623,393 clauses.
[2.682] The encoding contains a total of 64,872 distinct variables.
[2.682] Attempting solve with solver <glucose4> ...
c 52 assumptions
[2.685] Executed solver; result: UNSAT.
[2.685] 
[2.685] *************************************
[2.685] * * *   M a k e s p a n     6   * * *
[2.685] *************************************
[2.701] Computed next depth properties: array size of 74.
[2.771] Instantiated 109,712 transitional clauses.
[3.523] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.523] Instantiated 26,730,566 universal clauses.
[3.523] Instantiated and added clauses for a total of 116,463,671 clauses.
[3.523] The encoding contains a total of 84,481 distinct variables.
[3.523] Attempting solve with solver <glucose4> ...
c 74 assumptions
[3.526] Executed solver; result: UNSAT.
[3.526] 
[3.526] *************************************
[3.526] * * *   M a k e s p a n     7   * * *
[3.526] *************************************
[3.545] Computed next depth properties: array size of 82.
[3.609] Instantiated 97,736 transitional clauses.
[4.364] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.364] Instantiated 27,209,220 universal clauses.
[4.364] Instantiated and added clauses for a total of 143,770,627 clauses.
[4.364] The encoding contains a total of 104,968 distinct variables.
[4.364] Attempting solve with solver <glucose4> ...
c 82 assumptions
[4.371] Executed solver; result: UNSAT.
[4.371] 
[4.371] *************************************
[4.371] * * *   M a k e s p a n     8   * * *
[4.371] *************************************
[4.392] Computed next depth properties: array size of 90.
[4.468] Instantiated 100,188 transitional clauses.
[5.248] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[5.248] Instantiated 27,687,874 universal clauses.
[5.248] Instantiated and added clauses for a total of 171,558,689 clauses.
[5.248] The encoding contains a total of 126,689 distinct variables.
[5.248] Attempting solve with solver <glucose4> ...
c 90 assumptions
[5.257] Executed solver; result: UNSAT.
[5.257] 
[5.257] *************************************
[5.257] * * *   M a k e s p a n     9   * * *
[5.257] *************************************
[5.278] Computed next depth properties: array size of 98.
[5.347] Instantiated 102,640 transitional clauses.
[6.134] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[6.134] Instantiated 28,166,528 universal clauses.
[6.134] Instantiated and added clauses for a total of 199,827,857 clauses.
[6.134] The encoding contains a total of 149,644 distinct variables.
[6.134] Attempting solve with solver <glucose4> ...
c 98 assumptions
[6.220] Executed solver; result: UNSAT.
[6.220] 
[6.220] *************************************
[6.220] * * *   M a k e s p a n    10   * * *
[6.220] *************************************
[6.243] Computed next depth properties: array size of 106.
[6.334] Instantiated 105,092 transitional clauses.
[7.168] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[7.168] Instantiated 28,645,182 universal clauses.
[7.168] Instantiated and added clauses for a total of 228,578,131 clauses.
[7.168] The encoding contains a total of 173,833 distinct variables.
[7.168] Attempting solve with solver <glucose4> ...
c 106 assumptions
[7.194] Executed solver; result: UNSAT.
[7.194] 
[7.194] *************************************
[7.194] * * *   M a k e s p a n    11   * * *
[7.194] *************************************
[7.218] Computed next depth properties: array size of 114.
[7.294] Instantiated 107,544 transitional clauses.
[8.188] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[8.188] Instantiated 29,123,836 universal clauses.
[8.188] Instantiated and added clauses for a total of 257,809,511 clauses.
[8.188] The encoding contains a total of 199,256 distinct variables.
[8.188] Attempting solve with solver <glucose4> ...
c 114 assumptions
c last restart ## conflicts  :  30 2389 
[11.790] Executed solver; result: SAT.
[11.790] Solver returned SAT; a solution has been found at makespan 11.
53
solution 5853 1
5853 295 284 291 285 1303 1306 1305 1304 1307 292 272 273 284 359 348 355 349 1308 1311 1312 1310 1309 356 336 337 348 423 412 419 413 1317 1313 1316 1314 1315 420 400 401 412 487 476 483 477 1322 1319 1318 1321 1320 484 464 465 476 
[11.793] Exiting.

Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.104] Processed problem encoding.
[0.110] Calculated possible fact changes of composite elements.
[0.110] Initialized instantiation procedure.
[0.110] 
[0.110] *************************************
[0.110] * * *   M a k e s p a n     0   * * *
[0.110] *************************************
[0.113] Instantiated 269 initial clauses.
[0.114] The encoding contains a total of 183 distinct variables.
[0.114] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.114] Executed solver; result: UNSAT.
[0.114] 
[0.114] *************************************
[0.114] * * *   M a k e s p a n     1   * * *
[0.114] *************************************
[0.114] Computed next depth properties: array size of 3.
[0.115] Instantiated 23 transitional clauses.
[0.120] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.120] Instantiated 357 universal clauses.
[0.120] Instantiated and added clauses for a total of 649 clauses.
[0.120] The encoding contains a total of 286 distinct variables.
[0.120] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.120] Executed solver; result: UNSAT.
[0.120] 
[0.120] *************************************
[0.120] * * *   M a k e s p a n     2   * * *
[0.120] *************************************
[0.120] Computed next depth properties: array size of 6.
[0.122] Instantiated 585 transitional clauses.
[0.128] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.128] Instantiated 4,542 universal clauses.
[0.128] Instantiated and added clauses for a total of 5,776 clauses.
[0.128] The encoding contains a total of 774 distinct variables.
[0.128] Attempting solve with solver <glucose4> ...
c 6 assumptions
[0.128] Executed solver; result: UNSAT.
[0.128] 
[0.128] *************************************
[0.128] * * *   M a k e s p a n     3   * * *
[0.128] *************************************
[0.131] Computed next depth properties: array size of 14.
[0.147] Instantiated 117,393 transitional clauses.
[0.447] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.447] Instantiated 11,509,706 universal clauses.
[0.447] Instantiated and added clauses for a total of 11,632,875 clauses.
[0.447] The encoding contains a total of 8,672 distinct variables.
[0.447] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.447] Executed solver; result: UNSAT.
[0.447] 
[0.447] *************************************
[0.447] * * *   M a k e s p a n     4   * * *
[0.447] *************************************
[0.452] Computed next depth properties: array size of 23.
[0.486] Instantiated 131,699 transitional clauses.
[1.505] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[1.505] Instantiated 23,016,097 universal clauses.
[1.505] Instantiated and added clauses for a total of 34,780,671 clauses.
[1.505] The encoding contains a total of 23,777 distinct variables.
[1.505] Attempting solve with solver <glucose4> ...
c 23 assumptions
[1.506] Executed solver; result: UNSAT.
[1.506] 
[1.506] *************************************
[1.506] * * *   M a k e s p a n     5   * * *
[1.506] *************************************
[1.512] Computed next depth properties: array size of 33.
[1.547] Instantiated 146,081 transitional clauses.
[3.185] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.185] Instantiated 34,523,715 universal clauses.
[3.185] Instantiated and added clauses for a total of 69,450,467 clauses.
[3.185] The encoding contains a total of 46,128 distinct variables.
[3.185] Attempting solve with solver <glucose4> ...
c 33 assumptions
[3.187] Executed solver; result: UNSAT.
[3.187] 
[3.187] *************************************
[3.187] * * *   M a k e s p a n     6   * * *
[3.187] *************************************
[3.196] Computed next depth properties: array size of 44.
[3.237] Instantiated 160,539 transitional clauses.
[5.579] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[5.579] Instantiated 46,032,560 universal clauses.
[5.579] Instantiated and added clauses for a total of 115,643,566 clauses.
[5.579] The encoding contains a total of 75,764 distinct variables.
[5.579] Attempting solve with solver <glucose4> ...
c 44 assumptions
[5.582] Executed solver; result: UNSAT.
[5.582] 
[5.582] *************************************
[5.582] * * *   M a k e s p a n     7   * * *
[5.582] *************************************
[5.594] Computed next depth properties: array size of 56.
[5.647] Instantiated 175,073 transitional clauses.
[8.759] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[8.759] Instantiated 57,542,632 universal clauses.
[8.759] Instantiated and added clauses for a total of 173,361,271 clauses.
[8.759] The encoding contains a total of 112,724 distinct variables.
[8.759] Attempting solve with solver <glucose4> ...
c 56 assumptions
c |       90         0      111 |   77792 115717122 232588102 |     2     6229      428     3768 | 30.988 % |
c |      185         0      108 |   77792 115717122 232588102 |     3    11217      432     8780 | 30.988 % |
c |      333         0       90 |   77792 115717122 232588102 |     4    13822      478    16175 | 30.988 % |
c |      359         0      111 |   77792 115717122 232588102 |     5    13913      756    26084 | 30.988 % |
c |      376         0      132 |   77792 115717122 232588102 |     5    23913      862    26084 | 30.988 % |
[75.171] Executed solver; result: UNSAT.
[75.171] 
[75.171] *************************************
[75.171] * * *   M a k e s p a n     8   * * *
[75.171] *************************************
[75.183] Computed next depth properties: array size of 69.
[75.244] Instantiated 189,683 transitional clauses.
[80.216] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[80.223] Instantiated 69,053,931 universal clauses.
[80.230] Instantiated and added clauses for a total of 242,604,885 clauses.
[80.230] The encoding contains a total of 157,047 distinct variables.
[80.230] Attempting solve with solver <glucose4> ...
c 69 assumptions
c |      414        13      144 |  115124 173449957 348470317 |     6    21473     1046    38523 | 26.694 % |
c last restart ## conflicts  :  3221 246 
[80.927] Executed solver; result: SAT.
[80.927] Solver returned SAT; a solution has been found at makespan 8.
35
solution 5763 1
7 567 206 90 1723 153 147 150 24 3576 701 218 100 1943 157 163 160 55 4856 953 324 121 2001 169 166 172 58 5763 842 259 144 2193 180 174 177 
[80.935] Exiting.

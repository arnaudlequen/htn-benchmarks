Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.006] Processed problem encoding.
[0.006] Calculated possible fact changes of composite elements.
[0.006] Initialized instantiation procedure.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     0   * * *
[0.006] *************************************
[0.006] Instantiated 156 initial clauses.
[0.006] The encoding contains a total of 108 distinct variables.
[0.006] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.006] Executed solver; result: UNSAT.
[0.006] 
[0.006] *************************************
[0.006] * * *   M a k e s p a n     1   * * *
[0.006] *************************************
[0.006] Computed next depth properties: array size of 3.
[0.007] Instantiated 10 transitional clauses.
[0.007] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.007] Instantiated 199 universal clauses.
[0.007] Instantiated and added clauses for a total of 365 clauses.
[0.007] The encoding contains a total of 163 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.007] Executed solver; result: UNSAT.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     2   * * *
[0.007] *************************************
[0.007] Computed next depth properties: array size of 4.
[0.007] Instantiated 2,075 transitional clauses.
[0.009] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.009] Instantiated 58,232 universal clauses.
[0.009] Instantiated and added clauses for a total of 60,672 clauses.
[0.009] The encoding contains a total of 947 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.010] Executed solver; result: UNSAT.
[0.010] 
[0.010] *************************************
[0.010] * * *   M a k e s p a n     3   * * *
[0.010] *************************************
[0.010] Computed next depth properties: array size of 14.
[0.011] Instantiated 5,006 transitional clauses.
[0.015] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.015] Instantiated 114,113 universal clauses.
[0.015] Instantiated and added clauses for a total of 179,791 clauses.
[0.015] The encoding contains a total of 2,098 distinct variables.
[0.015] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.015] Executed solver; result: UNSAT.
[0.015] 
[0.015] *************************************
[0.015] * * *   M a k e s p a n     4   * * *
[0.015] *************************************
[0.016] Computed next depth properties: array size of 31.
[0.017] Instantiated 5,126 transitional clauses.
[0.022] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.022] Instantiated 120,712 universal clauses.
[0.022] Instantiated and added clauses for a total of 305,629 clauses.
[0.022] The encoding contains a total of 3,399 distinct variables.
[0.022] Attempting solve with solver <glucose4> ...
c 31 assumptions
c last restart ## conflicts  :  3 81 
[0.022] Executed solver; result: SAT.
[0.022] Solver returned SAT; a solution has been found at makespan 4.
18
solution 68 1
30 3 26 5 27 28 14 15 3 68 60 63 61 64 65 56 57 60 
[0.022] Exiting.

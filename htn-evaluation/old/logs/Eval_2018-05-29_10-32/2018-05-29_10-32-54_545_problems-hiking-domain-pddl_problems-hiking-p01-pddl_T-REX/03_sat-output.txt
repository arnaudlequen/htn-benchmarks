Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.006] Processed problem encoding.
[0.007] Calculated possible fact changes of composite elements.
[0.007] Initialized instantiation procedure.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     0   * * *
[0.007] *************************************
[0.007] Instantiated 174 initial clauses.
[0.007] The encoding contains a total of 120 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.007] Executed solver; result: UNSAT.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     1   * * *
[0.007] *************************************
[0.007] Computed next depth properties: array size of 3.
[0.007] Instantiated 22 transitional clauses.
[0.007] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.007] Instantiated 219 universal clauses.
[0.007] Instantiated and added clauses for a total of 415 clauses.
[0.007] The encoding contains a total of 179 distinct variables.
[0.007] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.007] Executed solver; result: UNSAT.
[0.007] 
[0.007] *************************************
[0.007] * * *   M a k e s p a n     2   * * *
[0.007] *************************************
[0.008] Computed next depth properties: array size of 4.
[0.008] Instantiated 2,153 transitional clauses.
[0.011] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.011] Instantiated 58,704 universal clauses.
[0.011] Instantiated and added clauses for a total of 61,272 clauses.
[0.011] The encoding contains a total of 1,052 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.011] Executed solver; result: UNSAT.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     3   * * *
[0.011] *************************************
[0.012] Computed next depth properties: array size of 14.
[0.013] Instantiated 5,899 transitional clauses.
[0.018] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.018] Instantiated 116,756 universal clauses.
[0.018] Instantiated and added clauses for a total of 183,927 clauses.
[0.018] The encoding contains a total of 2,488 distinct variables.
[0.018] Attempting solve with solver <glucose4> ...
c 14 assumptions
[0.018] Executed solver; result: UNSAT.
[0.018] 
[0.018] *************************************
[0.018] * * *   M a k e s p a n     4   * * *
[0.018] *************************************
[0.019] Computed next depth properties: array size of 32.
[0.021] Instantiated 7,032 transitional clauses.
[0.028] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.028] Instantiated 127,666 universal clauses.
[0.028] Instantiated and added clauses for a total of 318,625 clauses.
[0.028] The encoding contains a total of 4,154 distinct variables.
[0.028] Attempting solve with solver <glucose4> ...
c 32 assumptions
[0.028] Executed solver; result: UNSAT.
[0.028] 
[0.028] *************************************
[0.028] * * *   M a k e s p a n     5   * * *
[0.028] *************************************
[0.029] Computed next depth properties: array size of 36.
[0.031] Instantiated 5,230 transitional clauses.
[0.038] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.038] Instantiated 133,756 universal clauses.
[0.038] Instantiated and added clauses for a total of 457,611 clauses.
[0.038] The encoding contains a total of 5,739 distinct variables.
[0.038] Attempting solve with solver <glucose4> ...
c 36 assumptions
[0.039] Executed solver; result: UNSAT.
[0.039] 
[0.039] *************************************
[0.039] * * *   M a k e s p a n     6   * * *
[0.039] *************************************
[0.040] Computed next depth properties: array size of 40.
[0.043] Instantiated 5,492 transitional clauses.
[0.051] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.051] Instantiated 139,846 universal clauses.
[0.051] Instantiated and added clauses for a total of 602,949 clauses.
[0.051] The encoding contains a total of 7,459 distinct variables.
[0.051] Attempting solve with solver <glucose4> ...
c 40 assumptions
[0.053] Executed solver; result: UNSAT.
[0.053] 
[0.053] *************************************
[0.053] * * *   M a k e s p a n     7   * * *
[0.053] *************************************
[0.054] Computed next depth properties: array size of 44.
[0.056] Instantiated 5,754 transitional clauses.
[0.065] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.065] Instantiated 145,936 universal clauses.
[0.065] Instantiated and added clauses for a total of 754,639 clauses.
[0.065] The encoding contains a total of 9,314 distinct variables.
[0.065] Attempting solve with solver <glucose4> ...
c 44 assumptions
c last restart ## conflicts  :  30 120 
[0.067] Executed solver; result: SAT.
[0.067] Solver returned SAT; a solution has been found at makespan 7.
22
solution 218 1
92 71 93 73 214 215 213 94 75 76 71 126 105 127 107 217 216 218 128 109 110 105 
[0.067] Exiting.

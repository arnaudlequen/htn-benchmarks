Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.008] Processed problem encoding.
[0.009] Calculated possible fact changes of composite elements.
[0.009] Initialized instantiation procedure.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     0   * * *
[0.009] *************************************
[0.009] Instantiated 1,138 initial clauses.
[0.009] The encoding contains a total of 1,036 distinct variables.
[0.009] Attempting solve with solver <glucose4> ...
c 5 assumptions
[0.009] Executed solver; result: UNSAT.
[0.009] 
[0.009] *************************************
[0.009] * * *   M a k e s p a n     1   * * *
[0.009] *************************************
[0.010] Computed next depth properties: array size of 7.
[0.010] Instantiated 1,851 transitional clauses.
[0.011] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.011] Instantiated 3,404 universal clauses.
[0.011] Instantiated and added clauses for a total of 6,393 clauses.
[0.011] The encoding contains a total of 1,224 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.011] Executed solver; result: UNSAT.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     2   * * *
[0.011] *************************************
[0.011] Computed next depth properties: array size of 8.
[0.013] Instantiated 3,425 transitional clauses.
[0.014] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.014] Instantiated 19,967 universal clauses.
[0.014] Instantiated and added clauses for a total of 29,785 clauses.
[0.014] The encoding contains a total of 3,551 distinct variables.
[0.014] Attempting solve with solver <glucose4> ...
c 8 assumptions
[0.014] Executed solver; result: UNSAT.
[0.014] 
[0.014] *************************************
[0.014] * * *   M a k e s p a n     3   * * *
[0.014] *************************************
[0.015] Computed next depth properties: array size of 19.
[0.017] Instantiated 6,439 transitional clauses.
[0.022] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.022] Instantiated 131,566 universal clauses.
[0.022] Instantiated and added clauses for a total of 167,790 clauses.
[0.022] The encoding contains a total of 4,862 distinct variables.
[0.022] Attempting solve with solver <glucose4> ...
c 19 assumptions
c last restart ## conflicts  :  1 86 
[0.022] Executed solver; result: SAT.
[0.022] Solver returned SAT; a solution has been found at makespan 3.
12
solution 458 1
38 25 35 26 36 37 13 14 25 2 3 458 
[0.022] Exiting.

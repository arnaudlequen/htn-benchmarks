Command : ./interpreter_t-rex_binary -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.009] Processed problem encoding.
[0.010] Calculated possible fact changes of composite elements.
[0.010] Initialized instantiation procedure.
[0.010] 
[0.010] *************************************
[0.010] * * *   M a k e s p a n     0   * * *
[0.010] *************************************
[0.010] Instantiated 156 initial clauses.
[0.010] The encoding contains a total of 107 distinct variables.
[0.010] Attempting solve with solver <glucose4> ...
c 2 assumptions
[0.010] Executed solver; result: UNSAT.
[0.010] 
[0.010] *************************************
[0.010] * * *   M a k e s p a n     1   * * *
[0.010] *************************************
[0.010] Computed next depth properties: array size of 3.
[0.010] Instantiated 8 transitional clauses.
[0.011] Instantiated 198 universal clauses.
[0.011] Instantiated and added clauses for a total of 362 clauses.
[0.011] The encoding contains a total of 190 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.011] Executed solver; result: UNSAT.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     2   * * *
[0.011] *************************************
[0.011] Computed next depth properties: array size of 4.
[0.012] Instantiated 3,225 transitional clauses.
[0.014] Instantiated 12,777 universal clauses.
[0.014] Instantiated and added clauses for a total of 16,364 clauses.
[0.014] The encoding contains a total of 2,422 distinct variables.
[0.014] Attempting solve with solver <glucose4> ...
c 4 assumptions
[0.014] Executed solver; result: UNSAT.
[0.014] 
[0.014] *************************************
[0.014] * * *   M a k e s p a n     3   * * *
[0.014] *************************************
[0.015] Computed next depth properties: array size of 15.
[0.016] Instantiated 6,153 transitional clauses.
[0.019] Instantiated 19,872 universal clauses.
[0.019] Instantiated and added clauses for a total of 42,389 clauses.
[0.019] The encoding contains a total of 3,812 distinct variables.
[0.019] Attempting solve with solver <glucose4> ...
c 15 assumptions
[0.019] Executed solver; result: UNSAT.
[0.019] 
[0.019] *************************************
[0.019] * * *   M a k e s p a n     4   * * *
[0.019] *************************************
[0.020] Computed next depth properties: array size of 31.
[0.021] Instantiated 874 transitional clauses.
[0.023] Instantiated 2,760 universal clauses.
[0.023] Instantiated and added clauses for a total of 46,023 clauses.
[0.023] The encoding contains a total of 4,268 distinct variables.
[0.023] Attempting solve with solver <glucose4> ...
c 31 assumptions
c last restart ## conflicts  :  1 88 
[0.023] Executed solver; result: SAT.
[0.023] Solver returned SAT; a solution has been found at makespan 4.
18
solution 74 1
37 21 33 22 34 35 14 15 21 74 58 70 59 71 72 51 52 58 
[0.023] Exiting.

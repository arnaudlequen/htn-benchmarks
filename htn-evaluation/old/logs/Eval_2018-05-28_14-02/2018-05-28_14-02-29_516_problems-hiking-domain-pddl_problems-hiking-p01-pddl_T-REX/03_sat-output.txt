Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.011] Processed problem encoding.
[0.012] Calculated possible fact changes of composite elements.
[0.013] Initialized instantiation procedure.
[0.013] 
[0.013] *************************************
[0.013] * * *   M a k e s p a n     0   * * *
[0.013] *************************************
[0.014] Instantiated 1,138 initial clauses.
[0.014] The encoding contains a total of 1,036 distinct variables.
[0.014] Attempting solve with solver <glucose4> ...
c 5 assumptions
[0.014] Executed solver; result: UNSAT.
[0.014] 
[0.014] *************************************
[0.014] * * *   M a k e s p a n     1   * * *
[0.014] *************************************
[0.014] Computed next depth properties: array size of 7.
[0.015] Instantiated 1,851 transitional clauses.
[0.016] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.016] Instantiated 3,404 universal clauses.
[0.016] Instantiated and added clauses for a total of 6,393 clauses.
[0.016] The encoding contains a total of 1,224 distinct variables.
[0.016] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.016] Executed solver; result: UNSAT.
[0.016] 
[0.016] *************************************
[0.016] * * *   M a k e s p a n     2   * * *
[0.016] *************************************
[0.017] Computed next depth properties: array size of 8.
[0.019] Instantiated 3,425 transitional clauses.
[0.022] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.022] Instantiated 19,967 universal clauses.
[0.022] Instantiated and added clauses for a total of 29,785 clauses.
[0.022] The encoding contains a total of 3,551 distinct variables.
[0.022] Attempting solve with solver <glucose4> ...
c 8 assumptions
[0.022] Executed solver; result: UNSAT.
[0.022] 
[0.022] *************************************
[0.022] * * *   M a k e s p a n     3   * * *
[0.022] *************************************
[0.024] Computed next depth properties: array size of 19.
[0.026] Instantiated 6,439 transitional clauses.
[0.030] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.030] Instantiated 131,566 universal clauses.
[0.030] Instantiated and added clauses for a total of 167,790 clauses.
[0.030] The encoding contains a total of 4,862 distinct variables.
[0.030] Attempting solve with solver <glucose4> ...
c 19 assumptions
c last restart ## conflicts  :  1 86 
[0.031] Executed solver; result: SAT.
[0.031] Solver returned SAT; a solution has been found at makespan 3.
12
solution 458 1
38 25 35 26 36 37 13 14 25 2 3 458 
[0.031] Exiting.

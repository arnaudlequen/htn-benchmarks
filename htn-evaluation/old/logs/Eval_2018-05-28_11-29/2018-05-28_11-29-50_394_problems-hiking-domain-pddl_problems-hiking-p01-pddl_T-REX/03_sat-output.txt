Command : ./interpreter_t-rex_binary -P -b8192 -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.000] Parsed head comment information.
[0.009] Processed problem encoding.
[0.010] Calculated possible fact changes of composite elements.
[0.010] Initialized instantiation procedure.
[0.010] 
[0.010] *************************************
[0.010] * * *   M a k e s p a n     0   * * *
[0.010] *************************************
[0.011] Instantiated 1,118 initial clauses.
[0.011] The encoding contains a total of 1,023 distinct variables.
[0.011] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.011] Executed solver; result: UNSAT.
[0.011] 
[0.011] *************************************
[0.011] * * *   M a k e s p a n     1   * * *
[0.011] *************************************
[0.011] Computed next depth properties: array size of 5.
[0.011] Instantiated 1,845 transitional clauses.
[0.012] Instantiated 2,001 universal clauses.
[0.012] Instantiated and added clauses for a total of 4,964 clauses.
[0.012] The encoding contains a total of 1,255 distinct variables.
[0.012] Attempting solve with solver <glucose4> ...
c 5 assumptions
[0.012] Executed solver; result: UNSAT.
[0.012] 
[0.012] *************************************
[0.012] * * *   M a k e s p a n     2   * * *
[0.012] *************************************
[0.012] Computed next depth properties: array size of 6.
[0.013] Instantiated 3,223 transitional clauses.
[0.016] Instantiated 12,928 universal clauses.
[0.016] Instantiated and added clauses for a total of 21,115 clauses.
[0.016] The encoding contains a total of 3,488 distinct variables.
[0.016] Attempting solve with solver <glucose4> ...
c 6 assumptions
[0.016] Executed solver; result: UNSAT.
[0.016] 
[0.016] *************************************
[0.016] * * *   M a k e s p a n     3   * * *
[0.016] *************************************
[0.016] Computed next depth properties: array size of 17.
[0.017] Instantiated 6,065 transitional clauses.
[0.019] Instantiated 19,447 universal clauses.
[0.019] Instantiated and added clauses for a total of 46,627 clauses.
[0.019] The encoding contains a total of 4,767 distinct variables.
[0.019] Attempting solve with solver <glucose4> ...
c 17 assumptions
c last restart ## conflicts  :  2 75 
[0.019] Executed solver; result: SAT.
[0.019] Solver returned SAT; a solution has been found at makespan 3.
10
solution 491 1
32 23 33 24 34 35 8 9 23 491 
[0.019] Exiting.

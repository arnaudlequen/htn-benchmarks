Command : ./interpreter_t-rex_binary -P -b8192 -e -i f.cnf 
[0.000] This is T-REX interpreter, prototype version 04/2018.
[0.000] Reading problem encoding from file "f.cnf".
[0.001] Parsed head comment information.
[0.077] Processed problem encoding.
[0.131] Calculated possible fact changes of composite elements.
[0.131] Initialized instantiation procedure.
[0.131] 
[0.131] *************************************
[0.131] * * *   M a k e s p a n     0   * * *
[0.131] *************************************
[0.133] Instantiated 637 initial clauses.
[0.133] The encoding contains a total of 393 distinct variables.
[0.133] Attempting solve with solver <glucose4> ...
c 3 assumptions
[0.133] Executed solver; result: UNSAT.
[0.133] 
[0.133] *************************************
[0.133] * * *   M a k e s p a n     1   * * *
[0.133] *************************************
[0.133] Computed next depth properties: array size of 5.
[0.134] Instantiated 58 transitional clauses.
[0.138] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.138] Instantiated 987 universal clauses.
[0.138] Instantiated and added clauses for a total of 1,682 clauses.
[0.138] The encoding contains a total of 649 distinct variables.
[0.138] Attempting solve with solver <glucose4> ...
c 5 assumptions
[0.138] Executed solver; result: UNSAT.
[0.138] 
[0.138] *************************************
[0.138] * * *   M a k e s p a n     2   * * *
[0.138] *************************************
[0.141] Computed next depth properties: array size of 7.
[0.163] Instantiated 46,248 transitional clauses.
[0.804] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[0.804] Instantiated 13,478,653 universal clauses.
[0.804] Instantiated and added clauses for a total of 13,526,583 clauses.
[0.804] The encoding contains a total of 17,683 distinct variables.
[0.804] Attempting solve with solver <glucose4> ...
c 7 assumptions
[0.826] Executed solver; result: UNSAT.
[0.826] 
[0.826] *************************************
[0.826] * * *   M a k e s p a n     3   * * *
[0.826] *************************************
[0.835] Computed next depth properties: array size of 27.
[0.909] Instantiated 110,370 transitional clauses.
[2.103] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[2.103] Instantiated 27,509,235 universal clauses.
[2.103] Instantiated and added clauses for a total of 41,146,188 clauses.
[2.103] The encoding contains a total of 38,241 distinct variables.
[2.103] Attempting solve with solver <glucose4> ...
c 27 assumptions
[2.103] Executed solver; result: UNSAT.
[2.103] 
[2.103] *************************************
[2.103] * * *   M a k e s p a n     4   * * *
[2.103] *************************************
[2.115] Computed next depth properties: array size of 61.
[2.192] Instantiated 105,510 transitional clauses.
[3.464] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[3.464] Instantiated 28,930,285 universal clauses.
[3.464] Instantiated and added clauses for a total of 70,181,983 clauses.
[3.464] The encoding contains a total of 61,965 distinct variables.
[3.464] Attempting solve with solver <glucose4> ...
c 61 assumptions
[3.464] Executed solver; result: UNSAT.
[3.464] 
[3.464] *************************************
[3.464] * * *   M a k e s p a n     5   * * *
[3.464] *************************************
[3.481] Computed next depth properties: array size of 95.
[3.559] Instantiated 110,692 transitional clauses.
[4.908] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[4.908] Instantiated 30,347,833 universal clauses.
[4.908] Instantiated and added clauses for a total of 100,640,508 clauses.
[4.908] The encoding contains a total of 88,427 distinct variables.
[4.908] Attempting solve with solver <glucose4> ...
c 95 assumptions
[4.909] Executed solver; result: UNSAT.
[4.909] 
[4.909] *************************************
[4.909] * * *   M a k e s p a n     6   * * *
[4.909] *************************************
[4.933] Computed next depth properties: array size of 129.
[5.023] Instantiated 115,646 transitional clauses.
[6.500] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[6.500] Instantiated 31,761,869 universal clauses.
[6.500] Instantiated and added clauses for a total of 132,518,023 clauses.
[6.500] The encoding contains a total of 117,541 distinct variables.
[6.500] Attempting solve with solver <glucose4> ...
c 129 assumptions
[6.501] Executed solver; result: UNSAT.
[6.501] 
[6.501] *************************************
[6.501] * * *   M a k e s p a n     7   * * *
[6.501] *************************************
[6.524] Computed next depth properties: array size of 163.
[6.621] Instantiated 120,420 transitional clauses.
[8.198] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[8.198] Instantiated 33,172,775 universal clauses.
[8.198] Instantiated and added clauses for a total of 165,811,218 clauses.
[8.198] The encoding contains a total of 149,225 distinct variables.
[8.198] Attempting solve with solver <glucose4> ...
c 163 assumptions
[8.199] Executed solver; result: UNSAT.
[8.199] 
[8.199] *************************************
[8.199] * * *   M a k e s p a n     8   * * *
[8.199] *************************************
[8.226] Computed next depth properties: array size of 197.
[8.324] Instantiated 125,030 transitional clauses.
[9.965] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[9.965] Instantiated 34,581,053 universal clauses.
[9.965] Instantiated and added clauses for a total of 200,517,301 clauses.
[9.965] The encoding contains a total of 183,397 distinct variables.
[9.965] Attempting solve with solver <glucose4> ...
c 197 assumptions
[9.967] Executed solver; result: UNSAT.
[9.967] 
[9.967] *************************************
[9.967] * * *   M a k e s p a n     9   * * *
[9.967] *************************************
[10.000] Computed next depth properties: array size of 231.
[10.109] Instantiated 129,406 transitional clauses.
[11.866] 100.0% quadratic At-Most-One encodings, the rest being binary encoded.
[11.866] Instantiated 35,986,723 universal clauses.
[11.866] Instantiated and added clauses for a total of 236,633,430 clauses.
[11.866] The encoding contains a total of 219,879 distinct variables.
[11.866] Attempting solve with solver <glucose4> ...
c 231 assumptions
c last restart ## conflicts  :  335 331 
[13.392] Executed solver; result: SAT.
[13.392] Solver returned SAT; a solution has been found at makespan 9.
129
solution 4108 1
4041 25 21 26 22 27 28 8 9 21 66 54 63 55 64 65 48 49 54 103 91 100 92 101 102 85 86 91 140 128 137 129 138 139 122 123 128 173 169 174 170 175 176 156 157 169 210 206 211 207 212 213 193 194 206 247 243 248 244 249 250 230 231 243 949 4108 290 276 285 277 286 287 278 279 276 327 313 322 314 323 324 315 316 313 364 350 359 351 360 361 352 353 350 401 387 396 388 397 398 389 390 387 438 424 433 425 434 435 426 427 424 475 461 470 462 471 472 463 464 461 512 498 507 499 508 509 500 501 498 
[13.395] Exiting.
